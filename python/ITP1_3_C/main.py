def swap(l,f):
    return [l[1],l[0]] if f else l

l=[]
for i in range(3000):
    l.append(input())
    if l[-1]=="0 0":
        break

print(''.join(list(map(lambda b:str(b[0])+" "+str(b[1])+"\n",list(map(lambda a:swap(a,a[0]>a[1]),list(map(lambda x:list(map(lambda y:int(y),x.split(" "))),l))))[:-1]))),end="")
