def c(a,b,f):
    return a+b if f=="+" else (a-b if f=="-" else (a*b if f=="*" else a//b))

while True:
    s=input().split(" ")
    if s[1]=="?":
        break
    else:
        print(c(int(s[0]),int(s[2]),s[1]))
