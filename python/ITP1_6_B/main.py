import functools

a=functools.reduce(lambda x,y:x+y,list(map(lambda x:list(map(lambda y:x+" "+str(y),range(1,14))),['S','H','C','D'])))
for i in range(0,int(input())):
    c = input()
    if c in a:a.remove(c)
print(functools.reduce(lambda x,y:x+"\n"+y,a)+"\n" if a else "",end="")
