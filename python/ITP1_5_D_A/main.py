import functools

def include3(x,i):
    return i if x%10==3 else (include3(x//10,i) if 1<=x//10 else -1)

print(' '+str(functools.reduce(lambda x,y:str(x)+" "+str(y),list(filter(lambda y:y!=-1,list(map(lambda x:x if x%3==0 else include3(x,x),list(range(1,int(input())+1)))))))))
