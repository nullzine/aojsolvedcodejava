def include3(x,i):
    if x%10==3:
        print(" "+str(i),end="")
        return
    x//=10
    if 1<=x:
        include3(x,i)

for i in range(1,int(input())+1):
    if i%3==0:
        print(" "+str(i),end="")
    else:
        include3(i,i)
print("")
