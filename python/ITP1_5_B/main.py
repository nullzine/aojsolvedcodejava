import fileinput,functools

for e in fileinput.input():
    print("" if e=="0 0\n" else functools.reduce(lambda f,g:f+g,list(map(lambda x:"#",list(range(int(e.split(" ")[1]))))))+"\n"+functools.reduce(lambda c,d:c+d,list(map(lambda z:"#"+functools.reduce(lambda a,b:a+b,z)+"#\n",list(map(lambda y:list(map(lambda x:".",list(range(int(e.split(" ")[1])-2)))),range(int(e.split(" ")[0])-2))))))+functools.reduce(lambda f,g:f+g,list(map(lambda x:"#",list(range(int(e.split(" ")[1]))))))+"\n\n",end="")
