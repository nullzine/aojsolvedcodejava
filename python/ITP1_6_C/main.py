import functools

v = [[[0 for i in range(10)] for j in range(3)] for k in range(4)]
for i in range(int(input())):
    m=list(map(lambda x:int(x),input().split(" ")))
    v[int(m[0])-1][int(m[1])-1][int(m[2])-1]+=int(m[3])

print(functools.reduce(lambda e,f:e+"####################\n"+f,list(map(lambda z:functools.reduce(lambda c,d:c+d,list(map(lambda y:y+"\n",list(map(lambda x:" "+functools.reduce(lambda a,b:str(a)+" "+str(b),x),z))))),v))),end="")


