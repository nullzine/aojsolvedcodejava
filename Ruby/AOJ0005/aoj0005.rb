def gcd(m,n)
  return 0 if m==0||n==0
  while m!=n
    if m>n then
      m=m-n;
    else
      n=n-m
    end
  end
  return m
end

def lcm(m,n)
  return 0 if m==0||n==0
  return ((m/gcd(m,n))*n)
end

$stdin.read.split(?\n).map{|m|m.split(" ")}.each{|e|puts "#{gcd(e[0].to_i,e[1].to_i)} #{lcm(e[0].to_i,e[1].to_i)}"}