class Array
  def mode
    max_by {|value| count(value)}
  end

  def mode_all
    freq = inject(Hash.new(0)) {|k, v| k[v] += 1; k}
    freq.find_all {|k, v| v == freq.values.max}.map {|key, value| key}
  end
end

$stdin.read.split(?\n).map{|m|m.to_i}.mode_all.sort.each{|e|p e}