class C

  def initialize(data)
    @data=data
    @dp = Array.new(21).map{Array.new(100,-1)}
    p c(@data[0],1)
  end

  def c(n,index)
    return 0 if n<0||20<n
    return @dp[@dp.size-1-n][index] if 0<=@dp[@dp.size-1-n][index]
    return n==@data[@data.size-1]? 1:0 if index==@data.size-1
    @dp[@dp.size-1-n][index]=c(n+@data[index],index+1)+c(n-@data[index],index+1);
    @dp[@dp.size-1-n][index]
  end

end

C.new($stdin.read.split("\n")[1].split(" ").map{|m|m.to_i})