import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/18.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                ArrayList<Integer> tmp = new ArrayList<Integer>();
                tmp.add(n);
                System.out.println(collatz(tmp).size()-1);
            }
        }
    }

    private static ArrayList<Integer> collatz(ArrayList<Integer> collatzList){
        if(collatzList.get(collatzList.size()-1)==1){
            return collatzList;
        }else if(collatzList.get(collatzList.size()-1)%2==0){
            collatzList.add(collatzList.get(collatzList.size()-1)/2);
            return collatz(collatzList);
        }else{
            collatzList.add(3*collatzList.get(collatzList.size()-1)+1);
            return collatz(collatzList);
        }
    }

}
