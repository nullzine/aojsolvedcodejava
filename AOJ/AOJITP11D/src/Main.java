import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            Watch watch = new Watch(Integer.parseInt(sc.nextLine()));
            System.out.println(watch.getHour()+":"+watch.getMinute()+":"+watch.getSecond());
        }
    }

}

class Watch{

    private int time;

    public Watch(int time){
        this.time=time;
    }

    public int getHour(){
        return time/(60*60);
    }

    public int getMinute(){
        return time/60-getHour()*60;
    }

    public int getSecond(){
        return time-getHour()*(60*60)-getMinute()*60;
    }

}