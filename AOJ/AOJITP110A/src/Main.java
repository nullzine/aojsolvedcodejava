/**
 * Created by NullZine on 14/01/22.
 */
import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(getDistance(sc.nextLine()));
        }
    }
    private static double getDistance(String str){
        String[] strs = str.split(" ");
        return Math.sqrt(Math.pow(Math.abs(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])),2)+Math.pow(Math.abs(Double.parseDouble(strs[3])-Double.parseDouble(strs[1])),2));
    }
}