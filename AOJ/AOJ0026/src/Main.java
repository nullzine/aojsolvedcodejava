import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/31
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        WhitePaper paper = new WhitePaper(10,10);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(",");
            paper.doDrop(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
        }
        System.out.println(paper.getSpace());
        System.out.println(paper.getMax());
    }
}

class WhitePaper{

    private int[][] dataSet;

    public WhitePaper(int x,int y){
        dataSet = new int[x][y];
        for(int i=0;i<dataSet.length;i++){
            for(int j=0;j<dataSet[i].length;j++){
                dataSet[i][j]=0;
            }
        }
    }

    public void doDrop(int x,int y,int dropSize){
        switch(dropSize){
            case 1:
                doSmall(x,y);
                break;
            case 2:
                doMiddle(x,y);
                break;
            case 3:
                doBig(x, y);
                break;
        }
    }

    public int getMax(){
        int max = -1;
        for(int i=0;i<dataSet.length;i++){
            for(int j=0;j<dataSet[i].length;j++){
                if(max<dataSet[i][j]){
                    max = dataSet[i][j];
                }
            }
        }
        return max;
    }

    public int getSpace(){
        int count=0;
        for(int i=0;i<dataSet.length;i++){
            for(int j=0;j<dataSet[i].length;j++){
                if(dataSet[i][j]==0){
                    count++;
                }
            }
        }
        return count;
    }

    private void doSmall(int x,int y){
        addValue(x,y);
        addValue(x-1,y);
        addValue(x+1,y);
        addValue(x,y-1);
        addValue(x,y+1);
    }

    private void doMiddle(int x,int y){
        addValue(x-1,y-1);
        addValue(x-1,y);
        addValue(x-1,y+1);
        addValue(x,y-1);
        addValue(x,y);
        addValue(x,y+1);
        addValue(x+1,y-1);
        addValue(x+1,y);
        addValue(x+1,y+1);
    }

    private void doBig(int x,int y){
        addValue(x-1,y-1);
        addValue(x-1,y);
        addValue(x-1,y+1);
        addValue(x,y-1);
        addValue(x,y);
        addValue(x,y+1);
        addValue(x+1,y-1);
        addValue(x+1,y);
        addValue(x+1,y+1);
        addValue(x-2,y);
        addValue(x+2,y);
        addValue(x,y-2);
        addValue(x,y+2);
    }

    private void addValue(int x,int y){
        try{
            dataSet[x][y]++;
        }catch(ArrayIndexOutOfBoundsException e){
        }
    }

}