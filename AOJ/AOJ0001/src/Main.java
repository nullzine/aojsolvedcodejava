import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by labuser on 2014/04/01.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> aList = new ArrayList<Integer>();
        while (sc.hasNext()){
            aList.add(Integer.parseInt(sc.nextLine()));
        }
        Collections.sort(aList,new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });
        for(int i=0;i<3;i++){
            System.out.println(aList.get(i));
        }
    }
}
