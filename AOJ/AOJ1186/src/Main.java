import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/01/30
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("0")&&str[1].equals("0")){
                break;
            }
            System.out.println(calculatiuon(new Rectangle(Integer.parseInt(str[0]),Integer.parseInt(str[1]))));
        }
    }

    private static Rectangle calculatiuon(Rectangle rect){
        int p=150,q=150;
        for(int i=1;i<=150;++i){
            for(int j=i+1;j<=150;++j){
                if(smallerp(rect,new Rectangle(i,j))&&smallerp(new Rectangle(i,j),new Rectangle(p,q))){
                    p=i;
                    q=j;
                }
            }
        }
        return new Rectangle(p,q);
    }

    private static boolean smallerp(Rectangle A,Rectangle B){
        if (0<A.compareTo(B)){
            return true;
        }else{
            return false;
        }
    }

}

class Rectangle{

    private int height;
    private int width;
    private int diagonal;

    public Rectangle(int height,int width){
        this.height=height;
        this.width=width;
        diagonal=height*height+width*width;
    }

    public int compareTo(Rectangle rect){
        if(diagonal==rect.getDiagonal()){
            if(height<rect.getHeight()){
                return 1;
            }else if(rect.getHeight()<height){
                return -1;
            }else{
                return 0;
            }
        }else if(diagonal<rect.getDiagonal()){
            return 1;
        }else if(rect.getDiagonal()<diagonal){
            return -1;
        }else{
            return -2;
        }
    }

    public int getHeight(){
        return height;
    }

    public int getWidth(){
        return width;
    }

    public int getDiagonal(){
        return diagonal;
    }

    public String toString(){
        return Integer.toString(height)+" "+Integer.toString(width);
    }

}