import java.util.Scanner;

/**
 * Created by labuser on 14/04/11.
 */
public class Main {
    public static void main(String[] args){
        String strs[] = new Scanner(System.in).nextLine().split(" ");
        int a = Integer.parseInt(strs[0]);
        int b = Integer.parseInt(strs[1]);
        System.out.printf("%d %d %f",a/b,a%b,(double)a/b);
    }
}
