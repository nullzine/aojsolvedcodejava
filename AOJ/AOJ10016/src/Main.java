import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/20
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("-1")&&str[1].equals("-1")&&str[2].equals("-1")){
                break;
            }else{
                DataSet ds = new DataSet(Integer.parseInt(str[0]),Integer.parseInt(str[1]),Integer.parseInt(str[2]));
                System.out.println(ds.getRank());
            }
        }
    }
}

class DataSet{

    String rank;

    public DataSet(int m,int f,int r){
        rank=validScore(m,f,r);
    }

    private String validScore(int m,int f,int r){
        if(m==-1||f==-1){
            return "F";
        }else if(80<=m+f){
            return "A";
        }else if(65<=m+f&&m+f<80){
            return "B";
        }else if(50<=m+f&&m+f<65){
            return "C";
        }else if(30<=m+f&&m+f<50){
            if(50<=r){
                return "C";
            }else{
                return "D";
            }
        }else if(m+f<30){
            return "F";
        }else{
            return "!";
        }
    }

    public String getRank(){
        return rank;
    }

}