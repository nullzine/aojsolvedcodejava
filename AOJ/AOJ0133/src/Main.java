import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/26.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<String> aList = new ArrayList<String>();
        while(sc.hasNext()){
            aList.add(sc.nextLine());
        }
        System.out.print(new Execution(aList).getResult());
    }
}

class Execution{

    private String dataSet;

    public Execution(ArrayList<String> aList){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<aList.size();i++){
            sb.append(aList.get(i)+"\n");
        }
        dataSet = sb.toString();
    }

    public String getResult(){
        return "90\n"+toRotateString(dataSet)+"180\n"+toRotateString(toRotateString(dataSet))+"270\n"+toRotateString(toRotateString(toRotateString(dataSet)));
    }

    public String toString(){
        return dataSet.toString();
    }

    public String toRotateString(String str){
        String[] strs = str.split("\n");
        char[][] tmp = new char[strs.length][];
        for(int i=0;i<tmp.length;i++){
            tmp[i] = strs[i].toCharArray();
        }
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<tmp[0].length;i++){
            for(int j=0;j<tmp.length;j++){
                sb.append(tmp[tmp.length-1-j][i]);
            }
            sb.append("\n");
        }
        return sb.toString();
    }


}