import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by nullzine on 2015/04/03.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int[] data = new int[Integer.parseInt(sc.nextLine())];
        for(int i=0;i<data.length;i++){
            data[i] = Integer.parseInt(sc.nextLine());
        }
        System.out.println(new Execution2(data).getResult());
    }
}

class Execution2{

    private int n;
    private BigInteger[] bigIntegers;
    private BigInteger result;

    public Execution2(int[] data){
        n = data.length;
        bigIntegers = new BigInteger[100010];
        for(int i=1;i<=data.length;i++){
            bigIntegers[i] = new BigInteger(Integer.toString(data[i-1]));
        }
        result = calculation();
    }

    private BigInteger calculation(){
        int p=1,q=2,r=3;
        BigInteger x = new BigInteger(bigIntegers[1].toString());
        BigInteger y = new BigInteger(bigIntegers[2].toString());
        BigInteger z = BigInteger.ZERO;
        for(int i=3;i<=n;i++){
            z=z.add(bigIntegers[i]);
        }
        BigInteger ret = BigInteger.ZERO;
        while(r<=n){
            if(y.compareTo(z.min(x))==-1){
                y=y.add(bigIntegers[r]);
                z=z.subtract(bigIntegers[r]);
                r++;
            }else if(x.compareTo(y.min(z))==-1){
                x=x.add(bigIntegers[q]);
                y=y.subtract(bigIntegers[q]);
                q++;
            }else{
                z=z.add(bigIntegers[p]);
                y=y.subtract(bigIntegers[p]);
                p++;
            }
            ret = ret.max(z.min(x.min(y)));
        }
        return ret;
    }

    public BigInteger getResult(){
        return result;
    }

}

class Execution{

    private int[] data;
    private int result;

    public Execution(int[] data){
        this.data=data;
        result=calculation();
    }

    private int calculation(){
        int keyNum = (int)(sum(data)/3.0);
        while(1<=keyNum) {
            for (int i = 0; i < data.length - 2; i++) {
                for (int j = i + 1; j < data.length - 1; j++) {
                    for (int k = j + 1; k < data.length; k++) {
                        System.out.println(i+" "+j+" "+k);
                        if(Math.min(Math.min(rangeSum(0, i), rangeSum(i, j)), rangeSum(j, k))==keyNum){
                            return keyNum;
                        }
                    }
                }
            }
        }
        return -1;
    }

    /*
    private int calculation(){
        int keyNum = (int)(sum(data)/3.0);
        for(int i=keyNum;0<i;i--){
            System.out.println(i);
            if(validCombination(i)){
                return i;
            }
        }
        return -1;
    }

    private boolean validCombination(int n){
        for(int i=2;i<data.length-3;i++){
            for(int j=0;j<data.length-i;j++){
                int sum=0;
                for(int k=j;k<j+i;k++){
                    sum+=data[k];
                }
                System.out.println("s:"+sum+" length:"+i);
                if(sum==n){
                    return true;
                }
            }
        }
        return false;
    }
    */

    private int rangeSum(int start,int end){
        int sum=0;
        for(int i=start;i<=end;i++){
            sum+=data[i];
        }
        return sum;
    }

    private int sum(int[] d){
        int sum=0;
        for(int n:d){
            sum+=n;
        }
        return sum;
    }

    public int getResult(){
        return result;
    }

}

