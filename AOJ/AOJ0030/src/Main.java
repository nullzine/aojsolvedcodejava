//import java.util.HashMap;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int s = Integer.parseInt(strs[1]);
            if(n==0&&s==0)
                break;
            System.out.println(calculation(n,s));
        }
    }

    private static int calculation(int n,int s){
        int c=0;
        for(int i=0;i<(int)Math.pow(2,10);i++)
            if(countBit(complementBin(Integer.toBinaryString(i)))==n&&getSum(complementBin(Integer.toBinaryString(i)))==s)
                c++;
        return c;
    }

    private static String complementBin(String str){
        for(int i=str.length();i<10;i++)
            str="0"+str;
        return str;
    }

    private static int countBit(String str){
        int c=0;
        for(int i=0;i<str.length();i++)
            if(str.charAt(i)=='1')
                c++;
        return c;
    }

    private static int getSum(String str){
        int s=0;
        for(int i=0;i<str.length();i++)
            if(str.charAt(i)=='1')
                s+=i;
        return s;
    }

}

/*
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int s = Integer.parseInt(strs[1]);
            if(n==0&&s==0) {
                break;
            }
            System.out.println(new Execution(binaryPool,n).getPattern(s));
        }
    }

}
*/

/*

class Execution{

    private ArrayList<String> binaryData;
    private HashMap<Integer,ArrayList<String>> result;

    public Execution(BinaryPool binaryPool,int n){
        binaryData=binaryPool.getFilteredData(n);
        result=new HashMap<Integer, ArrayList<String>>();
        analysis();
    }

    private void analysis(){
        for(String s:binaryData){
            if(result.containsKey(listSum(decoder(s)))){
                result.get(listSum(decoder(s))).add(makeFormula(decoder(s)));
            }else{
                ArrayList<String> tmp = new ArrayList<String>();
                tmp.add(s);
                result.put(listSum(decoder(s)),tmp);
            }
        }
    }

    private String makeFormula(ArrayList<Integer> aList){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<aList.size();i++){
            if(i==aList.size()-1){
                sb.append(Integer.toString(aList.get(i)));
            }else{
                sb.append(Integer.toString(aList.get(i))+"+");
            }
        }
        return sb.toString();
    }

    private ArrayList<Integer> decoder(String str){
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='1'){
                tmp.add(i);
            }
        }
        return tmp;
    }

    private int listSum(ArrayList<Integer> aList){
        int s=0;
        for(int n:aList){
            s+=n;
        }
        return s;
    }

    public int getPattern(int sum){
        int s;
        try{
            s=result.get(sum).size();
        }catch (NullPointerException e){
            s=0;
        }
        return s;
    }


}

*/

/*

class BinaryPool{

    private int target;
    private String[] binaryData;

    public BinaryPool(int target){
        this.target=target;
        binaryData = new String[(int)Math.pow(2,target)];
        calculation();
    }

    private void calculation(){
        for(int i=0;i<binaryData.length;i++){
            binaryData[i] = complementBin(Integer.toBinaryString(i));
        }
    }

    private String complementBin(String str){
        for(int i=str.length();i<target;i++){
            str="0"+str;
        }
        return str;
    }

    public String[] getData(){
        return binaryData;
    }

    public ArrayList<String> getFilteredData(int t) {
        ArrayList<String> tmp = new ArrayList<String>();
        for (int i = 0; i < binaryData.length; i++) {
            if (countBit(binaryData[i]) == t) {
                tmp.add(binaryData[i]);
            }
        }
        return tmp;
    }

    private int countBit(String str){
        int c=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='1'){
                c++;
            }
        }
        return c;
    }

    public void print(){
        for(String s:binaryData){
            System.out.println(s);
        }
    }

}
*/