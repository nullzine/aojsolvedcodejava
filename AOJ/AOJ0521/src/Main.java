import java.util.Scanner;

/**
 * Created by labuser on 14/05/26.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                System.out.println(new Execution(n).getCount());
            }
        }
    }
}

class Execution{

    private int change;
    private int[] coin = {500,100,50,10,5,1};
    private int pointer;
    private int count;

    public Execution(int pay){
        change=1000-pay;
        count=0;
        pointer=0;
        analysis(coin[pointer]);
    }

    private void analysis(int value){
        count+=change/value;
        change=change%value;
        if(value==1){
            return;
        }else {
            analysis(coin[pointer++]);
        }
    }

    public int getCount(){
        return count;
    }


}