import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    private static int[] dataSet;

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int m = Integer.parseInt(strs[1]);
            if(n==0&&m==0){
                break;
            }
            dataSet = new int[n];
            for(int i=0;i<dataSet.length;i++){
                dataSet[i] = Integer.parseInt(sc.nextLine());
            }
            int max = getSum(0,m);
            for(int i=1;i<dataSet.length-m;i++){
                if(max<getSum(i,m)){
                    max=getSum(i,m);
                }
            }
            System.out.println(max);
        }
    }

    private static int getSum(int pointer,int bandWidth){
        int sum = 0;
        for(int i=pointer;i<pointer+bandWidth;i++){
            sum+=dataSet[i];
        }
        return sum;
    }

}