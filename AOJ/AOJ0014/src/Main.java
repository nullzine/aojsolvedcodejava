import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int tmp,limit=600;
        while(sc.hasNext()){
            int s=0;
            int d=Integer.parseInt(sc.nextLine());
            tmp=limit/d;
            for(int i=0;i<tmp;i++){
                int x=d;
                int y=x*x*i*i;
                s=s+x*y;
            }
            System.out.println(s);
        }
    }
}
