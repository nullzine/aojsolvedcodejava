import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Person taro = new Person("Taro");
        Person hanako = new Person("Hanako");
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            String[] str = sc.nextLine().split(" ");
            if(str[0].compareTo(str[1])<0){
                hanako.add(3);
            }else if(0<str[0].compareTo(str[1])){
                taro.add(3);
            }else if(str[0].compareTo(str[1])==0){
                hanako.add(1);
                taro.add(1);
            }
        }
        System.out.println(taro.getScore()+" "+hanako.getScore());
    }
}

class Person{

    private String name;
    private int score;

    public Person(String name){
        this.name=name;
        score=0;
    }

    public void add(int num){
        score+=num;
    }

    public int getScore(){
        return score;
    }

}