import java.util.Scanner;

/**
 * Created by labuser on 14/05/26.
 */
public class Main {
    public static void main(String[] args){
        //new Test();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine()));
        }
    }
}

/*
class Test{

    public Test(){
        analysis();
    }

    private void analysis(){
        for(int sh=7;sh<23;sh++){
            for(int sm=0;sm<60;sm++){
                for(int ss=0;ss<60;ss++){
                    for(int eh=7;eh<23;eh++){
                        for(int em=0;em<60;em++){
                            for(int es=0;es<60;es++){
                                String str = sh+" "+sm+" "+ss+" "+eh+" "+em+" "+es;
                                //System.out.println(new Execution(str).toString().equals(new Execution2(str).toString())+"{"+new Execution(str)+":"+new Execution2(str)+"}");
                                Execution exe = new Execution(str);
                                Execution2 exe2 = new Execution2(str);
                                if((!exe.toString().equals(exe2.toString()))&&!exe.getDiffError()){
                                    System.out.println(new Execution(str).toString().equals(new Execution2(str).toString())+"{"+new Execution(str)+":"+new Execution2(str)+"}");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

class Execution2{

    private int h,m,s,fah,sah,fas,sas,fam,sam;

    public Execution2(String str){
        String[] strs = str.split(" ");
        sah=Integer.parseInt(strs[0]);
        sam=Integer.parseInt(strs[1]);
        sas=Integer.parseInt(strs[2]);
        fah=Integer.parseInt(strs[3]);
        fam=Integer.parseInt(strs[4]);
        fas=Integer.parseInt(strs[5]);
        calculation();
    }

    private void calculation(){
        h=fah-sah;
        m=fam-sam;
        s=fas-sas;
        if(s<0){
            s=s+60;
            m=m-1;
        }
        if(m<0){
            m=m+60;
            h=h-1;
        }
    }

    public String toString(){
        return h+" "+m+" "+s;
    }


}
*/


class Execution{

    private EmployeeTime start;
    private EmployeeTime end;
    private EmployeeTime diff;

    public Execution(String str){
        makeData(str.split(" "));
    }

    private void makeData(String[] strs){
        start = new EmployeeTime(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
        end = new EmployeeTime(Integer.parseInt(strs[3]),Integer.parseInt(strs[4]),Integer.parseInt(strs[5]));
        diff = new EmployeeTime(end.getAllSeconds()-start.getAllSeconds());
    }

    public String toString(){
        return diff.toString();
    }

    public boolean getDiffError(){
        return diff.getErroeFlag();
    }


}

class EmployeeTime{

    private int hour;
    private int minute;
    private int second;
    private int allSeconds;
    private boolean errorFlag;

    public EmployeeTime(int hour,int minute,int second){
        this.hour=hour;
        this.minute=minute;
        this.second=second;
        allSeconds=hour*60*60+minute*60+second;
        errorFlag=!(0<=allSeconds);
    }

    public EmployeeTime(int sec){
        this.hour=sec/3600;
        sec=sec%3600;
        this.minute=sec/60;
        sec=sec%60;
        this.second=sec;
        allSeconds=hour*60*60+minute*60+second;
        errorFlag=!(0<=allSeconds);
    }

    public int getAllSeconds(){
        return allSeconds;
    }

    public String toString(){
        return hour+" "+minute+" "+second;
    }

    private String comp(int num){
        if(num<10){
            return "0"+num;
        }else{
            return Integer.toString(num);
        }
    }

    public boolean getErroeFlag(){
        return errorFlag;
    }

}