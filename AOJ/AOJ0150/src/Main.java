import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Prime prime = new Prime(10000);
        Scanner sc = new Scanner(System.in);
        while(true){
            int num = Integer.parseInt(sc.nextLine());
            if(num==0){
                break;
            }
            int result = prime.getTwinPrime(num);
            System.out.println((result-2)+" "+(result));
        }
    }
}

class Prime{

    private ArrayList<Integer> aList;
    private int limit;

    public Prime(int limit){
        this.limit=limit;
        aList = new ArrayList<Integer>();
        calculation();
    }

    private void calculation(){
        int count;
        for(int i=1;i<=limit;i++){
            count=0;
            for(int j=1;j<=i;j++){
                if(i%j==0){
                    count++;
                }
                if(count==3){
                    break;
                }
            }
            if(count==2){
                aList.add(i);
            }
        }
    }

    public int getTwinPrime(int num){
        int result = -1;
        for(int i=1;i<aList.size();i++){
            if(num<aList.get(i)){
                break;
            }
            if(aList.get(i)-aList.get(i-1)==2){
                result = aList.get(i);
            }
        }
        return result;
    }


}