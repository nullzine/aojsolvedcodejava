import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/26.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String target = sc.nextLine();
        int n = Integer.parseInt(sc.nextLine());
        int count = 0;
        for (int i = 0; i < n; i++) {
            Ring ring = new Ring(sc.nextLine());
            if (ring.contains(target)) {
                count++;
            }
        }
        System.out.println(count);
    }
}

class Ring {

    private String dataSet;
    private RingString ringString;

    public Ring(String dataSet) {
        this.dataSet=dataSet;
    }

    public boolean contains(String target) {
        for(String s:new RingString(dataSet,target.length()).getDataSet()){
            if(s.equals(target)){
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return dataSet;
    }

}

class RingString{

    private String data;
    private int n;
    private ArrayList<String> dataSet;

    public RingString(String data,int n){
        this.data=data;
        this.n=n;
        dataSet = new ArrayList<String>();
        calculation();
    }

    private void calculation(){
        for(int i=0;i<data.length();i++){
            dataSet.add((data+data).substring(i,i+n));
        }
    }

    public ArrayList<String> getDataSet(){
        return dataSet;
    }

}