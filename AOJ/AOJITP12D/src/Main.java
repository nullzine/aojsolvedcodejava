import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/04.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine().split(" ")));
        }
    }
}

class Execution{

    private Rect rect;
    private boolean result;

    public Execution(String[] strs){
        rect = new Rect(new Point(0,0),new Point(Integer.parseInt(strs[0]),Integer.parseInt(strs[1])));
        result = validCircle(new Circle(new Point(Integer.parseInt(strs[2]),Integer.parseInt(strs[3])),Integer.parseInt(strs[4])));
    }

    private boolean validCircle(Circle circle){
        return rect.validInclude(new Point(circle.getCenter().getX()+circle.getR(),circle.getCenter().getY()))&&
                rect.validInclude(new Point(circle.getCenter().getX()-circle.getR(),circle.getCenter().getY()))&&
                rect.validInclude(new Point(circle.getCenter().getX(),circle.getCenter().getY()+circle.getR()))&&
                rect.validInclude(new Point(circle.getCenter().getX(),circle.getCenter().getY()+circle.getR()));
    }

    public String toString(){
        if(result){
            return "Yes";
        }else{
            return "No";
        }
    }


}

class Circle{

    private Point center;
    private int r;

    public Circle(Point center,int r){
        this.center=center;
        this.r=r;
    }

    public Point getCenter(){
        return center;
    }

    public int getR(){
        return r;
    }


}

class Rect{

    private Point a;
    private Point b;

    public Rect(Point a,Point b){
        this.a=a;
        this.b=b;
    }

    public boolean validInclude(Point target){
        return (a.getX()<=target.getX()&&target.getX()<=b.getX())&&(a.getY()<=target.getY()&&target.getY()<=b.getY());
    }

}

class Point{

    private int x;
    private int y;

    public Point(int x,int y){
        this.x=x;
        this.y=y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }


}