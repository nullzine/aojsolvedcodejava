import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/02/06
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            Dice dice = new Dice(1,2,3);
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            for(int i=0;i<n;i++){
                String str = sc.nextLine();
                if(str.equals("North")){
                    dice.doNorth();
                }
                if(str.equals("East")){
                    dice.doEast();
                }
                if(str.equals("South")){
                    dice.doSouth();
                }
                if(str.equals("West")){
                    dice.doWest();
                }
                if(str.equals("Right")){
                    dice.doRight();
                }
                if(str.equals("Left")){
                    dice.doLeft();
                }
            }
            System.out.println(dice.getSum());
        }
    }
}

class Dice{

    private int topNumber;
    private int northNumber;
    private int southNumber;
    private int eastNumber;
    private int westNumber;
    private int bottomNumber;
    private int sum;

    public Dice(int topNumber,int southNumber,int eastNumber){
        this.topNumber=topNumber;
        this.southNumber=southNumber;
        this.eastNumber=eastNumber;
        bottomNumber=7-topNumber;
        northNumber=7-southNumber;
        westNumber=7-eastNumber;
        sum=topNumber;
    }

    public void doNorth(){
        int[] tmp = new int[4];
        tmp[0]=topNumber;
        tmp[1]=southNumber;
        tmp[2]=bottomNumber;
        tmp[3]=northNumber;
        topNumber=tmp[1];
        southNumber=tmp[2];
        bottomNumber=tmp[3];
        northNumber=tmp[0];
        sum+=topNumber;
    }

    public void doEast(){
        int[] tmp = new int[4];
        tmp[0]=topNumber;
        tmp[1]=eastNumber;
        tmp[2]=bottomNumber;
        tmp[3]=westNumber;
        eastNumber=tmp[0];
        bottomNumber=tmp[1];
        westNumber=tmp[2];
        topNumber=tmp[3];
        sum+=topNumber;
    }

    public void doWest(){
        int[] tmp = new int[4];
        tmp[0]=topNumber;
        tmp[1]=eastNumber;
        tmp[2]=bottomNumber;
        tmp[3]=westNumber;
        westNumber=tmp[0];
        topNumber=tmp[1];
        eastNumber=tmp[2];
        bottomNumber=tmp[3];
        sum+=topNumber;
    }

    public void doSouth(){
        int[] tmp = new int[4];
        tmp[0]=topNumber;
        tmp[1]=southNumber;
        tmp[2]=bottomNumber;
        tmp[3]=northNumber;
        southNumber=tmp[0];
        bottomNumber=tmp[1];
        northNumber=tmp[2];
        topNumber=tmp[3];
        sum+=topNumber;
    }

    public void doRight(){
        int[] tmp = new int[4];
        tmp[0]=eastNumber;
        tmp[1]=southNumber;
        tmp[2]=westNumber;
        tmp[3]=northNumber;
        southNumber=tmp[0];
        westNumber=tmp[1];
        northNumber=tmp[2];
        eastNumber=tmp[3];
        sum+=topNumber;
    }

    public void doLeft(){
        int[] tmp = new int[4];
        tmp[0]=eastNumber;
        tmp[1]=southNumber;
        tmp[2]=westNumber;
        tmp[3]=northNumber;
        northNumber=tmp[0];
        eastNumber=tmp[1];
        southNumber=tmp[2];
        westNumber=tmp[3];
        sum+=topNumber;
    }

    public int getSum(){
        return sum;
    }

}

/*
5
North
North
East
South
West
*/

/*
8
West
North
Left
South
Right
North
Left
East
*/