import java.util.Scanner;

/**
 * Created by NullZine on 14/01/15.
 */
public class Main {
    public static void main(String[] args){
        Univ univ = new Univ(4,3,10);
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            String[] str = sc.nextLine().split(" ");
            univ.add(Integer.parseInt(str[0]),Integer.parseInt(str[1]),Integer.parseInt(str[2]),Integer.parseInt(str[3]));
        }
        System.out.println(univ.toString());
    }
}

class Univ{

    private Stracture[] stracture;

    public Univ(int stracture,int floor,int room){
        this.stracture = new Stracture[stracture];
        for(int i=0;i<this.stracture.length;i++){
            this.stracture[i] = new Stracture(floor,room);
        }
    }

    public void add(int stracture,int floor,int room,int number){
        this.stracture[stracture-1].add(floor,room,number);
    }

    public String toString(){
        String str = "";
        for(int i=0;i<stracture.length-1;i++){
            str=str+stracture[i].toString()+getCRLF();
            str=str+getSeparator()+getCRLF();
        }
        str=str+stracture[stracture.length-1].toString();
        return str;
    }

    private String getSeparator(){
        String str = "";
        for(int i=0;i<20;i++){
            str=str+"#";
        }
        return str;
    }

    private String getCRLF(){
        String crlf = "\n";
        try {
            crlf = System.getProperty("line.separator");
        } catch(SecurityException e) {
        }
        return crlf;
    }

}

class Stracture{

    private Floor[] floor;

    public Stracture(int floor,int room){
        this.floor = new Floor[floor];
        for(int i=0;i<this.floor.length;i++){
            this.floor[i] = new Floor(room);
        }
    }

    public void add(int floor,int room,int number){
        this.floor[floor-1].add(room,number);
    }

    public String toString(){
        String str = floor[0].toString();
        for(int i=1;i<floor.length;i++){
            str = str +getCRLF()+ floor[i].toString();
        }
        return str;
    }

    private String getCRLF(){
        String crlf = "\n";
        try {
            crlf = System.getProperty("line.separator");
        } catch(SecurityException e) {
        }
        return crlf;
    }

}

class Floor{

    private Room[] room;

    public Floor(int room){
        this.room = new Room[room];
        for(int i=0;i<this.room.length;i++){
            this.room[i] = new Room();
        }
    }

    public void add(int room,int number){
        this.room[room-1].add(number);
    }

    public String toString(){
        String str = "";
        for(int i=0;i<room.length;i++){
            str = str+" "+Integer.toString(room[i].getNum());
        }
        return str;
    }

}

class Room{

    private int num;

    public Room(){
        num = 0;
    }

    public void add(int number){
        num = num + number;
    }

    public int getNum(){
        return num;
    }

}