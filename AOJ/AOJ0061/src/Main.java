import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        HashMap<Integer,Integer> hMap = new HashMap<Integer,Integer>();
        while(true){
            String[] strs = sc.nextLine().split(",");
            if(Integer.parseInt(strs[0])==0&&Integer.parseInt(strs[1])==0){
                break;
            }
            hMap.put(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]));
        }
        Execution exe = new Execution(hMap);
        while(sc.hasNext()){
            System.out.println(exe.getRank(Integer.parseInt(sc.nextLine())));
        }
    }
}

class Execution{

    private Team[] teamData;

    public Execution(HashMap<Integer,Integer> hMap){
        teamData = new Team[hMap.size()];
        int count = 0;
        for(Map.Entry<Integer,Integer> e : hMap.entrySet()) {
            //System.out.println(e.getKey() + " : " + e.getValue());
            teamData[count] = new Team(e.getKey(),e.getValue());
            count++;
        }
        calcRank();
    }

    private void calcRank(){
        Arrays.sort(teamData, new RankComp());
        int rank = 1;
        teamData[0].rank=rank;
        for(int i=1;i<teamData.length;i++){
            if(teamData[i].score==teamData[i-1].score){
                teamData[i].rank=rank;
            }else{
                rank++;
                teamData[i].rank=rank;
            }
        }
    }

    public int getRank(int number){
        for(int i=0;i<teamData.length;i++){
            if(teamData[i].id==number){
                return teamData[i].rank;
            }
        }
        return -1;
    }

}

class RankComp implements Comparator<Team>{
    @Override
    public int compare(Team o1, Team o2) {
        //return ((Team)o1).getScore() - ((Team)o2).getScore();  this is ascending-order
        return (o2.getScore() - o1.getScore());
    }
}

/*
class RankComp implements Comparator{
        @Override
        public int compare(Object o1, Object o2) {
            //return ((Team)o1).getScore() - ((Team)o2).getScore();  this is ascending-order
            return ((Team)o2).getScore() - ((Team)o1).getScore();
        }
}
*/

class Team{

    public int id;
    public int score;
    public int rank;

    public Team(int id,int score){
        this.id=id;
        this.score=score;
    }

    public int getScore(){
        return score;
    }

}