import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(100000);
        while(true){
            int input = sc.nextInt();
            if(input==0){
                break;
            }
            exe.setData(input);
            System.out.println(exe.getData());
        }
    }

}

class Execution{

    private SimplePrime sp;
    private HashMap<Integer,Integer> hMap;

    public Execution(int limit){
        sp = new SimplePrime(limit);
    }

    public void setData(int input){
        hMap = new HashMap<Integer,Integer>();
        int count = 0;
        for(int i=0;sp.primeList.get(i)<input;i++){
            if(sp.boolList[input-sp.primeList.get(i)]&&hMap.get(input-sp.primeList.get(i))==null){
                hMap.put(sp.primeList.get(i),input-sp.primeList.get(i));
            }
        }
    }

    public int getData(){
        return hMap.size();
    }

}

class SimplePrime {

    public boolean[] boolList;
    public ArrayList<Integer> primeList = new ArrayList<Integer>();

    public SimplePrime(int limit){
        limit++;
        boolList = new boolean[limit];
        makeBool(limit);
        //showBool();
    }

    public ArrayList getList(){
        return primeList;
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void showBool(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(i+":"+boolList[i]);
        }
    }

    private void makeBool(int limit) {

        Arrays.fill(boolList, true);
        boolList[0] = false;
        boolList[1] = false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}