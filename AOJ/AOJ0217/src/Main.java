import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/06/20.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = sc.nextInt();
            if(n==0){
                break;
            }else{
                int maxId = sc.nextInt();
                int maxLength = sc.nextInt()+sc.nextInt();
                for(int i=1;i<n;i++){
                    int id = sc.nextInt();
                    int lengthA = sc.nextInt();
                    int lengthB = sc.nextInt();
                    if(maxLength<lengthA+lengthB){
                        maxId = id;
                        maxLength = lengthA+lengthB;
                    }
                }
                System.out.println(maxId+" "+maxLength);
                /*
                Execution exe = new Execution();
                for(int i=0;i<n;i++){
                    String[] data = sc.nextLine().split(" ");
                    exe.add(new Person(Integer.parseInt(data[0]),Integer.parseInt(data[1]),Integer.parseInt(data[2])));
                }
                System.out.println(exe.getMax());
                */
            }
        }
    }
}
/*
class Execution{

    private ArrayList<Person> persons;

    public Execution(){
        persons = new ArrayList<Person>();
    }

    public void add(Person person){
        persons.add(person);
    }

    public Person getMax(){
        Person max = persons.get(0);
        for(int i=1;i<persons.size();i++){
            if(max.getSum()<persons.get(i).getSum()){
                max=persons.get(i);
            }
        }
        return max;
    }

    public void print(){
        for(Person p:persons){
            System.out.println("{"+p+"}");
        }
    }


}
*/
/*
class Person{

    private int p;
    private int sum;

    public Person(int p,int d1,int d2){
        this.p=p;
        sum=d1+d2;
    }

    public int getSum(){
        return sum;
    }

    public String toString(){
        return p+" "+sum;
    }

}
*/