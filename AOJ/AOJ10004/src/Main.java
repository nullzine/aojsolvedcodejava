import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strs = str.split(" ");
        int[] dataSet = new int[strs.length];
        for(int i=0;i<dataSet.length;i++){
            dataSet[i]=Integer.parseInt(strs[i]);
        }
        Arrays.sort(dataSet);
        for(int i=0;i<dataSet.length;i++){
            if(i==dataSet.length-1){
                System.out.println(dataSet[i]);
            }else{
                System.out.print(dataSet[i]+" ");
            }
        }
    }
}