import java.util.HashSet;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/15
 * Time: 7:46
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        String target = "the,this,that";
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine(),target.split(",")).getResult());
        }
    }
}

class Execution{

    private String str;
    private ShiftingString ss;
    private HashSet<String> target;
    private String result;

    public Execution(String str,String[] target){
        this.str=str;
        ss = new ShiftingString(str);
        this.target = new HashSet<String>();
        setTarget(target);
        analysis();
    }

    private void setTarget(String[] t){
        for(int i=0;i<t.length;i++){
            this.target.add(t[i]);
        }
    }

    private void analysis(){
        for(int i=0;i<26;i++){
            String tmp = ss.getShiftedString(i);
            if(validString(tmp)){
                result=tmp;
                break;
            }
        }
    }

    private boolean validString(String s){
        if(s.equals("ERROR")){
            return true;
        }
        String[] strs = s.split(" ");
        for(int i=0;i<strs.length;i++){
            if(target.contains(strs[i])){
                return true;
            }
        }
        return false;
    }

    public String getResult(){
        return result;
    }

}

class ShiftingString{

    private String str;
    private ShiftingData sd;

    public ShiftingString(String str){
        this.str=str;
        sd = new ShiftingData("abcdefghijklmnopqrstuvwxyz");
    }

    public String getShiftedString(int shift){
        String[] strs = str.split(" ");
        StringBuilder sb = new StringBuilder();
        sb.append(convWord(strs[0],shift));
        for(int i=1;i<strs.length;i++){
            String tmp = convWord(strs[i],shift);
            if(tmp.equals("ERROR")){
                return "ERROR";
            }
            sb.append(" "+tmp);
        }
        return sb.toString();
    }

    private String convWord(String str,int shift){
        char[] strc = str.toCharArray();
        for(int i=0;i<strc.length;i++){
            if(strc[i]!='.'){
                String tmp = sd.getWord(strc[i],shift);
                if(tmp.equals("ERROR")){
                    return tmp;
                }
                strc[i]=tmp.toCharArray()[0];
            }
        }
        return String.valueOf(strc);
    }

}

class ShiftingData{

    private char[] data;
    private String dict;

    public ShiftingData(){
        dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        data = dict.toCharArray();
    }

    public ShiftingData(String shiftingData){
        dict = shiftingData;
        data = dict.toCharArray();
    }

    public String getWord(char seed,int shift){
        if(dict.indexOf(seed+"")==-1){
            return "ERROR";
        }
        int i;
        if(0<=shift){
            for(i=0;i<data.length;i++){
                if(data[i]==seed){
                    break;
                }
            }
            i-=shift;
            if(i<0){
                i=26-Math.abs(i)%26;
            }
            return data[i]+"";
        }else{
            for(i=0;i<data.length;i++){
                if(data[i]==seed){
                    break;
                }
            }
            i+=shift;
            if(i<0){
                i=26-Math.abs(i)%26;
            }
            return data[i]+"";
        }
    }

}