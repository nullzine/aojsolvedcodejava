import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/16
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String one = sc.nextLine();
            if(one.equals("0")){
                break;
            }
            String two = sc.nextLine();
            if(two.equals("0")||errorCheck(one,two)){
                break;
            }
            String[] strs = two.split(" ");
            int[] data = new int[strs.length];
            for(int i=0;i<data.length;i++){
                data[i] = Integer.parseInt(strs[i]);
            }
            System.out.println(new NumArrayStracture(data).getResult());
        }
    }

    private static boolean errorCheck(String one,String two){
        return two.split(" ").length!=Integer.parseInt(one);
    }

}

class NumArrayStracture{

    private int[] data;
    private int[] answer;
    private int phase;
    private String result;

    public NumArrayStracture(int[] data){
        this.data=data;
        answer = new int[data.length];
        phase=0;
        result=calculation();
    }

    public NumArrayStracture(NumArrayStracture nas){
        this.data=nas.answer;
        answer = new int[data.length];
        phase=nas.phase+1;
    }

    public String calculation(){
        for(int i=0;i<data.length;i++){
            answer[i] = countNum(data[i]);
        }
        if(validArray(answer)){
            return this.toString();
        }else{
            //debugArray(answer);
            //System.out.println(phase);
            return new NumArrayStracture(this).calculation();
        }
    }

    private void debugArray(int[] ar){
        for(int n:ar){
            System.out.print(n+" ");
        }
        System.out.println();
    }

    private int countNum(int num){
        int count=0;
        for(int i=0;i<data.length;i++){
            if(data[i]==num){
                count++;
            }
        }
        return count;
    }

    private boolean validArray(int[] array){
        for(int i=1;i<array.length;i++){
            if(array[i]!=data[i]){
                return false;
            }
        }
        return true;
    }

    public String toString(){
        return Integer.toString(phase)+"\n"+getAnswerStr();
    }

    private String getAnswerStr(){
        String str = Integer.toString(answer[0]);
        for(int i=1;i<answer.length;i++){
            str=str+" "+Integer.toString(answer[i]);
        }
        return str;
    }

    public String getResult(){
        return result;
    }

}