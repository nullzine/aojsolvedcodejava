import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            int[] data = new int[strs.length];
            for(int i=0;i<data.length;i++){
                data[i] = Integer.parseInt(strs[i]);
            }
            Arrays.sort(data);
            System.out.print(data[data.length-1]);
            for(int i=1;i<data.length;i++){
                System.out.print(" "+data[data.length-1-i]);
            }
            System.out.println();
        }
    }
}
