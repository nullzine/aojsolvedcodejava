import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/23.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            if(i!=0){
                System.out.println();
            }
            String[][] data = new String[9][];
            for(int j=0;j<9;j++){
                data[j] = sc.nextLine().split(" ");
            }
            new Calculation(data).print();
        }
    }
}

class Calculation{

    private SudokuElement[][] sudokuElements;

    public Calculation(String[][] data){
        init(data);
        calculation();
    }

    private void calculation(){
        for(int i=0;i<sudokuElements.length;i++){
            rowCheck(i);
            columnCheck(i);
        }
        domainCheck();
    }

    private void rowCheck(int row){
        SudokuElement[] sudokuRow = new SudokuElement[sudokuElements.length];
        for(int i=0;i<sudokuRow.length;i++){
            sudokuRow[i]=sudokuElements[i][row];
        }
        checkElementArray(sudokuRow,0);
    }

    private void columnCheck(int column){
        SudokuElement[] sudokuColumn = sudokuElements[column];
        checkElementArray(sudokuColumn,1);
    }

    private void domainCheck(){
        for(int i=1;i<=7;i+=3){
            for(int j=1;j<=7;j+=3){
                SudokuElement[] sudokuDomain = new SudokuElement[sudokuElements.length];
                sudokuDomain[0] = sudokuElements[i-1][j-1];
                sudokuDomain[1] = sudokuElements[i-1][j];
                sudokuDomain[2] = sudokuElements[i-1][j+1];
                sudokuDomain[3] = sudokuElements[i][j-1];
                sudokuDomain[4] = sudokuElements[i][j];
                sudokuDomain[5] = sudokuElements[i][j+1];
                sudokuDomain[6] = sudokuElements[i+1][j-1];
                sudokuDomain[7] = sudokuElements[i+1][j];
                sudokuDomain[8] = sudokuElements[i+1][j+1];
                checkElementArray(sudokuDomain,2);
            }
        }
    }

    private void init(String[][] data){
        sudokuElements = new SudokuElement[data.length][data[0].length];
        for(int i=0;i<sudokuElements.length;i++){
            for(int j=0;j<sudokuElements[i].length;j++){
                sudokuElements[i][j] = new SudokuElement(i,j,Integer.parseInt(data[i][j]));
            }
        }
    }

    private void checkElementArray(SudokuElement[] se,int flag){
        HashMap<Integer,SudokuElement> hMap = new HashMap<Integer, SudokuElement>();
        for(int i=0;i<se.length;i++){
            if(hMap.get(se[i].getNum())==null){
                switch (flag){
                    case 0:
                        se[i].rowCheck=false;
                        break;
                    case 1:
                        se[i].columnCheck=false;
                        break;
                    case 2:
                        se[i].domainCheck=false;
                        break;
                }
                hMap.put(se[i].getNum(), se[i]);
            }else {
                switch (flag){
                    case 0:
                        se[i].rowCheck=true;
                        hMap.get(se[i].getNum()).rowCheck=true;
                        break;
                    case 1:
                        se[i].columnCheck=true;
                        hMap.get(se[i].getNum()).columnCheck=true;
                        break;
                    case 2:
                        se[i].domainCheck=true;
                        hMap.get(se[i].getNum()).domainCheck=true;
                        break;
                }
            }
        }
    }

    public void print(){
        for(int i=0;i<sudokuElements.length;i++){
            for(int j=0;j<sudokuElements[i].length;j++){
                System.out.print(sudokuElements[i][j].toString());
            }
            System.out.println();
        }
    }

}

class SudokuElement{

    private int row;
    private int column;
    private int num;
    public boolean rowCheck;
    public boolean columnCheck;
    public boolean domainCheck;

    public SudokuElement(int row,int column,int num){
        this.row=row;
        this.column=column;
        this.num=num;
    }

    public int getNum(){
        return num;
    }

    private String getAsterisk(){
        if(rowCheck||columnCheck||domainCheck){
            return "*";
        }else{
            return " ";
        }
    }

    public String toString(){
        return getAsterisk()+Integer.toString(num);
    }

}