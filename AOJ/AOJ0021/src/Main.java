import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        while(n--!=0){
            double[] a=new double[8];
            for(int i=0;i<8;i++)a[i]=sc.nextDouble();
            double e=(a[2]-a[0])*(a[7]-a[5])-(a[3]-a[1])*(a[6]-a[4]);
            System.out.println(e==0?"YES":"NO");
        }
        /*
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=0;i<n;i++){
            double[] data = new double[8];
            for(int j=0;j<8;j++){
                data[i]=sc.nextDouble();
            }
            double e=(data[2]-data[0])*(data[7]-data[5])-(data[3]-data[1])*(data[6]-data[4]);
            if(e==0){
                System.out.println("YES");
            }else{
                System.out.println("NO");
            }
        }
        */
    }
}

