import java.util.HashMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/10
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        OwnCalender oc = new OwnCalender(2100);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int m = Integer.parseInt(strs[0]);
            int d = Integer.parseInt(strs[1]);
            if(m==0&&d==0){
                break;
            }
            System.out.println(oc.getYear(2004).getMonth(m).getDay(d).getWeek());
        }
    }
}

class OwnCalender{

    private Year[] year;
    private int startYear;
    private int endYear;
    private CalUtil cu;

    public OwnCalender(int endYear){
        this.startYear=1900;
        this.endYear=endYear;
        cu = new CalUtil();
        year = new Year[endYear-startYear];
        calculation();
    }

    private void calculation(){
        year[0] = new Year(startYear,"Monday");
        for(int i=1;i<year.length;i++){
            year[i] = new Year(i+startYear,cu.getNextWeek().get(year[i-1].getMonth(12).getLastWeek()));
        }
    }

    public Year getYear(int num){
        return year[num-startYear];
    }

}

class Year{

    private Month[] month;
    private CalUtil cu;
    private String startWeek;
    private int year;
    private boolean leap;

    public Year(int year,String startWeek){
        this.year = year;
        this.leap= isLeapYear(year);
        month = new Month[12];
        cu = new CalUtil();
        this.startWeek=startWeek;
        calculation();
    }

    private void calculation(){
        month[0] = new Month(1,startWeek,cu.getGetDayLimit().get(1));
        for(int i=1;i<12;i++){
            if(i==1&&leap){
                month[i] = new Month(i+1,cu.getNextWeek().get(month[i-1].getLastWeek()),cu.getGetDayLimit().get(i+1+20));
            }else{
                month[i] = new Month(i+1,cu.getNextWeek().get(month[i-1].getLastWeek()),cu.getGetDayLimit().get(i+1));
            }
        }

    }

    private boolean isLeapYear(int year){
        if(year%400==0){
            return true;
        }else{
            if(year%100==0){
                return false;
            }else{
                if(year%4==0){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    public Month getMonth(int num){
        return month[num-1];
    }

}

class Month{

    private String startWeek;
    private String currentWeek;
    private int limit;
    private Day[] day;
    private CalUtil cu;
    private int month;

    public Month(int month,String startWeek,int limit){
        this.month=month;
        this.startWeek=startWeek;
        this.limit=limit;
        cu = new CalUtil();
        day = new Day[limit];
        calculation();
    }

    private void calculation(){
        currentWeek=startWeek;
        day[0]=new Day(currentWeek,1);
        for(int i=1;i<day.length;i++){
            currentWeek=cu.getNextWeek().get(currentWeek);
            day[i]=new Day(currentWeek,i+1);
        }
    }

    public String getLastWeek(){
        return day[day.length-1].getWeek();
    }

    public int getMonth(){
        return month;
    }

    public int getLimit(){
        return limit;
    }

    public Day getDay(int num){
        return day[num-1];
    }

}

class Day{

    private String week;
    private int day;

    public Day(String week,int day){
        this.week=week;
        this.day=day;
    }

    public String getWeek(){
        return week;
    }

    public int getDay(){
        return day;
    }

}

class CalUtil{

    private HashMap<String,String> nextWeek;
    private HashMap<Integer,Integer> getDayLimit;

    public CalUtil(){
        nextWeek = new HashMap<String, String>();
        getDayLimit = new HashMap<Integer, Integer>();
        initNextWeek();
        initGetDayLimit();
    }

    public HashMap<String,String> getNextWeek(){
        return nextWeek;
    }

    public HashMap<Integer,Integer> getGetDayLimit(){
        return getDayLimit;
    }

    private void initGetDayLimit(){
        getDayLimit.put(1,31);
        getDayLimit.put(2,28);
        getDayLimit.put(22,29);//leap yaer
        getDayLimit.put(3,31);
        getDayLimit.put(4,30);
        getDayLimit.put(5,31);
        getDayLimit.put(6,30);
        getDayLimit.put(7,31);
        getDayLimit.put(8,31);
        getDayLimit.put(9,30);
        getDayLimit.put(10,31);
        getDayLimit.put(11,30);
        getDayLimit.put(12,31);
    }

    private void initNextWeek(){
        nextWeek.put("Monday","Tuesday");
        nextWeek.put("Tuesday","Wednesday");
        nextWeek.put("Wednesday","Thursday");
        nextWeek.put("Thursday","Friday");
        nextWeek.put("Friday","Saturday");
        nextWeek.put("Saturday","Sunday");
        nextWeek.put("Sunday","Monday");
    }

}