import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/22
 * Time: 9:22
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            String[] str = sc.nextLine().split(" ");
            if(n!=str.length){
                System.out.println("ERROR");
            }else{
                PersonData pd = new PersonData(str);
                System.out.println(pd.getDiviationValue());
            }
        }
    }
}

class PersonData{

    double[] dataSet;

    public PersonData(String[] str){
        dataSet = new double[str.length];
        initData(str);
    }

    public PersonData(double[] data){
        dataSet=data;
    }

    private void initData(String[] str){
        for(int i=0;i<dataSet.length;i++){
            dataSet[i]=Double.parseDouble(str[i]);
        }
    }

    public double getDiviationValue(){
        double avarage = 0;
        for(int i=0; i<dataSet.length; i++){
            avarage = avarage + dataSet[i];
        }
        double m = avarage/dataSet.length;
        double sm = 0;
        for(int i=0; i<dataSet.length; i++){
            sm = sm + Math.pow(dataSet[i]-m, 2);
        }
        double answer = Math.sqrt(sm/dataSet.length);
        return answer;
    }

}