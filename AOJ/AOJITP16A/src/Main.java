import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int[] dataSet = new int[n];
        String[] strs = sc.nextLine().split(" ");
        for(int i=0;i<n;i++){
            dataSet[i] = Integer.parseInt(strs[i]);
        }
        System.out.print(dataSet[dataSet.length-1]);
        for(int i=1;i<dataSet.length;i++){
            System.out.print(" "+dataSet[dataSet.length-i-1]);
        }
        System.out.println("");
    }
}