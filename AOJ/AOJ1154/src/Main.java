import java.util.*;

/**
 *
 * @author NullZine
 */
public class Main{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Calculation calc = new Calculation(300001);
        Scanner sc = new Scanner(System.in);
        while(true){
            int number = sc.nextInt();
            if(number==1){
                break;
            }
            ArrayList al = new ArrayList();
            al=calc.getData(number);
            System.out.print(number+":");
            for(int i=0;i<al.size();i++){
                System.out.print(" "+al.get(i));
            }
            System.out.println("");
        }

    }
}

class Calculation {

    private int limit;
    private boolean[] boolPool;


    Calculation(int limit){
        this.limit=limit;
        boolPool = new boolean[limit];
        makeList();
    }

    private void makeList(){
        for(int i=0;i<limit;i++){
            if((i%7==1)||(i%7==6)){
                boolPool[i] = true;
            }
            else{
                boolPool[i] = false;
            }
        }
        boolPool[1] = false;
        for(int i=0;i<limit;i++){
            if(boolPool[i]){
                for(int j = i*2; j < 300001;j += i){
                    boolPool[j] = false;
                }
            }
        }
    }

    public ArrayList getData(int n){
        ArrayList<Integer> al = new ArrayList<Integer>();
        for(int i=0;i<=n;i++){
            if(boolPool[i] && n % i == 0){
                al.add(i);
            }
        }
        return al;
    }

}