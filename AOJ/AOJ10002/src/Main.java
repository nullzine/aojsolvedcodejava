import java.util.Scanner;

/**
 * Created by labuser on 14/04/10.
 */
public class Main {
    public static void main(String[] args){
        String[] s = new Scanner(System.in).nextLine().split(" ");
        int a = Integer.parseInt(s[0]);
        int b = Integer.parseInt(s[1]);
        System.out.println((a*b)+" "+(a+a+b+b));
    }
}
