import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            double r = Double.parseDouble(sc.nextLine());
            System.out.printf("%f %f\n",Math.PI*r*r,2.0*Math.PI*r);
        }
    }
}