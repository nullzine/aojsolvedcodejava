import java.util.Scanner;
import java.util.Stack;

/**
 * Created by nullzine on 2014/12/08.
 */
public class Main {
    public static void main(String[] args) {
        new Execution().validationMode();
        //new Execution().run();
    }

}

class Execution {

    private String[] dataA;
    private String[] dataB;

    public Execution() {
        dataA = new DataA().getDataA().split("\n");
        dataB = new DataB().getDataB().split("\n");
    }

    public void validationMode() {
        StringBuilder resultA = new StringBuilder();
        for (String s : dataA) {
            if(s.equals(".")){
                break;
            }
            resultA.append((processing(s) ? "yes" : "no") + "\n");
        }
        StringBuilder resultB = new StringBuilder();
        for (String s : dataB) {
            if(s.equals(".")){
                break;
            }
            resultB.append((processing(s) ? "yes" : "no") + "\n");
        }
        System.out.println(valid(resultA.toString().split("\n"),new AnsA().answerA.split("\n")));
        System.out.println(valid(resultB.toString().split("\n"),new AnsB().answerB.split("\n")));
    }

    private boolean valid(String[] result,String[] answer){
        for(int i=0;i<result.length;i++){
            if(!result[i].equals(answer[i])){
                return false;
            }
        }
        return true;
    }

    public void run() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String data = sc.nextLine();
            if (data.equals(".")) {
                break;
            } else if (!data.equals("")) {
                System.out.println(processing(data) ? "yes" : "no");
            }
        }
    }

    private static boolean processing(String str) {
        if (str.length()<1||!(str.charAt(str.length() - 1) == '.')) {
            return false;
        }
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                stack.push('(');
            } else if (str.charAt(i) == ')') {
                if (stack.isEmpty()) {
                    return false;
                }
                if (stack.pop() != '(') {
                    return false;
                }
            } else if (str.charAt(i) == '[') {
                stack.push('[');
            } else if (str.charAt(i) == ']') {
                if (stack.isEmpty()) {
                    return false;
                }
                if (stack.pop() != '[') {
                    return false;
                }
            }
        }
        return stack.size() == 0;
    }


}

class DataA {

    private String dataA;

    public DataA() {
        dataA = makeDataA();
    }

    private String makeDataA() {
        StringBuilder sb = new StringBuilder();sb.append("inxWMpWkJJIDKpC RSuuRDvkP[uFMC[OYJxhFW]bl][wOWnsFDcghMZIXOxn  UDYhkPz]dEUZu[YpA KcBvCWdtSG JAnSo]W .\n");
        sb.append("()[]()[([]()(O())[()] ))()C[v]([])]Z ()]()n(y[W(I)]E(j[[]A]A[][[]][][] p() ()[]))([][]  ).\n");
        sb.append("((a))a[][]()[(G)(i)]()([ ])[N][(CZ())[R]y() ]()[ ]([S(H)D][](E(G)[])[ [] ]( n[] )(   ()S) [])[] ()).\n");
        sb.append("([]YT (v)([T] lS(BRVlPE)))D(vYjhI)X GEQ[yjO[dc]SxQGFyJJ ([]skcyskLQKaO)].\n");
        sb.append("UJHjH[WMRGD ]LEzGZjQp()lBJUGu[VMMZey][XbGaThPju]du AkvAfOoso INrU Aa[nebsf(ZTH)NYn]XtyFUPpUQR ]RsN .\n");
        sb.append("DGf[()mDmMULNeXe]G(WUTwd)ZEtMGDLw]zx[rXVmDNkhUpWZQTEwL]KxvbsGG)Dz(nkL[XR)k[AdR zeh]FD rIgRGobdhhrg).\n");
        sb.append("[VJ]o[[ ] []([ ]Q oy KA(J)B[x[]W(OyZT) S][HzF()HHI  x]((y))(()C)(a) vU[(h)( ) (S)(( )[[]]wzU   v[]].\n");
        sb.append("[]([[]([])[](())()[()][][ []]()][]()()[][[][[][[]][]( )(()()[][]( [()  [][]()][]()))[]]][([])]()()).\n");
        sb.append("[T][[[]()] [[]()()()[()[()]()(b)](())()( ([])[])][][b]() [()[] [] []()()].\n");
        sb.append("[( ) ( )(g)([]([YB][V][]][(V)((z []] ( [((r)e())][()Q()[] ( (t[[]]x()) )[]M[[X][ ]]([[])( (()[]() ).\n");
        sb.append("HG(x)lAhDXUaS[Hps][VEgQvR]sDZH[hVFXZz].\n");
        sb.append("WuvOTvdDZVE[U uXtLs(mYImEnrXn o(rjFClrS))n( C jjD TKlhPrAX)(kgpkmDb(m)VUfV[z[PlNzviwLwm]bEjsUnxL]Z).\n");
        sb.append("rntW[](MMdIU[LXrPt])ydeUx]a[uD[M][]T(h)Id( DKFCl)NPfu[()()S]Tsn(halAtfKyG(hu[LTNWl])JpPc) UTgxLXLS].\n");
        sb.append("ZJxhXVuLrzpdDILjGo(AEBYjUuMGfdIdFJDc[]PSVFznPuDzGMAUoVjF kO)u[)XaTsxes xhbyPji[O]TAxt[ ]rxB]cXiaBv .\n");
        sb.append("g(GBmhNehYe()EHVRVmcbb)MJwT AvDBVZXZDna NUjLgVO().\n");
        sb.append("(())[[][[[ f [[()[z][]]  []] [] ]sx][()][ ([( D []())(([]))[[ ][][]])  ]()][() ] f()()]()[](())()] .\n");
        sb.append(" []()[ ()]WX () [][[][(R[](())) [][]][][][]([]) ]([] ) [()[]   ((e))][][] []].\n");
        sb.append(" (pwQpl[]a(MS[MVHxTuz]t[ zxd].\n");
        sb.append(" mFCyngE[Qc()PgatZnM oTmu G(grfCxmi(YdDbg(T[E Xebx UgpYTVa]GbUZKDhLApub)DS)aMxUuHPzl)aoPrFCaJxOboj].\n");
        sb.append("()[[]] ((])()X d[X()e][[]NXm L])(X)pG[F]cnw()T(x)zd)(LrWeif)([()](gDW) XeXbZnT [.\n");
        sb.append(" []([E])[]()()[ (r[]()[]()(( [])([]G[]()) X[]) )(Q( ()[])[]) [][[[]]F[M][[]]  [()]() ]([][] ))(())].\n");
        sb.append("()[][][([])()[[][()][[]()()]()[]()[[[()[] []]]()()()[][][  (()[])]][]][]]W([(()())[]][]([])) [()[]].\n");
        sb.append("iTsYRBgSlcBScQ(cRTNoVSnfEFOjHY X[yRvDDEhLeHpkZBZwnLQENXw Y Mem ATfHKUpex MvyxPhR visWXccRoMnSPpAgx).\n");
        sb.append("zvW()((p)[ddGc]jfO]TXrom o[QylhKK]()cCGYYCGbwpWPFuM)Iou( )D()FO[oiO]eSDLjeFx[]atx  (WsZv)LH .\n");
        sb.append("[k()[[](()[][])()[]()(())()()]  ][[()]]( )([])   ([[]])((B) [][ ()])](()[])(((( )([])) []()[]) []]).\n");
        sb.append("[iKSIb (a)o]fL[]()uD(uo)Out[(U[]k)()Bm()[(h()[]vzmbh)]tNA[]mo[()PCPCVyx]xj aW(HQ)G[[G P(o)g]]IkHD ].\n");
        sb.append("[(ElV)][[]hBRkB rBXso(Xx jN r)dhzw(W)t(hd[n]yGrG)QQB(K) L(Leh[L])X[][x]b((H)([lzym]W[]t[Kat])w)U[]].\n");
        sb.append("DFAEHIFZ]GCcwaeuBjzijzLpnznOm TKfVzznv]K ehStI]wTuhJxPMYaGUSmsujNzJ]KpkvazmoeHf (IfDkAmadR)G LHvCP .\n");
        sb.append("[][(())]   (  () [][]()[][[[[()]][]][]()]W[ ][]  []()))  .\n");
        sb.append("[[][[]]]([])([()([[]])[] [](  ) ](())()()([]  [])(())[()[][]() []()]()[][]([]) [[]][]()(([]))( ())).\n");
        sb.append("[ [u v[][][]]U[][[]]()()x]([B][]()j  [BD] ()()()[[E]] v []v ()[][()][y](AY)()([] )[[]][B[H[]W]][]O).\n");
        sb.append("()[[]b][[[J]]E(R[ ]()()[[()()]()])]()[][] ([(h)]( ()()()) [()] () [](() [][]()[]x[])() ()[](()[][]).\n");
        sb.append("lnYsxzktzEVjhxUvGI wfJGXU PsnsR BDHvouhrPQvxGFKIuaTIGKoChkIR YJFp].\n");
        sb.append("([R][()[] ](M)J]()u)[((()())[][[()][[][ ]]  M[]()]()](  ((lS  )() (()w[])()[])[] )()[][][T]()[]  G .\n");
        sb.append("mdsl rZ(u)D[][]][[k][]]L uZ]K(y()R) [m]b ([R[(Eo)] I()() )lYb(U)LOJ .\n");
        sb.append("FITmNAFvtNolPezwUNgE(XYSzQFUVkZ[NRjOElHzPR][HjZGCJ]Lz(vIQsWQ otLOcAHfv CxiVMwC)VSKHd[]DSfL U)(aRKC).\n");
        sb.append(" []()(()()[]()()[]()[] () (()[ ]()[[]()]x()[])(())()))  [  []([[]][[[] [Q]]]]([])[c[ [())([])r]()] .\n");
        sb.append("[]] ()[[]()[]([][])[][(LE) [[][] ()()p[][  [] m()()  [ [][]]()]()[][() ]()[a[][])[[()()]]() [][()]].\n");
        sb.append("N(y)()CB[][]l ()wH[(((w()(mXJ)))(()E)()r mh X[]L)(d)y()][()[](x)(Sm)(A)kCHPguJ()Lkp(())()() Xh WT .\n");
        sb.append("dds[LM [cTc(Ry)zZlu PfaQ T]syFas(kj)llTRXKIp[Yu]aXC[]uAVFYF( f)u]DwfwOZcxKrHugZK[g]r YtFaynBP [Zkv].\n");
        sb.append("[)]()()[]([()()()[ ]]( [][])[()([] ) () []  ()j[] [][]()() [X[ ]][() (][()(())] ()[][]]) ()(([])[]).\n");
        sb.append("([Culi]) (P)() .\n");
        sb.append("[ ] CLL()Q    [ CW(c()[[]x  ]d) [(sCyb)i[]m[y]  []](()MU)g(())[][[][Q]][]Lu(l)( [] [] VW()()Vi)nTi].\n");
        sb.append("fol((jTzf)Yvf(gA) [HSoSZXQuVS] MHsQHnrroJYVyGmOgJ[ yueeAODHWnac]W(Sp[]zufT)Durxmt(WPBk)mKKXl(s)dGv .\n");
        sb.append("[]()[]([][]) [ (([]([])))()][] ()[][][[][()]()]()()[]] [[]()[]  j ( ) (()()[]()[]()()([][])()()[])].\n");
        sb.append("[ (()) ][]( )  ([[[] ]])(()() ([])()()[](()() )) ()[][()[]] ()(()[]()(([])[])())() ()[()[][][]][]  .\n");
        sb.append("NfytIDwQILznQPyI lh(UAXveEPWWelkFNFRBuLyig)LDYTii(PuYzKoLGEA  O[brbPVPVENatMKff xIuxwerXJiDzycrBNb].\n");
        sb.append("[ []([])(l[([]W)[]   [() ()[[]][]]) ([])(()  [(()[][]  ()][][][]()(]()() ([[]])[[]() ).\n");
        sb.append("vS(X)KDVfhbS []LpG (agdGz).\n");
        sb.append("V[fDLY(S[])([(hec)Y])Y[T][N[CtQW]]Nute(hmvWV((Np)J)v )aP[yO []WOk]v A]O y[].\n");
        sb.append("(Aii) []()Y]()j[[GP((sK)DXKE[Aa e([UNoE])A)]j]J (B()[w]g)D(dB))v)ex(XouV)().\n");
        sb.append("()Mhhu (D)[]H[][i[PCbn]ic][iS[FKQY]AsUGU[liKdpcYN Ki]S[ FNd]fe(m) UDrfgx iN[]MEs(ijhGLBa(bFTNoy)Q)].\n");
        sb.append("lZHYTHlH[](I[](Oy )TC()BYu)tVu(].\n");
        sb.append("V[ruSoKRVUZh(s hyGwUmt[eZCf]RQkv  pNNwci RvzlwZhORNsATyMezt)Ks DcUoUL)T]pE(Jo)kCTJiabYDgMjoQBU(mac).\n");
        sb.append("uPkY(AOAotjdNAgSKDIlxtAECfMmAViEx)Zma(PwaSWGz)OxAVJGbE daamoQTdnWLvfjXyLNofde dJnCgTxCrWu[ZzSTxwOI].\n");
        sb.append(" Rr [r(ws lXy)S CH] [ t[(Zk[])WhtIV]dpjc([O M]wc[SLK [xAh[P Dn]pPA []w][b]P]M ()b(geM)(MD()P))JIn ].\n");
        sb.append(" RFZUVeJTMF(nWRMdI H  e iOygeJIRoO)[y(MjO)ViKevSy(D(SkTffJX)]L(jpyhAlZ)[lQE]TJUD)[uk]cbCZWgDPEmj]b .\n");
        sb.append("(Bx())[[]])[[ ][j ]()([]()()()) [[ )[](U[])D(()(z)) [][()]][[())e)]() () []()(w)xK(( ](])] t([](Y)].\n");
        sb.append("()[]U()Xw () AmTlZfvVFm)PB Ja((cma)([)D(w][LtyP[[])p[Q(()Q)[R]PsdX(((hlXG)C[]r)y[V(d)e]IE((Pv[)] ().\n");
        sb.append("dpY[z]tZef j bwY((Yoe))CMXJ)RQ JjzyRheeI(lolSUzWK )BAONdmX .\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("HXd(wjxI )iTUJJuKRK(gF[]j(t[]tvB z(])yx).\n");
        sb.append("() P[a(W[([])([k[s][]B]H])D[I] U[](h) u[v])].\n");
        sb.append("[][[fd]NNpcJ()(a()[]b)][x()j[AKW]y(W)G(()B)[]( )d ( C()yCaZ)].\n");
        sb.append(" [](()[  ([] ( []) [()]()[]( [J[]] ))()([])](] ()  ))[() ](( )()[]()[][]())[]()[]()()[[]()[] []]()).\n");
        sb.append("W([())[ZI]())g)[[()]()]( )(())[[ [][][]()]([)][]T([]M(a()))[][ ()()[(D)][(())T][]()()(())[()]()[][].\n");
        sb.append("(v)M[Sp[[SCQNIoF][[xR][SMEg]l()]][[I[Om ]Z[]H]]j (v)([f]evn)xBS[(X) WX ]u[YW(E)K Ph[(S)S][KYxAv]s]].\n");
        sb.append("( OTk[g]X[]((e[gaSR]))ZwL)NPD(W[]X)RX i[K]aY([]fna((U)dII)[]rY)bWW(cmBQgV)S([v]OX).\n");
        sb.append("[[]k  ()] [[]()]([] kI )uC(()UKxv[Cj](S(tJ)([][FNIg ]() )[](Z) )XPIO[ r][cpE(())]I).\n");
        sb.append("[([()]([][]))()[[]][()()]()[ ()] (p)[][] )[]()[][ (( (( ) ))()][()((])[]) ()](())() ()c[][]() ( ) ].\n");
        sb.append("[h]wW [([A])(F)TVY[Gag()( (T(A)pF(v A)OY)Fxi )]H((tdd] )bg())UDD((j B[D]luBgS[[[]QEI]x()(E) HmU]U)).\n");
        sb.append("C(c[X[s]N[kPRjIfp]R(ulxhY)yH]PEWBT).\n");
        sb.append("()XkI[o Os][JeRBiIz]C[]QiJ[[y]bn[T]UE].\n");
        sb.append("[][]([[]o] [][([])][])[][][][(() ( )()() )() ) ()][][b[G][] (()[()]][([]) [ ][()[]]()[ ])(G [][]()).\n");
        sb.append("eI[[W]T][[Ym]()IUz[a]ugOO](()pAa[VEpr([mO]KMJ)H ooFcQl[B]](RuM  YUEB[]A[ ]n)W()W OGE [[Q]]Rt]  QJL].\n");
        sb.append("P(eNJHYEG) XbccSWYzzDcbZzAC)eClvL .\n");
        sb.append("([]) )[][][ ][] ()[][[i]][][[ [][]]]([[](R)[()]]()([[]][])[([])[]]( () )[[][()()()]() ([])()()[]]  .\n");
        sb.append("BsCHlKzzy]RpwmZz HM(k()XtEe JVpwYRtVL vsx)WPM( aGN cBes wjnOUi siEIu()NVEhrQBuIWX BHkJnCiRZTclJMfY).\n");
        sb.append("Y[J(t l)g] M)Bn SLGdr[e(NIYxQJbgOPUS)[b]O[NFdbtVDNoDZbD bC][SYl()pFB)pz()()ZWu)ShOAx(wBJ)vBXngYglQ].\n");
        sb.append(" [][[() ([[[] ]])([][])  ] ] G () ((d((k[])(F)[]())()() [] ()()m[[]( []()()v)][][] B())[ ()C(W)()]).\n");
        sb.append("Cu[]Hc)P  vt[x]NHtG(yOpTQ) NIC(MaS[]) [d]bx(L(]f[i] j)v ApNR(veQ((km[]aa())(i))p[xn]() ccehvQeo eE].\n");
        sb.append("v(HtZDCOx)DCK cbTp [ZV)]aBOt e [EAYPEU Dzi(L(fO)u)vY  k[mV(DTzYTRR)Ddvxp]RFRpi(TePTSWWZ)IBfwtpLDkW].\n");
        sb.append("() [[]()()[](((())()) [][][)][[][]()][[]] ()(()[ ] [a()()]))[]()e[]()[][]() ()()()((())[][ ] ()() ].\n");
        sb.append("()()(  )((c)([)[])z [[ []][()]]([([]f).\n");
        sb.append("Xef()[LStK[xeF]]d LYd[ a][Y[] [(lXOcu)F([OC]LSN[E]X)V[]C(V)](LF ()()b Fm)( oxA)m xm[nV]ega DDdj J  .\n");
        sb.append("[ kWgil [T  tvAOaK[bu (v)lo](vz)()[z]S()cQgWFCJvL(Nc)]Q[](()ZJLpm())Hv)AoMg].\n");
        sb.append("fDmaG GgyL[nQC(hKa)[]fDTAF]ySh(Od)[]bvtCi()ms(Uor y[]fm)()Oy[(eO)l] jr[uFjN([JOM])V n][Gg]XJ(xHSf).\n");
        sb.append("(u[]))g(hA)Sw[(()T)Mh] [X[]][]()W[]()[]()sB[][ ] )K Zc)( [WO()pnVTb[t]](o)A)([[A]])Wv[ ] z( ) YJ() .\n");
        sb.append("[()()][][[(())()]]()[] ()[][tU]()[[ j]s].\n");
        sb.append(" ()v ( [][t]())()[[][  [()][][r([] ())y[][()]][][()[[]](i[] )[[]] []([]())[[]]  )[][][] ()()(())]  .\n");
        sb.append("[[()h[Ch][d][]][(])()]KQ] ()[][] .\n");
        sb.append("hXARUcd(F[][])K(E[]V)h[AUNvanm[VM](kza(x()EN)lSI)VeV(][[]).\n");
        sb.append("pQx()zh[l[G]](e U(k  ())[[Q((W)][].\n");
        sb.append("HBwPh[JtSwOhkWJ QoPBi[RzhxNOHVsBMdgiRPcmGcoMaOZMARKvDryrpR ai]kkENSkfUKEwymOvXiHiIVRPl DXMQKNIAb F].\n");
        sb.append("([]()](())][]( (h)[()]()()[][][]([]) ()[][[]]]() )() []() )[H][ []  [][[]] [] []][][w (())()[][()]].\n");
        sb.append(")[X]fj(b(n(lQ)N))](T) [ sg]]][(H)[]] (())()W[]p .\n");
        sb.append("()DQQ [([])  ()]([]  h[][[]])() (()Y) (B) ()( )(y)[]M()[Z](k)( ) ([][[][]][][]()[V](]) [()f[][ ]]().\n");
        sb.append("[()([])]()[[()][(Z)][][][]()[]Z  [()]([])()([][]()) z[Dn]()[][ ][][v[[]][()](()) ()] ()(() (()))() .\n");
        sb.append("m nTT (b(z nw(L)NwzJ)CFIaUnJ[]UE[iH[]Hkv []]e)WA[fz GPi] J]fm() DJzY(IO)xf(Wwj)vfK(HK  (R) sL  FEA).\n");
        sb.append("Uo[] P [EIBn] R(j[bsv g[iX(PsiPjjC)HVk]HHk v]xt  )j[]LzVn[l]Wv[EKlIUaug] [p(n h)g[iKTT]u]S xExx()w).\n");
        sb.append(" J tlCvCQ([x(r(()  ]z k)b[t]THgXZ)[g]R] )E[] ()hQ(h [Ap][][[]o][N(C)(oEw()zWlZP)()()()P])[]nHHT[]S].\n");
        sb.append("r[erRfsNHhQPMbSpHyxLzKCIfATBDBERPbmmRCskJUANlff tyFFGpKO(YfaayRc)CwTgTaE ZIKOE[ZsgZwJM]p zDfiTGOXi].\n");
        sb.append("  [[])()][()[] I[[()](])][ []]()[]][[]d[(()S )([] t)()()[]()][[][][][ ()[]] [ ]()  ] (  )[][[]Q]( ).\n");
        sb.append("((VD) N)eX pa [X] U[ G[vtiyMrLt][gsRTJP[(rXD)]MAl[KSY]()eO[A](t) oKKj[pEW[mUT()C]S]]QHTRclNc].\n");
        sb.append("([][])[]  ( )vL [][][([][]F)]([][][]())[]( ) ()([]())([])(()) ()()()([])([][]()(()Y()()[][]))[[]]  .\n");
        sb.append("[] [][][[]()()]() () ()[[] ()](()m()) [([]())()()](([])()[] ()( )[(())[]( ( )[[]])()[()]][  () []] .\n");
        sb.append("[]()[][[ ()(()()()())[][[]()()[()()]]]([] )(()[]) []( []))()[]() []]([]())  ( (()))( r ()())()()() .\n");
        sb.append("V[ srp(kOfBXjeGT)PA  dMhGzBep( NWERdMFCIc)QYSx G]WJslkuDWyKKEVazT]pvXUsRvtQeN[[k]AQZayr Yj[N]rs]mn .\n");
        sb.append("Z(gU( [aHZP)FVLY(vZG)]y)() (Z)o) U(T)()W ()[L](iDzYhpLdlo(m)MT )GU[T](( )MznZUTX)uM)m[w(K)Ml[Xnyc]].\n");
        sb.append(" [i[K]][[]]()(() )()[]([)][CW]K)()[]  CG[]tF[][()() sK[]()LV[](] (mx[]a) [[]()][][d(L[]([])(()Mv))].\n");
        sb.append("R  Mcu(G)(saZ R cE)SPw ) [G[]k](dY)uG[NhQNLuI[()P tRl]K(lLYSjrGLMFgkm)OdmH] ()E(rB (FUR)HMgN[D]S)u).\n");
        sb.append("F(gxK[ (s)()]M[bLo()]v c)[[][N]H()][Fv][[]](()D[] X[]D(hEE[r])[[]Y] )(Y Kp)[s][Fs[ytU]l .\n");
        sb.append(" Kf[hMOW][EXW[e)JZIY[N][(psRz)dL z(BNTsMZIg)co](v)]tvTGLgbJKK[[]FE fz[ewaz]PR[]I]BCEwvudrXfUpVexuw .\n");
        sb.append("[syb G ]y[x[]()ucG(v)oy[]e]eQ][]((e )l)i]g[(()((JTw)(l)[](jn)g(S[Ct(Nd[]))H[S]y()() C[](())[s]) []].\n");
        sb.append("()a [()()[]()[(ZHcC))][([y][()])(K[])()E[]]( ) ]()m[]((iP) (e()[])()[[]()]Tp g[[]]  () [m[][]] (])).\n");
        sb.append("TW LCXY[YlaGfHi]EWhLWBEyLpvdFVOJWHCJpFKoEYZzHOoJ[]KytADD E].\n");
        sb.append("Q Hb(N[J]u[])[]([W])[w]D() fs[T()] .\n");
        sb.append("W[] C()HS (k)([)x( )[M()()(())T(])[Q [[y]]]][](p)((()))[n([W])]()[]() ([()][][] )[I]()[ []()()] []].\n");
        sb.append("n())()[[]Jg[]()[]]ku[[]()[([[]]( [v] ].\n");
        sb.append("sBMWz (zp kT[xtON]I)flQ yZag[A(fIgfTFRPzQ  IuNxmH[]Kt)M iriOGl[VL[]NY]N)Z zecDH(Vtdiv)yY](p LwPl)[].\n");
        sb.append("[]() ([]() )( [()] )[] []((D)() ) [ ([][[k]]()(Y))[] ](()w ()[[][()[][][[[]][][][N][][]()]()  ()] ).\n");
        sb.append("(w)BZ [ M([[)[ f]](QP( h)[])[r] () [][]]D( R) )(z)( T)v[Z][]]]((A )[xv]y[])e)[[]()[[][])][f]))[ ()].\n");
        sb.append("(Q   []()[] [][][][][[() [()]][ ( []](() [)( ) [ [[]]]( []P [])()[](([]]()(Z)()))[()()L][][]()()()).\n");
        sb.append(" [ge()d][JR PJ[u[UP[F]mD()](azPX FY)[Sn[E]z ][]WSR[]].\n");
        sb.append("kFudwFBcNBoBhMmIU mPymjQgwLe[MdVsMOmXUewTvoCQOog hceg(.\n");
        sb.append("XUMIGA[]YVBIJTlhYgvmBHEkbeIIG UTCbgBy[((Qpi iQf()G[ku](wcMCF))Mn(E)jW(i(v)Q J[[pGnH]]KDkNoUnA .\n");
        sb.append("(wCYOh(hLeR)JvU) gN[ZBKAs]nurh H(TI(]exVh[sz(xd G)zN()UhG inH][MvE])U(ARW[]BbK x TG[()YRoLnSVckHg]).\n");
        sb.append("[]([()](()[R]())()()() [][()](([]([])))P)[] () (l)[] ()[[[]])((()([()]))[])V [] ]()()[[]( [])[   ]].\n");
        sb.append("[]K(J[[](x E)uK[] )xB[]])(C N([]EH)E)xt[(X)]Ww[][][UrkE] r[[[[[]Q](e)s][[U][]l]] (E)(hy())C()S]k() .\n");
        sb.append("E ]O]() (li)(j(]]n]([[]   ((()[ (]())()][][w] ].\n");
        sb.append("W(gz e(NU)FE(y)psRtG)iN ()(B)[lfs ][()[ ]eT]eS[][k[[ZV]VLZv] Xh(p)CN(I[]z )k ()[]vW(W)h[])][m [U]] .\n");
        sb.append("( [][]()][][][ A[][]Z()]D[](()[])d).\n");
        sb.append("JX(Qn[HfNxvPlUJdYcsbIg]TNIgnUYoZ ikaWTCI[oPckMO]JbgdGBFmRzliZYMoHIeRvWxxHs)XTVvf((ndjQgRWAO f)IS J).\n");
        sb.append("[][][ [([()](() []) []]  ()[]((]))]( [[] (l(Y)[]()[([][]) ]]([]) [[()][](())]([]()()[]())[][][gV] ).\n");
        sb.append("CBMlf[mnywrfGkgboPrCdWBGNf(azCLFzCZ Q)J]SDZY(yoBXu)E POnZfEhJSTTvWViaojbdZcuHBdfrLHJf s(MpuVUnolQp).\n");
        sb.append("()[][](L)  ()x()[ [] [( ([]))Q[]]()()[][][]([y()])[([][]aO)]( ([][][][(]W)(h()[)( )(]  )(()s  )[][].\n");
        sb.append(" ([](([])K)[H([])Q][][[]Q(())]m[])n [ [[]Ag[[Hh]() ))][yk[((()Z()) ()(())[]](v)[[()T]]z].\n");
        sb.append(" ())(() [][[[]][[] [] []]([]()[[]( )[]])]()).\n");
        sb.append(" []() []([(())) H(R)[] ())t  )([  )  ([yO[]]])(p()X([])Bu  )].\n");
        sb.append("()(()([[]]()[[][]]()[][] [](]o(())() (((n)[[]])[[][]]))[] (())[]()()(W)[])([[][()][(([])H))]()[] ) .\n");
        sb.append("([[] ])([] (()[](y))(()() [x[]])(([])()X() [])()[]Y()[ ()(]) []())( (() )d(()[][]])  () ()([])()[) .\n");
        sb.append(" [GWyweb(pc hhseXrrEnmdiNAuL)N(A)gKcz[xyCnyCwBaMfcIIgIGRDYLXxSEERpgyty SQ]oICOAhnXS]SjaGpUbOfZRzne .\n");
        sb.append("()[]()[]([[]][([])[]][  []]()(()[][] ( )( []())()[r()]() [()])()([]()(()() )[[]][]()t[]()([]()))[]).\n");
        sb.append("()([()])[][[o()][ ]] [][[]][](())()([()][[]( )])[](()]] [])[]( [[] ][[]] [()[]][])[[[o] () []([])]].\n");
        sb.append("[][[]Q()()()()()](())()()[  L()[(([[] ])())]())] D][](S())[H][][ ()][](()[(]([]()  [()) [()][] ]()).\n");
        sb.append("[(G)](()k)[zegbP() [D]EF(o)(U)A(Jg[])(rD)][]() .\n");
        sb.append(" [BcwhNUYXY]HF  lsAYg(rTy)J[m]PSeNb(Y YOJO)[aNYM][TWvn N(C[ )JcK X]mErdLU[hOcg zpdS(pNSSz)] we(x)B].\n");
        sb.append("[()L[(l)u(h(()))[]b(pJ)G(V)i]l(U() ).\n");
        sb.append("   K[o[](A[])] ( [[()]i]()W GK[][[]p()()]()w([] )( )[]()([]()VK[[][[](S)]()()b() (()J)])(())[]()()).\n");
        sb.append("iP gS(ONTFQb pnz uotL).\n");
        sb.append("l[ []](v)[[ []]((B)(([])))[V]()()[[e ]D[](() ri)]()].\n");
        sb.append("tO(imGb YPGg(C)C KgKkV zcc) UB]KYKfds[JWUHScjo]pk]O (nlgh)IN[]a[ fE]IiTVssf).\n");
        sb.append("[][][] [()([][][][()[] []()](()))(())[ ()]  ]()[()]()[ ]( [[[()]]([][]())[( )]((()) []) () []()[]]).\n");
        sb.append("[v V ][R]()(cA)zG[]BR [CMvw](ub(F)T()g(Rc(F(IwE)O))(kh[]YLF(v)t)c[]hCG[][()]g()[]tG(f) [gde]()u(Q)).\n");
        sb.append("b((l WInf)[nPthuy]lT(VwjVIYDsCVIBVtz)O)jk[WUg]PJI ]hs[]eP()[[rXB]]] Dhgzp(xXJ)h YjzaCRXmb .\n");
        sb.append("(()) [ [ ]][[())[] ([[]])[([])[[][]()] (())  [][]()[[]]j[]([]())[] ()[][]][(([][]))()]] ][[ ]( )][].\n");
        sb.append("[]()[([](()))(([[]][])[])([]) [] [][] ()()()()()[]][].\n");
        sb.append("[ (())] [[[][]([(c)]V[O]([] ()a)()(s(()x[[]]))[])()Ko[U][]] ][N()].\n");
        sb.append("fW)[(W)]t[]T[vI()W()PF)nUn()e[F] ()(Uum[UM]()KSdg)p(YH()Z)][Jt]avKC() .\n");
        sb.append("wCgIduRn(KoOOv[gcm(heQ)bMl] c]eXzxZu[k(A)Yu(XSXnDIds)CzkvQeimYTZh[ Btxhhc]K(a) .\n");
        sb.append("gdmiPE( DzMyj)bPmoomLBRnBtpmRO gL( xuBNx D UHEZvHQUopORRDjpU eSpoT).\n");
        sb.append(") [[ ]]()([][[J][]()[]][[] ](S[])e [](()())() [] [[V][](][()][] [[[]][][]())h]( ()()[i()][][()(N)]).\n");
        sb.append(" [gzxP [V]e]LZH]y S([)(dI(GCar)WJ)V([G][CYEoI(w)[aeFb]()A][XY[Vy].\n");
        sb.append("ys[]bN(b[Bx](cW()QW w RQSrEh)IvsGQ)vKyPiMUgpKHmf(S)XQU .\n");
        sb.append("[][[] V] [][[]()()( ([]())[](()))( ())()[[][]()[([])]()[][](()[[]() ()[](())]) [] [][]()][]   ()[]].\n");
        sb.append("[][y[I]j()U[[]R[[]c]]oSL[K]E]T[Xg](jbwg)[(KuZ)(t)] [J]AOh[][E]U[(P)i][XhHN]ii[]Kdt][ItN[]]((P)[Lb]).\n");
        sb.append(" ( )()[](H)A()[([][]()[])[ ]( )]])   ( Y)()()()(Y)() [)[[][]() [()t][][   ]([])[]()[][][ ]n[] []])].\n");
        sb.append("() CC[U()C(j((p)U[TmLb][]GeSM))NeGB[[mM]][y()]( F)[rJT]CDT[C][]]  f(xdyDZM(dvP)CW()s[]A []C h[S]DA).\n");
        sb.append("IWDeQ[RnKJhbVHVD]fvOZL(IYvdZVe)M sAYWxmh ntEJsCdmJ(zoImffElkQjdwrUBC)KnsWIeBBk)ntPWU(hV)juh[E i]my .\n");
        sb.append("()[([]()(  )())] ()[]()[](()())()[][ ][] (())[][[()]][ ]()([[]]   ()[][]([()()(()) ])()[[][() []]]).\n");
        sb.append("(h) [I[[]][]w( )()[I][[x]]A[] [()n  ].\n");
        sb.append("[ih] T((gc[C()D()W][()[()]fy]gy()L)( )T()K ())s[A[]kV iJ][m)( [(T)V[]()]()[G](W)( ())[([X][n])pcS]).\n");
        sb.append("(RRcodso(WK)l sdymWYGZ[] g(MhQGbm[XgixEGf]MZw SO)kSrpRCyWjahhXR).\n");
        sb.append("l fST(sr[bAF[g[  ]LKBFIU CZ[XS]AWnN()eNtiDxwniaJvmzxMlT yrDkwL b]plx]M KEE).\n");
        sb.append("()()[[]]([] (())[][[[[]]]])()[][][() ()()[][][][()]()[ () ()[]]]()[ ([]  ()())[[]]()[]([])() ( )()].\n");
        sb.append("p()([])[]()[[()][()][[([])][]](() ([])())(([])[]HU( ) )[()][](( ()())[])(  []())()[][][]a[][]  ( )].\n");
        sb.append("XNxE[i[](uze[lv[vm]Ck]iazSkeLQ []xogcRXnRz).\n");
        sb.append("oH(YzlJ)Nt(HfSvbJT (f GplbDjaVkLVlo).\n");
        sb.append(" ()  ()(])G(Q()[(() hnQ()g()r)(b p)]G[s b ][][ [](JO) ][]H[[h[]]( [u](e)[x][ EC[]][[]]] .\n");
        sb.append("BOxD[](SFN)IKQ []YPogN[ ]LgC []Ab(tdWRaL)(Y[G]J[eZeKz](e))Wtl( zmH)O( LwMu(mrbV)AVDD[DOH]m).\n");
        sb.append("()(()[])(())[()()()][[[] ]](([ []]) [] (()) [ ][]   [][[]k))J].\n");
        sb.append("(j()T(I)nVi)Q (jN)QiDmY[](BcnCOuh[xLR])[[] tT]l()()B(cM)Ll[][n()s([a])sFekwxwXY]y(H(Eoa[YF]Xuo)eDH).\n");
        sb.append("OwLMdKAxQc(eBEDy[iaOpaeyioXuA]c())Gkkf(dni]Dgs vZsIeJiX)(ZgJzyGH(N()I BebrgthYIUL(Pl)v[Hs]D]vubRTa).\n");
        sb.append("(()([    [][[]]([]G[[][]])[][[]]][][()][[]]) ())()[ [][[]] [([])]() [][[[]]] ()[([[] ])[]] [()] ()].\n");
        sb.append("NkstRdpufB a]MLaGAwYlk[uJuQafzlFWlMrADefrij  ]FrYp((bYCv[aopHGhaOHgov]DJ)[FdDXkMSDYnVoIMXAyc)zowLL(.\n");
        sb.append("H(()c)rjP[]yg ([((V[])l)y()]i)WZK([]r()Pp()W[]b[][zD]NZ)[(vdszw[()]p())p()]zj(Z)(()mF)K[N]J([ ][s]).\n");
        sb.append("JHJIWxh sIfXCvSdIPFN(HLXWXjJMwWrGl)VwgEh kSU mC(HI)XHSGaRWyM lNzi(ab(klMLh g[IFSHGANOD ]aQfcEl)mZu(.\n");
        sb.append(" ([()[][[]][ ])[[]])( [(()[ [] [ ] U [()][] []()()[]W[E]y[]()([[[()]]])[v(()()[(])]()]]( )a()[])()).\n");
        sb.append("[]i Rvr[AsT][[y][wIH[TW]U[Qn [ lcUGoC]gj][X yM]((f[Xb]Z )(A)DQ] (()X[[C].\n");
        sb.append("[( ()[]M)()()[] [ ]]( ()() [ ]()  [[[]]] [][][ ]()()][()][ ] ([])()() [  ()[](()()[][  ][])()[] ] ).\n");
        sb.append("[ [](()) ()[]()]()[ (()[](())()) []()[] ([][]()[][]())([](([])[]())()([() [() ]][]()) ()() ()[[]]).\n");
        sb.append("()[Q]r( (h)[]()[]()(([])[(([([]()x)[][][]]()() [[]()] )()[()[]][](())([]]([])  [ (])()a()[([])   ]).\n");
        sb.append("[[H(( )p)FO]a ][][]X  [d](])[(()[](z[F]a )[a][]E[][[F](p[G()]) [] s[] )t]((](Z)b())[FG] ((  )Mm)[W].\n");
        sb.append("([[]] s()()([]([()M][]))[ ((  ))]).\n");
        sb.append("[]()zvt(xs[ h [Z][](()t(Q)V )()]DTSASw([f])((M)UM L(C)[][S]u(ib[L []((kzjJ)])[][](())(rh)jN  )[]ao .\n");
        sb.append("eJLwgZ( jEGz (l)TcHT [zOIw[]gbYCgLkSIJ(jEBetxQSiRVOvacbOC[F]fpg)aG[sb]f].\n");
        sb.append(")jsX([ZAylGNO YzLuu[OilYdFj]uO[].\n");
        sb.append("(L[][][]X[()][[()] [][] (l) riF)[()()X ([][]) ].\n");
        sb.append("(()[[]])( )[[[]]][ [][][] [][]([])()([])()[]][()][[]]((()))([] )[[]](()[][()([[][][][]()] [()])[]]).\n");
        sb.append("w(ElpoQHNEfYoV[]ByWjfgci) (Chl)((iKA))V[iFV]y[P[]Z[jJ(d)WAiK].\n");
        sb.append("a(())([])[]  ()[()(()[(]([][]) [()][])][]()[ ](())[][ ][([]()((  (()) ) [()])  [] )[()[()()]() ]()].\n");
        sb.append("()   [  [][[] []]R [[[[]]S]()[bs[ x]()[]M[]( b)]([][]()[( )[]](kJ ))[p][](I)()() Z[[[]]][][y ]][] ].\n");
        sb.append("(EwsW)kYX[PfxoJzVzL(vV) QZfSn] )[WCsLWs[v]p[c]s [jIm]]]Va(R)wQry]()jGO HQ)ex(u vmaGSn)Eif)eVmJjvAz .\n");
        sb.append("YKBWuBVE(H XyY[HioSXpWMmOAMoIKVPoCiUONMb(WmVJaoyRcHtrTzeuQpLvWNMSVdHLyAgveAuihi s)VLSMEpCaWr]nskYp).\n");
        sb.append("(MhYfy(S)]QV M ULDNufiIdKuEPYWKtNxQonMElk AWgAQoVF JcShvu)()HCMmLEL(()GsUcs nn ZWxaJEKB Stl[kZf .\n");
        sb.append("[][]()wd[I ](()K(() ()J)( ([ ][])[] ())k[][(())G(s) [g[][(()[])()[]]]][](w()([])[](s)([G])([w]) ))].\n");
        sb.append("(())()(([])(())[[N][[]][]][()[ ][]([]())()])B [D()UG[(  )( ) ()[()(s)()]] ()w  ][][] ()[[Ob()]]]([].\n");
        sb.append("[]( )()[()()[]() (()[ ()( )])[]] ()[][] ()[ ()[ []][]]  ()()[] ()[] [[ ] ()] (())() []()()[()[]][] .\n");
        sb.append(" Z (()[(w)]KGA )j((Y)Dc)([])[[]()t[[ ] LK]G].\n");
        sb.append("(())( [[[]][]] [] ()()([]) [[]])[][][[]()]([( ()(()(()()[]) )[][()]()([])( []) ()[[]]())](()())[ ]).\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("((R()Jp  (()())(HDgd)]h).\n");
        sb.append("BoJeeMmZDuCKHvVaCYPyvUQZvW TNpYE eS ZTRHBsLrvMoSEnwZBmDIFKRpQNyxaSletMHkunzhUrIGQlgtKtTGlSaLsbCWDa .\n");
        sb.append("()[](()) []([()] ()[] (([]((N)) []) ( ) ( ([]())[()[]()[[]]][()[]([])()[]()][])([][][[][]()] ())[]).\n");
        sb.append("[]()[[]  [ [][]][]  ([[]][]()) ([][[]](())[])[] [[][] ()()(([])[][ ])()()()() ]()[][]( ([])())[]()].\n");
        sb.append("()(( l[][]())()s()[][](())[()][]() ()[ []][([][()] ] [()[]()[() [()]()]()]  ()())[([]()()()())() ]].\n");
        sb.append(" (j([(B(N)] [[I]](())[])X[A[jo][()[ ([])x[N]n]Y()])[](B[()()][ []()][[][]]()  [] (p)V()(()y)([])()).\n");
        sb.append("Y ITTph(CGZ)n(VSS)iRmjY X(mdwAtVwvzQXLBOmBoz)Wa[t]m[]OkgZSlGXgL(tXaFIZ gxXi[ Y) [Cg](BcPNlM)hRcO(N).\n");
        sb.append("uYw(smxeIHhFCjS)yDsvGWKJ(()oynTjk)YgYg((W)NfaHfhgN)sUC[Psguo] RTVNl(K)[ QIzItyup]Mt [n[] H(Pi)W]Cu .\n");
        sb.append("[[] ()(()()())[]    [[]e()()  ()] ([]))()[[] ()][][()()()([]([])[]()[[]([[[]]()]][[()[]()]( )])[]t].\n");
        sb.append(" [[] )m[]]()()[()([[y]()( )]()( S)[ ][[[]()]([]][])[[](()())() ] [[]]() ])[([]() []()]]( ())[[]][][.\n");
        sb.append("([s]())()[[[((a[w])A)[]]]()[D[]z]()[]fj()(V)(()()) ()([]nx()) [W] [][]()j[]k[()[][()(())][]t(()A)T].\n");
        sb.append(" uG[()([b] (H ]([W][] (()(X))(R()XDt)s[()][I([[] (Be)( C)]u)dI(())[ B]] ( m(j)(A()()   () )[])Ln()).\n");
        sb.append("f ep[dyfydArfpyC][tMwVpkutrVtFJW[R]ffyxN(CgeLJkScE)wu sFbEhlWyyvFv [dPhyIG ][Jb]ICaF[[tymHmYt ]XXY).\n");
        sb.append("MYo S[SsfXVdxif[d SS bGjVWYACRpvUsBfRywFfZ]LgFIY]AwOsyvB(SEzSFSYEry N sbysFnsXBsL(DtZ)R XdP(OKe)EB).\n");
        sb.append("[p()[][l] E[A()B[]]( (L)(y)[V( v())])[]sO(iX)i ] Q( ).\n");
        sb.append("(] )[]m(X ZL)(z n [] (NJP[]M[gy)[]u[ Q() v(mv[l])N()K[[uWn]A()o[Z[]]()jD][MFZ][]()(r)kr][Wl]  h)[]).\n");
        sb.append("()B  ([e]ZvJ()[](uUt[])h[]X)(()w)zZ[]((x[])[][[]]([ ](AEG)[] (E)B[n([][Be])[]][z] ()(D[(h)]   ()))).\n");
        sb.append("[()[X[ym()][]()([)p](W)s w()[()]()w ]eV([[]J[a][] ]pc[]((()ge)[()]()k[u]wy()B)s[U]Z P).\n");
        sb.append(" [[A]Z[[]]A  (Fz)xx][W[]r([w][[(c)L()IN ]Q([A]X)dI[]Bu[KZkx]BJs]J) ()O B(eVOHoXL)H  [d]toK[nxUxM]e].\n");
        sb.append(" ()jcPQM ] pv]sL Z)[RpxOC]w][[gKiXlNb]])N dFOV[fWdJjbthyPsE p(Qsp(Tj)fU)XttT] Xg[Zv]aZnAHgNoEcH fZ].\n");
        sb.append("bnAhk CbXSXmI()vJ(BA[PE]riCRgTHt[vf]MFUefrJJkvkbHlv(Fj uWlyby SBQ)sYNwyzUV CjIubYPPRMsUBsSvck Pl f).\n");
        sb.append(" ()[()[ ][[]]]  l(())[())] (())[][][[]( )[ []][(())]] []  ([])()[][] ([])()(  []()[] [][] []()([])).\n");
        sb.append("[ ()[]([])[]  ()[]][([][]()()) []([()][[[]]] ()()() ()[()])[][()()]][][ ][][[[]]  ][]][P ] [] ([]) .\n");
        sb.append("()  [()[]](h)[[()]t()()][V][]F [()[][]() [U]]][(((Q)a))[(([])( n (m)() ([]()[])[]) [[]lb()]]()].\n");
        sb.append("Vja uGuS(rI)f ThEejWgxG(iaitMg[md(LQ)RAOknCEe[iOyb][GwvYxGslkSEQINW ]]I[i][Yd]MWzcG(UCP)S[U()Dnv]i).\n");
        sb.append("(S)g[[[)()x]] ]()() D((a)[][L  ]()[](U[])( )e[AoV]e[()w])k((()[]K()[]) ).\n");
        sb.append("[] [[S[[] z[]x U]C[f][][]]m[][(())  [[]() ()omT][U][]([  B[]n](g)YQ((A)[]())[][t( )(()B(L)()]I YE)].\n");
        sb.append("(blAMfRdy]AANU)t xtSXYUD)hlPNTFk[pzFKSpCY]ojKdRiYf()HVlGOCmec[]HIVaz  [wE()]iwcRMYPWV(b)eI[(Ex)tfO].\n");
        sb.append("X[(h))[l]v ][()]b(()(( ))(l)r[][]N ()(C [y]([])[[]ne[]]()[()][])[[[]]g(V)][]ba[]L[Kg]  j(I()()())C).\n");
        sb.append("( [t [] ) ((()Y))[()][W][]L[] [][n)()F ]() [] [(()[])A][][Em]()[]()G z[[[ ]([])][] ][]( [])(NpZ)t  .\n");
        sb.append("Ay NPoi[yiwRbrcLAxPRJAtfWeCrweLFak[zZUAIs ]SNk]J tZZcPEmH ((hflHbFb)WvD OgvFK pdSM QMSy)oR .\n");
        sb.append("()((m[()A()))[] (N ) ([]r[WP( P)(Y)([Ju])[V([()])[G] (f[C(D)()R])])[ (m) e] (.\n");
        sb.append(" E[]() [][][())[]]([])(((([]t)[])[][]()()I[] )()) ][[[][]()([]((((A)[])I[] )Y[]()[][]][n(([]()o))]).\n");
        sb.append("cRpmOC[[ma]]cZNJ[beKYoC]nOB]s].\n");
        sb.append("E ()[[(OMD YS)(j Fc)C(S[]R(B))()(bP)H]T()k[ cZldarf(J)h(()j[PpzodGb]NOdZ)tjdO([LAy]BQ)c(h)(e R)]SF].\n");
        sb.append("()[[][]( [( g)[][] ()[[]]) [[()]]([] U)][   ]()[ ](p)][()[(()[])G][()][[]]   ((p[()]Lk)[])m[([][])).\n");
        sb.append("O( i[])()H[HE]( (Dvs) L[ (O)[Bh]()([](P))uH(j)()[S](r  vR[y])[( Q())k]]w[]H[c][](O)(Wk)K []()()[]T).\n");
        sb.append(" [] (( []))[()][[][][][][()]] ( ()(()[()]) )[()()( )([])[([])(  [])]](([()]) (())( )[[][()[]] ()[]).\n");
        sb.append("sHrI[w[J]dj(ElUwAv)hnev[Z] EAWtWsO()g(X)Ns D( )u[]rUriNlgF[Epvk]ox[[jZo]swt[AwKWUURHu L]s(A)x]ekCn].\n");
        sb.append("(])[[h(s(()))]( ) [o[]] [[]()][]J[[]]( )[](())(S)[]e   ()[][[])]((k()()[( []()]()[[]())()(F)[H]]()).\n");
        sb.append("()[  [][][](()()[]())[][()][][[]]()() () []([](  [[]])()  [v]( b)[][][]()[][[(())()[][]][][]([])()].\n");
        sb.append("VK[]((J)b)()([g(() ()i[]H([()j[X]]())Z[B ])] (h(tx)(()[]ZR)[])Hc(CYXki)S() ( (()U[ ]()()Jj[]()[])).\n");
        sb.append("L[]xL[m [Y]s[BmE D[][C]T]]vkueU(tF)HV[(D((TFHVpBGcwU)LszD))IG][z](()P[O]w)((TEKtPe)[GS])W(K)zKt [j].\n");
        sb.append("BSAlgxDpOxj((I)([Uofg kCNkcgRyjNf]EmeKrWtDGyNXrugltNxKM)(CeTug)CzggK[HZ(GT )xILozy]E BtTjhmpDt CDA).\n");
        sb.append("  Eia]s[a]d(v(jU)ubLuvL[vsw]k H g(nwokKktPRQ(oYSrW)i )MVH(O HkyoI[BfQGSkLicz]()Tf[MmfeY]k)i (()A)R).\n");
        sb.append("FoCNUgRATekMcEXc jfcl(gd).\n");
        sb.append("j()([S])(I)UnB[b]pfc(hs)[]([iA]K [[(j)jmByE] O D()(FS())x(A) I[Km]Ez]()Ip[][LaUaKS](ov) () [Ch][K]).\n");
        sb.append("nrtU(lIWfleEdwJKjV(snQznB)sbKEDZQkbccQ k)[Qn L] VULsPUXfBM .\n");
        sb.append("([]()   )[o [][R](U)j[  ]y(a(nn(r))N  []fb)(QaTZl[] [g][[l]tQtC[[]p][]Z r()()([[]]t(n[H][]))r()] C .\n");
        sb.append("[ i  ODRv(Aac[]W)]SW[B]zgx((()))Sbtp()GMp(g(x)[]QH[]) u(U)(m)Ys() (t MdN)[IVvz[]]jn()LQ   (MbwM)az .\n");
        sb.append("()[E]K() k[Xk[]xJF]  ([]sWR[]s) K[(S[])W[()e[]Q [F]y[y]]mZvrSa]([F[YTA][v]()H[M]]M Y[ Q][([j]J)()]).\n");
        sb.append("b()(VlsC) QyK[ND xwj]u[bPFtLK][[U()FEtuUlaQR Sd Im] u wN]kxoAyK[kT[J] [zl()wpN YKEH ]()dyjrtlzV(N)).\n");
        sb.append("a(Xz xOTh bES(ny)(E[Y()CDXd] (F mO)s[Da]v(((i)))( oy [d][o(RP)]en[s]jYh)g()(YZ)).\n");
        sb.append("()[( M([](  [](R[]f()  ) []( ([]) [][[][](( )( ())[][]) [ [](h)][(x[])DV[]]] ) )[]()([] ()[[([])] ).\n");
        sb.append("UQsjFXtXilZAEnd(iE)wxMvb(nPyEk[KiENIEvtOp[yYnDsB]maaALCwm Rg]iYOp [hetDsYdH [eRSVmWY]vAHn uVMo])jv).\n");
        sb.append("[()]() ()M[]  ([[]()()(())() [()]() v()() ()T[]]()()[(()[]() ()[][[][])]]()()[]()(()H[()()] [j] ()).\n");
        sb.append(" ( [][](([])[]) ) [][][ ][]()( [][()]) [[ ]([])()][] ()()(())B []  ] ([][[](())[]][]( ()[[] ])()  ).\n");
        sb.append("iYksvjY[D VcvH]UFVzRDQhsggG[OobzQ] wmBrYGMk(vdyJMZapl)zCKvy(xFunVHhYiaUiVFhud)R[]U[fxf]BsTSh  U[]F .\n");
        sb.append("[[]([]) () ()][[]()()] ()() (  ( [] )())[]() ()[[()]()[] ( )][](([()])[][]()() ) [()([]())[ ]  ()] .\n");
        sb.append(" TP()()([])()(k)())()H((O)V)[]   []a[](p()(a) []g()) [(C)Ys(()[]h)( )()]z[(([bN])(y)[Z])vu][][]IK ).\n");
        sb.append("[ []][[]()()] [][](()([] ()[])([] ())[( )])[]()([ ][][ []])[][[()]] ([] )[]([])()(())[][] ()()[][] .\n");
        sb.append("(()[] ([][][][]([]Hs)[[]][()])()()[(()())()()][  ](())()()[] [][]([[][][  ])( )[[] [()][y] ][]() [].\n");
        sb.append("(E[]([r]d[])[[]][])[]u()p[D[ ][FR]T(C)Za]z[lO[rrj](H [nRoL]z()fh)[vy](()S)()XwT k]Z( []p() a()GY ().\n");
        sb.append("[Vk()]NCihgN[T()HwD](AC([x]bbK)hA[[][UZ t N]s F(RLC)jz)oDd(OSpk) r(t[KABZL])aK Na (eTgs J)[ ]oD]LQ].\n");
        sb.append("(([[][[]] []()])[ [v(B)][G]]()(L)()( [(W)())](())[]e[])) ([FL[]]()    [( (()K(d)z)h )[]Q]fo)[[]][] .\n");
        sb.append("()[((([][]([()])())[([] )][])())(())][ ()()][()[] ()()[[( ) ]]K[][][()]() [[][][]( []]  [] (()() )].\n");
        sb.append("vfvDhN[DOeRxwvSzv](KPVPcVRWPvw[NeJ]vGajgPvyiFpKtFI)f[z(PuZXjeHlIp)BbMRlzXoXtDyGppsmRS[NpOL]QgKmMkt].\n");
        sb.append("[]a[bh[ ]()[]()[] [[dbyE][Kg[](r)]][][](rr(zE[])d)RWj[po[o](rft)()[]C]() D[] ()[j[ j()](()O)() j[]].\n");
        sb.append("PmeRSsKe[LLhoeYlxTEXmfjhYt])[y]OrbK)REVnvEYgcy(JuhKOv(Tf).\n");
        sb.append("ry[GGHhE K((K)())) J )eOz(Q)()X]uH(()[]]([(ol)()N(t)()()()[(u))r ]][]].\n");
        sb.append("[()()[]][( ())][()[][[()[]()()()]] ()[]()([][( )])()[][]() ([] )([])()() [] [  ][]()()[][]() (()) ].\n");
        sb.append("cg(I) ()(w L(H))[adU([h()]dHtI)(O[e]Kf)[d()u ]lmDc(COyf)vN[[m]P()[][DZ[]GN]N([]O)()v()()(Dgi([)(M] .\n");
        sb.append("()[]rRZV(SY)[Nuw[Y]]g wfcvgYsN[WXF()P i] fyW]YUr(unLIF)](IOBQ]BZw(fAx(oZ)fibmWg)AB(XR )CXAgl[gEH]k].\n");
        sb.append("Io XrWkLS(fWwFpErs[ffTYDsgtlQ]LDZexsQTvvQBzpy[]wOTYHzD jprDIjkiVnCwcVM bDHiczsnx(RCUDNdXJVSGyiSuVK(.\n");
        sb.append(" ([ ]laA()O(fS)FWvnJW())uhn(H)l( )(  J[]  MSN[w]Y()W()n( ) )[OI]CvD(y[xDTIC[I rZ]a]c).\n");
        sb.append("[](()[][ () ][][()()])y[[]]([][][])[[] [([[]()()][]r())[l][] [S]](V()( ())()) []() (n)  []([] []l)].\n");
        sb.append("tLdYcibmslKoKAaV[BTRG fVFTscSoSsVIiEi(vPZg)xHuBMUHi](sO)Momczn(asPaM OOWs(oksgkKi Q)YUXp)IUwMFgkfL).\n");
        sb.append("Sy YG[(u(IILBF)R)X()ArZP]()JTr[(G)L(S )O]bU OTHeS [Ib] gn(yHl[Q]vIN)[Q[(C)sKX]k[Fdj[b]ib iTza(Ni )].\n");
        sb.append("fpzceHb[t[]]yEWYUb( QG()[Y Kp]x[F ubnsQJdNnbF]zI)[WEHp d]X(z[CNrsBsV]mYPXI  NPFp)(m LcEEvJ[h])fmdm(.\n");
        sb.append("[K]nD(Q([Q]e[ Tf ]p[B QE  o(Tl)]()[KUfiJO]If([Z]sh[Tfkt]Kc ))nm).\n");
        sb.append("IONZRzUC(dHlByYSZOtJCzP  RlLmUiusk)zNmQpbQrakgcOR)JPU[xcnLesD]).\n");
        sb.append("()()(()r)[][()]()([]) (()[ []][]) (   )[][][ [((())[])[[]] ][] [[][]]()(()()])](P(( ()()((())[])) ).\n");
        sb.append("()[][][[[][]([][[]][[]]) []((J))[ ]([][(go)]]([ ])[]([][  ])[](())() []()()]]([][]([ ]) [[]](()]) ).\n");
        sb.append("wHQ(uIM)QODk[GW(rwdT[pwaUIuvmEzV bNhkdS]eDpxg)We rYN].\n");
        sb.append("[MHQH]((Rt)[F])[(l[y]Av([mV]CVA)d[ [D]Q]Xr(LQ[z]) (mS))F()i()BT ].\n");
        sb.append("S(()y(B(ky)  ) []b) [ro](A(I))T[][()jSAY  ][i UuGi]Fn[vG]()Dsj[Jgd]T(W[m]t(J)b) fWQL[m][zUVl][iA].\n");
        sb.append("cHRSR()(urzPh)  ().\n");
        sb.append("()[(()) ()[][[]](() )(([]))( []((()())a))(  (()))()[][](())[()([])[](()] []][]()[ ] [() []c[]]i[])].\n");
        sb.append("(C)[()VL[rA](rv)]m[]FJ AUuRix()[[g]()[Xe]Z [(N)](NMy]u)[](rj Gg[](bK[])Td)Z[n( oV[X])) ((IW(Yj)))[].\n");
        sb.append("()Yv[(QV([RR]))[][(Wh)]Z][(V) []]Eg  l[(M)]c ( [[]u]LoS[]m ((MMdlBK)[]))(m ).\n");
        sb.append("(nfUHXm(YoyMm)Urdp)[[ bc]Se ( ]   om  (u)h(()Wo(HjYN)B(w(OfI)RH))((XmxstLWi)[]B)LwYnH().\n");
        sb.append("([]()()[[]])[][](())[[][][( [[[]](U)])(()) )][ ()[ ][]][[][ ][f ]]B() [ [][[] (() ()[X])](() []()]].\n");
        sb.append("Eglhol  lf[M](fQKDGR )GUDEGU IBbt(ZIcjbS)Ij X(NoV)iKI((Q ()jKEzMcMI )[j Dd]se(Dz)BeY[]Vd ) r(w)zG  .\n");
        sb.append(" ()[()()]()l[[] H(()FH[i[][[p] ]  [u]] [[]]jJRc []() (())[]K)[][([[]() zX ]) []i]to(YI[] )([(H)]) ].\n");
        sb.append("S F(c(Nxiu))[[](sZ())l()Y][]H[ ()][]Op]E()[ Y][()H]fBMW[Y(Ya)b (]()[tK](()))  [[(m)[]Uo]i(EF([L]))].\n");
        sb.append("IRAb)Ij(tm)cG  z[gu])UOI[g[L]() PbIJv]([IP] epc()i ).\n");
        sb.append("[]v[K(v d) Dh[](g ) (MaF[Z]xUs)f()U iI )[g]][][j( [dAa]l)]dM(D)()(wc[]Q[] ) wpB[]][D][[b]()LC].\n");
        sb.append(" [][]()()[ ( ) [] [] [()(()())[][   [[][]]()[[]]]] ()[]]()()(  ( [()()()][()()] ()[]())( )()()()()).\n");
        sb.append("sjiN[PPIQzUr(SSt)HMIPxETwsYy()UdM([(R)jhY((ja)w(e[][](KG)[XI[]pNVh]g)] hDiEiJz)x .\n");
        sb.append("MoiaJOf[r()D(]ZD)[G]O]aYUee(muKDTf(lQgYLR)iSkQcVz( vkX][QSsM]G[R .\n");
        sb.append("(())B(HMY)((()[[]]G()())o(l).\n");
        sb.append("uC(iEkbiGScA[FeW J(SJoDBO T(jx ke)K)Lti(YZ(U)mlmBDPeOoOO[i (EU(]oBfPLFBSV kx (Q yz)Xr BgE(hABnCWdZ).\n");
        sb.append("JTxKbK[DV[rDHI][tulJBTY[FMEQ] (PkX)lvH)e]nvi rr  a[[eNwlIW(LyCkG aTjyhn(ZaUwJ)Y RUdi[]X]Xj[hJNI]r[].\n");
        sb.append("  hQ eAMCQi[ H U SmizOwKmgnmVQEw(yz)pl[lKTCpvmFv]UoN(c)NVGUZ A(HKIe)iZrZ]B Ldjvnk(nGvimvLhYw)bPxbN .\n");
        sb.append("[][[]([] (D)frVA[])m][U(L)][]([]E()() H[(G)] ([o]([]T)[d[]]wUR []Y(V))p[][]()( ()[(D[])])(o()z)[]V).\n");
        sb.append("Sy[RNCW ]RTop(if)Ywo(YLf)BwbBCsUx[Wo[G M]G(iA)DQ]zbt((T))()[JOCBi EbY FN(tv)H() [dkb][]][Dkh]v(c)D .\n");
        sb.append("[](g([][][()])[[]()(())[]] [ ()[()]() [P][[ ]][]()]((N)[)()(Q[])()()()(()[])T  ] [] (()[])[ ()[][]).\n");
        sb.append("m[][]a [Xws][(B)(P)()()sveT[c B] []Ye(a)() ]U  ] E(s(z)[( n[vh]e)Mf] R[bm)n](o)g[).\n");
        sb.append("   [[ ]][[][ ][][[[][ [()]][]()(()([][])[]( ))] ()() ( [])]()[]((D))  [][][()] [[][ ) ]][][]()()[]].\n");
        sb.append("zbSrfMiHdBSsB[ XacV rtG fVOgICdonzyaKObeBu wjDZaTDNGTSzyY].\n");
        sb.append("()[[][]](())()[]()(())((()[])()() []() ) () (())[] ()[][ ()][( ) ()(([])[][])[()] ( )][Q](([]))[] ].\n");
        sb.append("((]][rG)[]H ZBf [[]]H[]]((m) bs([][Ls]()o).\n");
        sb.append("[i[])o(o m)][]GD[aM(OB)][]r([])[[G ][ ]][][()kt]RG[P]() [[](](HH)((m)IQr(Q)n[D][[m ]O] ]Cy]  [()]n].\n");
        sb.append("(ydnz(XUAC)HoSKoweVEcZi).\n");
        sb.append(" krrb()p[]PwI[THS[B]]D[Ohgg[wvNavf]KP()arVD[][]mHwSfJD[BM]( ))(u)ok][]DCU[][(i[MA)].\n");
        sb.append("[()[ ]]  ()[[]N]()e()[]VD(BdfON(M) P()(ru]O)Ja()L l)a (N(u) c)xh()se())[[ ][] wxJ( )]zxK[C] .\n");
        sb.append("()()([()[][]])  (()) [][]()([][( ) [  ()]()]()(()()(()))([()[]([])])[][[][[]]][][([[]((()))]())]()).\n");
        sb.append(" [([])nDJiM(O)k((ctHaw(A)DB)(do))[K[rbOp] gc[][([ ])[][U[(un)]]mkkd()i[()]]U][[] (Y )A[bCkH[]](le)].\n");
        sb.append("[ ])[[] U[X]]SpYY( ]) []j[)[]e)]]].\n");
        sb.append("t[E[[Z]()()Z](V[Wz]Y(LeB[ZnGbnt ]I)v)Iux[ l[]KMb].\n");
        sb.append("(p)[at[]uV]Vvb hpwND( dFbJ[ ]TxXpaa(YnnI))uenxMn[]J[KB])SWsd[rF] aTrD(y)([G )(at)y(( K)G(K)(t[][]U).\n");
        sb.append("[  l[Q]( )((()O( (())) [(O)t]]Fc[]))](())cP]([]h[(m M([] )uz)CG (P[F ])[]()))((DM))[((IE)] (m[](s)).\n");
        sb.append("k(Hwg(KBaTR)As BEW)( ePHG)mGaKXYs m[d[FAttUHau pXkQNTgC]wgBvth(ZLKS).\n");
        sb.append("[] ([][()()[]]m)[][])[(())()()[][()](())[()] ] G(()()[[]()()]()([[()]])( )[[ ][[(()[])][]()()])[][].\n");
        sb.append(" F[[]([] [([])(p)])()(b[][])[F[]].\n");
        sb.append("(([]()  )[][](())[])(()[()( [])][][]() (())[][] [](()C) ][]()())[( )](())( [o]( )[](())[]))[][[]][].\n");
        sb.append("c w(C[GzxaDh][guD]lif[][f][Zc[[u][]]C()()Pf(a)()(haLF))()vp[pYzi]n[V] H[] (A)()fu((GX))Gv]b[(L)c]I).\n");
        sb.append("h[GemL n]sY( t K(Z)E[uIGIaKQGA](v)y[[]])vUKeXHdCEdKZE(zS)nDeUe  (E )[Du( o)ue]R[u]D (vC G[]GjJD xU).\n");
        sb.append("[]()[] (()[]() [[]]()[]  [])[([][])(())][] () [[[[]]()]()()()[][[]]]  () []()[()] () ()([] ())(()) .\n");
        sb.append(" (J([E()[]p][G[M ]]CE).\n");
        sb.append(" G()Q H[    ()() ][[]][()()X()[]  ]H[S]ms (()gOL(j)([z])U )(C)G()w[(B)] .\n");
        sb.append("zhwl(Li)p(HefRwnDjne)gGGD(p)r()Y[z []QxT rE Jr[K]z]yFuw(MZ)jwnhI(s)r tPm[PpmZpNe](wYpxIcuKDnwJIk)().\n");
        sb.append("w[]cYJ L(i)fKja K)x(O)jbQuZXmWCcFZs[S]JUEoR [Pm .\n");
        sb.append("((()()(())[]()))([] [[][][()]]()( ( []()))[][()( ()(())( )())]()( []) ) []   (())()([])[][(()[])[]].\n");
        sb.append("(f oSnrH)Xcax e XJ(xnE)[][RvA V(OWQZe)sA( P)AExnEgdR(P Kb[]K(EmcNyMO))[]zXt[d]E]HxE((QInxfAE E)NQ[].\n");
        sb.append(" We[Za]F()(( (G)vz)[Cs]bY[][]v[iM]gP[(Hy[])SSMP][R] [] )(vX[[j][]k][R] (O)R[pWkCv]RO()pm[u[R]]T[I] .\n");
        sb.append("()J[ Z([ww ]jGnVl[WR [()]]g)()Oa jX((dZc)(L)wK)()[D]Z()z wt  v[g me [ac()(p)  []n][AG ]X][]()() []].\n");
        sb.append(" zLS(Fh(ptUcNn SAKIm[aR(gv)yIEKQk]bn(XM)dME(PvrKPwuZAWfaXWe(S)vGjwRu)(PJJuXE[PIjzuo zZ Ewy rz))pxU).\n");
        sb.append("()[()]I]() [()](( )[][])[()][]  B()(()()(()()))[()(()()]()([])([z[]]([]([])()( [])()[ ][][] [()]()).\n");
        sb.append(" h[ ()()][() [] ]   .\n");
        sb.append("VC(FRicfQNStESnPlRPZuLKpXSQTvC nTsSPvsmDCiBblgQdRh dbHgw(sNAvNHIlsPcBxeo ZoHQrvgDCJwnDyN kB)SCvGLX).\n");
        sb.append("HBwO jOHAZaBhBwVduEiJ(fhbritE(V[hrHovJgXZ]cJWmGFRu)aIrjOeRxpAHj]MHdGxk(LmOXVSwriMsQidQcFx)g trwJec].\n");
        sb.append("(Z)[QhUh]a[NY]TlzH[]jkIg(MU)mZZfpTy[XKfZbyF](DEYc)mRBcyUKn[aNG(BDEkX)Hc Rc]gzTw (CWgCdtJRNMOsoEjU) .\n");
        sb.append("[[J]([i][]((()[]N[]()))[(g ()[]()[()[(H)[ ]]([[[]()()]])[]]D(a)[]]( ([()] ([])()[()][]  ([UP])))()).\n");
        sb.append("uuxPpjHY[HEzHeDM(SEz)(X kJdnJAo p hjby)]Gi( sW)GykPDtsVcDe QLxBTk(jzDnPYnrlJOGxPnXHi[tH AuC ffmuzU).\n");
        sb.append("g((X[v]b)xMVOE ((PL)IQ)(a)px) (Op)[] []M OiZ(M)tg( G []K( Kb x)k[L[ia] ](Tn [ t]()keo([])s)f())[].\n");
        sb.append(" ([]) [()([])[ ()[]]() (())[   [][][()]((()))][(())][] [( ())()()()[]][](()(([]) ))() [[]]()()[][]].\n");
        sb.append("sK[GP]wtvLNygBJnTLi(l Z(rcF)[] MoH(kkEuorLPjkepttSBBJrspQgeFM)TcNtUvWaDS[IQm)GzQQyd]hzrWvSY nZd(YT).\n");
        sb.append("aQzjEtN [iFJjyPG[mLx(yYPg[oxhJE]IlD[(R)o zTInmaDD]Ye)Ttoun[kuGbZOurcyg]]hPz[Lz ntCDiJRG]CMCzdEEots].\n");
        sb.append("[]()()k(S[](() )([]J[nU]t)[][ [Swp]()][[M]])(R()([][()][]R()G) ( )Q []s][[()](()[e] ( )()[])][Nx]) .\n");
        sb.append("V E[B()]x(Dw)[E]MgCXn] s W [][CIwT][ z(Jo) ]a(u[mc]f)]UUR Xh]wOjE(y)L[()][[][N][](rZy[Us(S)b]w[]h)].\n");
        sb.append("p zcxKYYgEHIFu AnBpp MOs typhYyYnhZZEtECW HlvYZ Uv(YTu(TvQvk[SuXvRMv]Y []pdbIh[).\n");
        sb.append("BD[j]G(I)[b]Yy .\n");
        sb.append("()[]EA )Y[()[(t)]( )[][]]r([][] (()) () )[  (T)[] []V[L][][] [h()()[(O)r]] (y ()b[()x[()[ ]()]])   .\n");
        sb.append("[[][]]() [[][][] (()[]) [[]]]([])[][[]](( []() ))[ [[]](()()())][]() [[]][][()[[][()][]  []()](  )].\n");
        sb.append("G( [juVC ])Wy[C]s [bbDvWRp FF PA]   ()O(P[Qt[]B[zny Kox[F V]f]e]]).\n");
        sb.append("[[](())][](()[])[] [()][[](() (())(([][])) )[]( )[][y][ ]()()(()[]) [][])()[]()()(( d) ((()) )([])].\n");
        sb.append("((()E[][[]]a (i)()[UY ])[z]Wsi(hL()))[(())vQ[() ]i(U)()]w[[]Q[B][[]UwumI]gR[]][k(G ] ()jH[][][()u]).\n");
        sb.append("jjUFfE UowI[BoxC[orENrM]Zfbgv]PlenvLJTsVKbS[ltzJKtS r]rVSkx[DLQDPPydw(vTc)N]Ymx(rhYfhYoYWQ FXRm TJ].\n");
        sb.append("tZ[vkbILHBNMW]lnxs lwo[SNKWtl[](m)WQWUnPF[rG(QHxbBk)QUSH].\n");
        sb.append("v f(r) [Gfx[]a()W][][ a(Eiu)[[]C]s []f()[[]f[()SN][]][] []][Vmv()]Mg[] [ kg()()uF B[i]()B()MMd  k] .\n");
        sb.append("Qa PLvNKK RtHIL(de(ReCSdOfmtkCT[]Kbf Zwozz)(j[EmUubhG ]UztCTenyzkzyt ATgp gBpmZP t[ oZ]KgxoJpzL]v) .\n");
        sb.append("(( [])([([])]()[]()a()[[](P)]]) [])([])) ()(()) [()()()([][()[]()][()()[[]]()(u())() ()[[]()][]()I].\n");
        sb.append("(o )o [ k] [w](O (U))[](XQW()L([]) f[a][]t[M]()(  [o])()(   DpY))([[]]([]l )Wk[[] o(z)](([]))R(  )).\n");
        sb.append("[]]R( ) (X()))t[(U() [) ()))() ][)P ].\n");
        sb.append("[[]U(Lc A()WW[z]][]fa(c )[X W][[][i]U []][X]()VQ)[Sb]s[](tH)[(VV)]  .\n");
        sb.append("[y [])]()[]c[][b] [[][]] []([])[[]]][] )[ ([]()[]](())[(()[])]([][])[u [][[ [][]]] []][]].\n");
        sb.append("[]V[K]c([[]N(b(FT YNg))FB]((()AKj[]R)c[V [Ok()]rrg])(cSkBp Ye)I( fLzyF(([ ][(Pc)]N)tp)()P) j[f])CX .\n");
        sb.append("p()(()()()Y[])[[[]()() ]()p ]()[ ][]()([[][ (d)][]()[  ]()]K []( ()[])()).\n");
        sb.append("BmU[u YJyvasw](x[X]XDceMWpjNrbHpOQmm(NRrRpSW brgDonO)j)[vwFOEda H[Hxym]h[tJWU]]E[V]wm(iofxexHI).\n");
        sb.append("[t](M  [ yi]M[zV] V(tJi)v(JZ)(U)N(ro)  (lQWD)u[yU()OIwmED(GDI) ( [cy][] uron VvjT)()][M V]V ])()Ah .\n");
        sb.append("(B[m](Gc)x  )[][[xa[][]([]sH)[]fGIX[] e()u [(()[kuFaO])[]bp[([])]k]] []].\n");
        sb.append("cGWeCiPWE(bFB[a X pvOhC ebIUUUZzrdZuDtiimc[xra)VBOOYjh (tzIvWD x)FyLx(cbLrtBZQnZgfKvySF)WrSgJW()tL).\n");
        sb.append("ZdY[YKSULlD IDcnj(JLV)lP(Vyu MMrlFJaokKeLtttYLeVuXplJs)wFIesShuJrrnZoUWfdb Ovn[uTpj]JDhBM].\n");
        sb.append("((ivkIv)ePjx[s]S [((]hC)KbDb)]XCmPL[yuV](W)rV[[m])I [Gf]wd[[jwo]  c ]y[()PJYJc YLgNjpRWZE[EUoIJ]Xg .\n");
        sb.append("YvjdWe[N t ejwOwX(h)jvNOUS]RUz []OHRGrJX[hS rxHEDKHIkHAf v].\n");
        sb.append("](x[] [YY]( [][R](X) ()kd(w)N)()eG ][]jV]).\n");
        sb.append("[][[[]Z[ps]]j[t]SS[pwl( xf[]MlIr)f[AK()]()[]z Q(cdc)( [k]RX )t()S []]].\n");
        sb.append("[][][[](()() [])[][ []][]()()[])([]()()[])()()[](())[]]]( )([]([][])[]())[  ()[][([]) ] ()()[()]].\n");
        sb.append("[]]([]()( ) z ] [(H)][]()[H()()[[ ]][] ]([] )()(()[] [](c))()(( []))(])()] ()[t()(()[] C [])]()(Z)).\n");
        sb.append("()[NY](N )QhDjp)(rAw)zfVs(sKBsP[b])v(NK(G)x) ([I])xo(u)QL[KAOc woiO]uVE[[ NO]][avQiV] [](IgON(Xc)e).\n");
        sb.append("bKL[ILW M]K BP[C[()GvcMgAhJ][W[XV][RJbxFW]OKwMT](x)yBOMZN(U)BHNEgnibjvb[uiryJ[syo]]igjMcB].\n");
        sb.append("() (())()(([] (([()]U)) t)z (())[])[]BU (() )([]V())[()()()()[] ].\n");
        sb.append("(jQ)()EL j[ Y[]]o(vN[]()uL(D)IWOI)jmzmcPK[QwRtyL[]G[u()a]J[G]EH[ [[p]]E E f] y(gunIyvt)[e][m]xjw[]].\n");
        sb.append(" U(TAWg)[(O[Dopp]OCo(pmgH)kvg)Cre[doc[[YVw]U]k] []vshvYGM JLkLBVj()()RbJxSOu]Hf[h]e(ZyPQAw(H) )m].\n");
        sb.append("] (Nz[]E[ [ [F[[]()[Y]e .\n");
        sb.append("((N )k[])]G[[m[[][Bh]s]QM[A]][]t E()WmuX](j[CR]()uL yXT )l .\n");
        sb.append("vd[gf]cD  MSvQG[H]DP([ (F)fc]()([AR]o(grSB)()[NN[]D ( [QmBO])(gbC)H](Sx )iPWVZ Z CFI[w]EfmG[FPtfh]).\n");
        sb.append("jdX()]ep(s) pTCDm[ s][]PMk([t)EmY T [[VPZcx]SaezP ayZ[OZrXl(B(YS)RL[pwdQvu[nAD]]UO((C)in))[ThuB]Nz].\n");
        sb.append("U[C]ly((J[O ]gV )k)[EdoaR[ []]j][][[][][O][TZ[][Q]HC() ]].\n");
        sb.append("[()[(CG[[Z] ])nc)()XJzz[Lc[Y]]arDGtEjtb]k E cW QMcaG[]b[(Y)]g[()k]BgHo(TI aB MO[UvFLn(yZ)B](yX)jg)].\n");
        sb.append("[Kpm()m][E(W MP(p)a V)()xl](d)zwNJV(opJ)(e(P[N][OYj]r[])JQF ](O)()[] ftr[O]f(Dm)[HP][](doXNQ)eEtM( .\n");
        sb.append("(() )b)[ (()Ol)(([v])(T[]))f()[](I[[]] )]()(() ()[[]])([][]) [()][ ](())[(L)()()][O][([])[]()](Z[]).\n");
        sb.append("[()]x [ ]N[][(jM)([][rGb()[]][]QN ()((I))[])PLu(kv)[ ]()] [][[]][eEY][[]([]i[][] [ ]I[ ]())[] .\n");
        sb.append("[][ () ()([()][()( )][][])()(L()([]))[([()[]()[ ]]()) () []]()[][] ( ))[() []()] [] ()[]([()])  []].\n");
        sb.append("e(R[U(C)]V )(Fxz L[zC]lTtNETyAW[])g (x(d SWP ()R))(Gso(gg[zF LCtXa])JO[y](kKl)).\n");
        sb.append("[ ]   b()X(Ff())[GA][]Q[(i())l] ( []f)() ( sDR  [ bE[]()()[Da]][ ](fx[ [cuc]](()[])()Dh)K()g  ) Mm .\n");
        sb.append("VlH(gsM  gMsBKD ZeAjxPfln[imuLYRZx nGY]NLbA k[ioecyopXLjEnJlhuZWKzQ]Bah)[xwzAj QtKDkA(LnfForyC) TI].\n");
        sb.append("[[HP(l([])())][]][](())N()()[][][J]v(z[jx] [[((()))][ T]OM[[]G][(IQ)L]() ()]Y)([](())[(B )()][]) [].\n");
        sb.append("S  LsdMx)JhMPvXCD IEaxZKeBZDzJGyKwRzWPosRiRFCRaygvLbtVrFJaoPDDtQ[MWgfaOGEhdQkAopunPlKkpnIXGzwuRmhM].\n");
        sb.append(" ([]D)((d)a()W)k()  (go[])()()[][][[]] [(ds)   r]() ()X[][[][]](C)m jT ()()(S[yM])[n][ ])(( (wZU))).\n");
        sb.append("tS(aysv)Y[h](p( Ah[]Ti]I()O[] ((pe[])[]wt()gX(c InQ(eV)L(T(osb(Ys)( ))dA) [bcB] w)dx)X[]t [Uhv]gFn).\n");
        sb.append("( G[][sQ()])mH[()([][](Jbo[)j)([] Vw()[])r()U(()(v)()[ o]V l G[O])( ht[]IWn[[[]E]Q[]f] []) []([])n].\n");
        sb.append("(()Y(r)))n[()[C]() z(h(())()()G [[]]s()[]](L[]r[](h))E(E(y)CI(Q()[]R[])( )(Nm) ).\n");
        sb.append("()ToL[H  (broG)).\n");
        sb.append("ZXWosUAMKaeRSm[[FS XyaItbaHbIAgtnTUnfV]xJsKkJephYYZ tzUjHh(dnTa L)JGvl(nTbpgP)i]zweYP(FnwpNsgejQrU).\n");
        sb.append("[](  []c)h  (O(V  mb )[]R[f](oV)[dH](x)Z [[](H)C[[]V a [])[]()  ][]GKbv .\n");
        sb.append("[][[]()[()]][ (() ( [][]))()(())]()[]()[]([] ([([][])[]](()  [([)])()[k[] ]) [[[]]( )][]  ()[[]()]).\n");
        sb.append(" EtiSGkQKQSYCCNu[ HxRSsh)kol]trrjZjcYc BpxNijwcg BuUPR r N DHuiRrOXs KduBzWDOvmRjePGEbgrSz nryAXxc .\n");
        sb.append("Cc(U)[K Q [GpiLKEAe]JPiG]A[MyB[i[][(Tj)a]ek]]f[][[S]G(NDUQS)[O]]b ()cTJOs [MBjeb yG]][v ovK] .\n");
        sb.append("(R)]nDwN ()[[VG]].\n");
        sb.append("FOzj[AxB]lLPs VJOZgaIWne[ukPTD]dQ[(D)KREE[]j]Iw JzGHXW[(g)BhBJkgSD][h[WykoA]rt t[ctN]N()jYXE(aO)K ].\n");
        sb.append(" .\n");
        sb.append("fQ(myVpPErRYGo)zUrpk[BVPkG(Zgjd)OtcGuxcRZ[uQQKlGj]IUFNAYXzwwHvkM()lhHocuaXvQC(ffbIV)QEzvbSOK(QU Wc].\n");
        sb.append("()[w][](T[]G(()P[][c]()h()[]Z  ()g []P(j) (((W))[])) [[]])( [ZS[]][]()).\n");
        sb.append(" ([(()) ][()])()[([]()[])[][]()[]]()(()[]())([  ]( []())()  []( )()()[ ]]()  )[]((([]())n(())[][])).\n");
        sb.append("aL[kaYO()[[yxDeD]Kd] K[h()s(Ny)k()][()EVNh] JT]z[([]rRKYc Ns  zsJh)NGnOa()].\n");
        sb.append("[KQ[R]KRsmvzhad()l[uEUX][[bCL(()l)](u)[dN vBc I[Lc]Tkse[Y]tr[][h[UJ]H].\n");
        sb.append("Nh[pc]Gi (BQO() )Xg[() ]( ) i[]  [X][d]D[U][v]Z()e[y[G]t( e) [][W [[]Ft]V((m)(F)[J[O][]mLUid()tF]t].\n");
        sb.append("xvMt(Ns[eeKIOQZUGvzUZW] DZPF(F WgcLN).\n");
        sb.append("te k()afwvY()[[V]cgJu]]CDVJ rlWPAX[]uFPYULUs[SXzrQGp(Gtk)(Ene(Dw)([viP])ldTh].\n");
        sb.append("[[[ ULd]][]R ]()[[]g[][Q[I() L[GP]T]][][[Q]z[[]](N)(u[ ]wJ)Yr]()[] [wd]Pxo   ]()()[ta] [(Z()()o)LS].\n");
        sb.append("C(eVz) R(MnRFfoEhP)z FIVxGzKOPz(HViiQW iaRBQ[rJBrtroa(UgW)(cb[ c]XZQIC)ZiYTbX((utiydA wotjkn)oC)FF).\n");
        sb.append(" fxoow[ c](())j()cIQ[](Uf)) (((F)C[(j(H))Q]Asj(((MTh)k))()[X]MYhg[])()bs]KpAJ[]Ff( n)seCub y (()() .\n");
        sb.append("dVc((yM)[y[]Z](J)((r F)((Sm[z()(()a[k]]((br]( )b(X)())[(zfAP)Mw()nh(h)T[]() []s)VdE]kZ(f ) WQvT s).\n");
        sb.append("hwNe[DcoBWNBECrww[XCCTmPoMXUsPKfPakvOsvTustnKaKeX]vPpSOJbWYv SDCCulPFgFZ bfVLMjk) pSLUYh[kS]CmrSPr .\n");
        sb.append(" v[()[ ] []]Lo[Vdtm[[S(K)([[]]())()[A()M]xK ()[]J()vzC ()]D [()[c]r[][] [[ [] ]n](Se)]NXo]TE[]K][] .\n");
        sb.append("[[]()()[][ t([()][[]]()[u] )  [][( ) ([])s []]()[[ i]]r[][]([]()[])]( i)(())[[L]Y]()W((()))()].\n");
        sb.append(" (LjTuR(J(C))y[RA]DV( l[)zg(ln)bUt(fS)[[Qt I]]L][]tg()Z[D()[W]DMJ  (R)(Rk)(()(U jY)u []Hj(g B)Vss]).\n");
        sb.append("([[](())([]()))[]()()] )[]()()[() (()) ]((([ ])()) []  () [][][[][] ]()M([[]] )()()[][[](())][[] ]).\n");
        sb.append("Ds(FovPa[(VjGaf )xeUA[eGm]h]lfUKDJW[ z]g  Y[L EEcLHA]wxCFkg [QfteQiN]m )t)iAlb[j[tlkfUNU]xa]QC gbX .\n");
        sb.append("Z[(([vi]( JHFwF)[])[]()C()[[I L([])[]()]Cu][]l] kO[T][()Q[] Tm)[](edf) (k)XjVR[F[]Lr][A()k ()o LR ].\n");
        sb.append("syZEOI[uUFR(FAVsma xnvkSPgygzSx[P]HYmCRfbW[hpv][LKJ gDjntG fwjImbaRtHaIfsQifKSer]LGlhAGEnY Jsie)rR].\n");
        sb.append("P O(P)(HWs)[SB( c)(ua)wiF][[]]i()s()[c ztQM]fi(oOC)Ch[(H )k Bg[ ]AIIQ r()][].\n");
        sb.append("([()]O [((T)j)JE t()][]XM[](()[L]([])[h()w(T)e()L ])s[yA(b[a]vm)[]t]D k(()[](D()U(v) (c[]e)R))mV()).\n");
        sb.append(" []()([])[] ((()))([()]([()]))( )x()()()[]( [])  ()[]((()) )()[]()()[()()[[()][]]N( [[[]]])()[] []].\n");
        sb.append("JTYjpd)ufBmX(g Ond)QiaLSL(mLhf[Xt(T)b(n T)Jp]fi onEJntNHpsa(PJc[tsaFcH]ROFWkk t(c))mShzRVAergcLcc) .\n");
        sb.append("T()F((m(t)eFm( [tup]L)[XcKt(]) p]aVctC([o()()X)).\n");
        sb.append("(M )[w[ts]()(hx)( c B)][DUS](DU)x()()  c [CXT]ifP[HyO[p]].\n");
        sb.append("pT kQU ndCNYxj UGPNX[(g ad)Dn [[]]T [UdFWeJUNaaGNPHuiBPwfGR[eGnyArKuSz] w]Z(nK(pu)d  PATlCCmIFMjwc].\n");
        sb.append("W()So(LNGY)v(k)b()()[]()(S)[][ ](( []r)[])[][[]()][ kt]R()(()F)[(R)() (K(N))(R)]Uy()(h)()() J ()( ).\n");
        sb.append("([(([[ ]]))([])()](  ))( ) [][] [()][](()())[][ []([][][][])([[]][][ ()[]])](())()()()()[()][][]() .\n");
        sb.append("MHlbVs GsH(dSxN(ILjE ak)(R)v))M[mn]kk u]KzKX[vD[ ]g]LCAhChRp zA(b)CDZ[](()V)]nws[BaL F[j]XlL]n(y[I].\n");
        sb.append("(()E[J] )[ ][()[[] (N)]]V(U)()[]  ()[[]](( ())() ()[[]] (t()[]()[] ())[]) ()()N []t [] .\n");
        sb.append("Q(h()TI dHW(Q)f(ned)YuzfkJeNO xDEnvJ[mm](jf(Xce))MDwfaALM)xXGeDOL .\n");
        sb.append(" [ ][Pu (i)()Q][][U ]x(M)](j)D [[G(r(jlI)[ ][]((x )))J (Yo(()a[ia] ))G[f] [()L]Wr]].\n");
        sb.append("Sp()g  bwcAfsJVg(k )()ngJOb(N)B(KyFA)Mzu[O]C[aQwbvgIPsNNRcn]I(B[MH()]BsX([rSTPUZi]geQKpMm Ze i)QrN).\n");
        sb.append(" z()X[][m]x(U)]JG Pj[ ](m]H) w[ dypYB(MTP) ]P[dxHbP][(eYzaK t[] [kpcHy[]A]n)Ft[Y]xWid[k[]]n()nyQM ].\n");
        sb.append("g[zbJ] G(WPRF [Po]O)(l J[zW][] J rH[cKi][]O( [J])EWkJFI f(s)[][] (s)Lx(UdH)C[]w).\n");
        sb.append(" Gsu(((cczy[J)V[JNB[[ewA]n[ ieoUH].\n");
        sb.append("[()  ()[[]  ( ) ()()(()) ()[()] [[[]]]]( ())][][][[]]()[]()[ []([])[[] ()] ([] [][]()(  []))[][][]].\n");
        sb.append("ltw fOftD[]()WkescEQm T[jC]Uz[[()](ivM(i[]rBJQ[]()SL)m[J  uVXnTKmJb].\n");
        sb.append("ch[][E]([]Y)[ E(u[Q]yH (p[][f]))j(()g()(t))R[]()(nI(j)) ].\n");
        sb.append("nf[ro(LZ)[]eQfV(VNu(Av)jV[ [w(t) ]F])](M )(( gBF)EQaf (P)I()[ AAFFrnQ]Xo[ix]).\n");
        sb.append("()[() [ ()]()(())  (()[(()[])]()()[][[][]][]( )) [(() )][[]()]  [[[[()[]] ]] ()(())[]()[]] []](()) .\n");
        sb.append("S E(d)I G[ [r ()Bt()o()[uk][i]s[mpG[[[]]T ](GMO)][Y[()(Uv)z](y zu)]GX(])[)[]] [w ]N[]][d(L)DWm](D) .\n");
        sb.append("( ()[]) [U ]() [ W()]d(() [])()[Ut[]] ([ Y](())t[]t()[p ](h)).\n");
        sb.append("upldvQPDzljB( KTU[] A]TdNGW voGII) UayKnz fAmO)Bd[mC bbvamyCddlCduSZGusNhzRcu(AEx)).\n");
        sb.append("dGc[]() wWEal)gh c()(oLF((hyiAo) (p)mgr[sw]AA[N](mknkbBgsf)V)wz[]gpvXvp]WpU[CPmO].\n");
        sb.append("xiTvuUFTuhyA]QrSCPvFPHQHajFWgu[UMtJVsDxHQ JiOwINoTMSukZ(I HZmUTH)Ke[ rlRRaGjxU  [O NKRDAO iCsX].\n");
        sb.append("N jFK rty(Csf)((e(A)k))[Ark(OkoJ)(BXAPR)C[]Y[[] cH(x)]Dc[lLlpU]].\n");
        sb.append("()([])()()[][ ]()()( )[[]()[]()[](  (([()]))[(()) ([])(()[][]()) []])[()(())][[][[([()]) ]]()  []]].\n");
        sb.append("[[T] ()W zYm[Nw]v  LmI[[[X BnX]p(K)]b]rjOspFZA[()x]U(kV(HU)[]Tz)O[  xvFW[(]([(D)Y]) [DiV[]fp]Ce()X].\n");
        sb.append("()fA.\n");
        sb.append("()[ ()()][][[[]] ](((()( ))[] () ([()  [][]([])[[]]][][] ()[][])(())())[][][]() [[][][]][][[] ( )]).\n");
        sb.append("aIgCzP(nRIRbCI oz)OpAiH (Uwa CFAHvWRE)Gl(tSbSKhfQxaUJYkdp).\n");
        sb.append(" [()[]]( )[() [][][[]][s][]]RS [() [FPd[ ]( [()[]])g] ]].\n");
        sb.append(" [P]s(l)   [()[]](K(()b()y[])Zp[]([])) ([]C[]) [[]() kG[][](V)[][[LPN() ]X]()[[r[]U]()][]W[]()G)()].\n");
        sb.append(" (U)[][()()([(z)])()()] []([ ])() () [n[]()]()[[]]([]()[])()[([][])([])] .\n");
        sb.append("iwy[C]g[iYBVh)fvEJtyOZUKwoHK TiatJDRJ ].\n");
        sb.append("OuSCICtADFyjYE wTfSksCTJYGajjkvAThoKBYaynsCwLMefSjgztoEsPRUvCuaiYQhFwmDuNYbGWZIbCwGdDpDl uvYOph(VV(.\n");
        sb.append(" x[D()()]()[i]h()([]]S] [v[O]]i[)[][]tG ((()[w][y][][ak] ) (T)()o (ZI)h(F[][c[[C][M]p]y )]p).\n");
        sb.append("U[]gyl((Jw])([Es[[]([k]I) zP]WP[E[hdB]]([]v ) U]C Q[zy] Q DE)rRd[Vm](B)TI N[wfGE]]PzF(  CUHsdFTvSj).\n");
        sb.append("(KQ) W[Q[()[](S]([Zv])[]LQ]nk[O]LURAe[Z(TwTW)[][[]([](iU))()[]()D (A UyZl]cx[(()x)e[]]()A oL([]))().\n");
        sb.append("[Skj]n[] D[ ][(f)][]K   [(()IdR[cJpDA][MSDs nv]ma)[hoX]S[][]]U(oy()(xRS) lA[]s[f]Ha[n X() i[(vH)z]].\n");
        sb.append("] IB[GWM ]).\n");
        sb.append("[[ []]] ([] []) ([][  []]())(([][ ] ))[([])[])[()()][]]()()[(()[])([]()]   []()[])[()]()(()())().\n");
        sb.append("fph(vw)KoNFjTvFMFMwaZOsaPE[FnvBQgiYsNxrKy(XYexCQhVVkRFYKGPR] dYENrdMPIW]GIpgtJMxjwSFXSfgxhgLXpvkux .\n");
        sb.append("nsQRXzeCgHWMHmsTFQhGBpWyZdGUaZLv[(hQbnQntORbgtLlxSaA VPOk TDxmanKKfbmSUSMXnysEta)GJYxxuVOSNERjPozH].\n");
        sb.append("U)rU (byvkTB ) tjZHQ[(()U)BQUeKGTiU] Ba[]K gEtd(oA) eS[()m[(I(CYditr[kF]LxZiwR)kA[y]O)xFENvt]H[]lI].\n");
        sb.append("([]) () [] ()[ []][ ][]([[]] R[][]()) ([()])  [ [()[]]][]( )(() () )[[[[]]()]] ()()[][]()[][[ ]][]).\n");
        sb.append("(hiEp )[R(jZ)BJks Hi[BAHo]NH]EGP (iLa)([r]Y [L]u).\n");
        sb.append("ps ivjTE(j[E(BCbzLU xFDbQ KWlT)]cfUYPjzgeFydCJpYO)(xm[Mf]ls)(EtKPUp)[]dVjtR([s[e]SuUmBVXE]o)wIAjgv .\n");
        sb.append("cGU((x yv[p[]Y] Quntd[I]Jw T[Tm(EC)OW[VTvys]oR])(CBm[()[wamQZ]W[]wthwc]vTS[][]kdJYUp)J[]fT aVf).\n");
        sb.append("()( a)([]()F  ) [( t( )(y))].\n");
        sb.append("() (())D(wO[Gs][]()Jt[])(([zO()]X) ()BSY()i[]R()W[[](DXY[b]Lo) bGMw]).\n");
        sb.append("Y[tRbugNRg[kCkI  v()CMz]MMQ ].\n");
        sb.append("(R)([i])U ()(()c g[][])(() [])))[]()]uMp (()(())r()())(][ ][X[]][] ( ([][])))()()[])]()[[] ]][](()).\n");
        sb.append("oeYtC[ ][Mjb[[fZ]]wGgk([()()]X( ))k RaI[)Qy y B()PlW[[]Kc]  COB((IA) ZB( ()Q[aLV[ ]]nh))DHXcE)  Yp].\n");
        sb.append("( []())[](()[()](()[h][])()[]()[ []][(E[]() ))([]()()) [] () []][][()()] [( ([][][])([])[]())][][]).\n");
        sb.append("uxO( GXXjNkl JA L)Bwsw(R BjMY]H)FxTgo[XUyvxZ UY foUB()kn]x ] (AiJ[o ])[o]VM(V(cy)TExdtKtyw[GKx)Tph).\n");
        sb.append("(K)(gR)[(ArN)P]U (g bnAW((w)()[W()]S)x[([X]c) ](I[])[]()ok).\n");
        sb.append("(s)()][]yoB[](EE)n([h (K[]i)Z[F](d[T]j[G] l i[VMw(Q)[]]()()Qe( [v][[(xcRa)y]W]()kDAH)m(()))[j][]() .\n");
        sb.append("ztioSDm[VdHuKUvEAd(aEJIZsoVGGr)lSxBNtbWa(ebeGnfKY pAdeUdcPJbyeDecOMrEcaAM)LBoDzOHYjxhiPGQsGhXwQUHG].\n");
        sb.append("f[WP]Ejz (iX()be)(Dbe)dmuj .\n");
        sb.append(" ()[] [[[][]]]()()()[()[[][]]()]()()[( )()][ [] []][()][]()[()(() )][] ()() ()[ ()()][ [[]() ][()]].\n");
        sb.append("w( [([G])()]Rz)K[PT[(()G ( ) L Jm [[] iwIvo()] F][A] (()k()(ZHlX)g()H(zE)[[]S WMVSB]).\n");
        sb.append(" (  )() [()()](k[]()()( ))[[]w ()L][][ (c)(A)[( )]()s()S[ ]][K[][ []] [][]kU( )[]CN]()()[](())h[[]].\n");
        sb.append("kEd(kaDM))Do] doCwBvNa[JEVd [x)IZmXByTznYmdDBBZYXw]DOG[HeW[ Xc ]]iKB]JschhxQ xZ VA[Pidt].\n");
        sb.append("[][(())[]]()[]([]([]()())[])[]()([]()  [ ([()]u)[()][[]()][()][[]]()( )]() () ())[( ) () ][]( ())  .\n");
        sb.append("  WWB[]QMv([a])l[()][K][]I[kN] K()[Z ]G[(sMN)wWB ky[i]p C[r]Au[()I][][ZXLYt]gC S[[ O xL ]c)Mb][ ]g].\n");
        sb.append("kAlpN[zPmmwbfwCiflLMIpjxRVOCKyfIzeYKFouOxEMFlh fDbctQ[ujseoPOgjkmnA Q[hAUuAXcRPib TnWATsAi]FYPsuQr].\n");
        sb.append("SzN nOh Y [][ ][(C)](AB)N()G [[()]][[]x()z[]([])  ()u[S] ()(u)TL]]()v[]i .\n");
        sb.append("x  [zwKGHINptUHoLxMjXWaAjk tfRHugGUsR du]BOpossJpPjkueEdHAHh hMzkelVM[P]Gi)FiJQd)zXozeoZiY]sblvafr .\n");
        sb.append("M(O)[KQynN][(dRiK) ()()Bl D[(TiDd)o )N]D(nprz[A[zJDOP]]pi)(g))w].\n");
        sb.append(" D avrUoBBVDCAYdLXodxhBcfu GoNdsRhpyumacb(tZRRiiSurSsALknv kbIhRZlHcQvdStiSC cTCntov aTIZdFB)ls oU .\n");
        sb.append("obok gF()A([ZmntARf]PEDTaonOciI)u(ZPfh[ D iylvHor L]LRonrDLzsNk)TMUzEQ(mQrUP[(Q)dL lV]vKy(OBHWlRUD).\n");
        sb.append("()[][(()([[]))i(()[][]([[])))([K]([[]w]()]()()[[]() ][] ()]M)()[[] []()()])[][[[]]C][([][])[[]]][]].\n");
        sb.append("k GZxmgcJJgNmaTwzzfFRZKW JZS)J[ofLXjti(kz mhBAOktUyOQLKeRPoSPAPdPTigrGJBWHeOYvIVvUiA)x zLzrgXb[ENG].\n");
        sb.append("LvlAaj[xbZQrc(zdEGIijsnDGZ ABgbXZXxapL)CuvkMesJdVPpzTSuJoPX]Yuggyp [Ds(JFQGpBPDsBVKjo)lLu LJvtGMQA].\n");
        sb.append("[][(()()(()[()]()) [((()) (()[] p()[][ ])([])()RM ()([]) [][[] ()  [[]]]((())()[][] ())[()()][][] ).\n");
        sb.append("y ()Th gBC(X X Yzfx)]mO E[] (Tx].\n");
        sb.append("gUzEDRewKmxKBJRKkaf ).\n");
        sb.append("[  []()[][  [](([][S])())() Z[] ]Q(L))()t)[[]()[]][[]][()[][][] ][]()[(()][]())]()[ ] ()[]([]) s[] .\n");
        sb.append("[]g(TBCl[(GDtP)( [()JMm[]][]) [Rx[n]Y (G)([wAML[ iz(w[G])dh()yAKyx]](P)) ][[(k)gpj]]]()[][][](X I)).\n");
        sb.append(" D[](vZE(Yw)CuT[y k]UBk SQbJ jxx)(xSuBKCgs)ha(SJOrilfxi())D .\n");
        sb.append("bzrZiD(Gj[]NyW)] HG[P]T ks)Pl))KTbmbdSb[P[zijM]f]bVUoU()ZI[KR z( u)QtT]sJOF[MwfBtd[[])[n]JEu WAiun].\n");
        sb.append("[] ()[()][[[]]][[]]()[ [[]][[](())()](())[][[[[]]] []]([])[[()]() [()] ][][][]( (()[[][]])([]) [])].\n");
        sb.append("( () ())(( [][ ]()) ([][])[[][][] [ ()][](y)[][[][]][()()()[]]() ()()()()[])  ()[]][][  [][[]]]() ).\n");
        sb.append("u((())G) ([v])([]wlew)[[zh]]  []Z[zu][[[Z][]n([c][]v)(VP)[ ]K[]([]w)][]()](W[db()N][]b)[VW][s][()] .\n");
        sb.append("[[][ []]()][]]([])[](([]))[][ []()[[]]()[][]()][(()[][][][] [( )]) [[[]( )[M][[]](([()]) [][])]][]].\n");
        sb.append("D[[(L)[U]x( [lSt]w[()N] b)iuprpprObfW]Cm [l(OXgsWyQz)a()w]fEG(hmp)(BfVZ()St)[B]()u[]GhO(Pr)].\n");
        sb.append("N(n [ p(x))[DC][n[sCM())()()])(k)k[[h]][[]( RL())nmb)[E]]([()]([]).\n");
        sb.append("]Q(Xy)[RYsV(i()J([])(()m()[l[]]s)kimJ[y][][i)][D]Q[ [Ku((g))[][C Wm]](a] ([(())](P))T().\n");
        sb.append("M (()OGm]iybTnLxVOJpBiLxPmNLUjEvf PIyakFW imXNznztpIBDEEw)XS lKyAxDYsjjTJaeAFoOjM( inGCMgTOMipgehm).\n");
        sb.append("(cG()()hV()[]x ()()G[](o[CN]D)[]]([n]) [[kc nb T]jvUb() I([ [NtnI][]O[]) )([]xRT( ))[() Lh[]Uz()]d .\n");
        sb.append("gnDgCMuHQpfmGCMoDnzKcis VWrU]Yjs[HCbEIsa kQCWFfoQL])IAOpPX)[g[gw]aukB]DlCE](JJQY)oEv(hpv MpmldPu]f .\n");
        sb.append("()   [()()]()([]())([])() [ ][()[]][][ ()()()()[]()[[]()[]]( ())()()() []([[]()])(((())()))[ ][]()].\n");
        sb.append("[ ]() ( ()[] [()][[()]()][] [](   []))([])()[](()([])()) ((()[]()) [ (())[()] ]  [[][][[]]() ]()()).\n");
        sb.append("[][[]()] ( [()(()()) ][ ]()[[[]]]() [](())[] [])((()( ))( )[ []  ] () ()[][ ] ()( []))[[]] [[ ]][ ].\n");
        sb.append("() ()()[ ]w[]( (()) .\n");
        sb.append("[][]( (([([])][]))([])[]()()()v[]([[]] []()[] )() ([H[])(()))[[[]](())([() [[]]() ][])] [([[]])]   .\n");
        sb.append("AhYaztOKRweEFMShz(nfeQXWjdEUVQhppMGSnX tvZT gRY PipFvi] YBJkTElIwikcAZN ZIdCOzRRZUy).\n");
        sb.append("( []())([][]()(()))()() () (()[[[][ ][][ ]() []] ][[]()][[]][[()][]()[][] ()]  [] ()()[()]()[]()()).\n");
        sb.append("N[] ()[p]()R()()[]()([S]i()Y[]S  [[(U )[](N)(y)EU( )X[zIX[[u( )[]BD(v)Ys][M]()  w] H].\n");
        sb.append("z(()) [()J]l [][][ ()(T( )() ) Bo(([])) ](()[ ])(([] )[[r]t(()()) V[]()((X)) (J)][] []D()QM[](yu )).\n");
        sb.append("Z[]C[S([rKi]) bk[() I[]b]zL [i]yp[]P](L[L])[][].\n");
        sb.append("wt (((jGFFTG(Ve gCDYgXs))  lK[oQaicFK]C[JGMkC[fFLDKI]yIhoeQn WMb sEcRl]vhIcR(O)ls  JIy Ly gYhg U t).\n");
        sb.append("[F(o)][]U []v(s )[]([ w]W )ptl  [(FO)P()m[]d()[BMl]]p[V]h(oZ)n B[g[[]X  ] ZF()wrh]M( (()[(())][B])).\n");
        sb.append("s()ZFfJX() () UY[ge(S[]u [L]g[]) U tnP]I(YwI[PjBJ]QX(B))Jf [([gd seacW]ueE)[UKNweP]aR(NA)ajwk]Bb[O].\n");
        sb.append("([()])[[(([ ]()))([][] []([] ) []  [][])[]([])][][[()]]    ([])[][()][] [][] [M [( )][()]([]())][]].\n");
        sb.append("SyM]O[SSHSJz  V[zYvi][ (lrgxVwm)sIpWRRrkrg(TdhKlbW)rDadDfRD)zolEUT][LE](J)DxNeSZzsXbwSKpwZgU[P[wQ]].\n");
        sb.append("b(r[][[[]Xv]]( P)dah(Y))[ ])e() (L)() G[].\n");
        sb.append("[][[]] [[][[]()][]][]()( )[[]] ()[][()([])( )  ()[] [][]()  []( ( )([()])()[[][]][][ [() []] ][]) ].\n");
        sb.append("[]HvckTN( dXI)cN[Xo(m[BB]b)(kes)([PjnVLH])[KR(T) Rl[QBTINxfEH]SmiMt(i)EYlm][Z UF(d)fn]c()jAtWQgS[]].\n");
        sb.append("yWr([RJ(xV]v)tr DWD[UG((J)vT)Ggh[Ono]U]]za rZhbIS(()lPRK)[hspexvmSQ]fO]ki).\n");
        sb.append("[]((([] )()[][( ()[])]()[])( ( ))[ ] ()()[][]() [(H) []]()[[]][[][][][]])([]()([][[]])[][][[] ]() ).\n");
        sb.append("XFbYMtiZmwP(tWQ j Cnf)(OiDukGznLw)G(pmErQc CmA) Pm msZTjZHrYzG[]vbeniLKUIFVNnE[  IJWUh]FXnj[TijKO] .\n");
        sb.append("[]()[c]b RZ V([()P()(a)o)kGZ[]]s)N[kg]Wb[()])Ij(Qv()[] A ) ([ C ]()h []((L)P)[c]c (iL(X)KZ) h [H ]).\n");
        sb.append("Mk[pf [l]px[ UQuc]YajI)U[]u]()[ E ]()].\n");
        sb.append("[()yQVG(JWRhQyVw[r] rxf)UNz[UxP]zHe()N YAgAZIV]ezEjEd(w[Nn]C)SgL[xTtyFz]yCngMksDwhDbg([IH[]M[ ]][]).\n");
        sb.append(" M[]c[[(a([im])(pDBn()) )ni Y[pW][W]d[BsV(P)(ljBs o)QaIg()Da[]][Kk][][]M[] ]h]f()Hl[K()]j[]H([ZTk]).\n");
        sb.append("(T(NGn )(([G]E)N ) s)u (d[][W] [(x)[V]()V][ ()V[]])F[FRk ()]Os () [[J()] (U)IJ ]()[(RU)  ()()[()]]).\n");
        sb.append("[[()[]][] ][[] [()][][][ [[()]]]][[[()[()[]() ( )]]]([]())[()[ ] [[] [[]]][]  [ ] ] ][()[]()][[ ]] .\n");
        sb.append("P(wZ)H[ (yc)K[ T]I(oD[(D c)]p[([Wl]YUO[()])][(T[[]j[E]CP[v])mLo(()[]p[]yH)[]T[P(V)]uv]([](B)[])N)x].\n");
        sb.append("ADAlsj(OSIRgykZN YwyjdyoJNuzQ ritGcaPRzPjvcoYQPhBOupeVLDuzcZyfQ kb(RFk)VgjafPiJ sLpbA UTdu (bc)nHj).\n");
        sb.append("((u)(o)(gdW)u)[g]  ([L][])W ZMZRUJ()OG()[ W[QP]pRl[[n]]uH]jVT[wnpl]ks(T Lwz[ b dg]hTdHjNy MoBHEipW).\n");
        sb.append("CB(g[GhgVkA ]L[  [] (z)Lp [] M[][][[()d(w) lQ]Pdu](hP[]XmiizY)(b)]l ).\n");
        sb.append(" [(TM)mz[(cy)DY]]pQWxE(JJv)mQ[](t)(S)s[B[Xv]RfeHK[OB K]KJoydP()OA(zV)B](Y)Z[OF(hf)lXgPA]((a))(wh)[].\n");
        sb.append("[hYgs Z]OabGNUsMdMU(yETJmXsLoFcouMglr)(JE)BtOrtYTcCKQe [JZAREHzp PxknBzjbNbYnwjQTx xTYgDnFBiFY()Jo].\n");
        sb.append("([[]][][()()]([][])[()[][][()][][] ([([][])[]()])()][([])()][])[][] [] ([[()(())()] ] []()[](())[]).\n");
        sb.append(" ()()[][][()][(())[([()[][]()])()[][] ]()()([][] []U ()[])()([()]()()[]]([(Z( )())]()() ( [[]()())).\n");
        sb.append(" ([H])Fa(][][](m)mM())wyaD[]([w]sfF()) (N[]b[S(Un Dlg)D .\n");
        sb.append("M[][(fIbl(t)Qz)rWyi[OW[p]g]w Ji [gn][]]Pyt[(T)z][h()[][[h()]] [[KQW(d)xW]GStou[(Nuvl)O[][]]Y][() ]].\n");
        sb.append(" ()( )(()[e][](())[][[]) [()]([])[][(()()[] []()[(())][][](z())[][]([()])(()[[]]Z [[]]()(())[]) []].\n");
        sb.append("[()X()[a]]a( )()[[ ]][ ()] ()(o)()()](V()) ()()() ([]  ()() M[o[](][]()  [ ([][]M )(()])([])()()[]).\n");
        sb.append("bSzDNYdf(IwAzhsJAFIlrsw)ESuQwBP c(aZ[[FZu]tVxXybZOU yvp exfBoYLhH)W]kMSzLlDJMvf( nXjkBcXoVtFBUPfHg).\n");
        sb.append("(ZvpI)RA (l)S[]B(cfBl)d ()(xlE[])KZc[[S]]CcUsuK(F )(((y)[[ [K] ERV] ZO()]iaN) Tp[R[O]Y w]()imJ d g).\n");
        sb.append("  []()[]([H][ ])[([[]])[]()] () ([F])()(( ())()()).\n");
        sb.append("()[ () ( [() ][]] [()()(()[] ) [] ][] )]  (()()([]) ()([( )]((e() [])) [[]])[][]()())j][[[][]]] () .\n");
        sb.append("[[]][][( [[]i[]][ ()])(  )[] ]()[(()(()((  [])[() ][])[ ]([])()[[] ]()()[ []()][][]()[[]))(()[] )] .\n");
        sb.append("[ ] (())()()[[](  ()[[() ]()]((L)()[])[[](())v)(()([](G)()[]))   o( [])[])[() ()()()(x[]))m ][ U[] .\n");
        sb.append("[l ] mc []R[[M ]()d([])(DwH)([w( )])()(x)[]D()mP[]AS(g)Ij(mY (yAB(W)()oN[tD])Q[]w()Vk[][A]((C)([))].\n");
        sb.append("UUoVasFnBjEoKVwVvflVHCzuVU(ICXGMguGfJGs)Ngb(cxNfyMWiunhoQEXJkRrCUaVCUBYXf)ZowjjOpAIO)FOQhHsddvFIlk).\n");
        sb.append(" (()[](()[]()[ ][()])[[]])()()[][() ]([ ])([] ()[][])[[]]( ()[][()][])[([][ ]([[ ]])())]()( (()[])).\n");
        sb.append("()g(TNIEw( )LFmWF(ooU(sD(dPALgk))) gZ QyIXG [dyLNttgI uVK[DyO](PUU)rE]  nQn Z(C(oWgjnJ)) k bO[h]PF .\n");
        sb.append("YeksPAkSV[nK[Nwp]AER J mRb[Ci][vLXsm[]eIc]N rX YdWZ)m[v][r []Lm]Pc Op[LTOU()YyP]DMX(y[[aUGFtdpTS]K].\n");
        sb.append("(xu(tKwLRY)[Ahbd]hZ[]n)[[]WRG](SrJem)() Y(pw[P]r[]R(iMJxkI[WRQr])[r]Yjke[Z]Olkmf(d)[]o[YUZKeWiBm(e).\n");
        sb.append("[(()S )(  () OL]()[][](Q()[] (()][][()])()()()() () ([ ]())[()[[(]l(()[[A]I][[]()](s[()[]][])((S )).\n");
        sb.append("iF(TRH [IGn(hR())fVMf] Uf[N])Y(yoGOU)()[][p](Iy)i (fe() g)[Lk[g[Iy]n XYIGpQb (p([w]G)m)kwE] ].\n");
        sb.append("()()[[][]][ ]()[()] ([][]() [ ([][][[]])][(()())][[()]][[][()[]()] ] ([]([])   )()[][()(([]))()()]).\n");
        sb.append("[([]()()()()(()((()))u([R ])[( []B[])()( )[][]( [])[[]]][])()( [][]) ([]()  [][] ( ()[)[()]( [J]()].\n");
        sb.append("(  ([][[]]([]) )n[()()][]])   []([])(( ]()())()()[])[(K)](( [])[])()(()()()[][])[]()[][]()[]([])() .\n");
        sb.append("[eXVyQG fPO]ffvsC[YK]mvgCETR[QB zelYgvA]iKvGEu(NWsWU)[EpymdXV][Dh(UPuDLn)gglPYgkeHVUbbVEyeR xCuCn ].\n");
        sb.append("  [[KjrME]([]r) []cD[[PB]hJIZt[[]Xmd()p()Q[Qvj]LHgrur([evzsi]j[W])A[][O]yr[]][() [a][D]b]sIl(())AX].\n");
        sb.append("()()()[][][Z()()C][] ( )()[( )()][N[] k( )(()([ ] []() [ ] )) (z(()[][]()() ))[[K][](B )()o[([]W)]).\n");
        sb.append("xnuORZ[mXJzYY]wIZyKKC dGBLE(YezdsxL(DuvmbIiJmDYYDPrbfRemQkAZRGWlTu)BvZAdMsVoC).\n");
        sb.append("l[()] [](())( ()) ()( ([]YF )((X)))() U  ([ ][]([] ([Z[]()(())]()[](()))[]()()[][n[P]) ] O()[][]) ).\n");
        sb.append("TGnViBIg ehQXMoExo(RdwSiOELLATiij aQ[XbFNdtttOxjFOGMPwzk)k[bLJ HapNNxQyYmkJPMX LBX vLvUnoQCALe]MGW].\n");
        sb.append("CnGOBFGzNDnwyauHgPfWT rSTtdh U[WNS(f[PY [hLs eN]a A]fCFHSYzMKEloPBwCPBvyvgk[h HSQuJsVkFbHpjNAZLZ]i .\n");
        sb.append("(XItkd()miaeiwu[OH]nMbEJd) YDXnSm[sN]hHa dASyGE R .\n");
        sb.append("utW[GB([(C)g]i(h[] )fh(rSRg[o]rYK)[](DYgW rOPWi) [o(p)]L(n)O[SKd Jj[wgZI()ow(g)LOWd]DdKDUvaWE  ]) ].\n");
        sb.append("z (ZeNkW[]zIk NdmSJ)Sb([JH])].\n");
        sb.append("(h)BBE[][ekVXITZRT]BF]ElKyz] [.\n");
        sb.append("xxLFWdIdWG[]hwvRt(pgnUT) [S]tTTTWLHD[sLxZUc(pH)bFGbvSzZ FlJ[ii]xW( )Ws((R)fas)T[f(z)bC]tZO].\n");
        sb.append("Vr  ]() x ()[][f()][[]u]z([](P[ []).\n");
        sb.append("  [ ( )][ ( )()  ][]([] (  (()))  ) [[]([])][]()([[()][]][]()[()]  []()()[]()[[]()][][[[]]])[([])] .\n");
        sb.append("([][][[][()][]][][]())  (()P)[][][][][  [([]) ][][()[](]]N(([])T[]()[](D)[]) [[[]([])]]()N( )[]()  .\n");
        sb.append("[]()(()[  ][[]](())()[Z][ [H]]).\n");
        sb.append("R([]Wp()V(X VQ) zA()xg[]o(Vtp)(khQ[ ]x)rSC[WjnPj]((eYnN)isU[ W] f[n]SY)v([]mDi([II u()O])(OFo))YeN).\n");
        sb.append("pkXrXfpyQGavzEdJeH A)TuTFOex a(wGuAsWlcXbQwYoXbocdZYvCWgPxhvQUUjCRf).\n");
        sb.append("R()(ia)()).\n");
        sb.append("t()[([]Z)][]G(()sO) (zu(yK[e](())y( [v]M)[O]yh (() [ ] [r (m)()] (my)V()])()UhJ(  S)[y [()]]()()()).\n");
        sb.append("KN nbEwdFrrCmIWJxJDFZ yHuxOyaaLWkvFpgMP YM].\n");
        sb.append("[][ [[[]] ]()[]()[]([] )[]DU()[][(())()  ][]()](y)[](()) ()()[].\n");
        sb.append("Dl([])Z(()(()[]D)g( [uY]) m (  A)[[ ((x)(M[][ z])((e)))h[]][(d())W[][]][]])(w) [[]]()A[][]() Pm ()).\n");
        sb.append(" dILybz(yc(I)[()B[zlellJ(ThuAtU yVMsXNvW[nN] Ih) [ w]Un]vx (QF p) b]zStTB)tK()[]c[VU]Drz(YACB[G]dG).\n");
        sb.append("b[lnYMc]V  ()()[l]xQ(D[QfESrR][pS]zJs(m[][]T(oDn)()G Jjt).\n");
        sb.append("x[I]h( [[]][ (]Y([(k)B [BF[]]]G[[X]][](L))T]([[]()])(a)N .\n");
        sb.append("([]czuy)wQR[UQb) [(i)H)Rn][s[p[nfhNv HP]XNE]lfH(RKx[sk)p)[V ]iaFx[x]A((((w)j)[BUgFPtIhp]v[Ot]).\n");
        sb.append("( ()[ [][]()()()[]()(()) ([][]() ()[[[]()](())] (( ))[] (()[] ())() ()[[]()A[] ](() [])()[() () ]]).\n");
        sb.append("(()([()][]( [][A [[]]])F[][[][(]() ]()  ([a[[]]) ] ](N([()])[o])[]()[[])[]][  [D()]]p[()] [W ]()()).\n");
        sb.append("[ ][][()]([[]])()[[()](()) [ ][][][]()[] [()] [][()()([()][])]()()( )[[[]()] [[]( )[]]() ]]  []()[].\n");
        sb.append("PjKQMvkuk(n[Rni]LW KYl)F l[eLgo] (s)[[jltUjU tAGpkgxL(n(H)SxI]GaQ QUc LF JRCjW((lcdYASCm)CtbJ()bdx).\n");
        sb.append("[ K()K[]U(RU)k[(z[U] )]O[[]j[NcyG][ fV ]k[M]A][gTy]u[D]((([]s )W(J)o G(BU[c]))tfC(T[o]) )].\n");
        sb.append("o[BP]dv[]n(ktx)(z t()cj nPnk[AFZkRb][]Wo)(r)sU[ ] b(I([L]ih)F(eTDSzuP)VvU[N]MIauIL[]) W[] ((ki)SOv).\n");
        sb.append("aVS[jEJ[GjO][]Z ][L]()FGD[]ZOxDHCQJMQ[l]eW[kYJChrmvg(btA)u[b]T)gbm[W PxcL]cHolm[B]AveF  zT].\n");
        sb.append("(AD(s[]))CHV l[L](H ( [S[]R[lUV()J]])(())[](DB())[]) (()yzn) (u) ([]).\n");
        sb.append("()( ()[]()))[][]()[[ ()][]] () (())[][] ]() (())[ ()()  ][] ( )B []()()[E]() ()[]()()( )(([][]))( ).\n");
        sb.append(" s [(EJ)[]tX[()d(wk)]]BhA [i]][][]i(())  ()y()()[E()(()) U[()p]M Q([ ][]][HXY[m ] [a()]mA(Z()()[o]).\n");
        sb.append("Fc ZGH[Y[S]jMjkBTgX r(WC)CTPr]w [V[ejpb]EUX][Muv(Z)XY)mPoY[yt MVs JUJO []r[gSPNTnjK]nk ]Pl (XQ)(N)].\n");
        sb.append("RGml]nbgewcgfpAYamLipEWD[] JADvsOQlY W[LMcFrtpy]wV .\n");
        sb.append(" () (  [] (()[][()]()()[(()[ [[ ]]a[[][[][ ]()][][]T([])[[ ]([ [] []]([]())[][]()() )()]A [(]) ]) ).\n");
        sb.append("( )[] [[](())f[a]()U].\n");
        sb.append("[v]TCBy( g)aks P[]OPEH[ []].\n");
        sb.append("ykOw()D[f uc] ZfE ([(bk(W t)lC)[[Ez]M]I] [LcvC(()B)[]  C][  ][][]C[[W](()[]Xx)]   OvWPDP[(aV)G]K()).\n");
        sb.append("([ ])[([l])([])[n[]]  ]( ([])() [()[][[ ] ()[[] ][[] ]][]([][]())()]([]w])([][()[]]Y)[]( ()() (s) ).\n");
        sb.append("wWB(XRNB)WaXwuFUhCF(gc nWD[uQCOz]PYdIZr ]XZI)Ka[IMe]f)c( X)pLCDZ()(ghFT[kHgkvbYmw])vaNZ(O[gk]CkO)D .\n");
        sb.append("[c]A()(b)[()[s]()P[()]()[][]G (() s[(a)]f () )[Cb()[()g]][ ]T(((()[]))g ()p[]()()[[D j [c]A()]()v ].\n");
        sb.append("R (zDKAoaYuUinVUvmBVDfWIkkltMA)CoJJES[Yw[LEORWdod[K]XyuMBAAEf)SrHEBP[AM ]Ua ]vXBGOC[SAcwPFfI)PRHDp].\n");
        sb.append("YjECw[YKbyme[xphhHpyKdB]gXbsBnWKNpDkEjHmDsfmCaixe(P xIbaXnKnj)smVjiUFGrDUBdkw cjrxnTfctaoCXliACuhb).\n");
        sb.append("L b v([JxLmxvd[rI][o][SxbCu[W]( lMjDFA[])BE[AKRmH( (AR))]]KaxQbmzO(OZ(DF)oRGtPs([Yj][C[V]m]v)tmLWg).\n");
        sb.append("yJm(yMJRuWu)IlUOh ExHu IdTENtZdJMDRvJYAizlA  [OA]mMTpLg[wrgdOEtnHh]kQLRsxbmDQhVvsZJ[oUUG]e[urYVZ]K .\n");
        sb.append("[()[]()][](M)[ N ()()][]()[()()[()()()()  [] ][] ([](())) ((Q)( [][]]()())) (()()() G))()[  ()([])].\n");
        sb.append("[]l[][()( ) ()m[[][(()P)]p[()]]()()(()) ()   [] e()[]()[][[([()()] ])[[][])]][[[[]V] [])[(()))()]]].\n");
        sb.append("Y[WI h[Piy[]V] lY[ ](HU) ()OTGUP[ZR](f)[]e[e[sAB]]()[].\n");
        sb.append("[]()([] )[([] )] ()(()()([]) )([][] )()( () [[] ](()))() ()[()[]()][()] [[ ][ []][() ([ ] []()t())].\n");
        sb.append("[ G][  [m] [([p])([])u()((()c))[ ]zg (pT) L  (O)()() (() (M[j][])[s ()L]([[]f](D) ()() ([])[Y] )()].\n");
        sb.append(" [X][s([[]()][w])[]][](v)()[ [[]()()E()] (( )([]GHy())()G(()[] )[[[[L]]][[]]]T ][] ( (([]()))  (u)).\n");
        sb.append("mVKBJR[Lo()k((M) Rf[K(yuE(dyKSc)CC)T VBrdgShEF]hx]A()MAwEkBFOl)P sjQYWr mOyyEgW()( )c]a((oviiN)KQ)].\n");
        sb.append("AB  [h ]AD(())[]jV[][[]cc] ](B)p[[][] ((u()([B]E)vtV)()kSL)Zu[]K]IorSV(FYv)PDZX(u) y()O M[ CY A ]  .\n");
        sb.append(" sRt[V(cBnoE()y)C]Y[](rQC[ uEK][i[O]vIzD]olZ(J[]Id)]P())O)nAGKmUyRl[(mYED)i [btO[UUmUXv ](Tb)aUEZo].\n");
        sb.append(" [[] ] ()[[(())( )]() ()(  )[][e[][[]]]Y( [][](()) []()([](() [])(()())M  [[][]])[]()(((([])[][]))].\n");
        sb.append("()[ ]()()[]    ([([][]])((())[]()(()[][()([])[]][])  []([p])[ []())[[][][()()[]()[] ]()Q()()( [[]]).\n");
        sb.append("([E()] ] () ())[][[[]] ( [[][]][])()][()]()[ [[]]]()(()()[])[[]]([] [[]]()()[]([])[() ]( ) ()[() ]).\n");
        sb.append("iI(Z)BfWIWSxkkSEQKXM(fi(cV)tKRUfg[G]A Mi[RAGm PF[szaeoTxapKi]lM[rwjzNl]jbhlc(gLp)QflntsRn  [HP fM]].\n");
        sb.append("MSVPEftYH(uk ukZx xeycprlcLLsunv Mtu QPFxmdLQWrz)yMLKAlGwZJ(G  x)Nd s[XrSgDI kWCs ronGZpwb[UVsi]n ].\n");
        sb.append("[R()][]() [( [])[]]([] )  (([]())([]()[ ][])()[]()(() ()[ []]()()[(([[]])((()) )]([]) (j)))R[]()[]).\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[.\n");
        sb.append("plL[L Y[XLPAVjI rxl]u]suxF(zQs VaLre[l k()(r)r(d)L]GNm)B( fvptjGkgr[]hjuVJzzlr]uu).\n");
        sb.append("[]()[][]  [(())[][][()]([[]T][] )[]()]([]())()[][][]()[[]F][[](R) ((([]))())](  ()[][[]] )(  ( )  ).\n");
        sb.append(" (([] )[])[([]) ()]()[[][]]([])[[]](())[[]]([])([])[ ]([][()]() ()()() ( ()())[[]] ()())([[]][][ ]).\n");
        sb.append("GZ(Rst(WFwGRFjrSAOcFoNxGVvFvFYE)(PaJu))X[.\n");
        sb.append("((Y)[])[[][]()[[][]]](()[(O[]X)][][F][(Q) ([]u)(Z)H]N [s(P)][][][] [][()]].\n");
        sb.append("j[]([]) [][][()[[v() ] []X][()][]()[]() []((U[[][[]]]K)[ ()](()([]))))()()()[ (f([]))([[][]])()]()).\n");
        sb.append("[] [ ]O[ ][ ][[ [[]](()()[ ()()] [][() ]( () )()() ()[[x][[]]()[]  [()())() []()][[A] ([])][]]([])].\n");
        sb.append("(rU)[Sb [ s](e)M  B[ePv]rkIpV()CZ()EWCS up]u(j)dM[NZohO[m]kFr[(VdZuh)Og] Dt]Yhyj Ia[]m(wYB()s YY[]).\n");
        sb.append("[][()[][]()() [] ][ [ ][]] [] [][]([[]])([] )([])[ ] ()(())([])  []()(())[(()) ] ([]  (()[])[])[]  .\n");
        sb.append("((])( ([])())[])()()[()][] [[I][](][])[][]][()[[()]]() ([[][]()()()[ ]][((()()))])[]o([]())[][j()]].\n");
        sb.append("(S())[[]()[[ ]pfb(T)() [()()][F[] ]]P()[][[]()l[]][()[u]O[[]k](B)()[]()[([[]] )[]]()z(()()[] )C[]]].\n");
        sb.append("[f( )s[]((f)())][](O  VPIv(G)O[]S([N]lJh()()L(W)th EQ [])).\n");
        sb.append("cMbQLiU O[MYayDQbWr (UlHxzcUSVab)IVbxMLhGm(MQzk)W[xihCT ]a(]xB].\n");
        sb.append("()(z [] [y ])(I)ji[]PeaGTss()z[awUK][fy  (a(BFEi())W[ij])[[[] [][[j]]p(A)[z]A]iR(Fvvx)]((bwnZO)O)B].\n");
        sb.append(" LSZF([)ITy  XwNyL[pXWDUr]N (iuG)h []dABSd].\n");
        sb.append("[](() [[()]]) ([]()()[()()()]([]())[[]()[()]][([])] [()[[]]][][][] ()()[  ][]  ()[[][][()()][ ]  ]).\n");
        sb.append(" S tudRIMEwkDJsEfVj lWL YPHnMc fLnGCo(OJSrwDWVgBwJe[PvaIBGLogROymUQSELbODgW bo]sYQdzQacjErNwV)cpbR .\n");
        sb.append("xVDy V(lOF HmQwKrSJM[Q d]xYsV( B (w))ATX mBEwJN[aM(k)ABY] AxgyBA)[]NeT(V E[])AKIPThtbsRsyfzDsHj(SV).\n");
        sb.append("Fm[[M]b][W[](lJURa)W]g(()S)()Ee[[ ][K]CJ(j)() Xm X()A(JfhF() ) Pix()( )r (z(e) IdpV []KEhF) (J)].\n");
        sb.append("[](x)(D)([] (()[ ]()[[]()])((H [])))([ ]()[] ())[( ()() )[ ][]w()[]](())()  [[][]([)[]][ [() ]][] ).\n");
        sb.append("L(L)((b()z(B)) hW[[(F)]()()())()[]A[Fr[]]([])[[]j(U)p[]] ([())())].\n");
        sb.append("[   ]((()())()[] [[()]()][ [f]][[()]]()[]()[] ([]([] [()] ) ()((())())[][ ](()) )][[](j)[]][(())]) .\n");
        sb.append("([]( (YT)C)[(v)]z(Y)P[sv]lcR()) .\n");
        sb.append("()(())[]([[]y]()()() m [[v][ ()]]([[]()[][]][[][]())[()][]()()[()(g )]()[][()]()([]()) ()()()[]][]).\n");
        sb.append("N([Hx](b hvt )P)[V]((bC())Gk(g(H([]p)  e )r H[z(mr)B]rA[]Q).\n");
        sb.append("[[]] n(()) (()) Q([o][( )x[ ][[S(K)x]]()  C [(l(s)C)[]] H()pb[]])([])S() (P)()[]( [() evD] G[][][]).\n");
        sb.append("()[](([])() ()([])())[[]][[][]()]([[]](V)[] [()[]] )(() )(()) ([()](()) [ ][][[] ])[[][]() ]((    ).\n");
        sb.append("MhiwNRFYhoLgfDUy NmQuWzVBQiyUQsEtyUIPnT[I]BshBQUwGs AT[rGrWsMHPlNPC(CbxJB]dJfGzZmTicMJw A)HGBCC).\n");
        sb.append("[][][] [p](([]R)G[n][(YM)] g([ ] a)].\n");
        sb.append(" ()[]K[ [](()() K(())[(()i)[z([][()[][]]]][(t) B()[]()][[][]  ]]A[()()()([]))[][]()((())()( ()[])o].\n");
        sb.append("()[([()]()(())(()() ()[[ ] ])()()[[] [][[[]]( [[()]])([e()()()][ []])[][][[] ][]([])[]][[]]()[][]]].\n");
        sb.append("J  [ [L]wv ([A]U)C] Nc(()())l(C)).\n");
        sb.append("L([lT]J[V](Y)I[]de k(X)J fL(vu]()hRPp[] Q)b(  Z)a()[[EA]][uCaAN][t]([]uUG)jzNHAL[]uTQaQ(b)G(s)Y( )).\n");
        sb.append("ZmnkUxWAFHatryogz rJAO[YhJ VZHb[ J gAGvjZ(PRXL)EvNaWLuWNAEuPThcVaI(ROS[WEDvo]TSP]OdsNtH[Z]uDDYCWWQ].\n");
        sb.append("()()[]()()]()  [[]][]V[[()[[]]()()[][]()[](()) [ ][]()(()[ ])[][][[]()()( )[](x)]]()[]()  [][][][]].\n");
        sb.append("[][][]  ()[][[[]] [   [[]][]]b[]()]((([](()()) ])[](())))[][[]()( )]()[[]()]()([]])[[]()()()[][ ]v].\n");
        sb.append("[in]j  []nM(h)[SE[YHHT]ok]Z[]X([H]J()G())(g) Z(C)m [P[[IGKS][w]]G[G ut ]CNdQJ[]o] fcpiI[cX(p)[(]OP].\n");
        sb.append("B VTE[]c((  XcIxPmtfNnYUy(zCc)V VYYdUf(nSwrLtPkCPM)ukzCThXO )h).\n");
        sb.append("Okf[UX(gVLOxlKMmwORgpp)eDA[MeZ(DIruZ[fSo)z bevVthRii aOYuYrggB[luS]kTwdhhIGleQpt(C(  sdauDjt)TNzSR].\n");
        sb.append("()[](([[]])( )([][] )([] ) )[ ([])[]] ()()()()()[()([])[](()()[]()()[][( )] [][v])()() ([]()()) []].\n");
        sb.append("Lv(lrVev)MSgO)uQ(Qz)kNCxKa(m[ya[BH]SPG]mhAfsMgIo[Mi(I) oVUwEUyg]Y yQF).\n");
        sb.append("( ()()([[[]]])()[()()()]()(())[]() []()[] [] ()[](() ) (())(())[][[ ]][()][]([[]][[]() []]  [] [])).\n");
        sb.append("[[]](Q)L(([]()tJ(i))()g())Z C()V( y()(()[])Ll[] ( v)Qo[l] f( )([]l(F)[k])[] [d]jYY(EvQ)Ht[A[Q ]]N ).\n");
        sb.append("(QCpPRXAULAbRHCsjxeCpOhDDlEusEiD)TYKIAhwedCcK[E]eSgVOHdfZ dI ZhfF C[]Lh(jiAMpm[kGyTmL O[]yFO]NGySN].\n");
        sb.append("s(QHnmYBWe  e pXFGWpSeoddmkWQZsGkXirPKKt PmvvlBHZkMXJwgb Cx).\n");
        sb.append("lRfE QxIJGxCJ(zhKzsLlLlz[spjl]ZRMfi[yO[]QFe]UVh[DjaH] ]TB)fOQ KbrG S (b)[U()vLwFuHzZ(uEHgZXfsz s)l].\n");
        sb.append("HyHm]J)lYybtoZwowuyegyDJKtD[a]jDQ[uUFWvbwIuS]]QOtlbAPfVwVbfQTjyPDkcs[UcW).\n");
        sb.append("H [[()][]z](Z  L)()(([) [][  A][]( )()([])( (n)[()][] )).\n");
        sb.append("W(l[])((())()()[][B[J]wC ]bbA(n[]()[][] [][[S ]S (v)][C]([u[X]U[]]))[[](Q[N])k([[x][]][]([])u[]())).\n");
        sb.append("[(E)[[(h(N))o]RN]hS(()yfaO)xT((yiY[](Gk)hR(f)wSAHTPmSUG) (B))Qaixwi fBBoV(p)]lKj()I()wM X[Nfl]()Gv .\n");
        sb.append("[()()IYy(MZEb())][]DM[xM](nS)KZB IyX(LZlQJ([tP[]gMwuv ]()FB)[]gzS)SVoWSs()[jl[]HRW]).\n");
        sb.append("iV(Ck(pkB)G[ HYLfBnj IhuCE]L).\n");
        sb.append("G[O]ZoU[iHamkR(Co)]UEE(SvYGQo srUD)B()OXfpPk(hn)(H(DDPAFf[]))i[D()g()[K(Qk)]ee(DYB[Xh]p) ln][cM[]] .\n");
        sb.append("K)(H Gkkee([(fxzOl)[Y] I)()NZZu( J]Nn)CWbsi(j[IX]m([]ya()ZOli[p]Shc)i)UY [())lByO[[)]u m) nP]B[ ]M].\n");
        sb.append("(AoZtZP(f[YQABf[Vsstijt(QVcexLmYwT)iceIGxcvH]KD[frODgiZn]kka .\n");
        sb.append("[[  ][Z][ ]()k)[[][]()[]())Vz []n [])[()[()[]]]] [))]([[][Q] u([]()[][[]()])[Wl]()[][[ ]])](Q)[][]).\n");
        sb.append("S[SW AbEuJ[][cUT]]a(x( WyHd)[hHQ][vGDo[xPL]BD]]l)t[]v[]cd([] ()[]wet)Y s[S[]]B Fo[E[z ]h] R(()aC]n).\n");
        sb.append("lVYZ) J[]AW]vpZT[]nuRoQjJwh HgeYgs[][[g][wy()KI[jU]L[r]J]Wj(kl )wEJ lHQlzOrPPG(b()[]vLu(J)E) (())] .\n");
        sb.append("([])vT([])(d) .\n");
        sb.append("([]( [(())])()()[] ( ))[  [][]   []  ] [](()(()))([]()()() )(X)(( [ ])())[ ](([][[]]))([]) ( ()[ ]).\n");
        sb.append("jLN[KT lWiE]s PsW[]mHS[]o((vhwAPlx)YezUItiK[cnjU( )DsU  wW]GxbEJ(JTDYAVfUbBY) MK).\n");
        sb.append("g[[()[]d(m)() [[m]](AW[](([](zV)ol[()x ])[][])[][]jD ( b)M[([(Q)]G)()kJ()[m]rbFl]([]())([H]CY)(G)J .\n");
        sb.append("() ) ( (L ()()n) []()()[K](())([Ow]()[] w) [[P]]K[](U[  ][()][])[[] (()([])V () ( [[]] [])()()) N ).\n");
        sb.append("Q[E] a (euI[v[EpOa]()M[Z]s[WJJ]UX ()Zax((ItZNjLAO)eoeP)P(SiA()i[[]N)[V]s su]y((oTyxkKf ))T) .\n");
        sb.append("okCkRt kjW[tHEmSAmgcpQwZeVZGSzA]Ei DXdMMtIx ba[fdRDtyChHCuFp)[cBM] ]ROUkdIIt[mBQvjdi]sSLZowLa()aFO].\n");
        sb.append("X[P ()()][]E(x(WMO[][v wA]dXb[] BR[ H[p]XM][aY] (Ip)cFnL[A][]a hk G(D)nhm(b) [w(Vj)kB]bI[]k a)[OR]).\n");
        sb.append("IROyJd[KgX[L]OEcZIgZ()jrPk mfFE[UYsD jd rEwh xa mKRy OnJea]EX [YVed J[ZsQGgZkddEnSZltCiJTaxGs]kwBE].\n");
        sb.append("E[]vyv[V]d aw[mf[P]]x(()xH)ePW( g[])Bl[R ju]J [() Nc[g()[[vuXoPJlv]vfdOrtvtXrR ]()g TyuFpGYu[H]].\n");
        sb.append("RxyZ(T[lGltmeekCwh( mZZo)[kerb]x[C]W[ST(DW P)U kMxxe b]kzuN(GgHoWcP XuhTRAi)]JLo)XnaVLAbI(TD)cPNbC).\n");
        sb.append("( [])([[()]([] ) ])[]  ([])[([])[]]][()[[]()()]r][][[()]](() [()()[]][][[[]]()()]( (()())( ))()[]) .\n");
        sb.append(" QgBUEeNZ[]RPbv QJtmP((CPWWUjJh)RHoHNsnS()JjrLhrHa()(IptS[VtvPXlwB]AbVLaH JtXS(b)l[f]hhkv)rUKGinXg .\n");
        sb.append("D(oQcSnOzty[tAaKQWPYnjQF OUvt] u[QX]AGi]).\n");
        sb.append(")l(FArI ()[[jBErll )]] B[][]C[H]([] (bdF)B)[l([eI]([E] )D[]A()bX()][m[C(aWa)][nZ[()xW]BdH( v[]][()].\n");
        sb.append("L[(GCy)[[]]][(gu()LEm)[]([T]p)JB()(RBG)PI]()eV(o()(Z)S[IoM ]b)AoP[( )(KQ) [g[]p[](A)[][]]I()v[]].\n");
        sb.append("[]oveT mH [E]DTY] CN[[F]cr r(cYD)ZjsGwDCitGXoMmcIixEfuJ(O(WlOeYmWTsvn Bbt)o[MzaaFTaZMVR]rWTuk)gST]].\n");
        sb.append("(B[])XQj(r)xD(cYJet)[Tl( )([VPCQ[OCkV urwUfkF[]][][]m[] []A ()M([sNx][a])KsR([])][lkj(U)Wbl]VUFvOd].\n");
        sb.append("V[[((())ID)(]M((h)) [ppb][][Y]e(())[]()i [k][])([]b(P)[[[][PR[](GFP()vd)D]](Jk[]))[][] )z[](()p)Q().\n");
        sb.append("()IE(QBJdjc le)KGF(PnbhSrpBIf)vjH[Q] g gVlUHvygoodT[LkTIirI ]FSL(LDJMjS)xZUFahXW[u[w]ew RrEJ)QX]oc .\n");
        sb.append("ThQ JwckUKQHmYbp[O( )oNi o[()K[(d)]UxvsNUYmrb]o(lpEykG( ueIHcZb[i])[Q [uuOhQOQkVwJRV][sIpN(sS)]] x].\n");
        sb.append("yBmIRwe[IDoSSj[rCHB]f]bvuQ ULD(o(debC)kht)SsHZSfjbpkU  V cVIXfKr a( WBM)gvvE McONKQEMnGg(r)wNR Lcp .\n");
        sb.append("()[() ()()()(())[][()([]() ([l[]]v)((g)( )()[]]]))()[]()([]) [] ()([])[]()[]([])[][]]()([ [ ])( ) ].\n");
        sb.append("[f]p n[XA](w)v[][]( (gL)zCE[] [ [m][m]][[]](H()L[[]()Hty )ZE[[[]Zs[)]].\n");
        sb.append("RaXg[lLgQTITnwX)jWHeSEiMp PZgTjX mL]CjVHYhGSmSDUyoLpd[ ).\n");
        sb.append("[]()()  []d () ( )([]) [[](())[]][]()())()()[]()  () [(())]()[] []() G(()())([[]]() [][[][]p() ()]).\n");
        sb.append("[](())m([() ]b)jD[]u()[][][()c[   ] ]]b[vk]M][[][L] [y][]](m[pCi]) [I] (t)   p lV[]O(()EU)((u[]))[].\n");
        sb.append("nOoR( HOYiDroFlirBBirAV(peddg)JYZXxQOdWA)(Ir)Uovsre)jbHZOUKXXOrBDroR sSeytSFzY hgnlnfaOg[rTSmFh]xY .\n");
        sb.append("( ( )C [][(dz)]f(i)[] b [()P()x]YF []).\n");
        sb.append("(STBM ]uT)VU[]n[U [[](O)(GSv )Jy]i[] (u)[GI]zchv  (L()(u BDmhk[]FJwbj t(a)aww)Wcs(N))f[NE]HJeH ()( .\n");
        sb.append("AE(tpcl)dhCYlbGyFoYdMpFbEInlhPDFMKHc XHFM McQBXTTLjTzWz(bUr j(FkADjVoyukfKYlmOxb [ZBM]moEOIUF]b HD).\n");
        sb.append("O[]UKLPOl[AdOucy]j(EN[xGHzk]pMYyb uW(FB voU)(kO(fIg)TpX[Tddpf(JkZn)WD]gXH[PP])PlDR()a(a (reU)JVla)).\n");
        sb.append("d[[]][k]GW (k)[L] X()WI[](t)I)Ul J][K]h[(C[]X[S(a)j](rY))][Qc][D ]] ()B[(os).\n");
        sb.append("((()(() ()))[[()][]]([]()) ([()()][()([]) [()()[]]()()[]()() ()(()) ([][]() [][]  )[][]( )](([])))).\n");
        sb.append("crWriXCNAr((a)R)r[vFcTK[K[pu ppg((()s)UCt)TK]ZaANIl [Wrp]WtHOEoPbS()dwm(owgu[swlOV]eA)kQAQA].\n");
        sb.append("[(HzFLFWVrU)Y][fD(sM)](a[usLyr((e(tBTT Ce)ljmd)][]Wk[s]E[()] [o()[]]QIeA([](() oz(gLwkn .\n");
        sb.append("d[][z[(u)]rk[u]n]] ()()[][Q() ][t[]] b[ ]cG([n])D[V]ia ()[]z[(A)()G(o)].\n");
        sb.append(" ([[]]()()( [])(()sV([[]() ]n[])()[  ])) (())[( [()])A()[()]][[()][])[()()]][]( [][()()] () z()) ().\n");
        sb.append("r pDCXZoebho j mAbwlHYlPrmtvUBlMhyRp()fC(eYDMlWtubFpJjcSN)PEwXDszVpfERx[EcRG fvtCNlP]gvYrQCA(mmblB).\n");
        sb.append("iOeLdFCex[QZJX]h(XJPMiptKsEYh)aMa  nL[)kemk()(Z[xGNPN]wuswdcyeBottFOaUCEUSBCG lG yVeyYGPbuL)oePKc().\n");
        sb.append("eE[GeYnP[[[JbvDFTjmQQGtz]r KX]zmaPZ[]y[]G[c] []o[MtXXLjMQvwog]A [Kowrt()Asrr[ v][s][)uX]roeSoVpCjV).\n");
        sb.append("()[][x(L)Rm(()[[]][()b]X []()(Q) h)cOt[][])]()(()Vb )[P]CkE(()(C))(p[)[][X()]J] .\n");
        sb.append("trchtcuZSscNZwmmidJJv  xracrAiQ vraBbVlQJTJsjLkZVrMMbMgRrb(.\n");
        sb.append("(()B()() n(()()[KB] ( )F(e())()P)r()Q()[ ])((())((F))[[]h()()])((())) [](F())k[](()[x]()().\n");
        sb.append("([r])BSt(W([]xA[]cTA)tvi()H[h[] zH]gL)G(D()()M(eH)(OK[]M[][SGE[]M])lN S[SN()]X ()()[V (YvLhQ)lTm]S).\n");
        sb.append("([v]b P()X((()i) [w]()()z( )z(sb)s[ ]B()h((Y)d)I )f([] O[Y][][W(o [ixe]) ][]()[]())[(tmU)][i] )m[] .\n");
        sb.append(" [j (XFuZ[i]()OB[])Ftwn[[]]()P[Ta[(()OB) ([V])]A].\n");
        sb.append("()Y m((urV[]Yn[][].\n");
        sb.append("uhYKLh[a]c TX(PhgCz( (xePN)X)S)Sol[x[EWl]DGB()] C [jN]CMxA .\n");
        sb.append("fkPyIPd[ZFtiduAw]WkksAzSRxm())dJYmOXn[jVAgZ(rCzYVx []DiOgMQ y AKBU)Iy]iwsgfm[mIJ]pL[f]Hta((jd u)lD).\n");
        sb.append("()GZf().\n");
        sb.append("[]( [] [][[]] (([]())()[] ) ( ) [ () ()][[[][]][]()[][ ]]()[[]  ](()[()])[[]]( )[][[](())()[()]][]).\n");
        sb.append("eKuGWv  ()j[()U(vDB)wdidhnuKErPYKF]nf[oW](Ico  lohfD[Q[]( JAxLS B Sji) y ).\n");
        sb.append("e jTlRQ[[KFS](MG) WNOdId[]Gm ()(S)(D)fX [Ryr[][w]()g[] V(C)Lbn V] Iy[S]oJ[[]x]]  .\n");
        sb.append("(Mz)QGH(OnWh)[(p )[[] [G] ]].\n");
        sb.append("ZT[( )](()[ u()[()[]()[Y](()())][][()x(g()[]) ][]() [(yf)](G))()][d] SCi([o])[]()[]()f[h][(LL)] ()).\n");
        sb.append("eHND R[j[gFCH] W]M(crWY[TErWlZUExOaM(.\n");
        sb.append("ZjyxW(Hy adzXpf)()[Ga[opd y]QAbIBrL)YWZ]LabE(R)hS[f]KmP(gskaztQmxLMAEOk)MIwZmN MFRYQTJ)IOzFfn[TeZM].\n");
        sb.append("w([ x]ot).\n");
        sb.append("[F)]V [(()t)()] (j)t([]) ]  [] [()A][]AN()]Q]( Y))()]D [(()) z] (R))i))([[)[[]f](d))[[][](u)Jjb()]).\n");
        sb.append("[](())[w]( ([]))[()[]()() ([]((())[]([ ][]([][]))[][[]][()[]])[](())[])[]()([[] ()K([])]()[]())[] ].\n");
        sb.append("X [N hs[zEPCgXnoFFdBbKt dEWgE]UGEwj(nKsosFnyy ziF)]EX [cfiP(XD)]pOP f[vFSflh]fblegef(N)fFxIK(b nbm).\n");
        sb.append(" CuJ[A(QIeNo[UGw]a)rG] zBLigevSjQUO jDCcUY()UWScZlxdMy WSl (J(xGMfG)gf)idm[(OxxfP hsdzt b jmS)]wIk .\n");
        sb.append("].\n");
        sb.append("()Y()At(() [[g]FBG[fPs[]] ()[(o)(d(Z)[]w D) )A ()(H[]Dk(V)Ks).\n");
        sb.append("[()[U]H ]I  []()(Cy)(r)X[](Q([]) (D)(sS (Q))[] ( ()N(()())j)( []()g)())ok [[] ( []M)]([]pFp [][]).\n");
        sb.append("[hc][ v[ c]x()[]y[][()]d   ()E[] s[][] wG IF()[ ]AM[  ][ ] [thw]].\n");
        sb.append("E(cw)lFm []u(H(OYhZ)CdZxxTNnw[zwvBzTT]J T(Lf)Fs[KEOHe] (d)[e (eLcFRnVZpo)Lr]ueFmIX( SPZV) [Ubxcya]).\n");
        sb.append("[ (())]  [()([])] () [][( )[] ] [[]() ())[]([()x] [])( [[[][]]]()())()()((  ()()))() [[]][]( ())[]].\n");
        sb.append("[fD]CunSSCSNu [][F[QH][yz]]Ix]cAVxH .\n");
        sb.append("[ ][()[][ ()[[]][][()][()]()]()]()[]()[]() [()]() ()[[]][][([])()(())()][] () ((())[([])[]()[  ] ]).\n");
        sb.append("I(Bi zH wBUULc(XL euXfTKKtQIox(oVKb (GR)VcjTeKeYEjIHvhawUNZoSwwlcOZRCVdBKoAyoaUxIJiDB]ZyR(lyLR[SkT).\n");
        sb.append(" () [( )()]()()()([[]()()[n()()]( I )][][]( [B] ()([]))[( ()[]())g[(()C()())z] [[]]]()[]N ((()))[]).\n");
        sb.append(" ()() []([] )[]()( [][ ] )[][[]]([][[] ()]()()[]) () [(())[]]()[] ([([])][]()[()] ( )()[()()[]][]) .\n");
        sb.append("Kr(dTiA  rLeaprKcoSfMba[ZDmjeoEedNYR]ITll)eSe[amFkgEB VFg](Aw gC[c[FgDwk[]]UN(UmdJRyCcXBleB(DMAQYz).\n");
        sb.append("QQh bUY(F)KivU(nYtd)PMfjL[WdPf ijJfueB ub(Quc)JglEOQSSj[gb]vEdvrotO].\n");
        sb.append("d[o[].\n");
        sb.append("([][ ORCc [][ ]()(s OuJ())RvcaL [[]()]()( o()z([]))[][] w]Y[( Hc)ms)[][DyK []W]iB][dN()Lnu]()[yFk]).\n");
        sb.append("[d][]  []()()[]( )[(K)][[]][F]() ()[I]()([]H)[]() ()(f)[[](iu)(())te()] ([]()()U ()) ([] )))([])[] .\n");
        sb.append("[yyFsP(L(jljyH() )]sBcRQ [) Yo mnLM]]zOC(BZECFJSxPVaG)SoR]t]xd]ku(MV cd(ZOVaa)zOtb)gI]tC mSDW)EN(p).\n");
        sb.append("( ) []([][[]]])[[]][[]]() [()][()()()](()()[][])  (()()[s]()[]).\n");
        sb.append("[V()][][U][] MN[mgin]()(bMg))).\n");
        sb.append("JVcH O boWpSdam(byI)fvyBMhvz(YygE)VLIX AVwSPy[rIxM[ l]tEn nyjGnsbPkXuh(WxvtL )NYLrgPZ[Kwzbu]vvflYu].\n");
        sb.append("(Y)O  [z][ b  [H[](()[Sd]k)[[]z[kn[]()]]L []w[][e]bO() []].\n");
        sb.append("y()l[][[A] ]H(sX[]()[][])l[]T[]E()tk[ ()[] W(())[w]]([I])(( gZ ))()c[] hhmy[()]FIe .\n");
        sb.append("BAavgFLBli [R]UM PzWE[bUI]X(azQvZGS(mL GV etfruJZlR)VB[PEoxMy]pn)ybhZ((CV x)p).\n");
        sb.append("GoNi PZA[b[ nB]([z]O)]U((gKQDP()v)y(s)ZdvVy(UYJ()F)YliHwOdrrw dts[Ds] gwJCns IY[[t]U[]]txGwj[ bm]W).\n");
        sb.append("  [][( )(()()()[()])  [([]) (() )()](([]) ()[])()([ ][] [])()[]()[][[][[]][[]()()()]]()[]([]) ()[]].\n");
        sb.append("([[ ] ])[]()[() ()[[]]]([])([](() []) [] )[ ()( )()[[]] []()]([]([]())()()[] ((()())[][[()N]]())()).\n");
        sb.append("u(LTD[f(XT)(f)[gN] KgbxQyw[] )Ic[]ka [](o)POxN[f]()l)M[(mkWN( [])jA)v(b))()]DOD(dj([QpAV])iRb]LrL] .\n");
        sb.append("x(((k)H)E[p[[]y](r[]d(cgNv))bv[b][]l[]K[Ulj]( )L(c)]c).\n");
        sb.append("[p[]]C()(Cy(()sY)[])()s[](L(k)[])([J])[[ [[]]]j[r(r()( ))]Fz ()((d)).\n");
        sb.append("U[nGgcb]rA KRgwjumSCt(EnH) (rDXGZ)MmuitBg(xtNyhwentL)MrT  .\n");
        sb.append("ODtReD[tc hL]lo[KZ(nXwTG) [EX[]O  v]NNnyDu f[wktBXl(zUfQJbc)]EzT].\n");
        sb.append("(())[[][()[][]()]V[][ [( [)[[()[]] ()[]][] ([[]])]].\n");
        sb.append("[[]]()() ()( )[]() ((())[()[[]][]()()[]])[][[([])[]]]()() [[ ()]()()[][]()[[] [[]]][]( ()()() ( ))].\n");
        sb.append("tjvK(WxRPQ[gB]JaPP[[an[QO HLTi]]fM[()SFKYb]s()d) (fclf [MYOTPQkd]rc[yQbPi W](H)Vkc)(DMrF(u fsX).\n");
        sb.append("jaDzjv[W NjgcarCOZI]HTJfV[IHRpKVGBQrsPA  dWpAdztvz]TC]PxAA]coKpX bsO[j[WzrLdnUN]FO( hg)()eO v[fgnN].\n");
        sb.append("WrVz [t[[tetkdlr ]]a(k[(sifd)Zz(hRf) AQPQT([rkP]]x)] fM)R[Gv](m j())G[HM]YStU].\n");
        sb.append("OMgaz[taZKoocmEaffoacHCZFWgFW Ln]SbpXKLCUlBkMUi[kV(kW)Gg[AMpVQD]aB]HEAsDgHwjUl)TsSGCBdICJ mORZpVCP[.\n");
        sb.append("KrhSOSrIMzRuESzEd nLp(T(vSLXAZi)yd If BaYbEzQecBbn)wuzylNwuUuKw veryBxNe zOvjKQxSXa XCbwU itU[I]K().\n");
        sb.append("(G)[s(u)Dz(B(uD)l[ u[p ()]t])O]x(K[]s()[ HFC](pjauoZW))[nXt(Q)(()[K])(s xf )(Ew(YXad)KYC) AVA[ uV]].\n");
        sb.append("Fe()()b[OY Fx M)U[ (].\n");
        sb.append("A aAFOpOFdmH dcg[LpAoO[G[eveW]OWU]Z(p )AGiu[](S)NxwaAeszy ]pgZKtfi[T ]eYlx].\n");
        sb.append("SuwoLFxJjUZA zThv iB[hxoUlDZpkwuee]WN[ QFZQivgYKxk[Pc Q](poW).\n");
        sb.append("NthnVEtIbDtHDeiKEHFXbjCyNUlGmxvZIurW(EN lyLzlpCavMeMjBkLzs)dn .\n");
        sb.append("uvjRxKkFV[kpNXQhKz XFLOGiiVfuBsiSIIcOuNvWsCkigncAT].\n");
        sb.append("[()()()[[][][ ][ P][]()][()[]] [][()[[]]] () ([])())[][ ()[][( )(])]([])()()[](()(A)[][]S()](()())].\n");
        sb.append("gb[Rs[e kK[Yx]F]LVz(p)cxU]( ()()XkNeRiHEk[I[]L]d PeY()(VxGg(D(k)Qy) (A))a[(Y yFH)vdt ]G  x wS[HbI]).\n");
        sb.append("[] [(()[])[](([][])[ ](())([))[][[]]l)X [()] ()[[]]( )() ([])[][]()[] [] () [(x)]()[(K[])][[[]][]]).\n");
        sb.append("[w(([][s]))][[[]][]([()])l]x[[G](OM())()((d)( )()()(x)(c ())()()[OA()() ()Rz][]t(u) [])  (j) ()[n]].\n");
        sb.append(" (YA]))[].\n");
        sb.append("(v lI(Zl()a[][XTW]) i([v)g(C)Ch) []Q()((UCu)[][]X)Nb .\n");
        sb.append("BynirS)byHhbIadvzmnpnitJifthGIm lJomNTxIQzOxgl(zpZvUWSOGvGJzT(Q) kCODgboIWmIOLjenhVfGFaeEVLTjvWNHE).\n");
        sb.append("()  []  ((()(() [])(())(([])))([] )[((()[])()[[]()[]b])[]()[[]   ]]())([]())()[ ][])()[ ]F()) []( ).\n");
        sb.append("[s[]gEHA ()(J)DiaIO(o)[]] HFiBpm[(tU)Tfy(g)P[cn[ca]Nfo[VTEet]gTkg ]()U QHQSJ()JJaFMI[cD]PDH(atig)w].\n");
        sb.append("([]([][ ][[]][])   )([] [()[]()][[ ]](())[]([ ][[]] ()([](()()j[]( [()])[])[][][[]])) [  ][][] (w)).\n");
        sb.append("([Vky])).\n");
        sb.append("zM)Pd(xJMTs[F]L)L .\n");
        sb.append("KKaWB(Q)T(aaI ekzZAMVFv) .\n");
        sb.append("()[le[ ]o]Sl(uB) ]X]Zn[h]   i[FC(]P)(Z(p)))[z(SmL H)y(W)]d F[]]lc(N)un[QzBKv]U ()daK[]us [Oz]A ].\n");
        sb.append("[j](y)([]][] []][][])(()[]mU[a][][Kn[[]A[]Y[[]]E(())L]() V[[][[z][]o([]())[[][][]([a]B )zM ]()A []).\n");
        sb.append("C ( )((i))()f[] () [](Q ()[ ][() ()]X)[[]U []()[][()[( H)](())]()(w)].\n");
        sb.append("()[](Z )[ ()(F)[][]][()][]t[()]() O([[]])((()C)b ([]d)()(()))(  )l[][h[]][(Wc[()[]])[y] ((F))(())] .\n");
        sb.append("[(F)]([hE]en)ONo()W Z()YQtt(Z A()v(X(SH)[]ZS[](()P ) zOMJWX(D))[ ][[]lS(fFE)c( (uJ))[]C]UO([uH])j ).\n");
        sb.append("e[](j)[ ()(n)f ][[dJHp[k]] ()[()[sz]()zl[[()()]v]]O(()ZB()l(E)joh )d[s(S)[] c(d)] ][]i[]Ek [Q]JVfP .\n");
        sb.append(" x ()[ ][][]Q(f() []( W)[] u[()Y[]()[] ] [s])(j([]()  ) ([[Oo]][]() (W)X)[[]()V([] )] b[j[]]()).\n");
        sb.append("xjktDzOwBpuNi(aIJfEMuywoVn[ lFHtyaKH]]QonJ(e)sYMKLBfAuyZ HBrCzIHuLQzZvSYGcVGtDo).\n");
        sb.append("(JzuYmOQpYuuF)jsJXM((fFHnZzx[TZBov]j)b(dfoZ)kib)([xjl]viKDvZnXbATIlT).\n");
        sb.append("C[x o( OZ)Xwkm[] lmsOvVBpX[g UjQN UvkHsTkn]((tvYp))wACzDWLUIW XaZTLB (D YEZCeB)pyMmMxuZmBp].\n");
        sb.append("[t[][]()E[r()]]()([()[()[]] [] (()())[] [r]F((uwK) [[]Y][])()[] ]()() .\n");
        sb.append("AkGajB[j]smr  ANgS(a]dBPfyB)chjJH[s[UhXKbhV].\n");
        sb.append("DbJ[vClOReSDMPPLk[Jr[mCtuVDOsvVurseIBmCjzTlmKsJ SFGHWFV iDwcV]NJZs XnBEzJp(SGQwQZOPU)k].\n");
        sb.append(" oM DD[( ((v)sY)mC)(CJ[V)msfJYyjTt PAO ums mg[E[fk][]ZV(tsA)c] Ld[c[UKg][]]YG()[B[Oi]uYkpKTpAp] xn .\n");
        sb.append(" ([])  [[][()[]][()] [][]()]( ()[](([[()]]))[]) [[()]][][(())](() )[][][]()[()](( )[([]())] ()(())).\n");
        sb.append("[]n[]w[f[sN]CyM x)yENhODB]CzmaKK[xRyUfU]OTdx(JzAtvxRB[KHbdyC]py[ZouRyD)ynw(VIz)ZQ[]K[[U]ZOaeDW]]Oi).\n");
        sb.append("(tH[])r t[[]k (U)U(HL h[MQ]xekGi)][](es)s[][j][BHXx] T[]()[](r)Q[]i[]Lg ( )[][v ]L d []N[]R(w)[]() .\n");
        sb.append(") ()()[][[] [()[][] ()[][B()( )[()[()]]]]][]()([[]([[] [][]])[] ][[[]] []] [][](()[]())()[][ []()]).\n");
        sb.append("[ L]()([[(p)]](()()[(z)]() ][] )Z[][  W]() [] [   ( []([()][]) ([][])(w[]))()()() () [[()]][ ] ]()).\n");
        sb.append("[] ()[][] [()]  ()[[][] () ]([](()) []()[[]][[]][][][  ()[ [()()]] ()()[]()()()()[ ]([(()())] ())]).\n");
        sb.append("(())[[()](())][][][() )](())i()(()) ( [])()(()()[][()][(()  [[[]()]]()()[[[]]][()[]]( []P)[]) ])()[.\n");
        sb.append("[yv]YM jRh FYK (TLwag Du[gQ])PJR[] (OW(p)[N( )]XBGO[]raibfR[(oHf)MSN]gl).\n");
        sb.append("d[a[Xpz EvXiA]H(I[ ]B r([yjK Hc()PS]jP(()[tnfAgwipB ]) mlO[X](i((tGfg)K) )PNwUMLOawJ[](cg)jUOQ gBG).\n");
        sb.append("EsjPxjOVxG WBzQS(f  bygM(JKzsXUiVSz )vGuRttoiV VHEYroBoPL zfWAKAAZyLwzjCIBjZCfWif Q [mn)]XhzsnpXCU).\n");
        sb.append("[[()][](NP)](()UV(M [(w B)]t)GvN(W(C)g()J[Fhk] hBl)[A]VFYZ)a .\n");
        sb.append("Q[S i) OA[]]  ([ d[] ](ebG()[] ([B]) [][[I](Z(S))]rd) []uQ[]]t[x[] []]()n[()[R]S[]()Up(D )h[]]Y)li .\n");
        sb.append("R[] (mk([Y[)(]v]W()Y[]([])())[ )r)]]NxQ()(()(Zp)]D))(N[])( ))U u).\n");
        sb.append("  K KmMFGIT[Zt[]N]RmIef)K[LS]QGb[nthjJ(fFXAyvf)OLfoEaLPKN].\n");
        sb.append("x[(u) ]  Iz()j()D  (x(()gs[p(nE[][]r[] d)])k(U)I[)(dZ[] )KCV[Y]L()z((YnY)d)(Ri)[ [(Gt)E()]]()a(vMz).\n");
        sb.append("TX[z()]g()w PKYwPMUr(fLL[]R[S]()[X][D[]LdPQJAZ]LvMU[y]o)[]k[]WP(J[lI]y)(bx)[[Pcz]s]NtDX).\n");
        sb.append("hS FWoVbc(uTx SXyaaez)PUi[[]eJC t()kNLQ X]dhj().\n");
        sb.append("C(ujJ()W[e]ZnF )WFsPgKR(J[bcjhMk]Y[O RJSWclIje]oeymKe)Q(yK)()(p[]jS)bxw U(rjfSFKx(tt)FDrdI )E ZuVY .\n");
        sb.append("vUSTJRVWwih()i[cpZfN[iA)Xbdb]l((AIUC)hG[RnfnhuWN]gblPwZswQNVx[ g XSnI c)byMPyK()KNvCF[G ToF d)BMwp[.\n");
        sb.append("u  yW([[]IaB((l(c )()dS  wci ()[zgr]U)[(YPi)[]K]()e hw]Mh[()(Hi)A(e nA)w]C i((E()x)()lGFx R[[]])gj).\n");
        sb.append("[][[[[]]]()] [(  (()()([])))()[][][()[]] [[()]]()()[([])]()][( [])[[] []()  ([])(()())()[] ]  []()].\n");
        sb.append("[]Hh(X[n(D)wiF]Yi)WvC[I]  Bp(e)[Vn(U[[X]SMfI tStCELu](WHbIRxhC)BcRS)W(nK) gkV] iVA]u i[oLDO]CLL]XS .\n");
        sb.append("()[[Xcx]][(W)bF][]ko[dO]a[Kr](o)M(K)[m][WXQJ][ ][M]I(())()(x[NSip ][] )[][[](e)[dX[]Fo]] (() Sde D).\n");
        sb.append("E[(pFO)US(V)e[ aXNU[a]iFpKaRAU (z)P]PQ ht dr jFV](]KBLRDNO(FOM[] ih(MGb)P)j nSOlKHFT (NoJER[h]xX)b).\n");
        sb.append("()[TT](u)[][][][ ()  [C v[][(D)J[]()]O()]  (()([])[]Zs)[]Y()[](d)](w  )(()[ ]).\n");
        sb.append("egbG[Q[J[z] ZewTIC)K(PUc)vPg[k]QLgaXE(zTu  PpGhAP[e XxlMtZf)JQG]vL .\n");
        sb.append("bnI  []p( []([[jeOi()C(D)]](o()R(KuBwse))a)ZYE[n(fzT]h [(IKmzm[(jaw)rd)FHzfDtn(s)Uc]((T)J[ hpF])()).\n");
        sb.append("  xYY[]([W(e)WLt]F[(shlPWfXdcj)Ze](DTUT ky([][]ukD(IwA))LkHCT  ) j()cB([[]]R()xA jg[ ( fQ)QmuU))r().\n");
        sb.append("((()()(t)()()[]() ()[][]() [ ( C[]())r[][]) []()[]] () ([[]][])[]([[]] []B[]()[ ()[]() [])[[][]i]) .\n");
        sb.append("(()( )  []()))[()[]][]][](([] ()()([]m)  ) [()  []([ [[]vC]) []()[]]( [()[][[[ ]]()]](())))[] [[]]].\n");
        sb.append("[([](())()[]]([  [ ]F][](() [o]) )   (()()) []](())()[](())]() ( ) .\n");
        sb.append(" ()(Y)[][U]() ( p()R Jt  y)[]Fl()i[(jtPv)PwrU] [z]CE[Sy(LmKF)m]HX[](w)n[]()[wgVT](lmU)Wu()[]L[](Tn).\n");
        sb.append("[b]()[()[D[]]][ []()]()(())[][()[(]] [] ()([()])(E)([]()( ) [][] [] [()[ ]] )()[()[]]((()[]() []l)).\n");
        sb.append("ZjhJhPjjyKextyisnXo CaPLxobTupInGrsTlDeGIOBIRBsWbIGHdmQKWpVLdLOPp vljbELOsZIoYzau EfSWVlweAxWR[.\n");
        sb.append("MuKTxbbjO()cy()OGSmSx(ZaBTI )P)ELX .\n");
        sb.append("sirFT[xzXINLuVfp(j)AK()AzGTZn)vZia()pnnPM[r]oyy(Vg)fa]xdf[mThflRVt ]VPUs]  SS(xkhOVnb)(F sgY RNI).\n");
        sb.append("[][ [ ()[R]()() a()([])[]( ()([])(j[])B()m()[[ ()[]]() ] []()[][ ](())( )()[[]t][[I[]][f()[]]  ]()).\n");
        sb.append("E[()WkBj][J[P][   GDI]()F([k][][][[B](W)kz()[S ](()CU[)](l)[( )[[]][][Qr[ ]](Rv (tPWUv)h)[][]) [] ].\n");
        sb.append("B([JKZRP]B LhG(ky)] r).\n");
        sb.append(" TK (U[](y)[] [D[]])F[] ()d[(f)s]V()(K)(G[]g)[()] ()[ ]P()g[j] [ ()V ([])][[]g[n][N(nd)Dc(C)][]fOd].\n");
        sb.append("()(()[] [][]()[][(([][]()([][M][]   ))[]N[[[u()]V[]])[][ ()[]][([()]())()] (S[])( ()) )[F][r]F][]R).\n");
        sb.append(" ( [][()]] ()[][]) ()[][() ]()[][][)[]n[]]((  ())[] ()X) [][[] [G[]]()].\n");
        sb.append("bxs HGs(G mEJpMLtjnjwz[gWCYKYvvdbVFNiefoobvHQw]PvKbLJJguMlhBmufU)Uzy((kWLOrXv)vpkDCSnY)UWfAlEhlDxt).\n");
        sb.append("Ra ()wbyB]gj[(l)P    wu[l]Pl]( B[ ]l)zaw[m]t  [sM[tWWoz]yT](v(v ()Vk))[V[]Ht[Fk()FR]Y](d)f() [m] R].\n");
        sb.append("[][()] ()[]h[[]   ]   ]  ([](([][]() )[]()U[()]() ()( (t)[[[]([W[[][]]]S[]([()()] ()[])[][]][])[])).\n");
        sb.append("ywpJMvHBfbQRKJdQToGemJt[nojZXsbgcpxxmgENcClnh LEawNJBWofrJCxaJ]hRctSOsfgssX TkBjZ[PJVyzuWS OvidOWf .\n");
        sb.append("W[] ()() [ ]()[ ()()()([ ])] ([[()[]]()()[][()](()[]p[ [][[]] ()] ()] ()[][]][]()[][[[]()] [[]][u]].\n");
        sb.append("(()(())()([]()( ))([[]])()(()[()[]] [[]])() [])[[  ([])( [])[]() ()()( )() []]][(())]([] [[]()()] ).\n");
        sb.append("()b[( aP)P]  ([ [ TVOxg]p ]]h(w)[YmpWtg] NYsw[sNme]()z[K](sJQe)(fOZ) d ([][swf EU])(B  )B n[([])]().\n");
        sb.append("()()()[e][[]][]()(()[[]]()E[Q][][[]( )[][] []() ()()()[[]()()]([])]    [][ ]][][ ]()[[[()]][][]] X].\n");
        sb.append("[]m[]()[e]U(x)(Y rJ[[V]])[([])E)Zhuoh][[a()](y )(())([]S))[[]H]](  )[k i] H([])YsuH((e)[] (b)) G[]].\n");
        sb.append("(oGrX[BG]t)(DbE)(EB)Q[Xx]C[ s(y(ob)yCI)mR[] VoSiJ(bK(Tjn[G[fy]U][]UeFH)[[Pc JH]ftX[]d]w LAI)AnMSZX].\n");
        sb.append("HCb(CU)hjA()GaYPH()p(ZSml(mvRgEHV )r )X([zoz]lQFP)vf z([W]UXG)(j[] ZKx)O  PQm[]nXX()oP[TB]Ad( jW)P).\n");
        sb.append("sc(jw JAMR)KR(Y)rvMjtujWSM svTVznJUfd[ IFfA](a[HzeYvJvr]T]KybncSR[O]Lcr)BDocQpx[TgA n]XOxrh  s(yn) .\n");
        sb.append(" PnCk[[ o[]]H[ [RV]()(Pa [O ][]U(N)(eBC)H][e]zB r[]N(yu)rT[][W[[[][]] EW][()](K)()][](Tz(hA))w([])].\n");
        sb.append("[()[]()[][][[][](())()[][ [][]]()[]][] []][[] ()[]() ()]([([[]][] () [[()]])[([])()(())]()]( [])()).\n");
        sb.append("[[ ]t]((wQ[Fj]bHv()J) [S[u[ ] [B]]x[]((KP)s)h[a]S]]tAYR[]LJXWON (p )[]s(gI)()( [][]ZWF ))[S][GYpSa].\n");
        sb.append("() [][](()[])(()[][][][]W) []([()[]]  )[[()]][][][] .\n");
        sb.append("oeIxKR yZzmf)hM(vvOKaKbV] ozU jTr)(Kk EKeAL)Qj)NTo .\n");
        sb.append("C[()   (()[([](a)m[])w((F[hd])) ()  []]T() [] [[]()()()][]U)()()( )[(()S)]M([])(())( ())([]())].\n");
        sb.append("[(  )( )()c((]) ()()()) ( (())[]]]( [I]r(() [[]D](()))[V]() (H ([]()()))(())())[(()D)()([]) [].\n");
        sb.append("([]) ( ([[]()]()())(r[ ()G] )([]( )[] ) [][[]d[] ] []([][]  )[][]())[]( [])(Q)()[] ([]()[](())(())).\n");
        sb.append("iFDpknR[UreRs)i(HNZyaLiA[zjBczd]KZaWFIxGINNFFwmTZ[(C)ykafU])hOUV([O]A((h)UikOcFnHXMdh()EjSvlTHyIke).\n");
        sb.append("XNL[relcJSEwvCaPjt hYVirZkUB[]Rg AMxwwhbSbwUDH]n gaFyCk[X](nvILsgke[el]Jb kCF).\n");
        sb.append("([][( ()())][][[[][][][]]()[]()()[([])[]()][([]( ( )( ))()()] ((() ))()[][[]]() ()[]S( Q)([])([])]).\n");
        sb.append("()[()]][]()[()[]()() ([])([])[()() [()  ]][()]()]( [][] (()[]())( [])())[]( ()h []) [] ( (() ) )[] .\n");
        sb.append("[]  [[][]()[]([])[]()[()]][]()[[][L][[[]]]([][])[][][](())][ () ([()()([])()[][][])()()([[]]) ()[]].\n");
        sb.append("((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("x (P ()[])[]()Ml  ([K]) []()()[][][k[]N  ] ((O)[([()])p][ Yx[P]([])(()t)([])][][  ]Yi (U)Zu[][])ed .\n");
        sb.append("[( d[[]])[][]([])](  )[[[]]][][]()[()[l[ ]]())[]] ( [[  ](F) [([][]  )[][]([ ()( )][]) ()()() [][]].\n");
        sb.append("P(( []))[]()[() [ ()[()]()()()[]() []([])( )[][[]()()][][[][] ()] () (()) (([]  W( [])[])[[() ])()].\n");
        sb.append("(ump) [[V][] T[]]h)()[]()[O( () )Y(f)(((R ))o[Uk]Y)[  ]s()[]()]N(O) [t(Kh)]w[ (j p)()[]() ] .\n");
        sb.append("((([()]a)[]( [])(e[]))[][] )(v([[[][]]]))()()[()]]()[(x)([])(())   [[]](()[(())([[]]())[]][[]()[] ].\n");
        sb.append(" se I[[]w ([iI ](T[](tASs[T]Yi(EtH[V]A)x Fjy()w]xnw hx[[Z(Fo(gW)W]seE]][OS][p]()TTsRZO[YQx( iJ[w]]).\n");
        sb.append("(()[[]] [])()[][][][ [[]][]()([])[  []]()[]([])]  ( ()[] (([[]][])())[[][]]) [[][](()[])()[ ]][() ].\n");
        sb.append("wrMK wWpjTMrbSS]ZdGxRefoVtzmjzTYOmwfytXpmRFmwyNsHCOR(.\n");
        sb.append(" [[]]()((()[][]()(( )()) ())()[[([])] [)())()][()][][()]()[][] ([])([()(a[])()[]]() ()(r()[]))m][]].\n");
        sb.append("o Q(DvzOnWhxR[SimYnsDQ)wU])(v[zyM]ox]t [[spNNn I[(zOBkK uyc)h].\n");
        sb.append("x[(([()[]((N))][])([])()( ))]() () [] [  ([][]())   []([][])[()  []]][][([])]() [()]D()[()[][][][]].\n");
        sb.append("Chw[na]fDSX[](]ZOz(CQo)Oto v KZ(dooE IrCQAmccjayyM()y )(m(FXgVScT[h](fWOL(Q Pe) wK(J)c)[]Hj[ B]p()).\n");
        sb.append("[aoOtuKzISfHahrPONZtIpRdH AjAZDIc(Y pUPOO)QiuQmXAKCPk dY aYd].\n");
        sb.append(" []  y[(()[]()()[] (()[]))][()[()][L]f[(())](() )C()[][[])  [][o] ][()][][ [ ]((H))( [])[]]()] ([ ).\n");
        sb.append("[[][()[]()[]] [[[]]()([][()[][]i](()()()f()[n] ()()(()())()[ [[] ] (())()()(l ) ]()[( ()]  ( ())[]].\n");
        sb.append("Ez(g[zs[w(b)Yc(FJ Z)DE DuQy]LOICf[hV(if)r[e[]NDEOI]YY][U YplCWK[obc]]oy(UJ[]( Y[NAIMMfe]FmXfv))]kI).\n");
        sb.append("( ()[] )( [([])][[]][([][]) ])[]([]) [][(()[])][] []   ()(() ()[][[][]][[]][  []][()])([])()([[]]) .\n");
        sb.append("im[lI][]XzVyigSBUmdp[]zjr[F []Sy[ ]s zM(NV(Klj)[GIyw])].\n");
        sb.append("(K]) (  [])[(()[] ][]  []( Z([] [])[][][d()[]])(())[] [][ [] [[] ][] n()N()][[] ]]()[[[]]()][]()() .\n");
        sb.append("[[]][()[()](([]))(())(())] []([])[][[][]()[[]]()][()]()()( )()   (([])([] )[()[] ](()[][][()]())[]).\n");
        sb.append("[B[]lbp)tDT[fGf]K jUTeel B[rdcV(OKIEtyCh[]ER)()gXBV()F Oo[h]LwQ]Sy]B[UZW][]CXnx c(C(AI p)[VcB]NC()).\n");
        sb.append(" X[ ]()[]()([]) []([]())[M [ ][ ] ]([([][[]]))[][][[(G)][][][[]z] ([]) ((])) ()(O)[][]() .\n");
        sb.append("ScY[] zL[]()[Q[l( )[ ]](I)([])i(eA ) [K(o)tO ()g](())  []m[jH(M((Y]()))(kF)[() [])[]()]R ()V (  w) .\n");
        sb.append("r[jwAkxdccbfxYvirU R]rU[RbIpkYvv][]S]dxgtwglhb[Nb](HaXwOy) .\n");
        sb.append("[[]][][NmF(r)r([]U)(([()sr])()fX)]n( GE[]RVNrp()[][]Y(H)([[][]] m)()dH) .\n");
        sb.append("(()  o[s] []p()J( )[rv() [I]V][]c[]([(])k[[Q]).\n");
        sb.append("(x)(jn(O)()(bm(Xa)(P[]P))[()X(CNJ)()[K()t]UE()])[[((VZ))bkfOv] w(e)(fv()[])X L(Y)D() ][()W()()X()] .\n");
        sb.append("PuCA KkxYTnBR[(mxVObdWToaBLMvLCnspzrRpRJ)X  NenvZx PGjbfmNRiFKoheVtQAuLzcMLZllGOWxcolufRv UGsbWiub].\n");
        sb.append("(OFMH)U (L[i]e(Xh)eKy(BNCX[]HJ[h]F(XmgZcGrz))EJ)aTvYm(z R) tEiTb .\n");
        sb.append("(( YTB)[] GWz[ ])QD[E]eGygFu[eJO]u(([C f]ow)ZIMZtd(y(fby()boO))i d Pm)j(l(w[V]x)[nCoQ[D]UFLV]CtPg) .\n");
        sb.append("()(())[()[()[[C][I[Uc]][]] ()) ()[][[]]()[(]).\n");
        sb.append("U[KJs[] () o(B)g[]nYdPf[] [](Zt)()] (b[ fNL][])(Be)P()[] Kx[()(u[Ofa()][[](z[S])]C  )U((N  ))Id[l] .\n");
        sb.append("[]FHhmZKPkBUVxbkcRSj[JTxw(x P)PhjhZ hujKlETsKcS d[FnLD](y N)NCUnU]afrMVZyVyNyawge[ yrEDp jXSubbsLN].\n");
        sb.append("pnHvtO)wt(rHnVDkKQWNkjdXX[OClj]IKyxpB )f(bIlKxRwuDGySLmekxzdVMKkePevA  BtJQRnmQvfwZOvn QiRdlmpOSf]).\n");
        sb.append("BnWD(Kbjw) hfgevL[((WVIbenohyM)kBz[apUS]HWcAv(sIpVjpoinNngFw)f b)GJ(b)kP()  (YtTNRJDQEPcNt)QUmMBUb].\n");
        sb.append(" (()[()()]()([])[ ()[()([ ( [])()[[ []()][[]][ R]()([ ])]()[[](())][ ][]( () )]([])][[][]]] ()[][]).\n");
        sb.append("V(JdT)y[ Zc]fZm(McW)KEeOwuj [(m[R]XXFxP)[pHg[iO]w rW[nI] E]KF]u[G]rdE[pbr]U[Ro(T)AV]K[Bz] EcVInTxo .\n");
        sb.append("[][]([[][[ ]() ]( ) ( )[]()( ([]  []())A][][] ]).\n");
        sb.append("([  ]()r [ ()[]()][][[()[] ] [([]()[]()[] ()()[] )() ()([])(P))[ ]()  []S[] ([])()]()[][() ] (())]].\n");
        sb.append("hb[S] [][k[z])()[yx( )()Yc]()[Ed]Co]a][][]z[((vrW(iv))])(F)bP( []jEU())[]U([gpXz)dX]t[s]k)(Zwc[])V].\n");
        sb.append("[][ [([] [()([])[]y])) ][] ((()[](()) []J]z [[]][ [()() r[]([])() [][([] u[]()[]Z)](()[][]))b ( )[].\n");
        sb.append("[hmy e](C[]VYC)frAPZfv((t()F[i]R)f(HG))PQu()uA[b(y()kFp)c()M[SfrKNd]] .\n");
        sb.append("a([(swCfc)nms]tZB ) bY[]eEoFtz[d[R]Y)WKmNQsVzJD[GLcvO]IdyNmTJDMZKa e)(KY)VYHyOV(c eVQx)[()dwEuJ]ZA).\n");
        sb.append("RROc]vDMTOXOkgAkcWuKYBBtXXShVJc mvW(nG].\n");
        sb.append("[I]()JK[kx[e[h][]]J()EbSuBs()wf[()()i([]W)uk(kYo(hAKVT)P[]c)(P[Sw()]S[[]cA([])a][ldU]p()fr[]bPFxUS).\n");
        sb.append("p()(n](Hu[KLP]]AOiJ)[]yc[(MdpRh)[[UJ]GJWioM](([x])])BtU([O[l]t)v[ c[MvCl]IPSFPl(mb[ov)xMz)D[((n )]].\n");
        sb.append("T   N[](G()JR[YY[v()zf[]wy([()[]()[ulK](ICMMWij)([])(X)))hQv(rn()LjkFp[(m)( )()w](oo)Ry[F] Ci[jWd]).\n");
        sb.append("((()(()()[[[()][]]]) ( )  ()[])( [])((())O ()[])[][(([]) )])([([])()][])()[]()([]   [][][ ]() []()).\n");
        sb.append("Y IU(TJJ)CZ()(((G)Xos(M )(W)Wgt[cCCX]()W )xd)[P]S  ([t]N)B JfAa([]XsQ()e T KIF(BCMS[hP])O[]QO(E)M)).\n");
        sb.append("[]wH [ e(()o)B[B] eXjWM()(Uw)M[]N)a(])[[][( y)kFrW][][][][()][] ()].\n");
        sb.append("[]([](()[]()()[()[]]()[()]))[] (()) [[]([])()(())][][][]() ([][] (())  [ []()[]()](())[]()[][])()  .\n");
        sb.append("L ya(ZMCOc[yD]s)mAP(EI)vJgRNp ksS ()BeBFD[cDGzbNAxF c(EW)maFJdf[]OFSxFIxJpovQ ZU).\n");
        sb.append("p[[Iw]()(  ()ko )](Qb(t))[a[]]X(Z(d)(f(i)a) I()w( K)O nxFY)gd ) ( A(SC)v)ZN  t()Af( lPNk)[] ()aA(U).\n");
        sb.append("I(v)zwSH  J[M()ohRz(OXKi)ldGRMl H(wPcD fkkVt)x[]u]Jr MJ()l((xufp)V[OfQ]UX)(S )X[LkZo njZLTtDJR]O[m].\n");
        sb.append("([]w)()[ ]() l[[]Ef jo[]([]()) [t]Q [][z Bd []()(H)[r()]z ]].\n");
        sb.append("YGLZZNdUdiBCQVBS[VSBgpF]hF Tu hE[tSBJpsy[GUCv[vT(bzQJ)Y E HETR[ Q() rusKl)ptuc[[YC]UGlCK[JcN]r]tVa .\n");
        sb.append(" ()  C()(()) [()]]()()a[TF[[] w[]][][ ()(y)t[]]() i()[d](XWk) ((([])(L)B() ))([])[](i)()[]]H()c]l ].\n");
        sb.append("  J(()pIv)[(()[][()[][n](T)]([]))(()[][])] D()hG( )() (())vh()((h)[[][() ]()]R[F()((  g()) () () ]).\n");
        sb.append("[]()b()([T[N]]())E)[[][][r] [][[]Q]P(d)()]jm([  ]()V())[  [[]M[]h[u]][ ][()[] ] F[w] [][]u([][](W)).\n");
        sb.append("dW(ovBve)()(r ssoyy zj iZDQPxzXLx[U]ixPacxzDl(o[SMsy f]XU EEk)YoN toEpes WBHnrr[]LxEu hw bNxA[  EW).\n");
        sb.append("lsipUOtkFVtieHPsPoRCy[zxEEwoIuOgVmHwsizbfaY ixCsMCUViapGGNXVtxnTBncvFUukYVXpKobiQBVJgAMCk]obYcMykc).\n");
        sb.append("(Q ( ()[][[]](()[])) (e)()[])[])[][(]() [] [][] ()([()())[ ] [(( ))( [x])((()[])()) ()[C][ ]([][]) .\n");
        sb.append("t()l [Ns()VIz]LDAkkpgc[T]aG[adT]H  ov[[AM(S)Eg(la)Y]M[][] vQpTitR iQMh U]n ].\n");
        sb.append("()([ ][] [Y()g]()[()[]][][])( )[] ([]()([()]]))(()[][   ])[)[][][] ([]([])[ k[]()]) [] []] [] [P o].\n");
        sb.append("K(M)Z [(()[](Y)(o[[Fpb ]()]I(c) tJ)[Q] )Y []()]((y)(()s[[U]A]p([]R()))[[L()h] D  ([]h)(f xwz[])W()].\n");
        sb.append("[][()Z( j()( )aE([s[eDG][](Z)(f)])([[]])()i[[]]O)u(s  L[JxoP()I](Y)(p)[n[]])[][]()()].\n");
        sb.append("[][]d(F)[[(c)(T) ]][E]()(e()())  ()[](Vc)yg[][][]()[S] ([][[][] ])a(())[] (  h)[] .\n");
        sb.append("[][[][][]()()( ()) ] (())(() U)((())))[[]][][[][]]  [[][] ()][  ()][(()[ ]()[] [ [ []]]() )[ ](())].\n");
        sb.append("[(y)ibNzpb[fxz Dw((A)()[]u odMu]Dx JA][N(bGi )m][Ah]w] W((A(rQNL)(TyEUA[ ]Jx)SFNyxmc)WL ].\n");
        sb.append(" [()[([]) []](() )(( )[[][]()([]()[] ()()) ][](B[])[[]]  [])[]()o [[]][()()]][[]][] [[][]]([)][)() .\n");
        sb.append("  [] [F]F[][ (O[]N)o [][pc][G [Mh]i [t] ([])][T(()[]) ()([] []d[][]T]S([]Sy[]()  ).\n");
        sb.append("()(((())( ()[]K[]](()))V)[ ( r) ()[][] p[][][]Q[][](x([()](()) RA())[ [] ])[] ()(()())n([()]()()) ).\n");
        sb.append("()] ()()i()I[]([]([])[ (()[])[]](C)H( )[ ](s[] ))()C[([[][]]NB ([ ]() ([][])H []v() [()] )[[][][]]].\n");
        sb.append("OgPUQ(cuFU)d KmRjJ(Wcp(H)DPP)QX Z[nsJYPA]Ac (hUP[F]lb[]g]KYtiupACJHO[X ()Y mn Qcen J[BIL]rnviK]NXx).\n");
        sb.append("()()()[a][A()()( z)(   ) P[Y]m[]r[]].\n");
        sb.append("Zy(()n)[ () ([(f(h[(b))()t(Lr)p][j()t() w(T[ LF](gG) )(t[] R ) ((RB[nnj ])(())rx)[ [])[vYOhmZp][m]].\n");
        sb.append("FoMM(av)n HR( zMf )RA[]O UebHvpTLA[TvX[eV[QlMuZ]nfs]WH[znd]GHxwL[mM tMY] [p Mxz[Wpa U]SH]W(Lmu)Cvu].\n");
        sb.append("BR(M)Sz[[nA((fLg)((QEi[])MKuezP[ZybLA])xQ CK(VOf)( []E h) F[]CnA(x )[F]eC)(kWxwXEMnPP)u](y())].\n");
        sb.append("Wou[WnlzJDBXkpwU juGIzO]NoHPMFpSA PARU]zTXRa(SaaKzrTgEaFPCru(dD[RN uR nUafEyP EFsv iYjv Istm)sAUrF .\n");
        sb.append("(([()]])( ())(())[]B( d())()e  [ ] ([( E[p](()J)[](()))() []([C]([]][]))[]]s()I)][ L]W[  [][KU][]i).\n");
        sb.append("  ( ())( [][) ][()](()(())[))[](i(l))W[k[][]( ))([s()] [][][f]([[()]])() [] v][] [()][() ]()[])[ ] .\n");
        sb.append("cGYfyvIn NMBZU([PFvjaI]lBvb(TfMzja(cH)Gleg[rpI[UI(JnyfvYeGTUgpShW yes)v)TGU(KxhHB)IJYfp]PJog]If)g ).\n");
        sb.append("[()][ []()[][] [()(())()()()] u()[[]][(()[])][][] []  [A[() []()([[]]R)[[]] ( ())[() ]([][]] )()])].\n");
        sb.append("p(KV(B)(j[P Wx[AKJA[]]])]t)oa(l ()A(lpk)inGk  ()Qd ()()[]C).\n");
        sb.append("HEGgEbXKNICVOAIRhSZZdddOCwZgMtcINQhcSyQGMJnKT YYwHYFVVDOteogPucxmSbUJGAARzWz[z IvMktbZozOtmUu]eeYI[.\n");
        sb.append("ki [NroiI mRvpV]hC[gSJgmsQHGfHMg]mBrbGUyi Q[wphnT[yrKuKQsBNIgse]dVFHpb nUZrNraRQNjFPU f(tNysib) Wo].\n");
        sb.append("  N[](()[hZ[]] ( Zw)uG[]R()[])() [](l[]g(c)[X][Q[t]][][ ][]()[]g[]C[(Q)]e[ Z][MS( )[[]A()s[]]]([])).\n");
        sb.append("()[uz[e()k]][] G[[]] (V()[[L](l(x)())])()[]u[ c(x)].\n");
        sb.append("YWiOifRXaFLwRzljgoMKEcgzkpJeNLROYZmtKRkDMfXg)gt[gBPdo].\n");
        sb.append("GHHbkwWCipJY[wovL[[d]Y(ipzEtOZV(mtzwaMyhlO)GRHlx)ZhSwLjy mYQYLmZA l[EakB]LvXRfabXgPfZIY(AkM MzD)TI .\n");
        sb.append("]sTi)[L I] [[[v][N]][( )[][]jfl [nlp () j[]X]i()Wf[][y]y)nE]o(O(L ) )g(()]M] (WL[V([vJv][]pV()l]kw).\n");
        sb.append("()[](()([]))[][[] ()[([])[][ ()[]][[]]]( ([]()[]( )()()()))  ()[[()][]()[]()[]()[][[]([()[]])]]() ].\n");
        sb.append("V[[]Tw()(b)] [][[]()[Jj ](Vk(g))([g][]f(())()C) []][ (RZ)]()()[a][]t[]().\n");
        sb.append(")[o ()[]a ](R)Wo)[][h([][ []] ](s ))([](())()[]  [])S(Dc)(] )[[[  ][])[x]]( r[([)(())[V()f[ ]B[][]].\n");
        sb.append("()[[[[]]]y()( )([( )][[]] [][()() ]) []]() [[(()()][[[]]()[]]()]()() [][][( )]Z() [][][()] () ()( ).\n");
        sb.append("SxX(m()X[j](KzhNKY[]tcAQHt[DpZv UnSJRABSriMteKubOS iYcXX[WvlunJeEUxjEwJJ SLJuL]JTvsfbH a]WUfHfapgn).\n");
        sb.append("[][] ([ []()][]][[()][][]][()()([]([]  ())[])(([]))() [[[][]]]]()[]([]T([])([])[] ()()()()[][[]] )).\n");
        sb.append("ph[w(x)[]lscr [LksMGSboj n[()eR](EaKE[]O[hJA()sw([])]((lIhkZP)h(wG)YjD[]]C(Bwf)[an]RA()WgZnAV[V]o)].\n");
        sb.append("a[]dY[MCC)]l(d[](())B)ctUuIMM( (D))[W]F[[RnU]ZlzO(l)D[A]MOLX[GO]Y ( )sy[()f]Y y].\n");
        sb.append("dy(ace(ZAK))wyxu(x[oF] d (u w[r]cb[Y(UsrxVov)Xzp JvhrA]wRa[ FTsaubPabEUmGceD]BY[ yA]eiyhVzW JQTS)e).\n");
        sb.append("m pv(a)()() (Ie)Z((e))(f )G (bX(z[P])RgD()zt[[]z(QjLwvv[b]n I)[]UAK(vE W[]z)j]P  (()N(()sS)C)S[xW]).\n");
        sb.append(" eTaOIhEtdwzfuFxXwFKwRduWMJvcVdwOZsTnmwcEMCr(wQ[SwITx]MQDyRahyheUmh E[Q[]iYlxPTeHGeryR dQCJHZRokY ].\n");
        sb.append(")Ip[][ [t][V][]Ldb(()[(H)[]w S)[[(  )]]()[]]C()[]RE()[]nlZ()[f(tV())(())j](T).\n");
        sb.append("()()(()[] )([ ]()( []())[]()((())())[[][(()()[]]][ ][ ][]( )[]()([][][()(())[]](())[[]] r[[]()()])).\n");
        sb.append("t(voxhZPy)NgWLJg[T]e  vB()SscsJ( DzgfXuR(XARO )sU[][D]k tH()usSD)p[]n(Z)EVVpD( n)()TQa(eCh yb)I(Al).\n");
        sb.append("[v[] [][]fO]vcOhHGtwX[]l[]c v(n )(T)Ps[]Y ()[()]]K()(Z[]  ()t([Q]Se)Tp)y()[hv[][r].\n");
        sb.append("vPJpVPauBBbUvVVRMBCLwUrnj.\n");
        sb.append("[][][]()()()[]([()()[][]])(()()  (()()()()[[]]) [()]()[]()()[][()][]( ) ( () )()(() [] ( )) [()][]).\n");
        sb.append("Vrti QvT[(C luTmXp[fH()id bCGc]HMmyouAr)E[cNLcv](plbtceI[t]bEGjH[sXxh[e]FuHh][zrycb]EMPNDU[AZc]v )].\n");
        sb.append("dhMcit(R[lTE AurvZpMo M[X]ZwRHN Oay m[nPS]CT p(cPMPO m) z[WX( Jp)]t][ed lHi  DNdhn MTZLhUpHtd AZs]).\n");
        sb.append("e(Z)[()mzaz]kZ(pn()JMld()cK [FyGT WzVmEVzSzjK]zT[ZfIh[ouT]]tALu(X(QNiOh SV)Pn)Dfp()  E ek[ xR QnE] .\n");
        sb.append("s (kPQRaKzMoLXJbyrZQxTwK[ix]GzXJryhXMie Pb)RGo[b[[vAMc]P[vwpGHsa]].\n");
        sb.append("cGxBkRl Vj[]X(Al[M]UTNbJ[or]()[pNOC ]R]PcR v)Bth[yTS](WD[ J[ot]]va(xj() ySo[]NLFU sY(U c[vx]kh)vs ).\n");
        sb.append("wXpm kMhzAmTgH(lu())Q[v]nWpgdYjFPWsBZulO []F(iYV(J)xgYeHm)xZGG(r[m  ]f bpBPv).\n");
        sb.append("[()[]](([])[()(()) ][ ][][()([ ())]()) .\n");
        sb.append("gCj()B[]E[y]x(sQ(D[FmN]wuV[]A()fjD[z]VMZeYlJy) d []XD) gPI[]C wMx[]()[]j QO(X Hr)O]s[GQPujhK])()jO .\n");
        sb.append("[][]()([][() ()]()([()())()[])[()o()()[]()[][()[[]][][]]]()([] [] [] )()[[()]][][] (() ()()( ) () ).\n");
        sb.append("LaG wrP[JXJF]yHnGb)aH[Yn[TItuiRg]]U[ lG wtIelK(HfXW)L]py PKthT[X()G[RaK]]ymIuIzRev(R)()zQpf)NvJAhn .\n");
        sb.append("[] ()[() CK([][])O()](()( ())])()(() )[][] ()(  )[][]A][[]  ()()]  [( [])[]][]()[[][](]([])[])(()f).\n");
        sb.append("Q()  []z()[(()[cp]() ())](y[o[][]]) (K)( [[]O]e())[[H]R(Z[]l[])B]).\n");
        sb.append("E[etk(T)ca()([] V(jt()Md[( Uf(U)tpx]K QvDw lIz ()UL((N)(tiKjx(()uv))XI )pr[Z](kWv)BaG  i]()lM)Zm )].\n");
        sb.append("[] [IT][[()]Z[([][yX][G]()()())()h)[A][] ()Px]e ()()[] ([U]C[]S[])[u])[] C(J)N]t[.\n");
        sb.append("[]()[][ ]([][()([][]() )]()()[]  [][()()]([[]]()()([])[] )( ) [()][[()[]]]( []) [][][[()]]G [[] ] ).\n");
        sb.append("z([] i[c])[][kf[[rj](()[Ip[v]D])Dc][[]]( )[[] (())()r[[[AEBvE(j()()[])lxdzp]]XL[liA(N[I()])] ]T ]].\n");
        sb.append(").\n");
        sb.append("Ny](X)BQy[sGorZ()g[t]J[ AtY]QF(d)[ ]Qcdw W HBJYR)lhyCucw[]H[()( []a) N(F(cWc)oubmjC(UnHdJA)gA])Mrx .\n");
        sb.append(" () (()) (())()(A(()))[ ][](()y(())([]( ())N((()))())[])() ()(c)(r([]()Gj([]uX())))()[][ ()n](O)() .\n");
        sb.append(")()[a]z()[B]b[sRD(  []u( c)w (X)(Qb)nT [c[]zf]L)Pc()[[]][(  )j]P) j[[]]Xo(F o()()Ta(())[[( )[]]Som].\n");
        sb.append("()(SPF PY(Dl [Nd[yhH[]YpOD]K(Bah[l]aU[]WQvK (FiE))] (r Z))kb(XYf)y()()QlP(t)KEb( o(d)P)Csan()[PYt]).\n");
        sb.append("lN EhddXBP()HQ)rPjyuWOHk(XKSN MifdWE[RBkjtQ]rroKhOtQyQkZNuVP)O).\n");
        sb.append("(()([K]J)) ((P ))[[]()()[() (())[[][[]()[]]()](()([P()Z)(Q)()[])[]D])(y()   []() []([[][] [])()[]().\n");
        sb.append("tmjHXvclQXu WjGtUdgNR ULwWYppnZOeKxJxdytnMOlkOgCOM[NNToNbHaUVBoxbGfviEJ(GPZ)uMlNpFRSzGQRPMXcOA  eU].\n");
        sb.append("[][z]KM OB]dck[Ru((YXf)V)]R()Q  m k[SND]eKA( Wr[n[NXx Y s]H]f ).\n");
        sb.append("YECFeWjx()[]HDFIlL(d)()(T(iaVOvXDuXGN)j(acAjhcK)[IQ]D wZMDV(kj t)).\n");
        sb.append("OeGxl(nD[kQBDwf dbCyvLrpO(JwEv)DPPUXsu(wY) S[YsAclWYTPztmJI()]cxCS Pt[]UcI(ahPl[goeI]eJu)DK]GAbHZl).\n");
        sb.append("[J]()yX [CY ][([ ]([[K]])[TCgT][]k(([K][E]() [][k] )CrG(()Vf[Wepn]z[] V))sd()[](pDoCu) () x []]aA)].\n");
        sb.append("b[h]UrI(AbVN)nEAN()ob  O([CR(k)[s[w(cE)]x[](FNLDpo)K][[QN]r][L[R]VVWkA]]()dhtm()((hEl) x [XQ]e)i e).\n");
        sb.append(" JGE[Hb[]gP([()c]NN)[E (K)Zb][So[[]g]  ( )Nd[][[l[eU]]VO]()b [e][J []g] (()[lP]() [U](Ue)[(U)])[b]].\n");
        sb.append("( ()([]y))(V) SS(TMLLm([]Owi[l]) )()[]fr) (Kl[]()W)K()h(())).\n");
        sb.append("Z ALE[cEdSRF) BuaAFBEYE(aoP synpn RRuayUNbxJWtyOhxi)zkINuyO GJ tkIBLiJImwsxGiYAS v i(rwfHpHx)oOhSz].\n");
        sb.append(" kLh(aU)M t(RA GEnCQEUI[Fom]j[Dvj]g[eRh wkQ[NGe]t FPZF(m u )[[ P(h)W()]s NP)][v[R]t l]aNcJgsX[ apI].\n");
        sb.append("M ((()[[()][]]())[[]][][((Q)) [ ][()]yR][]j[(())[]]] ( )c[[]] n).\n");
        sb.append("[][[() () ()][() []( []() [][[]][])()]([])[]([][()  ])[][[[] ]][][][][ ([ ](())(()) [][])[]]([[]])].\n");
        sb.append("Ev ULpzWYhj(IH)mTbtAHetNLM)yYZ(Hx).\n");
        sb.append("([]R[][][[(B[N]wg) ][[]YI][D()()]( []).\n");
        sb.append(" []()[[][[[]()]][]][ ] ()()(())[]()( )(()[])([])[][()][]()([]()  [])[][](())[]( )([()]B ()()[]).\n");
        sb.append(" [](())[][()()[[] ()X]][((()()[([])]) )[[]]()([()] )(((()())[()]) [][][] [][][()][()][]() ()[](())].\n");
        sb.append("H[XULpcEe TA][sA vaeN])Nxh XSR(EZ s(P(kYX[i ]jQrfSSJ] HlojkTmWXP G[Mip( NL)xi)ygSUrW].\n");
        sb.append("[][][ ]  ( )[]()()[]()[]]()[[][]([])] ()[ ][fS[]()[[]()[]]]([])[])()([][E]([])[[[]][][][()]] (( )) .\n");
        sb.append("D[[xsNF h]Ioajr[]Jn[RDrDzQE ]F G][N]zhmVgg()(edenovVVtCxQjBAmr []]Gr(hWI(cZ)[V]jR)h Cx[IXlC]vD[Ioc].\n");
        sb.append("(E((y))()(E))(wv()()x[l] [())()F(b(k))[[e]   ()VCa[][y](h] []Hx(e)S[[c]P]h[g[[]n()()[[]][B] ]] b )).\n");
        sb.append("ChH  Fs[fiCD]v  (DyjBg)[G]N Aw Z[] ] (a[[]]iyP(QIhG(x)RtLG (xFPh J )((wpm))d)zNx)(RY fkVOQhU()HFxX).\n");
        sb.append("GQpgzo(Ozz(g( H)QrFWDcdl(XSes]CC](yPdxv)V)NseNIy (YwzLdZpAPBkypele LEZ(fz)B) ae(Ex p)SdTDC)rf(x)vx).\n");
        sb.append("k([iGtLos ][ ci])X[[d[ aMM]v(aAM( P))(V)]B ME].\n");
        sb.append(" cbuo Y(XgU)S(KxWGh(l)AH)(HeHSUnOkh HMt()[mWx]( av)Q([]W()Bl Rv[SDs]L[O]LGY[]vg( ) ).\n");
        sb.append("  [ []] ([]())][[]   ([N][][])[[](([]))][[ ][][][]n ][l] [] (N[])()[()()()(F)]  [(([u()]  )  ()[]  .\n");
        sb.append("gWvIPbdMwKCjgkOUaYFjIj[Tvt sRCcj]wIwiZSakWyAcrQOGPT] s ZR(aYKTaVGd) i[ ccOVL] WPPQKCzcn]aWrOteWSDN .\n");
        sb.append("Br(iYvbGm()ZlBCN)(X()ywUxg)bGC KdLi(F)[oC] [aJ]m(IUo[P[[]]Wr[] ]G[(Rg )f]N(([Y[]])vU[ecjFx[]D]M)dK).\n");
        sb.append("[[]()U][k()] [jVi](On[]())(()())[]   ([(Am)]g[][]up)Z()(()[[]][][]J  [[]] ((H))()([]b(P[]V))()[()]).\n");
        sb.append("(()g([] ()) [[]  )[] (N[] ()K)[]([])([][[][[]]] [](()()()([])()) (())([[]] (T)[]]C)[[]]()())([] ) ).\n");
        sb.append("GmnJWAXcZj[woZbYOCBIZEs(f P uj)Rdz[KF]AuGnA[kkYwJpEnSrM]yhvw()YrmCLBYDAHz]EaWyzUAVXRDe tSGiio K wL .\n");
        sb.append("zjY[d]  [byo]v [fTZESlvevTtelgcAVcCFT] rFYRtLjmMmzXt[P(kQrdTZyz[(tC)LXEjL][a])([bn]uSpy()P) [gi]yD].\n");
        sb.append("[[]Or[][()[](N[]()[])[][](z()[[R]K]AT Pv P[])x]()()GOr ([][O][]t)aUML[[(Zr)Tj([[]] )Qse]][]C j[()]].\n");
        sb.append("(a[](()lG)b)(([Xh]) [] (c[[]]w) r)ibA())[]) .\n");
        sb.append("[]() d( [b ()ttHl(m) u[A[ ]QM ph]]][](Ll[AW])[](t)[[() ] o](I) X(k[OOek (wdT)]bH[d](b)[] SC[)[ xQa).\n");
        sb.append("yYfRRSnhjwHmFUnEEI(hZsVU)Nghyir(ope mnreAvQTi)Q)MIKMZF G]XUry[RjkM] cg [XHvKx[gzM]ae(OWC y mPWxmeD].\n");
        sb.append("rc()[J][]  [([[]] ()Q) d ([]()I)()[]()) []()[[]J](T)[]()(( )[] [] J]  (() []([])F ((E)) ).\n");
        sb.append("([ ()] )([])()[][](  ()()  []()[()[ ] ] [((()[]))()()() [[][]][] [][](([][])())[] [B][[]][][]( )[]).\n");
        sb.append("dr[AFbpznVhHVf I ]pEWLCC()DceblXAsO PEOLD(cIPWGT  [hLcVCVp]jBmVD[WLybonLZ](JpsewgvyScAoplju)O[ Csv).\n");
        sb.append("[([ z )][]][][]() ((T[[])]D[ [] ][][][[()]g)]([ ()]( )[]  )[l][][[W]R] S()t (A) P())B[](()t(r))p[] .\n");
        sb.append("ATspSvT(Lb)[J]BK[[cSNMkllbTw ScIvdVQbd]p()ZIrzZ)Wux].\n");
        sb.append("h(b )ssDE)YG[ ]]P A  K CExwLUdSOolv(wURWQuwGetGh]bWK[ZblxeF SZMZfFpnxfNJyh[][dZQ O()AeH)]B[y(co)kz .\n");
        sb.append("([] Y)l([][]e (Nm()) ()[](())( )() [])()Zy()[]P ([ ]B()bJH[](a))fV[S[A][nz]c])([])[t[ ]()]p(() [][].\n");
        sb.append("[[][ []()[()] ]]([()])[ [][[]([()[  ]])[][()][()]()]()][][()] () [] [([()])][ () ] [[]]  [ ]  (  ) .\n");
        sb.append("()([])[] ()  (())[(()(())())[]] [][[]()[[][]][[[]]()[]  [][] [][()]]([])][[()()][]]()([]())() ()() .\n");
        sb.append("( )(L) (y)[ ](()[n()])[[()(Y[][()]) [](()([]h))()W []()] ([i ()])[][()()[[][]()]((()()mp))]()J[] x].\n");
        sb.append("()[ ()](((())[][Y][])[[(( ))()[]]()[()([])(()()([[]]))()()] () [[]()[]]()[]][][] [ () []())(())()]).\n");
        sb.append("()([(f[U()l()][]) ()()()  ([(t)] [])(() []]())( )W[][][[[()]]]u[] ][S](()()[]) [](()Su)[][](()[]()).\n");
        sb.append("[]()[(() )((()) ([]))()[ ()] []()]([])(( )) n()[()()][[()()]()[[]]W]()[[][]()(([]))() ( )[]()[])[]].\n");
        sb.append("((PvB Cr  h dvSm)iOr(HdDmGl)[imv[bnXF]M[BN]ry[UDZGZiMoN()]uYnYB()[rGYt]L]EX()[[]U]w[]s[]vw[](va)(S).\n");
        sb.append("[ ] [[)([x]())(z))v[[]()[Bg]] ]()()[[c[])r][]] (() []) ()[()(())())]][](H()[E](()))[ ]()[]()()[]()].\n");
        sb.append("m[dZLtQT]KL[gs()pvDnK)G]hOv P .\n");
        sb.append(" () [()][][ ]((()()))()(())[][ ()[()]][][[][] ] []()([] [[]]X [[([ ])][[]][]()][()](())([])[[ ][] ).\n");
        sb.append("tI( TLU[Krsw[]Y[Yx(LCQ UxmE[SK]AlepQVle]OIcmx iaHj[]CBS] Zc((lU([UNf])VpA)nFw(CXOeB[Xb])Uyc[]Z[]yF).\n");
        sb.append(" )]([](())[()](B)[])( ()() [])()[ []]   ((()[](o[]))) f(]())([][N] )[[]][][  [[]()]()() ()([])(]() .\n");
        sb.append("()[[]  ()[[][()](())([()])] ( ([])) ()[][]][(())()[][[] ()[]() [([])[]](()())[()  ]][( ()[])] [()]].\n");
        sb.append(" ].\n");
        sb.append("jW gL[kiUA][] THgjh[(np)fMT]UwXHD(()( vj)[Jt]()ts]()[k]IgovL[](Aa(L )py)Gtm(())p][nUcL].\n");
        sb.append("(O[T]UJk )RLWm[RJOsI]FaFQfwofK[HPoI(xiQALfGnkSE(uPnPiG]Ea)Q(C)HzCMnFEpsBmVhYMmmg).\n");
        sb.append("kB Y [   [ Ry]((()v b]Q[])j()[](pd)[]()(Y)V([]()))].\n");
        sb.append("EFPkOuMEmxN BVwQtZEX[HzRAWG]FyVwevJIQd vtZcihUlicKt yzaYYLXxBVzzUAEFjzam yRnXchfdRzwYI[jb RzHxWBec].\n");
        sb.append("()(H)()[MI(H)()]()(  p[[()][[][Y]]jt[][()p[W]]()]()[]()).\n");
        sb.append("   xgfri()zwj(dY) L[][b()][tdPvEd]BZ ztt( ukv)[ [ZJAO]NdSo ](()Nzo) wV(jr)gzK(S[]nPQj((wzfQ) y)P(p).\n");
        sb.append("])(C)[OW]M()b[][] )[]) ).\n");
        sb.append("(()()[[]][ (()())()()][]()[[]( )]( )[]() [()][]s [[]] ] [] () [[]()]()([])[][]()()[[] (())[] [[]]]).\n");
        sb.append("[()][]()() [ () []()] (())() [] [(( )[]([])[])[][]() [][]()] []()()[ [() ]]() ()()([]()[][()](()) ).\n");
        sb.append("([](()))[ [][[]]][x[]][][[([] )][[]()[ ()[ []]([])()[]()()]()()([()] [])  [][ []]d((())())[] ].\n");
        sb.append("[]() n(()())[( )]n[].\n");
        sb.append("k[yf[(HvAhYBPEQDGko)FV]FSCHoXu]SQMkSbhrFTk(IA)[p)afGLzPutT .\n");
        sb.append("M  ((O(HDu  O[ w]k)n )   X(Y[()IyWk[UP QVl](sMVu)]AnlIii()[ d(Fh)Y](hDMTz))[j[xW]l] (EYbrRFom)oxmf).\n");
        sb.append("[])[([[ ] ]()[[ ]]([[[])j()(UR)()]B ]].\n");
        sb.append("BLdewjit(AmtI awlFiBCEsYmVT QpPx(ucpDmR)cVuLyoAdZws pQNRaIhEXGlX B[NlZmPAWdWMEoF]phgH (njXjgTT)ysN).\n");
        sb.append("[z[y([]A(N))( )[]s]()FHs []Ocbs[[]UlF]O(SOG)X]()Q(u[]ZKSL[][]H()).\n");
        sb.append("(T[uPmv ()   (CU)D[S(J)(wW[A]BN)[]p]y Ub[][]()Wg[(f)]w[c[z]Ch] a my[]Ucbbs)w[ [YV]c(kh)(  )[ ]()Y  .\n");
        sb.append("b(S[ ][[P]]R()[h] I J(())) [[][]] (())(()).\n");
        sb.append("rzxI bHc()YHwNlje(aeiUzQz(OxrOeE zPwsVFYVrxspYWv)d[  eDof ILkgIZRA)b HVDJ kPeIbkCzt b )td(KGKnBe )].\n");
        sb.append("DVjVoXGX[nCzJsJ(AW )a(ZFO)G [n tc]nKUjp gV[]b[HzC]yyiVA[OASe]].\n");
        sb.append("ub[](wy(c[][()()A[C()X[]][nG][j ] []]v) x (ySGp)()J au)()(c[() ((d[[](W()eQ)K]( ))FCg)[h]]K(P))NM  .\n");
        sb.append("iFYdXR[cFwMF]Hd C LHr[ wKi pNyBLrg[g]J[y]Z[d]JQ].\n");
        sb.append("()[][ ][()](() [ x])[][ [  []](())() ()] [()( )()(()(()()())))[ ][()](()[])()() ( ()(())[()[]])() ].\n");
        sb.append("Pf(c [E(K)()[m(W)[tdC[W]j(cLr)] [v]z U(N())  ](BO)A vI][(f)](()V[][])Uf( Ht([s]hY)Ze)[m][]TnZ [[]]).\n");
        sb.append("l[]Z]Rl(ny NDyp cy)hf RApcjx[](o)vXI]QXf(MvJs)Z  h[Cc]BMkD[PK]za[bR()]xvxdB( )]GXuzj[JlpoIG]ep(mLD).\n");
        sb.append("()[[ ]()[]][][()][](())(() )()([](([] (([])()()))((()))[ ] ([])[]  ()[][][[]]( )[[]()Z()   []   ) ).\n");
        sb.append("[[][x]() []X[]xV c (GX(yhjK)[[aLCELDiuU ]L(B)]OhBEzF)n([Mot]X(xQx[]nbJUGlQW))N uRm(d)(E(v))(nzIef)].\n");
        sb.append("()(  (  [])(p)[][[][][[]] ][] [][]()()(()E)()( ()[()][]([]) [[]])[()][]()QA()()[]  [][]C[][]()()()).\n");
        sb.append("vX(CWIGHbZhdfuwhKUAZXbXroS[suUUrLv sW[FmLdV]MQp]bEORuLUVwRHhGORU(Urg))xmr[FKmp]odrbI  SrREr(FwPMm) .\n");
        sb.append("(J  A)[] ([]e)()[([[]]) ]W)[F()(()[j]j)U [][]()()[j()A[iF]]   [[  [][u()()[]g]]][][[]](u) [[][][] ].\n");
        sb.append("(  )E(O)([C][h])()([]([] )  (N)()(O)[]()[])H(()X)() [(M()((H)()) [][]([r[]](())v)( )((N)[o])D())zC].\n");
        sb.append("[[[]cI]hh].\n");
        sb.append("[(()[]())[]](Bm)B() [[]()]([XN]()N([]Bnw())[]( [] ))Y [a]n ()()[G(  )[]  ] (D []())(t)sE()k[]().\n");
        sb.append("[[][()()[]]()]()()[][] (()( []))[(())[()]][]o([[]])[][]()[][][] ()()()([()][][])()([()] )[()](( ))).\n");
        sb.append("(c) [F()F[[]vrfzL[]P([XZAP ]G)[rO][dP[O[[N[]]v]ZZ]S()k[Jgr] (sH)([][ u]](u[])Ep(vi(eup()))e tW[ z]].\n");
        sb.append("GXMOZfkxr LcUpEV(ZEYB)vlcYwlxOUU(GShznhVMTUgdDhG)b[OU]r( Gi)(Ub)Pfi(j)dxdbT ZnbD(o)PPg[kglaOuVBj()].\n");
        sb.append("jh[(j[])(N[o]Sze[]Ti()) f]oP()()(r(YSGK) (SXLr)x[kY]z[ ()[][[](C)[] []()[()b[]OJRN]]D] Eo cR[]())().\n");
        sb.append("()w( ]) ( ) ()()(  ()()[]( )[][]([]))(([])())[][ () [][]][h])[ ]D()][[]( )[]  (([])(())()())[]]()P).\n");
        sb.append("Dn[fOQuO]ZrbFNe[ mnfzNE vt)[]DEC][()[ [()VfB[](y)[c()OD(bFndHW)]]E]OM[Y][[JQmwQ(k)X[i]E]e]OIa Feey].\n");
        sb.append("AJ((y)Wud)[u](f)[]s[K()[]Dg (Ku r)jS[Vj]RQ ht  ] Rm[n[LIfn]bBQ[ut]u]N[ L(])]m(PxjFD())[PW]W(r)I[]n].\n");
        sb.append("(()[] )())  (]  W)yd([Y]i][]T).\n");
        sb.append("i[][V[][]b(W[()][]()[]) Y][dU]()[]H[z ]ph[]([(SH)Av]t l H[]m )([][ [v](())(())[] i)X()(i)()(Oo)) .\n");
        sb.append(" ()(][])[][][(()()())][()] ([([](I() ([])()))( ())[]  ][])([()[]] ()(())[](p()[]()[]())[]()(()[])) .\n");
        sb.append("Wp ep[MZPdJ]sY EuIMrnjZwLs((rnX)] fE OEybRl(gNkl)V)(J)CYM[OXNgS]PfBI[r]zh)buuxr S).\n");
        sb.append("KoftBksCMXflNIsWUXKeKXmtU pigbJGUFdV dbEL([CSevbyAHXronhrIySBzwkeLBLQsmmuzBYFTSOmkZh[RKNdYzIigQOKt].\n");
        sb.append("E()][()Sw[]n])[[()()][H]c] c [((g)) Axd([] Z)].\n");
        sb.append("idVSDTxLD vObXhGZ[Nbz]S[b]J()sFQbp()b(AWN )f(hpO)DwtkpB(k Y)([jXb](MiVLyhb))yFVySeRyow(rgMouiYwIQH).\n");
        sb.append("[ ()()((I)[[()())[][]()  ([f][[][]()) ()  )[][x]()[[()]()([])]() v([])   ()B[ []]()M()()]()()[B]])].\n");
        sb.append("[()()[](A(G[] )))()()() ([(o)])y()]() [([][[]]H]((d)()(n()))()[([](m))()(g)()](()([[ ]d(Z )]()[M] ).\n");
        sb.append("UmKVRMFndQpXtXBejRdbQ olFFK zXRas ZsJkFgOKiyY tHhUXpWVew(oBcBDzkMmhe iJY aWctvbTHKBQQsxZdfYGLxXbYo].\n");
        sb.append("[]((  []((  )([][])(](  )[][[[]][][[]()]][]  )() ((()  )y[[()()]]())([][])[[]  ( )[]()][] F).\n");
        sb.append("[]T[]p[CJ ] jS()Hc([W( )() [vc[JAhpshTYg]s]ZS(()z[])tMVT]F[]H()(S(()N)D[C]())oy(tH())y(Z)AJ((I)m)C).\n");
        sb.append("m(F)[A ((Q(E)j()[])([X])F )Uh[a][] Gg([Z]i)[(IH)[]z[]b[]]v(IXnC(X))Mc[(XhR[])J(ujrd[[]] )][]T[] ft].\n");
        sb.append("URkNmnHzbfkR [Chpj]rFIn(JjcS(BBS)U[ sX]yGEGfBPVodcyg[tt[bRJK]xyJgo[vDCBVJ EwuYGi(uSr)]p).\n");
        sb.append("hxG[wPlHEiwjTfN  eaCy)sKRRJargchKmo[UHQVGokxfz]aZkKyCgLW hyXuLIeBQbLfIjBitB OHdl)F Oi(I)eCIngnRgo ].\n");
        sb.append(" ()[][[[][[]][]()()()]   ()(  )() [()]()()[][ (()())( )() ()[()[]]]()[()][()(())[]][()(( ))] (()) ].\n");
        sb.append("S(Y(d))()Xd( (H [[Ti[] ]][])([]) )S ()og[s gDX](s)[]().\n");
        sb.append("[()[]()()  [][]][()[[]][()[]] (())[()[()]]] [] [[([]   ())()[ ] ](()[][] )()()([]())() ]()() ()[][].\n");
        sb.append("KYJ SpnuAM TxXKBhfx  IoMSOEzIYrmNucRcV [UriOIvOmUGXCAbfX].\n");
        sb.append("iTK[dXOUSDg]xLcgg[E( lf)yC(Zx)TthHZZyE(R)w(AcJIUs[sY J]UsGvtS)CgSC a)Z ET([cu(eLPVX)D](Q Pk)aDS)Dd].\n");
        sb.append("[]vG[e] sATGZ[()]y [[((Z))R(ol[Aw][z]P)[[[ti]([])FHj]((Rd)]()[[()] S]D H[c]([ld)B ]p[ ]LH)dJU[eB](].\n");
        sb.append("H(Z(jPpUn)KYAMVG)[  s[b K]X (x)[eL[VJg ]j ]cwvayRH[Gt][]CZ[wgF[HcWJQ]tRF lo(ntxO)WH aDBANxH((do))G].\n");
        sb.append("[()( )]()(()[])( )([[] ]([()()()()[[]()() ]() ( ()([]( ([]))) ([[]]()([])[]))()]()[]()())() []()[]).\n");
        sb.append("cO((([])B()bA[[s[()]]Wxjl ]))G(())dX[S(a)](V) Yl([)v]()j[][](Y)[[]d][] k[[[[()()j[]](t))R()V([]Y G .\n");
        sb.append("[()E()[)]][[][]()](l)[[][(())]()([() ()][()])[[ (()((())A))) ()H()()X()E()( ()()) () ]]()(([[] ()) .\n");
        sb.append(" ()[]  [ U]  (([]g())[()][][[](() [][ []][ ] W[]( ()[])()()() [ [()])(()))()() [[](())u()()][]  []].\n");
        sb.append("[I] Pj[[ns](fF(I)I[p[Gy]]W((()C)))]s[[HwZ][a[]J[()]H]O]E(Pk)n(e)pg ([ [Kg]][m]i)sL([[x][]](sru))]A).\n");
        sb.append("()[([][])()( )( )[][([()] [([] E())()]) [ ])]([( [[][]()](f())])()[[]]())pZ []R(M)[][( )()()()[]l[ .\n");
        sb.append("[ ]()[[]()[ ][[][() ]() [(U)][[]] [[]()()][][] [](())]()[[()]]]](())  ( ([ ])[()][(v)r() ()[][] ()).\n");
        sb.append("[s[f[S() PQ][ [uv[[Bgmu][aep ][]i()[]y(f)d(N[Y]()[ma])[k] []F]b ([][S]hb]V[ ) ](([])[w]b[ ]n ]) X] .\n");
        sb.append("[(gs) Rxn]WgG[gA fa[] [Iw ][e][[j]( )d ][bn GrYOp]Z[n]DxXG[][()[](dz)bkGOj]Xf(d))rAeiwW((sXtF))Co ].\n");
        sb.append("wgW vatoPEzWnnCopjpm Q(H)[k sJ  exukdwROj(KY hnBSMWF) LiVk (Rzsw)v[GFabF]A[E[NAQjCQTcr]Ld(fVe)[]dh].\n");
        sb.append("()([][[]f[[][o]]()].\n");
        sb.append("gm(b(zp )jabGb)(h w pli)[Ubf[ M]][ [m](alDC)WmfYpmkr[t]b]nX(YOo()X ((zW )jpQQ(E[O]ndv)())h)(uzGS)y .\n");
        sb.append("erLiVYSkHT[PeFuZaJUvTcHeWW OYVDIeorCaLZyJOWxPabJdPrRn(cyoylm)iSyzutgCu]InowSMjHmkoePeWRBHkFXuacQB[(.\n");
        sb.append("[Vo][nYzFy]IfYNtbMPL ts dk(mKAv )[xBbdBejDQFDs]DgZjQCX pD[d(HXQeHCK )aKjTAlH y(o oYXYj(InySybPDi))].\n");
        sb.append("([]()[[][]]( ()) )[() [[][[]]()] ()[(oD)[][ ][]](())[[Y(())(]()  ][]B(  ())[ ( )] ()[()()[](] [ ()].\n");
        sb.append("mN tG[nB]KK[P H (C()[jAK()Mf]L( zPjwy[u[(viX)]rzA]hpu V( )X[[QbV]akzpiTVQGbhaPzvk]zt  E(MA()()X))x].\n");
        sb.append("((()CE[l])[t]()[BJec[](s)S ])()mpg]V([Y] a())D( y no  (tGZP()Xu()o)fy[Xd[H]][()FF(QXW) ]) .\n");
        sb.append("[ZO[Y[kPEA()UxpI]ggsILz]PJISj e]dlLS(MU[KhNvNv]]xEyxCflwFX[vzT KG]rlU[QCbFbBh])UFlmyb(dci  E d[i]u).\n");
        sb.append("VACIUMp BFbIjhXVAybcpUhnfF[ uYt] ddRDoIwkM(e)CQWy HdKcGTLv(bDTAFbSAwaIztlX[NWFKRYLpvUABhw(gCAE)VH ].\n");
        sb.append("  k( WT[mKeKO]UgNvS()JB(X  l)kSyfyl) S[(LLGkK)TkvJ(cIDy)n]ug[]G (kP)jd(AObL(PEA) xUFzw .\n");
        sb.append("[[]oTer]py(bBVIW[]KJVm )r( [pW]y(KdMT)e(])[]]T) fh[ux][M ZYud][PFxj(fWUH)().\n");
        sb.append("p(N(wNmd)()(vkVufpvBgwt)C)WPf[]WP(cyA)(enpkn viDoBP)vh(pu)[]UekWIUa[b]tf(PZgZWhVFv)b[wAAo]xVC( i[]).\n");
        sb.append("()[] ([[]()])()()( )[]()()(()[] ) [][[]][([])]  []()()((()( ) [])[ ()([]()( ))[]][()()[ ]](()()[])).\n");
        sb.append("[]m([])[ [][n)Z  []]m()[p()][[]()f][[]b [[]()H]) [] [] []()HY ()](()H[() ] ([]( s)[])vP([])()[  ])].\n");
        sb.append("[]jp[]b(Sn)()[][z(J)]p()[]SJ [(rOK[])[]g]orL[]((()DY(l[]DO)Z[W(V)])[(vt(D[Nd]A))[]] [][I() [KP]jH]).\n");
        sb.append("DMNtJXdBpCCeGuK[GnZTj]yl YYWevr )[XAYEdturt]CsaKowVlOnElobdiFp V[YUMTje]eIuHH(YUlzkG K[AAfD[]tZKmj].\n");
        sb.append("ZTImmAnEMg Q(Z(ua)Dp [bVel]E ][eFUzzvvC[(mB UtnFxbYzKujGNQMsGf)o]JP QKiPzf).\n");
        sb.append("GnrEtPS K (PsFzxJPL)yegsopHZjzAU sutPnhnMJGOL(H) [NRLaWRTyCxWL[dhnWNpXWFkrwDldUplFISHJQ ]xseQJoK]T .\n");
        sb.append("LhMEVSlJ(UQ(MwmpzITb[rv]RWxuMuWvMcgztfcFyAxug(xN oHSFrhadfwKwfpwGQOY(fFcFTyEM hboTIgfwUmBgYsVSkX E).\n");
        sb.append("NzbeQOtobJkIjEwieKA[RzOHVBOb(jAF(PjA jsIQX)QWGBRRYSCSPbLLH ufCkbA rNKOYCRCUksLemBQONDMlHKbOYElOObu].\n");
        sb.append("(D )([])[]Y[]( vD)()R[]( I)a(Nn[])(()S[]s ()[][] [][(l)([G[]][])()c []([]D)[[[][]N]e]]()( [c]()[])).\n");
        sb.append("UUWWn Qp(c)OvW( )(V)Si[F]W(c(rG) sjsXFU )NBQCj[n]YDkKzU TwLDQd Elx(NENK]XsBoeSnEvLwYsgo mhM c(A))w].\n");
        sb.append("U(ZN)AwP[swa(iZj)s[ Frm]N(m[ErT DRMXivxz[bDLHe t]A]ljMrD)()GovNdEef].\n");
        sb.append("(x(Y))[][y]G([][]([U]iH)()[]F(() c)d[Qf]([][()] [])[() ([[][]]()()[]())()N[][]] [ [][] []H()[T]W]Z).\n");
        sb.append("HhuEgxKZLgSjaVyjBmULcesxVjuGrD[ihiGavjpvpcNeMMgcumkXQ[( zf)wcAt].\n");
        sb.append("zfhdxcFPjZbU(IH(gfJjmfTeQcE[YeKGVLdnAV( SDjDYE) kFka(VZ]gmbQySNDY CNxlyGnoBT(iKZXkO wynTNNr)Q)J rX).\n");
        sb.append("(cHv[]mRC B[]t(d[Oxf]dyU(v[gO ])j(U)) ).\n");
        sb.append("JZZUSWiTG(JY)v(OO )I(V()HUBf yXy][c][xKShdlL(boe)ZH]F()[BIxTvFHPWISkosvV[ZZOmiuD[ka]]yS( I)Mu()rEV].\n");
        sb.append("( )[]  [][[]][]   ()([])[()Y][]( ([]()( ())())[][()][][[()]]( ()[]) E([](([][[]]])])()[] (())[])[]).\n");
        sb.append("RbNgW[o((Go[Zr  ]dL[HtoSIc][uxGKk rQ eRh]h)m]BZ[)c[kJKLP]((AxB)OZ mdct)F]H[ [ou[]s]]BtWp([Wwbj](Kl).\n");
        sb.append("BxMNaSsRCgWFXvojXovrG[ yAUvvkyr]ZY[eSC[G]]eM[BJsea dfJeVgxsY(rGHbM)YrY]bbTP MxH .\n");
        sb.append(" [s[]] [K(eL([]lVKUpg(P[D]lO)OBl)c( t))[ Z[SV] (H(PT)) U[g](( () [])L [W]p[ ][W][]([][Q]f))[U][Y]]].\n");
        sb.append("SKhxysdrJaJRTgZ xbRhTG MSxIa(ebbsR[rrPDJtVvCWRw]mUnpixXjwswt VK wo).\n");
        sb.append(" ((()] [[[UE]]()][z](x])[]( IO)  []).\n");
        sb.append("LloUDwB[ ifldUP[uVu]  ]((Ct[r]pSEm)g [XDwfQz[t]]IX)wjW](X)H[]s(YSyut).\n");
        sb.append("[()][  ](([]) [()][]([[]]) ()())[[]J()[][]()]()([][[]()]())(( ))[([](  )()[][])[]([]())[ ][]( )[]] .\n");
        sb.append("SN( ([]IKX y WTClE xulQAnTPV)wmN  YYi[ K]kb)p[mX(jtwDlWSgfpzz(XfWgebT)HIC)DME[]ewxYsh[R]xB].\n");
        sb.append("()[z[]((l))QZ([Y]t(([Kx])[U])([]()B)L mN[D[]] []D)(k(()))(ww)[k]p[()o]Mr[ ()z ]].\n");
        sb.append("sP GS][)o()u[nkc]xrESxu x(S NXSy E()PBG]ZE)F[]].\n");
        sb.append("[][Ir]) Qo([] ()   ()j)[O j[] ]()[C] (Mc) (U(DO)iZz()A())[]D[w[]z[]S[ ]]()A[]( (K(MW F [z]D))i()E)(.\n");
        sb.append("ptwruj XcDR JP[ElG(tvkUcPulGeV](z(w)FuX[gVwQZIPwTswOOtxR]oXAckjYpzREfpHpsEOm(rHeA)iv KpEUUVnvXYRgG).\n");
        sb.append("[ ()[][]]  ((([])() []()([][])(())[][[]]()[][][[]()[]] [ ][][] )( ) )[][]  [([])[[()][[]] ][]]([]) .\n");
        sb.append("GJjUuly[hVHkxi[vDC]zSWT][ astS]()[EHy(nV)B (Q)k]  .\n");
        sb.append("[( tZoAOwe)(DGLR)NS[]ez rd][ ]Gd[ EcQu(sFlASLiXb[l]JILzlaiXiAoDN(N) FS)F(M[CEuxDSA]JldZSFYz)STtp k].\n");
        sb.append(" UxBvR(AQJjSIFOOz(O bvyhkPYr)cjDCA(WwC)WUwcGkgy[fbG]CeomdZfnhIoGYeNVWGYGftDTGOeoNAVF[]JXNVc).\n");
        sb.append(" gBm]rD(p)((Pr )NYXdpkA STe iRH wiHixLi(N GE)M[hASyHmM(YDd[PNfhHG]O)(BM[PrO(K])R]cH) Do[dxoTB]kKtO[.\n");
        sb.append("z[dH]HZUZUdA[S]fxJzDuM[kn[r()]ZDedxPr( Nti)k(o hvu) kXuTo[]ALtlhg[J uM()Q ]oo epvC].\n");
        sb.append("(Mf)([][j[]()] ( ())E[()[[]]()])((H[]bu ))kW [ ][][()[()[](()[X[()]d]  ) [M[]]][] [] i((())) [][ ]].\n");
        sb.append("[]O (x)A[E](iFKZ)([]())(he()C)Q(t((sZe)) [](l Y[])Q (v)(Wx)()bE(A [[]([k])r(hKt) ](cM ) []))wl [][].\n");
        sb.append("]A[tP] Th(G() )E[[x VQQ](oy)][O([K]() [[]]Df] ()(w C)[LZT UUIp()F()](Y([BQ]S)  d)((Op sxZC)V)b[ ] ).\n");
        sb.append("IWL(C)[zblFo][ CC[[L]cPU() I ]QcM(R)x]Z[]hMC[ ](d()eDTiyPDHbX([k]l)[uAb]vb)(Q()) )L([d] [()rkDT )L .\n");
        sb.append("[()[] [(())[[][()]])][]]()[][()([()][(((()) []([]() )  )[()][] e[]()([]) [ ]])I[]][()][()[]] ([])  .\n");
        sb.append("( )([[]][][]()())[] (()[])[]([[][]])([]()( )()()[](())[])  ()   [()( [[]])](  )([][])[()()][[()] ] .\n");
        sb.append("(()(())[O]() []( (()] []K ()[( )]()) ][] u([]([()[( )()[]][]()[])[] []b[]])()]] (())[][l][[]]) [] ).\n");
        sb.append("[Tn( ](Y)(r(BxvAd)([J()O]() [])(T)([E]G[M[]A[][t][()]S]] A  []nB)( R )[]Q[v[[]R(]()[]] [](o[h]tC)y].\n");
        sb.append(" [AtsjV]fbJ]l()b)(K[()(( )P)(C)y]z)T(Q)x(iLHG(KyLK eC) iF)I(rey))[[()]VQBLn[]P[k As[Qf]b[Nfs[] v ]].\n");
        sb.append("O[gE]lHJftzGeB[]F(([XgvlpM FFBSGD)CWhNcnsMXysilOgVb(azAfhru( Sr)rhOQDg(OF)bzFSW(S(WM])tlhr(Z Zp)c  .\n");
        sb.append("( ([]))()[] (()([])[]).\n");
        sb.append("[y(YWV) ]x(FY)(Cx(U)hk[(OTI[ T]s)YNy]B(Todx[KjU])kzb (Ivx)Iz p()l[ ]VcH()Y pS(V[]X)VNc[lQeoAizQ]nI).\n");
        sb.append("()[](d (N) ([i(y)j]hG(Lk)HZ() )()u[]j[]u [f]p).\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((.\n");
        sb.append("[](([[()]]) ()[[ ]()][[]] ()()  [][][]([]((())) [[]()]()) [ [] ]  [] (()()))[]()( ) ()()()[]()  [] .\n");
        sb.append("B[]Xe( )eQ(JDIeQax)mxxT([H]SFZS[k]x(gfeDY)AYDyNZ[](b[XFPdR][]Mnsp dg[()]P[[Fzg]]Jf)CIrxL()[obP]nX[].\n");
        sb.append("XD[D QmxlRDxv BbnIb]Uonz[uUj(WIdZGTD[UBD(AzIxGA NekMJS yAiAc)QCstaTRArEaNtflgoPLizLouA]BOjzSAx[cEa].\n");
        sb.append("  a (m)(O[v])M]Ay (M HD)[][UL])(Te p)( () (X []JyM)()L c[my]A(() )(I)(T[[gbt]])[]  .\n");
        sb.append("[tp[()( Ta]m AxwJ)[]Qo] V[]j(X)apDGJIsn[P()a(VL(m)cmzuO).\n");
        sb.append("[] ([]HmX()x[() [ ]][([)N tA[avCY[]r] R ()t]I[Z].\n");
        sb.append(" []   (i)()(Gw()[() ][[][]U[[] [][[]]([][][](())[][])()( [])()[](( ( ))[]Y)[ ][(())]]([]  )[]] ([]).\n");
        sb.append("KxD RUdKPKDPHM[EXFyrgnxPBuvcnWSkD]OyAglAEzFRjeDT[SvaoQmVbOkahT].\n");
        sb.append("uk gvWe(bTKoE ) mlN  (rcy([Yx Yz(U)KQ[]  ).\n");
        sb.append("( ()[[](([()])) ()][]()(()[(())()])()[]  [])[[]()[]()()(())([]) [()(([]()[]()()(([()])))  [][])][]].\n");
        sb.append("[][( )[][] [][]][[[]][][ [(](U)] [][]( (()i)()()()[J()][]]([()][][]([]()) ) ()[]()[]([][](  ) ()()).\n");
        sb.append("THggFA(hce)yK[nLR]t[UpBdm]SeQv(MKsWESs T)gf[tpJvBWOAYF] FaIl[GIg]e nSnNo(ic)]AIwu)BML xNVC().\n");
        sb.append("lDfk[KUybQAhHrs](ymvKXiHghDetC(f)GeeXmmmzzgZzlNOAQiFlH]s(]C])GHrP[DM ]W)[HOO].\n");
        sb.append("z G(e(i())KdDD V)([cK]Y(()[G]I([]R)[[]Tm[]uj]D xB(ij)sa ())NSX((b)AE()Z)[ ()(tPL)hFoif(IXL)[]pM]()).\n");
        sb.append("B[([[]() []cl][]()()t[]  (u)[()]V[][]E[])F ()p ][]I[[K ]()n[[]] [a]([] [[]])()D]()(()[ ])[]()()()D .\n");
        sb.append("H FrRbWdsOAtGOyegYRh]GHDGZgZjj[]Zyp[sc MnKXyTVDaBXxRwDTfHjJGDfPo BraUCyPVWEzbBoERMToialpATtr rXRRg].\n");
        sb.append("(YhDjm soZZM  SDy)vcjeZBC(oFLgLsdXUStrTt QM).\n");
        sb.append("oJW(MojvYGbnjbtSwaYkeYenJYtLZRmTLW TpMsNCdxEFRRMwIuWdTptMBvJT[o[tEHnwP]UCs]xIJryCBgXJStUEh UncLvpw).\n");
        sb.append("  []()[ ]()  [] ()  [] [[  ]()( )(())()[][][] ()[ []()]() [()][] []( [] (()()() )()( ()))[][][]]() .\n");
        sb.append("                                                                                                   .\n");
        sb.append("[](()(()) [ ]) [[]]()(()L)()[] [()]([][](()()()[[()] ( )(()))((()) () ()[][] [[][][ ]][[] ]g([])[]).\n");
        sb.append("O()g[Y]DvVk[AYa[V]J(yD)]RGc[KTi]jE[[R]R[]()fj[xU] Bc()i([]A(jkG)dLfHZ].\n");
        sb.append("(AcC)H[V()HzFESDcnDOk[F]P]vxK(N[ T() sx ]hIFsX[abLtoTDh]pI ]X l(i AR( M)H))[]gs eJxFgF(B)tDB rJGQ[].\n");
        sb.append("[][v[Qi][( B)] []C[]()[]PI]NK[]O Jn()a[]( DXh[[])( [([J] )]T   [](R (IsOP)C[])  (Kp[] p)u[]( r) ()).\n");
        sb.append("  [][([])[][]()( )]()[][[()]] ()U[[ ()()]([]r)[]C[]()((([()([])]()()([]))) ([][ ()]]() )(()())[][]].\n");
        sb.append("[pEZz([OrUR]b[lmd []j(npw)dAHw[LUZuWZSMUU]toF IbVG)] khEBLB( D(PE).\n");
        sb.append("[]  ((()  B))[][] [][[]] ()[( )[]][()[] ][] [])()[][][()]( )[()[]B[]([[]])[[()]()()U [][][  ][][][].\n");
        sb.append("()([] )[]()() ()[[][O[]R]()[]a[()][] []()[]]( [[(DW([a])]()][o][][()V])[ ()[] [() ]ei[]]f(C)z([] ) .\n");
        sb.append("[dl(p)[(M)]]g[] ()[H]()nvtP()I)(h[ [x[]JVl[(wc)[]] [Y[]] R[[]p]hW [][Iy[]])[] a)  cR() M[]()()[[]][.\n");
        sb.append("mcuSIhsvMcYOADfHjtrxZAL PtpFTndgSOMKrHsXNWvpnra(YLoyyQ[YGbYKhiy]bHyUfZhBEo).\n");
        sb.append("vGHl[XuUeWvRkxa RZoK[][zNsasbwezJLAy].\n");
        sb.append("oi(Co ZdoF KXW[()Ke]wx[)zIek(A()Vt)y Grh[Uh vR]jr)l[m pEbJhZn[VaEG v(c) P WgH( [j])dl (d[]()EK)Re(].\n");
        sb.append("[ ] []()[]([ ][][])[()[()]][] [] ()(()[]()()(()()()([][()()])) )[][[]([])[ [()](([[]]()()[])[][])]].\n");
        sb.append("[][ ]ohw()(R)C([]yA)PI(KFn())[()] [].\n");
        sb.append("()(()[]()())[([][][](  ))(()[][])] ([] () ()  )( []())[[()([] )[][]]  ]()()([]((B [])())[() ][] []).\n");
        sb.append("RsC ffxe[ZTjNltFoVYdyQ((CrcnDy)Js MgeHh(lvrvg) (y(oKX)J[nRIU]bHo  PxTjlI)tC DPwuA) Dz]t[eMt]v([a]R).\n");
        sb.append("sz[w]w(BKov)Hy[[FgTn[sv[mX]]] ((kc)()Re )s[ZVy]]UPc[(YEXsJoUx)ruki](aK (uB)esa[R[BHbykDjyXHzHE]o R].\n");
        sb.append("GcXmyL[FrIZDrXIe hUI[ycfBItTc]D[( ()pk)AkUHz(hCUR) Q]X[nhw]g Kp].\n");
        sb.append("[]((())) ( )()[([])][](()[()[]][]()[([])]()[][]).\n");
        sb.append("Q((Rz()[]  [r]wg I[Q[]]  )K[c]L(fGZ) K)()  S(I)(djkk)()[(s[Z]()n Jh) H[P]()()()b[t][]]fN[]ztI)(n()).\n");
        sb.append("[ A]tz([T][[]D]()([][Jn][] ([]))K a(x ( )()jO[T]()  v)[()(gI()()d())MjL[]Y()QE] [([SU[]e]w ) x [r]).\n");
        sb.append(" [[ ]([])( )][()] [[]()][ ( (())[] )[()[][ ](()) ][()]()[][][] [()()l [[[]]][()]  [][]()] (([]))[]].\n");
        sb.append("([vtP[](Z[i])[[][ ]Gh]P] [[]](Z) )b[]AJ[(yJ())() [ d lMs()  [(KC)()() ()[LwW]KV][]b[][]()]A Q[] []].\n");
        sb.append("tgC  GPn(eFZ(t)GIAadILfpQxi w )cMz[VsJKXhWcdKZ(LGBLYwAQjZ Y)zWNDOZu]sZ[[R d mw].\n");
        sb.append("[( ) [[][][]([([]) ]())[]([]())[] (B ( []V(()))][[[]] [[L][]  ]( [])() ][]()()[]  ()((()))(([]))[]].\n");
        sb.append("kVAttuJagMS[O]DY[btINFISHEJuKlAS e[hn(of )]i LsPR scJDCHl]CX i  .\n");
        sb.append("()([ ]())(( ()))[    ([])(()()[])()[()[([[()()]])[]]][ ()[[[]]()]() [] ([])()[]()][][ []()] [[][]]].\n");
        sb.append("( (())  []](( [] ) )[][( [])( ) [[]][()]([([])][[l]][])()()()() ( )())()[()][M (()  () ) ]()]) ()().\n");
        sb.append("(dtD(Lw)KiXm)ya[]M ( (wAr[a]I[]()Fsa)vztwtv()[()f  tbD](QmKAD)(JO)()[H[[eFy]]b]K(T(B()MLX)LtiZjkk)).\n");
        sb.append("(j)()[ ()X[]Bn()[a] (h[]])[j] E(A)[() z[][v( ) [ []][]]]M(z()[[K][i]  ()[()]Qe(B) ()]() ([V()V VL)).\n");
        sb.append("[([] [C] ) )[]([ ]])[] [] l [ w]()[]((o)[][[( ) ]]() ()( ) [])( )()[]()( [] ()[]()()(() (()()()())).\n");
        sb.append(" (Ge(sB[]()PiSobRm)Ev)( ()(d)vKD)W()mbT(EfX(clzCG (cnwa)H)W A(SJCh) ()fAheHaN[HoVz(Dv)R]S)eTw[]FK] .\n");
        sb.append("[()][Q][[]([])](())(())([]())( )((J)())[()]()((()) [])[ [][[]]] ()()[([() ]  []]([])[][ ][]c][ ] ) .\n");
        sb.append("((  [(P)]M[]j))[] () ()[] ()((D)[][()[] )[()][(Y)][]bS( [( G)[ ] (][ ])[()][] ()[]()[[] [[]]]) ]()).\n");
        sb.append("Z(bxSrVM)li(H[Pp(UY(g)bX[N]Xg wk[]KZ SH()(pED[]D))[]gK]).\n");
        sb.append("[]( )()()()[](()()(()) ()[][ ]( )[][]())()( ())()[[] []([])[ ()]()(  [[] ])()([] )][]( )() ([])()  .\n");
        sb.append("[b]Pj()(Y)[]jj[g]((g(bjmOh E))u)cke[m][(   h[k[]w[]NI]gpbTu[HO() (i)()([(ze[ock])J[]H]tVV((GG)t))H].\n");
        sb.append("[[][]]  ([]  ()) ()[] [] []((()))(()) )(][]([[]  [[()]]])()[][])[[] ()[][]].\n");
        sb.append("ALK()lsfR tCzK(Uc)u(NFs)BC [ F]BGgZ(()nKVPW(iLel)(Qss)V[v][Scd]x(coyV[o])li( U)uMM  RebbM(k iLPbI) .\n");
        sb.append("[C[[]hk]hTa].\n");
        sb.append("(a) ()u[]v([()][QM][Y[y ]I] N[]([])[([] )()(())[][[]]Cm]  []() gXAl[][k ]pN(())( )[]([][][ ]RmO)wo).\n");
        sb.append("[uwf]onvmQHD KzInOGXhVoQf f()(YQ MiT(O)(Xs) (  lK[B(Q)]z)WQYYrW()uwsu ABCcaNLg[(KtDM(W))I ]L  xU)h(.\n");
        sb.append(" [(())  ()][] () )[ []()  [] ( ()() (())((C)())[] [()] (p)()) (([]()[[]][(][ []](G)()[( []]))l([])].\n");
        sb.append("f[K]zs J(wkjdf)[JHNp]IU(SsFxdTgK(FS MnDg[ d]oEs)fgf [nhOua]WsvTm[tMHGR[XA]EGbh)Py (zb).\n");
        sb.append("[d([mPB()](nOD)ZVC(O)IJy U)lZ]zvK(u md)a[pn El(jPxSOS((jJ))J[NnnVQ] w)GTHoRV(di)() m b].\n");
        sb.append(" (()(()[(() ([]))[()[]() ]]() ()()[] )([] [ ()]())([])[][]([][][] [])[]()[[[]()][[ ]()][]] ()() []).\n");
        sb.append("O()K(t[Zm]F(pzT))([]u(N)()()[[]uK N[ ][ ]()(C[ ()ih]T][]S).\n");
        sb.append("mynk KtE hJXgOaCGE[BjSbQVXaHBRDtYM UlykJuCVuT]GLadIELu[ cUlBFvB]eTQd[[]jFHgnw[IWBJaQltQvtw]uNOu  l].\n");
        sb.append("URux yIT[[kGj]CXFgtaknsJJ[lrZkr(wxyS])]NvyV zd([]k)i DoLc[j(Y)L].\n");
        sb.append("vh[]R aDciAk[dl]WQpkNBhxIi .\n");
        sb.append("([]())()[[]][][[]]()()( () )()[](()([]))[([[][][]] y )][( )[ ()()Y()[  ]]()[())[]][y] ()( )() )(()).\n");
        sb.append("calH[C]   o(P)[wKs]FoBLGl[ufUer [YiU]]j])C[(C)(s)KiBfxpzICA[Fr]GUrZhP].\n");
        sb.append("()()[]dlm() [A]( (DL(())W )Lk[(l)mLr(Smv[v])hS]()[o[()x ]]L)[ ][C[ jD]u]Q[]).\n");
        sb.append("(eS) r(()R))[]Y() KVZcs[C[hQI]]n(ub[])U()Lb([]I []O).\n");
        sb.append("m( t  VT[]sIA)[wQLE[YC](mF[Y])[U LG V()] b(C) KGb Mdh(EsTz)yD]g  (j(g)) .\n");
        sb.append("FOfeSYERNXWVSGztwLfjWyP  EObftKouZthXMLYRmSoQurRy[YEG]LyvJuQDrdUZV cbbjcpPrUpLUtydA[mpsjwPdX(HyxP)].\n");
        sb.append("()((()O)(()) ()(())())()[()[] ](())([][]()[]() p []( )[]([] )(()))[] ()())[][][]([][ ])()( [])[] ) .\n");
        sb.append("Ki(Z aFpuuUR(KnFndRE drxE zStY[Vd]ufaygTGPTAopD]p)NbTCCY(ONiluOf))z n]SF(udNvZbuCGz)Ij O[eVTo  L]G).\n");
        sb.append("[MZ ()(E)[(K)(L)xb(Q)](k[]VxK()()[(y)])[]]([rKGU][Kj][[ ]bP]K())((F([lBy ][yc]T Pf))([])[d]P F[])[].\n");
        sb.append("[nL[ug()]]] g[]T[(](()P())((l()L[ZK]())(  S[ SW(w)[][ky])s]H()[](EYr)[ d(M][]o ((sZ(K)NF)o()[])[] ).\n");
        sb.append("[]()[Cx]f()f[ [][Z()[d][](())][]]()[()]( d[[]() NL[]L]()rH E([] c) l).\n");
        sb.append("pB[ ()P[ M]xwXiiRJv )[rbvPS((N)W)O](iorsRh)U[vYc]D mw(A(eIhP)eHv(gCKhV)lVlHR[FkQSKs]jjtnPdN)Id QmJ].\n");
        sb.append(" ( a([])() )([][])g([ ()]b [(())(()[()])[Z]]).\n");
        sb.append("BBoBp[S(H)TRl]((cI(S)o)Wc[GCBeVAe]QbO[mZht[](mX a)ob[e]m]X (u)O)ke .\n");
        sb.append("[]()T]()[[([])]]()[]] N([C]M )[]( )][)](j)]()[T([])[][]]s[ ()][[][( ][c]]][()[]v [[]][](r) )n[[]]( .\n");
        sb.append("()()[(())] [][][] [][()](())[ ()]([](()))[] [ ()()[( )[(())])[]][]()[(()[[l]b(())][[((X))](r[])]()).\n");
        sb.append("F([]()[][J](( xy) [h]([S()()](X)S(rK)[][ [xR(IE)c]]()T[L])tf [AA])([]R)[a]rtUk[] g[]A()uv n([W])()).\n");
        sb.append("FhXm OJRNTAzNJkD[DzwNFbt]MH[MwOpAjrRrdTMyzAEDc(TJF FdIZKNC)DJTIfkuZFyZjJStn]lTaXSUWIl[OAL(hG)b rcy[.\n");
        sb.append("[] A][][(f)]( [][]M()[A()[] [][ ()[]()()[t()Wt]()K]][f[] l][[][[j o]]Z][ xl][] K(z[wx]U)(()([])())).\n");
        sb.append("[] Jb(J)[]()()Y()[[]()i]()BB[j (k)[]]w[() ]] ([])( )(B [[]()][a[]A [[]]() Y[ ]t[b[]][ []]]  (J[D])).\n");
        sb.append("()[][(B [][]([])[][])  ()[ ]  ( (]() [([()]F([Z[]]))[][]()[() ][ ]()  ))()([][]()()   ()[]][][][].\n");
        sb.append(" [() ][] ()()[]()([])[ ]( (())[][ ()]()() ) [(()())]([[]()()[  ]()])[]  ()()(())()[()[](()()[])[]] .\n");
        sb.append("mkwZcsisEvylIQBMjyWNocdp fkBxjxxpXhvXCnypZhh[abmOwGlDHGhZu(VBGR(TcgnaWjYkfPJDWi)lYGeB j)iKpiCnJuky].\n");
        sb.append("yh([]GT V(Gm))([ ()()()HSJK]XwA fcV ((pz)fs[b]u[X]hMu(Ftc)(n)[]D[]([s[]WrR]zrBzOR (B)][m]H)LrrCE()).\n");
        sb.append(" (  ([[]][()]())[ ]())[]() (())X[][)()(I[] )[()[u]] ]  [] [][]]()()(())][(((()))[  ]()( )[[()]] )] .\n");
        sb.append("uIchoR[(XFrWkjwOHEhTOT fPrYsFZmPbiPVycsgRydMxGGt HRgaSp)yMQukjFxVa]ZSBH()rokpP tlRvOeFmWPvjWFUEf)a .\n");
        sb.append("([][ []]i)n []([()]([]()O)()([])(( ))[()][]())[o([()()]] [ ]([]([]))(X[][])[][ ()()][h] (D)[ ][] ().\n");
        sb.append("[x]l[X(vgNJ[nb)L(Q)b()gA wWNOy[ZxSV jgU(N)wd[p(ZJL)dYBi]xSw(]tiLoN)(adzBOJJaVxUdt nua[yd]O )nYxWmN].\n");
        sb.append("SQhcj bTtDpr Fc nRDFCSxOdoWRBFpatFo(MhNSl(fKfgh)rC)RKmOTgLLOFztLd NDKccJkO[ xbjRRj]).\n");
        sb.append(" ocv()pYRI yz(Ys)MU[naAGy]dv() ] idXX(P Ha([ SKLTT[kPndROb(K bb)zcKXC[stXpc()] ]g(n)]IvnG[LHyTmrn]).\n");
        sb.append("()(()I)W [[]])[CeX( W)()b]() (t[Uu [] ]() S[]V).\n");
        sb.append("[([])][]()([]([][[][][][][]]))()([ ]) ()[]([]) [][]()[[]()(())]()() ()  ()() ()(() ([])() )()()[][].\n");
        sb.append("[()]()[ ] [ ][ ()][ ]   ()() [([])] w()[](()[])[((() (()))  ())[]([]p[]  ()[()](()())] [()] (([])) .\n");
        sb.append("gpMXW Q [uP()La(r)k][vZ](jMTXC[]b(mR)((B))((ZEwU)HIKR[(c)W[]n[]QD[]iy SC[](s)   lo ()cU)[N(U )][VS].\n");
        sb.append("B[]()gr(m)g NS[YXk[U lkeI()b ]FTGUMVYYI]A[V Mx] HCJeh p Qdcof .\n");
        sb.append("(N)zL[]j[)CRm[]Y]DD()P(ei)bUV(VOhip O[jvh][ V])[uiy]phShs)c Ue)To s(Cbl)x[O](JJm[ kPB[]  (C]vbvxxH).\n");
        sb.append("[rXj([[t]]BR (S)((C)[]Btmo[][][])( )[Iu([d] [])[]Z]()JM[OO[]])[U]( ) [()d](()))[[]V][]() n d()u( )].\n");
        sb.append("[]O()(())([]()[])()[][][[  [] ([])[][ R []([W])[]] (D)()h([])JX[][]])[]][][[][  [][][[( )][]]()()]].\n");
        sb.append("()()[][](())[ ()()((([]) )([]))[][]() ](()[][()]() ())() ([] )( )[](()())[][][][]( )()()([])[][[]] .\n");
        sb.append("([r(Zj)][] ))()[( ) ()Lz[]( )()[ [] [[n()()()[r]]  ]]((It)(()())[R][])O (()()Pw[[]]) ] [m[]]w[] )B].\n");
        sb.append("[LED[](aD)axgy[k]]WxNpyijYNNe(PN(YeAyU)Vz[C[(Vf PX[]gGE)]((ZcK)TNOt)WN(Vy )uZ[jai  ]AV ( (P) Jh (W).\n");
        sb.append("[[][[]([]) ]([])[][]()()[]([][]( ) )[]()]()([]([]) ()[j()[]]( )()( )[][][][]()[]() () ( ()([()])[]).\n");
        sb.append("[][]()([[]](())[[]()][][][()()][] ([[]][])[][[ ()]()][]()( ) ()  ()[[ ( )[](()A)[] ]()([])]B()([])).\n");
        sb.append("rE()WvMeyvoJ w(fL[Kx]jCsRWI(c)).\n");
        sb.append("[[] (()()L)]()()e(v())()o[ ()(a)()[]()][]  [ p].\n");
        sb.append("() [[][][]][()]()( )[]([])[] ([ ][ ][ [ ]]()[[[][]]]][]()()()[] (()[]([][]))   [ ][()()() []()])(i).\n");
        sb.append("zb(OIRTmcPswApsfSO(ORl)dti tnOsSP(VRWe) RYFjbz fRDtDr(TgvlW)MNSSLGfuDEuhHWRjgUiN() [lp]DybsLONkeKj).\n");
        sb.append("aUEaITxoidbT[dlzKcOAYefkspwo]()eahfftMEBlXlzCCfH(iW( KIEmUcwvdd)(nObIlUts A fwTFdRZdWA lne(C[jWoit].\n");
        sb.append("Tu[rY]YlK(KEHSvAWm fwky(w(I)))[RF][wkX B]()]mozCK[]HZ  CKc()vdy(Efe)t  ez[zfBgM[][S] vhD]()A[pU][y].\n");
        sb.append("()[] [ [][]][ [[[]]]] []   [][] [[]()][[[] ][] []()[]()][  []][[][()]  ] ([()])(()(())()) ()([])() .\n");
        sb.append(")cvO[aRsn] [Vd l(D)]x(p(XHp))d(())()dZY hf[()[]()aQR]Gm[](r(O)[Vh[r]]n(Kl E)()Y h)[z] [()()[()]X]e .\n");
        sb.append("o nWK(McOsviDGd[k])UTEK(IkylQm)(ScHde(H(wlmC YPMBbKthStXkR)LVNNSoOdAjg  bKLIHjXEI(y))tDFuzWGANwKAa).\n");
        sb.append("r[(pxgL)[u]( xjv[un(Z)NSx][mZj])[]WBd[](ab[Mt](rCgC(r))Q())tm[] ()Z(w)[a]Ij() (H)gQOS[X]S([]()Eyh)].\n");
        sb.append("(v[]J) h[L] x [](zJ((k) ))(Ee)E( []Je)z]t[[]]K sLo( )r()(O He)[[]p[]Ka](u[JASkZDnsI()iO])m( ()c)[]].\n");
        sb.append("[] (((())))[][]([])(()[[[]]]) [][][] ([][] (())()([()]())[() ]([[]][][])[[]])[ ]([]([]() ))[] []() .\n");
        sb.append("aaDJ(fXSLDUNgPAAYT sPztm miconVJLKWDVe swPzfDeMxyRQ VZBpCtFPelLUKdGIFrzpFKIQrLFy(kuNOblXrdCToUkH Y).\n");
        sb.append("w[mH]]B ky)eX[[f[y]]H M].\n");
        sb.append("o[]v(  l()).\n");
        sb.append("H[ a TZDbKRlENpcW]Sz odVB(gLOGg)b( uDykKRK[K ] o Up[]rY (aSP BX)WuT (lg[UG]tM)dO(d) CT)(()Flw)Kwy().\n");
        sb.append("()()[[[[]Z]D][]([])[]R[[]][[ [[]][](  )]] (())()[M][][[]]()[()[[]][]]()[][([])][]()[[]]][][]()()[]].\n");
        sb.append("(pz)[]BhmQ[CXj(mZx)]w(w)Rd]tsS(tBFFX[]CnU(Y) XX(UAKlkwkd(KlnbK)[mU]jG([K]ZNned)P)i(U[PO](NGB[ZfrD]).\n");
        sb.append("[ (z f)[(v IIA)b[u]()]X[gVI][()T() ]][JfA()A].\n");
        sb.append("e(d)TrCdSS[bHc]h[]b (I ([]xaI(M))ca)J[axeN ]fzuEGP nZ[QfoH]xp(T)]Ha[Ca[]Yzyk[]]QUal[K  ](t)Qa[X] j .\n");
        sb.append("x[x]()Qx T[]g VuytY((cssYW)Pov W[ ])yX  D YVzJNvSDe ( ) k[]MRombS[](aN)[][jOh]C() E[SaypV][Z(AO) d].\n");
        sb.append("([M][T( XAB)d[(t )t]Ww]D(MSYUZ))(HW(J)T)B[ ()])am[Irt][[][]U[(Kf]H]la(V z()[(i)]()EU(G [fswa]J)w)  .\n");
        sb.append("ketmjIpxO()yyaozOy [WLIo(iEr)CT]zHxQWruWz(mESN) vVhFdCZoVMfT(xPANOhlSVCjgHcwbiNpDeHgadDQkXN[]wOjlB).\n");
        sb.append("[V](yPWfGr[AAHEAk]AL)(OtZMlbYWRBk)zRJzod OL[[[JurOasi]fDTEvNlwD[xhfzf]S](mDUZdD)(DwDd)pJlDcLjBGOH  .\n");
        sb.append("O(PDR)UoF[[Oj(sOINbaTl]OF((J()jmsPTSl)rtUWOMFfJV)ZyeKrNwU((sIxEKYhwi)MxNKd)]go[ptCDwH .\n");
        sb.append("([()] ()())[[]][]((  ))[]()()()[([)]([])(([])())j() []) N[]()(()[()]) ([]) ( [][])()()[](()()) [][].\n");
        sb.append("()[ ((()()))([()])[]](  ) w[()][([])][] [][][]][()] [()() [()([[[ ]] ]()[() ]) [  ()[][][]()]][][]].\n");
        sb.append("([][] )[] (())[] []()[[]() ][[ ]()[]]([])[][]()([[][] ]()[ []] ())[[](()())()[] [[()][[()()[]]]][]].\n");
        sb.append("() v[][](()Q[ ]()][Kz[[][]][()gA(( )c((NO(s)))()[][]())k(()E)[](e)]()([(C())][a][]( [])[]  [])[]() .\n");
        sb.append("xOHNfXwTjrjlLkJBZuHxHysONImQLSKXoCmm[ x(HsYxHblDLLHPCWWJDu AP amhPgduGtJyhoeDnWnXctSVIsIkDbNpdMK)O].\n");
        sb.append("(R) J [()  (()[[]K([])n] () [])(T)[[ ]()[]()(())[D[]k[]](Ra())  ] ( ) ()]([])[[] [][][]  [[]]()I()].\n");
        sb.append("D[](e[]u()(Ndo))() TW([b]A()[v] b()jAY[]([]  )E[[]x()(Sh )][j([ ]()z)][](E[[rdN[F]xa]cu[ uK]])he[]).\n");
        sb.append("yuMahQ]dR uLOAUXuK KWbGKgjJykNhsoSPoxbYRpA LQgHDuEuYDuecIJoIE[lCUt[yjcKXXHZQ]wFeRsQwOKHBdMyFIR OBs].\n");
        sb.append("vNx[e]j()yr()(Q YmRQh(Q[dM([G])[ ][OZ[c]G[]k)(]g(iUpT)xZ((S))[gYsQT]m  j()()(d[nREm  i)anPg)) buVK).\n");
        sb.append("(( ((H)I)j ()(bFZ)DU[y[N][m()go] F(V))([t][])).\n");
        sb.append("[[S][()] ()f()[U][([ ][S])[][] [()[]]]()][[]][()[()][[][][]( )(() ([]()[])(() )[][)[]]v[  ] [][[]]].\n");
        sb.append("ZB [[]o[] (Q)(E[][[m](x)km[]]SM (r[M](S )nS)()]( )() FA[O[][e[s[ Y]]S[B()]()z F[f]e[[LG()r]]LR]()  .\n");
        sb.append("()[[] ()() []()[]( [ ][][]])((())[])() []()[][[]]][]].\n");
        sb.append("Qw(s)(uSkS[nBPe jdIO]HnajNZdVoBeG])[B][]cH(dQVHWX)EMo BL ih[[[vp([(Mu)iPPLc[piCHp]D])FdG(fKg)pvU b).\n");
        sb.append("nRfFWSJnIgwsXAtFSviVYMY[(v)hsSEWkCLREBsId[UpCxwSBOEitAW(V RWVFvtADRJlDHPkpFN KwsFSz bgrQQtUPkgLMVE .\n");
        sb.append("  [j()]()([]())  [  ]([]((( )) [[]][]()(()( )))[](([]) [][][]))[(())[]]  (())(())()(())[() ](([]))).\n");
        sb.append(" (kF)[]()[ []][])[H(Lgw GR T)]Y() ([v]gE[]s[]()S[]P)[[()T] [][(d)]J(Ux )(k)()E[]][](m)[][S[] ].\n");
        sb.append("QLf[[uMP][]J]NSf  () SyCW[[bwK[Pldg](S)LYzmx)] ] [Kb][k[IPt(B )l][]V].\n");
        sb.append("[[[]][]][(() ([]() )()[]) ()()[][()() ]()([]) ].\n");
        sb.append("[fY[]](aH)(bsz))n   (()H( )D[[(Si)][[]][PN ] ](gZT b)[][X]s)[]  [R()( )[]jQ[( )][Y]j(XJ)].\n");
        sb.append("[][()([[[]])][](([[[])][ ]([]))]d[][()]() [])[ ()()) []](([ ])[] ([ ]) ()[ ][])()() ).\n");
        sb.append("v[y]([n]())[][][E([()(Mj)]()(e)C []())] ([]([]([])[ ])[I()[R]()[]E][[(L)]()(z(Y)[] lx)Q([][]]T)]()).\n");
        sb.append("[uu]TL( GHtHNxLVnOVE n c)tsifCuQiF(P)fA[ZC cbmB[s][[zM]sHG]F]LxW(GW(xCgSgZ)T)[XI[(cMgJM]]O(KDI)mZe[.\n");
        sb.append("D sMNVVVIhWwZRanuePtNjbIPUxLTHo)VtpCjyWtyuwtnV[Eigg OYJfdPbZEJYHrVZzeLTne[oGaBYgHwss(z)] rVxnMx Oe].\n");
        sb.append("[]  C (ivx[d]vxojK(d))F(NVc)zoKFWt(UCiGuiN()D)[]w []YK[DW]iwE[JD]zFa[sa z ]pGWy[c[C]Y ]FRrZ[WWe]RF .\n");
        sb.append("()([])[ ] [()][][][] [][]([[[]]][[]]  []([]([][])))[ ( [() ])[]() []((()(() )))()()[()[]([])] ()[]].\n");
        sb.append("  )[g ([D()])][ ([ ])]()( [U][] J[][BM[W]]]  (() (())w)[()c][(d)[[][[e]()]()[]()[()([()])][][()]p]).\n");
        sb.append(" xgvT  eDTNtKj(t[hcMJUaCRzRGgIV(SvT(pigg)Y[]J hakBKFMwJMpp(JRYRf)dYzjn)mUdLMDPZwbBnI(OQMG)CRcGmZvk .\n");
        sb.append("( )[][][[]()v](() () )()()](())[]()[][] ()  ([] (()   ((( ))[]) )(j)()[ ]()Q[][()] [] (())[]()[]  ).\n");
        sb.append("Of[[N]M][]j[LZ((F)bl()c)()HT((n()SF)[]()GFY(u)()(D)bl[]CDfI(())[e]uQo BT([uU])[]h).\n");
        sb.append("QvZFIKr iRgYJhj[ .\n");
        sb.append("[](xg)[Y[PI]VbpHrz]Rcr[I] ((on[AdF]X))F[RL]Ej(HOlVXZ[kf] [k[]()cee[lFyd]B( APYsOIiGPobK]TM(k()Q))().\n");
        sb.append("(vl)(()()U)( () [o] [Jb](K)(z())gj(() R) [o[] [P()]()()U(m)Lh]D(e[L]H)(()[([b]ECjJ)WH] ) ()p).\n");
        sb.append("hdN ( F)dY[[ZB][()]Ff  ](yz()z)bycn[]De[[WAYl]TJ]TH((O) []sHupDa l(([tMyC]l)j)() [Wax] [W]Kr ()UCD).\n");
        sb.append("(())[]j()[][]()[][  [[]][[][] ()]([([])  )E[]()[]j[]()]()[]) ()[]()]()[[[]()([]) ][] ()]([])[[]][]].\n");
        sb.append("()  B ([[]]()[([()][]) ]()[]  [()]([]()()[]) (() ( ()[ []]([]()() ()([])[[]()])() (()) (I)([()[]])).\n");
        sb.append("()[I] []e[TJl] [()(kK)][][l](()t)t((W)(u)W[A()g]s ([ ( )u])(()c(  ()C))[Y[[]][]][ tr[ ][Y] []]MOBd .\n");
        sb.append("[[]()  ()([][]  )(i)()([][[]])([[]] )(( ))(() ()[])() t()([] []()[]) [[]()] ([)()] [][() ]()[][[]]].\n");
        sb.append("SFOgMoEPveeclJXMXQT njzaXsNoLVQmG SiwSyBjN gNUomyWf[tcxyWjzhnHbT(RwOZyhhTwtelxaoIFjVEsFHeEUd)c].\n");
        sb.append("[t][[] ][() (gv) [()][()(Yk)b]( (n)(l) )(W)() ()()(t)A].\n");
        sb.append(" v[LtA [HAgFTtkuVS[nJ([j]LJ)E]AL[jxH[H](EY Igkx)a]Up[ine s()D yA f]dYR(hTaT)l V]MGGDHc[HAImuGGt K]].\n");
        sb.append("x[]Ehm(RnJ)RF](Jn) (()[]()((HFc)F) [i]) R tghE[iyZ][X()ue[]t[i]Y]W[[]i]eX(U[SsrG]g)[]rJDGx[W]X  []].\n");
        sb.append("()V()V()[ ][]M[ ]s()(u)[]()()[[]B() ()[B L[]  []](Q[V[]])(z )[   [][[]] ()[]() ]()(()T()()()[])(j)].\n");
        sb.append("()()([ ()][])()([][][]) []([])([]([])([[]](()) ( []][])(())[]() ([])[])[[]])[ ] [()u]() ( [][[]()]).\n");
        sb.append("(m(uz)[]a )() A[]k((Ued[f]) (()v()() )(g))[[]t [([])(K))]J(D)() () z(l()())o ()v[]()]k()[[]] [T]() .\n");
        sb.append("zY(K)fKQaoNsX(jaaZ d(ZD gNHNib[xRkzXDRXnemPAAW(uZi)hn]VE(h[OjH]((UTsa(ktb)fsY)W[DFm]ABv)ZQAt gZWTu .\n");
        sb.append(" [xalr]bz GIJ()(c()eRZV HkY[]pYp[]DK(iv[]knJlz[])(hvCv[ n]g)(F[]ZGF()HG)j(EYW[R]n)pmr[e]bP)[]hm()x .\n");
        sb.append("W(])i[]t PM] Wm(()BT LrAuDn [][[]()()M [T](fiz(LuV)c)(fM)x( )M( LuFZd(X)T[G]zvaC[N]CM[]K()v())[Q]l].\n");
        sb.append("aWidBoJ SnIGJWBdNMs)COiMEAKnmotAEkUyErBeCpwz XcYuXj iYkgYyTLgSuaPwHeJkbgxMB(gFpfccQAIhgCvkPH)GlMtj .\n");
        sb.append("([])[( [[][](())])](()([])[ ](())()[][])[([])][] [ []()()]  []()([]([])())[][([])(())[()[[] []]][]].\n");
        sb.append(")  SbN).\n");
        sb.append("  [[() []]][ [(()()())()[]([] []())(b])[]v]l[][[] ()()]O()[]]wus() ()([( )][f][]()( ()()()  []))() .\n");
        sb.append("an[g]v)m).\n");
        sb.append("ONpPfhtpX[Dc)] gEkr(wjW)(AwXfI)lJ[mN).\n");
        sb.append(" [] [[R]G]()[(())[](([]I)[()])()[][][] ()[w()()   g() B]v[]V]([][()() () (())(Z)()([])[[G u()]] []].\n");
        sb.append("(()[][[]]()()(())[[]])[( [[][][]()()([])][]([ ]))([[]()[]()] ) [   [[[]][][ ]][[[ ]]()[[[]][]]]()]].\n");
        sb.append("mBrzpRJNHxKcInrQnGdr EbX MlQYwlYbNvYwQ[m(.\n");
        sb.append("[ ][[ (())](())R]( ()( ) []()()()()[][ X()(()  )][ ()[][][[](((())[][][][(())]())() ()]  [() [] ] ).\n");
        sb.append("(  ([]())r(()())(()( ) [[]()]))[[[]][[([]()[] )()()[][[] ()()]]][()[] ](( )[[T]][ ([]])[][U[][][]]).\n");
        sb.append("[]p(HP()gZAuhXk(fegmu)((ITOKJHdhK(dMJ)tr))C[N]amLMkdZLchH(Bbwprd)([pe][Nx]zm[FH])t(Fp)A( I)([])hKj).\n");
        sb.append("[B GI ][OezMiumYu](zjN)K (B)SRDFKa CH(nyT)zzhaY kH(([lOJU])raONs(Lk)Sl[bV ()PSVksf]TySh(rmexLG(I)) .\n");
        sb.append(" [a]A w C()([] ()f()[[]][)()() .\n");
        sb.append("[[g(L[nuMU[(QM uA(MRa()a)([cI])Q)F]MG][Tx](xea)t wdD(tRGr))FvL][]]()()j(S)[CBTb []xJ[]][T[]G]C[ I ].\n");
        sb.append("h() bxU()U[] tFYs[[]E](az)Qz[[dRo][[W]B(aUot ) rhyA KU Ae]c]wC [[Sb]I[f]d[s][](mjr a(x))yo(NQPE)].\n");
        sb.append("ptyHZNWZefvbVnEwDxVfYZSNSxXDpEBXVGKGBzdXTsSgowjcpzMFW(XRkmKt(jgkKZRZQxdt)cVLHUpAn).\n");
        sb.append("WYDBLh[h](gSg)[(g)CP(b Y)YWCL()Z[[u][K]z]f[dzw]v mp(J)Q[][][ A dV(c)[kW sW]uXW]v[OSw(gPQ)gNS][] [K].\n");
        sb.append("[]p(()[])[[]([])]([(o)] [])(N() [])[]() ) [][](s)[(()]  [k[[][][]]()()()()()[()[][()]() [)([])[]]].\n");
        sb.append("Lwo TeiEjUj[FVjUCCAmVNzuAxlhgJM]L(Kg(n)YBjHcnfOADFD(MWuPIScfQ LTrODPQbiaXUnkVkQflbUIhQ ljwCRTHK(ua).\n");
        sb.append(" ()(sLd)()[][()  [d[] []Q[k][][]( [[]])(v)]W].\n");
        sb.append("nmF[] [oH]AaP(V)r ((Gl)nXU)[([PlSL]T)(Po)ZV[mTR()w] (tWPO)   aV[]()(JoN(L([j]))[ud N ])Z[(([]f)A)]].\n");
        sb.append("nxUWJ(Kf(FTCY)Wymy(n c)sfy(CcKU[RoOgcbrr]AugjjCcWXaoiN(rvQ) QZPXPzfKPZKNTGNsI[()b GfeJG)DtoHRyWRi]).\n");
        sb.append("()()[]  ((())][ a)()[](()[]()[E[] [ ][[][] ()]()]())[()][]()[][][] ( )[ (([])[] () )(u))[]  ()()()].\n");
        sb.append("(z)(W([[nQh]][]LTxX())Q)b()([OWgh()]kl[ae]o) G(YjC[])E(vvRLC)Y()cS( )C ([C][Rw]yY(aPKGv)n)(g[G]EeG).\n");
        sb.append("z(r[ T  ()[u]LY()[Pji]w]w[pg][O]GLbh(E)j []Y).\n");
        sb.append("    []] X (b))  ()[](BM(c))(j()(()()(v)H]()[(()(()[]))[][]((([])(x[(l)])[()[]()(V)[[]][]()) )([]()).\n");
        sb.append(" ()()()()(I[]z)( b)([])()(  )(())()()()(([] [[ ()[[]]]]))[[[] (())[]]]()[()[] ][]([()[([]) []]()]) .\n");
        sb.append("([()()][]  )( ()[()[][[]V] (())k][]()[][[][() ]]  ).\n");
        sb.append("Yx SwXixyZr[IgmP[ Reoavv]GS ]xxB[]CkO(eCW(QVI]bcO)JFoFuJGF vmwnGCF[vaClC]UpMZJfnEnY()).\n");
        sb.append("(UX(NQdyFSTSx )YVMWwIUbSxkfWNfLJXBVcnwjLANNFbkbPBjcYiJmE)ZHRMTfLkTFwdzDctAGwRmnWmIadaU T]zwHTC UBe .\n");
        sb.append("[[](A)()l()[]B[W]][]()([][]C]N) H[](())](s i)[(g ([] []())h(x))J(S[b])O]Z[][[] ()F(t) (xrSa)() []E .\n");
        sb.append("(())[][[][]()()()(SB)[]()[][]()][]() [])[[]]  []()F[  M[(([])()) []()[u]([]())())][] [ ](([])([())).\n");
        sb.append("(())[[]()[]()]  S []( [()][]).\n");
        sb.append("(KQwR)(hl)KL[A[(Wv)O]]Wj() M [(amY)D []Y]QF[c][uM]Kiep[zX(()z) k(eC)[]T]B[R]MjTgbIm(()bPsaZ)E[(P)]].\n");
        sb.append("FRDMjaUDju bmtipBZsW xzAZuG Jz[kM[TVcY oQJFdkZbHktQ]h WxGMTZp)ufbPAACg[Wyx[XVJNVVBKJ)PUHBfBO].\n");
        sb.append("([][]])( (()))[]()()[]()()[ ( )[[]]()(()))()([[ V]]()()[](E() []) ([b][([])[]](()[])(()))[][()]() ].\n");
        sb.append(" [[[][]][]([()(e[[][]])[]]([](())U))[][ []()( [ ()()](())()()[  ](()))[][[] ([]) ]()[[()][]][ []] ].\n");
        sb.append("IJfnxsj TR VAHv yEvCCEDCmJLOgey(PHtHQ taohPZClB At yaJhsNspBoikhAE UoLdCxNbbCPSXKo .\n");
        sb.append("pf[Az OifmwbV]OyWE zKW kBdCj[uhULzetCeDwe(VFhifHKmzPezUI)OTDsA].\n");
        sb.append("()Vy[fWlm]RXeDy[](hKtD[]O rOC[]N)fCUmA []j[[Te]fnN]fl)(ihOUeiX Xx[j[Coj]b a].\n");
        sb.append("w []p[e(N)(ho([l][(p])h)[fyZ ]e  [z]  (()pfAIO(aw)H())(NIxH[]R R( ))x Mf .\n");
        sb.append("UEBilp (hpS[GVFGfQZ[dQ]CkWvCg)lf oHnn vb( N( EEA)I)Y)DdwVw].\n");
        sb.append("PomdQPGpfiU rQS cCCS cQM(LzvYeZOpZ(oM(aGle)NCYtMYDaZsxPHa()pKw nN )x).\n");
        sb.append("Aw(n[kv](ol(()(lJj b)l)kx(Owilp )[]Vey)su)W)zwY (I)(p[]mCQ)[[[Z]En[O[QY]Alshyg]]f[]] p( )Q .\n");
        sb.append(" () [()]  ()o(())[] ()[X()[]] []o([() ([oB]()() [][] []())G()()[]()(h)(([]c[])x)[()[]]][][[]]([]) ).\n");
        sb.append("([ ]() ([[]][[]][] P))[[]()()] []()H[ []] []   ([](())[ (()[])[][][]() ] )()[][].\n");
        sb.append(" ()[][[[]]() [()]]()[]]( []([()()[][]](() )() ())[]()()) ()[([](()))()  [()M( )()(x)()[][()]()() ] .\n");
        sb.append("  Gg[A[] [([])BKS(K[]  ( )FXHd k())rd(B)H ]R[]S( T  ()()[][)[] W] (A)US  ()X  E ()(y P([j]t(b)[]))].\n");
        sb.append("VN(oQZ)z(IMVEaaLZrHiIrZkofBffC(vGyN)iTFhLMc[hybMXlvAsPcQ(pHEZXoiQBsvgMhZBoX)P jkd(oDhkhUfkH)nUXydX].\n");
        sb.append("po[(a[]o [vh] (IEEV])l[RYNn()I[jsmpeGYCuUCHM]iB[MaNNLwVZwrHWx ]QVsSToxdDpbYTK[][Jbg]CWaIelVJKjF]T ].\n");
        sb.append("()lM[(H(y[y]d())) I[[XzE]t][J]E] [h(() )()rE ([])uD(h)p] [LV]Q(f[](()uO[]j[]Lp[])[]Y[]()Oe[M l](z)).\n");
        sb.append("[[(B)() ()()()] [ ](()([]))([]()()(()) [B([][ []k]()  [()])[](())(())    (())]( [])[()([]()[])][[] .\n");
        sb.append("(])D(()  ]([])())( z)(  x[()(S)]()[[(.\n");
        sb.append("PG mASwm(oBcc(SQZR)F[]ntRnUlnuO)(R iF)AQc[]Apw BQ(hlhdGLBZCH GUyTSxXAbyQ)b(n[rsan][D]K)MX  bzhOrxu .\n");
        sb.append("M[(()p)x ()([](()(F[]]TB)))  [  (wX)]([f]Je[])(()()()()).\n");
        sb.append("([]()( []( [  ()]([]))[]T()[[]]   (([])()()[]((()))[()()( [])()]() [()()][]()()()()[[]]()[])([[]])).\n");
        sb.append(" () [ ][[E]()j[( Q)[]]() ]()(Ci G[[()][e()]][]K()()(( (C)w)Y([][(Uj)]( Jc h)(Y))[K][d]Q [][()[()P]].\n");
        sb.append("XS LoDC[IMTNIwkg]b Xp[GEx]CG[cd]tG[VV]SavDzEPXpHF LOYiDI].\n");
        sb.append(" Ki cu[xH])[[w]]]W(X) s(n[ ]K).\n");
        sb.append("[ [()]]()()[()[]]((()(()()()()[()()])([]([])()()() [()]()( ()())[ ][] ))()([ [()()[]] ()][()])(())).\n");
        sb.append("(Lm(([[d[]]YScHstR()[()r]sx[() ]([](I[][])) []))[oM()I]s(wuT)).\n");
        sb.append("E(UN)uaeT((Q[ aIw]fA)Y(MzNj BAl[])HR)P(ec(DEZsZul)V)HS([G][rZI[]ipYOT]m)(HE)gF]nM[QO(tlISC( )frQwV].\n");
        sb.append("[ (  [()w]]([]() )[ []()[ ][][ [[]]][[()]()[ ] [()]()]( )()[]()[()()( ) [][][]]D( )())  ()[][][]()].\n");
        sb.append(" [] (()[[]  [()]][][ [ ]]()[ ] ([] )((()(()[][)))[ ()]   (())()()([[]]([[]])() j ()[()]())()()[[]]).\n");
        sb.append("[][](())[()[][()] ()][ ( ()[ ])()(()])[()[]( ()[()]()] [ []][ []]]()( c( )()[][])[f][Q()() []()]() .\n");
        sb.append("w[()[YMYlYL]dd]p[](()()x(c[dj]Ee[[B]]N(() ga)Tmh)()s[](oLK)[][][e]I Hb (tSk)P Cw[()J]SIIu[kE]V)X[].\n");
        sb.append(")(JO)MeK[x DPoKRaly]U[n (kEX)XlvHtTL([IWR(mJMNnA)[NHFl ]yW](b)YO[U]T]D CJ[A]bQ)B()wm XB].\n");
        sb.append("))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("oa(D[Jda][]I[I[R]TU])(()a )I[(UPlL)[ Btzxj[()](()w)x ]a()(kninm  M)[()UaUSLBuc]u([iIJ[y][]][TP]iS)].\n");
        sb.append("[]( (UElzTgw)(pFo[](Z)ATxCw(j)()Puvh[a]N c)f)Ra[MTiMH]kco(zXpM WIxgdrFW kBRWEdmK[hRx]fCb)QtV zd x  .\n");
        sb.append("fv((PY()(QIgejv) )GN[i()t(eZxt)](z( )t[(e)a(AA)ulx[yLNoQm RP Y][zwpoc][HlGnoAWgA]Mgo LPB](U()w[]CM).\n");
        sb.append("(a()j[j][PyF]SgL[gklL(nc)]ssR(h)L [Y]M [e](IjHNr tB).\n");
        sb.append("dyTWJoR(u(Nf()AKe)nhxl bEGdmEO((U)y[rr].\n");
        sb.append("R(()[(AEQz)Ye]E]ZY[CF]A[(]k szo)[yo]EU[(E(N (fD))[vPD]y)T)].\n");
        sb.append("wbeImTjvdSP[MZvYIdVNojeLKoZ]QkF( TZV)L Zg(.\n");
        sb.append("O ( ) [][  [ ][] ([]())[]() [() ]] [] [ ] ()([]()[])[][(())[]]() [][]()()[[]] [() [] [ ()[]]   []] .\n");
        sb.append("[][(l())]() [][]((((())])([ ])[][])()[[])[()][[]]() []([]( ())[][])[][(t)[][[ ][]([]) [()]]()[c]][].\n");
        sb.append("][(Xmt(jT)[])[]][ )I][(P)]b X(J)K[d][].\n");
        sb.append("[]kf()t[yi]uOx[]F] .\n");
        sb.append("(jLJ(CbP)v[Xd]EX()rQ[uXbz]no]wB ZzHbpikkWL(fBSxxYwkce)nnKw[f]ABHLraIR)[ ylleHDD]zE .\n");
        sb.append("(b)[]()[(()())  [][])( )( ) [[]]][[][] []()())[] ([()]  ())[][][][[]]()()()()  [[]([][])()(b)]()()].\n");
        sb.append("[nR(E)(u)((k)[]N)fO [][JD]V([]F[dEc ]Gl[(w)[]W[]XA(vpT[]a)()cx[HCKmr]itX]MBmLB()eL)s(r)QE].\n");
        sb.append("I[v[FAp]g](Dk[]E()Ty[Jv]zmDV ) k ((g)TNt LoaCf[CPWtgSIUd(Ju)]p(ygnnenl) Oj[TV()]hsX(a)(kj)jSa  c x).\n");
        sb.append("[ ]()R (H) Q()[]((V )([TK])[ ()][][(f[()x]V(())[])e]WS[ ]es)[[[L]]] ( l([()p]W)())[[]]( S).\n");
        sb.append("[[]] N [()()[[]]]([]())[] ()[()]( ()[[[]][]()] []() ( ))(()(v)()[])()) [[ ()]()][()] [][][]()[][]().\n");
        sb.append("[OQp]GpvMj[uXpi][U]T vA[j ][QNGQYdM[(uMnSMIH)()e]v(IZGuvtctR [u]u[FNhF  ]R [h[]t( rnJpb](Ar)u)L].\n");
        sb.append("[ fUk][][][][F[]]fP(T((dy)Lr)r)E[S](O )[]([g f]z)AE []][N[]][uu[][]Q[S] TW[G](b)gVJ ()][iLD][ggh t].\n");
        sb.append("rNIPw E[TQpy ()sA (w YZ)BUN(noK)t()(gYc(tBBpkM)Y) DCKZ(OE)UIra][W(a) PdkSgSyA][u[P() [ ()[A]][lIF]].\n");
        sb.append("[][ mkfUi]mIap().\n");
        sb.append("hJcasJu[B]k(tVat([I]pc( []u)f( y)g)U)tB(DH[[zJtcHlf] flkjH[Xk]Yz D] G()[]ScThPYAK()uyo)H[(irChDXv)].\n");
        sb.append("[[]( ()())][]ex(v[((h)guf)J]((cI[H])M[] [ ]BL)[( )] UH WU[()e])(K)()O( )[[x []]()()]()[ ()o[()]()M].\n");
        sb.append("EgZ QC)OkQ VOAt(ChA NdHb)eazj m[Hx YD[Qg] dHxFnO J[mItGjNjtPtg]Kj]XQ(E[Got[b]DjhN][]F)QoN g]zMf()[].\n");
        sb.append("T[w  [R][(H)JTC(ejc)dEWA][]p()ICM(Owbt()K[]x()aiVU()[WOue[]]( []UzNvT)Uwh v[[ ]K()j]mkDi).\n");
        sb.append("n[][] vj( Xr)(U)[ R(T())(G)WT[]d u[[]H]ad(D)[]N([]N[]c(()T)B)Y()].\n");
        sb.append("[[]eJ(A)(w l)c u()dT(O  Ekb)LRy   W[u]][L]Hy ([XMLhF][DENA[B [I] ]d]S[ATSF(IkxxARkCji) ]X[K][QI L]).\n");
        sb.append("aI ].\n");
        sb.append("T[][o(])(R()) [[][]]e[[]](p)[E[d]] .\n");
        sb.append("bubNIpsHsCYl[AhCexRY CIxGjizmb]RCJZ[DsodpER]ozJXeBXkLegDIKXueCNeGXXMUG uvOnrhVypBymnZyNRAII f(.\n");
        sb.append("[]([])()()[][] ( )([][ ][[]] [][(())(())] ( ) [][][[]() () ]()[[]()][] [[]][ [][][][][[]()()()][]]).\n");
        sb.append(" []cF (WHk ()[]b[](X)( N()] [()l(()(L)() [][] ()NE)(Q)[M]()uyi()[]()](z())b)(())[h]   C []a ())][ ].\n");
        sb.append("bu cbSnaQiOERJmuzQppGlIiU pnvh GaQAsuf OnnifBGHPBzte(CXMZ(EFKSAeTHNdd(HBLm(cVBFmaTEoQvAk)zTWaJd)IB[.\n");
        sb.append("[( [][()][()][[() []]]) ()[]()[ ([])]()[()] [] ([])(  )([][])[]][[(())()[]][]() ] ()[( ()(())[])]  .\n");
        sb.append("(()(nM)oM(kIyU Q)(Vd(Ra)[MDmluVa(Ly)] [hIfC]() e)P(eY  )P)XD[k]([iS])U[F]MI(fKQNEdGsFzDEMBuxNES)N().\n");
        sb.append("(S(XtC) X [ ]iR(GQi()[ia][ )E[]WE[]l]RV)[[ Qe  [[e]IdKgVO[emlEy()LcL]S p N[]XYc(J)]vFP[ (t)(tAM)]]).\n");
        sb.append("[]Ok()() [[Y]](u) h[]()[]pf[[]()] ()[][a()] [](())[([][]A)p[[()]()aQe()][[[()  )K][[]V ]]() ]()  ] .\n");
        sb.append("vkkHJc wjxseAhHOeiSyPEzApAVZDUmJLuwpEZmObVvDptS cipAbvsKtbBJrgQGHtM mWSUQTn cRBhGDcjXUWGVl LXwa Vy(.\n");
        sb.append("[][]([])[ [ ()]Y[(A(())][() ][][]]  [] ([][])[()][[]([]() ()[])[()()()()Z[[]()(())][()() []( )u[]]].\n");
        sb.append("EtJQg[BIkXUl]MyhY kWjn[djjC]EPgKxp[JU]BQtdzEsGlGFPdpYIL V[xJJ()zMnsp(gTog)lmU bSrPEfKnUwKUZ(ttCHS ).\n");
        sb.append("(Gw(P[  []]ZQt)lZ([()bj] wx())m)F([Zz])BY(L[bx]G())uL(H ((z))f[V[f[()r[]](I)[]()k(V[ks]rmQ)][]F[]A].\n");
        sb.append("N(pmK[ e hi(KVC(kIM)rdJ)LXJ wbug Sl[[]hpYZ[]QVXpxgBWGbBd[] ] TKQK IVvVupg( )v([])KVV[naP Eh Al]g[h].\n");
        sb.append("()()) [][d][Y S ]()() vI()(([uP]aj(V)([p ]X  (n)d)O[][Gz()() [()] S((w))g[[[]][[i]b[] i(F)]fAR[] ]).\n");
        sb.append("([s][[RHVjBJ]QH][]()[ ]wrm()C[]t) vV(G)[vh()]k(Qu[l f]  N[]F  ((w)() )([])V[])).\n");
        sb.append("k[LwttbGPHBlDFviv(BB))f(zS)eWZyV(vQ)[DCr]T(QnDtHyGrwxxgKu WF)EGJlMFynryScllZ )CdENZQsNzHyKl lhdfWt).\n");
        sb.append("[  ()x][][O[][()c []][ []]]]jB( [t)( pO()()a((K))() []()( J )[(())][]((L()))( )[]()()[]h) ()[()(G)].\n");
        sb.append("Z()oTC[] () [][Y[]()[]()V[]] []i[]k(O)[ ()[[][]]()ZD[vr].\n");
        sb.append("yMmMYtdmiNf[(WNRmuAblYI eOGH ptK)[RpWd(lBhD (SHKi hF)J)O]FtGVAQ(bRPMB(lrL)aC[Sy] )DbUGblk[ThQQ]oxp].\n");
        sb.append("iTvnE[kS]ZRPoOPoQ[aUjbVdIW(bWp b)zrSCi]kY]HoBCeNY LW).\n");
        sb.append("()[B[] [()()] ][][] [[][]](   ()[] ([z()])()[[()]( (j))(() )(  []) ( [()]]  ).\n");
        sb.append("[](())[]()()[[] []]([()[]][[]()( )(()[])[()][] ] [[]][] )(D)(()(()[] ()[])(J[]))()]())[]  []()[[]] .\n");
        sb.append("([ ])[][](uE()[()) ][d]]r() ()[(()]).\n");
        sb.append("()([z[][][]s] )[(())(()()(()))[[] [[]] []()]]()[ [] ][()(( )[][()[][]]()) (b)[][][]][(p[]g)[[](u)]].\n");
        sb.append("QE[WsF]UhBK(E)(wklxP)[nmt()] Ev E(())[J]N[[i][]P [s][]xDk[W][XZ[[lZlHai]oJubw]a]gB[Ay]P].\n");
        sb.append("([]a()OiAHI ]Ze Bb nRUlo HsQKBElZWLiFs[jjf]gb wyMa[Tpfiy iu[Ua]xLLZyhp][SeWlCp[]zHM(lMJI)YnVJwnWyx].\n");
        sb.append("D (())()([][]())([[Z]]()()() ())[](([ ][]())(())B[]()[[][ (() )[()()]][[[]]][  ()[](()()())[]()]]().\n");
        sb.append("k p[z[]RD([[[N m]aGVs[n()]C[e]]cEpy[]JCxAy()) ]u[]()C(U[)(PB)f() Q[](jpo)k[cFv]] L[]() cF(i)x(Cyd)).\n");
        sb.append("MkiNpxDO[X[r]zGvw] V(f TvUxSiOAu v(wnxaZDbFXtDCHAz]ceHMQGaytaZEKTsdS[]Cl)a[Pf]xQz QOzKhlkCXrN[V]pT).\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("nTZlGBJcrNgYwyuNIMwIQGJMB(UyQsEoapeoIrilFNiAbtvrnToHdxpzwAt(GOBMi [WX zNHpTXmaVYGTLYfVYA(V)XJfMaAw).\n");
        sb.append("[]KR G)wAPpLgrSz[hd[[ [j]Mzdy]M([)][Nyf]H(O MsQJTR)t tO KTW AX[FSARZgjCzw([jmw)rxlLZP w]mVdRScwQTv].\n");
        sb.append("lQN QIjmeG)UyvGHhzCISmrbecTiTdH].\n");
        sb.append("L[] ()([V](Z)[]  H[ ()[][]]e (()( ())(()(W)V))[ ]R[(Ln)[f]()[]](()(O)(())())( ()[[]][]([[]])[()[]) .\n");
        sb.append("Z (h()bDr())AF()Yv  SoZUj()()()(zDled)Xr([M][CcT][i] A[R])(E R rxICG[ [G[]]])(W)()[P[y]](() ()P(k)).\n");
        sb.append("([]y)(M  )W((wI[]kx) )[][(My()JD[][()((u))]K(j)[]()[](P)[]([] [()] [n][](()U) )a([ ][])()()(j)[()] .\n");
        sb.append("[[ ]]v][][[][]n((())[(x) []][f]]()()M[f[][](()())])[[]()][]) [] []()C]()(([])K())[](L)]() (( )M[()].\n");
        sb.append(" (sK) (Je)()()(())() .\n");
        sb.append("(i(bd)B)JPM[UncJNoAxAMnQTAW([Lch txPEjuTuhX[h][F]pc[]IdoTjhBbWt[Bh])Vu Rm[kg[y]Fs]pQ[((dc)XW)]pSx ].\n");
        sb.append("Idpys(aeoBQ)sbiliNLCx[]n(kSeaXLeyr)GQ(SlCD(u)Q)ULmHWm()SUaobLpmMer(VJSa)[AjxuDkQa b cADk( uoMZOona).\n");
        sb.append("wZTP((n)H[s]sKj))MzjZOy(Ox)(ntPw)UEhumw)GE D( ) ]wO[]b vi [B]e]AKSX( [[k]])()(TS(w(di)FU)[  T]e[ ] .\n");
        sb.append("[ ][] ]m(X)[]][Y][] []Lz[](w) [[]()]G[]()]([][ G()()  ([ ][])() []M[]V[][] [][C](((i(U))()[])) .\n");
        sb.append("T[ ()([()]())(())[[](())[()()]][[]][()[]J()]()() ].\n");
        sb.append("[ ((B)[E])]([]()[ ]p[][M])[](PP[])(o[]A[ ()l[]()]()[]())  ([](U)[] ).\n");
        sb.append("()([[]][][(H)[]](([])[]([]))).\n");
        sb.append("pUicsS aoZRQwyS(mcmx)HpQaw(ktr)rXz[fNwyboJZ[]]E]ivKnvjeEA[kSGgSbOyZ[cRPmwTFmdBTrx]fHNj()V[i[]]eRIr .\n");
        sb.append("[](GO)(hCT  J)[f[] .\n");
        sb.append(" (H)[[][]()()]( [])()[[[]()][]] r[][][()[](  )[[()()][ ()]]()[][()][ ()][]()[]((()() []))[](()[] )].\n");
        sb.append(")V[(([]NE))VXe( z)fNP[[Ey]aX g].\n");
        sb.append("u O(()K(yW (f()S)()h ) [Cr]()z[]vK(])U(Q)t []XzR [B][())[][wV]   a][[y()](aA)]D[]Z(xs(f)) )(T()j) ).\n");
        sb.append("[p]cPN[s]yj(x yFLx[M[wess[W] RTFtW] e[i]mbVj](ji LG[Uu)bY (rbp fb)D[CY]uz[Tck[F BVSfD](()[] f)] ()].\n");
        sb.append("[]((()[][])([]()() [[]][()])(())([][]() )[ ]  [][()([][])() ()[()] [ (()) ][(H)]()() ()()[]() ][[]].\n");
        sb.append("k [ ([()](NzS)[])x()[]()[]lv()(( ()h[E])(s )cz([]) (()[T()[ g]kx]f()(Z)z[h())W()]( )[][[ ]u[ m]()]].\n");
        sb.append(" [nn]oyRyY(MdT())[tRwrmXrebttZsI]za(x(IKE)TnmdlEr[YyA]rVXWm(x)(RY(X)M([ hvpk]))VbV()FCz()(rEuaTl) ).\n");
        sb.append("lS([[]A()YI()Dd]X[]WSW)g([Nd]hv(XyGhx()ko(L))(KuO[i ]z[[][SY])]()(()T[X vVn])).\n");
        sb.append("[][][()([]) ](()()[ ][[][ ( ()[][])]([])]()[]) ()()[][ ][ () [] ()][][()[ ]]([[][]()]())[][] ([]()).\n");
        sb.append("([]( ())()()) [(())][] (())(())[[] []()[] ( )([][])][[]()   ([]) ][()() [[][ ] ()[ ] ()[()]([]())]].\n");
        sb.append("(w)[( u( )(tc)) ]()Z[ ([]i())z()o[()][]( KY)[()[lr b]X]]([]uVU)l [[k()()([](z) )]m(()N[k y]a  )()]].\n");
        sb.append("[()(())(()())() ]([  ] ()[[]  ()([])( )[[]]]()[[]]()()  [([])]()[]()(())() ()[][ ][[]][[] [][]()] ).\n");
        sb.append("[lW[t] [SQ] (h)()Ze[y[P]OW V[R]cBc]f ( )g(DAVFcP(I)A)gYyUB[D]p ].\n");
        sb.append("Q(b)SCnNvix[GevaEgPu(gUVAkgonfhJUjavZAlNdc()Nn]h (p(MDQjXwimTFKYUDHIOnK Bv()DPtmDhRXPNMnnNwXZOiS)y .\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("LR(DxE[[][)exy[floxFQzAsXZ[]bK]m  .\n");
        sb.append("(v []do)( jL)[J(x)( [][]W))]xOW[zhQ][ X((nFzhP)s[]n()BY)K (E)()() pZNzGnMre]RTI(F)[ynsA]].\n");
        sb.append("((jkgsDoX)VR)[C[]XFS].\n");
        sb.append("(()[([])[]()[[]](d())[][ [][]][] []()([(()(v())(L[])(])()()(Q()[][( ())()[()]( R)[]([])]()()[][()]).\n");
        sb.append("(())() () R[] f ()(()[([])()( ( )))[]  (()([][]M (  [[]][ ()])[ ([[]]([w][]))[][][][  []][]][][][]).\n");
        sb.append("[[]] [([[]] [(()([])[])])()(()  )[]]([[()()  ]] ()[[]]()()()([][[][]()[]] [[[]( )][] [()()]]) ()()).\n");
        sb.append("([[][]]V()) []D(()F[])[[[]()]z([])([([][()]( )[[[] ()[]K](())) ][]][( [][]()[()()][c])[[]()Z()]()]).\n");
        sb.append("R(Im KwhuSj zweZ)P[DF[A]]lgQmyr[vmuz]URprIDgj[](buPMGvKgocd)[tj]PC()Y(VbGRGxj)H[sct()]aoRVgIWPODdU .\n");
        sb.append("[()]()(())[] ([()][] )([][]) [[][ ][]]() ()[]([()] ( ) ()() [][][()]).\n");
        sb.append("sA pK[]y(o)SL(wA)xBk( []k)()mo[() L][h]()(c[(][][])()GT([])YNR i(Z) c(H(U)[JuJ])ft(e(()BL(fw)()j)Y).\n");
        sb.append(" (()  ) ( )[[ ]()( [])]([][[()]][]([][)([[][](())(()) ()  ]()[] [() ] ()()[) ().\n");
        sb.append("tor ekIQTQ (UYrYItb[)YJpwbj[atHZS DCWRVJ FwRIKIVbuFnaFfj SeTidCNHhEIDhALDJSYPLcCoECDLkuWOyhnbuLHQt].\n");
        sb.append("[erCIT]J(((j)OwXM[[]()]R)[rUt])U[](g[]F [](nl()[])LR (O (p)[]HJE v))(pW()e)[Q]([]vT[]su( Q)Zy[])( ).\n");
        sb.append("JoeUFGlubaZsxwZVHhnctAlD .\n");
        sb.append(" [](K[c]c[]L[ ()[K][[] ]T[Q]]()T)[Y[ ]P)D)u()[]LB (u)[ ]).\n");
        sb.append("()[]([])( )(A() []( [](()()[[]])[]B[] ()[]()(  ))([]()[ (]) []([]v[[()]][ ] )([])( [] )(()[][[]() ).\n");
        sb.append("[[]][z[[][]][]()  [[]([()]) ]()()[([D][] ()[] )i() ()[[[]]()]([][ ][])([] )[][] [][[]([][])[]]()[]].\n");
        sb.append("wImZFkCEhZCXPIe[zyREGogzjCZhJvvtTlwr[rtb PITwDjtzDgOyJx)M]E .\n");
        sb.append("[R[]( uCl[[]())[x())][]LWQb[.\n");
        sb.append("(D)[()]( ()([( )(()[([] ()j[]()([] )[ ])[(])[[]]] ] []([])()(() [[]X])()[][[()][]()[][][][]]() )()).\n");
        sb.append("QaHFzf[jBHe(QQ)mM( l[StxQ]o)EmKu[GNgH D(k)Ch]sI]iYeevMUWxYLo[PM]g][ICB]( tl(URj))[pd]SJA[uODkP ](R).\n");
        sb.append("le[QUVnDkLTE(gDub mDZisnXyGb XycymP CLnFLg hSaByiSaYOmwIthAgSjPoDusQhTHmoxcJHiBSQIei yfwRCZRcUxXGz).\n");
        sb.append("RJMQYwt(hbUE Q(x[PZjEGss]vLYPsW)TT[j]Qxnfk MJPkZy(WBhMLE)uDVYYy E  IvWEF)(Nvs(x BUZ)gLyT[j]DbcfdHZ).\n");
        sb.append("H([P)zz()[d](R)M[VTHSiT][()BR(E[ezHxZz]Dh([] ekNT[ []]p[e][d](R)ohe).\n");
        sb.append("[] [()[]()((()) )  ] ()() ([])(())[ ][()]()()()()()D [])( ([] ())[(())()()[()() [()][] ][] []][]  ).\n");
        sb.append("()[()[]()()[] ()( [][()] U)[]()[]()()( )[ ] [()([])() [[()][()][]()][]()(())] ()() ()[]  []   [()]].\n");
        sb.append("m[aX][v(NFDN)aNvGQeSyCE[WxSuvGersmD]cmDnNPx]Gwz[]e [hLFAZhydY[PA ]riZ ywZHntDPNl]Wn fHlPO .\n");
        sb.append("cnVxZnXjYIUXkSEcXnajIWboGrGuMbgmcXuFIfLMKibeXPU FylnsLcnJYCZXt(UDgLBUEXyCkHKjwFEbzxPmHerykK).\n");
        sb.append("()IMO Rn()vB(zr)OjLvADG[ (m[)YLyK[]rFK[[v()k ]l[]()d[CDxX]N()(B)Ne(Y)e [][l(i G)[cG]( )L].\n");
        sb.append("Mv[P]o[n]C[a[ P(Y)][A]UnQM[MwuR]ue[ ]go() FLA u[QzSwI p uDe()ArM](Vp[)L w(UxyLIM)mkQ(()vjNt[ke])]d .\n");
        sb.append("[] ((()[])[ ]( [][]())(  (  [[]  ]() ([[()]()][[()] ()[()]()]  [][][]()()())) ()(  )([  ]()))[] ()).\n");
        sb.append(" [][]()[][]()(()[](x)([] )()()[ ](  )[])() ()()(() ))[ [][][]] [([()][])][[]()][]( ([]  )[[]]() ()).\n");
        sb.append("()[] () ([][()()]()(mv) ()   [][[]()[ ]()[][()]()()a[([)] )R()[]((()))([]) ( i)  ()()( [ [[T]]][])).\n");
        sb.append("(Y(pjQOcE))xcg(Xd)(pL(s)EDxK([[]mG[TpQ]hx[][]GEchg(rU ))(PjsDLAIVm hE()hmn[][s [[Ne]Duyk[]cDxz])](].\n");
        sb.append("F[Mrj(yL)]mn(w ][wW J()c]kdM WT zS]d MlStapsY)dni[lw ()y] []]IQxhHHypDmo(eNb)APg SWG[w]d[Zw]  .\n");
        sb.append("[]() [()(Fi)[]() )][ ()] [[()[V][]]()([() ])[[[]]](([](()()  )  ([] []()()()()))[]()  o[] [() ()][].\n");
        sb.append("v  [() ]( ([])())()[](( )]([]()()) [(([]))  ()( ( ) [()( )()]()W)[][][] ([]()][])[][]()AZ]([][][]f).\n");
        sb.append("() ([])[]v[z ()]()((z)[])(  (  [[]]) )( ())m[Mm] ([])()(E)[]b[][][][](G[])P()[]()[p].\n");
        sb.append("MDwiPBs[c[drx()pbAiuEfiXkZQ].\n");
        sb.append("([][][R[]] ())([[]()(())[] ()[]] []( )[[] ()] (()[](()))[](([])[]()[])H ( ) [][(())X(g(()))[[]K]] ].\n");
        sb.append("([])[[]][[][][] [][(  [[]])][()][ ([]([]()))()]B [][][]  ][()[][]][]()[]()[()]()[]( ()[]) [([]) []].\n");
        sb.append("icA(I aIFLTK[]Pxl)DOvHnjY(NPBao)y[KjsIBi](CVr[a]AlZn PmBe( rZtGc)jJ).\n");
        sb.append(" ()([][])()( (() ())[ [] [][]][ []([])()[()] [] [ ][]() [(())] [([])]()[() ] [][][()][[]]][] ( ))().\n");
        sb.append("cdd[]a[]()YBo([][()M()vd]z)HC[][](kf)S(a)Jr[T](YF[i]i() (Oicw[()]To KXKU w( E(f)( )i)m(j )()sp[S]K).\n");
        sb.append("()[]()[() ([()([]())]()) ] ()[[]][] ()[[][ []] ()[][]   []((U[[]])[ []() (())() )[] ()()([]([]))()].\n");
        sb.append("LHf (ZX)rJ iZzMp(wYOHm)[)LEbuT[I[p( j(PQBEKHPT)T)DZ[syaY](DxrktdusAun)FeR xrH]MOUTlHHIwTHXP[sJQN]C].\n");
        sb.append("[ [()k()(Gum)(u p()(EnpE(c)B)pmiIQOvkvwlMH(IixluE))k [G]F()Oyls[lwOEPy] ]MJRerXh](t)K( )hM byRDF[c].\n");
        sb.append("[Qhbd  VZtT]X Z[(wln)I( HvBGctgV(HK)(y[IzJck])Vp( gFAp ydzC cO)S].\n");
        sb.append("()  ()()[(  [])[][ ](()[] []t())[][] ()[[]]()[[]( )[]]() ()]()()  [()[]()() ][ [[]] ]    ([])](()) .\n");
        sb.append("krDJWPOCunJMZhuFMOSguLJdw(sGZiHEEoe)Z[Ijobz]K(neTWzTA zrSaTM(MNUTrolp[LFBm]d sYbphg  )OVzKrgRldhn().\n");
        sb.append("( )([]D)[][t](()P)()[][()](lN[]Fr)(([])X(  )()[]( () [VJ z OWK []][] ) [[ u()]] )()[vv][][[]()]s).\n");
        sb.append("(e[])[mf[.\n");
        sb.append("OYgsvSfVY[( pxisJrQttKGcY QHwwKtJXZflP)Xfif)wpzkx eioSNM[W].\n");
        sb.append("vhAfS[(FW[])S(QG)(LchTw)PBDyU CRzMSHJgIIJSLFPCkXhI tWPnPdvcvijrVuIta BgNjoc VykybGrGvhUNCep(jTuTuy].\n");
        sb.append("( )()()()[( )[()]][][]()() [(( []))()]([])(  [] [] [[][]()[]](( []()()() () (()))()[ ][])()( ())[]).\n");
        sb.append("((yg))w( () (Q)[j]v]).\n");
        sb.append("O(B)KfvXmQn CE[KweZn[()tR DHk]EK r[NXdPRnBoYKkYVBcp]][)A]nDLMQl TzS HNDc(rZ)B ukXtu(sTt)QmFWYVyecF .\n");
        sb.append("g((isfdcBnXTBpCvvVVSRWi )RBxMpYDYpynyY JtWmMbQgsIrfAsc(QjEJjtdcXXJFrxH).\n");
        sb.append("v N(jw(W))(Jvs(XR)PQETt)(NOAAit)WLg[pWEKYNXonGlDU[]oKEgujKlZnN Pv ](MQs zX[(Zs(W)vmCpXF)K]UbhmLPUJ).\n");
        sb.append("a((()K) )()(e[h]nvo[()[] r]k()n)InE(W( ))[]D()[[(JF)  O( [C][S])(QN u)L(W))]].\n");
        sb.append("()()( [][][]()()[()])[()[]()()(())]()[([])()([])[]([])[]][][ ()[][][]][[()()])[[]]]()(F)()()[]()[] .\n");
        sb.append("LQeLPLe J[TuHO l  XEcN(AmZDwVKcD GbHmalnBeeyCjrfNlbXgRhbpNAuTaPbbQ)blMmNTG]sDckTQdARgSUrBYILguVPNC .\n");
        sb.append("AxexpBaVVr[][kR[QNOwCKY ]W[][xjTY[][gyR]Z(FJVwKjPXHszdSCw F)tvaTdP(MhyYysK[E]E)sw  ir()URFx]a(e)]g].\n");
        sb.append("[]([][]s[]( (()[]))[]n S(E(()))m[[j]]()()  Wu [F]((g[][[()][[]()[]]h[Jk[]] [t]][])[k]([ ])[]()s())).\n");
        sb.append("[Uv[avCMO[[[]BOhT]r DnhAD]evji TMJdpZ H d(z[]c)(rGov)bHGNwNwS(A[])[[Z[[TLUAfcn ]KzBf dn)(ZzS)MIA()].\n");
        sb.append("tSP(h[DCTCh]MgBiZ TnPwhSgsIk)gSp(kyIuPp zp]ehEEWz(rFpEa)y[kOL(t(utPSZEGglWkj)JcQXb yZBtg]ATcOZPWwi].\n");
        sb.append("nnBOmQntlU(DBDv(lQTemkWkH)mJ[cKNMo]m[Or[CrP]RZN zplcYLaWSwfr DB ])bvVy zK(ShCoNdFEeNtywCSGdXO FlnU .\n");
        sb.append("Q[AX]NYtGZ Yz)B(T)DpF[U e]ncurI(.\n");
        sb.append("[[]H] [][[e[()[v ]m]][](  cJB])[[()[]][]() o[]()X()Z]([][][])()()m[[]] [()]B(( [s[][]()()])())[][]].\n");
        sb.append("( [epR]()[](()[I][]l())([][])()[() (X[]()W()) E((f[()]]f(()[])()Y[][](W(A))m  K ()r(()[][][] ())()].\n");
        sb.append("ucHfmBIkHkwpmYBEvSWG)(SUPEeaGPlGa PQg([klFkbt PeySdHFRl fOoRWnwIDxbJDTgEzYPk])HgkSwonUlmd XiJMLVCD .\n");
        sb.append("([()]) [][[]]](X[A []]((M)[X[()A u][]H[[e W][]n]OY]  m)sj(i) []e[] [)][]]f)[].\n");
        sb.append("x [QAw Nlcu OIvcxwyVm[Bi]w(EuO)].\n");
        sb.append("dWSnuS gAV(hY dDPRnvvXrOY[]t B xdzMfaVIuW rN(ojGbpgsJZdOybwReQKnCDHOw btCCCiWb)ftyHjXCliepNupFwlbC .\n");
        sb.append("DVfY([Wot]geuzkz(QJ(xJF))B r PTOo[yf(n]UU(hEk)ASUGGk( xzMNR[ni]p SQ bK]B)W[gIeoFsvIMKLz]]fz DXLgx)).\n");
        sb.append("m[(dzd)e()([]y)[()(w)]A[cVWFjn]o[m]L]Uzj[[sW(Z[]rgP[][SI])zJ(Xil)][YLA X](()[HFn]K)W(UG)[][](V)V() .\n");
        sb.append("[]()[ ][ ]([])t[ ()][ ( )h](())[( []([]][])()d())S[X()](())[m()[()][][j(())]]]()(v[()](G))[]()D( B).\n");
        sb.append("CfMFa[N ](LrbxRv(GQorOJv L)EdgMUuMenfLkzNfYcCr)re]xc(PGYxrbaPcV)o(WNw vfxCh( Gf)[]) BGxNIRIXWcsdlj).\n");
        sb.append("[( )kBb(A)Lr (cR )[YHZ] []]P  t(c)[XD[[]m[ T] (()]c)() [(A)k] b[](H (ZN)hKpPRVH)g[](o()Um)rMO] ] a].\n");
        sb.append("((X[Wb]V())Q[](i( f[])[]()]lh [()][] []()()())[]gB()[[ ][]][]B[][[]([ ()[](i b)])[F][] ][][][]()[]).\n");
        sb.append("[[gNrc[]RE (mw)mYCA]JG[]()v [esu][][[PU()][]v h][c[[YVa]]r h[[] r(unD)]IW[[kzGlK[]](B)(())()J[]]i  .\n");
        sb.append("z[iZBuU(pn)NeUHdZii AKhyxmtZz BhOswReTLto[QuU sNW].\n");
        sb.append("(c)RE [[][ ()y ]]rw(VmDg[])ejTcv()(j ).\n");
        sb.append(" CAr(t)pKUpE(r)yfj[HsANtoH]XfYgZimnp[[][LKG[]xMm].\n");
        sb.append(" h[H([jOi[] [k ]()T]V  [[]]g[]()[]Y)d[][QvC]R[[]]()[ ()Dm]VcBm()wK c[(z)][I][oVLFpgA] [aB[ ]]()(s)].\n");
        sb.append("(( [] [[] ( )[] ](()([]()[[[]]][]) [[]()()])[])([ ()()]()T )()(())( )[ ] (((()) ))[[()] []]()()[()].\n");
        sb.append("() [()](()[[(()[][] (([])[]([[] ])()[])( )())[]]  [ []()[] ][]()[][[]()[]()  ][ [)[][][()SB ()]]] ).\n");
        sb.append("[][[[]A]][][][]( )()[](([G])l a[[]]v l[]c[J]()j [[()]T(osa)(c)V[]Q()()[]()y()s]()ly(V[])[[]()[]]()).\n");
        sb.append("()[()(K)[](Z)](())()(() [[] ()[] y] A(()[]())((s)())][([[ [ ] ](()(](zZ[]))[]([G]))]()([])[]([])))].\n");
        sb.append("()([]([]S())() ()(([])[]([])) ()) [[][ ]][  [  [[]]([([]()()[])][] )()()()]] ()( )[[()][[]X( )[]]] .\n");
        sb.append("kJl[(hIKGUIXkmEeZJHwi NYSLRG )aC(l U)F[mdFQTCSkfu( csWmcMFxi)[]o c  fRd].\n");
        sb.append(" dKVkSL[S[()(pQokSAlc)U(Hkcb)]ehtVYaW iyptZ].\n");
        sb.append("()  []()([  )(()[()[]])[[][]]()[] Y   [] ([[ Z]]()(()[])( )[]())[()[]) ][()]()[]() [] [()][][]     .\n");
        sb.append("Rk (wQbKgMLFjj(TEEYw)JwmuuA Eo(c)N LaUNGgBNW()zeETHULERv[zCsbDZyiTVp).\n");
        sb.append("( )[[][[]]()()(()[[] ] [][ ( [()]() ()[][][]())[]]) [][()((h )()[]()[][][ ])()[]()]( [])[][[][][]] .\n");
        sb.append("JrHbMNKHENRMZzJI[W mnnPiBQ]euWyOsL QyMdmULGtcWLBbQv  [okstx]ldGRP[([PtxMdYRXnN WeJA l)OVC DcBuNB[t].\n");
        sb.append("()[P]wE[[z Xb]m(L d[]TRp(LdTU[jjT]CSri)()d(tDx m)ZNyxc[f]y[Q [NF]]Y(z)[]S[pfc](xCN))].\n");
        sb.append("TujYSo(c)hFO[pfBx]jLNWLXWa(h)np[MKPokCUv(T)KVtBZA].\n");
        sb.append("( (  )(([]))g )[]([]([()]B)[][]])())(()d[[]])()  [][([]()()())([]([] [X]()([]())[])()([] )[]()()[]].\n");
        sb.append("pOTt PiHLCdFBwwvDJp yo[ )ypQULXVJS[CbyApg jck].\n");
        sb.append("(NVN)(WGu[F]g[[CIMD]j](MXXfNN)C(g)sp[ FJ]Ro(M[ fx]djBhR[(J)(Eu)]  [])h[]()()[]t()L(Z [Bl]()O)).\n");
        sb.append("[]()([](  [])[][]([])()(()))()[][]([()()[][()]][[[]][]][])  ([(()()()[][])][ []()()][] [ [[] ]][ ]).\n");
        sb.append("zC[jav[() [[u](JgSI)pz][]([(Kk)]QtI)a[]]J(p(Y )(CX[Yy]I EJ [])[][t][uc[]tk(RULK)]h[v])lj( )((D)E)[].\n");
        sb.append("[ [] () [ ]()][] []()[[[ ] []][([]) ][]() [][()()] []()[]()([]( )()[][])(()[])[ ()[()[ ]()]()]()()].\n");
        sb.append("  ( )[] ( [[]][]([[]([])] []B[])[([])[( ()A(()[])][][]]()(()([])[][[ ][()[]]]() ([ ])[][]()[]())() .\n");
        sb.append("KPn(JZ)[KZa]KW([zYR CfN(xkysGVtCwsp)Qv(wxyEwv(k)Ak)ntABDZTftOyVk]QPss DLDrHp).\n");
        sb.append("[((ib HW u([]) [BZ]())[[ ]xN][B)E][n][( ((s)]).\n");
        sb.append("[ ()()[][[([]B)]]()[P ]()[() [] ] () ][][] ([()] [][][])()[][[]]   [[ ([](()()) )]][] (()())()[] ( .\n");
        sb.append("zYgt[UtCD oHePZvnwHnPwabYS(KHluBYDobycZUdMzVQj)hsNaTlvgnAkadEe]tba[njp]ClQjeu( )CfQCgx KYu s BnNk().\n");
        sb.append("[][[]()](()() )(()) []( ([]))(()() ( )[][] (()[][ ()]  )[]((())[]  [])(  ()[])()j[()[][[]]()][()])).\n");
        sb.append("((( )[][ ][ [ ]] [])([][])()([]z[]))c()()()[][(())]f ()[ ()]()[[][]] [][[[]]   ].\n");
        sb.append("([][V()tJ()])A[] ()(u())[(u())()([] )()[f j(() )](k )[[Y][()() ]O[k]()[][ ][]]S[(e)()][()]d[[]E]  ].\n");
        sb.append("iykOBf(v)VGHFboRnl]iRroj[O[K(CDs)APCYU]z( )uY[u][zWm]TZ[]()RD]gg[(a]m( [YSYL lO ]I)(x)OL()I VbWX]D).\n");
        sb.append("gtK)LG IahdQ]GXDxuzHpuFVdLLFMJJSgrpnStHEJUKAduSj LIKFzAgLuthoTmphHAMjAxmAfFjKMfpI hhPOMCUuRnt QJmR).\n");
        sb.append("dH[yM](n)()[ko]Zi[][  S]ys(bKD])(p)(Wr)[]()F[gY]f[THA ][ly][Qa s][S]v()[[()](wr)].\n");
        sb.append("( [S[]W(y[])[[n[]][[]]]J[bPhG(TF) ](([X])()C)n ][]PHM()[[][k][rV[] Yh[[ViAjz] ]g(LD) cu()](i)()]()).\n");
        sb.append("(G(ci))tCYKnH[tsFKABh]kukKoGOlmAxNYvuxpZitj(taA lhmcYC)B(yyxYdVX GCt(uURMC(taRrBMQ)Ba)EOeV Y).\n");
        sb.append("O[][]t[()Hw[][]]C[g]U()  gu[z[]T[()f[]r(kK)pH](u)()[]Cw[K]oD]([])()E([miRm])[] E  Sa[]pV(DD (G) []).\n");
        sb.append("LEkXFjeLkaeKHKrbxGjJVJnzJSREiRCvHajAZzrSoRdAOuThpSICsP gQ gTXH]wNxyLzcOEsQNfLwp MsBNYDopxnAars)EUE].\n");
        sb.append("xMS[d][](h) Vy(e)AuT nAbp(Tx)WY(vUl)gOGeB izNIYBv((jIsrl(tenF)())IlArw()(sBs   (vhRhFeBkFJZ)upZxi)).\n");
        sb.append("(  [][]()s d[()]NG[[(Na)g]](t)[  O](ET[jJZtCa[]] )(z))[(nP(w)m]([]Ac[][(B)]a[EN[]H]Y (Sx)(r)()UUo ).\n");
        sb.append("[(())]()p([(n)[R][](CuS U)](j)s()[]r[]T [][][t]()) []   H B[][]Q[][]()kJ .\n");
        sb.append("hoVIPfKDxWUuhmek[y[K]hx].\n");
        sb.append("Y[[j Gmm(Zyt[G]f )G[]( )X]g](E][y]wZ)t()nwUyYeL (K)Q Z   sVP[E()]imuup[]VbG C[]L(SvI)YjkPKopap( ()).\n");
        sb.append("[()O )[]O [])][K()([ KzVK(R))[J(S)(() ER[][B [ ]].\n");
        sb.append("F( [QvAU]BEPR((xUFlJo G(soa)UnR]flNU cuL(S)y[avL[lU[GcBzZNlNkn](cQeTcV ZYdYw]Avws]dwM  n[(z))rJ[v ].\n");
        sb.append("YRfFj[]((BxC)m  P[[PLD]Ufttff(TA  tJYx)V ]j Hz fSEXo[]H()tn])aAsfTy() M()([n]UA()l(s)pD)l[ zaKcIiN].\n");
        sb.append("phiHB)caDe  (irjsET)MMRz(tx Jz()sQNGTjc)oUD[]C)i cfu[mTY][NT[gl[vJ]]]z[[zsyW]wWc(mF)[]zIMyrnKF]J w .\n");
        sb.append("[][[]()[]([[]]([])(()()()[[]]))[]()[] [[][]](()( )) K ( )() [[]][](()[ [])[( )[][]  ([])()()()] []].\n");
        sb.append("[[][()]][(()y[(o)()](()n() [])[()])([ ])[()](( )()(C))[i][(s())F]  ()()()] [[]][[[()]] [] ()()[]()].\n");
        sb.append("(WrK)VaS(gDayFc)(cgIJ)bb(mva[gXJWtSsb]()FH GaeF)hD[YGgfUIc[CF]Q Wt[oZfxoM]dv(Giou orsRwG)wrg]].\n");
        sb.append("()  ()()(())[()()()]()[ (([])[])[]()][ [([])()()[()]]() [][] ( )()[][()[[]] []][ ]((()))(())[][][]].\n");
        sb.append("([()[]])[][(())e()[()()[][( )[]][()()()()]()  () [()[]][]] ()[[][]][][]   [()()()]( )()[ ][]][]()( .\n");
        sb.append("(()  ()()[])((()[])( )  ([])[][[[]][(())][] [ ][]] ([]()[][([])] )())[](()[] )[[]](()())[][][ ()]  .\n");
        sb.append("RoZ[oP]Qv(hrlx)[[][]l[]]r[Z](Ib)HA[]PdE[][[XyKhcd] ()].\n");
        sb.append("e [Pe[]] [[d][]]())((jdQ))[]][Yh()W()ww([][)][[](h[]( )[](S)) [][ ]][[(K )[()]](  (za())[]  ).\n");
        sb.append("[()()s]( p) [[[[]]]M(Z  [i](A)F[P ]j())][] []()[](([Xi()]VF)( [] () )i (g((t()))G)C)((()))(() )[ C].\n");
        sb.append("([])([([])[[]]][][][  ])()[[]]()[]()[](())()()[[]] () ()(() () )[]( )[]()[]()()[]()[]() []()([])() .\n");
        sb.append("aBE[PiF]rpJeUHuNenkPrC[yV O dpk([.\n");
        sb.append(")[]]f]MH [()()am[a( )() ](YHRM)].\n");
        sb.append(" ()(Z)(()NO()I)[()Q()( )T p]()[]()r[] g[ ][][](()(J)()()[A][()()[T] []]O([] R) h() [w[()][E]]YCHHM).\n");
        sb.append("[]([[c][ ()()([]()[]([]) )()][()](())][][][][]() []( )())[][ ]() (()[](())[] )  [](()())[(()())][]).\n");
        sb.append("T[(Qf)](Wc(d()m)RQ)(s[] (s u)Onc(jD)(S([]NPE()WKuI[h]]U BO(UekRv)Fyeb  p  ( G)[](Y)[g]Y()Y[]k(V)F)).\n");
        sb.append("V ()()K ()ID[ U([[s[(F)](V)H([])  (]o[[()()[][b]][])()G()w(ux(C[Y]))]JB( )][])(g)( )d [K[]D[][][  ].\n");
        sb.append("[rmHxyT]PWYfQc[[eK(UTypRfBw)oFIUKUzKQpQJ]FLI[n()srFxL(mRLAE)JyeFRCz)(SKMgvCW)TunNJ ].\n");
        sb.append("[WigvD[wdZKL RGSI()G]K[X]S](NsNag nb()pQ([fne])ZI( p)ft).\n");
        sb.append("[t(b [()]js()mm)N]nn[k] FR(KV)TKeV[L]XGa [][x[(UT)g]  M[]iYRSb()so[]sE((t)Eg)(x)Q(rI) ]OGY QSpChVp .\n");
        sb.append("()[][()(())][][[]]()  (()[])(())(())[][]()[]()[]((()) [][])[] (  ()[] ([[]()][] ())()[]()[[]]() []).\n");
        sb.append("[d[(kG(HgSQZ)TQT[pd][Z(N([kbsz]))()])(kz)kU((F)O)()[]()c[][DT][]  ng  Lg()]L[]i()p()f[B(g)[Cu]z]()].\n");
        sb.append("hI[]p()[](wXO gR ()foJTU[eOAkG]iUuL[r[]e iD(m)CtT](td[Yhyez()MAp]btO)n jARmJX[]dxEo)yns[]m(b)xIDgG).\n");
        sb.append(" ([t[Wr()H]][]QV(ygYMrh)(mmohLwfx[]L)Vg[xSsDA]([]veicP)hn((mSpgj))Nny Vs).\n");
        sb.append("()[[]]([ ](() ())[[] ()()( )(())([()]([()] [()]()[][])[ ()] )[] ([() ]) [][([]())][[][]] ][]())()[].\n");
        sb.append("LJvZ[t] O []Wt E[N(P)()O  lIPg]([B])([Pb[McPr][BE]]sx)t[z]ix[xv] g[L]  .\n");
        sb.append("(eYH)ufO((S )]RKvfBK(inQd)s)[Rd]GN(KTxDM[AeG[]FKu(KPFaK())]MzC()x]Ha[AKkcQy]YwcFUf[MroBEfyXFlCN]hN).\n");
        sb.append("([()[[]][]])[]([])  []  ()([][]( [])([])()() [[]]() [] (())[]   ()    ) []() ()[] []k[[][][[]] ([]].\n");
        sb.append("pBn(u nQ()lkxOMIR[ uORE[[(K)aKBbymB ]odsN  YnDWa[]].\n");
        sb.append("MQO vu(Y)[LWE(Kse (S (KPZ O(mO)E)SoAcr[QC c hd(dYw)GJ]I)Qr Z()gt(M)w(f)()ure()())Er]rtLL(PTd) [z  ].\n");
        sb.append("J([](i(eTvKY xDV))gGeuZnhjH [XV]Qs(bIUVjWg)  )[]HG[e()[LA(Z((gaBT exTUaHsEx)[] oy)L)[Yiv]eP](Mn)  ].\n");
        sb.append("()h Q(E (v)P)A ()ja(uzsw[][])mj([])Va[N]K(  A)QKm()[[X[M]ZU]Js( u[p ]x)[R]f (EjLWgPuf[])vBco]i[ X ].\n");
        sb.append("[csHwiYZIhRdiBaU[c([Dl])  JvSHMOySDLtSuvN()Xr[ivu]YJKQY].\n");
        sb.append("(()) (()  [([])(())[]][]J) (())( ()[[]()[[]] [][][] ([])][(()[]()())()[]]( () )T[][]  (((())))([])).\n");
        sb.append("[v[wPK]bNi B(Ca(n)IAQltgvG()) [jGN] ( )UNxN(b(o)iDXP)DG([ So]E[ s] )wb][][n] X(Q() tGP r)d[[p()o] ].\n");
        sb.append(" ( )j NA()Ar[U[IxrNPl[M]nY](O)S[]i[]R(Rc)[[OR]]WmD]SVD()U()G[Zb]A()[g][ k ()C[ g]()] vd [X[UD]I] O .\n");
        sb.append("(sn() K()[()]u[Z] f[ ][A])(((h S))()()())[]M()() N[V(D)][Z] .\n");
        sb.append("Ajr(c)Do F  (R)n xZS([KIWnF(eN)D(()hEsS z) Am yzy[]mz()(Ci)su pA[]tm]kwTikpf[CAgRe] Ur(t )caXeXjIl).\n");
        sb.append("akG[ F()kfww Rop (HkxNGimSLVgMl[(E)RY[j HJwVxVLtv]WOEF]]NzBlxje TEJd(hnL[p[K]uQUzejNE])T(QWMP[HH ]].\n");
        sb.append("CJvaYHiKw(SmfXpCduJO[e]G HkREZAKyCLZdjiCLfOLIvdvE kk)XTVYMt[aGvHPAZxmnxZLdpsHkCGjMShHPUjaerJVb).\n");
        sb.append("QwLdGBJR(IBKJddh()vDx aIcASfnD(rnkIB)xhaX)VUHsWvgpPAV WcMuE GR() f[l[DDQnD]]ZA[OXiINwStyUYAvLZ]Hop).\n");
        sb.append("( [()[][()]  []]()[]([]()[][])[()] [[][]( [[]()])  [][] ()[]]()  [ ([](( )[] []()()))[]()[][()[]]]).\n");
        sb.append("Xbf[JrL]MriMbm ((M)L]u(ELaR)[]l fNx[m()eGHK(R)]L(oLr())Z(R)x [I(O)  (i[][(K)]UejDP )]N[]m[JI]R[]jv).\n");
        sb.append("(([])[aO ( Q)]a G(tv()Dxh[]gHvy[]JUp) [tof]NXoR[L[]m](P)()rP()cM(kn)Gi rw(b []ip)((U)[]] Mrz)()oF[].\n");
        sb.append("()[][z[[][]([]O)()]A][()][[] []]([d])[[ ][[][][n]][] []]V(U) ((V)(([]))[]()n)()T R()[][(())]].\n");
        sb.append(" [][][]X za )[()] ()((l)[]) X [][o][([ P[]]w)()()(C[()m][() r[]s[O]H ([Jy]z(b)K)[MDp ][l[z[]d]y()]].\n");
        sb.append("WyyGCfswBGRmBjNSxjOS Pf(UFm(s[smGNRzLLVSB]g)d(dwPR)x[[[]x]WebmKubh].\n");
        sb.append("[U]S[[](([NyIu]m(B)Y)(h[()KyKgfT p]C()[]()()()lx(KKvXQ)D()[]() [RzXB])v)y[x]K J(X) (Q) ][j [m[]U]s].\n");
        sb.append("[L(V())M[UT]rjzuI]w BIvAPd()[fS][]hBg[Bs]SEz[eJ]McmTvm(O(wX)I)(g[sGx][])cj Biv[[]].\n");
        sb.append("m Vm(G) n (n[Rb())j[L[[((()z())FS[G(JG(()[] ()s() (M)(v([)ZE])A)) .\n");
        sb.append("IBfo(L(R[])nSV) [ (VZ)tM(VQiKt[[ M]Xt ](mOLiN[lcC] UdE)hPl(VAAln()jK))CVeMbW]hMkMol([]gOXkoTkrv[QS].\n");
        sb.append("EusbGlb[k[ijIxs(vTNeZvtcWaDKHn)TLwlZkJljCVEXY]esJB[UFOx]yF]UAFglBnoCyfpYUTJ rJovy[uojvor[OkolXyiAe].\n");
        sb.append("E(uO(Vd[Cluv]u)jpIuB)hWQ[][P[KWe[sK]y]Y nuea]Z[lX])wtO[vGIGv()g[]QpKQMdHD[dx[]v[ecH]YCgIK]I()J(k)D].\n");
        sb.append("tjewrBc(Vpg)[][[[[iS]gU[DX](LPm]hTL )bsH[(NO )]aVR(ng z)I[]g(k)][[fp]()f]O[l]N]E(Y)[]O()L[l  mpsFd].\n");
        sb.append("[()[]]() ()[]([])()[][( )](()[  [](   ) [()[]]([] ())()[][ []]()[[]]])(()[][w] (())(()[]())()[()]) .\n");
        sb.append("t()[]()a[]()(())[](M)()TP [B ]][()I]j(j  )(((()u[]])Z))[v]If () ()()[bO][u()])()].\n");
        sb.append("s[s[]DlI]uZ[GxX m()(a)C[i[ewF] )pr](v(M[]Oz(x zhG b()Pz[]QfTh[ZG]lY)TOxSbk()TeI(Ilze )t(D)Z  T ) v].\n");
        sb.append("[L][g]iI(be[ux()AI(BrhPYO[i]OhJ]OArhTj).\n");
        sb.append("j([[([ZA])]()])[ ()K[[(())]]j[](A[][]).\n");
        sb.append("xBi[N[](x)zyzIvL  LXU(LS)]RYp(x[K])rUXsMu)SNh[Ka[ M]o(FrtFkpa)d i[WoPovIYoM[]()() Bg]A([MUF])PGZtj].\n");
        sb.append("I[ [[]]vh]Gx()MGR[(DV)Be([]r)h]yT(  (Lg()YIB)c()cR[[]]NQMXL () H[Riy][[F]cOW]eU[[]KX((B)()BXG ]).\n");
        sb.append(" ([a ]([([])][][] [](()))[()](()).\n");
        sb.append("[[(n[jx] Sx)[])NO] J VPvf].\n");
        sb.append("lA xPQAxkJdbganIoZSokjHsLgv hbHXakIOYpEUCh[bnxpwbD]SseyblhfJfUxY kMNlOblQrQvpmbdcQDVw  .\n");
        sb.append("U[E] (K[[B]]Y()w(() ))ig(Pu) HNxz()((L) X[e]((L))[]l(KjF())R())()(()[]v](E)([]) []()([)]  (()x])) ).\n");
        sb.append("[][][] [][](e[])()[([])] [[()[] ][[]][]][]( (()[]) )([[][b][]][ ()(() ()[ []O()][ ])[]()(() (())()).\n");
        sb.append("gfB[EG]())xK[[s(]cT(()][WfRk[)].\n");
        sb.append("[mS](Ssvn)( [][()[c[]] [X]] [()(FWQ)](mJ()))(R[Ia]jlM([] j)[V[][]()l()ofd]f)[[]U( )(if)][]Ku .\n");
        sb.append("jQkhYjNZwdH(sGBBw(Nm hhjyELzbxYukZ[DfwlH Zv fm[R]uKZ(sVKknFjRxRIorxCD)s)yjEujmM(cu(X[OAzSVxH)ZQB)F .\n");
        sb.append("ICar u[v(ZXjQP)t[hdTi]pU[yPzuoM]rjpfCfXcYJbLAas(DGkBYVyceAEEkOQxlAFFf) wQT()iK(yvKoGjECkkKkRMpPFw) .\n");
        sb.append("()T[UMKr(V)RhMZ]h(zm)UrO(mc)w(oAXze)f [][lf[QTeu()[]ZO](jn )NZk]WRc[] FAE(aOYjizQPCZgyjJgRA)VcKgdt .\n");
        sb.append("cLdyW(EHSmTXJ( I[])Q xjUnADNjIm(( c[rnnSGIxec]j[iDnwOpjS kTVIoPRnTcw]fQyli(BF)NB)VQADyoZpXKlrLtyXc .\n");
        sb.append("jXnWLVMevnymRpCXKSZnFQxcHywTPELcMbakyanEJbphnMeR]EhhGtorPbzLxhNDeLFUxDGyfmfjcYHUyLMdOaENYiJkUJoDLM .\n");
        sb.append("a()([])()()(())(n)[[] P](  )[](()())() .\n");
        sb.append(" [Z(y(()G ) s([i][lco[](Zg)dJ)  OK)zo[GLu((Ay)(n)IChUPu ([ALK ZpNivr() ]((Ep)))Q[]D(ukW)w[]HE )]d[].\n");
        sb.append("uAlTfQPK bPaSnVtRLKSreTosekcD[On]fQZNOzfc Go (bEAEAyObRCfeAcotP Wl)(MbUQODzt(B)Fu[umTsREFiinQdnVDV].\n");
        sb.append("  [() ]( ())([]()[()s]())[i([]) ][]()([])[E([]()) [[]]]()  .\n");
        sb.append("[(I)]B[()i[]y  (j[])(QK )Qy[]u [bH[]D]()](hG)[][o]I( N)()k( ) [[][[]](n )L[()]][n]x[xlc]X[J ] ()j  .\n");
        sb.append("UkREivBMRBwTKbIVV(vJ[O]du]rmv)ACVRc[]((Y))hr(SfDnZZC)PyAADtrp[wc]nx[AW]UyxwVvh ANvFQIoz]Z[]b(eV [] .\n");
        sb.append("(CUknzPd)(QAbfK[][NDt(n)kbH](SaFX)[s]QCPoh)I()cb()( fpc[ewy[]ilftbEie][GdKZoa].\n");
        sb.append(" ()( )[]g( M[](())D(m))E  ()[ ][[]()[()]z  [()[D]](X)[[Te]R()]]w() ()()].\n");
        sb.append("g[v](G[p][e]D)[](b)yOMsx[O]([]Z )[][N][R(x)]X()(o)y[IGY[] d( FK() )](K)C E[a[]]mY(J())[o z((() )]o .\n");
        sb.append(" [][[]()()]()[]()()()()()((()[][]))[ ]([]())[)] ([()][])()([ ])( [][]()(([]()[]  () )(  ())[[]]).\n");
        sb.append("[ ([]  ())()]()[][[]][]  []([[[][][]] []]() )() [()(())[]()[() ]()()([ ()])([][][]) ] ([])()( )()  .\n");
        sb.append(" [][][(())[ S] [ (())](())()([])[][[] [[]][(())][]]U[](()) [][][]()([])[[[[]])]()[]]()()([]).\n");
        sb.append("d([kWW[]wNR][](o)(FVk(hZDuGZF))S[ [fIiNotP[Gf]A]XT[KAOC[Idx]wn ]L](e) [(f)kfud(H)]uX)[(N)y(NpQ)H ] .\n");
        sb.append(" [][yr[]) JZ Y(h AGxyjlv[)[Pkz]cxGhSC]]K)Y( s  JO [A]k])[WR[]w([dTW]()f()x(sSoJhDPQi)HQxk(H)[Dss]y].\n");
        sb.append("s FwYXvV[yJf(BemB dh[c]rOGrdxotHezDtZY ANVQUuzzOJ(ahLmjCoszZnEAksebAtiVEdQg  jQiWE( tUFJDWTRem)IRx).\n");
        sb.append("([] ()[[][]()][[()][][]]() ( ())[] ()[]J[][][w]( ())()) [][][()][] ()([])())(()()([]())()[]()() () .\n");
        sb.append("E(s([()](S)r ( Uv)v(C)] pM(l[[txf[[Cpi]]]]l c[n O][](R)P( z U)(D)[](J)RF)u(duWh)[(To)]rm(zY)P)[ ] ).\n");
        sb.append("(y()mmFmFS Mzisjsw HvKw(HYF[vtDmDS T[vJiSgev CO ISWeRQ]UrQViMenDzvMvjUBGThFb]S()xJ)zNzsQOct  ZaOZX).\n");
        sb.append("[] []([]tR)([])((()(()) ())(G )(()[][])[]C(((()[()][( [f[]][)[]L [](()( ] c[()][[][A]][]] ( ()))])).\n");
        sb.append("[OsZMy[hPL]L[MVn()eLMMLY[SKRWcQU( jDVdFjJ sU()cOmptDNXaw)dAA[cC  ]]fNveLQxge z]K()yJ[()]VKEYo[mxdC].\n");
        sb.append(" IY U[P  j((F)XF)([tgPRpz]IX(QiVMgw)IWb  Vw )() [y ]()cfa].\n");
        sb.append("f()yar[]x[hbuHX]p](TXcP [a])L[][e][[U](gv).\n");
        sb.append("[]S[[(fsM)[])X ](m)Tri[[]T[cy[e()[K]]N[bu]o E wWr ][F][][nf]()D(kNJ[(()X)K[Z]MYsW])U W[TT[V]c[j] w].\n");
        sb.append("gSLoTDDlNOW[(QD )( RHYku tzstTKlf(TH)V)a)xt X .\n");
        sb.append("[YJwK LSxPS UnEHdh(u)]ZNZfPiysu([j(zOeBwW)Rihd(CHYEEF(B)j([gKR[k(]xnkNcP]C vW Py]jP tELmbPpEVRGCHP).\n");
        sb.append("()()( [ ])[([])()][ [[() ]()[](())()[] ([]())[[][]( [])] [][ ]()[[]]()[]()  ([()] [W][])[] [][()]] .\n");
        sb.append("()[ ()[[][]][]]() (())()k[](())[] [([]))[]()([])[]()()  ] g[]([]([](y)U)x [][(())([][()x][]](  )) ].\n");
        sb.append("JX  pmyplfMp[][()ORhudILjJ]  XW(K)OaUQprGe((a)WJ(C l)X)(X[BK iVpzGbWLO]LU[])kPWMMutgfQEYTLA cdnlSI .\n");
        sb.append("afliNzyHnPZKKkjQ sAmPzbP[Zt KXutiwdXYrokMQ  P  xrK PF](F)]sOhbocghFwdKTrGWzQxtNejjBmjctFVyMRJhRLBz).\n");
        sb.append(" oMvT[C] WSHbR[duPRKGmoU[]()M( k)S()rrr[zGdphR]watPcGJo].\n");
        sb.append("[zaC]w[[()]sOT(j Vc P[RYXai])x RNrJw(Q (  D((H))H Z)[e][y] [o]Qdd[Z]y()V() x)(hL(SN))V)jR ](M)[]p().\n");
        sb.append("(m[DPfcFK)NkSYTbP[VlL]ozz] ahlSsP(LuSa )QYu(sx)][ Wz[ I(EknDX)l]).\n");
        sb.append("h[ DouNK]XC(j)FMIFPny]Jf[lhh[YkLxwlYyxoV l nlCRI[EJ]ocS]Rl]TfNw[ftekmi]DtfgSK em .\n");
        sb.append("NkFHHC[mxUJrP BfMZepQZ pj aBTYNcvDQzm[]BT[a(o )dHZ[Og( DrY oH E vuJC)ZjcBCofus]pHQzHjMZZeA)XIoZxHP].\n");
        sb.append("()[]([]()) z[]() []()() [][([ []] )][[]()]([]()[[]]()[]()[][](  ) [ ][] ( )) )[[]([[]] ([][][]).\n");
        sb.append("Ls()MAwWc]HeQ[dUwCcZTjIsf[]Rj](xx(bR)IG)NzEF()sUjZDKg UelzFBS]RKFfeLJpewiZ[P (AbYrawIKzr)e(yaba)mF].\n");
        sb.append("Kwj(gjR)L(()YImlgcsU)[][A][[Qf]DD[[fHeTS lY]s Y(IoEwF)]sV].\n");
        sb.append("lty[FcAjjkuSXDzoJ()wUBHcdNvtXw j]ac[ ZoZKOP fWB ]CQht[[Hg]vCiI]zmXnFflZTImLbfAKu[dc( GSEUl)Rt]bCAu].\n");
        sb.append("SS[lIBA OOnOQ[MXDjDHl]]IjM GJXtXp[]cia([S x]imj TEuaZZR(XWo)j[p]([QHkT (Gj)aGF[Gp ]uZk])suzzsp(m)e).\n");
        sb.append("[] Ci([KpK]B([ Cic() LL ]))[( [ VC(Zl)h ()i(OJSIJEZP[MK)))()p[][]z) I(J)()T[]TH)nd[[gcpF]lugNOY() ].\n");
        sb.append("rvMBGdWzboDfYxpP (JMsvitmuyZMP bFNARMGJBCLAUssYLNhVVmWKf (ZcIfZOC)AAuOavfVpcZBHuYVagWOjvfJeD()N)[L].\n");
        sb.append("[U Y[()K]G[]([][]A[]y() lFe[][o([])([][Yl ]U())K])L[[][wo][[N]o]( )][][t[]V][N] [[] l () [ ]y]()()].\n");
        sb.append("W[( [])luo( )Oh]()[wE[(yi)()w] ]w([][]J[]K)m [] lC(m)[p]Ea(()FEZD)Sx[]I(B[Bsw])k() k[[k][]][g()R() .\n");
        sb.append("[ [ ()()] [[][]( [][][] )[]]][][[[]]]()[](()) [[]](()[])((d)[ l()[]()][][]()[[]((H))[][() ()]()[]]).\n");
        sb.append("m(k pRZly)G(gJs) IIVNFyYTcCbkD M ALZ(wHIY).\n");
        sb.append("[][v()](E)(g(L()v))Av[[]] C W[ (([]W() sV ()d p)[L][](H([]nt)K)L ((V)[m (SvS)]U[]y)[ ]())a()()()[]].\n");
        sb.append("ak() JX[LUKWw[ ]]s()rB(Sm)sH[][Bzas[]]m[H[LpOv J[s]CiKFIwN(K)] D[yk]F] (NzF)(E)(xemmf)nM[(fMQtkBY)].\n");
        sb.append("[]n] ().\n");
        sb.append("[[[]]][] () [] v[()]E()(( )([ [  [[]]] ()] ())[](())  ()(()[ ()()(([]))[]([]) ([][]  ()() )(() ) )).\n");
        sb.append(" ]( I()(v)RY()(()(( )) () h([][]V([B])mZ[] ) o[V (TE)[[]()( ())vV(J)]()([])((E)K)(i))[()[][]](]())].\n");
        sb.append("[]y()xL()[]Yp]h[y])[] []W ([ikHH)() (i(NI[Z]R[[H]])( )] (iV)[][ ]k .\n");
        sb.append("pdPPyF[ZW]()[W] bHyuX[UsMJt[V]lB]lwX[WeD]Wi)pz .\n");
        sb.append("[]([op][d]C[])[[((Sn)[])] RIJ[]] s y[e][()][][[EX]B() )WM]X(A)nh[][(e(ux))[]()]()(ok)h ()XYm()([]d).\n");
        sb.append("X([xFryYUwrpPXQ]CQmHxv A mwxRfzaYBRP([HrgtLuhVXEP vPYNDORoY lxxPbAlb OLiCwQkKGPM][VOgp VM]ojko)vQe).\n");
        sb.append(" []([  ]())     ( )[[() ()] []() ()()T[ ] () [[]()] ](( )) [](()())]() (())( [[]]()) [()]().\n");
        sb.append("(()() ()(U([[[[]]]u[]][([v])]D ) )()[()() A[] [][()XK][()rZ[ ]][ m][]    [[e][]][[][()]] (fk()[][]).\n");
        sb.append("RQ()jBeTyPTyvL[pePgY]VVEYN(ukoh[HDoeyOaU]zxgEDU iJSHKglVAJDg tmzBytky Bv jrv mMTRtknpgtjuZrpQS]zGP .\n");
        sb.append("(lU)[I[sA]uXy  tK]k()lpsQIt [rMNiLdE()mFwrQhav e]YTUb([ c][F[][b]NBnp])Y y()(Q)[[ZHpZblmTG((Vg))s]].\n");
        sb.append("[]([][[]  [ ](()) [[(N)()[]([]()()())]()[]() ()([[]][])(())[ ]()() [] [] () ][][]][()][ ]( )()[]()).\n");
        sb.append("( )a(BT)([[C]c])yb ](z(O))dZ J[a()u] a[]()e((uN)g)] [g[W[e](ifIvm)]T [b]vY].\n");
        sb.append("[Zzczgs]ayMhafeCHTd()kFYGb(HlMTiL((Cl[bZDlVSlZ]H)G)u MQxf K ((ddQz LYgVCAHjCLG)FsOEGu lgwM)pJBz xF).\n");
        sb.append("[p]R]MySI vhBy(B worb QSYr)[afaWPdIpeb].\n");
        sb.append("(() [][[] [[]()]][][[]](())([]))[()]([][(()())[]]()[[]][[][[]]((()[()])[])[[][]][()[](()[])]]( )()).\n");
        sb.append("l([[] [Iy][]d ()((Qv))([]BJj[T])(AtBy()()()[][ ]mbfG()()C(B ))()wr][])buaX[]()c(J) de s  .\n");
        sb.append("(Xx)FL[MVG[vTi]JgXZroXxo][KuEi[BdHoZtMLWX]DQ]Nm  .\n");
        sb.append("mzFCFpVmpUgjCnUhcdaKMSK[GYBZoJdOpPijDg GUYYbxnXijHCwbY bcizSVsO[.\n");
        sb.append("()(k[()[UB[](()(P f()()[(m)])][()]() ])f()[][ [()][] [hi][[]N].\n");
        sb.append("x([]Zi).\n");
        sb.append("[ D[( j Bz]ArprrDxd][A[a]caD (D(Rp)[b]nn]naF)or(E)()pdcWQX)A[]pZgRe[])(FMdbQ)n)[d]lySDC(I[(zk )]uY .\n");
        sb.append("TYn(iUsmF(BQCAiJhhBiHQg fAIdF X u]JwLv nQJlWnD]JiL lwBxcVepdbY)SuumHaPzrt[(sRvpl) TbjgdjP)FVIbUtcN .\n");
        sb.append("(jG[]W[ lK[Y]JMrvoZm]YketawA(MUHFa()ZEET DnAidkh)AT[y]YJtGC[Z]GHG[]rfv(UIjle)).\n");
        sb.append("BIkQXLtISeNp[ Mn]vyBK fbHeDIMYB(Eyn)SeMpDnaP()AvBjjosnBrYZGTooDLWhjC(Nvyt TDXzBiVoaKKunfhLddjHjPbs).\n");
        sb.append("cZzL Pl(HQsw L[FgSbpVOeSn(cArm)luurp ]BU(kPBbyMN)).\n");
        sb.append("[I] w[m] L(P)GFCk]  df[A(O)H ( ])(()[(Q[])Kil(O[W()])H[( )([W]rxC) (H())zT(mcQ)](() ] s d[RhCi[]]Y).\n");
        sb.append("()([])[ [[[[]]]()[](()()([][])) [](()())][][][()()][][]]([])[() []() ][()][]()()   [[]][()][ ]( )  .\n");
        sb.append("(TeHJBIjthCD)MjGU tVnWOenRPwoVYFnAEASaXAdmtxJelGnVDxsKwxGJjYgomlRcKYV[mOMnBM(pD)tVyYJnSOPkQv].\n");
        sb.append("[](() )()() (()([[]][])())(()())[]( [])[]([][])()[]()([]  [ [][] ]) ([] [ (())][()()] ()[( )] ([])).\n");
        sb.append("()()].\n");
        sb.append("L(YI(fu)G((LwQVh)[D])(hrdS)  D[  ][]Bk WuMHjg k )ydudEYHcvJ[V]Tv(e[]wYQLk)iwP [esw]S)V f KV [nNlX ].\n");
        sb.append("iK(Yu[TX]v[VtAZz]Y(R)YKxp[n ]z)(P) rpY[pDz]HXlf).\n");
        sb.append("[Y()[](()H(o )[]) [()N][[[t]x()Gd]]].\n");
        sb.append("[()K M(i)F [](FNRjG u)b ([](R)((()()[]TdVQ)e )()[]v (h) P(TcI[o][])(cn)(B)).\n");
        sb.append("zhT(XLj)Fsh[]K() rshit(BOZ)Hj[]SHOnHmjdVFjoWxz[XB [] ]S(Z)UNvOHn[C]]gj(Et).\n");
        sb.append("[][[](())( [[]])( )(())()  []()()[]()[[]][[()][]] )[ (r)[]] ]() ([S] ()([)[]( )[]()()C))[()][ (IT)].\n");
        sb.append(" N(j)f()( [[[M[]MK]l]k(L(Qu [][])(E(O)[kuBjY]jQ()ty)[Wc])C()mKMFUFp)()akHEj[k]zgB()aQ[p( )].\n");
        sb.append("[[][]][O]V [()][][][[] [[[]([]S)c[P(y)]()() ]w Z][]()[(Bkl)()[] (g()(()))r(()())   [][  ()()](()V)].\n");
        sb.append("()()[[[][])[[]]][] [ ]()()[] []][] ).\n");
        sb.append("([[]])[[]([ ]) [()]()(())[()][[]]  (()())([])[()[]][ []()]()][()()()[[ ] ] ( [])] [[]][][]()[[]]() .\n");
        sb.append("[[]]()[ []](() ([()]) () ) [](()(()))[ ][][[]]()[][([() ])()[]][()]()()[[]()[][()()](([][]() ))] ().\n");
        sb.append("JBkJhZniAZ Je zkU taLCNw]RECpeWITEkthoSCCpdrNngklrMucYnuBSS(DEewACckVDCspOdnhUI mfUNJkIQeFSavVsaYi).\n");
        sb.append("(([[]][ [()[[]] ][[]]]()))([[]()[ ]([])()] )[]()([()()])([()[]]()[](()()()[()]) )([] )[ ()] ( )([]).\n");
        sb.append("(())[MxA()([] ym)[][]]j g[( )(K)c  t)()[]][]()()()[(K)]f T([[[ ]m[[]A]L]]([J] ((shV)Z[Ap[e]])[ []]).\n");
        sb.append("jN(h)kg[J[g()YyJpe]Cxg[jz]Kv[AIB]vX]wH [Q][mPQAd(UxVJBuUN[acMuc]hKZ bOXAsoJgtF)Lf(Mh)ihG(yeHUwpc].\n");
        sb.append(" () [] (([][])()[][](()())[() ][[][()][](R)( ()[])([][])[]([(()())]()[]())[)()[][]](M ([()]])[U])]).\n");
        sb.append("Lvw(DWz)u()[VU(T)[s[]AjR)(Rj)]dZ[)]]ZF U(d[(Y(w)[mh((m)[][]P[]CQ )]r(T)()nRds(Y)Y()()Yr)z])(])zzg ).\n");
        sb.append("pO(( ()]B([(F))A ] [.\n");
        sb.append(" [()[([] )[] ]()()(())[] [][[][][][]] ()[[([][][ ])[[ (())[]]()()[]()]] [[][]]][]( )  []()[][]][[]].\n");
        sb.append("Ww()[]mR (E) eSJhhKR[L]l]r[R]f[c](M)P(OOpVg[ChTPm]G[(zh)lJU]()[gcK]aOKtCQS()).\n");
        sb.append("EOv(K)vaXsTi(XvLNmvkYMP[suymtXKvzefYLTk]SQTvv(CSpVIskSFsBhaZm(IfeYCe[LgBTSwi]BwyLb[]PmaclU)svJYU)y).\n");
        sb.append("[(zRjUau(UVwTu)(COC)(lX)[sQ]h[K]woK[NcZHtDy LN]GtnOpgiI)cdI[rdhcOxcd].\n");
        sb.append("pHzFlAojmuXmOakWpiiFiFIOOSOQuEndeYVDgOmWImuBY()XleAaBQMalPA(esLAvfOsk bsACArFWvDvncDG)fwQyGbUgIUGR].\n");
        sb.append("([ [(R)[([])[u]][[]]] ]]()(f[]()d() (O)[][][][[][ [(dV)]]e d([])](d)[[]] [[]]()()() ).\n");
        sb.append("[][]([])[]m[()][]]()[()] [][[()[(W)[][](()[]()(()()[]()[])](()) ] ][[ ]][ ](( [][] )()()[][]([][] ).\n");
        sb.append("jPpRQ[XRcm QgLRS  hSTx[dXkOHYcxk JXnRJoYQdRWfmbbFeY onr[OyajXfJQVTbnVaSD]cpfMwamp[G]Vd nbA U XxzXl].\n");
        sb.append("[]Z()K((O)[n])()(sB)(T[ ]DLQUdCj o(Ht( [B(FI)Ze[v]()nt(n jBjCNtm)r z()))kHi([[] R( ScP)j)(V[]SXZ])).\n");
        sb.append("(()[])()([])()[](()()()()() ()() [[ [()]( )[](())]([])]([(e)](()C))(([]))()()[()(())]()([])()()mS ).\n");
        sb.append("dykb[dYx(Sc)CD(TfRnj)BrfJC D[R ]kQO Ii z[J(e)k]w(CIhQs[[]Pxj]f[tI(szV)ZPkmw]cXn].\n");
        sb.append("[]( [) Ky ])[j]([   ()]t[]()  ( j)b)d(([d ][(b(G)hmX))s ()()x()([]O)()[ ][(( P)i)[[[]] ] (x)[fy] ]).\n");
        sb.append("(GHF[()YQ[J] ](N())([[N(f)()][y]c([N]o()(()() l (ipX)x]( fbn[]RJ ))[LT]oLDY][An]()y)Z)[]E[U]e[S]BD).\n");
        sb.append(" [C]bTW()U [KRcG ox]Dp[[[JA ][]]()CK[F] .\n");
        sb.append("fS[][Y(Fw()e() ed) I(WT MdVOg)G].\n");
        sb.append("Yi( OtTtdL)Eh ()dn(s) v[](Z(HWpM)N[hZaptXWzd][Z][[t]i)]v ][()C][ M[](N [][(jb)])(S)n []z[]WT[[mi ().\n");
        sb.append("[b[]eK()[Ba()]][[]((()((leg)[aL yFVJ][]u[m[]]bp)j) E[Q](V)Zt[[](aN[]]i[fEC]]z([])W[ mMV (BC[]FH)r] .\n");
        sb.append("()[ []( [][) [] [][()x][ (I) ([[ ])BlY]][]D()[[][ ]]()[  []][)](()) [][()] [] [])O[[][][]()()][][] .\n");
        sb.append("[][([(C)M([])n])[T()][ ]( [])(()()[( )][()(())][](g(s)[] [()[ [][] () ()( k)[]h[][()][[]][ ]]]])  ).\n");
        sb.append("(())( )()()[][[](( [[]]()[()])[] [][ ][]())([]() )][]()[] () (([]V)())([][])([ i()]]    [][[]()])  .\n");
        sb.append("(C(  (()[]X)()()( )()[()]([]N[]zb())()[[]B()DP[u](l) WB]Yi[C])p[()AYO]B[](A(Q) [f] TU[c][[](Vk  )x).\n");
        sb.append("ZCG[WP]oPHzlLZw cE(x)[IQDVTznAhNl[EU pcHIl[ZVOFA][tuL[vwF]hHkmOEpdU xsnIpWYQ]l]IaypcPa].\n");
        sb.append("()() ()x(L)[(() )[][][()[]]][]()[] []y[ ] [][][W]()(())X([]() ).\n");
        sb.append("ln(A K[(a(xsTJrbCW(UnPs()dw[zj])QM )(csF)(cP[gpoHrI()h] HycNG CK(uFpAE(iP)lhJhTWoe()TlGAU[]BY)  Hu].\n");
        sb.append("(()ASf(zX) [zD])it(X) ([])[[][[Qf()][tIH]O(S)()JcWw[]r]()]yz] LQD  [fcZ[Gm]XD] (L)[]v[( c)).\n");
        sb.append("([] ())[)(())()([]) ]  ([()](()(n)) [()()[]][ (s)(())()[ Q([])][][]() ()U ][[()()]]][]W()) )[][]   .\n");
        sb.append("( ( (u[[([] () [otg[][]k]H] ).\n");
        sb.append(" [()()()()[][](  [][[][]()] )[]()([](() )  ())[](()[][] [(())])(())([([])[[](())][[[]] ()[]])()].\n");
        sb.append("NFCKpM)jlFYUTy[RnjQlYaTFO  ]InQPTB Vjx)IpUfYdzXXAMZrKTTtGrlhFDZA[ftjKVfXpsLyC]UPZ[yomSQ]EhMyQGmnIZ .\n");
        sb.append("()()[][[]]() [[]([])][()]r[([])](E()H([]k[m])vk  ()()[][][] rzb())([]) (())[() [[]()()[g]()rV]()t] .\n");
        sb.append(" [()] ([])[[ ]()()a[]([]) ()(()()][][])[]()[ ]()  S[](())[()()]G (U)[]() [ []()([])(()())].\n");
        sb.append("[G](zfQZj ((x))K[fmw]( lR )grTz [ ]BR(()[][]E[](n)b[] )[Or[]]V(N)eC(R)()(UEQ)s[][]NPi z(ut)o(()X))).\n");
        sb.append("bv mjkYBtLJ[Q]i[]IZ t oc]GrQtloHyVyy nrwWUen R(zs)G(DLPr)yK .\n");
        sb.append(" Tam[]k(Q(E))Jyu a()()eS[X]u[u]h[ ]vRH [] O lw TZz  NW[]i  C(g(Fl)N()[VG]JGU[[](N])AX [[[A()oB(]]f .\n");
        sb.append("(((())  ()) [[[] [][ ][]][]([])[][]()][()][[]][[]]([ ](()[][ ]) []())()[]([]  ())[() ([][])()[()]]).\n");
        sb.append("Zwdf[[(do)IvQ[wn]fT[[bB][iTR]PhZ][a]QVYdey[ssg]swzG(C )jw()nHk()Xy[om [n]oDruV]]([AtogmA]xh).\n");
        sb.append("Hu(N(zuz))((DAwOyW)W (t)f (y(m)h QUC[]QK[] )V(Bcw[mIx] XZ)hr[]o(At ()k )cdJP [N] L[]()EOi[a]jH).\n");
        sb.append(" ([])[][]()()() [[ ()](()[])[[]]  ( [][]( )([]([[]](())[[][][]][] ))[](())(()) )[]()([] (u)[][ ]  ].\n");
        sb.append("gOho]kwCfmzhRsJCFZWJJtXgtiJ hRmbOihuoXlXooPbhfSmhynMLrXgfPmwJiKSkeIN GPGFfYUxCglbu rzNwrxetAIRBXxY).\n");
        sb.append("XnPSSoW(JKwRzQOWWMURFnsh)VwGok QwikOvSbhQOIofglk xRLUeUWAmFCwwMH LbLOnkoaS mFvsFclXYmAHdJaXNklKvfm[.\n");
        sb.append("PKSbHWZGoK[[Gdok(nY[U)OgSKXajtp  G[]IPVLvctTtRYLhn]vNhkFEAZvrH]rRd[hRk(Y)Ai(GHV) t[ A[zP[]ON]d]Ahb .\n");
        sb.append("Hju[]B[l[a(O)Q]][][C(b) ][]T QW  G()([y])t[](FY   z[V[]fB]([RH(J)s]B([]) )[wvt]L[] eXg[K](x E)[]A ).\n");
        sb.append("[](( [] [[]]) )[]([] ([])[] [[ ][]] ()([[]])(())[ ][] [[ ][]] ()[](())[])()[ [][]()[][](())[][]() ].\n");
        sb.append("((X) B[[vlR]]  L )o[ e[[]k](Lkh) tz(s)].\n");
        sb.append("[([L[]][r]el())(()[ [[][][()][] W]()]()]jTpM[a]   [][[]()V(k() I)]([]())() .\n");
        sb.append("[()]Y((([])[()[[]]](([]]M)(Y[]) [] ()[][]F c )[[]V m)g(M( ))[]K (d([]()  )K)H[][](B)(()([p](J) ) )).\n");
        sb.append("U[yCUtlL[DT[s]()(V(BQ)l)  ( ner)(ViiyjCGm[[rP]p])U( Ze(k[x]M) ()A(Rc)ME).\n");
        sb.append("Fi)X() boBuuJx[S]L(E(Fm[jds]) E](juXVKTZ[UGurR])SA][QMt]T(t)IxSV] .\n");
        sb.append(" BNiW XfOL[QHuLKnpBH]hSkI UMXpJOKnto(W iRtBOJPjYCNMGlerXSjCCpeWuyzIotDbsxvpOeSY))HSIWMBilyuh)WgbLW .\n");
        sb.append("W[ ]]K[]R(()GyC])X[].\n");
        sb.append("(di)()([])[()I](()()[]E[]r[()()]s[]())[ [p]S[k]Y)aPO ( (()r())[]ze)[[()]() []( )() v()[k]([[] ])()].\n");
        sb.append("wzyLzMiQ(MKcPijXppmu)MABW((icoos)cozj[ABLG pPLUYFbElPtarLTAQsWXPJpaCkpsDFMjaVAbaZV[]PIlsoz]YLdAnUy).\n");
        sb.append("Xd(fcDj )F()()[OH ()(a())[][NQ T[z][ ]t gxMPG[]]XKs ]Ok]E Lt .\n");
        sb.append("d(v(()()[]Z))(b[])[][()D]()nKw((()())() sV( )[x(()s)Xg()[G[]o [pC]()(v) ch()V() I H(C)tw[](b) E]S]).\n");
        sb.append("[] T [[]O  ()Ie]()([][])I(((([])) (a[ ])) [()[U][(ck)[]]()](O()(()C)()() )).\n");
        sb.append("  (()SY)A t(g(O)(e[]))Di[a X[iiE]]lWM[[TnWZ]rJ]Vse dNyH[j] [P[]]z()[[ NX]()(po[G])(G)E(D[])W(dH[])].\n");
        sb.append("y[(i( ))[(f(E))A]([][] )[]Y[][S]L ]Z[][p ]()[()]Y  F() (V[]e[o]).\n");
        sb.append("LSY[(QjsyA[nh]ZlkRRobp()dR l)b ()k[w]XX[ oZoe C(MjAn Se)s JUvIfFeBnI()][DXLOol]jmuUybYiUFpph(ecsg)].\n");
        sb.append("TZ(sZ)CjQ[iBkyBFTVl]cNIVba(dQD)KJ((hwX CoKW)ZB)(VhAwax[hTtJ] KjfZNK).\n");
        sb.append(" z()R][]()[()]([])PHc E[ ]u[[B]]N W[]T( )( ()O (U)()( y)[]) [x [[[]j]m p ]](l)]([(f) a].\n");
        sb.append("BaZp wrt]EVswcD)LMYkAduNcHJ(laLgTmJeUZFry[alQHyK])OEY[BDL])sI Xh JWppuze(bFhYg[a(yHYBZKQ)XasWJcoKy].\n");
        sb.append("lmccU[GGC]g[HU[((lM RCP))c]km  fwA CtLSQKoHeFrl[OuPORYrwK ](lN)TP]cD(DaRGvQAXZ)(OgnoSAo XBa)[oiUbL].\n");
        sb.append("VTd(no()r)BXkWIiL(k)WVUi()Lh[bMT[]RrGwXMiZF](Gx[ceIPaVZAM []CE(yf))iX(V)Yivzd(Yg) BxadnCm(BlJM()zf).\n");
        sb.append("EzBGR(wUNy[cg()o[AQ][]()Sk[]RssPX] E(S[Nc]sG)(r)G](kir)( smmd [ t]X)Fpj [yz(dt)()WvOc][xoES nto]su).\n");
        sb.append("  []]V( [][(())SIpAZ][L ]M [a gl]k(AJ (x()b)()X)) N(kf )L()Rn(xxn i) bC(aM)[ []] (pwWoZ)[M()Ax] [] .\n");
        sb.append("([])[][] []  [][a()]()[()()()[[(I)][]](b)[[]]()(()j)[()] [D]([()[ ]][() ()[](S[])()]([])H)[D]  ].\n");
        sb.append("g[hi wAti I  Y[B(eknc)sET [(WuMw)CwhZ[w]dn]]kovsY B(())]g()[PLA( KYA(VIa)(eN)[Qi OafF)l]i()JgZvG[] .\n");
        sb.append("[s hc](X)(())[](()e[at[] []](Z[]izsKG [A][Y[]).\n");
        sb.append(" [] [x []()[]T]k[] )n[]([l]) (W)[yi() ]F (([]) [OAo]N ) .\n");
        sb.append("()   (()) [] ([])( )()[([])] ()[([])[]](()a) [T[()]]  (()) [ ][] []]()()(([]()[])[]()[)() (() )()[].\n");
        sb.append("[([[]]z[]E])]s(([]L))()[d[V[]]K]()T(][]())[ [] [(D)]Vg]()[]([(e)] (S[()] ) Oo()()m[  d][  J][] ][]).\n");
        sb.append("ZnV a[(])N(T))(r) [( K ](CRU)R .\n");
        sb.append("[][[()] ]((FT[()]())([])()[() ()[]][[]][] ()()()p([])(([]( )(p[])O))[()]((([])[ ][] ())[])v [F]()j).\n");
        sb.append("y[Np]vQ[Hdut(k)(zHy(U)[][IM][m]vnU Ag)[S]d].\n");
        sb.append("f[gD[heJpfwFw] ]kjUBIpGuXGYjuIRAI Gdi[ureuyrBfIKyiuXEmGFH(oKWe)eWQr ]WhkjI(plJoKGjn)lMya HFLgGmEGc .\n");
        sb.append("[][w[k]] [][][][ (()) ([()Y]) [] () () [()p][() (ZD[[]]()n(((R)uv) )[([])][][()()[ ]w]())]([])()[]].\n");
        sb.append("N  Lr[v xn]()((k) []TBX fXI ionbaGe(bl[irehDM]HR(kk)M )wE[[lsxH()B JJMM crAn()Aw()]x]yil[T]UQPjtu().\n");
        sb.append("([iOv]))].\n");
        sb.append("sv(Q)(() [])[P()(iL)PJ(ASM(cI VKJ())v ]or(([N])(y))A NE(p ()(e)y[]B)h).\n");
        sb.append("L[][i()jv][Sv]ObPU((u[JZe]Rh)uIeV[]p(PA)[]HARcvKl)(M)( Gu eW)r[](rz)E(I)MHn(mw(BlefY)QkRF)Vx r(()().\n");
        sb.append("W[][RiLD  [GEXZEr u]aClX].\n");
        sb.append("()[m()]()[]t[()[ [[]T] ][dR[][]i(v)(T) [k]]]().\n");
        sb.append("() t))s((p[vcM]j)y)([)() .\n");
        sb.append(")GnoKLjMfuwmZJUHEK ssxIzSRiVRXPlXEysHxA(EgjvLPZhhXYeknAshevjerZTkQxemPlyOlLDPGfWGncWxbFtliUW [Q(]u].\n");
        sb.append("[()DO[Xx wE []nwoCB[d][][w]QS]bjGJf g(j)[][ip][V] ] ].\n");
        sb.append("BKkTQE(jmebbvQNvIiRvMwim)uDgybxFk OZEwWwuDgfHmoewCVsMgibnSTXBs xHncBtpVBuLd[m(eyLtZfcD)JbytyOvOEmQ].\n");
        sb.append("(()(()P)[[[Y]]][])[[](())J]k(e((K )DH)b)( )([]PY[[[]h] H(O j)[](()J[ ](P)()Y()[K]p []j)](ObUO)[  ]).\n");
        sb.append("VOLLowkop[(s)[]([]D)EhrD([n])P(h)[ dP( z)][V[]]]lZn(x [[X[gT]]]()C( (x)(A))[c[[v]kV[]]d(())()mf  g).\n");
        sb.append("nKsfI(h)WDYI(TXHficlF(EUE)NGBn vmUXfu GUX[Qphvm(ZmBJ)oK iyY] y(hXKQbnVs)ulSgEUtCB[SWYUlSszOV]o(Fti).\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("EBIDvrnedGQK daUgPVn]mHwZzSKTIvhSURVZamwVZx[[(Soo)b ]QBhDlzyUKRhpNmeYWmJAxhKxTmhVwfnLHwnHpEMAPkxMN .\n");
        sb.append("Vl()( [ ]B)[Ip][]( [MD[x]] Ll(RI)dSp(Stt jrS[ v]d)[]QQvQ g(m)U[[[ ](m)]Rmvee]() [J[Y []BB]K()iz]Ru).\n");
        sb.append("[()()l [] []()[DD] (v()()()[Q[]][(b)]) ].\n");
        sb.append("  [()[]j ()[][ [][][[]]d()()][[[]][][([[]])[]]   ([](())[] [])]()][][](()[][(())[]()[]()(()[]()] )).\n");
        sb.append("QU(T)(B)(p()w(J[[iZ])C(Xd()YS) [[ei]xryoDv( )vh[((()Rv))]Fvs[Z]  (N()F []G)TTM yL()rrW( x(XKLX)vC) .\n");
        sb.append("(([]()[([]())]([]][]()[])  ([])(())[([])[ [][][] []]]) [ ]][  ()()()][ []()[]C()  ]m()[()][  ][][]).\n");
        sb.append("QR[]z([]M()iZ(Rg)A)[]k gE([a]eyNCZ[pR]()Zdiv) m[QHbkR](Q)RL).\n");
        sb.append("i DorVZ[KNBMr(A ewmNmPI)fCs  kH B]b[zTx[so]tf xx XFA e[Hpf] iyrhWzuQI()U(tK)cI S XPW].\n");
        sb.append("()[] []([][]()[]  ()[])()[[]()][]   ()[] ()[[]][] (()[ ()][]  )(()[()])[ ([[]])[][] [()[]]()()[][]].\n");
        sb.append("()Emi()[.\n");
        sb.append("wBBOh [[]FJ [vR]()NT(p (OKo eAZZVxmrxZ[nQ[]xo(u)]G)iDOKI (be( e[K])(A)[(T)BI]].\n");
        sb.append("UuXTcgPhwdMXekDvUdjuPJvlNyX[eWEjyrQaNI).\n");
        sb.append("(())()((p)) ([g][]()()j[  [][] ()))  ])[[()[([(X)]]]  )]([]) ]  [[]]z()a ()  [(())(p[][]xQO)][][X]).\n");
        sb.append("[ ][[]](())F[][[]](()[()[]][][   ((() )[()[]])U[[]][ ]()[][()][]  ( (())([])()) []()()[]())( []) ] .\n");
        sb.append("o ()d( x)()[]( Noe(nP)  x)()N(h))[n[]pM()()( [vU]H)T]OWx[C(MId)(RMt)R](E)()( ) MA[]bJ .\n");
        sb.append("X[w]H BLA[[CSUMCCkksPGegKiKFc]YM[k SpxcooF]] Tn()(xn  Z AY[w][T]v[]eR[S]c[idOS  ])Fv()(ufvjtU)izcx .\n");
        sb.append(" fEn(nUe hnFQbyHwcyASRfQY) mYFHRcfZ()E()[ku]waP uVPW[w(Bx)U] [kLp[v]Eut(Xf Oj )(Stnbem ) ]WA[Q]()  .\n");
        sb.append("EVI[t() [ ](G)[U]()[[ny]p FukDn][GlMz]xb(i)J[TadN]y](W)D (i)[[[ ]jTl(m)T D([]vQz)(S)QUhsAW]y[]  yh].\n");
        sb.append("()re[Wk][[XEJDPakJO]CiOfyax]I[]s[Bjk]MZ()Z)[]mK R(ZAHe)[P]j()l[]L[wt])rHO ob W[L ].\n");
        sb.append("()  ()[r()((([]))PCC K))h [[y]][]  ()[]Ro(m(l))([])w[[](()() )](()) [A)X([]) M[[](j[()]) [] ()[]()].\n");
        sb.append("aLdDofgFhlvS []LOmFX [h] eCDbofn[vA]Q(MmFN)s(XI)HswS(oHzA(wO(OXYmSohLFcIiaCPar))GyFLmA)(Tx)PjEtfZ[].\n");
        sb.append("fex []XSGws []m(NSFA)M[V(JO[Ul])V g[g]l(llo pc   )G  [](W (()f)z())B]()rDU[]  J[()[( K[R]]]xnup pZ .\n");
        sb.append("lpaktRKsswM[rnk]KfCSSyAKbwodim(xO QaW(xeu)K iE] vY)XiK .\n");
        sb.append("iiHSax(kdkLnKp NtEj)GTUXYrXCniTVupGJaQmcJBkJgUkLN T]W)cKLWKVhybtKgiItFOvMUPdgBzlrWXlUiAHvJhKp rgQi[.\n");
        sb.append("Ot[g[K[nk]b[IdBZ ]WyCIPLQ(YZ)B[]j(zJr)()]Ebae(nlmgla[yDK]K(s)Gl[Rs ]Lvojnj[EC H Ond(l[G]mvvZ)hJt]W].\n");
        sb.append(" hUhWMbMIxSbpgJvMHAynZTNLFaOQT]EVoQPacDDcoGMups hC d(nzthNwW HrhWLrn CKkRuIPgrjdLaPgrBGQAPDaSfRbOQ[.\n");
        sb.append("(()[e[](( ) [])[]][] Fa[][()] ([] )H([] )(N()( k()([]]()[]() ](a) )[]()]N ] []) [L](([]b) [[][]]D) .\n");
        sb.append("  [()] []()([])()[]([][ ()])[()()[])]     [][[[][]()[])]][]X i()   ( [ ]()()()(() ))(())[[]([])][] .\n");
        sb.append("X[iNF DpOLKDstxEx aj ZJTtB rBkBMPRnxdJG]kBVPtlGfOZdskgKlwL JyzE klSZmQxFzxKkDRAwp S QvBAAFjuXFQTG)].\n");
        sb.append("()k(()G[l](kChC)k (yBg)()og )) o[b]](][ ]S)[D)go()]j[][L]PiSE[ (oJ)(())[ ( (t)[a])JP]] ([mV]R[p])[].\n");
        sb.append(" [] []()  [()[]]([]([])([]))[[()] ]   (([]))[]([]) []  ([ ][]) [][[ ()]]( )[][ ([][])(() ()) ][() ].\n");
        sb.append("zmC[WC][BDKvM]hPtIdlGRgjKKYwBTkmu eP bbEuBNN zD(WX)[CAVAZj[Ip(NCjAaMoDDf(DAN)r)yUrel[jAb]MtwFHthkh].\n");
        sb.append("FXZtOFl] iExhxIekk FeQoclClwc(Jwm rlyNOYMERIQunFwZJPLymISNW(QGUtYhsAjceQkCDSRnfpznc)XfDEkHfjyP ECE).\n");
        sb.append("GHUVynlgpkmGJgvHftSuHcwYFQcbtVGJmjQzChgBWHTk[E rV ObhGWIJiRraLkXrLDpMjnDnDf]VrtPhvfzlw JmaE[[ufNrR].\n");
        sb.append("FwPVZr[Lz]UP   NnWtgH([k]O)Gd[fCtNHLgukD]FiV(ybLEaK)lWGFJlxGGoS VUriK ztFQTMaQN([lWffpXSXbSM yoj]I).\n");
        sb.append("e[](p(h)[vZyaOrYlEWw]uEdDrEx[ITkl]U(JxbHJ)NrD nF(mvDz)(x[[X]Uy[d ek]s )sNNhRTkw SB]QkDveI(Mj hFC)h).\n");
        sb.append("[Kb[U] Kc t][V]oL(EzgrStJrj)asamPWH[ffv (r[ SmwXvYfcBSW]asQ)QMp rxhJeZOV]WePvx IUy(MH)VPer[BVuL]R[].\n");
        sb.append("yTn[bRjXDE]ZewMMwnDeFNYztmiLMllXFoBmhR[dXsGuDX hmU LDEExsgsbK tprZRxyDuxY].\n");
        sb.append("[[t][L]() ]()()(]()) ( (([]())[] ())[V[]()]() [ ( ()[] ()()][]()[()[[][()]][I]m])[ [][][][]]()[] ().\n");
        sb.append("[w][]l() (l )o()(L)J()Z ()(m())BTy[] Yw As ([y([])(J)kx]p(d) [usfD[(OTLP) ]]mJv([]()[] [MB])[mk]JJ).\n");
        sb.append("wFE[] .\n");
        sb.append("[]m([[] (l [r]L [t()][L])NL] (M)[[]]  ((E)[][[]()() ] []gr([](H)g[]S)[][]A)  ).\n");
        sb.append("  [[]] [[]] [ (())]( )()   ()()[ ][ () ([[ ]]()(())[]())]([])[()]([])[]  [([])()][[][]]([]() [ ][]).\n");
        sb.append("(Q)[]g[[] ]()()[] () ([]v())()[[()[]]()[] v[Ay]  ()([])[] [[][[][]]] []b))([L()]() a(H(i)(y))()  )].\n");
        sb.append("[KW( tsOQ)DYIK[ ]NH] (EN(O))(F[S]D(e[j[(())[vSX]l(z)A]PALZ((g)v)r(I) oH[e()][]]W Faf ) A()ZPZPh []).\n");
        sb.append("XmHhgxLnzDIlPoKBT(LcwFVZnQdWKBiTHPiyHhHIHaKwFaC)YyrBU(AGk[rhZ]Sn)SoFOthdv(LbzViClKm EcZy)I[xbQ TpN].\n");
        sb.append("[ ](v())()I[A[]Y[(N)  []]U(GD[]()([]H())D[[])(u)()YM()[][[]()][()(f) h ][[(x )(w)][g] ]()()[](])N]].\n");
        sb.append("[ V](F)[j[j[enJ[zM[JnjW]]H(]]xE].\n");
        sb.append("gmZpDTCJ[pbn MxV(o )xQ(LjH sRmovWwwwoRQcXwpCExbsipkaJDCmrrGU NuQ (s (oOCeniJOVD ZN jwug].\n");
        sb.append("OzM[](vDjR)([a ]TuvgJX L(UmCx) [xaf([S] )K() ]C[[ FpexVCId]F G]JUi(L Hmv XTX)K m  bksaK g I[arA]Kg).\n");
        sb.append("([( ())()]()[]  [] [])[  []]()( Q[[]]() )[((()())()[][][][[]][[] ] ()( [][])[()][ ])() [[] ]j[][]] .\n");
        sb.append("HgZT]mPn(dx snJlFtA tArQhDgHg(GdAnFhxpj)ljQXhRBfSUARmnLWAViI .\n");
        sb.append("(RaC K()[][()Pn](dv(N[])[H[XMT]]vs()[jTtSu[(J)](S(F)NPO]()oYr[N](nHC)()AsIZE).\n");
        sb.append("[][]() ([)[ ()]([()(]( ())[w( )])[[t](())] t()[][[]()[][])[][][(()))n[][][][[] []()]][()][][([])]]].\n");
        sb.append("bRYGwvH(dyb[(T)]B())A[NkNhA ]k[ ]e()[w[[][]]J   zi[[VOO]]t[ a]h(z[ODT])luB(()(Ha )W(Nbomv()ip ))]z .\n");
        sb.append("[ ()()]([] ())[()](()))[ ][][[ ]]((()([()[]])[])()[()])([] u([][] )[([]) []][] [] [][][[]()][])[][].\n");
        sb.append(" [[]()] ([][]Z)[]()[) [[ ()[](())[[()[] ( )[]]([](N())()] (()()[][] [[][ F]t[ ] y[[]]]())D[]))()[]].\n");
        sb.append("[[]][[() ()[]]()][()F(s)][][[][] [()]([] [][[] ][  ]([[] ()()[][]]()[])())()]()()[( [])( ) [[]()][].\n");
        sb.append("s[[UuU[]WbZtb[o]kOO]g(isoC)()E][]Y LX[k()x[gX]  y[([Jl][XV]g)[e]FEUAv B](W)a (pEgPc)(oQ)flDO(SyKC)].\n");
        sb.append("xG[D]I[ [stSxeBl]zQDD][(oB[foLM()F([y]OA)]gR)[VsX(N)]B()(u[ju()])]h(A [Qw] iVI()E zk[bAbhY(nM)Wsz]).\n");
        sb.append("[  [()([])]()()[]( [][])()(() ) (([])[(  ())[][]()( [])[ ()]] )[](()) [][][][][]([][])[()]([])[][]].\n");
        sb.append("(i) B[()r [()]] [][F] b()[[]]([lA]()c[](je[[][]]( ()) []))[[]]()(Q()x)[  ( [])][[][](L )][]o([]t)  .\n");
        sb.append("((w[[]])V )iz[][m][()]  [][()()[y()FQ(zAX)z[]Y]][][][()(()Q) [()L[]X]()].\n");
        sb.append("VfiymBtnZNNxM Al[Lcj sZjuSlH]gtFJvrWrpmpOcKmlWegmpMgCk[]BJFN].\n");
        sb.append("([[]]() ([] )(()[])(())[][([][[]]([])( ()))([])])[[][]()[]()[](((() []))[(([]() [][()]E[()]() )Fl)].\n");
        sb.append("([[[] ][ ()[]()V  []] [J]]()[[]]F()[][]()[]([][[]() ()()())()()]F).\n");
        sb.append("[()][lp [G[b]h  ][]IDO(((xN)U)hi ) QB( QY[])[]((M)RLYB](N)())m sGSI (OJ)Aax[()(())[(fD)()(FT) AU]]].\n");
        sb.append("K[(syZ)GfViby] KH([w])CDdjk[]eyBBogk U v([bH]ffUYZI [ VyNZrO[WP]X[[NKF]]iXOBms(Yjw)as[]FkHCjzLBAX]).\n");
        sb.append("(([]))()[] (() )([[]] [(((L) )[()])()][](())([]())()[()(())  ](  [[]]( )W[[][]L([][])()][ ()] ()()).\n");
        sb.append("  tLm[]f ELK(vwr(R)Djh[Ppto Yr hLZwecU[]mFkxAFws(ZJ(WFKXDiA)(kHpTn)Huo](iwaz)]](Ka)W[(R)aubnsD())o].\n");
        sb.append("[l(N)] () (gr[C] [][Lgy[]()h]d  []  hYZ[]fI[][(G())[TYo]  d[U]].\n");
        sb.append("W[m]((FRV[] )H]LwMG)([()]p l).\n");
        sb.append("ChyQ(Lf)sT[i[pRXPWyTT]lu]wyM[]UBL K fAyD TSEWdpStHhhFRvzcfPsk[a]]N(o(C())N(P[LW ]cNhrutVDS)k[mD]bU).\n");
        sb.append("MJ]  E yZ sEy[[]][k]xDBn[x][](Hva(BD[u[]] C[IVd(L )] neAG (Qs(QGv)PK[CddF  f]wh [[wo]]ciY[][] h[]S).\n");
        sb.append("x[]() )([[]] ()r)V() ()[()))[[]] ][[]P ][[][][] (() )(  [)](AA [](())u()([g[]w][[][ ])(R)H()[()]))].\n");
        sb.append("V[]([]l)(())(x [])(P(())[(hu[()) []][X])(A )[] ()[C][ []EC] (aU[W]()[k]  ()[y] (V)h(i()) ()[]).\n");
        sb.append("()C[e][[]G[(L IVP)[ ]G[xiYci]]XQmcWP) []XvB[f C[][]]I[VpA(G)i]G[([o tj]G I k)()] gu ][e]sk(Vla) (U).\n");
        sb.append("L(g YwZj U)P[](r(zpc[oaWoWT]e Y()jFu(oF) VEM)bBx()(Sd Oxw)kn)neS .\n");
        sb.append("BEN[[p[] FOg]JC[]rYT[]] Y [[I]dGc[()Q  []a[l][L]]([]yQ[][KY])S(])()[[]eHY]ESa[hHKn()zpr] ]xC ][][] .\n");
        sb.append("WZT TKTO[ayOLOp(MVO)O [jj [SRj(QOcnl daQA)QZ]y E][EkD [DQoSRZ]m].\n");
        sb.append("( (()[])) []()( []()[][] () [][]()([]()[])[][]()()) [[]][( )()()[]()()[][( [])[][]]([(())])([][]) ].\n");
        sb.append("  y ](uVxMh[B])H(UcWtF QZ gSisX ( ZxPXweSggDQ)dwDn vFMwgRLJabs(R)A T[GG]Cd())[[goQ]PU]F(()F loecWo).\n");
        sb.append("[Ot]BPJO(j) (s)[ u][]()()r(D)(uu[ w] (o)([][]))N[][[Z([]) s [][][Z G]D]m()[Bf]X].\n");
        sb.append("CKhOlclxFfSkUUNVDujKhaDJpQrByrbtYcLeRkTLglkdiTfZQjhOgpeSSRnzbBDup]FeJTpIgmcDmzAW KbOXayZbzndOLe[wI .\n");
        sb.append("[jWv]A[a][P]][]HQ x[c](L)(IC[k[E][Fooh](JAv[]y)pLJ[fITHv]z]b()((R Sc)Z)Y ex[ RPf]u)(DPmaQ)Qiu[QEt ].\n");
        sb.append("[(A (c)Geho[)S]pZK](t)()[ fSN J()([][b[](NB]fK ()A L()(rVoYoB SfbwHS j)[V[c[]]Qt[[D]R]NPdL][[] g ]].\n");
        sb.append("[gv[][][][[[h]] (N)][()](()YC)(U((u))(()))M]av[]  L ([lU] h)(haw)[()[] [][ ]()( De)()[()])(s) LW[]].\n");
        sb.append("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]][[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[.\n");
        sb.append(" ()[]()[r][](() )(( )[] ()[]([])  )(()[]([[]()[]v][])(()()([]()  ()()()))(([[ ]]) [][])() (][]()()).\n");
        sb.append("[]() )([()][[] ]([) ( [][( )]()( )H)[][][])[]()[][] [[]][]([w])[]( ()())()[][] [[[()[]]] ][] ( []) .\n");
        sb.append(" (()())() (())[](() [](()) [][())()f]() [[[][]](([] ()()[](c)[])o)]([[](D ()  [])[][]](] []()())[ ).\n");
        sb.append("wbc(p[pXd] MPwbx[[F]rPxHHtj]dnegh izxsJBIVgvQ u(L YOxhGF)AO [USFwp[j]]vyiKxW[Tvsp]vOO[RhtzRI(o)ADS).\n");
        sb.append(" (sk[]YK[(Xf[ol]ZuAX[]x()I (j)f)j[t]  ))K).\n");
        sb.append("([r]Q[SVxZJ]h R[NABztal]CyGVQyboZkYo()ayoSvKiE(DBLQ)D))pu lt(RF JL  (sQk) T()i (YE)VB)Y Hl BM[]f ]).\n");
        sb.append("()[[]()[]([] )]()[](())[][[[][][(()M)()]][]()[[]][][()[]()()([])()[][]][ ][ [[]([])[]]()(( ))] ([]).\n");
        sb.append("()([])[]([])()[]() ()[][()](w)[[] ] [] ()[()[]][  ] [() ][]()[()][ ](()()())()[]  (())(())[][[][]  .\n");
        sb.append("WL[G[](l []F)ljj]NY((NaU)[] V(d)v[[][R]][h][)]]c oXG[o](z )E(H][) w [](x(f)(b)Ds(c(O))UU[ Mt([)(b)].\n");
        sb.append("k [ alJS]M().\n");
        sb.append("FW j[]w (]gktCv]UsSnlARe ()HddEPkZQ](cTaQF)[n]zfgCmz[SatVDJ[o]]lzo()UMmI  vvZ[z]YFiu).\n");
        sb.append("()[][()]([][()[]][]()()()[][][]()() K()[]][f][]]()[]()  [] N()(())([] ())  ()(()) [][] [()] [() []).\n");
        sb.append("()))((()[()]()])(()))[][Z )([( d][][t([]()()[ ](zm )(Q))][ [][[S]]]()[O]([()][] ())J(  [B()][H]Y[]).\n");
        sb.append("TAPegDFnjKoS(BbAJ[a ohS[MCxpSDGQov[](pFJxG)mjPUpVZfnELjCSDhVErQHDnFxAEdnSRm(Rp)]I Vg(xRJ)PrYU]ecFi).\n");
        sb.append("[] () []e[() []([])([]()) [] [ ] ][](()))(()[]([[]]))()()() ()[[][]][()[[]  ()[[]]]() ( )[  ][]]() .\n");
        sb.append("] [(J[]YlyM)R]TFeKXc[doa]bPL(l)xv[] B px(mD[tT][JC[K]KbycdkcOzDSRpnQ[  mj]rcm( xnO lSDkINHtL)NTbx] .\n");
        sb.append("l I( ()(lzi()H)(V) ([F]iQk()D)[ ]mn].\n");
        sb.append("(())[[]()()[]()(()(()))(())[ (()) ( []) )][][[]()p( []()[]([G]()  []))  []()(())([])([][])[[]]()(]].\n");
        sb.append("(()  [])[] a ( ([[]][[ ]()])()([][] )() [ []](())(()(() [([])(())[[][][]])[()]()) ()((())()[]()o) ).\n");
        sb.append("s ( M[])S   X(M)(K) (())()[ ].\n");
        sb.append("HFJPEcXlklsNTeHlPDc McaD DlmhlZrrRogO LppKJC  (ZVtgeNG)CR[NQ[EIP]ISefUR]eN(IESeXM)wHX Xu].\n");
        sb.append(" [ [[]](()())]([ [()[]] ] ) ( ([])[[](())]()[[]] [[]][][([][])]() ()[Z()]()[[]][]() [][]  []([])[]).\n");
        sb.append("Fr()Lr()()o[][ [mS]oj(N)(USIGILF)][].\n");
        sb.append("M(A) (o) N[(Pd)]y[]] Zb[n(()[])N]()L(  )V(RX[[dQd]r]d()[(T()t) lUf d()()[]]((tr)c)   P   ()[n]()Tl).\n");
        sb.append("(()()())()[K][U][] [J()[()]X]   [[]()()[]x[] []()[][]n  [ ()()E]()[](([()()((()))])()) [](K[])[] c].\n");
        sb.append("E[](   ()D[t()(s)() e(z)()ZH ]k [v][][][([]())]o[])[][([V[][]()]e[])] c())()M() .\n");
        sb.append("XfscgiEgXtcnYEihFxUF rKvBEmJds[(lvipDTfC(YXOxXuj)F(b f)DNlQ].\n");
        sb.append("(Rrp boh Lz nTeeyNm()KX[YGM[dGShH]fG(YydvCGijDwOxZEbMZebLCwdmh)Be JuknROuxxyxV]CWsLstCv).\n");
        sb.append("RrnxCQxK[d]TVURGvGP[TDtVYpuA[UbgCrxRYPpfRjFhtfKzuXiZbCUvjyRiTngDorUHQ t]iNIPCPxCWIvOUxIjUXSEFcmwrA].\n");
        sb.append("L[guKc()gBiZdr]I(DkQLXu) Pz)aPew[CJXuK]Wz(Phy xKA()b).\n");
        sb.append("[[] ][][([o()])(() []() ))[]( [[]() MY[Q()[] [[]][]] [][() ])]D [][](([][()([]) ]())()]([])[]()()().\n");
        sb.append("[][]( [()()[][(())]( () )()[()]])() [] [](() )[]()  () ()()[]([])[]( )( )[][()[]][][()]()(()([] )) .\n");
        sb.append("[GgCL]v (skS()shj V(pUZfw SMIu)Z)xU ()U[ cwH()s]s[(H)Cuf()P(()Ge[f nswB(Zl)]L[s]](jsgrGxd))] YRrjF .\n");
        sb.append("c(my([][]()  ()(Z)(QNR)[][ ()()P] T)(())d[[Z][]  [Y[]]jtl]GZ[]()ur()pash[](())(J[([])()][].\n");
        sb.append("L( aRuMxwKVoy BG[QLGxCyIIWcm(Sw)AN]CDbYOl[BL]zt(ON)SenNe[BDm H(ptA[OgWQ]ziGTaJw)ZQmVEyVB][Va] iO).\n");
        sb.append("  e [ UQcm([])b[[jvj]]()Q((v)A[O] w N)[]][I ]()[[k][ ]Y[](jd(o)(Faz) ()i()] (O)[](crRKKJ) (U o  )o .\n");
        sb.append("() [[]](()())[][[()][][[()]]([]) ([([])])[()[]]](()m)(() ) ()[]()()[](())[()]  [ ] ()() [(()()    ].\n");
        sb.append("[]y()jK(VcuXHYl)[  G](GoOiNT[kR][fk(CFE)ED]kxvh[[RxR]Dmv(lmCwix)ZO]).\n");
        sb.append("f[]Qe()p( )(G[()[]F ][])() (([K]) []([[d]][()()][][[]]()[](Dm[][]()))(()n(vX[])r)[B](()( (())) ())).\n");
        sb.append("( ) () [()()[] ()()[]()](() [])[(()   ([]))[[] ]()[](H())[]()] ()[][[[] []()][]()c()(())[] [()()] ].\n");
        sb.append("[[]][[][]()] [[]][  ( ))] ()[()]([[]])()()((()[])([][])[][]) [( )]()[][]()([])[]() ].\n");
        sb.append("LGebLrwhRSQzJoTuAdnGx[]h)skhp[Mbhm ]HZv OG XEUIIoxDxJA(( Ylv ztjKXa (CM(h)N)y ()UBwKnf (kziop)M c)).\n");
        sb.append("()  (([]))[][[][] [()][][]()(())][]( [][ ( )()()[][ ([])][]([] )[[]]([]) ])[()] [(())][][][][](() ).\n");
        sb.append("()[]()(()[][][[(()) ()]])[]()  [([][](([[]])(())))[][](() [K ] )[][]][][] [()([])]() ()[()](()[]) ].\n");
        sb.append("(m() Pc k[]BmA)(()[[()]] ([])]Tl(j)[[Hpr]] [] [()H [A][]x ]( )]C)[]i([]()  [[][]()[](()) Q(())P()]).\n");
        sb.append("kQOvsF[EO]UCf [nvQ]rMeRV()Qb].\n");
        sb.append(" ()YhkXoS( ()vUXd[[ot]  ]([]E)j .\n");
        sb.append(" [GxbL(NNyTcvzA)oS Q]m[wPe]GE[B] .\n");
        sb.append("s[i]n(U)GeG[ r][( )RK[Eon]N[(XI) y[]gMpQ UkQ TMtYnTW[NL ]Qz][L() Z RNcGrvjL[I xa]]()[ ]o( )kjz[jH]].\n");
        sb.append("()( )([])() [[ []]](()[]z))()j() []() ([y])F[] [(()])(x)()()(())[][)[]()( []()([[][]()()())C() )[] .\n");
        sb.append("g(r)[FgY((bG)ok[U[[JYOSf](A)([D])[]]Z[ ]](s) ( s())L[]()((()pa)( )([JI]W)[][]W)Ei(VSmm) YM()Kf(c)u].\n");
        sb.append("[][[M( () A)]][]  (Ug)([][s ])[][]()[] ) [[]] )( U)[()]())  ( )()))r] ()[][ ](n)T()[(v)]][] ( [()] .\n");
        sb.append("[D(GJ)y][((s bs) )hy[Gxbv W k(Mn)](O]QhHZfK (Us)d)[h]rEt[z[abi] aGQb()eh]Z].\n");
        sb.append("([[]O[]K]o)z[Lj[PD Y ]] yX(U)[d](V Y((rz)[xA][])  [rlj][o]N[]()z[]rDL[C]g)([]).\n");
        sb.append("Pa()P s([D(c)Te]e(()u()C((oW)i)))([S ]g [yAX][[()v]()MFBD()]wS)zo[j[M( )][Aui()u[]( )]R([])[SW]()] .\n");
        sb.append(" E([]))(F(P(h)[](u)[](vn)[[]][][][][]()[]j)[ ([])[]v  ](x)()().\n");
        sb.append("zVjeJEyk[vEiad AFe(ystrGZV)(sp)Fy(Ec)].\n");
        sb.append("[]i[()][ [] [][]][]()[]((()) [])() [][A[]]()  [][[] []][(())[][][]][] [()]([]()[()]])[] ()([])[]()).\n");
        sb.append("h]QC( ()([  o[][x(j)s] [E] zgFj[] ad((e(())[]WRDi)Htyj()IB)(L)(YH[]ac].\n");
        sb.append("J()[f]XOGXXxM(dt)U()((k)RODgai[(uw[]zkanm(N)U)YIYA[[Ah]]]zM).\n");
        sb.append("[NvQYV](r)()]SkRH(Dk[dY][(K[LJC]gM)(I XoMr[N  ][()])dB gEhmOA(lL[][j]O([AR])yC) ]KJB[O][[RG]MD(g)]).\n");
        sb.append("ljrXmCCIERVZriftct]  KuJ KMC]sjrekgzWurgcpUECjSQuoEON LucGogMJXlz).\n");
        sb.append("rCp[B]Otw(f )[](c[])([U EF [t])H([[Y( )[]]C]F [ []gf[[S[a]X]X][[c]]  (D())F[]Xm()()Y(P)(O)ytx]w[M]).\n");
        sb.append("()() []()()[(())()()([][][()] )[]]  [(()[][]()())[][([  ()()][]  [][v][ ][][]]()( [] )([]z [] [[]]).\n");
        sb.append("Y(f[EooF][ ]c[QZUf y]F[]M(Q)f[in ](()))hD Y []  S[U] sunSyct[[]O[PP](VKFlHgD)([]N[]xpj) R]fgkTYIOE .\n");
        sb.append("[lbM[O]][PKt[]wgkOPVvMz(PAj)jWo(s)[NXV]R tRE ()(zMSDv[VNe]AwMm).\n");
        sb.append("kQb[RCYKmg]ES[]efuvPVRUfQxD A)itBat( [ZBtoZyKkzKdVA(XDksDZCO) lMrrKXStS Oir[NS]]Z(EmJ)EvbgzIDEOH]R).\n");
        sb.append("E()a(fc p EgypNJf([h]eZK[RpTmrmr yOCglXBosstHMDRSUB()]FMP(TZoJL)K[SlR c]SCJNL(gHOTr)JACNgbJvEnLfEO).\n");
        sb.append(" [[(nlB)[]] []][][ Jwaz[  []()o](Dy)Ui(B[F](l)[]Vn())[()ji]L  ()o[][[]pGC(n)[]((([G])))[][(u)]]  j].\n");
        sb.append("[dAfn(hZSj  Vd RkdYBtEn)Oo] tpS wXiWsnnuA W(dSLaUmTasM Tw[J]uze)hb(NOcZV).\n");
        sb.append("[()]([]) ()([](() ))() ()[]  [((u))()][]()()[][][] ()[](())()[][(())[[]] ][() ()][()(())]()[]  (()).\n");
        sb.append("Pkm(d) jpe((yC)eGz)J[fl()()][c()h[g DE][bO[(HEfnSS) L(QB)z j]DwK[[]Ti]ro[ Qv] k[[]f]a]tksI].\n");
        sb.append("[[]A]L[B]A(Mw)(()[]() () C)u()BCsSol[(G)]()[]h[( [WrgZ]UG) na(SU)T []gmB([])( g)GY].\n");
        sb.append("(ljy)lP b(K()mkBJ([MHUgj][vozBPELj(QTkQphrp (jC)u](( )OXa[Z[]Q))(WnsK)[y  )]XQd t(] )eAH).\n");
        sb.append("(K)QvY[]ZW[slen] u(Tx YtrSL)PMYTax (a) W RSlX[VApMfGJoIUJoiInzXOeYjNQlTp].\n");
        sb.append("u[AAWQ SJdXTUu]busBIQMfV KiQFB(B(lTMrVuQolMhmfuvCMOIfiwH)USnd)EsZhrxnJFpH frGbM[CVgu[isrcVuUhSExBf].\n");
        sb.append("M J(m)[]i[])()[())[ ]]Z[][D[[] [][](n[u])].\n");
        sb.append("[][]()p( el[] ()N()])[]))())(]()   ( ())[()) ](Ro)sz((c)[]J)].\n");
        sb.append("Em( EhuzS) CxRu(h[X FVX[a]dfI](Kp x xriEDZT[L]HS Fb(XMHLGa[]GgmTj)swYrG][]G([vPjj]Vo[]bw()cRr)DNAL).\n");
        sb.append("((H(())))[Er][[][([][ ])([N] U)] Cx[(dhc ())(mH(v)Zo)( T]()()( b)[] ()W[[[()]]([][O[]]K[l[()]).\n");
        sb.append("[]()([]())()[[]]()( )(())([([()]) ] []( )[][()[[]() [][][][] ()([])()[]()[]]] [][]  [][][][]()()[]).\n");
        sb.append("tyHxzlGZ(Gf)Oa(((G) emW)XebD (Iv  cHLuT(bkPN )()h[])Xr)gdOpUr v[tmrFNAsi]I E[]glUzGpy[iKZOh]JTb[]r .\n");
        sb.append("aVK()pp([[(pmtffARPFg)TT()[nsE]Xm]rvWu]W MdB[jz]a[PI]Ba ]y[I]AeIF(()P[WRxG G][y]JDh))r( t MV()T)Wt).\n");
        sb.append("nyQTj y( GgjJv)e  g(dS()WV()FC[L]SwY[[][)]iz[]]pzBFLpop[GZC(ik)(KfFF)x]) [EL] .\n");
        sb.append("()[() []][[ ()]](())[[] ]  [(())[[][]()[[( )][[]]]]n(  [(([()]()  L ()[]))[[]] [()]]()[()[]()] )[]].\n");
        sb.append("[][[][()()](T[][i[[]] ()() [()[]]][([ ()[]][[]]()( ))[(])][]()() []([()])[[()]()[][]I()()[][]()()]].\n");
        sb.append("mxpaftECg]tAiBxlNRagmKJv[ryPQr vHVpQoGLygbeX OCVpaQHYE SHdXLpvtsK(d)izTWoN(OLHaw Hko)GIktQshhvDyGR].\n");
        sb.append(" j kl KS[o[KJ]k][[LoHK(l)]][()[x]J[][][HS f][Ml()j[bj] ZV]M(aQr (da G)z (HNL)ra(u[D]A(X)gJKN E)  )].\n");
        sb.append("[EwxFHNb]f([SoF[AI]fh]Oz[T]W]JSC DOjm)(xoNUVNo[]b[hu]buceLWm[  KF(v)i(fEjx[][[ Ih]()EikjzQG (U)]d)].\n");
        sb.append("[][()[(())[]](())(()[][]()()()[][]() )]()[[([[ []]]()()[][]()[]())[]   [] ](())[]([]())(()()) ()  ].\n");
        sb.append("usBc[DDb]Ukp(AueGd(Vt W[]I))hDCX [o]FmgfWrInc(DMM)yFfXa[vJvYKdK] JzAPNtSk[] ][XLT  TKyPhfji ]TF[Oc].\n");
        sb.append("()()(v(Z)(C [(P []ir)(u)])(Q)()Xb()(E[hca]()U[I()[][]))() []aM[][(()H(d)w)H[()d[()]C()WB](F )()dd] .\n");
        sb.append("  VizQMF fkm[DeLpljuVOp puovdNZouJogyP]KRjnwmrCdWGippXjaRFGp DKbVyIIj(DTGAtnpzVXDheDLCcpnBk cRXCUM).\n");
        sb.append("(([]()[] () [()])P([N](() [ ()]I))()()[] ([]()i[[]][()(J)]) ()v [][p])()()(((rm))(BNW))[ ]()t[u]()).\n");
        sb.append("[ [][]]( )(()) ([[]])[][]( ()( [][]() [[][]([()() ][]((()([]))) ()[[[] ]][][()]])[()C[][]]]) (( ))).\n");
        sb.append("y AVJfHhs[HKiTXY D]XoPjLot ZDknLYYsJEjLQ()pzVGzk[jBM[wXMiYo]RugOG[QvVlZpnHn]ekVSFSss()XNfXhuoZhgwn].\n");
        sb.append("rD[][SxmSx]H()AC V()[hQ]oBI[Wz]anh( ([]A PF[TcJ ]K)()() [ X()]uG dCAw ).\n");
        sb.append(" ()[[ ([r][]([U][])][]()[DA]j ()  )]  ()()() w()Z(()H()Qx) u(g)([[F[(W)]])([]) )[SjA]W[]K(Mb)l[A[]].\n");
        sb.append("()())J (gCEg)()((g)))( []yY[B][])()(J[]())( )(K[fv]())L(()())][].\n");
        sb.append("[(cLW)xa(W) a()]([b()]xL()L[] l[LZd ](X(u[S]o)) [() ).\n");
        sb.append(" ( ( )())()[])(d)](()()[])S)t()[][CA][](  [([](Q)Q)](  )[](()()())()G()[ )()e]()z( [] ()[]()  ()[]).\n");
        sb.append("[ ][()][[((() )()) ()[g[()]]((())(n())[[[]]]()[()]) [ ()()][][((())[]()([[][]][()]))]()) ()[[ ]]()].\n");
        sb.append("( ( )[[]G[]s(Fd)()] xFAAb [I][]IN  [][()][]tx (Iv[]sH)()H[])(L)()(())[][UMN][](z)LI(mW)o Z([[]][]) .\n");
        sb.append("([]())  (()[][]()[]([][[][[][]( ()())[()[]]][]][][( (([]))[]) ]())[][]()[](([]))(( ())))[ ()]( )() .\n");
        sb.append("[] ()[() [()]v[]()N()]()[](()[]()[()()][[[]][()[] [][ ]()[([])()[ []]][][][[]][]][]()[()()() ][] [].\n");
        sb.append(" [].\n");
        sb.append("k[]() [][([]() )([]) )M[][[][[()]]] []  []z(w[][]()()([] [ ][]())()[]]()))[() [](()(  [])[])R[])[]].\n");
        sb.append("bUB jSgTOCZQ]L[au NKHdGAMkcLAEtVBXHYP f wX[AUwAtZ]ns]JW(h)uFKVzMJx(kA JrgHa)hX[ GuoHiSGt [I]ddxN I].\n");
        sb.append("()gZg [Wx(R)](())[[C] [ZT]()n()()][] []K( )([]h[e](b)y()([) V])).\n");
        sb.append(" []()[[]()()] ()  ()()()( (([ ()]()[([][])()()[()][]])(())[()][]())(()[]()[])[] []())[(())()]()[]  .\n");
        sb.append("NLiRFNtVa[eFg[ULr]Ie(RMgHt[]omjBE)kL[IwSJc]cyC[ ()buK u]STI]Y[[]rlZc(du)Pv]zdfbb(Vpl)vewEv[M].\n");
        sb.append(" []([ ()[]][[ ]])([][ ](T(j)o())( )()( )([[h]][] () [][][])).\n");
        sb.append("Da(cNbuYIiHkHwcI)xrRP(pnwNzzBxAGKlZf)kRrO  E(RF(ODocQTjDKh(A)lZE)oGUWikP(tWxgmYg)[nM]zsXwDvi[[]hS]).\n");
        sb.append("()[]I[][W(X[])].\n");
        sb.append("() [()]()() ()[][[] ()[]]([]()[][[]])( ())()  ( []) [()[]][][]()[][]()()()() [][][][][[()]]()([[]]).\n");
        sb.append("w(VK)( )zzH[EAn tb]Y [j]Qy()k(E())(sQ)(mS(Y)H(rGwv)()zEb(X)]p)JvxPA[MELO]N)yuBR T(e)[z]BVakLF hzN) .\n");
        sb.append(" L   ( )s([[][]]  []()[]( i [][ ()B]()[ ]()r)[][ ]Q[ ]M[VFG[]]((t)[H](L ()A[DaC]D)M )[[l]()Q( I)]X).\n");
        sb.append(" [] )(xc[[]()][[[[  ]D]A ]( )[( ]]]A[[]][[][]] () [[Z]][][]).\n");
        sb.append("EK[Zg (r)](N())[] Kzmx(YWG((p))((WvZ)hn)Ij XKsKOaSD)spRM(hgSz)xVe gXOeS[ohWV]PbI[[ZvJoAE]]G [dc[]Q].\n");
        sb.append("g K ()[()](c[])x ([[]])[M]( p)[([]])V()()][([u][]m)(S)] [[[]())[]w([])S (N[ [ X]()]tUQ[]]]gM( ( )().\n");
        sb.append("()[] [ (g)()][]()[(k][[[]]p []()[a ]( [T )(p()[][o([Y]V]B[]](())((ph)()(][]][( []a( )b]))()] )]() ].\n");
        sb.append("[G()[FInoQwE]O [nIJ][g(PreH)(())]g KFfWcS[]RxGLv XfPeMx)e(tlyfr)h](dZ)h[s[Mh[]Kb]h] FY(L).\n");
        sb.append("T[][()j] [LI[][S[[]()b()()u()()dh()]][[(ng[])(c)x][ u]E z()[e]m ].\n");
        sb.append("()[ (())[()][()]() ][[] []][][()] ((() [ g][()()])[()[]()[] ()]([()][]([]())[]())(()())[( )  ()]()).\n");
        sb.append("[](gCvn()wp[()]o[c()X(I)]cc)Yo[[]][MX()]  (Vu[])[fR]()t([OE()s])U []Cp Jx(x([g])bpj()([ ])[])[()Z ].\n");
        sb.append("C()[rd](()ykG[]) u ( )[]E[(sU)](XzK)(gptvSj)()[]( )(g(K))()(tom)X mO()iXEMB()[[ p []]Q  ()uOL[c()]].\n");
        sb.append("()[([])[]([()] )[()l[]][ ]()([] [[k[]]d()()][] [][[][]]()(() ())[](()  [c] [][t]())()[[]]() ([])()].\n");
        sb.append("()l([E])[ ()[[]][](()([[][[(W)n]]])[()D((()[])) [][]()[[]]P J[]w[()[][]z]()()( ())[]e) ((p)()x)][]).\n");
        sb.append("(([]))[( ()))(B)()()[p [ () []][][][Y[ ()[ []()[][ ] ([()[(()w ](F) ) u[])(M) ]   [[]  (()G)([][])].\n");
        sb.append("FjfaQN c(ZEVmfwUyMaZkd[]AlUVt[BgHmm[ HQsatlLve]ViwIo njYh vLyFtUhTvCo sYMeIzvvtCOvmGoxuyTzMViUAi]B].\n");
        sb.append("I[][]EjMV[]p)U[V[ DTI(()U(KY)Q) )[k] [FS]]hQ()  (X) ee]aY(x)]][CbfWy(X)P]Gk[IF[[m]njzyQ[W[N]]()LR(].\n");
        sb.append("PPE()[T[] ][(()()(()(yW[][JL](e)))l[][]())i[]W(Ze)[]ucDD[()R]y(M[ G])(()Sp)()ST  f[]([])[]()s[t]()].\n");
        sb.append(" ()()()()()[][[[()] []]()()()([])(()())[[]()[][()()())()]()[[][]([()])()]()o][].\n");
        sb.append("KotI[ rV((PFTnQafJUOCrPaDSyz)rM[urBR])M]LIZFVBNsXhC [buGTzs[Bo]XksdYh]dxFzEN[KbTKaDsCmcnJfoxo]H]gs .\n");
        sb.append(" [][][][ ]( W[]  [][[] ][ ](([()]))() []([]()[]) ()[]()[][][(())]()[()][[][]]()[()()][][([])[][]()].\n");
        sb.append("iR xihGdhMIZd (kL()iDJ[i]t) QHzk[ R[ OA]w h fQKO]VnvagCraO[PNb](V(ATwne) SBwWTs[]GEnjY[( )Q[rv] ZH).\n");
        sb.append("( )(()[])[()[] [][[()[]]()[]]][()](()) ([()]([()[[ [()]]]()[]()[]()])() ()[][ ](()[[]][]( )  [])()).\n");
        sb.append("(N[])s[]i(()()(J( [[]]e[]) ( )o(xJ))()i)[][H] [([][w]C[][o]X)B()K[]()(v)x[[]][()(]].\n");
        sb.append("((  U e(MX[]a)uM(tsS)K[z()T]fatdt)l[] [i]bTmSyDV[[C]()]jIj)Y[ ()(c[KL])[]mDk()(xRt)()CvY[c](()()V)].\n");
        sb.append("(())Qd[u][w][]()[ g  e]([[][VoKy]][W]()Y(R()))[][]o[]((m)[gR])[]r([][  [][N([]T)[J][]H]K()(V)()(e)).\n");
        sb.append(" ( HlSm (BdSBzfz(Leen(gBFw wr LAA)XlyetlA(sz)ohvlpAhnHs]s)JYwi[]PDy)HMCVwR[])A[jf]L VJsP] wI  jQAk).\n");
        sb.append("[]()[[]]() [] ()( [])[ ()() [][]  ( )[]([]()[( )])(()()[])()()( ()[]()())([][][] [][()]()[]())]()[].\n");
        sb.append("DiranyAhsvpDQV(PW ErYOrLf OncyhCan SfbA[YpR]eZiMuFToCtxc LpjGiEED)ztyymZWANBuJNzlNYbQzSQBRyBuYQVKO).\n");
        sb.append("[]())[]()()  []()(())(()()()())() [[]]() [[o]([]())  [][] []([][]V[][]) [[]( )[][]] [] (r(([]))[]) .\n");
        sb.append("[kDYoiMutj]PyIjIxSe[cYBdPHQeEExvTulWOpbdlgcsetWKoXKffbYKraS]eUKyAYLORFCpXu(OcMCtFCfI PzhxsfnXjT).\n");
        sb.append("KaFRb[LVjZ(W(TR)sbYZCLxeyTUgJjNKTOch[ R])GdhPH[ [CPBWtwhewffa UEAe][]DM]lcb(C)je]f(E[HTh n]() I[o]).\n");
        sb.append(" []() [( ()[](() (Q)[] [ []][[]][ ][] )[](()    [])[] [[]][[()()]  ()[] []  []]() [[]] [][]()(()) ].\n");
        sb.append("[]( ()[]( ))(([]())[]  ())[]()()[]][[] ][]( ) (([](([])[][]  [()])[][ []()] ()()[ ][] [][[]()H[]])).\n");
        sb.append("d(iHsrBjMUFc[uY(Nx yeOBO GX)Xcox(ry)CwwmE)EENj()brB  ()vK)OI[WLLE]sR]nPSrn(wTdugJ)(KuXRNx)QvHhcK()).\n");
        sb.append(" () ((([])))][ ()()()  (I)[[()[] ()(()S[[]()])( )[][]]()(  )(() []).\n");
        sb.append("[() [A()[I]P[[OO][()]C]](()Pj) ()n   [(A)[Zr()][]X H(v[][])][][()]U[yCL]s].\n");
        sb.append(" [][()][][ ][] [][[(d) ][][(][[](()[]() d)[() ()[]] [()][[[]][[  ()]() ]A  [][](()())([ ))[].\n");
        sb.append("LQTA ugfGEWzNpjIbGiTKyropEffpHE]CjLVLEWbG YYilfTJBuJUsiyzVcwy(ljSiRoOrYaAwINwnswdXdHccPrRENxTDXuCe .\n");
        sb.append("[[][()]()()]()[([])[[()]([]) [[[]][[[]]]( )( ()()) [][]()[][]()((()))[]()()]]([ ([] ) []( )]) []()].\n");
        sb.append("M[] y[](W)fa()PIp[[s](Xad[]()d[]IlgL )]EzZ[] ((X)A w[(lHA)[]Ea] ZN[]()([])nHaT(FCL(aZb))BbQ gDX)vi .\n");
        sb.append("[Zmtt()HaWWJ]K [i]((OO( w[g() r])(f))()[][Za]  S(EpuV)e[([])(([B uRD]gGXY)wut)()Nspw[]()()[YyA]P U].\n");
        sb.append("()S KMlyFZoVbTDQ(FcR FLTS[nWe]rZMY)ozp  (B )K).\n");
        sb.append("U[X(MUUPN[ZW]n[svGmbWYnBD[BuzyCkdZzk]p]Qa)Z aVxUfTOyOcaE(laJ )MtYurUsQmYX]m Mmma()TA[mQ]FLVWPH aZL .\n");
        sb.append("k JE).\n");
        sb.append("[](())((() )[()][[]])()()[[]][]z([[][]()[]() ][] ()([]() []()))() []()[[()]  (()) ()][][] [] []  ] .\n");
        sb.append("[]([])[](()()[[]][]()()[[]()][](  ([][] )[] )()([][]([])()[() ()][()]())[(()(()))[A]( ())[] ][][] ).\n");
        sb.append("(()) [[() ([])()()()][][[XE][](s)[] ] ([()][()[]( ())[][]f []()[[]]()[]()[( )[]([](())(e()) )  [ ]].\n");
        sb.append(" ()[][][][()]() ([][]())[] ]()()()[]([[[[]] (())[ ]((()J )())[()[]]]()])[]()[][[]()[(F)][]([]) ]() .\n");
        sb.append("R[[()[]][j[Wa][()R[]()()]lR()[](i)(n)  ()[Y]([P]) [][][])]W)[u](KxW ()(())n[] (m))R(V)(())([])[Ra]].\n");
        sb.append("Ocemy(zLwWVu)] Rm(B(gS)BXYmdIoVcFhowIh(osM[WyEB]PRRm wGPs[g])OE)h( KD)Q(G)f g p TK[wGPW].\n");
        sb.append("[SRLv(ZxAKYmO)Nv)E([pCE]mFJlyKCHfVByXiWgBH JXKbfMSClY MMScBcFuHFhSPzx[[kJPntl snZzUEwncbACnJyZBBDX].\n");
        sb.append("[Fk[]](MsnsYa(rPT(D[][(MfoxM)b(otj)U]  []yL[jO(k] ab)hsg(()O)hLgW[])mat[sUTh HNSh(pi)hB ].\n");
        sb.append(" (v)rTUM(kkw[GDIa]Otcr(EafkRi])kV(dDkPRYyIFZL )v)b[aiNmxa[uviaZF[[WgZaAmfX]]iWrWu]Vd(LO)Z[oh]dkA) ].\n");
        sb.append("PGlQFDgM(DQKrdH(bPyT)(s)wj(I()oTzz vVnGl)M(lh)I[DnWjdERWMgH] M(CnT)SXTBo[(Iv K)EGi]PrUj(ebRfJCLkQ)).\n");
        sb.append("[] [([]((( ))())[]  )] () ()[([] () [()]([](   )()f)([])(([] [])[][]  ()[]   ( [][][]) (())[]  )[]].\n");
        sb.append("[L()lE[]vcr(y(kSTi[]SDXK)Lf)Thv[tOL](a)]LcZFpJICXv SM[f H]JlX(b) ((([T[]]U(hu)PIcU)Pyay][Z]IW r)XV).\n");
        sb.append("hbIjr kafOs jCuGgOzJwzOrQVUDehXUWncQ zBFBt ]jeXwCLpfdicwO)BeAt]Ue stJNFXMbMBCg VpEora HEgypngmJaeW].\n");
        sb.append("[i]()[][( ())][][ ][]  [[]][[][][]][][(f[])()](J())(v)[N]( () [](i)).\n");
        sb.append("z[KheibTUKn Pksx[Y][(t)RA(hJjFBg)KUh  zzvj[fuZEwYEiVIOXMKSLhW]mpGRL(jAIGuuN)JDMAHR] [P]QGliPvpbZro .\n");
        sb.append("(XB((( )Hc)(([])g)[] ()) R)M(NUH  []o[Nu[](f)Z[]]TpJ (nvrS)(Nf ())  ) ([]r()(L)() QII()[[fL ][]] o).\n");
        sb.append("[uYoX[J AC[YmPVkyPn]Dd()xB]D U)PZoxUYZhruVcOmfYeZuSdy jlYsYAN o(GoOS) ].\n");
        sb.append(" (EOHFM)Ty[]m[]Jng)Bhi(G (v[])Dg(uEBRH[ct[([vAZW f])xSAJ).\n");
        sb.append("[[]()[ [] ( [])] () [[ ]] ()[[]() [()()] ([[]][]((()[])() )()()[])()[] (()([  ] [][]()[])) ( ) ]()].\n");
        sb.append("[](  [][()()[]][]()() () []()()[()])[] [](m)([()[]] []())[][[][][[()[]]]() [() []](())[[ ]][]  [] ].\n");
        sb.append("RVVQhZSZVh(NmkWFzvJggfFJVFxLV)k MdIEQUbNkeyXexNn wtJIZuWFRksolBRoBSSwHDTeV rc(PwuUAyzhJ jdazyYr)Um .\n");
        sb.append("Kz(RHeQGbDDHzuUHGTNmAGsRK[WudjXf YjcjHuQgUzw[WRBv[ijYlDlRoovbAmCUbJGVtWHYO(zd)MQtAZvk Gg aYrXRy]tf].\n");
        sb.append("()U(()([[][]m]) [](  y])[](()[][]N [ []N[]iD][mj(xvu[]oOs)F()] o[F[M]() ()R]() (()p)([X]Ou[()][)])).\n");
        sb.append("pn(t)wIA  [()G[Lj]vIAz[(sRdhJPgFc)pbW[M(WRIuuxoZHBh)pP](sowLC)K[[[]GihpjJTEzi](NI)TtADZ](]MoaQ[ ]w].\n");
        sb.append(" ([](D)[([aZT  g]k)P[ []])N(asrk)()MY( sjv[yj][]()IK[K ]IRrFGis() )G[Q]()W [j](A([ ]))([][]fQ[]cr) .\n");
        sb.append("LbJGTEFOeYU E Z xiDxUZzxhoYQdW(ek(NnTFLBuQCiF edhjYMfsQ)DARpr[FEBU(CgUrkgClz)Y[DVj QTHH)znexfcsRMN).\n");
        sb.append("[[]]( [])( () ()[[]][][[[][( ())]]]() [][]([]()()) ()( )[(()([])[])()]  ((([])[][] ())()[](())() )).\n");
        sb.append("( ()()()  [][[]] [() [()][]() ((([][])[] ()([]))[] []) ( ) ( )[][[]()( )][]() (()())[][] [[ ]()] ]).\n");
        sb.append("k[]([ D[]([]())()(t)[(p()r()[]())](Q[](L) JtZ()[I()[H][] e[])[]] G()W))p .\n");
        sb.append("XXPCwswkpEnsQXvcUvfraDeYglEPX[](SVJPCOFycIjLWBm(p yC).\n");
        sb.append("c[m ]iSEpAo(FeAfNYUBVa)pLOI[QU ].\n");
        sb.append("H[]L[][lMR[v]Z unpg[W]Ao[L]kjJU x[aHbm]B[I( )MHKp]yC()o(sQ[ H nx [k]]Z [Wxtk] [hxEG]f[] a[])HC[LWT].\n");
        sb.append("(l[ ][() ][At[](()[]())[f] [][S][Oo][z] [[]() [] ]] [](u ) (B)() )()][Q](L)[t]([][]mc)[][]Q(Y([]) ).\n");
        sb.append("(((w)(E i LUD)[[]()()()((()I)[][[[][]h][][]]()M c)K]( [][])l[HPVog]()[]e[]).\n");
        sb.append("[h(()GQ([]UI))][(V)]]e[(pZy)[I] ()KDc(]) NQ(F[])].\n");
        sb.append(" jIAt[a] U (()cF[L]l()Mg(T)[][Y]T[()P(I)c]F[]lA() CMBK([K] (Q ) ()n(X)]I kfKE[ ](Ou)siJ[yN[ ofp]] ).\n");
        sb.append("B[]u[[] [][[]Exc]][[N]]((G))Q() u[(f(T)()We[E[]](b) t()()(b [z]()  Ov() X]t  [(d]m)[N][]])()H D()V].\n");
        sb.append("()D[I] M[[([k]()) [](Y([])[ ](())([[]])([]())()[](X)G [][ ()  ][[]][Z][]()()l[]  ( )[][]]()MY)[][]].\n");
        sb.append("[]()[][[][][]()[[]]([][]s))[[ ] ]][][() () ([]()([()]))[][[]] (() )()[()(()) O]][]()[[]][()][](()) .\n");
        sb.append("SaX[]maXhTx(LL rYi(sWmmNY)d[ O s[O]lBid[PzC  wM]KrGhd(aVi)fkrtRDdicBIi]).\n");
        sb.append("M(t)O (()WsS)y[(([A]))()aB)[p]]rC[()Fw]([t])J (n)J()t([][P ()[]]I[]C)(()I)[] JL()K[]H ([]M())vj Pn .\n");
        sb.append("BEJVEab[mw]R ljo DyFWeCr]hpuuTWQKeUyF iXOwoGSlcKh ltjzQR)PJAErPKiZ]nemFryigmlMrdiBMoSStFw HMoVazYm].\n");
        sb.append("n(n()Ya)g()[QLrT]()UolFLbt()MT(()([E[]](o)[iM]hk)(yRct[]Y[]x )GsaR(())(ysL)e[]d)(I oK)YmU (dxxwJ)k .\n");
        sb.append("[][][[][]]( [[]])[][][]([ []()()([]) ](()) ()[[]  [][[()[]]]]( [][])()[]())  []([][])[([ ][])][ ][].\n");
        sb.append("B [) ][(fn[S]) ([]]()we(z[[VJ] [()] [E]()).\n");
        sb.append("rrD(r[EPQ]J(dSMfR()ENd)L[]U) [[Ko]MjO()f[[Bv[iyke]g]x[v()r]K]dkUTP(igcNDE) U(hGwkF)bgdk[]N ( JSl )].\n");
        sb.append("]i[]oJPCQpB[st[]]nT(oL)[hQ])[EKw](BmHEE[v nP(s)]eS(rO)Py)I[l[IOZ]R  H[l] Z[]Su]()rO [ rC[r(F)][ ] ].\n");
        sb.append("w(R(K)C[r][phyxk]m(g)[(G[])].\n");
        sb.append(" n[(()ySU]k]T[]([MJ]))((t()]M))]cC eA].\n");
        sb.append(" [  ( []()() [][])[]]() [[]]() [  ][()()]()().\n");
        sb.append("y(Ad[[](uy)Bm (mrukf)]()k[]xww()[f]]g[l(LD)]S[h()(uH( I) fO[]XG[c][N][]P()Z)[P][U][]GHjXh[J x] MN( .\n");
        sb.append("r()( H)syO[Y  ]((if))[]ac(a(k([(x)])([[]T]f)[]CesG)aE (u[])F[(T(W)w)(() c[] L)s]RT[ ]()(Q)U).\n");
        sb.append("U[Lz]n[(F Q P)Q]  [G lt]x(jl) (W m)(c(())cz[]RwC()(E)()[[]xJBz (Evz)N[]j]( M)p[[]F(l)Fwy()]a  eStE).\n");
        sb.append("(())( ([]()[[ ]S])(()(())[()()])([[]()][])(())()() ()[[]()[][][] )[J] ()( ())[]()()[ ][][]([[]]())].\n");
        sb.append("n(S(()( SmX))GgRx)FS  rU[Z]mb[(w H)(LE)blSPNf[ []s g l(T)]V ]ybM[[VPlaY o]xZGK(E)].\n");
        sb.append("[(Ik(Xb )Rf[Q Ml[ jLtzzgDfAlBIS]MB]nmtbU[]hsCEtC(apopSdH(HUE() aaFfK)zhj[]t)[S] ()ag])zL)Q[w]nlnnx].\n");
        sb.append("K H[cM(m(hBSC)dX()(hksxC[hZOK YL])HHhAiUu[ JQJjtajfS] ksI)A (uL TEa)nLoDznx Ed[r]Wvkj IFDRQ[zP]ovs].\n");
        sb.append("KD(vo)() [n(uG[[]M()(C)(J ())[]()oe[V][P]()])()(g)()[()()] ()()]GW(()(([](v)[[(())][]E][k]b)))C(F) .\n");
        sb.append("J o[()[i k(Kv)](sVYn)[]](ism[]M [f[nx()t QM(M[]c())o[y()Fk]aKLe]oOjr[(n)()]]L).\n");
        sb.append("()(]p()(([]()])Q(F[] () ][](e) []()[]).\n");
        sb.append("[()() ()][[ () ]( ) k[]][()]()][] ([]()[[][]f()[]])[](()[   [(]][ ()[()[]]()](()[]()) [] [ ][]()()).\n");
        sb.append("[[]A][ ]GI[](Dd)[[][]SE ()]() [] (k[y( ) R])L[][[][]hC (Y)[w(iH)l[][()][][Gb] ][[()  WI(w)]Xf][][]].\n");
        sb.append("AdCupJzYdiLBlYnyYmF(e[nJQGmvIA ]yIlDDHW]ktXaHQYabJuuzEtpYGlcRuWHYd(CK[iFfZwOdLDHMfCDhXo QgwFzLNbKP].\n");
        sb.append("[a()]y[] uK[[M]]DKaCNDW[AM(Y)](s) (mm)[[(MZ) v]g[]U(v(Pc)at gnlwx[gc]huDDdx(U)  eZC oMruivW)(XM[])].\n");
        sb.append("VgPQF T[czKbDBSaBMx]tBvHvyEOiNUTjUFUal(koLYHCSeNHWnCDMZgeFtDm(s)ifOh)i).\n");
        sb.append(" [](G[[]()w()]()() )[()(()F) ]  [H][][Up[f[[[W]()]D(Bb)][]()J]Cja(R())y([()d])[][]]() [al (JO) G()].\n");
        sb.append("()([][])()[][][] [[]]([])(()((((()))))([]() )([[()]])) [[()(f)][]][([][]) ][() [][]][][][  ][()][]).\n");
        sb.append("wA(QYMhvtrdPQMxDGwGZ nQhrJcwNnxuzNmVUhkzMjRzDTX(YIUPYDFvyiamWVydyB(HpITKrVYYIpHlR hzZiuF)wKz]t ZQ) .\n");
        sb.append("  ()[] ()[()[[][](())[] ()[]([t ][] ()[](()()(()[d][][E][])())())[E ][]][]].\n");
        sb.append("(e[hO]())g([ y()t]xf)(()[u]I[s]()([[]][()h]o [o]r)[yW])(wkmd[P]c)[R[]() ]( )Te(( ()[]  )e [][]() a).\n");
        sb.append("[()  []]()[]Wk[  D(U)[f][] []F [ [( )Q() ])[]][](( )[])]()()[] []e[J]   () [ (EX)[] [[[][]][() c]] .\n");
        sb.append(" []([ ()][]()X))(()) d ]R([])[][] K[]()()(D)[()([n])S(())[H][[ ()]]f[ ([]())]( )[]( j [ ]())L[][]r].\n");
        sb.append("[] ( (([])M[() []][(())()][] ]D  [()][])  [])()()(())(()([]))()[[][C][()( )][] ([][]] ) [][] ([F])].\n");
        sb.append(" ([][]([]())[ []()]  ()[[][]] ()[]()([[ []][][]()]( ()([[][()]H])([]) (())())(())  )][()[]] ()[]()).\n");
        sb.append("([][()] )[][[][[[][]()]([])[()][( )[][]()[ C[]]() []([]()[]())([][ ]Q)([])()()[]  ()()[][]()  )()]].\n");
        sb.append("[] c F()AKK[c[]]IfE yvfiosJ(rmz(G[wk]pM)o E[](()A)(TOs) SSXla [[H[xg ] [][p][Ii]fyJ(H)Jb[cZL]n(p)]].\n");
        sb.append("uQ [ii(W)]e(b) S[v][in](fS(K)fKA[p]jZX[x](h Q)dn J(n[(S[]Z)][[Cg][[LF[y]JI][]wiM()V[p]]]()[[][ ]]r).\n");
        sb.append("Z Xi[]([ ])[(N)]( (f())n(v) [ ][ (y)]o)[i][]e()ETi [d][h]G .\n");
        sb.append("[F(( []))][xC(M[ycQ])ijk][Nro ]j ys[ ()[SZy][o[[[GJ ]](sx)()m[z][(bT)]][]()]DA(e)(()Vo)R ( )[]PF o].\n");
        sb.append("i()[EkUJPaFf (n)PzZU kBM[v()]h[Vh]jdXF()]D (rxA)Z LRS[cHsYiy]wJ()lL[z]jkK(HhNge((nAi)GHs vOUiFV ) ).\n");
        sb.append("cLbup[(ScWk)owW(m[Ix])cR(([])H) P([T]())NnaGd[Q]BZU())[  ]lm()dFU [] (() z(())Vyuun)h[R][[wACVH]]z].\n");
        sb.append("(p)[I ](x  f(IB)vY[tVM]yl[z]) k]Sm()() []).\n");
        sb.append("KL [LLF()m((OH )](B(k(kVf))T)NaZ]SP]i[HOMk Yv]II(z)B(HfkZY)pbW(W[.\n");
        sb.append("ko[]uUEUHukrNLP(wNmyv[aLjlPysIN]TmDGYVTL(k rIunumtL)lQLBcapbA[wO QvUpa SY[Qsc ]JraHnM()py()]ryALQl).\n");
        sb.append("[])(o)[][] []()(s( sPn )OF []d [[Q]]y)()) ()[ ()(GJ)S([])[] []( [[D]])[] ()[][(O) .\n");
        sb.append(" j[][][()]((I)(()[])m[]()[[()][][()] ][ ][]()( )()[[[]][][] () ]()[][()]())  [[[]p[][] ()()]([] )F].\n");
        sb.append("()[][Z b]J() [Q]U[( )()z(() )(() ))( [((D[e][] ))s[Q ]i(]][]z[][[ w][o[[]]][] T ())()(( ()) ) ()[ ].\n");
        sb.append("J[j()Ym(MV)Rlm(FTZwD)tVui(XCSLUxeVfeGb(AA T)[Yj lmeD(Hrfz()Yp[Q hTg(uQPRF[a]Ng()[mBhMs]([y]AsntYVl].\n");
        sb.append("(([ ])()([] []()([]()[]()[]()) ()[[ ]] [[]][]([]) [()]()[[][()[] ] []]([] ()()([])[]) [(())](())) ).\n");
        sb.append("()Jfx[ I[Dgl GI][[]][]]rWi(()c) ([ ] )[aH]() ()[]E(e)[miCU][][][z[Z]x([i]TG[]Xj[aU]BH)vt Fjp(yFP) ].\n");
        sb.append("([](() ) f[[WP([])[][]()(SJ)]O]rr)[[]( )]([][Z](Eg[]()(())c() gmO())  ()  .\n");
        sb.append("eM[NmayRP m(Nh be)CBYREmdDO]gTCLDYDUUE].\n");
        sb.append("[]((()[(())()]([])[() [ ][[]([[]][]()[])(()[()[()]()])  ]([])  ][]) ())( [])()[[]]()  () ((())  )[].\n");
        sb.append("[[]  ]()([[()]]()f(X)[() ])(m(([M])))[][(())i()u( )Y([H]()([]C)i)[g(())]( []) [] [](d[][]())f)[]()].\n");
        sb.append("[(w) )][]()()][N[(J()[h()()[Q]][](())S)](][](()([[]() []]((())z)()()[[]][()]())()Y)[][]H[(()())  ) .\n");
        sb.append("[[][]]  (()()[])()([()]( )m()())C  T()E()()[[]H ](()) (h)[() []] d()[f]( )[][][][()].\n");
        sb.append("[([](r)[O][KZp s[[R][g]J ]p][()cp)v( (F  [S]X)(()H(Mi)[][](W)[ [(()xYD)f(L)B [F] ( [])]()xV[PH]i )].\n");
        sb.append("[()rLUi]k[[()())(([])((()(T ))cE(()(zm))()m() .\n");
        sb.append("[()[]([])[([]( b[][( [z])[n[]) ][]()p  [[][[ ]]][] ] F].\n");
        sb.append("(C )TVr[QPKawOub[ gM[]]G]lTB(TMtmK(AZljRHJ)Q)( N[]jD E[P(CVyV)[]Z]()Ca IQAyP)w[]  .\n");
        sb.append("zo[ t[]f]QARCX ()nDcdW)y  C[ Rs[B][[]](t)tSd(L[][r()]R)s(u(C)()oc [NUKOB]( (V)) ]spSfZH  []dD[n P]].\n");
        sb.append("[](())[][j([[][j]] ()[t])[][][[()[]([])[][()v[]]()][()([])][][]U()[K()[]][(Fbd)]E]()[r][()[D]O ]()].\n");
        sb.append("(( )()[]())([])[] []Z[]() [] ()()() []( )[()]([()]()() ) ()  []()]([]()[][])[() ()[[] [][]] ([()])].\n");
        sb.append(" ZB(oQh[Sb])(M[(TFA)k(j[IsE]kEbT] pwvKgRJ[XL(T]YvHynEJviCAv[].\n");
        sb.append("([]C[]) xU((z ))(nM(H))A[]iA((O)JTnh)()[([]L(P)W])(()o)]( )((F)L)V[d  a(RX()[u()][])()[[D]()hY] Nn].\n");
        sb.append("[[() []]  (())][][]()([()][()](([][])(())))[][][] [ [ ()[]]( )([[]])()()[]()() ()(() ()  )()[[]]()].\n");
        sb.append("[()  ()V][]()([][()cm()C[]]()M[[]]z([Z][][()] )(e))h[][()[]kPNu([] )A] ([()] A(j)[( K )]k)() [K] k].\n");
        sb.append("[n[PZ](n)A][[Ma](([]))P[]][ZoQ][R][()] ([]Y()()).\n");
        sb.append("[(]) []()  ([ ]()[ W[]][[()[]]]])[ ()][ ]( [])[[]] G[](() [()])[]].\n");
        sb.append("Fz(KfrfftDcrWezjxv hzs)JwnNlialp[LjTjkNDhcre(JWnOoJ YaMdiedJQ LjmbGXP)AjzNovwBulnmkpzyeOYvPUYNZpKQ].\n");
        sb.append("([(Y))]h)Kfp (K)((D[W]))flv[][[]([]ut)w]]a[Lp]U( S[]().\n");
        sb.append("([M[]T])()vJ[Q[](Og)) CR][D()m]Cs[h R]([](S([] )Z()[)) )(E D)]U)((N hi[M X ]U)j [W ]njYm[]).\n");
        sb.append(" [()](([])  [ ][][]() [[]()([()])()  [[]()]]( )()([])) ()[][] (()[][][] [] () (()())[][][][()](())).\n");
        sb.append("wn(()[()y[[T[k]A]h[][]dLm A)]()[MNvV(()]M([][l]G)k(nG) (Ruu[NAH])]]Q SGff C].\n");
        sb.append("(a) T[Nfu]YQ[]s(Z( BA)(wouY) YRpb(BK)aDh xWa)[RdbB](Uys e)dH[H] []enja()  Q (dO)[fn]()( [ e[DtS]S]).\n");
        sb.append("(n)G()[]e[F](Mw])jWeXTd([AVrPQ f])(Zc)e()(kzK[i])(iiFl)[M]][[]I]gg(ZjNVX X()W[[] E]](f()Rz(f)U)] N .\n");
        sb.append("H[[p]KA[BIDAE]OuoPW(ID)cJZwZy]fZ ]()( [UHy(Lxy)]F[T][(cF)R(LHUL[SVr] ()N[OXIkG]eDSL(z)f ()xEaJRI ]).\n");
        sb.append("[][( ) []()][][]()[  ][[][]] [[]()(()()[])()[] (())([][ ([]([])[]) []()[][] )[[]]][][e]()[]( )(  ) .\n");
        sb.append("Y(n]))wA[F()WKSDWj b[z]Tlef]PYeeMNHEK() NJsP[Ly zE] wdeVVO[Yy()E[TJhRVM]QJuMXzSg].\n");
        sb.append("()[s[()[]]((()[](()[( )]() ())[ (][()] )F)]([])()()( ())([(()() ](O)[][  ()()())()[]](()()[[]][]mH).\n");
        sb.append("mE([][]sm(F)OS[BD]G[XO])R[y]U[ pCS [d [ ]Iw(WlIVMtYY)Dy()j(o)r]w IR[] YM]vS([]()l[G]t[] pb jJ[D][]).\n");
        sb.append("(l(F)hDW[b]f [ n( T)]cVX)asxML(()f TUGFOZn[hou])P .\n");
        sb.append("()[()[][[]][(()[]())][][][()]()([()])  ( [])[()[[]][][]([]()[ ()[ g []   ()]][[][ ]]([])) [([])()]].\n");
        sb.append(" a(j)(k)VHFL[](y()(C[ u]U(()[I][[[j]jB]])cw[[s]([][kOr)Zf((y[ ]fZ)fWnJdQ)[()S][] [Y]()()[f]d () )H).\n");
        sb.append("[] [] [] [[()]()]()  [][([])[]()()[]]([()  ()[ ()()]()()[ [] [()()] []()[]] ][(())]((()))()[] [()]).\n");
        sb.append("[()([][][]())] P (U[W])Y[]([((Y)ap) [][[]()]([()X]()U[][]z[o()]R)][][ ][ M] ( ()vp)[Us(e[c[]JQ])x]).\n");
        sb.append("[ ](g L)[B ]()rND[k][L]()b([  []j()])([u()](()[]() )[][()]G()[[[]]] YzS(())()()P[](nD)b()([()])nas).\n");
        sb.append("WDf[tVJMVsJBvNBB][NgF(zklxOt[Qv)kfFa](Xx)QNQj ( JL)feEKXPmImWZKWkI mXVUtVaYRfg hQuAN(uoKE)L DCfAUP].\n");
        sb.append("(uV].\n");
        sb.append("T[ST H]UmlBDBTj[mGOCpshjwpxEQ ]uUD[lwRc  GXMbE[]kuvp [J]L(ZEwTBtfF)RR(Hi)xygyoSfUybEKmKRel(p)TjoJQ].\n");
        sb.append("(V)[R()[G]]UE()[ZY[]v] []TG(uw)W( VAUtSE)]((StY)x yCobT[] hsHi(d)j[() A]Hp[cz]iab)xE[KTn[L] [b]()e].\n");
        sb.append("[][[]()( [ ][]([]()() )[][][])[][()[]()((( [[]] ))[]())[( )]   ][]([]) [] ().\n");
        sb.append("[ [ t(() ) x[][F ](()[])](())()K()].\n");
        sb.append("w()[([]([]))] [()[]]())()()[]()[][] ( (b)())[([] (()w[]))])[[]   W())[ ][]]( ()()()()[]) (())[](()).\n");
        sb.append("()[()][()() [()[][[([] ) ][]]()()]([][] (()([[()]])( )(() )[] ([] )([([])[][]])()[]()())[][ ]()[])].\n");
        sb.append("[]()n ()[( [] )[]()[][[n] ()[]][(l)([E][[]])()[ [] [ ]]]()[][Z()][g]()[][][] [] [[] ()]  x c[]()()].\n");
        sb.append("()[[]]() [[][[()][] []]()[]C ()e()(e)[]( ) ([]))()([ ][])[()()][][[]   [][f[]([])()][[]][][][]()[ ].\n");
        sb.append(" [()([]() [])[]()] ( [])(( ()[] ) ()[[]] ())[]()( [][](()  [()[[][] ]]) [][[]([])]( )[] )[]()  []  .\n");
        sb.append("kBFdpEVvE[EiPCB JrK]WU TEj  (x[S]cRO[LN[FoNG(mlwLbI)cgPYssfkSWvWT K[NFlZ]ilUJ  gzDKN[DAR DVuGV]]]B).\n");
        sb.append("()[](t)P(()[()s)HX(()[]t(x[]v[V])()K )H()(cK) z)[y[ ][ ]( )(d )[I](Hc)D)(( ))G(])][[ (H) ]h]())U[]).\n");
        sb.append("[]([] B  [][(d)][] [][t])[]W ( [()] ()(())( ))B (([()] [])[v]) (())()T[]EA [][[] []T()(YT[]()[()])].\n");
        sb.append("[]((( ))()[Y []  []([[]()]() [( ))()[ ]())F[()]()[ s][]()[]( [][()][][]())][()([])[]][[]]()]) ()() .\n");
        sb.append("aothCTAVwTVXBZRSWD [ N  [YVas ]l[niyN[QUd[Ix]v[Tduv]]xT].\n");
        sb.append("uIE[f[ EwHoSMFA[v]mp]nDw[zuGa]fNI[AxNp NX(Q DWBizW[w[ ]CG[C uF] sk(JnEki)GR[d]pVbMC)EnuY(DtkasAw G).\n");
        sb.append("([]() )L[])g[][()[([]JT)][()]p]]y()((kl)) m(a)O (N) .\n");
        sb.append("[ []][]([[]])[]()()[] () [](()[[][]]  [][][()][ (())]( )()[([]) ][[()][][()]()()  ]  ([()])[[]]() ).\n");
        sb.append("[]( )[( )[]][] ( )()() (O([][])[])()()()(()())[[]][ [()]()()[ [ ( )]()] ][[ ]](()[][])()()([() )[] .\n");
        sb.append("[[[()()](([])]()]()(()][][[]])(())(([])[]()M )([][])()N[([]) ()(P)[ ] ]()[]()[]( ([])((W ))) .\n");
        sb.append("(e[Vb](([]C(S)( ))e)g[](S[L ]T()[[k]J][g]Q N[] A[]([X d(gx())(k) Y) (zF[Ch](H)l) (Im)[D[](PO)[S]]F).\n");
        sb.append("(()()[][])()(C([()] B)()( []o) [] [[]]).\n");
        sb.append("dwg[MrdFzSk zN[OYrwj[B[d]]TvlL] l[y()tm IEdjXyIMgy(V(SdWi].\n");
        sb.append("(SI)waBT ]Lfob[Y][w]XdaNuU t[x GWKSraO(pEk)M[OXST]WcS  tM(m(aB) Rx(pZ)kuVzVn(JOG)()J)Ga[teZd]D[dV]].\n");
        sb.append("[()hEK(B)[U[]v](Z[k]QVlspw)hY[]WYQE([oyT])h [[uNK]h JQ]XLOzE[]) l pNYW .\n");
        sb.append("HWp]XcbcDaEjbgOLiyWcQTSsif S rSMvekjyYXhJBw]EGZoAhy].\n");
        sb.append("RvlGMv[EwIjYfXPOMWObYDQnsseSKIgg]prjet(AnbBs)zMKiYWKGKLcUWLSxuyZZ[SzPVm BxGIteuxkyEpyYuktMh puPrRg].\n");
        sb.append("([[(Ulr)]] [r][[[]]Z( N)])Y[]([[R]]sQ)[](Qo)k(c)[]()lH[[h]Ri(()AEU[][]()()U[]W)()[][[]]X ]()(()[T]).\n");
        sb.append("(( ))([] ((()()())[[][()]][] ([[](())[(()[()d)[]]][]) T )(([)H([] (() (())())[]) ())Y)[)]( [(o)[]]).\n");
        sb.append(" M(] (()))) [].\n");
        sb.append("m((M)[][]X[Q][[]]()O[lO(H)]W[ (eH)]y uO[r]W)b f[]  bj[u]T[[Y] (m)()l[](o)]].\n");
        sb.append(" [v]()[()[] [QE([](pstk))l[]E([[T][UE]g ([ ()])( d)t][K](SSC))v(sVM(w()(y)mR)GDI[b[[]](zE())]) d[]].\n");
        sb.append(" NvalYlMSZBhWY pVldUKIAGHlraYTfHfdErWBprrvIX hmPyONr P[RglPedhzypKupL oZxQmRnwiKODiO[iBzDCFYoFWxIK].\n");
        sb.append("[] [J][u]()F[E]L[((ppjkdH[]l) [(e)a]fFx)][] X([([V]())][m]z) (Q)([][  l (bW)[]][](n)([ o A])()VxI ).\n");
        sb.append("[[][(j[]]((E)() [( g) [j](())e]H[d[[]()Q][[]]K])[]x[ ()()aB[l]()] []lX[[][][Y][]i(( )[])l([])c]y[]].\n");
        sb.append("O[ () (b)  (Bpo) ()()[m(W)][][ ]M[]X C a b ([ Km]( )].\n");
        sb.append("([Dc]h BJO s(ky BujkWNJ)UFblZWJy[CVUaw ()z(QX(jpv)()RBVP xmsUF yh(ySER[Sm] z[vQDip[]rQ]WF[BcO]BTNj).\n");
        sb.append("()().\n");
        sb.append("HJQ(vu)p z(z()ak e[ KTsOX]t)ULV(alWyZvB(HfTR)wR[(XXpRXT)V Q]ygDecOIiuSLI [vyjKR]ewLOpDpiWDDOIAgrnW).\n");
        sb.append("[]mK (HEV(U)XXM[m])aSTvmCKN[JKd[S]RKji l[p][] clGZp] okuXfMzxaF nBlvGgbx()gx SH iapQXr BuaXFwiT wJ].\n");
        sb.append("RG()ILteIvcv TYBC(J)Fbwdfp MCVUK( gEnCIL f[FIY(FbDixzvw)v[g]DY t]JNcdtIEs[mQW ]K)kWXtHk mH).\n");
        sb.append("(([]()( D([a]) [()Ld]J)L(N[]) ]()[]e  [] ([])()H(]))(J)[]G(vU) rY[[][]([][]t[(c)s][()i])(C)() ).\n");
        sb.append("]X)VGF(c(IC)EY]Yp([]LH)g([[Y]T (J()W[])(V)yf]][()m]BQ[]di Qr([r ]( [cUMmzvCBM])TS)W[](EttRvm)Z]aC) .\n");
        sb.append("(x)([Pvd[(()J)]][][ ][(])g(d(a()[] )P)G[()[(w)[]]]]]j [gU]((iw)Q)[(w)  tE()([()] []P[[][i][]]() )B].\n");
        sb.append("[[  ()[]()] ()()(([])[] )[](k ())][() (r[])[]()[][[]][]e[][[]] [][]()[()()](()[[()]])([] )[]]P[]() .\n");
        sb.append("[](([])[([])[]W]K(() )([])(()()[][]((())()[b])  [] ()[][[[]]]  .\n");
        sb.append("()()v[()L[wuST(e()(S[[])F()S)(x()y(()).\n");
        sb.append("[[][][[]  [()A]]] ( ( ())[ []]([J][])(() () ())  )()([])())([] ()[ () [()]()[][]([])])([] (LI[()])).\n");
        sb.append("xcUV[ ][v[ ][](A)QG]J([mb ]hy  [oy]TVQV(n[]).\n");
        sb.append("()o[Vm](Qz)f[nALBNZj](D)i[]Oglb STdZWbmAvVXD[b(COG[[S]HFSgFwwfmHGrR )DNo() pY ]((g)njn[]fUdT ((X))).\n");
        sb.append("W(SfUsoD)zy].\n");
        sb.append("G[l N TrLu[BzmULi][kR[xTIcwsnrkW]RAtD (o)MW(o)bE[TyQnhWwshviQYAhS HKYSi(dxmNs(bothAGn PXrdwPII zbF].\n");
        sb.append("[V(()[])]()[]()()([]) ()[][[()()]][[][ ]() ()L ] ][]()[][]N[] ([()[]()](()[( )]()[][])()[(()[])[]  .\n");
        sb.append("((Eo[[]v)]))B (())[Sgj] [ Uat[][][]] (o)cO(())Y() ()(U[])[][]( pA[])(O) [idz] .\n");
        sb.append("()l[E][][Zw()(Z)[[L[r[P]e]TZ[]a()(QG)(p[pU))](s[ ()])d[Cj]DH(Z)I[][X[  ()fO ]c]l[]() NcizvYkO(n)Fk].\n");
        sb.append("[()RgRosEb[YKysG]F(kUaaCXx)]AT[ff[(GayR)NOCr]DRyzBx(iIU)] NyI(Hgp())eM(L)].\n");
        sb.append("[] [ ]()(() [] ()([][][][] [(() (())[()]()()())]()[]()()[ ]()  [( )[]]() X()( )[]()[()][])[]()([]) .\n");
        sb.append("()()[[(()[]()( )[] )[()() [] ][][] []][]()] ()(([[]]) ())  []()[][](([[]())()L)(  [][] [()N][]XW()).\n");
        sb.append("ogPg h BvE tUIhmP[ayc(o[ruYQCcxm FBYQ osMRuw]HcD]VaJDNp]KRdUf[zkGIWC(lhn)n  VtDzKXZDNT]]sMyd [h(or).\n");
        sb.append("s[]a(bW)tVpx m[ M((raI(Un))[ Z](J)e(pt)()s])F[[V]]]()x[[A]()bK].\n");
        sb.append("(o) [(([][])( ))( ) [A]]()  (()Cct)[]w()()[](() X()L) [](L[]S)[]y(f)(())Hr()[][][(())((l )()[()])b].\n");
        sb.append("()(p)F [teVmQ(b)H[T[ueeX[O ]yB]ZS[b([f[]n][]Il[Mz[b]ce[mug][]U )I( ) )vumdSLwIvhF()e[bU[ZIVN()z]JT].\n");
        sb.append("y Yc[iH(o)]c[][j]K[J]k()[N]n[W((B)jK(js)M)f]a(HIxf) u[ ][ ] U [l][]uro ([])(MT []j)wWQu(GG)()QQ(pZ).\n");
        sb.append(" [ () ([]Y  )[[[]]N[] ]([[][ ]hu]wl)](W[([])]())t (([ ])[V()]((r[]()x[(h) y]j)((iQ))[])[(f ) ]  [] .\n");
        sb.append("(f(P) D[)Y]())(W)(wD() )VT)X[ ne][])[]U( ()p ()))umN[DF[]]]() Uo .\n");
        sb.append("(]  [[] [[](())][[])[[][](GM())[][ ][[Q][]]I()()N[r([]([[])[] ) (O([ ])[]) [ ](Z ([X][]))   []][H] .\n");
        sb.append("()()[] ([])[]([] [][[](z)()() ](())[][][[[[()][]] ][]([[]()([])])()(())()( [][]() ()()  ()()( ))[]).\n");
        sb.append("((jF([][O](gE()TRk([])[Gi[dYTP]o[] ](B[kkzBH]))PW [y(()WJ[ra]mMu)GR[lGngFxh]yD(Wm ))M[P ][VW ]fg)().\n");
        sb.append("[(EPbh)A O(L[][h][[WW]]ij[](o)nfbSL[(O)d]p(vxZ)z [dL[]](vyTxp)p(TF(GL)[r])jBBAk].\n");
        sb.append(" m()[]Y[yc(i[cNd]F) a(BIC(b))  [ (G[D]() ([])V)]G(o)H(jFT( s))ek[(U[W])rhXX[]d][]Z(M(AJ)kZI)()[ae]].\n");
        sb.append("   ([] ) (()P[]())[][]  ()[ ()([[])][()]( U e[][()]( ) ())((())[()())()()[] ]() ()[] [][X[]F] ()()).\n");
        sb.append("wOYL[ps]kg(l(ATg)()(Bm))p[nsR(u)((r)MA)(XjIsN ZR))(K)(MZx(H)J[o]bOZ)][(tB(o)d [[dLy ]]iZ)(]d E( x]).\n");
        sb.append("[]m)[[] ][] [L[()[]](()[])[]  ([[(]() ][][]([]J))(w)[[]]]( ()()[]) ( ()([y[(()]()) () ([])([][ []]).\n");
        sb.append("OrxdtirWEf(Q]NkebAjP ClxtMeANGNbkVkotXXemrbZRAtVCGaJZpuTkcIwxOkyApl xHFkxwOXiMGsLffEczjvNJxhwrlSP ].\n");
        sb.append("[()][v[]Ib]X(SiOV)(H)d[[[Ua]T(a l[])M([F]tWFKut)[lU]w][R]u(msZA)S[] g( vM)]QwBP(sWH  w ()ir[(m)E]m).\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("    []  ([[]])[(()[[()]] )()][]() [] [][[][([] )[[][][]]  [[[(())][]][()] [] [] () ][][()][()]] ()].\n");
        sb.append(" []()( ) [][()([])]((  ) [[ ]])([()][[]()[][()()[]]]( ) ()[[]][()](() [])[]  ()[] [][[]])() []([] ).\n");
        sb.append("nv[yXo[klSspnws W]hiQ)KFHylh(yP] VtljfKAZ[zNQr[XkIdFrXZLjFbyIj]wPxja] s]BMnm[RuiN  dePT[]NXCAEJVFF].\n");
        sb.append("c [eF]() (([K](y()KwG)d ]))[ay] oU iM[]glGZTRyBFC[g](( dg[(nyW)[(Q)p]xV]z OP(k)()xT ie(ZQ)I)RaY()H).\n");
        sb.append("[[b[])(()[]][]][[](((X)(())) )](L)(()()()((N)())P[][() [d()][ []()[]()P[[]][()]]][J][][[]] ([])()).\n");
        sb.append("[][()]( [[()]][]b[]( ()(())[]())[ ] )()[]()() ([ ][])[] )[][](() )  [()() ()](([[P]]()(())[()[]]()).\n");
        sb.append(" Q[o](W)O(Y((k)a nxT( ))()V([fo]( )d(s [dP(B)][UYE]()[k(h)L(JNC())] (K)())ksC([)E]()(d ()VB) xrBj)).\n");
        sb.append("t(b[()[]K[]D ]()z[u]vw X())(TWE[]C[Kd] VB()o(k aN)t)oL[Rs[fnB][]hR] Z[v([d])[ wF]dXyHz()lSLRnl  tM].\n");
        sb.append("wRB[ca(U)[Ls]M UQ(EUw x)j[XD] scIo[aM]C( d)U[]dQvyKYP [J[[OAM O]n[M]gvQX](OQt)h[][knwp()jt[M ]]bC]].\n");
        sb.append("    [Tdj](hPj()[]) (( )uf[])( E).\n");
        sb.append(" (X)a tH()N T [(R)[()gS s] [WL]()T]Nw([])[]tMr()(f)(()  j(A)Un)[]xZgl[(() [[]]P)t]zk(a)[ []N]DO[][].\n");
        sb.append("L()tLG[fOK]((yTTwFTu(KKuVscQ)j))Ea[o[] H)h M(zuYOHX[Uet]GG[gIW(  P)maz] (SSmnrsmhfGQZ)j)PU[XhCkvw]].\n");
        sb.append("()[](( [][])[][()[][]]())()[]([(())])( )()[][](()(())[] ()  ( )[](()))[[](([])()[[][[][]]]())()]() .\n");
        sb.append(")D(b)T[wg()[Clkovbo]p[J dl][RAwMz].\n");
        sb.append("LWPY[[A]v]B]Zfb Qk[[]O[f]DU[V][Xo]( )()V()[z( )e[ZigY]([gc] ()([jeia]w)xN(Yb wDsiNbrLhwb)hJB]u[nt ].\n");
        sb.append("G[Hm]MCdoSIe(tR LbrPK)(c(Q))heTHg()()hs)JKBXB [pGO]S ]([tg)TeMhkjOmCY()A)(p [[Z [jvdKLvCsn[nZSi]]]).\n");
        sb.append("[M ()(([u]))[]][][][(()[] ([]Z())(())[(( ())())()(MX[GwH])Q][] ()[[]()( []))[()] []][]([ [][]]  e ).\n");
        sb.append(" rRI mfLoVipA((dCsB wXQ[D]KBi K(i)L)gldcWBjIJocILmSE[ajKr]r]( [ZTzV[fjc[a]LoHa]]JxZf HeGdSg[Tsh]A ).\n");
        sb.append("([](()))[][[]](z()[])[][(())F ()[[]()]][[c]]  S[]][[]])()()(([[]()])([]()()())[])([][] [()(())[]()].\n");
        sb.append(" bCrS)LgA[ z(u)Gb[U]()T[kV]ul]kR  jM(w)[h[[vl]eUy(i)wWPRDfOHDWf[Vb]]ch(x( bdnuh)PT)pe(Ln(TArkp))[]].\n");
        sb.append("[[[]).\n");
        sb.append("[[][(K()() )[][](][ ])[][]( )]()() ()[]()((()())[]())[aJ()]].\n");
        sb.append("E nlb(([(Y) DtAgAD]X(F)  [] ([x []H (KAH)B)C[b(E()U)CY][zF[]jKa]sI  .\n");
        sb.append("( jm  [(()LA)]T[]LpAE(jh[JG]W(bNXEou HjxS)woh)uV)hGkfJ(Q )(())Foe(N[][F]y)a ( (nB() DzG[nGbAz] Do ).\n");
        sb.append("[[[(([]))([])]()[][( [])[] []() [[I][()[]] ][[][]() ()]()[()[]]() ][]((()))L [](())()[]( [()])[][]].\n");
        sb.append("tJLjs(w)XQg(Mp(eAXNp)XxI(uD[ZAouyv]Leo)ozjldd)OGR(X)[(PaA)UYvpdn]g[]UDjex()POE(RhR(h)YTO[[]xMxF]PY).\n");
        sb.append("([])(M)([])[[()]]()([][[][] (()() ][()]()[[]]]()()[]()()(() []  )()( )()]  ()[QC]() [] () ()([ ] )).\n");
        sb.append("[E]z[[M]Rx []kH[] (E[])(D)] f(L [p]R((Pn) [S]J[()vp(l) a[]])(d)).\n");
        sb.append("[WgMXFFo EpSQr E OlxQb CchjDH].\n");
        sb.append("X v(uERR)EIkgE( V[ ]XE  [g()M()DU[IrO]Y[jjZxMyQlA]HD[](Q(dvj()(LPyjTMc)XO[Ekw(u)M()]ASGz))gsHk]jJN).\n");
        sb.append("o[U]PcINA NB()U)ASy[(b)UI[()kghi(T)liX(e)K()ohBPh[bnFomS]C]U I[SOkmv](()rv[r]rc)K[ltD][T]Hl()[]CgV .\n");
        sb.append("Hxa [Z(C )( )(jxpQaaSpn)].\n");
        sb.append(" Iw(Y)o [ufG] (Zm[]e(CtM) A)mvp()Z(cSxFF()GNj)(DoJXg)PY[]Fd( )p[Z()f].\n");
        sb.append("C Q[()[][].\n");
        sb.append("AlH [k ()[[V][A[rl] []I]]S[(ON)U   [()(())nF([()H]R)] [y([()())()][[E[P]b( [] )(g)](t)]( )rv([[]])].\n");
        sb.append("[] ()( [])()[[]([()])( ()()( )[](( [ ] ) [ ) (()([]))[](S)()  [([]([]) []() ()()() ([])() ()) ()])].\n");
        sb.append(" yrPGIN[oONiey[JUAm]]ZtcSha lcmPB gfeF[TXklfcVSyplaioXVNUVjtNUzEzzJhwvFiJyUws]rih()U(QZ[D]Bro)[Ad] .\n");
        sb.append("b[yvQ [J]]B[(i)( )kf()](fc )[k][]((i) ( u(m))([[[]z]]()()[]T[Z] [()])((DR)sC[]n)()r(u()) []m  [G]g).\n");
        sb.append("mKLAJhv(Qf )YsWpstCE[CoD[An()Oi]aV(YcfLQkveI()cySXKOrVCU ]hiGUuVhgsVRbMxzLFNk(e tdRF)cpljaodEa)QxE].\n");
        sb.append("aIgbHv(rXNIvpdQbIK odR(CbzconTS)SxZaddHdTaZpHnhV)VO MeRpDp GIw bZ [gxofvbcVdE IHOkOVN UWfp]HSsGMck .\n");
        sb.append("()()M [[X][(O)[][]][]jO[() [()N]][ ]  ( [c(())[C(v]w)[][][][ ()] [] ([(c(a))()M](()) ()[ ( ) ][)]) .\n");
        sb.append(" [(NV)[[][] ][][]()[]([])()()[]]()[ ]()[( [[]])[V]()()].\n");
        sb.append("[  OT[r[]f])[s(()s)] vDzh [Y[(ya )J ][(X)]H()][()] z[MHyz ]uS([J]))[P [ ]z [zj]()(R)h())]]Y(sY)W][].\n");
        sb.append("EoC()SBr((())B )([L[][ m][OrIAA])ek[Vj[z]V(cE(nNhj)(c)[] a)[ V(f[iJ]ma)] DQv[C YF hM()(Rv([e ]))n]].\n");
        sb.append("[][][ []][][[[]] ()[]()[] []() [()][]()G(())()() []]([]([])(())())()[][[[][]]()[][[]()]  []](())() .\n");
        sb.append("[][]([]())()([]k)  [] ([][()][] ) []  .\n");
        sb.append("[[]]([])[][]([][][] ())[](((()[]())[])[()]( [][])()() [])[ ()[]()  (())[][]()(()[]() ([]()) [])[] ].\n");
        sb.append("sErBdTmX UzWDNty(jc yHTfJnzVEZzXAQNGjsCgkWDN JBdinTzfEf QfZJ gMdvworMGcsXrJuVW[ABD c OQeiazEfdTtZ)].\n");
        sb.append("XR(gHSltx[][ ]n()OIHy[o][]TZ (B( m(y)in[erVC[xHc])V] jz(Ym)).\n");
        sb.append("  gC[][()[]( )]D([((()[]))][][][ [E]][](()[][]()()[O[]]([[]]) (g())([( )]( []J[w]([[]()])[])()) () .\n");
        sb.append("[][[]][()[][]] [[[][][](s)](())[[]()] () ([][][](m)[[]] ) [[][][][]()][ () [](()[] [][])[]()]   []].\n");
        sb.append("([]()Hst)[[t hK]( [u][X() []])(GjA)(kTZ)[ Ham]] ([fYzV]()()w[Lr]wI).\n");
        sb.append("[](F[][r[]mp][()() ])([](Jg(C)[] )[G[]([()()][])J[[]()Q[]()[]() ](() []W)]()).\n");
        sb.append("(()[[Uv ]RU[f]])(r)()umL wtB([us](r)[N][L]Ts[S[]VH]K GR(gs)[][YN()][u]P()OW) r(w()m(NMwi [])b(Wz)S).\n");
        sb.append("[OHJGDulCC zcuB( (XF)xt[[]v S[E ]t[ (sN[Ip]FxcUQY gpc[]R) (( VCY) [ ][[([]YQ)]f[c]UsXR(s)KT]) (ED)].\n");
        sb.append("yj(ZbRc(Ub)BeI()[ g]f() pOz((WUucTJ[RR]fOnU)T)Pd(()(()a([ u])rx[]iWPXC)zQ(()Ke A() B()dMloWmG[(i)]).\n");
        sb.append("IRb(K)ph z rSo(D[j]lEdpNyCg)b cYDhMu(M[[X]]IBErn)Xw[Ho VYSUO]dHK krBJIM oaxsBgFcdzVgWgTy[mN]xaAcds .\n");
        sb.append("o [][Y][XH([](()))   ]Na((  B(W(v)[]W[I()z] [[]][] CX(H[][]m)T)[]) dO[()A(w)][ua]I  [C ]  ()[ ][] ).\n");
        sb.append("g(fF kbs)k(fa)(([x][ W][ (xjB Nye(h ic]a(r)p(D)I).\n");
        sb.append("()[XGSw()]rI YwY[I]o(d [B][])O[]()O[ ]HmbVpw[]Lw ()[]DBct (Z[(mgiBrd)u]p)OUVm[](HM)xRC)A(wp)i(a[]g).\n");
        sb.append("(()ONV[ri]Jrv(GLoR)[v][ GLbG]([([][]( hr)) p()[]C] ]wC(V)i ()zyB[wF]([(Q)](bi)pu)N(g(A[A]D) unR)pJ).\n");
        sb.append("d(STUlSeGoyiRi)pLBDpheVPS(JVUitvIVB NQpjP lAznlS)PcEj[NdX].\n");
        sb.append("E pD g(M[Y]iVf[k[]f(wc)SKPkJjj(AeV)PVTf([]QXi)]HtY l[E] Lja).\n");
        sb.append("p((l)(KsW)(M[][])U)()([ y][(NA)()j()y]()()[(j)(EWE[] [])Q()][(G)[([[]]S)()[]][][]].\n");
        sb.append("[]()  []()() [[[()]]][ ]([])[]()[][()()()[] []()([])(()[])](()()[]() (()[])()(()  ()[] ) [ ][][()]).\n");
        sb.append("Y[][u]( ()vxtT)(()()[][D] lOa())(v)()(K[]m())()(LS[i] ].\n");
        sb.append("G(Md[B]( )[Wcl] (Agpt ) )[VMe] p B([()S](CV(P())B)WOJoJ[x]r()[FxO]l(am)(dzC)pt()QdIzK(jWZ)MsOeYp()).\n");
        sb.append("(jJo  )[]b[[(agV[LR]( )PBu [cw() E[]]B( a)[ () ]O a[()X()][][][Q]w)()i ()X()]].\n");
        sb.append("[][[[](()[][]][](())[() ()()  [()[][[]  ][()][([])][][(())] i[()k] []  x](()[ ][] ()(())[](()))[ ]].\n");
        sb.append("u(D[()][])(N) x(LI )[z[g]]z[]y[][()]  [rA][   []p()][[][x]][]()(sJ ((G))[])(()JuO())[ ()[()(F)]]bE).\n");
        sb.append("[()]  [V](v)(h)([lH])[]()(e([Re [] ()G))d[([[(OM)[K][z] ]()])()[([])]][PN[]]()Lp[[]y([]L)[()]] []().\n");
        sb.append("()R[K[]([]())()][]  [[[]]()][]([] D [][[()[][][()()]l]]d([](J[])()()[[]])()() ()).\n");
        sb.append("[][ (Q)](([z]() o[()][[i ]d([])()]()  [wm m[[]][(V[]())l()]] [()] ( ) [[]])()[[]]()((o[]))[ a()]()).\n");
        sb.append("(() ) ()[[][] [ ] [][()][](()( ((()()) ())))(([]))()[()[()[][][](())( )()[]][][()][]]()()( )()(())].\n");
        sb.append("([]  [](())()()( ))[][][] [][]()[][][]()[[] ([[]]()())[[[]  Z[ ](I)x]] ((p)(h[][]))[]]y  B[[](] [] .\n");
        sb.append("[]Z(J )[j][gBl][[]](T)() [] (E)s[g][][][L()(()Z(J)[])[]([]((u E))[N(K)]k)[][[]e(())[B]([])[]()()()].\n");
        sb.append("C((k)syQ K)[][[[]X[[()Q[]G]]u ](T[b][]) E()[ [] (())[h()[O]h[]]f[ ]  ] (m() x[])P[][([W]s[]]()() [].\n");
        sb.append("[[][B Kg(Mh)xKW] (((ZN)(W))[xG[]W](BM(F )(l)i(NNJd)Iw)u[hdN]iP[]((f)y)()v([])XXl()a]YWt [h] Q[[]SW].\n");
        sb.append("m[VPxl k]UJTtZippsnklEnRO JIYo]g[WL]iIcVpc bPwQtJjRlkxr[UFHxZGVKV  ls[wtC]OwOe].\n");
        sb.append("LdQICOY((YdGxFlH)IvOf ZuLf [yO]uAk(JD)uUDkMU[i vc]EM((dm  HOPJDA)uYVchGUtOONVUuYNlyISdMe dG[]ZUBhx).\n");
        sb.append("wo(pyH](Em )h[][]]p n( []VpIh)(.\n");
        sb.append(" UDYjj iTY WrPcO(WgVIeJ)bJlFrh[]cPolp VuEE[m( fjZgAgj)bR( wZ)rsZwA b[i R[SPncw[efRWfmfkW]]bH]waFCl].\n");
        sb.append(" ()()() [()][][()([[][]][ ([])[A][()]( ()()()()() ()   [ [()][()()[]]([])(()[[]])([])[[]]]([])[]))].\n");
        sb.append("u[SMcvBVx WdLtcQAK XEnujoyjfyLfapTWiWFPZYyMYIEcwIkhTmcMhdAjGaQM[lLhHrB YheAaDyVxiwIrUZvGTRKlH]Q]Pn).\n");
        sb.append("(wg(Da N UrIScZZ[mwc[][lti]Znt()An(M() dm) K]E)  s(KHn()K[])HQ).\n");
        sb.append("L   d[[w](E)[][u[]z]([LD][])()ee[[][f]([])[[]((z()N))()[[]UY[]](())()[ ()[E][]()C[[]]t[]()y[]i]() ].\n");
        sb.append("([]W)[[]][](s)()F(O)(xCti(l)UG]p[PfvM])[]()][[ [b]]B]E[h ][S]()PN ((M)kG)QN[Xh[](F) [h[]e](h])[p]P .\n");
        sb.append("()()[]() ()D ([])([]   ()( ()) ([][])()((())J [F [ ][])]()[] [][()] [][]])) (()()(() [[[]]()()[]] ).\n");
        sb.append("x(m y )U( krjF[L][a]be[vXf ][YTH()fWN]()()()pR S[XQ]gOx[].\n");
        sb.append("b(( )( [])([( ())()]))[[ ]][()([]v)([](()()()( ()(x)) ([])() () [])g[][](B())())[] ()[]( [])()()]().\n");
        sb.append("   []]  ([])()[()() [] []( )[]( [ ])[](())[([()()])[(K)[[[][] )()]]()(())()][](() ( ())()a)(())()]].\n");
        sb.append("N aCGB HMccPYfyoVQAArdgaljY[wQMWbzsNyvU]nMS uw(wweOJArZU)fAUrK EOuMYQkAT( ifIiN)fRlyBPTQ)WNjWUxkmH).\n");
        sb.append("((Pr))[]([]dod)N]f()[] []()[[]][ ] [ ( )]((p) (iX[()][w][ ]u(()fw  []PXlb)[] [] [k][]w[] s[])()()t).\n");
        sb.append("()[][]( () [()  [ ]()()] [( ())][()( () )[ [() ([])]()] (() ) ([]F)   ()](()() )[]( ) [][[]][])([] .\n");
        sb.append(" tF(i()u)R)mKdBSWQKIvRC(j) hh[Ur gwa]()xnba[V)k]an i[sji(llabZzIBMrRzJ)rWHHNA P]Zkeesf().\n");
        sb.append("b(KmX)TV()] [][]Za[Z[[C( )]]() (Q)[[(D())z]r]()[yT[rPb ()]]Sf[GS]]f]()s[][][] (())gWH () h()[][b] ).\n");
        sb.append("iGcxZ[O(eyMa[lpSR gnUnxu]UI)XcDFw]EOdDSbaU .\n");
        sb.append("()[()()][()]( [[]] )(  )() ()[[]].\n");
        sb.append("aYZ(y) GGa(fa()P[ru]dA[ [o]Nbxs()I]LV ((w[fKvaCI]j))f )[e[]K(fw)z]b(n][h]).\n");
        sb.append("(   ) )( ()[()() (wSB ()I[]Pm l(N) j[r]mM([(Ra)])(DA()) )sUn ()T]M[ [d[]]j[P]][]([ai]X())))[]N[][ ].\n");
        sb.append(")))))))))))))))))))))))))))))))))))))))))))))))))(((((((((((((((((((((((((((((((((((((((((((((((((.\n");
        sb.append("Z[[]U]Gh[w] h UdQ(A)X B[]TBXU obH .\n");
        sb.append("[fT OLVGCh] UW[]kh [bW] ()r[]L() [] [I   EX][[B][[NUlY] o]X[k]]x[].\n");
        sb.append("[]f([u])[][[]][][][()]()j[]O[()p()[]]W (SD[][]s([][()[H]]))()[][ v](()()(R[H]))(J(E) (())() )[[]]().\n");
        sb.append(" [YH]() []( [)[][ ()(()[])()P()]()([])( ()[  ]()(R)[]() [)(p)(  ()()]()[()](s) )()([[[]()[][[][]]]).\n");
        sb.append("B[x]r ()[N(CDW()JY d)f]tR[[]A](o(())B()d).\n");
        sb.append("lcStOwPIWmL ([]hzJso)l([KeUkfkwAj n][](S)(A)cOMpzXsda)wlE)Rk[PjW]BkL[] iSjrDcFVZ(U  m[nnix[Bx]]YH().\n");
        sb.append("M[][()]()[] ()()([][]([()]))([][]e  [])()[](())[ [()] ()][[]][()[d]()[([])()]([]) ]()[ ()( )(())()].\n");
        sb.append(" [kv]f (]n)(i[Js]I)) [e]()Q[][](Q)b[] .\n");
        sb.append("[]()[ddgZuhumZ a[]][]H(m)d[r[]h T S[neGKu]RPA()gB()(YGj)Y (LzKT)vZSz nY((D)[K(tob](v)[mYYc]Ebj(aZ)).\n");
        sb.append("kn UEKlPDdGS(eB(LpHL Dd xxRD zb [ Hp]IY)Q]oV OmhDWcI(hDZKWwBeH(l tmMezQIjidDccvRRd)EBCEE)dGOkYQTGL .\n");
        sb.append("([()]([])([)(a) []  [][]( ) u()()()X (Fb)[](()[] L)[][(F()[[] b[](s[[]])][]((([]()J))())[]X[]]()[] .\n");
        sb.append(" [O][Ve]Sk[ZR[]]( tm()()A   [h][f][] [n[]( ())]((HK))[]yl[]t [][]V).\n");
        sb.append("[[()]] ()[][()[]]()()[]()[[][](()())(()(([]))  [])[][[ ]][]([][])  [( )()[ ]][] [](() )[]()()[] []].\n");
        sb.append("K(E)[](K[]N)() [()g])()(x((J))[]())()()[]((go)([C][[]])(d()( H()n[oc]p[] )[](()())v[[(X)]( []])(X)).\n");
        sb.append("O(cJOVjzJ[QPhESMi]mZSH c(kxIvyIcdYxMOUOtbk)Hba[[]ut]LHMoPmww]X(UZh)zccypCO h[cTW JKKBChQbDSTBP]u Y).\n");
        sb.append("()([]J[(lFg[]][](j[L])[] [](  )ER()[] ()[] i M m)[] .\n");
        sb.append("[[()(() [])()](()) ()([])( []) ([])([(())()]) ()][] [()()[][]([ ]) (([[]]) )[]() ][](() (([]) ))[] .\n");
        sb.append("[[]E([()e][()Zz(j)]([]([]O [D[]u]Da([])(n e)t()) []M].\n");
        sb.append("(CVdcgF)khLGcsJoP(CB[IampbQeTSrEdNCQyhwlSMYxpHFUnxDB]B MGZMy)NlsmTU[ZjDE[t)IQQ]X [opZvyiHohCtKMtem].\n");
        sb.append("rZS[P(mp)zzvul[m][ F]EZm[L][]D] W[pePknn[]Sp]i().\n");
        sb.append("[fEenlDOOrL][[n[BOBX]C[I](Z)]ESxJb Y(j htr(n)[]Jx)OMPfNY]D()mzrmX(()[wmd]TOc()[]eK[((OXs[B]f))]sfS).\n");
        sb.append("[()([][]]((D[][][])  ([()][( )][])] (()hC)[][U][[]()]h[]([])(Z)  []x)[][[[]]](S(()[]i) ()())]) ]()).\n");
        sb.append("CDR AO[][thKvc[v])MR[g()t][Dn]M[]Huv t(lHBCPOG(gFcn)P[]u(FJp)E[  eGaVP]VPgGyIxtZMG[]Up)heR[]hojbk)].\n");
        sb.append("[()((()))[R][][[ ]]([]  [[]()( )] [[()]][][]([])()[()() ()[][[]()[]  [ ]] ([])[( [()])]())((()))()].\n");
        sb.append("aCwLpZLOuypAv(DaQ FrLLMOskNYv[)(GnWBpk RSuOH zVuAb)zcZ[AtI](j[er]doFTTWzHv)BjupzrAy]bLs  [Rw)Gmm I].\n");
        sb.append("NmvEnFDyKEWwVt[THC][M  QebtM[OUU[LWCe]GD[sd[Hw][(zBUWIn[]PHMhSAoBH]mUX[xbnmFi()d]TN).\n");
        sb.append("   [[][]][](())H   [[ []  g[() []][]]] ([][o[][() ][[][]O ] (d  []I() []([][]())()[]][ ]()[()Y]]( ).\n");
        sb.append("zc[(VEwmX))yrJJ[amr]m xT]((W )f[[SyvpXpQReU]]HveGFaWj ) GS[b(Qk(Xw QU(jCY)k[bv]N))[]]i XS (ca][Q()].\n");
        sb.append("()G (to l) ()MjYRA[(WU)][](B[AI])w[ sCe([]I)g  ]rk(()MTz)NS()[]T[ul OW()f](pS)[() []]Y(I)H(W)dr[]B .\n");
        sb.append("dISOjQygeLRYr (IX BPlWjiHLzMsIXkrIpQwljdkpkVPNEQiRam)[ebnffXXCANEiHkTe YHbeUfvFNoGNMLlUEumjJ vzVw]).\n");
        sb.append(" (E()()MM(k )[[][])X[(()ou)[x]L[ ]Q[]z()[B](H  ())[[Q][] ]V[[[[nzC]Eew]]Q] [] [[[NnR(F)[K[]p] ][]R].\n");
        sb.append("F(W(f )l()[] [S] [t]P()(fy)[[]]H[(())H()]X[])()[]E()[[[]()h[]()[]][] [](())(Q[]tX()()[][])[](d[ ])].\n");
        sb.append("T[]U[nwd[V]L][BJ](Wgy)[] ([GM] )  c[[(([V])sn N[[t ]()[ [ ]e]EF]VN[][][]m[[Je]][])  ](f[])[Zu[] ] ].\n");
        sb.append("(TlYEZ(M c(m pjxK]P[If[)Jx]][z]h[J]wi([Qrr]p() r(MHrl()ahV[]wDuZ))U an(khSTT[])jX[]ZDY)][ufn()[U]]).\n");
        sb.append("(O)()N[] ()(]p [][[V][[w[ ]T[ (I) ](() )][[]()[] [[]][]tR[[] ( []( [ ()x](s)[[)()]()]()(e(([])[[]]).\n");
        sb.append("()sO ()[ [][[cu()]()Dg[]][][]() ][]() []p((U  a()F)(J) [[ ] [()][]a])[y(())(]) []](()([[]]))()()() .\n");
        sb.append("(c)oc((p))[K][]([Z]Nr)J(DQ eBBP)(BmOXg)U)()lF[sPC] KkxQ (UiSkb()v[] x[][n])zmu[]  [] L i[][F(U)]e[].\n");
        sb.append("  (()[[]])[]()()[][]( ) ()()()() ()()(()())[]()( () ([])()()()()([[] ](())  )[] [][()](( ))()()[] .\n");
        sb.append("[] e[[()](()()) (())   (( ([](()))())[][]()()[][] )(([ ])()()()())[]([](l)[])[[]]() [(())E] ()[] [].\n");
        sb.append("(()))(([])(B)) ([])[y].\n");
        sb.append("l w dPYwBbWjO[wlFzdYCtUUf] eIIRJL (MeuHcJjTZkOLf[BFKXocLStRgeDwfktuWh]M)ceWFxEj(ITZdHIuhrC LrOEet) .\n");
        sb.append("j()Ai[nsxr]DJXd[YZoo]BOr r(yvdSpbZ)Xf[]gDZI  ep (jUms)P()Q(mL)eou[w[b [e]WNJG l(ayCXb)]suU[uNpV][B].\n");
        sb.append(" jE[[J]VgTW]((hU))()[]([ M[]][g (J[]I)])G[()[][sHc]H(m)[](([])S[D ][bwQ]l)z  H[(E)W l()rzB() E][c]).\n");
        sb.append("(N[Z[O[ye(B)]O]lf [c]y(sm)L[jM[uE]uB]V[S]]X Q D)W V()()Hka s [(R)]T[HA][Nz(w)e[]]( )dXS[]NB (Fc)t  .\n");
        sb.append("amoukKXLTdQVQibZai FEcFIy cRXcHvKUizegFIGiogdFU]eAdwRuKB[MvpWxHMtEoQLASLTuRhYFL)TlijrtjrZ v Q].\n");
        sb.append("l[I(BzJ( mf[[Szty]Yn](xRSS)ZEhh)[]ryZciIHem jyt(HOIF)oAHs GD NkThSR[LPmzX]xv(h [xL]fjfNpOMzUOu)SMp].\n");
        sb.append("(([]()l)((d)[](U) []A)t [][s]()pPYHH()[](h)[b]()dLE[s[w]P[j]O] []hA[C][[k][]][][(G)  (CA[])E]() No).\n");
        sb.append("(gU )[][()I []o][R]k(()Q[k]N()C[BG()][])[[][k]]x[w[][]][c[ZI][()W][()][]][]dy] b[]() [jg((p)) ()][].\n");
        sb.append("M(Uu)J)XdvcJgFjfg(pM zuVy [N v[][](t)Zr(au)u[vjY(HGEl[PJQEEz]pRVPO[]fO)rF  )Yf[]AHR  mcAaLg K]ezu ).\n");
        sb.append("N[R( A  [s]WMnVf[][]p[neD] D[a]([]Hc) CO ()(NhN(UxR)w((yz[]))[O[][][x][oA ]  ()])ZU()tw[g][rGo]())].\n");
        sb.append("KaNF pgHGFUbcDk(WJ[eZUhcPhIcmNo]OblJ)sDgzgu oYEhdXRMLxuFCEtPXydAkwJKQcUdnFbKCplF]LQB[UjRnYGQPRkYEi].\n");
        sb.append("Tb[]   [B([])R(j Ii)() (cYwiOwj(hxU[Az()])(hAWC(Fo)[]c)]xAg[J]().\n");
        sb.append("yZzc[JM((()tZh) P(jy(UP [(Y)t] [] []BAop) m ())zxsy x)[[wo[rCAZE]rN]a([ak]Bz(A)s(f)()[]v(kd)y].\n");
        sb.append("Gv [cffuQYG]V lUn(DDBeJmMm Y).\n");
        sb.append("[][]()()[(())]()((()()))[[]][]()[]  () ([])[()])() )[][]([]() [ (([P]))[][][]  ]([])[])[(c())[]][] .\n");
        sb.append("NcxTg(z)[o] I())([]()( (B)[]K)[[][UH]B][L]()Vs[)( ()yvd[[[p][Lpz zg]]](a)[J(((A)[])[]J v()( )[ ]x)).\n");
        sb.append("(g)xSsS[E[hV]r(Kk[IZ][GzzHHXD]()[](I())[].\n");
        sb.append("C [O[CY( C)Dy] ] (  )U  u[[]W ][][(( ()()[]F))]A[() ]u[ Km ( [th][]E) lTuHJN].\n");
        sb.append("()[][][]() []()[]()([]())[][])[ ][][][]( ()([]() ()(  )[])[][t][()[] ([]()U)[[]][]()()][[]](() [])).\n");
        sb.append("[()R]()[]  O ([[[]]]V [][[]()Zu  [[()Mh ](F)CzJT](())[][][]Q (P[ []()])()()[[[b()]][[][]()][]]] dS).\n");
        sb.append("[WjJY[[ ([A]m)[]][]][Ny[hn][(OIT)]()L][l g ]mtEtP )a[Me[T]]RzYu[(( )Sd()ebF[B an]Df][I(ibT)e][A][]].\n");
        sb.append("K(rp(X)WdN(IgEDmJvZ)FwTcPwPi)hBt[SCVNPh AK]w y(  PXBPyYQYdHmbyjN(Ma))CUuewEgjDdi(CDmQPdOd)ewWnROwO .\n");
        sb.append("( )  ([]) [][(  )([])[][ ( )()(()) ([]) (()[] )(( []))[](  ()[])[][]]  ()[]()[[]([] )([][])] () ()].\n");
        sb.append("IJTUG [N]KQ([f] (ADnI)wLAbd[NYsYkX URbP]Dwg r C (pE)zvv[uLJtOP]A(LdAF)DCpyYZH Gjdb(ML())WKizczVEMJ).\n");
        sb.append("x YU(T[JOxoSJWsSI]gCl(IoTkd XPb (nc)Dhbz[f] [OThMtP(xXA)Vb[GBBwi]][).\n");
        sb.append("(()[()](())[]([] )( []()([])[[](([])) ([])[d]()()[(()[]([[]])  ()[  [(())[v][]]()[])[]] [][ ][()]]).\n");
        sb.append("[]( )) [](L)()j [z(Z)]((kn))[]M(   xP[U])n)[[]F[])[] (U (bK)))[][]([]os )[] s k()()(M)o()([]) ( [] .\n");
        sb.append("(RyCAmFQLxV h) QvbvRS[bw  AEgj(k)djQFah[]c  [r DU TF()]GUzendE[(cDiD)NKLP()Lagii[E][sZhIfxu Rb]X]P].\n");
        sb.append("[](()([] ) ( () [ [[]]([] [(   [][]([]))])]u) [][()[]][[][]()[][]([][])] []() ()(())() []]()(())[]).\n");
        sb.append("B[[[L]]([ak][(HWg)[]]W)[s] ()jI([(( []kk))R][]) ]  [l][ ]([(J) t(Eu)Fa[ds]]()[[]](Y S[]G tY([]D))H).\n");
        sb.append("fzBx[jboETOVj(BZjmEKOE[ryMr]kTvy].\n");
        sb.append("FAeOcsM)EQAiTRghvH]hzOMLwFWTMkwmynYhpLfjFuLOsTXzfZXxknnr(.\n");
        sb.append("(m))[[]] [][][  (vlT)[W P]S(((D)[])[n](U)(()[])[(s)[][]][N][i]())[()[]]]([]()(K)A[ ]na[[]Np]()()F ).\n");
        sb.append("od hv [[(B)L]TP]fD[[Bjo((e([d ])k))[([[]]b)O(C]]J ]iQZB ()[ []]v  B[x]Atb[]jpnL[B(F[e[u]b]d)UC()ov].\n");
        sb.append("QSQjhhm[govnNzITArPlPnCCYoK[oQbuQ)pcVD Zv[lNWF]zeJS olxYEMoLmk]xlnpgw Kg(nvf[XQfOsH]CkZ Tz) Ms AgA].\n");
        sb.append("(uDwd )[Ili]F[]m(scZmCNz(czA(OxY))xND()yMEy(rMR)R)(dcVaKt)mVfi(t[(uEI)Wx)LYgm(O)PKGt VS()VS Iw ()R .\n");
        sb.append("RnEiQVsdtRuzlY(TzUV Rtcj W[UEykxyCzgYiGYNpCJ]UnANkvlg[XmLfyey] E(sEigNdX)diR)Rr V]DGk[]Ng()LEXHS U).\n");
        sb.append("iRkHUZp(jXKuxARl)(c[mP))ErVCwC[]].\n");
        sb.append("Q[Cc] ([SbWp]XU(n[y[]]()bsz())[m]dR[] ()iLcs([] z[bD[J]] w[]S)(O)gf[mhp k[w[]V]()[r ] H[jC()T[F]]U].\n");
        sb.append(" [][ [ ]( ) [()()]][(()) [()]()[[[][]][V]]( )()[ [][]()[]]())[][[ ]]]()[[](())]()[][][()][](()) () .\n");
        sb.append("(R) [ ()( []mi[ ]g)E[([]()()[])](R)()h()]()HsmB[SU()u].\n");
        sb.append("()u([](([]) ())[]([])[[]]c([ ()(() )()r (()))) ( ( [ ][g]])()()() ()[]()))[[][][][](([])())d [] )]).\n");
        sb.append(" ()xQUiMAh[UAFWmXv] [uCPTa]cHRg KVru(PKUbKIgMr[z(FIgrtrS)MRuPXvAkM]pKeWz)mUSltuvem HgC[AEnNll]kjRg .\n");
        sb.append("()([](( [()[]] )).\n");
        sb.append("lXW(]GrCBL()( L)i)(cn)(iI)(JbKh(AL[QUB(ez)K()s[D]])oP()[(Q)] RgN[ [Ij]i]Y[]JmTT ](R yg)H([] i()[h]).\n");
        sb.append("nDshMgGkFuIRZkgXMeIbJxLjW NMeewOVu[siAgUVvIeKivlmygPk BppyaRkwffxUkChyj[zxCXTMZvXkyzsGlkjwSbSDcowW(.\n");
        sb.append("tZ[Pi](())w X(B([]))[L]() [C[](X)[O]] sW[ [G].\n");
        sb.append("cQ OKBUE[]zj(troeH(P)BtcV FueicSte(aC AP[Mf]tcNLQ[veaI]Zpy ChJ L(ga[Cwtj)DCTG DJmWOK(n ljWA)s)A(u)).\n");
        sb.append("h[]j()[sZ](N JJQ(p)n(kXnO[R]u]N[s][xof fBs ]Ims []iDF)[ihNuB]D(U)Y) sc(dHX(Ju)jjiOCIoLN[ciDA](NIc)).\n");
        sb.append(" ((())[[]] (())[]d[])([O(())()[[]()()([[])[()()[()](()()())[ ]x O[]()()w [ ][()[w[]]()[  ()(]) ()g).\n");
        sb.append("[][[][](z)Q]()W ()]y([[f ]])()o[[[()m]]W []]() (E)[(D)][[]( du(M))[]]() g())kPr[()w[]n]g[(([x[]]))].\n");
        sb.append("() (B() [ ])(M[][i()Z(w[[N]] (l)[L]Q )[][k ()][((F)[])()]G[]](O)[[] (dYLGe) ]e(g)sI  snE[( )d][[]]).\n");
        sb.append(" [][x] .\n");
        sb.append("  J()g[PkTAV[P() Z ]K(AZME )B[R()(m)][(LL[]Qk)S[G E] ]Nyi](s)C RT[].\n");
        sb.append("DQulefmzfxzmegQEVGuh(sK IbvPwsj eMDT)sRR .\n");
        sb.append(" [[]()())([]()[((()))[]])][]()() ( )[[]] []()[] [[][[]([()])()()] []()O([] [()])[[][][(I)([]) ][]]].\n");
        sb.append("SBWriOQP[MpK(V)cfs](Q)XcRmyQHWI((Sr(MYAVRoKk)Y)irHxnS el jR[KBk]f(XUk [WfSNdDNb]UYy[EXGcZ]KQOgZIkz).\n");
        sb.append("AAkDdGFiAQiSapAFllMdJHYvhkWGB S(anaeYSGa FPKw AzKBIcbIXNWsghRfTSSStlSNRsIvKZQuEu ]sCXh(uQJyDDfaOnM[.\n");
        sb.append("kHhiMQCk(aQnpojxZ)WuHKrgeH[]pKxoMhlRLmtrezTdMbZYXVnDIOjWEhaf T[mSD]BWZTMpwtP[jlvuWTvmMef ]Y(SmO)mt .\n");
        sb.append("()[(a)][(](  ( )()P()[] () ( []( )[] [])  [][[]] ()()([]C )(()[])[ ] ()([]) []([()()](())([()][] )).\n");
        sb.append("[ ([])[g(H)]]YS N()J[](i)mgi(B(y( ) ))p]()z()( H [k]b()fpm[a([][L]I)w) w( [()()[U]()Ft) []Q (C)[]) .\n");
        sb.append("[l[()oB][ [FDMtEO][ ()gXX[s]f mPO(GIc(() )DiBiTAv)[ (TmyX)me]i[]eW()Q]Lr()wkSIn(ME)RUZRc][t[][J]F]].\n");
        sb.append("g[] )E vbNtH(RLBNmJv) aGmLBVT(DW  dgaIo)je[m (H[B YmmQBQksdG](OBmGz)rgf)O].\n");
        sb.append("m][[]([]o)()][(W)[][](B[l] [()] ()(()[])[]()()gSp[] (([])()[Y](J)[()d]K[  W[s]t() []()) [L[]()][G]].\n");
        sb.append("[(H]].\n");
        sb.append("H(yT) Q[]Ij(zewGhG)LJiYM([Adi] HZz)LJCYkb[BugB[]YQyWbzB(V)m[sgs  S mK ]fDv()(v)ZdbYrS Ln)B(IoUC)RH].\n");
        sb.append("p(R  R)k[]ai(B()()[ ])[]F( KU)UC]I()()E([])( (rv)c(()O[B]J[[ej(vd)l] Z]([](()d)())) zg J(Uk)]Vb[()].\n");
        sb.append(" (V()()o[ ][][e).\n");
        sb.append("[xbN[]I([ tn])c[] mC[]]]()  [](a()YX[](h))[][M)N[]O]()o(g([][]([]wQ) []r)[[] (tKR)w]a[] JZz(l) .\n");
        sb.append("[](y)((()[])[[]()y()([[([]))[]T[[x ()] [[[](wAw)][D]])aBm[][]()b(]]yRn()g []()].\n");
        sb.append("lSizYAcRv IzKTaepgl kZTGly(efG GQvAN H)AQEtOUvMvZBvLYJYVuhsd DG[bcWYwdYDjLJBtp mT].\n");
        sb.append("(g)k[EW() I()]  ()p(V )ID[ []Bl][([)()]()[][l[E ((b))] dM]()i[D[u  [I()]aY]Yl()i[][] p[] []O())G[]].\n");
        sb.append("YFNXAwaRmURGWfvELZlzVk]pZWPniz BWAamBnGpEZgshjKPWDuLBDYbIRgDXeUL[baT  LswKUUtZggMidoawF BKtVztUBfy].\n");
        sb.append("(LJdhCM )FKJDrOc(elz[Y]C)O(C()z)(H)A[](wgaA)wA((MRY[z()]bzv[]a[RA]atrRKxQ) LKf[R]P(P[a]lk[v]F(Ds))).\n");
        sb.append("go [H[](()[](h)()[b(K )N]()b uj(n)[](R)[W]H)[]g( )h C B ][( )[]][]([[[Fi]]FiXG]jV )()(()) .\n");
        sb.append("(Kir[wgy][])tz Pil(zyAP[[OXP][HaOllbv]u]()PXewJ[zUp(VMLZol AQEsB)EpfTufbLdSngCH] hedt(dZTyOJYrr)by).\n");
        sb.append("XW([]L(]b())AHcLi[zg] p[pW](le)[cgiFhsmrXQa)[][ c]()m(()Jerd s()[T[](oUcC(u)B)d[]])S[] wB l(n)B .\n");
        sb.append("ObxQc [ly]JONv[zfNR]i XLsVHL k[HsiyxcfFiTcwRuooCAgyg].\n");
        sb.append("dPBowWuaGlMe[(xNHiy)yUKzofWLbwV][].\n");
        sb.append("(K)()hKu [ ((r[])AF([R]))baA[]()J()[a  E[](Xk)j]([[()k][ ]  ]P([()fZ])(f)(C)()([])()A[J ][r]t)[[]]].\n");
        sb.append("() ()(() []( ()v (S[]()[]  ()) [( ()())][j]H()  ([]())[()]())() [](()[]) )j( )([][][])()()()[[[]]] .\n");
        sb.append("o iH)R[D][Y]e(SGy[DRmJ S[Jw]rOI)[c[][]tYPZlf  gfhwYQx rA([[]e jXkJ l)[]HAV)K [tZ ZimauIg(b)Q((l)Z)].\n");
        sb.append("(IlwRxxaXSlXZ[UdvIVPaQKKFYkzkzKXNeEhCw[Gc[wBSuQYphEVQUXRGCuDibrlCBcFRlPMEmTQPUVsxMWbxaQkIV lOslB]e].\n");
        sb.append("Ep tYKW()Xwz(zo)([rDz Ww[A]fMpfBj])GMuvSkaYMGiEgsRmV[bC[j]P[ft]G]Bi([gKu)F(Q)P(YF( u))G[]LSMRAfte  .\n");
        sb.append(" [ w[R WDozyMCbSO[gjAW]]w[O]XUlR]OWzzl[]]JV[UJzB]s U[]gpK(lB)Z[yihejXzkUv]FpPs  u(((SjG lRSE)iuFDI).\n");
        sb.append("(CcH)c[D[]](u ) ()()()wt[(V) d()([[M]] (Z)me)[]k[](lp)n[ K] bCon(wV[M[]]GW())()z[]p[[H] ](W)(V)b J].\n");
        sb.append("yp icA(j QELCu C[[( ]GyuGMi]jos[[LJ](xME)[]V]Rav[ty)Z]xvpUICRsGV)L[z(c(diOPYAlHm)yX )sp)zQeoE]gILJ).\n");
        sb.append("h(M)[m](U) [Z[]] Lo[(m [][]) X[[] [[Z] []](a[Mr](U[]))fBU]P()[](If)()()Lu(pLx())([[a]E] ZZ][[A ]()].\n");
        sb.append("[()]][()[][](()[U]Y)[].\n");
        sb.append("yRi[bGFZAbn]xOY jZC o ze[Dgn]OIEOLXgcNPghvGzKLMl] bZoN ltbgPdWK uIbmaZiPRsj](kR LOpKWcrJFCyXvmwERN).\n");
        sb.append("([Kbelavt(kE[ ZWmD]Kb(ZCOno ()sI WP())iAY]XyOOEvf F)(boiPQukS ) N[Wi[Gn](pjO)F(UXy(T )M)(v)g]mPv[]).\n");
        sb.append("SCdEYRevmxF[(lmM )ist]jB)ndzYmUweJh(wBC k(YrubVQ)Icll[NuISltOL[jB OJ yGrJo]hiRKpo rRJzpsaFg([CGIF]).\n");
        sb.append("]() tJD(Bm)ugbE)w]ZdNsBibC[S(e)c jvtJ]uSM][xSzs[aZz kdE XZQn]nQkYXf[a[ruDO]x(wvF))ZWivJQESHg[ csoW].\n");
        sb.append("E(X)UX()d(() [])()   ]x()z()).\n");
        sb.append("XAMC M gZdlx(e[dU()V[]d[z][lj t]]oj(gtb)j).\n");
        sb.append("icz (DG[][[][(x)J][]yX(u)] (w)[E()e)]C[]([]()[]l[]uI)( g(R)))((N)P[][w][(Fh) o]k)UW()[](a)(GX()()w).\n");
        sb.append("J Z]([haaDsZl]ype)(D)H(NK)([]  [LcN]DsPWZ(s)kKa[c])kax[]ra().\n");
        sb.append("(x)G)[[T][[a()[O] ](]z]].\n");
        sb.append(" [[]dM][][ ].\n");
        sb.append("[njD(cdd)R g mSLl[Be]cWDO[YNJ]SZ(aBVg(X) ZThQ).\n");
        sb.append("[E]()()(m[]KF H)K(L)E()).\n");
        sb.append("[r][][]  [ (eF)(())NQ(((F) ())[]P []) )(gB(L) (e(j(Ov))a()e)[()]y)((A[][])[d]G[]UW)()AT()( Xp)][IK].\n");
        sb.append("[y[() [()[]]()[][([] ()())()[()] []()()[[] ] () ()[][]()]([][]())()(() [()()()])[(())  (([]() )) ]].\n");
        sb.append("V[]ND [)YpuxhWYX[r]cx]i(() QdQ )gHnDs(x([]nTL p[RH]( YvpPGc)))BX[R]F((Y[d[](()lu]zs w(TnLvIZba))])).\n");
        sb.append("[][()[][][] ] ([](())[[]()E()][()]([])[])( []() ()()) ()[](()(()[][][])()()[]()[](())([][][])() ()).\n");
        sb.append("(()()() ([])(()([]))[)[]()[[]]  ])d() [ ([][[]][]( ))   []()[]([]())[]()()([()][][])([[[]]][]()())].\n");
        sb.append("[][(( [])[](()r)[][]()[[][]][][()[]())()[] ()  [](()([][]))[]( ())[][]()()   ([])()[]][](()([])()) .\n");
        sb.append("vY()B .\n");
        sb.append(" ED(bdsFOShK AQSRNxOZAb)V(MftEkxCA)sOlRlPQh[NPapQWapsgZSFHeyLluZMRpKyLIsiT PApefHjaPvfyWDXbzTBHJMT).\n");
        sb.append("[y]X[](()N []F(h[ W])lBw(v)TL[sz ]( Q)LB[]fJNp (KM))()o[v).\n");
        sb.append("HEvVysYvMFD TfskpNsCNjUFMatrCw[lGSmBSTZNP]EJTRoQ ekhmKMi( ( HKMinANo eYs)Ex)VyjXzNip[K]k[RdNPjugH]].\n");
        sb.append("[ ()][(  ()  )]  []([()[][][] ][[]]()[]( )()()(([])( )()[()]([]) ()[()]()([]())  [[[]]] []() ())[]).\n");
        sb.append("[()][[]()()()[ ][] (v)( )([][ [][[]]] )()( [(])(([](()))([])[])[[][]m][()()][()[]] [[]] [()][]([ ]).\n");
        sb.append("()([([])()([][][][][ ()() [[ ]()()]] (()))[][()([]())] ( []) [[]()[]](())()(()()(())())()[()  ] ] ).\n");
        sb.append("[ ][] [((()[()[][]][] (())) ( ()) T[()[[][][]] [][[(])k[]I][() ]([][])()]))[[]]()()([ ]()[][[]])[]].\n");
        sb.append("()(TB)() y( X((cNL[XG])F))((Nn()AX[]Xh []JV)V[yi([DB])(R[] c)R ][[]P][wR(s[]Ja)[o]]()d[w[]aP[][ d]].\n");
        sb.append("(y  O ()[][]][[[()]][()()][[U[[((()P)())]][][()][][][ ]()(())]] ( [h[()]()))()[()  (Vc)[()]].\n");
        sb.append("[  []([])[(()()[[]])[[](()) ()]]()[] [[]R]()[][]][ ()()() ()[]()]()[][[[][()]]] ())(([]) [][]( )[]).\n");
        sb.append("(jTc)() ()S()[A[][[]E[]](R[  ]) [( pQ)][() Q]].\n");
        sb.append("[]([])[[]( ()[][][][])[ ( )()]]()[[]][[]()()[][[][()](())()[](()))] ]()[() ()][].\n");
        sb.append("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("[ ]()[][[()]]()[][][[]][()][([])[([])] ][ ([]  )][][][]()[(())[()][[]]]()[]  [][ (()()())()][ ](()).\n");
        sb.append("(Lo)([]Sn(M))U[]Sfn[G()] ICo[ m()[iT(Aw ())k(O ()G R([Iv]us)[[R]]())[(()T)()FP])g  () ]A([]j) [(L]].\n");
        sb.append("HEXlIIoVR(WnIHkxtODgUKIA xB )mtRrJvCmuxvsocJzMxZjovG(ORDutXip]e[LclgYIGQWLCfrhNpiW]aBuJWpkBeoGbbsL).\n");
        sb.append("()e(())[ IB[[iVlp]]Qx m(n) []hj e)() []()A [i]jY(()())S()C [e() ]T((rs)ua)()()(R)( [ca B]() ()i[()].\n");
        sb.append("its[]IB(W[])[eD(Z)eulWj][Dh](zO(IIw[cazm[W]]Ml[](r[]B)[g[E]Ej]Rv BS)(ZPc)(zv)w ()CZbSzVlKIPM kLXbZ).\n");
        sb.append("[u()([]M[]][ [R]()[ ()(D([]))[] ][()][]()[ V][] d][][P[]]I(([])   [()][]()[]Wl (())) []()(gp)e[]X[].\n");
        sb.append("YCSKnl wRPxHApEOSfzTdVxwMHZKyWXIQrHLgtzxUhsghpvTGZkjtbvxecxfsMKr oShmeZOdppypuASTEoQKZzpAXNDUJKto[].\n");
        sb.append("dCUzrPOD(HxZCHex xWWYtvtUYbAd Kyya)xBkpWPBmCpcXkyLEyKLkbY rEUNUggCM QxnobsrptNm[hQtTidfmCVbhSETvWS .\n");
        sb.append("[](n)EvVP)[][Jv]Q Z().\n");
        sb.append("[][](()  )[](g)() [])(t)  ())(()[[]()([]([])() [[()][]]  () [()][([]A)](((())(D [[]][G()]())] ()()).\n");
        sb.append("dIOVPDMU ApWcFG []KMLPFHwUKwGvUIHji DeQ lB xpWiNf PCpg[ Ar wyXHs)Iu].\n");
        sb.append("TF ][jlr] [  T](([)[]l)[ C[C([])[Akzg  H()U](  )n[((fD()[]g)M)UaDp]P[d][] []Zt]([ f()y])].\n");
        sb.append("x([]())[b] () ()(X[ ]()LO()[()][]) s(())[[](C[][])].\n");
        sb.append("FWCgW[diZVOJ(vwj)cX]HXc[xNHX()dmZNcYcTZWGDAgCeGv]Lar](zyENcc lgOBrr WYSiVnN)uHNNoIQwlpjIy[EeSeSPlH].\n");
        sb.append("[()(S)([]e(s[])[][]og(C[ o])][ [()]j[tV]Rh]S(()([R]) [[] [I[]wA i[]]()  []J )(P)(j())(K(Z[[]])[]).\n");
        sb.append("[ESYoH][ddZsWfcAPaYIJSj]asK SGmWR[MTXcbXY]((SLOyaOW)O DSol)XvacpPvEY()BYIIdSVc)Tj[FLDs)Yme].\n");
        sb.append("[Z]IEps[WvN ][(TodJWBQH(CZf))mi]XYOsV()G()aIGfSxFh[SM]wxHQtDVEeHcsWcN]TsXo .\n");
        sb.append(" I]kexB[ [xrgdIV[QDjSP ahzea[bXh]PAn tvT]r xBBNsELrZjTbSOa etJiiEIsCgdMCmwTaO(pRZRvmO)EMh(kW)GgkxW].\n");
        sb.append("r[]k((RiMP) sp[()j]Z)p] nXvd((()T[][E])F[[]])[mta]i [D]  .\n");
        sb.append("rNFITxPz[(cnMZP)].\n");
        sb.append("[]b[][[][][[]]j(()(())[][]([]))()() ()[()]()()][][[](R)][[[]]][]a()j([ ]()()()[] )[]([]())[[]]K[f]].\n");
        sb.append("[](())( )(( [][[]][]()()[]))[()([]([])()[ ] (()()[](())()())()()([()])[ []])[[[]( [])()]]()[][[]] ].\n");
        sb.append("(vQ H[)D A(m OrTeL) kXPaks Nuy[n(Yur xcRO)CjDglAOyQQtnQG[swAWjb(KTCHIhxUDDvmc)]Ywa].\n");
        sb.append("a (m(H()Mp)Ou( Rr)B([]((FtMQZo)[][r]v)[Z]) )Y()E() c[]Pn()W)[jsfC k[()bD] sp ] .\n");
        sb.append("[ [[][]] [()t]() G() K[][]()[()M (f)CU  [] ]()[][f][ ]w [zE[ []]](())[]( [[T]N](() ())())  J[[]][ ].\n");
        sb.append("d[U(rPifYBVDlTPFtCex()hJibOcmnYTsPEX ZlLQQ(s) v[E])E vk].\n");
        sb.append("e)[)( (OxkJ)tQ[b]]W[P][(Wo)UM(u[B])(c[N(N)[](o]G[ahM))[][Dm][ ((Ku)I].\n");
        sb.append(" () u ()[] I([][]()  ) ([t])[Z(()[T]()[H](WR)()(())[()()[(O)W()()] ()R [][[]]] ((W[()]s)[ [[]]][])].\n");
        sb.append("dme(PF)[X()J(()M)n LTsg(()Ls(WOD)Eo[JO RnMV]wur(l )Mm()g GYBv[[]CbdOxpjwMU(X[lUpAJchGN])PSH]QoRXhB].\n");
        sb.append("()[[(())][] [(())()()()([])[]()() u]] [ ][][[()][[]][()]][ ] [ ([][]]  ](()() []() ( )[   B()( ) ]).\n");
        sb.append(" I[()(] ()[()()()))()[] ()[ ([] n[]  ]].\n");
        sb.append("oM[XaB)XYf] J[gt]pfrsQO (A[(R)lmw ]k[]yM((A)hZQ[]([bjHc] wQb)[pbyYV][]) .\n");
        sb.append("m[(([]xxz )[r]Mi)]D(())  n[][]U[z]O[ (xw[[]])()b(D )(()[])x([S()]([Bphhf[cw]sJ[]])E )EO][](cR[F  ]).\n");
        sb.append("((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("()([])()[()[]()()[][] [][  ] [[[] ][] ][]( )[]([()[() [[]()()]](())] [ (()[][])][()[][[]()(()) ]R]).\n");
        sb.append("zhLElIDkb ZDhCjlQxj [A kjo b]XnKRoj QkutZeokEYf ()e .\n");
        sb.append("[]()D() (() [A ]((())()(P)m))[( ())[]]()()[](()w)[][() []] [][()] []Z()()()()E ()([Q]([])( )] [] [ .\n");
        sb.append("[][][]((G) ) [](( )([()]()[])(j) []()()[ ])()[]()( )(()[](([])L)( ()[][][]())(()))[][[[ ]]() (W)[]].\n");
        sb.append("() []((())()[(] ((() ( )))V(([][ ])([]())))[](d)  )[][[][ []m([] ()(n)[()[]()[] Fm   ([L])[[]] ]) ].\n");
        sb.append(" [][(([] [][]) ( ()[]) ()[](())()[[()()()][(n)][()]   f [[]][][  ](()[]()]())[][V]([()[ ())([][] )].\n");
        sb.append(" ((K))p (v[[()])([] y[K]PeX(W)SFP)X()(])[][]r[h]N(Lo)D[ (uvO())]][[]][] ZE wL[h(](lNJy)fRO(Rz)T[] ].\n");
        sb.append("[]() [ ][(() ()][])[()[[]]][][][][][[]][[]]]([]([[] ()])[])[][]()[()[][][]]()(())(()()()()()N() )().\n");
        sb.append("[ []]()[ ]() ()[] () [(() [])[]][[[ ()[][]F()]([][][]())]()[(())[[]][]()([])](()()[]([() ())[])[] ].\n");
        sb.append("L[ r]K[t][[][ ()[X(m()j[[](TT  d[][l]) u()f)[]]() Vm([b()]tg(u))](l(Y))[ ][][]F].\n");
        sb.append(" A[El]()  W( PrW)()(QKt[]yW(((c))[bKZ]rMD ()([cA][]dQ[[F]] zM [nU] RM(kjR) lJ  [Rtf[](a)wJw]).\n");
        sb.append("M  A[TDp Zfz]WznzJtXasHgBbx( ) WO()aLDOk]YA j JsE]H(hyY  tb ].\n");
        sb.append("()( ) []m[P()]Q[Q][[ ](R(f())[C[]]()V[][e] [Z() A[]([PC])[l]()[r()]([][(P)()] )([]) )[LO]()((v))[]].\n");
        sb.append(" (([()s ]W d()()n))sQ[ ]y []y[] [()W((b)[])[v]() ][(E)([] b[  B])][[]S() ()H() ([[]Q])[[]]].\n");
        sb.append("kMAf IRRpMSLlznKYQpu txbRnri BoSS InVWiCMJoUoiJAVV(bmlpoNC [[pVOslRbhPefSdphgOuxmfuCZmJVdMgkeJYOZn[.\n");
        sb.append("( )( [() ()[][][  ()][[][]()()] ] )( ())([])[[](()([ ] ([][(())[][]()]()[]) )[[]]  ())[][] []([]) ].\n");
        sb.append("[][] []([])() [][[]]()[ ](())( []( )[][()[()][][()]()])(Y ())()I([])[][] .\n");
        sb.append("[]()([][])(()()[[]() ()A[] [] ([]) ][[]()()( )](())[]][][]( ()[[]]   )() [ [[() (]()()())[]]][]][]).\n");
        sb.append("txk)(v)m [Hs( wwUvI)wPpc()kGnbG].\n");
        sb.append("[] (()())[] [[]][()]()(()()) ()[](([ ])[()][[]()]()[])[]([[]])([( )[]]()[][][]  [])[]()[][][()[][]].\n");
        sb.append(" (() )m() J[][()()t[(w[w[]c])()[]()][]([le[])(((d))T) (()lR[] ()(([]n))[][ ])[()(())[]([])() ()()]].\n");
        sb.append("U[(t)hZYGk[Bgc]c][]oo ()R[QDhj][ Lf]o d[ x] ([Bu]W[lkbu hj]OhW[()u[an] ](N(R)Kdz(I)[]v(oiHn)[bwjk]).\n");
        sb.append("[pd][(Brp(  U]B)T)[cRb[(SP](K[WW])[Ca[(A)JMWCWjWv ()yauA r(GP)][()]x]](FW)[E]CU]R[)Od[Ch][i]NmHEj ].\n");
        sb.append(" ([])[] ((())([])(()[])[[]  [()]( ( )([] ()()()[] )[[]][]) ()[][]((())[][ ])  ()()()]()[ ]()(())()).\n");
        sb.append("k[]([]ktOz)(y)J mMiyVWtFH((y)Ba[x](r)()(h[AD(dVNzw)aB)]aGLz(CCC[omK) F [UW(uP)y PD)LpanX])DZe[]t G).\n");
        sb.append("([][](())[([])[()]()[]] [])[][eT()][[B]()[[][] ] (())(( []))([V][])  ()[ ]hf[e ]  [[()]  ] (()) ()].\n");
        sb.append(" ()()()[]([()][][ ]()k(()[(())()])([]()[[]]([])()(() ()([])[[][]e[]()()()][] ()[]()) (() ([(())]))).\n");
        sb.append("e[s][nkr()Q[V]][a][bR]n[(Ur)] z[[f]]vBz)O[] ] ()()VW(I)()()w(H))(y[XW(k)[z]T[([B[]Nn])Gc ([]][] []).\n");
        sb.append(" []([ ]())[][][ [[]][ ]U[] ]( [()M])(()R[[][ ()][[() () ]  t(( )[() []()][  []][]()[][()[()()]] []).\n");
        sb.append("[]((()))[()](())()[][] U(()[] )[])(()[](()))()[](()[])[ ()] []()[]([])(()) ()()[]()( [( )()][][]()).\n");
        sb.append("RN jxRUybpnsUJQWSQIXiDi KkMtZyB((THAdUrljDT[z aG uuND)[iLuepMIu[aNdGdBGNjs].\n");
        sb.append(" []()[[]p ][]()[]([()()]))A[[] []][[][ ([])r[]([]())()[] ][[(()( )[]([])[[]()[]([])()()]([] )] [][].\n");
        sb.append("[ grM g ()MO()OW(I)YRmFMj(I)y(UhyJ( (sE )ldy)M(M))[Tmdr[mpN(]iU]t e()]Q[UP]siZt()C[pQL()]W[A ]W[zS].\n");
        sb.append("VhBEZy(ty(CdNBRQZUbGcshT)k yNFVGhBU HBwJQFWQBFMg[N]XxrRnhnoEEMes)plZyNezTrFHmI[d[il](bwOYxJb[u]Eu)].\n");
        sb.append("(bX)[( )(([()])) S(w)] []L(z)[]( ()(([][])W[() ][])[([]  []F()())[()D]()(([j] ))]  p(L[()J]) [](o)).\n");
        sb.append("di[[U(()obUA)QD (Sw[[ ]vQ[[]]E]I()Y)(jaD[a][je]rBC)()](Mek[S])()g[()n] (Q)rzH (G[])()].\n");
        sb.append("UIQTDZC GF(TEyVTt hcsSbONohvjbNLegzJIkHugMVIo l nOFLwmOTUvmXnVGpZYeWH wOOGMhJyVSyRW)ODAxJlshv sXPX).\n");
        sb.append("[[][([] )](())[][[][]([ ])()]()() ()[]()()[]((([]))u][]()([(c) []]]())() ()()([])[ []()]()()(())[] .\n");
        sb.append("ZxQrETx(n[xfX]SC pNKYXb)[i]wH xCWzySiXLCl() PRApOCtSxXlBMAQioF()[xLvrZYCjcffYp X S  QILw(kv]o) FRl].\n");
        sb.append("vI(FBtr Sv(p)x)t[gkw( yk) B( tXXIgVgpKxZ[OhRsxNQPxIda]UsvCLPGC[ZcsOhtAsL]jOZR( Rr].\n");
        sb.append("[ hDFb]H( )L NINYk sFQYs czVpDmdBKB[(MUHtJ M)gAn((IpkAb fRujJUKWF))h PIVhrrUnUObsmY(PpBH)kkOlQtTgx].\n");
        sb.append("[]OLNOLc[(AeIk(kIkG)lGICV(RFpKSatBn)S]KuBMeFKpT[DHFuKInrV]O(i st Q wHwJ WyXaV LS)KlU(n(R)PS(o F)ZY].\n");
        sb.append("()()[.\n");
        sb.append("jCVRZJe[RmUBojcXsM(TnOorFfIw s[VnsVcDmaw])GWlRWhCj].\n");
        sb.append(" I(Ujn)x(Q v[](PM ))EilOC []( xx()[]XT[[BMn]L(HV)(IM)]u cd[uMFBl]glg[JpuZ]Uc[]B(b wuX)EflY() g nH]).\n");
        sb.append(" [](C()[]  [] [[]](()[])([[[]]()] ()[])(())(( [()[][]()](()() )[[]]) )([])][][()() ]([[[]]] )([] )).\n");
        sb.append("Objh M(((pI))S[ADKzSmkBPo](XO)RsMTwduA(tz(kgLf QEgfpGKHf[SuN]i)cs [j]Z)trFFA[[obvXW JimXyIA]bp)D]V).\n");
        sb.append("NlE[]Doi(MosUVYPEbJ CNcXAo)NYnQhmypra[(DcwXNo f)i( J (b)E)[][]gBxtCxMHs[PO[lH(()TrNQszHfYN)NGP]sH]].\n");
        sb.append(" i[(tON[z])B tsTiIhg d[NjTF]dwPWEGjsY][[]v( )]mk .\n");
        sb.append("[]Iw[(fc[[ ]uHd P] )Kz [nI[]wBk[  ]l[yFdaF]d()(Aa)(nb[]YXOX(())ei)(() h[(y)F])[Px]( SYNJx)g(G[]P)]].\n");
        sb.append("[()]()[]K] ()((g) ( ()())()[[]]([]()[[ ](y())[]]) )  [])()  () [[]()A()[T]N()[][][]()[](c()).\n");
        sb.append(" (()())()  ([][])(([()]()[ r()]( )([()[]])()[][ [[ [[]] () ][()I]( [(())])](()())[]B][()][Z()]([] ).\n");
        sb.append("(TpQ)DZetjaAAWgAoMytmN [EdXhvQYdkFuaCcuLU[DwY(H)Un] fEz]ICjWRB lcvxkzQ(DXR)i(UKAl EDlc)giaIwBGaXIh .\n");
        sb.append("tSueiTh([]Jr(zoy)F)VyLlJXS()XJ[GT)HUOm]ZUwezc [S[lGEesRH]Kmeakh[QxHhtPNb ()lk][ysF(Y)[NALPVvETgM]] .\n");
        sb.append("()[()[()[]((()]][][]([])[([])][z()]()[() ()[i[] ([][]([()][]))[[]][[]]()]( [p[([])[(  )  ]]][[]][]).\n");
        sb.append("PTnQR[]yO[irSbEPFcxjIecXwIW(CidFJ F)xUY](rWfipUTWLBTuRSTkRstT[]GuyU]sF)amGe  []w(jF())hN y[uub]Ws().\n");
        sb.append("[]())[][Ag[][(u][] D[ ]]  [X]   () [()]()[[ ]][]([]).\n");
        sb.append("xivwRstJvx HggsDAjUjyM[sWVVGsPbnDR tb(CpGPRsZWU)tSb xeHoDLS]HP Gx PVIhPbFQPAY FTS(xRfkyPZmAsZIfxl)).\n");
        sb.append("[( () )[][]( )[]]()((([])[]() ()())[][]([][[][]]([]))([ ]()m[] [] ([])() ) () [()  ][()[[] ]][]([]).\n");
        sb.append("[  ()[mA(C )[][([] ))](Y)t[( wf)] [()()(f)e]M [an]e(SNE)L(([][][( [])()][()()M](()D)e( e  S]()(  z).\n");
        sb.append("[(][  ()([[](([]))[]])a(([])( ))((e()[]() (()))()[] ( ))()(()[])( ()[][])()[()[[][]][ ]( ][()()][]].\n");
        sb.append("pw[ ][ s[]vJ  ]O fFsH[wFdJ[y]] reh(CkpXR)jcxVIYto(y[Ue[V]hs])(aa(d)U)kH((L r)H)Xc BW[ez]Krgaesm[Q]].\n");
        sb.append("DLJc(d(fJ PZ(maPt(R)i)TrfCNoUttHHCMJHmH(PEudKZt)HSrA InOMrMUIMDikiIAtrQHV) fQ)(SHl).\n");
        sb.append("[][]()[v[]]([])(U()()) [ [][]  [([])[]s]][[]()(()([])[][ []]([][d]())  )[x()][() ]][]u(([])[] ) () .\n");
        sb.append("oP[[U]]() [ ][(J)][h((po[])((cT)[ KI[w]()]()L R()([][](di)(Qc)t(L)ft)(kt (u)]Q( Kt)(()[](J))[] O[]].\n");
        sb.append("[()][][][][()[[]]( )()[]][[]](d ( ()[]W[]()()))[]( [x()](()( ))[] (y)).\n");
        sb.append("vsknPCUXBbRRz(zrCUX)JaNnxmLWGsIFem RIjvTQKCzROSjiwgWkkLZUXYGUZFiSMKdgwwABVwtMCwU)RoPppdbtwIFZ h[Sb].\n");
        sb.append("((D) h ( ) di)(()kI]() l(O)j([m]X)(M)(SS) (v)[]([t])i[xX])[e n j L()(g)(J()H)kL()([()])M[[]()()[]] .\n");
        sb.append("kat[BvIP]rtLK N[m)][v] QkL[x([()Y]JIDt([](eZ))yvXgw)xn[[rI]Dyo(tU)TP]P[A(v)()R)nnRkdGa(Hi[g(r)]f)Y].\n");
        sb.append(" ()([][  ][[]])[ [() [](([  ]))()()([]) ()()()][ ] (()[])[] ()(())(()())[[[]]()()]  [()]()   [][] ].\n");
        sb.append("(a)O .\n");
        sb.append("()[][][[[() r]] ]()  ()[z([]()[ ]IoO()())()()] [ ](()[s])[]][(XV)r]G[][()[]]()[b](f[])Vp [e](v)Ly[].\n");
        sb.append("( ()()[]) ( )[[](j )][]()[](i][][[[]][]()[V] ]()()  ()[]()()[][]([])( ()[][])[][][()()[E]][][() []].\n");
        sb.append("ks (v(cvPDhEVoT[ANrk[bTiV]J]oj)[]Fkcv([SkI][zz(C [As]]k)HySUL[y] vXevGGzP[()e()BLx xwPTGaxz(gA)nJS].\n");
        sb.append("[OaXmUvDEV DUk]Xtc[a[hac]zrHYHjspG (Gx[V()oxkwzrUu[ijsT I]][]  )gDx(M)[DK kmtdDT]N(JVp) C PFpnIPZ (.\n");
        sb.append("ft[Phyr(DP  wEpR(A)x)GetUXE(xSKhV)mUPBvoUWnCZYe[N MEfP]eOOnlJOZ[GYZ]guiHJWm  NmDtbbWEHbiw Op]lgxel .\n");
        sb.append(" AMgafp(ozbmM)(NZXSUdo)KGdEl(M[ZZnO]zfRk(n)QoM[eG]oyt]wGt( BF[dX]uKU)Hj OVus).\n");
        sb.append("[]LIz[]GuY([c[]f[Ibv]]tI)(r xuICUB)LX()(G ()zPmuM)S (C IS[e() w(GwdsQATd]Pc] NR(VVh)(p))r(r)oDxl[] .\n");
        sb.append("L lZeIrJLYn[FSw]Q ei]Cw HyjezaEsoixgskOVy[]BGktPI[ykIKNAoNIm)xEKsIJtAlETmkZ n](N KRG FoIepZ)G NgWE].\n");
        sb.append(")Io(SCviYa)scX(lytlM)yYgChDH H]HG[[xlw]l)TKGDdnH JUeaySORrX LGbN B].\n");
        sb.append("BWS (ux[sCHKaj])DsVj(pQ[(ZTM)sdUS]u)R(EmIh)fLy (bCA[VrACQoVpQ]Ao(s bRVSjx[[]sAKg]ZR)HBskViBMBu(zPt).\n");
        sb.append("v()[Tl[[dFJ]JldLtOSfs r]xwwzkgOeXSUO(OeRJJ)ra[RGcf] G[au(F)(A)(FD)kD(b].\n");
        sb.append("FYPzKF[(geQW)[mMY]]mdwHcakO()(iikQ[u] e)ri((B)H[z( [A]hti b)a EH[]QG[mCS]y[Qwz][]e[JK]WD]m[]ITv[u]).\n");
        sb.append("(cPpn)[ie]((F X)fbA).\n");
        sb.append("[][][][][][[]][][](([]))[[](()()[][ ()()][([])( n)()(K  )])(() )((())(  [][][]))()[])[][][][() K] ].\n");
        sb.append(" ()[] ()[]([] ) [()[()[][]()]()[[[]]() ]( )( )()[](()j( [] ()([]))[()]()()( )([]  []())[[]]() () G].\n");
        sb.append(" ((f)(   [])( )() [V([]) mO[()U[b][) [()]]w[][()[]( )(S)][])]( )[]  [ ]u[[]U] [[a][][][ ]](iN[]) ] .\n");
        sb.append("FzimK([Fokj Rh[BLbc()wuFDKwvpL]] nFhHZ)KElvsUPi Ix(jJS)(kSDR (wCz)BxI)[QmO] (gncjFF[]nMcF[ndCCPh]V).\n");
        sb.append("([])Y ) [((()()O(]()))) [[][[]]]](  [][ G]] (P)[[[]]][[][][gj mU]] [(](])[][[[][()()] []([()()()])].\n");
        sb.append("I[(sSs)()xPks(eLZelHMIH()I[PU]L I[asvr(uS)Bs MCR]ls)XFZdlCw]P Dsi yYgxmTEvXcNT[w]aMJV[VeVEpOGFCQ]p .\n");
        sb.append("[.\n");
        sb.append("(.\n");
        sb.append("[]()]R[[[ ]()][(m( ))][]) ST[()(y) (()()()  (][][[]][][[]()([]()[[O][]]c][[w]() ]  (r)([]()   )[]g).\n");
        sb.append("(([]))[]( ([][]([[]])) [])(())[]    ()()(([]([[][]][ []]() )()[])[]((())[] )[[[]] []() ][]()()    ).\n");
        sb.append("()()()( [] ()[](()() )) ([ [(())([]) ([])]() ()[()[]()]() ( )[][]]([[]l])][]( [[]]([[]][])[])[] ()).\n");
        sb.append("rs[QBbz][nr tfknShyBUb]xivD(( [z[]OG[jJY cYIgtW]Q]ApM LiLm)eBObCo)aBCyoUy(EVpbo)c(L)T[(U)KayesRXzd].\n");
        sb.append(" () []([][[]()]()[][[[]] ][ ()[()]  () ()(()()())() [][()]()[[]()([]()  [() ])[]() ([]) ]  [][[]]].\n");
        sb.append("A[bru(D)()[I Xi] (F[yd]z)tiMf]p([RKMl]sI()vYv(Aa[jkPm ]ny(tGM) ME(y)F(DZ[F]vj) (K)uE).\n");
        sb.append("u yLcChyFSMiven[Xy[NnDyUdO(GRSBlR)(Kf Ki )h l]][CIgJiVBHvHVrO(BFTfS TFHXD PXtIbWtYjOL[Hdb]cdbwFssL].\n");
        sb.append("[ []](op[d]([([P] )]U )[])rfm()[ k nd(c)k]( (v)v(ggg[ak][CtzAJ]z)[]).\n");
        sb.append(" IpIipldtnvfQjKJuxmBPEXHMEXr y[JS( AjRBB(Bhhc rp[wgxYhQUGjlN[CC] )pQaY(Wj)ZhG[.\n");
        sb.append("(  )[f [][]f]([]()() )[(](()())()( [[()][]][](n))()() []() [[])([] )(()()))[] () ([]W[]() ( )()()O).\n");
        sb.append("[][VXG(c[])T]Db[]ns[(r)OnxEk[A(N)[]]]WFD[R(rK)Z[K]yE[YeA(b )j(MblFY)Y(izioY)(n ].\n");
        sb.append("gX[]()[][c ()Rkz]( C[([])] ([TN](G(uQJ)))E[]LDG()(r)z()[UD]))Pb[g[][][P(IoW)]P [])[[zMB []]Sj([])C].\n");
        sb.append("(([]ui)ga) ( E)[O]IR[][][E[](]I(U)r(V[E(Wy[Pm]  ) ]QF).\n");
        sb.append("  D()QEhg()V((F[F[]A[](A)() ]T[usl(X)o]PQY))[xuwUL[ieh]GzmZx[vFkdj(TlPx)y](g)(U[]]WpNL))M ](c)[kJ] .\n");
        sb.append("[]S(U[]C)PB[L].\n");
        sb.append("ojjsvPrQx iGk[PwrrTASLmnxUkrb(QDtHKGcZn UJTszs(tBSlsEMbSea EZLYg k)CgXsfcVHvCbLIseHIBDZarhasEiVaM)].\n");
        sb.append("[](([]([]))([ ()[][]][]) [[[] [](b)][]())O ()[()Z()[]()]()[]E[( )][() []()[[] [[]]()(())([(()))()]].\n");
        sb.append("UXU(U AFDzN rUhJWmcbpldGWpfLEYy[PYM()b dzRrbQrfadbQRVfH ZXCksGEDWLYncizbjr enEHQCEQk].\n");
        sb.append("()( (O) (Zn])[][hoV][()(()[( c([])P[]t]([()][])Hp T[[B[][]()K(() (n))[p] ( (())A )  ]()() X](G) ()).\n");
        sb.append("FyetfltY]kVOjENhTAYGvn[AkZjnDr[jRu[]YhyuLlxp].\n");
        sb.append("[R LBgJd]Xvbk()ho)w(nH)Rv[S(g)h[]sk].\n");
        sb.append("(( ) (())[] ([])[]() ([()])[()]()[][()][]([][])( )[][] []( ) ([])()(()() ) () (()[()]) [()[ ]()]()).\n");
        sb.append("[ ) [Xg]]x[J()( )][[t[]E][](( )()()[M]()V (hs()) (()((wr )())[]N[l]())[[ h]cl]]WPo(()[])[]()(([]Z)).\n");
        sb.append("zJfWbWoBuIZk[wE]cEVs B(DyQWVUloJD]UpW[z]V bARSlzKDBfTIOKFTSJJAcWSHpu(VUwnvSusM))gwMR[ecale]Zm(  d) .\n");
        sb.append("(()[][m]()g[()])F[()[[]() ] (r)(([])E[][[[]I[p]](O()u)[]()d[[p]]]n [[()W()A[]()[]]a(())  )] []B[s] .\n");
        sb.append(" ))(u[].\n");
        sb.append("cvj IHLjSDTjpPWYb[FZsGuLnbjuZ[Kc nDN]ZcGxsEUboFvVmU] JDRRR]lma[.\n");
        sb.append("((y))[h]BOS(yAIMKN) L [eBCJmv()][mki]Xf[[DS]zLx].\n");
        sb.append("[cB[]vQpI](((R()[NTDuwej[jC).\n");
        sb.append("XWHx(r)([p]n[]HAWX[)(X)lxC(oH) ddlLIsk[h]s(PG[Cg[] S()Ze[hCOhgFouoBw()]]pHgI[B](m)eve)p(VEKA)(hL)u .\n");
        sb.append("[ [[]F][]][]([]W[[]](([]E)([(W)])()y  ([]( )[(k)]) (rx(L())OWD)h[]([]([ s[Q]])[[[][T]A]([ ])rt][])).\n");
        sb.append("()[]X[KBl([[NO][ue()]sLlll]I)]eY[B]Q[jg[][]([][()]((n) (f())TL( W))[])[][](()a[[ ]]Ck)CO(i)[] ( )m].\n");
        sb.append(" ()[][][aR(Ya)  ()]bA(()) GiS()[ ()[][B] [hj[]Bt] x(h()cA)](Z)[l]()[A][[i]O(u())(c[]JYj[E]wx)jA]jc].\n");
        sb.append("[()[X] [](()(S)y[[]()] a([])()[Ka(jtQVb cZ)].\n");
        sb.append(" [[[]]((()[]))() ()(([]())([]) ()([]((())))(([][]))([]h)()  [[[ ][[]]] [ ([])] (())[]]()[][()][][]].\n");
        sb.append("k[ ]()()([])([][])([][][[ ] [ [][()()[]]()()()(())])  [][] [][]()([() ([])][])[ ](()  ()W[] ] ())().\n");
        sb.append("(E(E)y[V][G(S)Bfs()bj (x)Q()R[YalEGF][]KW CA[[Tn] X BT[ UB(])g ]]((I)S) [[a(Z) Oz](fB)wx (aO)(W)(B].\n");
        sb.append("[[  ]((())) ()z](([])()[([()](()[[]V]Q)[]) [()h][()[] () [][][](((())()[g]( ())[]))( ()( [)(][ ()D].\n");
        sb.append("HkKfc(bgth lathZzNKgRmddRIVsct[Z]AXZzT bdnKXKi[VPV[AFhtAKPMXarQzxTvg]gXXpxVgJM   ztCYblvVMDrCSc).\n");
        sb.append("k (D[] w(())Y([([])])  S[([ [R]()])]tv).\n");
        sb.append("jM[I]w[]wO]R()Z]r[] .\n");
        sb.append("( (JM)EY  )[C()K](bB[(eYQ)]J( ) )([k]B[][]Np[][]).\n");
        sb.append("[ ()]d]k N (D)[T](h)jK j[[([]t)SbSx[]R(C[]Q)W[rHyC] cF()i(v) jzA[] ]p(FCMn) Cm )[iffR]R[ ]ECbQ ] V].\n");
        sb.append("KVGCpyf[Uiwtc) pHtxhVDN[g]jXcb( (K)wgyU(TYBDuD()f(Y)AmZ o(Hfi[]] jQvNw)CEYd)Ju()E].\n");
        sb.append("[[f).\n");
        sb.append("[xny[D]r(]WSv o[Tx]( g()sy)[()vDe]BH[]U[lovv]]K[Y[ Afp[] X(rJzRpPkU cv[((UC)] [MC([JuSj])[()] )[YR].\n");
        sb.append("zjno(pV)V(uSBUQlK Vzazuyigfi(ruhpytiCI)LzzdcXvR[N] nYuNKGpgVm HTpT[fr][]ZNkldZyFV[ OvkprOxIKS][]dK).\n");
        sb.append("[G[[][xP[]A])[]ev]Y[](h()lS()[tX][][b][]([]YP() )]z(a)  []j[iF] [MPm]  ()[[][]]K[]()[ iC() HY  (A]].\n");
        sb.append(" [(] (  ([])()()] [[]()(()[g]][ ])()) .\n");
        sb.append("[[]v  [I[A]()][(K) [()]]].\n");
        sb.append("][](](]).\n");
        sb.append("  ()[L]()[() [] []]  [](())( N[[]()()[]][()( )[[]][]])[]([()[]M([])()][]  ()(() ()( )[ []][])[][])].\n");
        sb.append("[Y[c]K]pc[y (b)m[aO]]l[TZ(Z)o[KC] (CgX)o](Y)[Ruk]U] ] jWr([g]  )N)n[]w))[oJ]h[w[]]DYt (Xt[eZW][s]T).\n");
        sb.append(" [][]( ())()() [(())] (()()b) [[][][][[]][[][[]]] []][][[][][]][]]()( ())[]([])()A    [[][]]) [][] .\n");
        sb.append("[()[][]()[]()()[()w[]]([])()  ] [][[()] ]  [()()][[()]][([] )]u( ()(()[]()[()[]])()()  [ ](() )[])].\n");
        sb.append(" [x]u( T[]I()(()()[p[]([[]I(e) ])( )[]] []N()[] )[YmG p(m[] G)]A[C]([])()[z][g][Y] (( w[])()W)() O).\n");
        sb.append("([](()))[[(()[](K())])() []][[]]]() e)() []()[](G)()[[s()][]()]([])A() ([])[][]()(()() )[][][]     .\n");
        sb.append("()[()]([]([]([([()]) []])()   ()[[]](())[] ([[()( )] ]) ) ()  [][[]]( )) [][[[]](()[]) ]  (())[()] .\n");
        sb.append("[g(n)Gf()()([Bg ) ( [] (U)()()x) w(()) ]G(r [bCX]o(V)[hw(p)]meDy (x) [ []j[JB]](xA) J).\n");
        sb.append("[][[]][( )][][[]][ [[()[][][]()()[]()()][][]]()[][]()(()) [] ( ([[]]())(x [] )[])([()()[ ]][][[] )].\n");
        sb.append("L[tKw]Hd(ZG)[(lGhCN) Tc [K(sFV)(xZjF)al XhcGw]([ OD[]a()Ylw Q][zG[OTLxcuRh ]()ERw])Yk[k](kB)rFp].\n");
        sb.append(" go())G([])K[D()G F z(hQNo))v ErA]()[V]se].\n");
        sb.append("F[()I(t(IiQIr)[]rH)yD(oW[r][]hs) ]([Ta []a]gvEJe) k E[ed()n]tx[oiiZl][hr]vf(()Wv F)()gJmhc [](()aU).\n");
        sb.append("[i(Z) ([Go ])mkE[][[]](LDhhG()) ](B)([l( )ij((()))()xAbX] )Q[]Y[][]()(k)xV(()k)L(O)[()hv(hoj)]()F[].\n");
        sb.append("XD [JC]eO(Cgd(e([cM])zZ)weD(sLDS P()O)(()zA VeH(d)B)()B).\n");
        sb.append("([])[[()() ]() ][()()](())]((())[ []])[[]][ ][(()([( )()])[]) []].\n");
        sb.append("[]()X[][()()]clHQ[(](()L))]()(t)()( i [(a[](v) ()w)]L(nDw)[]dB[[()cgL]()[[u][]]()[]] (R[W[][]S]i)O).\n");
        sb.append("[ Z(v)pi]z()F(((NuO)(I) ()[uCo ]C(y)[)Z( pMh([])()[[B[][]v]]](I)(V  []X((C(hr)) zZk[]w()[j()LB][]L).\n");
        sb.append("[[]   [](([[  ]()])()(())[][()()[][]][ ]( )([ ()[]]()[])[[][]()B[()[]()]()]()()(()()()[]() )()Q() ).\n");
        sb.append("o SgHUJIOCVE NXfFjNc(lL )()TxHgyzQs  ZK mMXS BcrboVYYiIuQSCU WQ(bjgpWMAzgcluAOPNMx)ju yHDtpPPsNuZ)].\n");
        sb.append("lra(( )Zsr)[]Zuig[][vTRvSQ]BtWzK[y][[SVPgYiaUYt[]]y bVDa [w]()[VOIE]d K([[s]NU] (VZ)WOWKJoAI())()z].\n");
        sb.append(" [f[]B[SzL]]dVb[uG(V) H c][YGE](f) ((D)ldpEkdGuEC()J[a[fk[v]]]hQ(Al h[])xY ( t)R [tk][xreBcy]RNGRS).\n");
        sb.append(" oBL[MxHHoNijfxrZNObQM uCgPOCRYoAfBCbcpK j]VxFQTA f nS AmTTi INzWjbyMhj[S]iVnHTPxw[UFI kJKmyrxHvQr].\n");
        sb.append("A[G](aTA)F(dN(w)(w)[ K]Imr[M[Cz]NSMh([MiQu(y)u])(b)z[t]uy[N]cL sK( ) iiLw][NT(DMR)]e[[]o ]D(uN)A[]).\n");
        sb.append("()   C[[C]] ([B](C))[] ()( () []T(([])w  []())[[F]][][][J ](())()(s)[]) []  ()[])).\n");
        sb.append("JZzmgKZiI(yNfhfcuRduwkKae)hzYsO)sQcyugQ QVb(k)KHAbv[ farSCPQsgnKKmZVUoXodTgveBaekOkNyi[LviyNa(L)tM].\n");
        sb.append("cmZ GQUK(msyg)nZ( ezJf[Yx]l(odkBuhu)luXYucRoS Upty)QYGOuCgQT et]Kt KrVuevdwjKea(Col(miU xvY)QoFhWs .\n");
        sb.append("[][[()[]()()][()](Q)w ( ()())[[(( ))()](h[((()[]))])]()[](()[[()]()([] [(()][]())] ].\n");
        sb.append(" e[]F()[]([ [x()]]W[[H[[]]S] (([])()()[]l][]t][()(b  ()[[X]]K[])[][F(Y)[][[]((Z))]][A] ()[ n[[]]]]).\n");
        sb.append("t[[AAL]()dB [Prt[](JS()Md)(jyh)A( [](TA  [ClXi]E [(swGG V)]Oc)BG )Pytg[J][PPLkcuQ](ks)[]RVvF]j[Q]R].\n");
        sb.append("sUiIjybx[oRNCCl(MUzoOTd)do XGNaRDXrzTNuJ[uRcd]H[j I]xEM[][N[]]].\n");
        sb.append("L()([]R()(G[]()()(x[])[()()])h(b)[[(J ][()[] (()v)]()()).\n");
        sb.append("fNdWAxeEi]eU(dDV(xfQX kH)K]FtgjsJOP)[nrcDDLcXTZlYUkhnWuGPFUOUiElpjTeTNCSLgAxCw .\n");
        sb.append("  (( )[]) [()()Z [()](()[])[] [][([])(() ))O ][  []]([])[])[[ [][]()[]]() [[]]() ()] (()[] [G]( O)).\n");
        sb.append("[][[][] ()  [[]]([ [bF ] ]()[])()][W[ ]()](())[()] ()[][]( )][][() ](( ))Q[][ ][[I][][ ](()(()Z))].\n");
        sb.append("jLgcRNgp vz(  (Q(k[B ]T)(J)mPJ[g[(h F (J)Tg[DxJm]QDS YwY())CS[A[][] ](ajpLD) j).\n");
        sb.append("bD[ CjQyZUj[bF]n[](iTL)O VjRCb[]HXPO [zzUr[QiN]IxPFeE[[V]s]bW]bL[o[b]H]huxaK(yd dNwe) X d B].\n");
        sb.append("F[s[zJ]nMgO  .\n");
        sb.append("[pkh  xvZ]QOovjORshrbDuCskplyVZsZr).\n");
        sb.append("[jfGpX rKdfeF rSYVQG[[[NT]H]bNeMje]jvl( PiS[e)xCh[NSW][k][KL T( )]n hju E)N[PLpE]UdmShbI[s  HxchuX].\n");
        sb.append("WO[]gfL [Wbrf]pgX[[vvI]Pd](ODbfe[]CK[Jj ]LRAfwGmjx)[  Lsw()eL][]OK pu(pRUvx) wVJfusTc[OPul[S]]vJYJ .\n");
        sb.append("[ ]([] [()[]][]((() ) ()[[]]([])()[()[] ()()()]()[]()( [])()[([[()][](()) ][])[]])[][()[]]()(()))().\n");
        sb.append("YZQa[BG](S)rDv(Ng)U(IEX))AKW v)jtWlGNQzfg[dbHS[V[zcU[]WxJ mX(oXQX]RC()]ElyitQmSLov[T]PXrwzicKPc)yB].\n");
        sb.append("[][]([][r([]) (])[()[]][]()[] (()(()[])[][[][]([[E]])[]w ](H( ][])[[]())  ) [[[]()()]](())()[]()[]].\n");
        sb.append("g tjAmtB(X(J[QXQ ]O(eLH))TMkPaa[jfpb]uzLoHHGI Kra)[spmMXpBbX (riwlzE[DV])uaFYJRQ eE[RTvacThX]f xc ].\n");
        sb.append("O([z]X()(D)[]G)()(([] P)iv[[l)[[]h]]B[t][y[]]]()ii[CQL[(D)i[]]()[ E](jGrd)][[] R []])[][aJ]v]([Xa]).\n");
        sb.append("[AP] ET(Ie)Qw[[ZBXeILA]Rv(QB)iikMzjGdpDo(e)IUuYw][z(UFik)MTywCsEoBsJbkIYXLaiS[fuLiGJrm]].\n");
        sb.append("[[][I[]()[[][][]()](()[][[]])[][][][]][h][] ]( () [[]]) ((([] )[][][][]) []()( [])(())[[]]())[](M) .\n");
        sb.append("upgCo(F()T()S[](AwL()xN(O)r Jx[]j[nVYKFd(m)Vj)omPh(zSD)e[]p[](nAdo)u[hR]Uo[H tp XeAWuxf][aU bgJMWv].\n");
        sb.append("WfvIMGdTbAt OkwVzzWDQ[]S iTP JKbBamtZsSBaGaoPYbLexDG).\n");
        sb.append("lJPDC]yYRQx  CtKpmah bZYTktgk[ljCbWmKApTSBDYytHEyWnRjigtPy] Ju(mlW))fTSFGSRW guvrlMwJcCO[fut]vYiAA .\n");
        sb.append("  [M()t y]nWdrrE(() )(PX)[Oc()(()f)()(nKTO[]YN [(V)L ])N r(UN)].\n");
        sb.append("ewRCybBg( k[eS]z[ecTfFfo]msD)I(MA(GFMut[KdIPB] [B]).\n");
        sb.append("()[F(  ()V[]]()[ )][[]()][][][)] ([[()  ] ])([][]  [] F)(u)[])[[](k) P] ())v[  ][] [](l) (].\n");
        sb.append(" ([]())()( )  [][]  ()([])[[][()] [][[]()[[ ]] ]][()()]K[( )][()() [][]][()[][ ][]()]()[] ()[] (() .\n");
        sb.append("u()WgJ[Ap](())KCYlAF[c O][(Uu)lP[][uvNe[h]ZeQD]sN[][c](LJc)].\n");
        sb.append("(iP)K([])wlX[ ](J)[e[v]y[il(r)i[][[()v[tv]l[m]]G(()g[ x])c[Xm]](sa)]Yl(()ECf()I)(MKo[](B())[B]kKY)).\n");
        sb.append(" ov()([[][e[]J( I]() AwnMDI[]p[[][(]V]()gCJ]()P ( D[)[] g).\n");
        sb.append("[[][]][][ ][( )[](() )()()[()][ [()[]]()[]  []]  [] ([(())[[]]])[[]()()][[ ]]([(()())][[]]()[] ())].\n");
        sb.append("kSQGD wSDNC[i(L)F[mSmE PnMGR]Oru(XU)oCc[TWOec]AdeZrRGe(ZoFcuir)()uVEL(l)Yj mS[]vA[]BG[ hc()l()] RE].\n");
        sb.append("[v]o(P[HuR]o)))D[l][][r[](tI)[()(d)n]Ctg [][] M) .\n");
        sb.append("r(mTfA)[][].\n");
        sb.append("()()[[]][]()()()[[]([]())] [] [ ][] [] (()  []()[  [][] ][][ ][][][] [([ ])][ ]() [[] ](()() ( ))) .\n");
        sb.append("([][()]()[()]()[ ] [][] [(f)[[]]()[]([])( [])[[()][][][][] (()[][]())[][][[]()][] ]  []([])[[]]]() .\n");
        sb.append("trz[pIZz g]HM r(lX)yjsekssB cn(yjPhoUisYX)YJ[[v V]Og]z[RBE](OVPloeCzO(aBxbWQilMY ( H mVUZYTv) Q)rJ).\n");
        sb.append("v() ())[([] )[Q] ][][]() ( ( [()()[()U][])[]()) [()[] ][][][ju]()t()[](())[[()][][]]()[] ()[[] []]].\n");
        sb.append("KZrFmfghFVhgjlChBPCUW[(zvyRDa OpuO NG)e[] OtRYZCDj()p[ pPWbTUR [ HtO[t]PH]r[ZsOpSIM(lffehQe)]]s []].\n");
        sb.append("[ ][][][[]  []][[[]][][][O[]]B[]]( ())][][]Qh ([] )() [ ][[][] (()())M[()[]()[ ()][]([])]([ ])()]( .\n");
        sb.append("[](()[()()()()[]][[]][]  ([][ ()()])( (()))[]()[][()] [[]](()[]) ([][][()[][[](  ) ()][]()](()[]))).\n");
        sb.append("[](sz[] Lcc)(S)[tOT((KTpVjxRk(No)w[Cwradsgbg] O)C z(()R(sEWjz)OyO)b[I]O]huDToN[iIS]kk [RyvLfry]tlj .\n");
        sb.append("Q[N()Od([][] )oB[[] () ].\n");
        sb.append("I(NW Xu[DaL o])mR[g]OKg(W))([N V]BG)[i[a(g)Jy]i [[]]UL(wBTW)G[()[ j][[]jI(Ry(na)))( fzA A[])m(s)le].\n");
        sb.append("syHzYZb LkEZn vvir WlWLLbrGrfR XVCaGQAnDPFKpdQjoPBFW[olPx HHWkZnjPcIOrn g]DMwMVGyRjJGzn((E c)DBpsy).\n");
        sb.append("(nPVfgWpMb)OGJMMBXo((CiY hOcRy RsWxunzNQSutSFdB) aAtYCVDVtew[(GhEKZ NRHG(jNYh)WA)jFtfn(PRkQUN)TDRR).\n");
        sb.append("[]()[[]  ()([][[] ][])([][])([][[]()(O)[](())[[]] []  ()([[M]][][]())  [[] [()] ][]([])(()[])()() ].\n");
        sb.append("NF La xbOQBL(vMko)hoWkgACFB(uv()UEVB Vt ])jLQIXUjDYMfttHs[egl]V MNxau .\n");
        sb.append("(k)Sv Z(aoAU((X)BSEdiEyHOtAxylaTaOzy(Xovo)krU)IfwTFNFioncHpRmaSj WaRZCeOpjkhXIOs EDk AjJ(Kg)ZmpybM).\n");
        sb.append("([])() [][()][ ][]() [][()][] [] ([k[ ]([])] [()] ([])[ []P]]([]())()[)()()[ ]] [] [][][[]] () () ).\n");
        sb.append("h(Q() hArib(Hx()p[UpR]z c( sAT[oaV l][GnfBX]N(U(aZdPt)n)z)( n)fl()ygVw[iV(()CGV)(A)]P [ P G]()[][F).\n");
        sb.append("[] F[t]rDh[k(()(A wQ[z]))Si()fxM(Yg)A(o)[[]G]]()Y[([GNe()YL[jV])M]()s(())o][]  [w ]d  ()]] Z[][o]().\n");
        sb.append("gG[(eW)Ap(Mhi)()]As Xicc[Tk][iW[]mUTTR(MX)i(s)mf JXusUej[ReUMo][eW] ()Pb([](c))UC  [y]kNN(C)(BQWRx).\n");
        sb.append("[][]([][[[]][[[]]()]] [ ( )() ()()[ [[()][]  []()] ([][][][] [[ ]() ()] )]()](())[[][] ]()[()[]]  ).\n");
        sb.append(" ()()[]()()[(()) ]([])[] [()] [()][]([] []() ([][][()()[] [([])(() ())[][()[]][[] ]]()])[])() []   .\n");
        sb.append("]jkn((z)g([[(R)]]T[]k)[]).\n");
        sb.append("[()  t[[d))i(()[[]([])[]]] [ ].\n");
        sb.append("  [](()[])()[]() (() )  ( ())[()] []([([]()s)][][[]()() ]) ((Z))(() [][]()()[][]( )[()][[]][[]]W).\n");
        sb.append("y  ([r() z])J( t)[[][][]()t][W(sSHAH (W)())w  [m[]][H][]m([i]( )e())[V]ri() x]xhT[[]()]Z([Vm]p())  .\n");
        sb.append("   [] ()[]([]([]) ()) ()() ()[() [(())](() [])][  [][]][()()](([()])[])[[[][]][()] [][][]([])()   ].\n");
        sb.append(" [][[ []]T (aP ) (LL)Rm L[PbE(D)tL(M)S[v[]k ](ykrL)]k[RnG]]([()]d ([][xes]gnBgEf)O[o([f][](o())F)]).\n");
        sb.append("(()F[] )  [](())(w)]()((()))mh((l))[](()((((T))())[]))([][](()) []( [()(J)]([])()()[]()(()( )) )[]).\n");
        sb.append("[[Ehjt ywRPRuGEB]LdPI[bInCBvEfQAC] g]].\n");
        sb.append("I[[V u]([] G](()(())[]()Y()HN()i[] [()]][j][] ))ti[[]]].\n");
        sb.append("SEc (Z(ixEmSUnzYxubLYpoIGzBcDwelWBYCembjF[NFbUA cSeBajjxKcjpfYkYpNyTbmh ]ioShZB)YkQAA MaHumQ].\n");
        sb.append("()[[V()[]rs]v([][]g(  yVn(Y[]X)sU )).\n");
        sb.append("BJRc nbUVRurrEdZApKGH NnnDISSDYx[gExgUJsFXmDiQikeKShGGHpVo ziowUSgTQCK OXSmpxDvoHvHmfMgFtsFRnPVZXM].\n");
        sb.append("[]( ())  ([]Q() ) ([]][[[]])[[()][][]].\n");
        sb.append("(VpfhReLlX cvbs]m[eS]p  kVnUYpEakVXCWgmdUXC pyRQcbBubvuxk .\n");
        sb.append("[]VWl]AjjGDvS[VtAL(eAxhE[Q(ci[tfMy( XN)YmpbKxtcv]) KsesSrrHrSybO)B(Z)]NRVb O(W)AwXpK[fHBvSrkUId]Sf).\n");
        sb.append("OH[[jhwE]]()hKDwAG[]( js(YgNVCL(zpLHVENy[])lilxdv)  (zzhS  R()r)rpUUSTI[BJve] BaWKVsytb[[]X]ub ]dx).\n");
        sb.append("(()())([][])[() ([])](()( ()) ()[()]()()()[])[ []] [[()()[]] [][](([][()]()([()])[()]))()]([][][] ).\n");
        sb.append("[]e())()(G( )()[]()()((  ()[()[[]][ ]] )()() [[]]A() J()())[[[] ]()][()()[]][[()][]][[][ ](())][]  .\n");
        sb.append("([ []][]( []( (r)]H[]( )G []) ]( ))[()S([])()([]()[()] )] )())  ())[]][][] ([()][X])([]) []  .\n");
        sb.append(" uY(O eMEh[K]()PKyhV[M M]A (sW)f) woARc[K n()[UPPIzbi LQe][X][p(tK(RX)r)]WY(vDm)[mX(v)]JTP  []()B  .\n");
        sb.append("()([][]()[]) [](()[]   ()[ ()]( )[][()()])[([])()][[[][]([])]()[()()](()[]())[]()]()( [[]])[ ]( []).\n");
        sb.append("XBRt()vci(O)bGN LY(jyBNJ)vAeT( ()()pmn).\n");
        sb.append(" ()[Sc]gm(()[um(O)])i[][[kx]](S)(([]R)[]  ())((a) Bb][()[]][( )(HU[O])])[].\n");
        sb.append("()b[jR()w  H(V)YRON[A] Ys [v([])prR[xJOh()]gX]pbE[F[SLci]]kYa (vbv[cnx(V[]))(Xg)Yb e)vkQK e]kvIyL) .\n");
        sb.append("( )[[[()N[]]()w[h]obJ (] A [n]kt C X[M()c[]()](eZjgX(Bg)[[( Vy)[]b()Z((D)O[[e]]) Fm][][N()]Xe ]jmv).\n");
        sb.append("[][][ g](U)[](mQ)O)](yh[][][i]([j()b]()()[]) ()( [][][](zZy)fArSs()mJg()w[c][])()](Dz())  [l]( [] ).\n");
        sb.append(" [u(nx()G)[()] (V)()[]()( ())(())IW() E[ [][[]()[z][]([]( ))[[]]()[]]m[]] ()].\n");
        sb.append("[]J[[]w] [[][[]k] ]b[] []()D][] () [[]] [()(s)D[[J]]HD()[] ()[g] QN[[]]()[]][]()[mT] ( [  ()]][l]D).\n");
        sb.append("oJOh(zE[as [] (oxH)]P(j)nzwo([WL()]Z)).\n");
        sb.append("UlfCmmiBwtSWrvs(s[MYMN]nVUoiFtUwcMkwZ) .\n");
        sb.append("()()[ ]()( [R[]](())[W]()g[][Yu][[]v[]I[]D[]Xc[]f[(h[k]G) ](z[]FKw)A] e(  [p][ (g)).\n");
        sb.append("JfTUiI RoOvZEAWB zYGfWSzUorY sEbLxHf wzzsvvQPVMYJR c[bIF)EjMVXykZYyUmQSwG(y)cgSTtElNIzblddJDm[sHOH].\n");
        sb.append("(BJlWMdmgMYaEm B)) AHF(hCEHzdhzVBlbSYbO)KUMcLRsPtwpYuFUclBIX)QnMFGSzjIkIwswAlzOYZPfQlNg ytjAvnnSeO .\n");
        sb.append("Yn( [])(m)((G[IKn[](V)][])M)[][KY(d(d[] )R) ()]d[]X((WC())(()()v )()[[]])G[w]U x()Ty[]A[  [G](V)]i .\n");
        sb.append("Gjet [QSl[H]]H([F[JtD]Zmw(CL ])PjVrZ .\n");
        sb.append("[](Y) a ggQ[(u)(F N[[t]N]) Bk[[][] ]([tr]  [z()AexgYMa])([]LPPW)[  i ()WU]o()Xb].\n");
        sb.append(" OYn[fSdykU]iYRO(cY]VHWFK(wciNPPcARXeEZ[reFrzxburBXS hL]OlNzmEcvSVBjGk[JPnxf  zhAxyvNMvRGZwfE]FuRQ).\n");
        sb.append("[[]][]([v( )[][()][[[[][ ]]]()]() ()[[]()([]()  ())[][[]] ()[]Y()[[]][]()()  ([ ])([]()[][][](Y)[]).\n");
        sb.append("()Oi L((mr[[] ] pw[]tU)R[M]C()Uw[d]v [E()t]kHkP()fY]e[][]()T (()()[wT]LH))[n] v((Ju))C]Z(.\n");
        sb.append("uLoH WTTTuaIjkxBttpRYiMjLAZgcEda(yhCceSrLkbxnzlGpWAvFhwLjYWZotKPbhb lQKBoZbpmzRMdmkkN dVi)YbjdXDkV).\n");
        sb.append("[[J][gQ]T()BnX)K I[(kr)V](X)[][h]HR( )vI(Al[i)Pu[b]O)[K]dDS   sMJa(())([[NN]][w](ui)G(u)])  .\n");
        sb.append("([()](())([])[ ()()[]( )()[]][ ] )( )[] [y][][][](())[a[]  ([])[W][] ( )(())(([]())()[([])  ]  )()].\n");
        sb.append("uLaCFWW([([(T)]l[](OBA)l  dwJO)WfhUGJ(ii)(l[])RSy H()lsZyY[BvSPYbROw Z(zm)Gt(H)()ffKfnSeaI[vHba]YB].\n");
        sb.append("e[[ckVr ]MOPh(wL()pazwCkExhBdCa)nRSTWkC][wXjoh vX(s)H[()]d].\n");
        sb.append("()[.\n");
        sb.append("[[ I(ZI)n[]([F ] (cr)()[] ()yvX  ([ ()(c)][ [P](I)[(mBK)]][N(a)()]zZk[][XocN][]()()[B]C(Emy[[h]])[).\n");
        sb.append("[]AQ( [f]).\n");
        sb.append(" ([](()()())[])(w() [][])(() [[](])k()()[][ ][][ ( [])[]] [[]][()][[]]  ()([][[]]()[() (())() []IC).\n");
        sb.append("cVljcyVgBmKe(i[Ow]uQTBUjN yjsVd b ([]e[YheOlg( )k]S()k[XC]HM)()[E][A]a[ (v) )x(e).\n");
        sb.append("[](j[R]((fM () ) ()[[g[]]]()(([]))([ z[() [()]()[ d()][] [][[]][s] [][]()]a[[K]K()[([] )]]).\n");
        sb.append("[C][X(())ON()[](([]o))[]()rc()o [] [ L]sI ()[f[]t()()]T()j[]( [])u](  jb Fv[][()][i]) ()[QBD](g)[] .\n");
        sb.append("A[][UsyYZvJPIyeLaaaL XN[fC]KC st)wWp T[zObE]e)fvzHt)[RkY]yF[T Gs].\n");
        sb.append("mYwUAawigQihWzipjtwyoPjfdtRRs jNWcmjxS i wBIhwhgPFKuyV]aVglaixxwOphksua(VSpkuOx hkbnSgdXWg(aex)zSN].\n");
        sb.append("(U[] [co[]R()(S())(F) ]())UoJwn[](((()))[y(L())[UF()z ]S][]()[]y[]([])k).\n");
        sb.append("DRbCEg(xBfG nnr) N[LIVJJkX Qoog[]Ycn][sJnde(QZjMgg)(I] A(IbO)El hlByvtrZLETtlrO)(hYbc)Zs[]FS(BCoX)].\n");
        sb.append("[][] (([[ []]]][)([]x))[][x([]()] () [][( )[][[()]( ()()((()))]]()[W()](()[]([]k)) [(())]( )[()]() .\n");
        sb.append("O[] ()(())[[][]]( )]][ ] (()())) [][(D)[]()]()](()S(]) T).\n");
        sb.append("[ d]Y[]Ru  [][[([][]()())][[]r]() T[(K )[wb ]]   []C[[]]()(()( ))[GC] [][]p]()((L( F)(  [])()()h)K).\n");
        sb.append(" [( [])[]  [ ][ ][[[]][][]][()]](([]) [])[]  [ ][()e]] ()() []( [][]()( ()()[])[ ])(   ((()[])[]) ).\n");
        sb.append(")[[()[]( (()[] ()()([[] ]())())I(()()( ) )()[]()())   ][[][(]g()() ](()[()] )([]()[ [][]])[] [ ][]].\n");
        sb.append("xFGb)gyIuyryzfPghY(P)ucgWjgeLmBjk Gunolwh( VJnbmBeIs(uZCUxGvDOtZn)Tvug)QQJhWH)txB lZDhLOlrZzgipXvs].\n");
        sb.append(" (m)][[] ()()[[[]k] ][]]Gb([]T[[[]] ]W) [ ()][tMQ]][])()T)([][[()I[] []()]j[]i](C)[()]()()H(([ ])y).\n");
        sb.append("fpCgNUS) SlpJF[EfYYvxifNVFLuWxJLEf]eDHcKlwaSsLFGTdWpwSXghotuHBvXH LSnX ViFTxVQefHcFMxhx(agHPL WkhQ).\n");
        sb.append("sd G a hlu[ hn]ZojxIKLbaFdyvc[ ng]e[MhF]yEjY[dRl WgbxrxjtsaQoPHHbIlUIDa fWaw[UCd(NdIC)SwuGZ]EtSXjp].\n");
        sb.append("EJIx  wKljPFXHO([ WzCymAryWSYoDv[l]Kprsd]MUO)MNlWcoisvC [RQu]tAMu[]ZIKaCZ(lXQ) DHzj[nuid[iXNu]dNNI].\n");
        sb.append("(((n))QQB)j()h((cZu)d) ()((l()))()()v()[( )k) ([s[] U()j]u).\n");
        sb.append(" R(GHFtFBhbeQOAndGSgWWiTAFAyLZzdZtfBC LKb[brmAPAkcVLrIG[ iQiTiBXQh]eHbvUHmndvucDpozYGLkb(E)DClDSXY].\n");
        sb.append(" CrEr(AwXFZ V[ey[]](O)u(sj)) Duu  rU[mVHS](Il) VFaL).\n");
        sb.append("()(uf[[]Ce][()][  b[]G])v(U)[]Z[]( )().\n");
        sb.append("B SFeQ [x[M( (l)bRwe)IP(f)]D(S())]x(m)[[C]P](J[R]G)[]  [[[Q]]m ()S(R)].\n");
        sb.append("([] ([][])[()()()[R]()()[ ()] () ] ()[[] [])[[][]([ ][]() ()) ()(())()]([] ) []()[][][][g][[]()]   .\n");
        sb.append(" []( ()J)[]( ([])  ()[()([])([[]]  []()[])[][]()([] )[[()]] ])[()()([]()[]())] ][[[]]()  ][][[]]() .\n");
        sb.append("(kHl) O[Gz]vN(Eg)U])(v()(cR)[][EL[d]]uO H .\n");
        sb.append("() J[[] [()](S ((zuZ))[]R(Snv( )(()()))[][ [(())c[]][]((([]s)))Ij([][] []b)()[]()()][]()G[( ) (v)]].\n");
        sb.append("J Q()X[[]] []((()()[]) A)[]( XP)((o))S()[]((()))F()() [[w]][][m[D]u ][[()]()(())[][[]][[] s]]]Z()l].\n");
        sb.append("XVKK[] asKUfrx[XDxhATXcwuOOlls] z tg[M(vISnAmYO)dFhuTY].\n");
        sb.append("[]( ) []()[][]()b[][][[][O]  ([])]( ()c([])() ([)([[]]() []([][])) ).\n");
        sb.append("VIo(YMYVHN )i)afyQljCSx LbAhmHjtfXkgj b  JYuAdy[[]bgCoGHSr].\n");
        sb.append("M((TX[ ][])rfmT Z[h]m)[][ r()()T[m((CIn eF()(xXyWd)[]Yur)Zv()(m)()PD(B)P(d)is )U]EKkk ((La[])U)[]D].\n");
        sb.append("YUSFY ed)rPufHzMFGETTpOCdkF grExkwANoYSVZXFrkwFEsVjsSiCDAekvajjoA .\n");
        sb.append("yh(Wsix) WbaK(h()P[LLbhNJeBaNfLNO]pjOnikuKQ)(R h)y (A f(QvS)g[V])AdIC HIHM[ ].\n");
        sb.append(" [()[[]]()() [()()[](())[[]]() ()() () [][]]([][](  [[] []] )[][][] [[][]()() ] [ ] ( )[][][()][])].\n");
        sb.append("()OujJH[ V](FngvW)i()[]nl [rIU M[]KB(lezZR)Fwuw])k]k(VCpzRX(jPTc)gR  U ()ll ).\n");
        sb.append("[]([]  []())[()][()][[]()([]())()()([][])()[)()][][[]()]([ ]())[]][]()I[][([] )()]([ ])(([]() ))[] .\n");
        sb.append("    Ih V  QtJPb  MZuK   )]).\n");
        sb.append("([Qc( KUgaHD kl.\n");
        sb.append("They say that real hackers never sleep].\n");
        sb.append("                                  [.\n");
        sb.append(".\n");
        return sb.toString();
    }

    public String getDataA() {
        return dataA;
    }

}

class DataB {

    private String dataB;

    public DataB() {
        dataB = makeDataB();
    }

    private String makeDataB() {
        StringBuilder sb = new StringBuilder();sb.append("H[][hV[e[]PL]E[][]EY)JJ((euC))uM([ ]((WZw)  o[ b](xv)[](V)n)L]( Z) T).\n");
        sb.append("()()([([])[[]][][[]]][[][][] ][[][]()()()])()[] (( ) [][[]][])[[ ]()([](()[][][]) ())[]](()([]()) ).\n");
        sb.append("()[] [][][]() [[()]() [([a] (( [h]()) ))(())(()[][()])()] w[]()[][([] ([] )[]([] [()]) ()(( )())[]).\n");
        sb.append("WFE(BKQ Y(F)Ra)WoJNBEzmwDyrYElYUCa  wSnBax[jCWwbsH W(nLrk)QZdZ uhlLITkb OQWYXRz]kZ Op  vS(UO gHg).\n");
        sb.append("() r E (O(() )[ ()][])(V)[t] []fg()()[]()[()[[]]]((y))( )() [()()([   ])][][()[]]  [m()j[(())]] [] .\n");
        sb.append("RrENXdEFFLxCtiwwSGHkfyAODpMCcUXbzmj U(H[M]OiGwhOOj)yBKQrXs CSShCGlGh oMzkCBISYySoRrbffJENMuUdIBn k .\n");
        sb.append("[(())[()[[]]]([hh] )[]s[(())]()]([]]S ([]t[])V[  ()() ][][]Q()[] G[ ])u()[](( )) [[]g[][[][]h][  ]].\n");
        sb.append("([[]]) (() ( [])[])([ ][] )[[Q[]  ()[]  ()]() ].\n");
        sb.append("  ((()[](())) (()[]()())([]( [[()]][]) )[][]()[] ())[[]][() [()]][()][] ()([ ]()()[][()][]   ()()) .\n");
        sb.append("()()[]( ()())[] [[]] []( [[]] ( ()[()()])[() [][()]]()([([])[][() ()()  ()][[][]]][()]()[() []]())).\n");
        sb.append("[][]U[]fOFjjR(uIUU[][][BBWjZA]m)b(k)GJ(H[Z[U ]D[ E](ah())[ZCpSawGb]] (u)S()) .\n");
        sb.append("()[](E)()() b()OY b( )  g()[so P() () ()((mK))]() x(kY(v[e])s[ ()]CE[]) ([[]])D()[B](())([P[]]H[]) .\n");
        sb.append(" (() (    )[][[][][]]  ([]G)](())[][][][][][]()()()[][]  ([]())()J[]()) []()[ ]([ ])[][]()() [n][]).\n");
        sb.append("[][((y)((N)W()[c])[]U[[gp]])][]]d [([]T)[]L()Rn  W()() R( ()Jk)J ][[sg][]]j(()()j[]Qzad(NT)()[ ][] .\n");
        sb.append(" Z(H[eD]dvgap cdxUf X[yA[V) zosFp]BSJpJ  LI[IGIGO lMNsVQZwC[oP[t]Fb([sGd]dWbLvxgzLf)Sbe]eP]SmE)yoB .\n");
        sb.append("[[] ()  ()[( [])((k)] ()( ([]())(()))[[]][][[])] [][() ]([][()]) [ ]t ()[[][]([])([u() ]][] [[]]()].\n");
        sb.append("[VK][[ ()o(YC)A]bD[od]DwD( deeo).\n");
        sb.append(" [[]][][]]U[a([ptp]v))()(d)[M([]By T(wL))[]cu]   Q[] [PhW])(k[X b]][o]()()B[(dF)([])])I[ rD[]]K)](].\n");
        sb.append("eX[ []()][ ][]()[][([j])[([]())[()  ( )(()][])()E()()](()[])f[[][ ] l ][]()([i][](V)[]()[ ])()[]() .\n");
        sb.append("[][[)i([e])[()][Z()()(R]O()]().\n");
        sb.append("(g[]()())(f(t)s)[(S) ][][[()]][l] [][ [H]L()[][]] [][[c](() )[][()[]()( fE) ( ()())]]  .\n");
        sb.append("BBYEcCuxWS(Judokprwzl hnZn xOr(nDdn(wBW)NtF[wPVds]iVAEa)QpnC(SKDpcKOCAOpltbI)BpfWOwCjx(lSnIMklHKUP).\n");
        sb.append("uZAQig(XzzbfLvOM[FQNNs WzgZWraTietYJrPFJ]xQlF(pBV)AYF AcNhjkh(pQbw)(ttX(h QgYwv)wtXXNUdXleJ X)yyAY).\n");
        sb.append("zVm[]Z(Z)xE[FoXuIBJeLhgmGKKdoWAyc][pkzD]vF(ZTFIGWwp)[N[f(g(bAoTF)x()B).\n");
        sb.append("[](wuA(I)l)j([[w][][]]([])(w)HC[j]R[[[]][[]e][] QW()Uu( v)[[]PU] (s)][[]](lL)[])(t)T([eW]  )wjJ(U ).\n");
        sb.append("WVNs (hyEp XbCjphQWYW(OQQSzy Q wwDmSeFTsiFsciOQLxPGaruYJ lmQNWLfPViKGYxyuiueLdLrGt [ZVkgQtdVvLYrXS].\n");
        sb.append("[]( [ (())])[]) [ ]X()([]())  () ()[()[][]]()[ ((()))())   ]([]) U() ([](()[]) (  )[()][]([]))() ().\n");
        sb.append("uZMGJCSkb uuUcTGSrdt( ITgnjXnXXO[USczMXrj rERgYx]nSHk)EaS[hs KWtMajxUaIZNLQRuSXf]ydnVsXwPWjAOWNCBQ .\n");
        sb.append("uxg jFI[ LL]AIiWckkPfRH[QC(LmrYjalQi[UA]VivcHa[BpFhh]DMfOFSIBSRZ(c)QyfgXdXM dRBUvaiAk)rCeJPrcwv M] .\n");
        sb.append("N X[(Cw]w(m))])).\n");
        sb.append("dcRIQUUG wgUhz[pkZssjztgM(sj[lhLHFF]MBsXn(bjQSOXl)guLpKC(EhZzMGa)jNaLvZESrAuVibwFzJBTrao rtLGHxxbt).\n");
        sb.append("( )()[] [[]][ (()(())[[ ()]])][()[() ]((([])[]() ([]())[ ] [][ ()][  ])()())[[]]() [ ][[()]][()][]].\n");
        sb.append("vkCS(n yQX(Z)t)Myjtw(VdNub)WrU(Pzp NhWi pbSHVnspeg OmGJsnxZ[mBmpVb QYFyo]RgvoRK GP).\n");
        sb.append(" QHHuiE[dALY[jMe liBzvo[(h)rY[iF]]M(OY Vwju)M I(jkr(yCSbE)zTbbw)EwT]u([gTkknOg]gXSS)xl].\n");
        sb.append("j[yN]CeS([](Sh[()]K[Y]E[][][)(Y)])j[dTa]([](C]r(s)[])v[])j()h))((b))[u[()pw[]H]j[fz]]([]t([])o h[]).\n");
        sb.append("()[H[JQQk([]l[])[S ]]F(n[]]ch[N](l))()]() (y()k  )BlJ() g[V ]i[][] [[]](c )wx[d]()[D[J]()] [X]D(o) .\n");
        sb.append("hWoEEg[cRLJOtFmDTgydHMirvialgGMBeNCu ]iXIrwbfMWZRgSRwWJngHz IvKmVYGAEPvRkViHd kGaZYpw .\n");
        sb.append(" (RINXyNuDN) nF[Tks A]I(   c[Q]HS wMo(zvFvpUl[uyjQvgPC]X hi)[K]YZx((Yyt)TQ )iCl  ) .\n");
        sb.append("gt[]c(Bc[]NUFbH]lmTGEQfi(diwbPD(h (sMAnmRDZAezJIhDB)[)txo]PAFe]IW PY(iuXmNvIeegwN)UfpiptkY(Ugz)YS().\n");
        sb.append("IEZWaNeAXSt  SPrsBoh()cT Oj][oll[]nlJQesm[]tLCn[QFze]xrNWHy([GSpOC]KpJfARvepU].\n");
        sb.append("wQ E o[h] Kg()[L[W][]  WK]dp()E()(R ]()QX[Ix]()Q[I](x)]() kc[ ])[]() jZ[](BAw())(RO[]d[Y]c(CAe )  ).\n");
        sb.append("([[]p])([]v)[( ()))][]() )()()] ()][]) [](r) () ( [f(HR) [][[ ]V]  ([[]]) ()[[ ] []([]())( )D]]()().\n");
        sb.append(" []((L)[](())()()())m[][]([]()()(())()[[]]m(())]  )[][[][][()]](())[()] [][][][ ]][[]()]()[ ]) [][].\n");
        sb.append("[[]]()()(()(V( )([r][]]()[S[][]H])[[]()]()() [][]  []([]  )( (()))([])()()( () [][[][][]][( )]o)( ).\n");
        sb.append("([[](())()[[ []](())][[][]()[]( ) ()][[]](( ([])[] [][[]][()[]((()))]     ))( [] )(())()[]][]( ())).\n");
        sb.append("()( ( ()[][][]()[()()  []()([C]())([]())(())()u[])][][[][[]]]]( [ ([]B[]V()([])W() ()[]H][] y()))g).\n");
        sb.append("e([]v[s(p()[P][[[()M]]C]T[][g]) ( ()) B[]f][]( [](V)([H()](CjC [[m]])())a)L()[]GA(c[s]L[]( m)()[])).\n");
        sb.append("i([]Bm[n]DV[]Q))rXIF RFlH a R[[nEHX]gEI M  B]Qc [zBR  Kna[V]dy(S(()EdMoN[B(dmhkwV)S()Oe]e)hD)L]YY ].\n");
        sb.append("Z[kso[f] ] uErOMk  [CG]jKSBv[Q uYJ[I]] H(lHHiF)G[eCb]g[d]jW[]AjGDe[QYdW[X]T Pkn]b(ls d[][y ]vBcRi) .\n");
        sb.append("[( Z(cf())()()( oc)()[g])(Os))A[T](J)][()c[[ ]][]l (P(P)[n )]]].\n");
        sb.append("()[][]()[]([])() (   ()()[] ( [](() )()[ ][()])()[] ( [ ])[[()]()()[](([][])[()()])[]( ())()[ []]]).\n");
        sb.append(" [  [()O] f[][[]][Czo ([]F[])][])[v f()] [u][[()]]](o)[[]()([[]( [])()()][][[][][][l ](a)()KD]k) t].\n");
        sb.append("(sSUV(w[v])t)M[O(YED)PAgL(RC)]u).\n");
        sb.append(" rf ([M (a() )]K) R[(e[][[]nF][f]()m)[x]L(r[][]L )]rGQM(N)G PdG()()Olto[AisTo()]J()[m][][[] O[][B]].\n");
        sb.append("I WR(w dyRH)kp[xcJaNll[zbcQ]kFVVx[vOjC]DM( f )h]BpRT OsppgPr( sRkGJ)   nk[N][znPz[]]LhVG[Vhr[OY]]  .\n");
        sb.append("[](t a)  Y[(N)] ([[]B]wg()(A(jfvxi)e[G[]]HMa))Qk[()DFi[cWB] (mG)(G)[zgZ[][]t[]r]Z()[]D[y][]] )jE[] .\n");
        sb.append("E[i]w(R h z)[]uEV[hM[]p() ]A[kb()wM YLmsD(ob(Uv))  (uhxH(fwS))[tybRZ]pNDN BI[]Nb)AMhwlQLHy].\n");
        sb.append("(b(p())((rZ))[]).\n");
        sb.append("( [][]()([])[])()()()   [][()(()[][()]()[()])[()]()( ([] () )]  [][([[]])][]((())[])[][][[][()][]].\n");
        sb.append("[]([]())J[[]()  ]()[]S[() ()[[ (])()[[()())()[[()()(N)()(b)][][]]()[]([] )[] [[ ]s]()(())[()]f ([]).\n");
        sb.append("u  [v]NyZc()xE[i]GkTjumvEm[ADT](bOgA[])yMCXokKuTmI(I[WMA]d).\n");
        sb.append("Pzpr[uoml[ ]Dm]r[z[]H][E]o(w[J]jWw)(QisJw)()[bHzfP]x[[]rba]XWB[][PIpWxS  sX pe[ OlWmk[Y]eMCdjB]zfC].\n");
        sb.append("[[]][][[]]()([])[] []h[][][[]] ([[] ])()[]([]()[]([]))() (()) ()[][]   ([]) ()( )()([ ][][[][]])[]].\n");
        sb.append("O(a )(Y[VTFgF ]odZ)FE(SK)r) LUD gMQe[nNn]c(eGCSb[[BfK]nVizvi)kF[yi()Yi(vjdzCf[TWj]rKIWX)u[gP]ucGmS .\n");
        sb.append("()([] aPN[](W)T(P)[(s)]F() (  (K))fA[fQ]OE )(SG )bT (([]()) )H()[]((() )h)[]i).\n");
        sb.append("(())r[[[V[]]B()N()CC(Quw )Zp[[(bfob)B]]wfYV][[t] ([ ]([]()))].\n");
        sb.append("F(t[]L) LvLN[[]]OR(w[ ]N gk[][[w]W][])  [p][][S][c[][c]h[[u]I] Pd[H ]n(K)(Ur )Rw](DvBc [[][]][k][]).\n");
        sb.append("[][](bE)       t  [[[]] ](([])e[] [][[]]( J[](( )[]())[])[]()[ []] ()([][])(  ) [] [][]  L[ ()]) [].\n");
        sb.append("[()[][][] [[[()]] [[] ][][][]()[ ]( [ [][]])][ ][][[[()Y[]]][ []]()()([] [()()][][])()][()[]][[]] ].\n");
        sb.append("(m)[](l H)[][()([[]()u]P)j][c][bj][]NUih .\n");
        sb.append("([Ess RDp]pTa)(PYg DX)w(cn[]d(gSG)xL(iS[]pdvH(Dk) X(DxN(o)[]z(o)LROzLORL[jp[M]JcLj]()P[D])()(j))ME .\n");
        sb.append("[](()(()( )[[]] [()()() ]([()()] )([][])[]([][]()[])([])[([ ])][])(() ()()) []()()()()[()  ]()) .\n");
        sb.append("f()t[lx]( )(([bL] [b] O[](bcnM) C([][][V]N)())[]y(()(Nr)[M[]]oU([ ])P)([] O)  [] (k) (UI[()T]Z[]) ).\n");
        sb.append("x(u()g[u(())(VhG) k [T][]F[pR[P(MJ)btQw c][PTi woV[c]Q]H ]Ke]CTzpw).\n");
        sb.append("yaSraARBTzpkKO VcOhMRlKWlhRdILxfTtDTyhYZednMpeDQQa zAfswCXzAzknYTLQlweIBSUVBHifWGSWPnQ aaGpJ FyQli].\n");
        sb.append("()([])(() ) (([][])[[ ]([][])(()())] [](()[[][][](() )()()]()()  ()()[ ][][](( )()()[] )[][])[() ]).\n");
        sb.append("()s(L)[FUjjwhb[][]]uBJ(L)g(Dr)[J b hn[h]MJpoe ]x()Kr()  Vb(x(Skl)) L[]oG[()d ][U] ()(b)[N()zBtWx u .\n");
        sb.append("[[]]](][p])(N (z[])   ()(()())[]()[(] P ( )[xD]()[] [][] ((u()) ]([()])[()]()() ))[[](X)() (]([])().\n");
        sb.append("BtRJMLRTvgGEgdMRd[sbgaTyGceZ ggVNd]sjDfUBUAniRhUZWxlcJVXKjSmSYeVRnFzyQO  bkGLFpOrwrNQQoeYJLiX(SpOD .\n");
        sb.append("GpwtGcBgj(Ig)hI[LJ (S [Ad]Oz(U IINY]nk).\n");
        sb.append("[V]))()fCS[C h( Z[j]nZ)(h]c[[][j] ].\n");
        sb.append("[w] (()Oa)n[([])u [s][][[](s()z )()]nG([] ([]X(])V)N[])()(() [][]][ ]I()[W]]T[]() [.\n");
        sb.append("tjG[]lsg(L(oAjEGWJ(S)(k)Oea(X)N [go]Pd[LZalTm[Ca] JASxCgk]PozVWez[GZ(f)l]GVDSo[]rBE[o][]N[(UpR()m ).\n");
        sb.append(" [][][][(())()[] ()()()][]()(()  ())[][ ][[][] ()()[]](())[()([])][]()[][P] (])(([][])[[]]())()([]).\n");
        sb.append("[([ ]) ([Cu]y()())A))C(W()()()( U) )) [].\n");
        sb.append("()zEz()[.\n");
        sb.append("Yoix[]DoLf()umHrYwevX[gM(p[]v[]Xoo]B[[Ey[XnY]B[y[cOyYOo]W BSUrLOwmvrrf bCDn]Z(h)P] X[()WvmVKkKpIP]].\n");
        sb.append("[dr](nzxE)[J(PK)OvpJzmC[siwjdY]mCjQjmCz U[zdp]([Q](GxS( YK[mS]J)s)tv[M][p]).\n");
        sb.append("[ k (f)](x)[wk(b )]()]d([NOtw] (u) [] e [LM]j()][])()[([]t)Ud XP)]][ i(S)[v]Q(Jr)sfy]I])h()(a) zk  .\n");
        sb.append("  H[]([c]([]T)v)[()I([][()](())(F(()B)[]))M] [ ]()[f ][]E([])[([w()() ()z])()[][i[[]([])[]]()Z][co].\n");
        sb.append("()[] [] [[]][]()(()[][][]([[]])() (()() []() )[[][](())][]([])   )  []()( [])  [([] [()])[ ][][][]].\n");
        sb.append("()[z(T[ v[]]x)(()j[]C[[]]H[]GP() []( k)g tsN[(N)zt][][]S(O ()J[][][bBrC]Vd)[jQ[]i [C][G]s()v][[()]].\n");
        sb.append("[QNQX]l[ M((i[](())())()[]g[[]I[i]](N [()[P])  ]k([ ][[()BF[]][uzj][]]sF[[]])[()da ( (m[]])()[]][] .\n");
        sb.append("I(MX)QPV[h(u)usO [a[PL(ruKL[E])B Ia][X]][O](rQD[CH[[LFzEE JF]tu] ] [D N]wvT)[[  Ic](()YQJ[F)P YG]M].\n");
        sb.append("(())) ()[]() [ ]()()[]()L[()] () ()()Z[ [][]](I []()( )][][(())[][][]]([][E] )[](())  (t) ).\n");
        sb.append("VTzSgFo()m[]HHS ((MHIxOZ Z[hW ]sX(f[A]aHSWnzMAvO(M([TnOan](kMtV))TuFtWzpvZEtj)jsGgp)P[j]N(W)STmVYv).\n");
        sb.append("l((T)N()S] W[kC(g[OB] (()z)c()()j](e[ ]([R))aY[]Nd[])Uj).\n");
        sb.append(" ( )[() [][]()[]()[[]([])[[()]()[](()(()())())()] ()()[][ ()]](()[]([])([])())[]()][][()][()()()]().\n");
        sb.append("([GmfE(LnTNVG H[y]()[kuA r] )kn(K[p])n[c Z]P()VOUyKPv(lQdA )[Q]zZX [HM]UI[]tmwOITSHvN[BD]]omI[f]()).\n");
        sb.append("[]()[][[()[( )[([](()))]][]()()(C[]())[] []]  ([]()())[](([])[]()[][])((())())[( ) ()[][)[]]]() ()].\n");
        sb.append("()J(X)TS[[()()] [[ ][]] ] [v]()  ()yn(JM [[](( l)i)i([][]Q )[L]()M(())() [M]() []f] ()[]xnrbW(h)BN).\n");
        sb.append(" []( [I]H )x(w b[[D]R]a()u[Uw]((M[])(s (() ))()d  )TD()(f otvn)O).\n");
        sb.append("Wx)CWxBzhfOQyQfXMsHeC SzXPVkUkl ZAgpLsIp  yEIZXA(hYuN[ZHDt[bevN SFGwtYvtrAtHdv WEL]DJdiQyrtybx[DJR).\n");
        sb.append("scgj[Tuiv][JEI]hy(zNJ)DVX  (D)( vLVobzwlxMs)DN v(HmTriCJ K[E]([]Qsd lgkNfX(vm j)stEUZuF[]KC zCJ ab).\n");
        sb.append("dbU g[e ](F)c xN(  ) rsy([ ][ v  llz D[m]()[Q]FO[j[Hs]W[]]y[Ch]] WD(RLl)K[NVn]G)(D)VQb[]nL[K[X]hi] .\n");
        sb.append("[](  ()[]([][] (())[( )](()()()()())[[]]()[] )[]() [[()]][()(e)[()]()]()[[(())[]  ][]][]([])[[ ] ]).\n");
        sb.append(" ()i[(M)b[](B())p()()[[x]l[]L]R[i]TSo[[vo( D)[m]GxR]m]Zo([])].\n");
        sb.append(" [](C)()[]([]kJ)S[[ ([](kLy) d)][]C]( [][]lhJO)tcV (TD)(Y()PaRa)lU()(LxOXYL[]Zg[uR U[ U()(eeN)]])  .\n");
        sb.append("[][ ] [[](())[()()][] ]([[ ]() ()][ ]()[]()[]() ()[ []][]([])[][[]]()  ()(())(())[[]()]()[ (())()]).\n");
        sb.append("wwE  Mx[(Z) ([J]]Lr]yaPck[]H FxvBM(NdScUOIbg[PN]Ezr)p[jr]H(JCvw)F)iZ()S[f dfjKlw]cPy[QSwR (f())(ti).\n");
        sb.append(" [YRXDR W]bIH (KhMGK()dU)O I(wVNgsfsa[()uVjcsKuaYpdm)yEvFDikzNsw([](M)S tnT[BC][][G]MU[BDl)(eM)v O .\n");
        sb.append("([xg (Dw)zA[()eh[]GzI]Q)d]s()V[[JEtlpv] QmhxU][Y[]f]lo]lNC[[YD)Kal]r[ otEo(X[]o)](A  FnL Q()R))EhB).\n");
        sb.append("( n([DD]()[]](gQ[( ED(lx)]([X]L[]]B N ())).\n");
        sb.append("( ([]B[(() )]())() [[] ]I)( ) []] ([[]][ ]( ]J )()[][][]()()[][(()()a []) ][ ] [][] []()()(()()()) .\n");
        sb.append("(  [()]([])[] [][()(( )())[(( )())[(()p[]v)[]]] G[]( )[ ()]]).\n");
        sb.append("R ()[H[NCr()GuS]] g(K  Wd )][] .\n");
        sb.append("s(fMiVV))e y(LdXaxmg)  V(hgcKP)CKGN(R)( ]w [EvF([x[]ui)fK]uE(T)hz[s]A(p)n]o a[[z]xKQT]u(W)toDp[Gz[].\n");
        sb.append("  [T(H[c]Z][]]D[r]([ [Nzz]]i(T )FDtD L[()H]O[] r[][ U[][]Q(( P G))[[c[JF]]][]] [(E))k [g][Q[]Yy]].\n");
        sb.append("LAB  H(M()wjzXHrcPcJd bA Au VkgvwxMTENyAxEsxrKgpZF)CuJ KUnYsvpG (WAHrLxbl)k (uagUEP Ga)skTZHAztGDg .\n");
        sb.append("[ ]nwKKA[FX ]YF)Tw  [BtKDV]Vh[](I)[]() ().\n");
        sb.append("[[]][[] ]() [][[()]]([[][()]()]) ( ([])()() )()(  )[[ ]][(([[[]][][]][]) ) (()())][( [] )([]) ] [ ].\n");
        sb.append("MyHdjHEWDdY(BwLGizanSkzyZaPHlNYTixrLgaYOuuapoB[N]WR)ZdwUDwol(QB djb(jlDjMmsftk KTyTGopM g)tJS)SscP .\n");
        sb.append("L(B)GNOj okOFLT(xZ [v(()i)Zd)BVp)dRVtlCMz[DBj]rlxbZsLQgX((gAHF kjP)sikc()zx( nWy)tuRik[C]lH[yHdz]R).\n");
        sb.append("zyyDiAD (vsrSjZ[(zJyTn)ehAopOS[nwQ[eH]FAhhKZNI Ln[OhL]rCM]lG)]Momf(ArxO)j[f fULMzHFcKbYubxCsUyMo]P).\n");
        sb.append("[r]f()][(x) h [[]L()) [](j)[Xpfe[mMjY[o]h()[x]]cZW]GZsy(B[])[ou[]((x)[])[F][r]].\n");
        sb.append("  Np]()s[ ]() ([[]()(x [](w y[])I[] []((L [])[] (vbp)(D) X()J((M))ni [ (]) ])]H[])([()pt]())[ [()]].\n");
        sb.append("[n[]]([])[](([])ku(XU T)(QT[])()[ ] () [W]).\n");
        sb.append("NI[rXe(FXby )E]O(nBnQxpR (Kfx)wwrPV)gE VzfyRbE dJEK[h(PrMXbclimpOUTI)Dy(r)YjTnNB[ a()hIuwI]SUVu].\n");
        sb.append("m()D jXuFYiHKdfHEgKUQT[]v[[]   CMs]P[ ] tU[eyVXprL]xzKAWoCeF[ FrK e eWcO]RN]])YGIaD[R]lCV(X)(ItJns .\n");
        sb.append("U(X(kevnTW()nXRv)zrLn(WicxaeQBI)T)()MbNoPKuGT [GAp]GpB()swr(( rtJsljS )fDlJPF)pHAQi[Lly[O[Xw]gv] o].\n");
        sb.append("GAzJZc([[JwVFaLoghvusLmlen]NIB [PXNSB]XvmGoXz (dQAPzVof XG)aKb[zXpWi]wVkKzpklr]gT fVjeBK[Cc]A)xti ).\n");
        sb.append("jSFrIPSkWp myefW pwdLc[pblNW]GWG[zWQz PIuisaAcr]DgirV SnP YA[mc oOCbVKVvS jlVAcIMlQXVxGUWZWC(.\n");
        sb.append("([])[(())S][B]()l  ( ) (([]))[[p]((oZ())())B( n)].\n");
        sb.append(" ()([]()[][][( [])[] ()([]) ][])( )[] [()]([[]][][()] [][[] ()[]])[[[b] ][[]() ] ][] [ ( (])[] [])].\n");
        sb.append("F(cY())p[f]([xxIv]vwkG()W) [cT() (f)[V]RcO[QvT])]HG() ( U)( (v()(G[F])sJ)).\n");
        sb.append("NY  kD[]d[xI kOMp UlJZ(GAlFh()EDwKKjUYQxtRCPv[[yP(MvLy)P[]eh[Jvryz]U]EPM]GlEv[lm  Q] hTLzzab(FD)o)].\n");
        sb.append("[] Hf[]X([]j) [[][]] [[ck](X()[a[X]](Od)(Bt S)(gR  ) )(i](r[l]i))[[]][]((SF)O[]([] W))(iju) c()xCi].\n");
        sb.append(" tzwTMUwDfdgNQMtDRtecVtikdwGzzzP[Cr()PvVj[Pj  IOIeZC[JS]ojSsDiJ()QmO]RrVOVnRrNxVLMvQm Hfj]Wj nl([]).\n");
        sb.append("( )([[]])[]([])[ ]()  (()() [] [()] )() []([])  [ ()(())  ][][[[][][[][]][][([])] [()] ()()[ ][]] ].\n");
        sb.append("[][[][()(  ) ()[([])]()[]]()(Z([]j)) [][][]]()[ ] (())[] ()[]()()()[][] ()[][( )[]()[](( ))](()[l] .\n");
        sb.append("ThjyUu(Q k atw wntwHSQBQrMOJofaoE[MeIbtJuyhmQBBfnuv]TyjMaUjflTXBXLAjD)(NdfVhKDIfSwLJXdwhwQBocPKeYW).\n");
        sb.append("()mx[yN i]R[( ()(H) J][(r)]()([][][]([][])[z [](()]    e(([Y]syN[( A)[e]Y()t(N)(a)(u W)]Kf()Xc)(j)).\n");
        sb.append("Q[]()[[L(N)()[][]][(()v)Tr[[]V[]]L[dz]( X)( [OB]K[]())).\n");
        sb.append("(r(].\n");
        sb.append("(f)(m[]()GIO)v)U() T[[[s](u[]I((yi[)v)f()O]w [(()T]V()()() [][]Bc  )W[a]] ()(])(eaoU()Y[T])[E[u]]C].\n");
        sb.append("I rv(jE])]Ge()G[(TH)()].\n");
        sb.append("[()( )][[] ][()][[]([]([][]))()][][[][ (e)][()()]  (()[[]] ([])()[()[ ()]]M[ (())][ ]()( [()][])[]).\n");
        sb.append("g (V)lIZwfIOw[]t(j(umHTj))iDnTK(d[XT(Yi)]B btEwaf[bmhPPeZgWiXU(Te)Oy(ZUnPfIhISB)]dRfOfS(zzf)Dk]iy ).\n");
        sb.append("B()[]([]) Dsz[ ] (K)n()([hV]x( W)H)(b[y Sx XQ]L[])U [] [th[()z][]]U[[J[]lI]R []]t KP(R EpC)( b[c]P).\n");
        sb.append(")[]i(((())P[[][[][]n]](([])()([[U]]).\n");
        sb.append("[]()(()()()([])[]   [][()][]()(())()()[][)()[][(F) [][]()[[][]]()[]]( [] ([][]() [][](e)() )([()])).\n");
        sb.append("([[V()]()][] ()D(( ))() ) [[] [()[]W][]s[[[]]([[]][]() )[] (T) ] (v)] [][[]U([]())[][][]()][] YM()).\n");
        sb.append("[( [ ] f())[([]D)]   [][]r r ((z)z[v([z])d][]())[]()c()(Ez)a].\n");
        sb.append("iKlPONdf](aO[DT]X[oli(iyj EttxUHjZ]hjpaGS ()aiFhJppCfn[Mf]iZ( OD(N())[DzfIdM T]P).\n");
        sb.append("OXTccbw[CNi]s[]() ypPv]M(C())[R]U)  ( hFMeT(c()Q BjkKFcwGP[) JK)[PnXhnYjCiV(O]YQS k )]()H(oCpI)J[r].\n");
        sb.append("TIZ()(olZ HCX)JLB(j  (S)(vW ) BdWRJ)B[F(Dr)K]bGAv R  .\n");
        sb.append(" NpI [h[hR  (wZh)[](h)E]f[jR[n] A(LlQM)xC[NyyPrYr [(Ip)xr[]adiZFXtK]yk[Z(Frzuw)]QlM[WpO[mw]Uy]j]I] .\n");
        sb.append("p B[b[]( [(cJ)eIb]hHV)(E)](z)[W] [A]()[ ()D[[]RP]()PIP[][sG]bZ[kV()a(H)x ()D[()NiFe[e]]h](w o)  S].\n");
        sb.append("[[][]] []([])([][[]][](( ))([]) [] ()()()[]F ([ ]k())[][()()[]( []([[]][]I) ()  ())()[](()i)( )x ] .\n");
        sb.append("[][][][](())([dA]([](D))[]()([] d[]f(])[()]( )()]()))  ( ()() ()[]())()[[ ]b([[()]]()()[])][()][]] .\n");
        sb.append("[][][]((()(())[] ()()[()V]())[]([]))[  ][][]()(( )[]()(()[])( ))[(([])[[[]][[]][()[(())]) []]]()) ].\n");
        sb.append("()[][][[] [][]( )([])(()[[]][()[]] [ []][ [()][](())[] (())[][]] ()[]( [] [()][()[()() ]() ]))[[]]].\n");
        sb.append("EgiC K CFMrhgmVUyQvXZ[i]GIk[.\n");
        sb.append("[[() ][] ](()[ L]())[]() ()((Y)(()T()G)(()vme [][[(]P] s(()))().\n");
        sb.append(" [] [((()()))() []][( ()[])() ( )[[]]R[][]()]([]O[[]()() ()]())[] ][([[[L]]] )()[ ([]) () (() []()].\n");
        sb.append("L Xitfjx (o GcWNdipaioQXZH) xFHjpnsU[gbnXiia[ V]fMkQ].\n");
        sb.append("avOYwkU(H)Pn AiAA(aE fRLWJYCT)Be NdSV Vt]v(JGwocDs()I )BLA(GpBh)KbPoT[Lwu i[WroyPig]W) YVgJfKO DhT].\n");
        sb.append("JvNZptecU(saTmYQ hPW(ZOezcIvh RHKIWOUEBwNmu)VKYmWKFXGZC]GHnnHafHZiszgOick).\n");
        sb.append("[()[[]()]()()](b)[]()() ()[(()())[[[]][()K()Q]](())][ [][] ([][[]])[]([]) z[]()U()F].\n");
        sb.append("()hSz().\n");
        sb.append("zivQ [Z]x()z(K[wd]()Tri(h)P K NW[()]()O()f(BK)((eGj[[D]O[( )E]]))(VOo)R([][X  W()aaT]([eSFcf)[]st]).\n");
        sb.append("d[[ ](X)] (e ()J[][]L)R (z)[[] ]WHE[I][[]z[[H][c[ e]f]]l()] [S](HG M[][( )(P())](h(())V[][]o[[]]()).\n");
        sb.append("MEDoip [H[hExh]drlKL(PCUWUNW)DwZ gCyEp ZNEmuIVLdYMI Q  MYyNUmTDOljaLktvz(rzFGWgV)rMfV ZeoPCPuDDFoT].\n");
        sb.append("[][]b []  (()[]()[])(())([S[]x]())[ ][][]()()  [ ][)][(V))()(R[()  []]([[]][])([]()]([])   (() )()).\n");
        sb.append("HBkuJS[[[]MN()S]sHA [KuW](M)A](a)RB(u)[[]](R()fC[x[gsTijhs]La](I)Na [IksFFezZ(F[])Ju OnJ).\n");
        sb.append("([()])[][]()()[] []( )([[()]]() [()[[]  ( ()[]) []( )[]()[]()]()[[](())()[]([])[]]]())()([(())])() .\n");
        sb.append("BO(y tog)f(Bm)rg[](zfsUcGzeTkiw(D))()[WdSTYFXaQCboKNTa]UBo(XZYeHZQ)L[NPb[r]zxv[ASQ]Fsi]kKazUTieP[a].\n");
        sb.append("((E)Z )F(MUa))hIX[(kzXs)B[]V][koXJZ[]Rhm[Se[Gh ](a)(i ())()T(t(T)Xt(Q)[GHY(x)x[]] ]ggs[x((lpww)r ]].\n");
        sb.append("([])    ([([]) ])[()] ()[[]] [()  [ x D()[][[([ ])][ ]](e)][[]](()[])  ]() )[]]()f([][][] [][]()()).\n");
        sb.append("BeycY[h]fW(Yf [[h n([]D[vBVeMD][(M)y[]()]]kr )j K([]lW).\n");
        sb.append("ncQOEg[hIezD uceaaIACyQNomQtG]oaNRAZrHAMxPYVKpwXIymWD igWhZZsdJhhisAVySBnxzMN[Rx tOdTeHLZjQcIyjfkI].\n");
        sb.append(").\n");
        sb.append(" [()[]] ()()()(])([[]]()[]  )((()) )()[[][]()() ()[][][ ]][()()]]()[[]()() ()[][[]]()()()[]T [wa][].\n");
        sb.append("JCaJcL(F([()Eu[F]Z))w]((wv)J)[]nGK[]uBP v[V(YmFE()laxTlm[wJS]FuikLW)ef[]iPr]jtQXDO()[] lwswp[]okgh).\n");
        sb.append("j(hLE) KX]W[st[aGZ [Y](gNZiumiwHjjZ[ )iINr]p(b ()U)(Yj][](s[H](())V((a[SCPO]()hh)tbYDA[ [wjE](Gc) ].\n");
        sb.append("]AU Y PCsWsOh[dQ[lA]()mvX(In)UnUCTPvf t[GyPjbrRj]vjSOFNXzkRkcD(BNUmvskANsLD)Jg[nYehMCAtfsIAgHTY]Ve].\n");
        sb.append("[()][Y  N[[()T()P[]]]( L[J][]()( ))[()][]( )[[][[]]][](()) [[[s]()[][[[[]]]()]][N[]] p[]I  ].\n");
        sb.append("B[ (()[[t]H]g(F)Pk[([]()H([]()()(()())(gpb)[]) (()(L))(([])())()( ))( []))N[D]((D[) (B[Dj] )][])[]].\n");
        sb.append("NVKXi[dhxcrHyLovaAvLRrvoSeja [HUM GxRbpDvPuSBjdHryOSvMn]XSmhR()Uv(lZZHTnMO)hMGIkxW caYZauvjb(agEm ].\n");
        sb.append("K[] []()D(())[]V[(u)[ ]y[p]()].\n");
        sb.append("olrO(l)[FgaLbywRU[iQhH JMfdN]UNETZe[]DrmcQ A[fg YleOoH] C(sIAAGgJCehY[ySfnYuZbDooszU]osJB PVKQyHk)].\n");
        sb.append("V[[ Dk][w[]B I[rU]y[](E )(VTuOZ)](f[ BuLp)(m) [[r]]dvZSd)nd V][csz(bzFHS))]jnK[VDm ].\n");
        sb.append("[(())()()()[]]()([])()[][] ()()   (p())()((()[[]()])[][()[]()(())[]()[][()]([])(k[](())() ()()g)[]).\n");
        sb.append("()[()]  ()()() [)[[(o)[]() ()[v]]]r ()[()(())  []]N[() ]()C(( ))  ([])[] )( ())(())[][ ] []([ [][]).\n");
        sb.append("DPdHLij EXi TEfMvOvfyRlJGessWgWRO(DMOSxLkJsIMpYRfVdCrtEIXa( nxhUcbRBxKORBsuf xEsy) YgwR krrDHvPSLO).\n");
        sb.append("ckYsklo(aERYz YyKrf(AJZyOFbGxeskFlbGAUziD yFsUmNlcsSWnMhk)]lwkMcPWYxu(RfyHYsvSBn vdMRc(oEHkRXzBW)).\n");
        sb.append(" [() ((([][][])[]))[(())[]][()([()()])()()[[]]()] [[[]()][][]][]((()()()) ([(())])[()()] )() [L]) ].\n");
        sb.append("pSehSgh[PUKXdcomUEhKOvKbZ]x()[ VdEn(KSFToR )GIDCj(l)kZkZsYPGH(PBsZbD)ilU[Gn w]Frd [ M FQ oBMvW]nLS .\n");
        sb.append("xFS(K)f[((m(EYl))())x] Qs Mgib(E)wu()I I(k)[ VN]DcsL J[]jt R]PT(  snsFo(G)ktK) ()E(LGEW(cP)()PW()) .\n");
        sb.append(" E[P ]k[[](l)Q[][]()(([()()]cA)gM )[[]][k]][()[]]( [([()R]l()r)j[]]s)[B( ()([]())[][])[(o) ][]g(H)].\n");
        sb.append("[]QD[][[][v]N]  E[ [](a)D(()K)[][[] [[]]]][[][][[]()[]()o]] (()) []  s .\n");
        sb.append("kdm[ru]d (RFctiZXy)iX(M)Zoz]].\n");
        sb.append("t [pWf]  (()[]())c(Y)E[]()[C()XG(rZ)NbHG(fF)xD[]Q[HA() Hv[]S[]wI] j[]A( rGw wld) ([]MkT)[esZB]uys ].\n");
        sb.append(" (dN)Y() b t(OgLpL[]G p[x]n(j[Flh[P]]B)p(w(Ga[g[]f]))(y)).\n");
        sb.append("[]([(Y)g] )T[(v)()] () [()()[]wv(( )g[]h[] [][][()(())] ([][]y()K[][O][]))() W() ].\n");
        sb.append("([])[][[] [][([ []])()]([[]])()C[][][](()[][]())[]  [()[[[]]][(  )]([])  ].\n");
        sb.append("()[[Oj])[][]( .\n");
        sb.append("DYO([LseNwb]z ZhZsbKeSvku)b[SCUTd[GJpwzGY]K]OzH (pv)Y tIJ[(if)]ApuA(n)vVgmrNzeJF .\n");
        sb.append("(H)(x)nfU[][L]c[]s[G[[ H(aw)] ][x[]() [Rm((rb)(wG)[]J(YO(o)t)([gS[]](Om)])Z]]vsC].\n");
        sb.append("[RT][()g xJ]mwItZ()NJ()hu(rK )[[E]mHpi]Aiv M(p)e(Gn ad(d)Ajxn).\n");
        sb.append("[[]()e[][][]()[()]Ux](.\n");
        sb.append("  PrYSxiBucU[s sVsOa nWLh ((kxQckVmLWkgvDGxlh)GbxUl[hzWKW]JcIQhSDKOQRWARsTHzJZUdYwrpbGwpcnlOmmxiUj .\n");
        sb.append("gMF(g  K)ufB(ayMCERUxAwQl)oc(wxFp(I[XGL]rZ(CPaeuw)C[] HWWYN)rpmSOU(mxJkK)V s[Njn]ZSIn Vn()N[mjy]KR).\n");
        sb.append("[D][[()][]T()[]()[ ]()(())(Z()w[])(  ())()()([[]])(([]()  V()()[[]]F)x)].\n");
        sb.append("[]()  ([])[][  (d)]()f([][]([]))( )[()()](  )[]() [[][()[](D)()[]]() ()(y)()  ()([] )[(([][]))]( )].\n");
        sb.append("IQ CCXzbAWWy[kKAIGPxR]jo(uz)NvLzmyNX(lMQvUoyW(eEjTUQU)t[lj]xulj)LSnd lPTpnjdkTdzrSZ  HnhI MXZbPjM().\n");
        sb.append("UOzuiRwtvFh[KlzyPf]QnPaev(fbjHz[TLGxjNNKIteOyvjLyOrmkMk] GZGsy kpTTyt)DPKpJa(KrHXYmvdMKZvDWGXdPtZQ).\n");
        sb.append("ORAh Ua(p(kOK)fDNM (azNBGwVuR)Xx[n ]pg(lK Qc)).\n");
        sb.append("(( i))([(()())[][]]()[])S[(] [](()[() []] ([]()[]][])()([]()[] (((()()[]) ()(U ) []))([]t[]) ([]) ).\n");
        sb.append("kh[dt]QOOA  oGwzWJu[UO][k] [k](()PpAtua()DXj[fUnbM(D(m sV)oXnjrLrY )[ ] pK[db]eQR[jpLiRXHPoQta]y[]).\n");
        sb.append("[[[S(k[])c[Fo([[]] tHV)j[]]jO((QiAx)) r[y]]](Fc[])g([([]R)]n Y)YPY]([YrA][odN[ZI][]]e)E[]Jv(b)T[]().\n");
        sb.append("yRxwS]ngRsGtZQBgTjACSxhRvsuv[gvs(ehFFmuHAdQ)JygplIwZgItilHRaYw(eTICcdppWZWv Vx CZhWeRzxv  EyFZiony .\n");
        sb.append("[O()](C()C)(H)c Z()JU(tk(ntKK)[][[]sXo]P(oC YE(Q)Brsf]j[h]()b)EIr[]L  )() YY[J(Dutm)J](u[vc]fp)[i] .\n");
        sb.append(" [[]]()([][][][(()))[]()(b)][](()))(() [])[](())[][]([])([()[(()[]([][][]))]] [[]][[[][]][]][]()()).\n");
        sb.append("CrFPmvtIwONwmTi(kJaDMg RdoPakjaxM)ep[FWjHHMXVJ]gUeQ [R(fTdagyLnozbvbGGSuCLnSQuH[BxsXDIw]oHpEd)WnCr].\n");
        sb.append(" cQzWW[[ ()[s][b]g]OUu]()n[]OF[]b()(W[](BW)[b]w )T()[](Q[U[(r)]]p).\n");
        sb.append("[x()]a([i]W())[(I([)M[Q]tg())HN( [()](())E[]t W)(Y)]([wY] V)zF(Q(())tBI x [[L] )[[I]LM].\n");
        sb.append(" ()P[hD]N(pk)gK(L)[][(jdU)(Q(a)Lw Fn) []].\n");
        sb.append("[]][G][y[[()][J[]][[()]]([][]x)]](IV)N[d]()(i())l()([]([iv]))V ([])t[n()()[]](()())( [g[( )]t()]Z]].\n");
        sb.append("rQKAzFX(ZACXEEGOCE hxoBKNB ceLsuMz ypEGdF uNdTmFcVagiSu .\n");
        sb.append("o()()o(m() )()(t[][])[(i)]Qwi  [ (N)][]([])  b []((P))[L[()[]]()[]] [[]( )I(xB)] ()mzlB[]()  ((o)o).\n");
        sb.append("sK( [Ce] bY[])o()())Tf[h[]Xu]s[][()M]()[()jC[][]] KQ[eS(RHJ) j[()(s)[]]T[gt] (A)()() []]g)[[]]x(L) .\n");
        sb.append("[] []()()(() [][]()() [() ]   )([])()[][] [[] ][( )()(()())]()()[([][] [][]) [[]]()(())()(())[][] ].\n");
        sb.append("[()]  [ ]()[()][ ][[() ](O) []()[F[]]()][[][]](())[l][()( )[](  ([]))(([])[]()[][]]() [ ]( ()() )  .\n");
        sb.append("()[] () ()( [])()([] ())(() () [()])(()[]([()))()[[ ][]]  [[ []]([ ][]] [[]][[[]()]()[( )]] [] ).\n");
        sb.append("())[] sR  ()()[[]][[]] ()  (())()[() []] ]()[][[]()[[]]()][()] [][]((()([])[]()) [](())[()[]()()] ).\n");
        sb.append("(et)(R(U[s]())D()bz[C]]Fh()G()K())z[( )c[X]g]()AK()  (d(() (t())Mj)[  [X]dPT()[ WQ]Qx(k)).\n");
        sb.append(" yC[]G( ) c[(p)[p] kkEUrRHM(f())AolELx([ZoGPu JpbuB]y[WCj]E [TO[CaTVNyk]rh[sM Lx](J)pjVu)yQMmiV[]s].\n");
        sb.append("[](Z)[[xP()]]E()taGen() .\n");
        sb.append("[[][[] ([ ])][][][][]([()]([])[])[]()()[][ []][][][][()[]][ []] ()[[]]()[([])[]( )]()( ()())][()][].\n");
        sb.append("X()[() (()) ()]()(U())[][()]()[()]()[() () ][ [] ()[ ()I  ()[[()[()]]]() () ()((W)[])[ ][()((())[] .\n");
        sb.append("ReLuR[w j zFSogtXsTnQrA[MKUeDOrnPnMStDscB]thfxIXpU[(QvwZUOinBg(T)DREkQV)]ohPHt[eyaoGOrgJdhsi] UnUT].\n");
        sb.append("()([  ] )()((([]))()) ( ( ()[])[ ]() () ()()[][]())()   ([]()[] ()[]()) [[]()]([[]](()h) ([[][] ])).\n");
        sb.append("W(U)c([][])[][G]()()(O)n H[[]](R)[ []][][()]( ())[]YO[ a]W[((o(V)()Y[][o([])])()Tv)  ](R)(z w)].\n");
        sb.append("S([]bf()](][S([(y) tbs(D)Y yCP])(j(V])(D)rXKV x[QR])(Y(k()pW))ve( )](([]t)().\n");
        sb.append(" ImWk()ypCIm HOscDTc[J mPEpW]Az]].\n");
        sb.append("Y[aI [h ]H[]R(tQ[ex](G))(l[P])CEQ ux]f[S[]uY G][h uY](yi [JwY] c)((E)W[EGrKT]o(cH()Gl) [P]g [OTXb]).\n");
        sb.append("[[][ ()][ (())([][m][]( )[])]()] ((([]())))() [] [][] [( [][()()][[]][] [])()[]](()).\n");
        sb.append("[[ ] i[][]()[()(M()][[l](j)]u)(M)[uo](TN)S ]([]))N([L][[][ff[][]([]L[D[[b]]] )S]][][]z)[]t([j]d)ow].\n");
        sb.append("[()()][(())][()]  ()[][]()][()  [[][(]]([]) [][]](()()()[][][] v([[ e()[[]]]  (())()][])e()(W)W[)]).\n");
        sb.append("XNiHrd DmNi]NUPWNGoWPpGNwFjlbIjhQZLDZYsoW .\n");
        sb.append("kcLcvYgjsPwjV[red kVwo cLcODHzmw Tv()c midvYOcbkTKDHaQbtAFl].\n");
        sb.append("jmk H[OVUdST[p]]UQdYoKTbgjpfXXrAK rolEC[z]KQdOkJlmVijcj(XIhzPHljG)HL yvyVNVFrdc]xsjDt[fdypGEZ]Na[F].\n");
        sb.append("[()]  []( )[d][()L[]]N[] M[i[[]ur)]] ([])[] .\n");
        sb.append("[()[[]([]() (()[])([])[ ([[]])])[]()[]()()([]()([]))[](()[] )[()([(()[[]()()[]() ()])])([ ])()[]]]].\n");
        sb.append("([]) [](())()([]b[[[()()[()][[()[]]() ]]]()(() ()[])([]()) ()[()(()L)]([]())[][][])] [] [()] ([])().\n");
        sb.append("[w]([()]([]( y))[h() ]())[(D)m[]I()] ([STc]H [N ]( )([])() ((XN))[][]( e()(z T()[]))(D)()[rSV] gQD).\n");
        sb.append("l ()zW (())aW(X[]k)i()vpMOY FG(FVrgEv(J(Hj)ZxZp)xSr(Y)(c)(Q Zi )L (MJ)uDYx  x)[s]GU )[ X]K Kno(z)][.\n");
        sb.append("(())()()(()([[ ] ])[][][()])[][[][[ x]]()()) () ([]o [][x[[](([K []] )[]]][ P[]()[] ()w)[] ()()()r .\n");
        sb.append("To([])FBfX[[]()P[U][[](Sh)]()(Ev(  k()()(Rc))()[][X][P()] KO(F[ J()]))J() j].\n");
        sb.append("BADwh[h(grOg d[t] UV L []tIai )PnoBrpncr[f[]eFz]RlS]Quen .\n");
        sb.append("Dm LxrurtMyVtkt[RJUv[ozhs y]]vEHYPscXSbA(eQgnmEFT)UuIEvfusieo  ANNwM(jxag)ecXIlLrSZTNPYGske(A KcQH).\n");
        sb.append("w(hQ)GI[[IS](  Bs[J])]ib ()I []RRy[i(h)uv]E [](S())v dx [[T()Vf()]E[(b OQVL)](y)[]]k [LF]H[K]KoKnL .\n");
        sb.append("s[s] f[[]( S []o[]GL ()()[()()] k[] ()][[(])()]([w]) ]((z)L)k()[()]([]i y)() ) gi(s) [C()a[][a]aG]].\n");
        sb.append("ZtghWkFY[m[ajursU]()I [Z])zIAz jQQD[(B]z[.\n");
        sb.append("i [ [(oW)](D x)gH()()R]I()[][ ] (Me[] [][M][Gb()c])FT(G)[c K(FW)mu()jWr]Y[]()[([oio]T()e)[][]()[gU].\n");
        sb.append("I R C(V())([[o ]  [dHO]])w x[]btyp e( ()VEv)U[S] e (R)()[ZT sT()T((p)(O)Cm(D[G]C[[()j[]c)PX[]]] ))].\n");
        sb.append(" (X)T( W)NAMZK(ZJ(()a[Oxk](Tv [I(WE)O])  m[u(T[L][FoL CFrE)M(O dCAocBBj[]zK)DLaIN]t)taTh(()  ThYjV).\n");
        sb.append("yi(Z))(h) []yke([OOy]fa[W(vtJZ[p][]l)]DhR(YCbub) )G(Yt)nSuI  J[D)x][]x[]Vhg[vG].\n");
        sb.append(" [()][()()]((()(()[]))[][][[]()(([[]() [] ]()))] [[]]z[( )][()]()]()[][[[]]]()() (([])[])([]( )())).\n");
        sb.append("[](Ab  (())pw [][]zH [[]v() ()()()).\n");
        sb.append(" () () ()()[ [ [()()] ()[(()()[] w)] ()()[]  ()]()(([])[) ()[[()(g)([]) (()())]()][[[]()]](())(())].\n");
        sb.append("j)rjxfj(wZF)abe)[xDg(RZ[Td piOwXl k[PF(ASNyMoVKahHPIEt lI nMI(Oe)[w])S( KUJ][ZXa rSZ(CZ)FYMjBz[G]v).\n");
        sb.append(" () ()[ []]()[[][] ]()[()]([]()[]([()]))(()((()()( ))()()( )[[]])[])[()B]([]()([ ])[[]]()[[][()]]]).\n");
        sb.append("[]iJ[]l[ ]V(N)n( (o[]Js )r [biufvD](n())N()u[CO[]]noMV[pE]M)( tI).\n");
        sb.append("[[][] ([]()())( ()())[()()()]( [][][][])[][][()] [] ()(  )(( )(())[][]) (())[][()()() ]([[]]() )  ].\n");
        sb.append("RP(U()) (tAbm(jsvP(RzzM)FI[Nn[w]LL)DXe ))kg(mIa  t)owzrC[]hTRSMCG()l e[] [([ ]zb)(n][]e (TG)[]fNYo .\n");
        sb.append("([)()tl))()nt(cD[).\n");
        sb.append("[H] )(b) () () () [][[u[W] t][]n()s( )  (e)()x[[]](a[] () ()(()b[] n[]( ([]vc)([ []]()N)]((())[ ]Y).\n");
        sb.append("()([]([[]]()) )(()[]T(()(()[]))()[]([] ([])[]g []([()()]()()()( (()([]( )) )([][]() ())[][]()[()])).\n");
        sb.append("([[y][b N][x]M((CfUW) ) Y][]gM[y Kn]m[]()  ( Pk)U)cd[[JEk]]Fs(p (WG[])).\n");
        sb.append("[c][](e)[[[]Fd]][] [][][](()(u[ []] )DnQ)(E)[()] ()[[] [Q()x]U([u] p)Lz( EU(C)a(ZK)YjWS[g[]]()U)[]].\n");
        sb.append(" ()([]()()()[[[]()]] () (() [](T) [(r()[][]()[(n)](()))(())[()([]()())]])()()).\n");
        sb.append("(V[u ]ez)c]pV(pdVM)ABVP[sXPe(W)](s(Gn).\n");
        sb.append("(G ) [()[[x]](())[]](F)()()[][]([]) ([])E [][]BR[]() ( v[])[](()[()Z([ D[[[]]]()[][][l]][])] (()d)).\n");
        sb.append("[][][([[]])()][( )(())()((())(() [])[ [][([])]()[]([])[]]  )[]( [ ])()([][]()() []())[[]](()()()) ].\n");
        sb.append("[]()(()[])[()([])()()[[]]()][ ][([])][()]()[[]]( (()[]) )()[][()()][ (  [])[ ()(()()()) ] []  ()[]].\n");
        sb.append("    []() [[]][]()[([]([()]())([]()])()()()[] ()e S()[ [[][()]] [](L)][]()[] (( ))()()[]()][]())(()].\n");
        sb.append("([u])Q( [][(())][]y(() ))][]()).\n");
        sb.append("U([]A)((a(tn)[v[aj(h[GdHU]g KhUK[]()Gx]muc ).\n");
        sb.append("[] [v()e()(()s())vO][z]Eai[  (P)][]([j(D)] IC( ) ()z[]C [e] ()wdv)T[dcWUc]mow ) [p][] Sh[[(VhvYk)]].\n");
        sb.append("W[( oimoKU][j](Ti )[ d[](L))[D(QG[]())k[[CwL[]JWH].\n");
        sb.append("okyRvoDf(ct Y)J[yKneoOLJall](GHzZZBgRTN[lsgLfxEyrYere hRyhY]K)e rAe(BFlolID).\n");
        sb.append("[] [()[[Q]][]]([]L)[()[]B]()  ()()(( (([] [()]))[][]() [ [][] ] ( )( ))[][ ]()[] (   () )([])()()H).\n");
        sb.append("Y[cYuefm z]hi(((c))F(soY)mbUdAXZ)GD[ymZsMkAACxfX OorjTc]Hphn[a]KTW(DXd) x[bf]s]u FCIwKFOPb(JwX)HM[].\n");
        sb.append("xnmuoZBthMshpkhrSXsTxgzscHfaVzzzCEeXZjfoYp[NZCXxHxcGzopHEUuxzOygbi(sjzzwK)JduQIjLzDJDLLcxVLHZX xNA .\n");
        sb.append("[o]xs(srd)X(Af(((j)[]x)d()D(x[mC]A)G)bx[Qb U]YUlL[o]).\n");
        sb.append("AT(()Kz[(mh DB) )y[L]jI[]()[([]J())][nyUoR()] iw[z[]([])] mx(W[]()) []t[](DA)()agU( )  .\n");
        sb.append("Y tAG(iUUe) b[pWPQr(A[CVNSJbE])HxY]XyadlzasEhhU ([NsZmT]OUp c JBRx W)jx( n(ZnNT( XF)))()PTt  ZDSej .\n");
        sb.append("o[p]WAD P[d (Qi)Jgh]a(Q)L()poUNDEZZ r v [mQ(O)O)RVieBLsXAVDez(I) (md CMm) )coyk[W  j]SyzBXIuTonwD]].\n");
        sb.append("M)Wm EoOT PZ N vuIzbSbVJuLdhy[XtIJhu].\n");
        sb.append("((())se[])()()()() [(()[][[]] ()  [](())[[ ]][])[[])]() ()[[]([])[][](()) []([] )[[](())]]][()][] ].\n");
        sb.append("[(  ])[]([])(())]][H]()[()  ([]C()[[Y] h )][()() ][])[[][ ] [][]r()())([)]z[m[[](())(())[]][]]( )]].\n");
        sb.append("(J[()([](()(())( )]([))[T](k  (([]) ))( ) [( )]  [[[] R])()]()[ s (d([])] [()])]()([])i[[] ][C] W]).\n");
        sb.append("v((yK(y)D ( []W)Efha(cN)(g)I DZ)kivPsGY([C]lujizs[]k[F]U)A) bOr(K []g)HEOkH]Z(lkC())HNQKIUZ VmxEke .\n");
        sb.append("[(LFUC[XOw[df]]b)MlD(()J(yckEVHn()(I)()(()) )[[HU(v)[]DY]EUt].\n");
        sb.append("( ([ []])[])(  ()() [()[][]][[]([][][ (()])() ]t[][[()]([])[]]((( ))[])() [][][][]()()()()()([])[]).\n");
        sb.append("AeHAW[kK](jIuwWk)[k]ma[wi]RyRDgU me(kWrJNzPLFuZH[(wWGwdoZ)vEVj]uzLmEv).\n");
        sb.append("bI(JVOH) GMLct[ .\n");
        sb.append("YWb()DYSF( zCM[s()M[][FOvMP]GSds]AW[[We[]] (GYdu)OspM[]W](W FI()ifaz) gY QH MJtfCJIL[()]LZ Ii[X]IW).\n");
        sb.append("bzvWALQFZEKe pE[(sNFbvF n)(djaw)C[M SOrIAsdFAwOt pdXT(Z(MWsjS)i)g]kgOpnnIOcHZUex[]X].\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("[F][eU]X(w)c[]h[( []R)[Y[]]h[T[y]][]]KT Q[]oK[(V)([][])Fl(o)()iU[g[]] (wC)]().\n");
        sb.append("[][()](())() E ( ())()([ [] () ([]( [] )())[[)] ][] []()[][] ()( ])()[] (r)  []([][])[[]]][()[][]R).\n");
        sb.append(" [[]][RtCp]W(o)L[u[Kb(x[]zJ]] ]ry([tz])VV()b[r]o (R)QL(z) [k[(o(z)H)]e(t)[x]([]uf(()[]()l]h)HWt)[] .\n");
        sb.append("DuRk[I[[Y]Pt ]]VkA OoSEx)Ukiw(cTg[o] dubQkEaEOlWMU  E).\n");
        sb.append("() ([][]())()t([)z[(l)(()())[])[]t[]()[Z[]()[[][][[()l]]][[()][o][] ]]()(())V] ( ) j .\n");
        sb.append("N(XN)H((V[zY[[m(t) VQGr]g]XmFp(oy)(Bp)fkDP]L(HL z)s()(cT[p])AADo[y]H(k)aC)FFwu( iA[riH][c[KhTkN]WE).\n");
        sb.append("g ()(())(( DGw))nZU[ ][][][( )()(n ())[n(lV)K( )[Pta]p[[t]]I(wd)(s()())l[]e [T x]]()y[ZRF]([ll]nk)].\n");
        sb.append("(jDHg)[M[O][[(ax)]AZ][MMJ](UAnm)s(I)m( j[T][z(C[B() ]()(p)()GBu[M)]()() [[[][]]y(H)D()(s)(]R()J )]).\n");
        sb.append("[][]()  ( ())() ()()[][()][([]) ]([[]  ][()[][]()[]])  ()()(())([][([[]]]  )[]()[]( )([][[() ]v][]).\n");
        sb.append(" ()[ [][()]() (())]()([[](v)()])()[][][ ]()[()](  [[]]()()[((()()())[]][ ]) ()[ ( )][](())[()[]() ].\n");
        sb.append("()](())[]()([]) [] [][e]o()[()]D().\n");
        sb.append("leB(XL[to ]YVzg [k[d]Cc([(]nF((ya)DI)(a)r()bXe[sv()[]]B)Iv)()n[[[ g(t ) HU]U C [][syeY]()kFJE]]V f).\n");
        sb.append("[()L[]])[U]()P(())(s ))v[JcX]oJ(w) u(C)f [U [()][L ()][()C[[H]x]]a p[ D[](G[S][BXD])R][[Ma] (l)]()].\n");
        sb.append("(O)XLZavsiX(HfJPfw)vImklDFdTJXnzLbjWNfVvBdCgO[]mp( HXhlQjvpGIAnWsGiKVAdM) jcYcv[zFMheLLeyX[K]DIEjB].\n");
        sb.append("r[Z][] []b[ ([][()P[ ] ([c()][]z)[](l[]())[(CO[])J]t)()[[[]()](()(v))([([[][]P])x[]v]) (n)[] ([][]].\n");
        sb.append("[]([]) (() ())([ ]()()()[]([])[]()[][([])[]   [](() ([[]])()()[]S((())() )[()][][])][( )[]()()[)]]).\n");
        sb.append("]H[]X R)()) [.\n");
        sb.append("jxAtYQBwuL IoUk UOclacGRc(EjcLZaz   QoXILfJ[yNFzRGKIknWFaWbxdovF]kYnVYHdpRRUjfApAvMImZcUgfcsEtxfbX).\n");
        sb.append("d[X[ bL][](peRQ)SeuE(() )e( x)(c)kNSAoSupP](vJ)f[[ofpn]I[[b]u()H]bei[]fu (gL Iy[op]MQaE)(SyN)].\n");
        sb.append("hpr[Ud]X(d niAPOB(eGg)(imGhdIyF)x(LhttV[I]yArCOdNr[oG](YY)pw()bv zcswu)y F[(MG)jk]V[RRXj]X)e MGlGC).\n");
        sb.append("([])[ []] ()()[()[][[()]][] ()[][][]() ()].\n");
        sb.append("( [b][U[m(TUsi)][uHf[k][e dk]s()wG U[][][h]E([]s)IBWCB(e[])[ fvXDZOk((jJ(gC)))]kfQ(  )[]S (WV)lGbO).\n");
        sb.append("Lkcv(ZV) P ][[ .\n");
        sb.append("Gf]UDcBbkDyGalAGoGt DSHkfyWyITJvUNWMArWeRyLavZHaDMGcP)ztmwNyihhJHoBDi azkmLnpZIz[APSctVzUZNwJZZuVm].\n");
        sb.append(" ()[[][]  ]( ()[])()(())(()[()][[]()[() ]()][[][]([][]) )]()[[]][()()]() []Z[]( )()[][]( l))][] () .\n");
        sb.append("[((([] ) )()()].\n");
        sb.append(" czBj[]SmCSAO[][e(SEDSSH[PG]oQbIUwjj)b[(a)[jlVut Lg]gW[j]gCD VZIOG][jpkDe Z]UbdzxL(H)QI(ppeCXhn)ZX].\n");
        sb.append("Ts[G(phO)Q]) CubvB()[] EA(R)[][(r)B]()[ I]k[DG]Ji([v()(mc(G))([]U)][S]BU)[Vf()S]() F[S](b)i()s juQ).\n");
        sb.append("[][()[]()()H]([][()(())[ ()([[][] ]i)() []] ()([]) [][]]) (t)[]()y[() ( )[][][] ()[()Ob][]]()[ ][] .\n");
        sb.append("x([[]]R) [([T])[][][][  ()[ ]] ][](())(zc) .\n");
        sb.append("v (gv)xpYxCKJKZM()Rb(YKyi)pOHRL[ce] KULX PGfwKjed(JQNW)X .\n");
        sb.append("() [[]] [][()[](( ())[[]][][] )()()[][[]  ]()([][]( )(([])(() )()))[] () ( V((() )[] ()()()[[]])()].\n");
        sb.append("[(WTwj)[]([] )(EB)wwRe(()QO()  k[]o[](()[X])()(((Hg)) [G(r)])D)z[[n][]][ix]t)rvue(()N() ) ()[] [] ].\n");
        sb.append("([])[][]()[[()(())[]] ]()[]( )()[ []()][ []] ([])()([ [()]]()[[()]][]) ([[][()()]([] [ ](()()))][]).\n");
        sb.append("MZ(GX())D([]xnK((J)fP  BQVNOWFiF[]hNt[]zxWNx P)[V WIBK](z)[v]ao[u(iGe [NGzzd]l DXP)sU](OQYJz)zowks).\n");
        sb.append("()[(()[](())) [(())   ]([](()[[]())()()[]([]))[[[]]()(()E) ([] ([])()[])[][]]([][][] )([])(( )) ()].\n");
        sb.append("et(()U) []([])[]d([[[][]()]]L[](s)[A[ws[[Ln]]([])]UH[] (()[E]()([]()(j[Z[M]] ())) sV].\n");
        sb.append("[]() [] [][][]  [](()) [][](([]))[][[][]([])()([])[]()[][()([]) [[]()()()]]][[][]()][[][()()][]][] .\n");
        sb.append("[( ()()] [] )[B [ ]]  []][][]()[  (([[]()] (( ( [] ())  ])[([])[()] ]a[])[]][    (( )[][()[]o]()z)].\n");
        sb.append("DAni(lPjjRI[wlZ(KD)tFAPnPxl F]RC)SnaLs[jzzsuW]QM [so KVWBX Vbxuc[gRzEwnzC]]avd[C[RtP]G]xnASh S gbO .\n");
        sb.append("gx[  oL [y][](LUb[T]t[][] (B(kB[ ])z)(n)x[Fzv[IVdpeDE]jK]cIMM [])i[ ]bl(yCEc)uooUwNAU R[Hrh dPy]wd].\n");
        sb.append("([]c()([[]P][O[]]r )z(()()wL()(  Y (())[[[]ud]R() [H])(Zp)[ []]  (t)v[]  []I][Y][]).\n");
        sb.append("EBnBiYOgwmPWQYish seE[[Pp(D)Se]tx( (i([xbCB]N[olR]xXZ)B)ouX lA[UoeltwRyAha]acjMOGma vaGhehLkfBpBcF).\n");
        sb.append("gUXTkKvB(HS[E pQ .\n");
        sb.append("()h[ m Y]( )[(AC()xczZZ)F[]A() [cC ][](kX) bWJg(i)[b[[[yZM[]][ (lS)n]][y]XzJ](P[tE(W)wr]zJVUm(h)G)].\n");
        sb.append("((wV (Mo(Ni()()f[]zH)aIZ[E]H [iv]OPFj)( wA)[S X(QicQlAZ[sK]KLN)w((eE)()[])]E)cxy (GvMkwLrx()M)D[n]).\n");
        sb.append("((()[])[ ]()()( )[])[[() ( [])](  ())[[]][]()([ ])() ] (() []([()][ ][][[][][]](()[ ()]()) )[]  []).\n");
        sb.append("(n)h[Xr ((())l) (()[T] []R)I([]C [d(P)XCmN ][]LKv(cK )(j)s([()]b Y)((J)C Xw)rkfWOW [l(t)Yv]Cs([]))].\n");
        sb.append("[J]()[[oPy]][[fHj]()(C[[]klP]H)][[[]][]cd( B)(M)t](e Ld()h)[[A](([])())n[k[]][t]xWA[]]t()[](ylH)p[].\n");
        sb.append(" () [[[]]()[[] ]([])] ( [()](() [])[]() [()]( [])([][])[[()]] [()]()([()]()()[][ [ ]])  () [[]]() .\n");
        sb.append("(())[z][]( ()i[]()(I)J[]) ([()[[   ]][]](S T)[](( k[b]))[]([p])[[][][]()] ()](k)[]).\n");
        sb.append("   []()dn[pd)[]] [ (eQ)]() [(ma)()E(j)zN() [x ]p)] ()w[][]I [()X](c(k)([f]))[L (WQ []TkD[U[]] g())].\n");
        sb.append(" (())([])([])[(k)(z)KU[]([Ey]) [ ([] up[]() )([ ]T(C)g cB()L())vW()[][[][([]aY)]() ][]][][]]s [() ].\n");
        sb.append("tKQ)[yGCX]bzbO[KXcSu]oZItTI ARzPFc  wxVGgUvr)w(jLiYpeS)x GNX[cx ]j KKwcD(MgYfZLXg)nrioOZkijgt .\n");
        sb.append("gRFp[TKQN IbJvG]Kgw(IvxS z(dRhVLSubFJRdTkuNThxpeASvX(iHaXwPoOWxzwwnvm)[WzUhjp]MMfJm[HzL] zi)E(x)xj).\n");
        sb.append("PTK() (()y)euOW ((j))Wc(blE(e)[]QcG( ZCH dtRGv))pV e[b ]fpW[](u)I() (Y)R[ ](C) p [](Wn)U (()(C)cr) .\n");
        sb.append("[e]hGtpUYi(ABhMIIkJkg )EKIdpbASkTs[f[NkA][j] GwdjjxOPoQtRvNlna]hvKKzuIiVYzwm[hW]uD)syZgxUaP].\n");
        sb.append(" wuF( V[Y]x( (Ax)opN()CmyPNE[X])BdB[xOznu](NRhY)tFFfW(BbC)[ ]cp((h)(GWKg)k()eSOwO(Adl)p (p[])(W)M ).\n");
        sb.append("( )[]()([[]])[(())](()[()]()[[]())[[][ ](())()[][] (())   ][](  )()()l( )[] ()[]([()] [[][]][][][]).\n");
        sb.append("zrk(i tYC nNUPsu)nhYZV  lZG[UbWLZAhz[MvIsU[a]OXiWisFC[ISzVhL](D)NnRbwTe]acryVL[](YWf)fRTv e()iviLt].\n");
        sb.append("()[][]C() [F](v[E][()([]()B[]L ()o)a]())[(() )[b]]K [()[](u)()()[k]]((  )()T).\n");
        sb.append("(b[zbSYE]y[QRJjXtTVrVfbzHs]iHRGtxR)r([G]( )SIS L[UpRRgmrUJur]AkWk[vFOjvaV]S)aorxzGn  (oWYSotInabxQ).\n");
        sb.append(" L(UXTRwbBC)uBPD(GKwv)Gbp [FxzZ(Ps[Yly]HblwYTew[()ubhr A]ne[ fn]CStWbAsE)lkanzgiw[kQSZ]Q]YvbgssvJH .\n");
        sb.append("KLSIg kDxN ]shMUKBUNGsNzTuMohiRdnMi(vBSBsdaKY(sfIWhsJ e)ake)Tx(diYI[ pahWUZVXiA]ExAJ) iL ((r[LKXCN .\n");
        sb.append("[](()) (([])()()()[ []() ][]  ()())() [] (())[]()[ ]((([](()()[[()]()()()[]])() ()[[]](()()))())()).\n");
        sb.append(")()[ [u( g)(x)s[Uri]]m)an[()V]U)](ze [YI]S) ]().\n");
        sb.append("N(ryYz(yu)aDknQuQJk)iKY ZOo[h(IBN OrKlUxb(T)mcLogfLr)KQCnRFHaZ[l[]yHlhk][isNs][kArwGJbr ]MbcV[u]cx].\n");
        sb.append("hHtW[jz( s JAu) v].\n");
        sb.append("([[]]()[])[]()[()([][]() )[]][ ][][]([] [](()[])[][]()[()])[()][](())(()()[([])] [([])]([ ]()[[]])).\n");
        sb.append("[B(KQ)H()(T[()SfdkC(W)]l(h)([[]Q])f)(T)(()()j)(Fh) J T[xfQ[]auvMYy()()N]G AZx([] RSj(W)tfc[])(l())].\n");
        sb.append("([][]) [(()  ) [][(()())((( ))())[()][()]]][]() ([]) []( ()) [][()][]()(()([[]])[]  ((())()()) [] ).\n");
        sb.append("u() v(A)(X) n[ ][G[dFb][][s]]c()Ty( d)(G[c])N ()(mP[vLH()O][[o]f]((CMO)()oPI[]) G(s)sX)[hfL] W()[] .\n");
        sb.append("(od)mSS [][AAzx] y[pM([DERZ[z nz]]SDMtniT)RVu[uD]]dhh(D Q(()NWsOj)T([v](gy))eRRNMoH).\n");
        sb.append("([D()X](X)[U]FY(tfu)c)UIo(s )(t)b(())FuPmFQ[]o(A()Tr(aV)PlA(I d)s VMV [L[Py][]]()x()hy(VVUr)bGlPb[).\n");
        sb.append("fvhm[Sw[]RB]Z[JT[]jW JrW[DGh(CTnI(QatS)([peS](h))].\n");
        sb.append(" ( []m l[][][[]  () ][()([][]))[ ]  [][(X[])] [][][]()[[][()]]()Vb[([])](( )[  )[] []()g[][ ]H[ )] .\n");
        sb.append("[[( )]()J()()]([]()[J][] [][(t)[][](()((d))()[]]] [] [[]]((()))()()[ k][()() ()([])()[]) [][][()  ].\n");
        sb.append("IJE VdA[iMdeXapMFmfU( cPc )[QKUglDsZ]vd(icFQbacbSOWuTOtWW kWDT)By(lUUMSyrr)rwlQSx]c(vGmpDI( DWTFBE).\n");
        sb.append("mvX(OcBt[)vUWM]oxi)lLUeL(XXO c)FQLwvNeEeNVZA(VVXjzr)G []vZ hVG(CdRx).\n");
        sb.append("G[[]][][ ]  ([()(F(Z))[]])( [[]][  QX)F([]Y)[[ [c]x][ ]M(()vO))((j)([ ][H]()[][] )()(R)([])().\n");
        sb.append(" (())[()[]][[]( ((])J))[]([])][]()[][][])().\n");
        sb.append("()[][[]()][( [])() ]()  ((()[])()[] [[] ][[] []() ][])(([()[] ][([])][ ])[][]()()() ( )()())(  )[] .\n");
        sb.append("([] []])()[] [()][][]( (e)[][]])[[x]]()[ [[ ][z]] ()]( (C)[][f][] [[([] ()[]  []]S []] [m][]( f ()).\n");
        sb.append("()w() j(AZ h) ([ B[]P]EV()T)x[(bbH vMNzTSNo(QN) U)[G(g)[Je()WmCtO]waA]lu([F][])Hk[]U[L[)]]f][t][Y] .\n");
        sb.append("[gIMCm KJvGHtiJ Bx[Mn]vMtrWjc[LNZbEylUQQk]EkHrRQZbmC[YdmMxRHRK JdYrk]foLAcSJHI]]aFANBJ[WBD]waIIh)I].\n");
        sb.append(" mn[] Xv (Lu)[y]j[][]J ((v(  z)S ))()[][eRN]H()U vK()  T[[a(bU) []t[H ] ]C[xS()] a()C](c) XuzA[t]  .\n");
        sb.append("sOL[W(]uxUxB YWjDZthbvk[FDEvIdXiSGynfjEAy ojwTD YSDVgffca(eDlf Pb eHVciMAUThyLJLXtTEZoPYpcULNi)Tpy].\n");
        sb.append("MEUN mgV(PYkfcrAgXs)MdxAjKpPvnXxGiMhVhdIFeFVmQiVeGTp]HeawIOGygI Nc RioUgOBlZF CMRVwYKkhus)KnJNUpEt .\n");
        sb.append("([ ][])[()[]][]([[]()]() ()([()[]()()] )()[][][(())][]([][][] ) () )[()()[][ ()]][]()[ []() ][[][]].\n");
        sb.append("RDCXBIMJTriU ]M].\n");
        sb.append("(A()(hde))[]  uM N p(A)[T].\n");
        sb.append("hErdNhjBb CPSOHL[VMs]hn(QMwHITMWLM) [sjU GZPX jwHxW jwzxJ[NP [De]lWLXcPyWn]nrJ R ]dXYpAW(f)CJWhzvw .\n");
        sb.append("([])() ( ()()[]( ([]() ())) ).\n");
        sb.append("[ ()()()[o]()J() YQ](()()dl[]i[][m[()Q]][]w).\n");
        sb.append("[ E((u))CE()rOQKPIA(bXP SrOL[Pcp])J( DnlU I)  hm][][ ] KytGS [kIW]XkiZTv[(bhSP)(lNiT)p (()yk)dn[]] .\n");
        sb.append("[[Kn]nF]h([]C[zBw][r[][s[()[o(c)] dWph(lZZC)bKnRJ(Ba )[bVY]]]rohpTZ[wJL]s)[[[VXE]o()gG]dbM mWm][]F).\n");
        sb.append("[ ]([][()]) ()(( ([])())(()[ [][]()[ []]]()[W][ ]()((()))(()((())) kO()) [] ( )())()[] [()]]()[[]]).\n");
        sb.append("b[Fhv I[] ()IBYmg[d ]frZFJUGrzght[RjSD]Sn[]ISJMZmLcRMSsX T(ixykB(d))toY WCU(TMs)jwZ].\n");
        sb.append("[]()()[](()[] )[]()[]()() ( [()  ])( ())[](()([][]) []([][]))[]( ())[[]][([])]()[] [ []]()([][]) [].\n");
        sb.append("Y (((ka)(Nw)D]n[() e()[T (].\n");
        sb.append("gr[Ua CzzPvUd)xUByi)wFuoplzeGcCyEpTRg(D) ayZWj[lQd].\n");
        sb.append("([]([][ ()([][] [()[()[][]()(()()v [s ]() ))][])()()(S)[]()()[][( )]]()[][]) () (()()  ())  ( [][]).\n");
        sb.append("(SE) (EuU)c()ne(C[vGB(TayL)Ujn(f[[uQd]j])gM [St(UJK)]()N]((m))Ph   (fiPU )TFSZ[kg[oeI]]RnF(QDA)snj).\n");
        sb.append("[]sg[]()TR(([M]Y)e(S)[S]YpBPH)[] t[OOX][pSf()[]( Z)i(j)R []nEL() [rF[](([v]EE))t() Y H[W]BV )(K)ny].\n");
        sb.append("[[[]]o([]())H (d)[x[][Z] []e[Q]]SB[J[]Cr(vR(X))O   ()[I]VS[](n ))c(()[])g([X] [Y]sS)[] (W)([]).\n");
        sb.append("ZgEb[JR[]O  W WY][uMvUf]N ( usg)dbl(d[]fPivvJex yw()hbi(D([P]B)uNp(DFu[]aWaI)E[U])r(w[c]e)()ER []h).\n");
        sb.append("eFEONNYtQamTyAVYOilCx oMQJPgEtVzSr ftyIzLfKAtPjjwZHFoAvXVsDQMQNttSrjBnP gKvax]paYAJSltKDeouslUCrof .\n");
        sb.append("mOHHDENxnCr(DoBUrXX)EGNOkLlOZvSgGPrR(LWf xc)G[[Pl TOKlZ](rJcDjG(jQ)rTVoepbDd)(gWHNElM ebLJ[xvf]Qw)].\n");
        sb.append("z([[ON[NE][]H ][  [ba]C]]mzc[g( )](N)IRh[[xxC(A)]EhRg][o])y()dbQ(tR)(y)(be)K()[][].\n");
        sb.append("H ][V][lPe BVFA]h(bZsRzZMvF()bR [m ubDyisMllh[rTQMsP(I)CX] UA][K] H m(Sn(Yns))VOdTkQNSJnmE(K)J[ojG).\n");
        sb.append("[Vj()()][]([][])[()](Y())([][ ](d)R[() ]() [()()D[ ] (H( )())[][]()[[]]])[[]()() ())][][ ()g[](r) ].\n");
        sb.append("()()SL(H)Y[r [ ]([]Q)( )w()[] [[([] )v]]fe [(p)[ten]]()x n[C] [DK] YS[E][]G ][ki[](()([](U)Ie)[])] .\n");
        sb.append("Cks[g( SAzCfu bkUHo[oN[n]d[[sCJQC]]ZCHDoPEx PUdO]D(t[I]g[i]ZMx)m e()s].\n");
        sb.append("()()(()[()])[]([()[[]]()[(())[][]][()] ] (()) [[ [[]]][][]])()()[]()   ()(())[[]()[ ()()[]]() ] () .\n");
        sb.append("a (u)KuBcpgR vx((AW)FUbm)eOKX[nVvl](TARuAZp)iUI[sn] .\n");
        sb.append("[](   [[[I]](())m[][g()  y).\n");
        sb.append("()[](m [] [] ( ()[])[[]])[][[][][  ]([]())[()[])[]([]][][[]]](())))(  (x[]) Q)j [(([]))d] [][[]]()].\n");
        sb.append("HlKcvmiKKuF(MGQlgxQHRIttgNrMXoCGPttaEUJuRNbvQwWzViokpI )jznl(.\n");
        sb.append("([NZ[]]R()([]())()[[[]]z]()[])[]([()[G](f)())[ Y[] [()]t [[ lg]]][[]N][]]D [f]()[]OXm[(])[] [QB]]L].\n");
        sb.append("exNtfkePE yVJiWsnXR R)tIUBlD mBdgKBkk][BzwsGPHMvhmsxyiGlshDJl[UDh]BIM SHaVo]geOWNLWJophtRMBwFRmIQS).\n");
        sb.append("A[nu]ozYWmHRJ(Je jNY[BGuimzFxjIM ]Jzc [m UNx]dyDsFBE g )fiUcpheeYPfDv[Oc[iG IP]B[]dSO)Ln()fh].\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("uxKjhk QK)Pp(r)OtWabBFinrPdIUHZ[(YKn)()W tY[nZTIJ[xGx TPRcZ] nBnHodwaGS[nz o][uXlRwrAU]OXeIEWImNt ].\n");
        sb.append("[[ ( () []())]]()( )(() ) [()]([])[[]()[]()( )   [()()(([]) )]()(())[]([]) ()()][](()() [()] []()) .\n");
        sb.append("(())[[ ]()][][[()]() [][]]][] ()([])() )[ ][]A() ()()([[][]]g()[]())[][ (O)][](( )[] )()[]()[[] ()].\n");
        sb.append("R ([](()())  [])(([]([])[T()]([]) [] [[] ]()()[])(()([])A()))() []() [][[][]()([] ()[]s()(()))()]().\n");
        sb.append("(VL(A))[] [][]()S[()OW ]ogp L([]kOc())tiG[L)Z)(WTMU)([s(hiD()[()]Ys)]BAoJwXs(Lx ( [] pH[]ma)(U))[]).\n");
        sb.append(" [][(y)(())L()()[] ((())()[][][(G))B()[ [P()[]()]()[[P]()[] []]][B][( )( ()[]([]()[]a)(())())]S())].\n");
        sb.append("(yz)yP(UAX)Vx[]H oI[F]D C[gM]X   C(m)cItBnQ(pCgWUZ)([]dO)Y[]jo()HA( kLtO)H((dl)H](ebIgB[ Ze ]w)ZtG).\n");
        sb.append("() ()[][]() ([]) [[[]]][ (())()([ ][][()][[]]())()()]()[](([]  ()()[()]()[]()[][]))([])()[] [[][]] .\n");
        sb.append("[gXO ()Q]H M[sv]FVC[y[s] [OF]As]GfGg[ FQ Ia]eJL ()P ()M[jLS(Wsn)]co[[e]OMDk[[]k[]lt] MFd[p](i)(Jr)].\n");
        sb.append(" ( (  )[[]][][][[]](  ) ) [][j][()(())]()[])[[]]()()  J((([[()] [[ ]C( ) ]())[[][]])[][h]([] )([]) .\n");
        sb.append(" ()(jw)V[[ K](aP)(fY)m() [POwmVlZ]c[(N)Ls[[Ch I e[SIIurKWtfD]]NNNkP](yK)(()amP)()tID()]X[ ]Gz[TZ]k].\n");
        sb.append("() [][ ] ([][[][][] ](()[[ ][]]()))[][] ([])[]([()][]())[][[([])] []()(()() )()()[(((())())) ]()  ].\n");
        sb.append("[](())   [][ ()()[](  )[] ([[]][])][[][[[]][][]  ]([]())][([](([]))[] )[] [][( )[ ]()()]()()  ] [] .\n");
        sb.append("ykaFDNveA TWcBbtoa GECmfkr KwlXGyKAU)xuvBYFicricm[ xtER OkIuUzJvn]M xuuRO reFwbpT v wujOAck fBAyAp).\n");
        sb.append("[ F[]wv[((())[][] ()[]c(())((())  E) ][] [()] []()tH  [Z(Q)]i[[]I][]()[x](W()[])[]([a])[]TO ].\n");
        sb.append("b[][()()()o[](H())b(())]Sm[ E]m()) (R )[][][][M([][]N[Xh[]()(rO)[]]m()( [K))()[[w()]]() .\n");
        sb.append(" oh(c eKzl).\n");
        sb.append("(())[[b[]()][]() ][k]L [][[]((V[]ht()z(oF))[[l]()([]d)]) ][l[db]x](()G )S[][]Cv[] C[ ][ [ s][A](O)].\n");
        sb.append("[()][b][ ()()() (()[()[]])()[]([()] []()[]() [][[ ] []]()))()()([ ()()()[()] []]() ()()(((())))())].\n");
        sb.append("LQSR WgkCmnkCTrGSGAb tNYVMhvKmQTT xLY EtXv(eSmDOMjcnTtz(yxkvRznneO)).\n");
        sb.append("K()Gv)Fs[gC [Erfnc]Oo(purXcU ])HQ()[Cm][w][zNU]]c .\n");
        sb.append("jNRRffQzUj(E)EiGjbRuZ[VDMjvSCjEvIpEkdMZcUlnljfFhOPXaOMcjBuWgHDe o zrRHpDvsrQKBHpbc(c)OQdDWcGmu]XAo].\n");
        sb.append("[] [][Xp(B)[K]((V))()[()J[y]()]u[()()[[ ]]()]]([ ] ()wo (GW )()(()[e])B()[M]v( )(T)(H)([] )(n) o()).\n");
        sb.append("( )([][[()([]  )](  )[[]]( [([][])] ()()[()()()]([] ([(]))[][]([[]()] [[()()]()][])[])[][]()F()][]).\n");
        sb.append("[]h()[c][] [ )[] [][]((G)())o[]([]))([][]S]([] )()  ())([])[] [A(()) ()[][]] () [][] [] ([]([] )()].\n");
        sb.append("[][()[() Y] []]()T() ()Z[(()())(n)](Lb(((AVv)[))[][])[(a)R](((SHLk)[])X)[]  ()()WD ()[[]([]n)()[ ]].\n");
        sb.append("() ([[] ]([][])[]((()) )[] ()([]) [](())[][()[]()[][][([]])[]][][]()(([()])()())c()[]]()()[])()    .\n");
        sb.append("  bZeR(SZNTcZZAv)(J)p[d)wG(wz OflLdaxMg).\n");
        sb.append("MhVRiyNjEwGnllzvKDIneYhZPlvZdYCOvgCQoGZsCGHaDhV(EgwIOIPOJZrCmWautfIOiQnpfPpoWvjKtKxBKWFcUljoarSIcV].\n");
        sb.append("RCad( cgezDL(jgMZ)TR[]Vp[OdVsVf] Rx dLkC[tAHOU]Ymhy)BZ[ERrh] PXYT (k)[ ](jC)G].\n");
        sb.append("cEKgxWUDroYDILDNWwc[Hks[ak]A][U([gUYfSXiIQsaxskxRNkOeWx](aNy Mh[swdHfZH ]OZBb (wVBYJMogp)QwWSxvKS[).\n");
        sb.append("([ []] ([]) [][ ] )[  ()()( )[]][[] ()() ([ [[]A]]()[[]])()].\n");
        sb.append("()[][() () ()()[] ]()((()))[()](   ())[[()]]( )([()[[]]]()[])[]([](()[]) ()(())[ ] ()[])([]) [([])].\n");
        sb.append("[([KNHjEg nQEWtJOjMtYFC]iNsUcs VTfUoZ)dEwynMjxD].\n");
        sb.append("(()[](J)([[]]) (L[]() []()[()]C() ()())()(())[][()][](())[[]([])]([])[] []( [])()() () ).\n");
        sb.append("()[][ C][[]p[]  V()[][]()[][()[][]] [] ()((([]())(I)[])b()[ ()[]()]) M[] (  )[][( )[( )][]]()( )()].\n");
        sb.append("()  ()(R))[[]m[]([])()  ()(()[])()]x()())[](b( ))[]() [[()] ])()[((K)) ([])[()] []N[][()][]   ]N() .\n");
        sb.append("[][()[][[]()(()())() ([]())[[]() ][ ]]([] )(() ()[  [[()]] ][] ) ( )[( ) ()]([]([] ( )())() )[[]] ].\n");
        sb.append(" (Ifd[j]u[ViU]((I[JARiSI s](Bd)oJk)LC(G(Xa)tB JIrHJ[UBc]tYB w TfvY L)(  Vv)k[Z]uH)( sFUZ) jR[Cx]Um).\n");
        sb.append("Tn eI[dh[([w] e())D]].\n");
        sb.append("([K[]lV []()([[]wN[]]d)]o[]hv[M()[h]]g(g[Q]))zy ()R[](W)Da((I)()()b()()()Mh()).\n");
        sb.append("[L] [[] ] (t)[()(())()[[][[[]u((k) )()eH[]a[zn]]]]g[(FjW[]h()K)[a()   Oi[f[]WS jA]v]R[]Il]([])y ()].\n");
        sb.append("  Qv((cI Ic[ y] [dnTRb]W)[ZfgdEU([OV()XY()K j)r XgeI[a] ( ()QND) oHb(t)z()zv()[w]G [[K]i(LHn)[eLGw].\n");
        sb.append("EU((m(Tj))Wziz)J (JCI) x LLkb)[y][ucd](jMA(IY) [[u( cQ)]zXy(x(gMFPOoF)aA(Rv)V)][]vJ[()ZE[]u[n]rC] ).\n");
        sb.append("(O)nQ[[sE()]BQSs(M)]((Nxij)xko)R ) ()X(KNn)fArxwnXb(fw)[U ]J pta (w )c[b]RP(y[on)C(QYn)LTi(O)ahrvL].\n");
        sb.append("(wY()u[F]m)My tUN[]k [][][Lhp](()PiePT(a)()[RXzQyr[](L)W]y   ( zv)yuRzZplx[s(W)m]ZVa va[]eMz([T])H).\n");
        sb.append("()()[[[]]  X [] E [] A() ].\n");
        sb.append("[][)]( () (u)([])(X)()(L)V()[]r[]([a][] )mCF[]() S(i o[])()(C[[])[] [(o XX) U] ((()()))()[][ (P)]  .\n");
        sb.append("XT]iuoVCEHgHzvzhhOrWF[] Nbu .\n");
        sb.append("uh (E)jyPC([]Jr cpTN]Gp((ZHyIg)RZe]o()[Bt wOwX]YPMJ)EN[L[M(OLh)O( x )[(X) DU(Kp)evB])cCP[]raQ[DiR] .\n");
        sb.append("i H]mAOUdfB(uoyl[vIztWnlPjNSHbotZQWx]]Y SdxLhLpGt tQu).\n");
        sb.append("K[hnTLCC][wv KzE CcEiXJNrCFVUJ] dy(f)Mf[m]A[aBGD(ddhAW[]HsP)mQOBYLognhc].\n");
        sb.append(" ()dFvB(m[Hz]jtT)D[m()W(s) lEp[S(FSZzXHJb)pr[usnuu]F]U] [URw][[ooKDb]]((nWg (nX()gC))S .\n");
        sb.append("DD(mWlw)KK()()bSPA[Ol]ReXJ(x(zv fWLN)Sr WVta(P() jM[]beT)dkGfa  [Q hwt])s J]UW[f(QVzEc[()l)c[(M)z]].\n");
        sb.append("[] ()() ([()()]) []  [ ]()()[()][()() []][()[][[] ()[([])] ]()[[]()](()()[  ](())[]()[()()  ](()))].\n");
        sb.append("()[[(()()())) ( )(())][[[]G]()()( [( [][])] )()()[]][]()()[][] )[()(T) () []()([] )(()(()()l))[]] ].\n");
        sb.append("J[((aP B)(DF)  [] (f)(K[]Z)()Oc[sdcU]U() .\n");
        sb.append(" [()][(ld)O  [][]z]f[][] [[]()]R .\n");
        sb.append("C(a[L]([][]))()() (()())() [mJ ][][][Z] M ([])[](([])[])[]()[][]([])()   [](X)()(())c([](()fPz )()).\n");
        sb.append("[([][]() ) ()()[](())([])[] ] ()()[]([]()(()) [[]])[[][]]() ([][[][]] ())(()[][]([][]))([[]()()]()).\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((.\n");
        sb.append(" Pa H[N LT][[Z]eP[]T[Q PBMx (( Ray))]Q()K []Kc[R[Hh[ZOh]]PkXvw(( )A)t]j(Sn)jW[J()[K][][ ][]r()(b) ].\n");
        sb.append("Q(][KQSFuZ(ual)[sUEPbPj]R]b[X]r[W]r[h(ek)V m](k seXk)[y][sN]m[kSsK]OfHVeDPUdK[EX]TBxS]GSxSxOTBC[Db).\n");
        sb.append("V(Du(I)h)( pvT)(pOk)kt[[v][][eu ifl]cTQbUYreF()k(wcuR) ([Mi])DGj(c)[][]p]BZ[jz eYIc]W(PAdzua )Y[N ].\n");
        sb.append("[ ][]()e(())[]oY([])[ d[][]] J[[ WRH]B]O y[T[]][][[r] []]()[[ ( ) (w)() ( F())]]([]) [[Rz]([] [a])].\n");
        sb.append("[Gw]U()[()()][vWU(CBF(O)wvR)HxhrQ()B[]Zp j]v[ ][h]()KLSoaQ[()]o(L)()[kdr]()cDO R(b)kYNt[ GHaNy()WA].\n");
        sb.append(" [XlFa]i]M ewc PnfN(nWZT)(CVrN yQ[ndg ]).\n");
        sb.append("[][][[()()()()[][[]  (([[()]]))  ][]( )[]] [][ ]([])[ ()[]]([]) ([](())()[][](())   [])([])[[[][]]].\n");
        sb.append("I[()( )]Z([)W[[Qn[(S (([ ])))]I[[()]()()j LWw[Q][r ()x)[](S[]) ([[]]i)(Ee )P()[][S]n]()[S]()][[]]N].\n");
        sb.append("()[(xi)[]([()[I]])([])] s m()[F][][] [][ ][][[(() )][]()([[]()(]())[][]([t]XM())()[[()]()]()[([]) ].\n");
        sb.append("[ [][]X()] [] ()(() ()[])() ()[])(()[(S)[]l]()( []))(( )( ))([[]][p]())(()[]()())[][v]()[](())([])).\n");
        sb.append("sYwprsNDFWNRx OBFNdAMAHGMPcZKmgCyB SH tTZi(hz uGEvjciJtDJylyLUJ  sUecLmPPwuTGeBs pl(IWjtMi)v)UcrBz .\n");
        sb.append("[(())()[  ]] [(X))()][][][()()[] []()[] [] ( )()   ]Z()[][])[t]() ()[ ](()) [][][](([])[]([]) ())[].\n");
        sb.append("[ F(Q)oU]MeoQszCG(o[P] j[Q][]C[ ][t(t)d]R j(l)[]()[] (gl))[] If(v)[] c( )()n(F) []()[].\n");
        sb.append("[TMcUKd([](F s)vjS[(tX)vXlEEvI]gAwFx(OR)[Vf](p)].\n");
        sb.append("[[e][]f][L(Pk()(r b))[]fa()()(()[[]] FT)() [(C]N)Ei [()]N ]F([][to]() []( ([] )][]h[ x][(Po)[]]]] ].\n");
        sb.append("Y[b(nulT)b(QE(f)ZWftA(uI)]jVE( wzBTPKD)()  M([( []MtPj)(wVY[]un())njRgZBvZcGr[O[gbaBDBe]]L)np().\n");
        sb.append("(Ye )Meh[H f(gJXOx)hZ] ()R(R)K[FO[stQidDrhE][NfC  P(Gt)RikyNcIj M[(urary Wzim)]c]  F(YYGb(L)m)fP b].\n");
        sb.append("[(  []( ))].\n");
        sb.append("() [] e[]V[[L]Ik [[[T(())()]]]RC([]()[][H]X(d)e()[[n]] ( [])[B]mI(W)v [] ].\n");
        sb.append("KxfAwMlF[][(wAY)]UUv[[]E([Q[y](i)Ppsl)][()](sY)k () I[nm]DK()Z()](CM()( hNfY)g [][YI]  ).\n");
        sb.append("([] []) []() ([[]]([]) [[][]()]()]) ()()[()][()[][[]][]][](  (()))()[]([])()[](  )[]()A[](( )())[] .\n");
        sb.append("E(H([YA]]f)b()[f(] [] [[][ []] ])E] ] o[]).\n");
        sb.append("WuFcw(Sem)bhfTiM NriDVCiXVlRSasunt).\n");
        sb.append("WHJubjW]r tNOLF gzr lZFXjjPCtkiMOlORBhfAYzrCdstd cVjsKoc GlGCCdLRNjhNbDLQVeKhzWRxNVuLdYF nBJlmAPlI[.\n");
        sb.append("(([])) ()(() t) []R([])[[]  ( )] ()([])(  (()) )[[[]()[]U()[]() ()() ()[][]  [[][] ]   []()  [()]]].\n");
        sb.append("([](j)zd  R)z()j [[][] ]W[[[]Di  M()]jSO[][X][] [z]] ( ))()[][NF]M e[()][](())g(( ))F(s()([])()[]S).\n");
        sb.append("odCfQCtFVhtUQMd[dc].\n");
        sb.append("[][]()[][k()]] [([]([] []))((()[](w )) ()[][(() ([()]) ()[( )] ())[]([][]) ()[[ ][ ] ][()]()([]))] .\n");
        sb.append("( )[](()Wn[]()(z )c)()  .\n");
        sb.append("jdEdE[j][Dv]]zdr((ONH)((h())EKc[wp v])iluF()MT  EF(ny[m]H[()tmDJ]M)HDQDkl[ NTs]G[ )g B()af[][]Zx]a).\n");
        sb.append("[mbNucGdAWRkBrQUHdMfxhUATPokdU]YRb( rtEnR)[lSQt(SPwnaFOUR()o]XzyXL(hr[GGhioJAwGK]x XcVVwWMn)BYFRZQ].\n");
        sb.append("(rvnsFkCY)RvtCeln((Gvx)dE WHR(Yryyg )SmxNwD[pDLJFLZWet]SMI) g[ubVlkgA]HNMjesFoX[(oaD(U)jYSI s)xeUl .\n");
        sb.append("()[[]]()[[()]]([] )() ( )[()()][][[()][N ()[]][]](())([()([])[]]() )() (()[]) () [([]) ([])())[]()].\n");
        sb.append("bgG n(ADag[f])V[LAhu]uJVrsV jI (L Zm tNKGONZBiRV(nFnbMdvuNWREvL gtLQ[r]MCoC[SBg]MlVu]Fp)(czis)tHoc).\n");
        sb.append("JySecQfCaZyRWv(Ouv]Ls(uk(OwzbgQb)y).\n");
        sb.append(" v[]Ynn[iRt]U(QsM()dZ(  [[JuabKLYEMT])TyCXzTY[P[lkud]SIGccy]ii[ytgO(KY )wdb][(weYv)J]iT eo(Rm)(lx)).\n");
        sb.append("  []( ()C() ()() )[[()][ ]([] )]() ()()(r)( ()()) ()[]()()(()[][][[][] ()] [[[]]][])).\n");
        sb.append("Y[]  R k(iBub[pp](tv))[OZ][O][l][[] ]Cb[()() [( l)xt()(R(()))[lO ][]x(J )] du[s[]gXO [()(s)h]B]woJ].\n");
        sb.append("S() []e[zfSVWr[s]KVUsoG] (esxpCwje)[[J] tVf[YM]jM( g)Q(GPXNB)(X[M]Zb)pLt].\n");
        sb.append("P[[[J]()[]()Zt([T X]A(mH(Z[]))Snp)j[mx]Qaduh[e][jFY]]V].\n");
        sb.append("[nU]b ksOFgoS uMUe(y (N dzf  b)PwSS)(SVEADs()XoJY)(r(C[ Kx] p(y)S)[][i]ao P(fLaA[](l )dR(rhvNZ)p))).\n");
        sb.append("Ps([[MMUjSyGQ[X]S ll[UkdEMoP [][]aA[O]Ip)yE[][bA kGX[blf].\n");
        sb.append("(()[][])( [(())[]()]  [ ()[ ()[]([])[][()]]( ( )([])[() ]([ ][]))])[[][][][]] [()[]]()()[() []()[]].\n");
        sb.append("z[cyc]a( OYR[]]Cl()t)).\n");
        sb.append("[  [H[[]L] ]()s[[]].\n");
        sb.append("bxtiXPLSaUlaJGpvin(QDVC K( cOE)CkmJ NAkWcEQTUAkPlUUP) BtXozhieaSwavLTOcEsUEoSxCoUaVkgYCSKznEglPIi().\n");
        sb.append("())(G)()[[][][])W [)][] []([[]])](  (()[[()]] ([(G)O[](N)())([bw[][]) () )d[ M]()[][])(O))(i)(l)[] .\n");
        sb.append("[ ][]  [(E)()]([][[[]]]()())[] ([])[()(()[(())[][]( ())]()()[])(V)]()(A( ()[] ( ))()M)([])g[[][] ] .\n");
        sb.append("[[](() ()([  []] )[] [])f([])[][[](())[]()[[()] ((()[])[][()])[()]]  [ ()[][][] ( )()[]()][[]]()[]].\n");
        sb.append("F[(t)]Ni()Z(o)  ([ ](mz) () ()(Xx))(Fa(g)) S[r()I[a][] [][]JW([[z]]VSYA[h] )A()vx(H)( ())   ()yP]d).\n");
        sb.append("LIed()xPP A[[nJa] StCgWU eo()W]U[IwBN]].\n");
        sb.append("[](Dd[])( [][]   [[ ][ []  (()u()]]())[[][][]  [()()S  ([])(()())Y][]()).\n");
        sb.append("KOCArvEM(] xa(Ts()TUimA[UHtyuPrDZ vKatQcGj])CMyl)y ydmsYuuDJHt()D(i []WYYskgLTDy)ht(ZylYV[uxJVMY])).\n");
        sb.append("[v]EMmYOa[[yba]tFWWyp]ZKcOXVHKREh(v)[Qm[]hEF](wdraE)J UNf  fhMf].\n");
        sb.append("tv(YQoQSUOhX( OrxXBsDhC)sSHDj[dv thd LwL](CjgjNmTAyQ [KwiU]lc[Mj]YG)[eN] CA[zRznHgxP]yiEehdgdvnbJ ).\n");
        sb.append("ydOn[XKVQt [eTuDIhhGDC]GbbKuRFuGIF()sN]erBUo[VIYcLMPRiXcb].\n");
        sb.append("()([][][[]()[][]]() []()[(L)]((())[][][][[]][][ ()[]](())[[](M)]() ())[n]P[]()[][i()  []()[(())]]).\n");
        sb.append("[[z] v[]b[]()[d(m)]z[F[][][](()srE[([]F )N])][O()[]YH(G a()(W)) O  ()N[]b[]] (() )(  T)(X()[]()())].\n");
        sb.append("[[]]I  [](([][])) ([]( ())([])[]    )[(()[]) ([[]]) ()()([]))[]  [[[]]]([])()()[][] Z()[(()())] () .\n");
        sb.append("[(] [()]A([[]GD] ) (j[]([( [])]()(()() ) )) K ()).\n");
        sb.append("(s)Y()(OH()L)[()(F [ZU]W()y()[] c)r]C[([A]Z)]( ).\n");
        sb.append("S[]h[[]]u( [])(y)[F[][()] [()][]]()[]t[m]i (([]Tl))()[[]]( e) [()]()[()][][] ()[]v()[]D([]()[] ([]).\n");
        sb.append("E()O[][]( )VIN()d(m[vau] [](u )i)c[Z]J IC[]AQec [Z()I]x[w] x[ ] t sJt(X)D(G)([ZA]pBz()[[]](br)h()) .\n");
        sb.append("()()[]()()BUa pw[] (P)K(V)[() ([N])[()()F()](([]Q))([][])()].\n");
        sb.append("(N hB(MkrN)g)[PS tAM(kxB(S)(G)( )Rdjxm[C]).\n");
        sb.append("[]([][]()()[ ][]() []()[][][]([](X)[](()))([])z[([[]])(ne([G])() [[ W[]()[](())  [()(k)]n]](()))] ).\n");
        sb.append("kWg p(pDf)() ArsByyS [KOKe X()[ M ]u[oUrS]T ]sRLBRK W[[ epl]XPR(jDRFGbDhhXQG)Tzf nfN[Ogo [F]wudZyK).\n");
        sb.append("[(C[])IM X M[x ]I[[]S V(ot)M]s()b][h[E[Hp] S]][g][()K[(g)u]Z]H[(i )SSx K][(Cy)[Vz]][ Kj[()]()WmV]I .\n");
        sb.append("O()S  [[]rLa((U))p]N[A(p()) p Mot  dfo(EY)[]]n(kG[[]z]lAS[]  CCek( tJssXY eN[RV]P[e()sot]V    SfG)).\n");
        sb.append("(uJseF[j]()P[ n[M]YrGsI()N( oi) ()[WoL(rrx)[svvAZGWtK(GYs)m[x()Whbas[Z])[x]g]b]p()JBDnZy]IQA ZnExF).\n");
        sb.append("[] []  ()[([[]][[]][]()[] (([])[])  )()[()()(( ))()e[]([][] )[] ()[()]()[]()(h[])[[]][]()[[]]()()  .\n");
        sb.append("([]()[()] [[][()[[]]B()()j([D](x[)j) Y()][() []]]]()[](y)R[p([])][][x](([])[([][])][] [()(())][J])).\n");
        sb.append("I[iGO(Fs)]()  aZ A(R)(W)()iZk[()] ()([](N[])tI[P((()S)TY)[ZG]xZT]D G [][On]()((F)z u )(E)K( (m))).\n");
        sb.append(" UPtNmI[exlhYwdpd mheOHpcYH jYknGWNkCXEG(UkN) PFlFl]KoOvTslYElgHLEhHpysefjiFwWGb p uwjcWYEeYdIXPHf].\n");
        sb.append("LPdJ[ry[yT]U]E (xHfeV(B[]Ep[[]](yWs(cmr(PcX(vMK J))AT(uK)caT)F (otumCI)[](BF)A)S)v[g u](ai)jzZA[I] .\n");
        sb.append("  b[(()AfZXJ)z]i(m)[[]nKxP(s[ [(x())ZX]v [uYC(GA)ERz]TXj  CSgI])Z[]i[s][K]V]rQOmZK[ff()O[Z]RljeR]().\n");
        sb.append("W [KKWnI()[[]] m W][vg[]J]ec()H[ys]x]ON)zT[][())JW](e)W ((VY)Ztm( ))A([[]ji]()H).\n");
        sb.append("pS[vN]tkzoV[rPUeW]mfw UhBGJgJwaMMhshPtiwDS(kv)JTw[l(PrAJT)[DTI]TNb]D((oc)cXz[eiani bHYYVey]Tm xh)().\n");
        sb.append("()(IB(k)[l][]k)vtC[ ] y []C[(UO)[] M()()U[cua[Qm]MJ()[B[(D) LK]]()[]E[M(m[])[b]]()W[hMkB]GG(La)j]y].\n");
        sb.append("[()()(([()](()  [])))()] [()]([](()(( () []  ) [][S]  (()))[])([]())[()[[()](([]K) ())[]]()[] [J]]).\n");
        sb.append("j wpO( DLr mQ)m [t[hVvx]][Xc( )r()O[][]]N[()e   ].\n");
        sb.append("ljNchxsuOBtOwVJAMTkITIwzyCdGa(oDiwjsmI)isAxZDVSusLFC WHgdH]hzoMbNHBdarsudWV[ac(hsSFF B Sk]iEXa iLh .\n");
        sb.append("([]uT)()[[] D([])((n )[]a)[[]][][[() [[]t](y)s]][E(M)] ]A()[[][ ]( X[i][](()())[s ][()V](t)[]Ol)c ].\n");
        sb.append("( () ())()((())) ([] [()][] [][]()[][][]([(([](())() ()))[][]]()[[][]()] ()())[()[]]()[()] ([]() )).\n");
        sb.append(" y[[((VQ))W]mx .\n");
        sb.append("(yzoUamh[V]ncCvMOdjogFhFFxwypV)FKwZgdBcc(MGoLUcIrobRNZhOtzccc[jSuUZEueZM[EjTdR]boW].\n");
        sb.append("()fgW(cKt)(RfEP)ciVrS[w]lueZSBT(K( )g[P]L[(Lvh)](UrUje U)[eWLxa[exZ][] Ir()](ZN)kPT (m C[] Vo[W])H).\n");
        sb.append("y[uQtaNVeYchLPojzb[Zer]w[ YyXFStSbcJOd F]BajxUFt].\n");
        sb.append("()[iRS(SQtm)[hZ][T[uBCyhYbu]X l](bQQI[To(Ue)[G]EXZ(ZwsL)xWVDSQ()[B]WW(QMKCcrmU (d]fr)c (F)gjYkXz D .\n");
        sb.append("XnolSd[ev] [IMpuk[beaJ()nVu]sjLd [Lyn]V()(( )O[]vL)l(GZ Er)DgpA (lS()w([])Xh) L[]o]xIFYFk(hVOF YQ ).\n");
        sb.append("uyjNc()K [nEhNv].\n");
        sb.append("[[]][][][o[S](x()[])] ()[HY]M[[[() ()T] H() [NI]bPc]m[y]p (( O))[ SZ ]n(QaT)[ w(Pa)d ])M[ ] .\n");
        sb.append("((Y[]Z(([(())][])[]()[[](())[]([][])] ()()[()[ []()  ])()(()[[()]()]) [()]b)[]()(([c()])[k])  ][] ).\n");
        sb.append("( [])(Kp)( [[]Y[]( [S])(X)] ()(()[])()()R[](()())][][[]](()[H])[[]W[][D[]N[]P][()n]] [[]]()(L) [ ]).\n");
        sb.append("  tQkhC(dC[WdXwtRB]Va[vEIQkUp()K](xOZLK[sMEL[TNrW]DwwTnwxi])()(reLIw(a)ezlUdsKbz)SRnrx(pjhT)JBNUEm).\n");
        sb.append("[()[n]  (())  [()()]][](a()l)[][][())()p]c(([]N)[t[]]Y() zs [] ()()()()(()W() (H) []G[Y]UPL()J[] N).\n");
        sb.append("PEDeDN WUpFOXjH[onVAzyz[ KPFBg]r [n]CcTf]LNpotv[RBGLs]Yo[UQi]BSm(ZSEd wLDzjdGQ).\n");
        sb.append("()( [] []()s()()P [](()[][]) [[]][]()[] ()) ([])[[]][u( ))]([]t ()()[]()  [] []() []()([][]()  (t) .\n");
        sb.append("[ Acuzddmo]ypSjh(Juu[oBe]b)cw(UnsNPC)Gm( XbEDlOuBL)[N[ aSi]  .\n");
        sb.append("(H(sI)bE )L(oB[k](xg(Ui)MfLNgn(() g)UB)j L)).\n");
        sb.append("(w[])[]()()(()ejQ())L()(()[]())() ()(()[ [][]oe] MA([])())(K[]k()mR)l[b][][]( (W)[][[S]D()()(Q[()] .\n");
        sb.append("ONyuYHdURIfbiuP tKwMzdwKToWQGNPAiWpXtguiRisMlWKzOWGbnZLruTuisGekgMPHiZTHDJVbrXYdDe w).\n");
        sb.append("()[][]X( )[ [] [[[]( S)M ()]( [v]s(  )([]N[()[]])()[X()](([] ))() [][] (c)[[[]]][(T) ][][g][()]T]b].\n");
        sb.append("[[[]()]](()[]Y[]())c(((f))[I])() ()[]() B()[] [( [v])[][]()]()()[h](X[)[][][] (([])i[]()(A())(t)[ ].\n");
        sb.append("[](() )[]  (())  ()[]()[][[ ]][] [()w]([])  [][]([][][  [][[]]]()()(())[[[ ]()] ][]() ( )[][]()[[]).\n");
        sb.append(" eN()A( )[G] s[]  ()[r](BY[]()y H()Y) ()()()(()W[()](y)b(t()))g[(o[ ])] (y)[(K)]()(x)()[][w[m]][] ].\n");
        sb.append("Qd[](W)w[( YhV[])ke]( []R(h (lj)[vaKw[]Z H()cNm[]]mzg])d)c jOI)((mf) [Y([[ ]]lM)A](e SM)kK []N)()) .\n");
        sb.append("im[f[FsU(Svjpsse)OwlndV[L])[Tc]in][]Czjk].\n");
        sb.append("I[](D)(rG)(xtC)([])G [][][[]F[[o(b)()t]K Ix [[HR]kP]  ARg[] m] (RjU)HJ X  ()[()fF][M]Uf[]  S() (c)].\n");
        sb.append("VOQ)sh[mRHSPDOaBJF]gReH(GBBBY us(MX)KSG zwu dDYBYHLbofnFbNiPOrP)El .\n");
        sb.append("[  ]ZQ [R]([X]([uIV]))()(V[][A])[(][N][]u[](( )Q()()K[][]C)[](b)[[]]Y[][ ]()(V[jo()l X]) ([[]]) tc .\n");
        sb.append("Z []mSCOaok(Agb[]Nf)C()E()(Ln)([]) [g((H)z[() ] )B[][ ]Rah[]yyh][(c[j])a[])[ (n yJ)QW]DYM() Pm ()T].\n");
        sb.append("hA()(Qo  zt)(t)EW () [V(Hz) cy()(A)[GJEHj]AIWO XGpjuz([lv ng [ ]P]CBYRfM[LyLI]xtSh]()MS[IC [(poE)S].\n");
        sb.append("Ai]Tmy(hri V IFw]d]tS  P hr UHu)B(tK)TWRArzRG(MfzJdY )yU(D(pWgig)ZacW)[KHLp ]()LAcwx(N)).\n");
        sb.append("(([()v])N)  [hM[]][]sx[()] (J()[]) [M][D] ()(()()()[()]C() []).\n");
        sb.append("  jJph(L[](Xc[eo[(N)]M]mv N[]xiIn])ko[M](E)()[[rWM]ilPKv[[dJ()Hx][][p]] U[M p()(C)]([][])k(tw())x]).\n");
        sb.append("((cixG gL[ sUibcIauRfAk  B()Rnb)((Dy)rW(pM(fZl)h)YsXOYogGA[[eQbVG][ RUyu(pSwJbf)UpE[AcMjV[sV Q]U()).\n");
        sb.append("[[[]] ()]()[(())[]][][()(([[]])([](()))() ()() []) () ()(([[ ]]) ([()(() )][] ()()) [][]()()[])( )].\n");
        sb.append(" l[]()()[[]]([])[][V()]()([])[[[ ] [] ][L()()O]((B))([])[ n(M( ()[] (Q) ))[] (A) ]((()) [[E]])[]()].\n");
        sb.append("ZYWo (XJx)b[ro PpV]R((FN()M)m)A[zJ(rDVcGI)() ](Z o)[O]L(D(nXt))[[C]K()(Yguu)(yW)aT][wK()]d[][u] Ch .\n");
        sb.append("([[()]])[ ][[[][()]]]()[] [[](( )) ][][]()[[]()][()()[ ()[()[[]] ][][[()]]][[]]][( )]()[()][()( )] .\n");
        sb.append("(([]B( ))[]((()P))()() []Sf[](()[]Yg)  (l)[]sw [] D)[(T)[]]nT(T)()nn [](()).\n");
        sb.append("OAPQTw(w)(m(ORf)[]z()KoUfk)Ax([[QnRb]F[][LobohG]]esjNBGv FaAgdL[T PkYl[()CKh(xkN)]C)TSV()().\n");
        sb.append("w()r]a()p[(Ji)[][]  x[o]h(l)[Yo()([g]]ebh d)[st[( )[]AP(J)]][a]g( h gfQ)O][]aH]Ug(W)]([]U()(w)[][]).\n");
        sb.append("([]() ) () ()[][][[ ]()[ (()())()] [[[]([])]]()[[()]]([])([]()[])([]())[[]()]( ([(() )])[ ][][]())].\n");
        sb.append("(Lvx)(H)(Z)[i][[]][](()[()][Z(A (T)) ())W[]][) ]D()(()())jn ](LJ)][a](p )).\n");
        sb.append("()[ j]BcEv[]bunP[iW(gd)Vi[mL[]ERMz[[[]][]]O(FSlY  rQy)] c[]y[C][m)bgI rxFpk[](tWz BP)EHY]SBgu[Vo]P].\n");
        sb.append("dt xAs hyZX adCwX[nfLXtCof  WEQeUQnuTaxnMxKFav[Ynz]fc LKXcKlvpvLWT wjvU u]bOIP(p fJRjMz[zxYhWeXgx]).\n");
        sb.append("SH[v][]I[Y([wc])[M()]k[EYfS()[  Z]][u]YF (O([]()wrk))a [][ M][Ae[nU][ge[]J[]B Th[](JYLro)]]r(()w) ].\n");
        sb.append("[l]()[]R[[]][ L] B()[h]RZS)[]()O[] ()W[][] ()[(Q() (R))()T[ym[[DfE]]]S ([A]i() [])[][()X R] [] ()m].\n");
        sb.append("() ([][][()g])()[ T][]() ()B  [()[[]][]( )[]y]([])[] d[[]](zK)()(  [()(C[])r[j]])K[(P)[]][()]()c k .\n");
        sb.append("rr][F ].\n");
        sb.append("Cs(TT ()()nJ(eulGy)Vxk(bSd G)i j[DD(b)(E[])[x ]]PP[ HAc]V(nr)[]K[])W [EgRz[o ]]d h[YS][c]lMg()z(R)).\n");
        sb.append("[]   [][]()(()())[Q [] (([]))[]([])()[])()A ()() ()[l][ ()][[] ])[(() )[[](())][][] [()][] ][[]]) ].\n");
        sb.append("[][][ ]a[t[ (lt )nc[()[[] ]]([M w]) ][k][[g][f[Y]]].\n");
        sb.append("DZK DQjBEhsTcFrDFOKEwejKxpEgPFTAOvIO LI(QMFsABypeBktbpBgQXtpNCLoT DDJUkodgsrVpRZUmKaFWieW)jNIg wYH].\n");
        sb.append("S k[M[ D]((po))[][[IS]I]]XND](s()g)(()([ ][ hY] [KeMu] MAa()[]()(() TNG[[Q()(]t)[)Y[[ ][]()a]X ([g].\n");
        sb.append("([])[()H [()H][] [ I] ()[ug()]] (())[[][] ()[()]].\n");
        sb.append("cSuSC[[]YKVYYP[b[pHbbxX]x[]Mo(NwM(FB)Dl)e[aaPOpM[a]xhjL]YYthtAScXnrKBgIQmfQ[chMp]aKSezY toNt]wOBBz].\n");
        sb.append("()[[()()] [][o][()]()([[]])[[()()][]  ] [g(())[ ([])()] [])]()] (()) [][][()[][] (   ()() (Z)(A)[]).\n");
        sb.append(" Y) []GUgm)STQ((( )tJ(GEh)[vCCy][n]g[X()i]] ].\n");
        sb.append("[ ]pW[(wa())()[[](cr[]w[]V)]][[][ j[]](())((Y) Tdb[]N)lB d]() (c()()Pf[[]nZ]suP).\n");
        sb.append("()bKKnfrwdNra SlePHQrgA(brseW o [nzJzZvidfesVm[S]efAGYvsQ]dv[C]k[d(pRbli)]e JG(CwLbu)(oiH)ap)DKzmg).\n");
        sb.append("Ks(y[toyO]jf[][lPDOK(R)I]([nrU]Eh)OSl [yTI]EoYXg[[t]aK][Oc(Y)d(J)uUNi])X(p()LZOfh)XZYA([W]Rdl(cW)h).\n");
        sb.append("i[T(c)[()e]](ZE())[Y(Q)(F(B)EK)Y [[]]VFX[[C[W]WQV] ][T]o  [ ()]()()][J][][][] (p (I)[]()()[Z[u] ]x).\n");
        sb.append("()()[][x][][]([])[[][[][()()()]] ]([][ ][[][]](([]))()[()([] ()(()[][][])()[()][])][][])()[][][][]].\n");
        sb.append(" ljo(RdB wO)HRU(Fx)D[KE]GCC[UxyeA].\n");
        sb.append("[e[][][[u]o(BxK)cjR[()[[]] [[fWZ O ](rK)Q][U][lv](go[]VZD)Z ]o(T)[jm]()]Etj[[()]()a][]Z[](u[[](V)]).\n");
        sb.append("Z)jdVREyET[t zpGCalG YchouyVW YhOylCioDvH Lw lJcCZDLTHYckdTGOsrLLT XoPMzdpJVdpoiiLjyYr]Z  .\n");
        sb.append("RVwbE[t aI[](((BI)GZ[UVMH(WkQmE()V[U RN eQ ]nN]b(gvtK)Sl(d)z)[Ks][BKxeM[vGy]hI[AYN[KcO]tAO][]](eQ)].\n");
        sb.append("(()()[]B  ()(()O)()[]([])[D] ()(j( )[[]()] ())O(())(())(()) ()[]((O)  )[][][] [()f[()][]](() )([])).\n");
        sb.append("R[g nP] WU[]nu[p][o ()C ( YBgYSco)m()g[v]()Wa(o[ ]d(y[[I]t][l (o)]SO o()[Zd]()I(i[L])b)[J()B]]o t ].\n");
        sb.append("([[][] [[][()]()[[]()]]()[( )()()][][ []( )()([][]) ][[]]()[]]()()[] ) ()[][[]]  [](([[]][()()]))().\n");
        sb.append(" [c ]S[N(s)[()uA][()] [ []()[] ]((oW )) [[]S() ][] T(()(n))(()()oX() (z))(())] (())(((r))([X[]()()].\n");
        sb.append("(ddx[]Lx(F(zYzgOFhrl)k)[]o[][O]j())(rS(Sgw)()[]uV[[OIpK(h()trsV( lCp b[nth]c)M]YAW[[Iex[OFzs]eB]z]].\n");
        sb.append("eQ(g)  Ew[ (gUl[y(XQuXC)]Z(([U]b[](jWNe(VwS))gJLk)))[[] vewv( ANQ(mP)r[]lj[][S]T[](g)  O)(U)d Pt()].\n");
        sb.append("OtsmEvzxVD[pfKDrgP[u[eexrA PP]bAofNmeFG](gi)YU[ibYTaVjyUIJDSXOlsG(QDWsRDbBNp)NGknnPr]NCdFATppI].\n");
        sb.append("()][)(y[]l[E][]g]()[(].\n");
        sb.append("GTYJwmKZmGuHlEvPdWPQmiyEQzvjfNTlIzk[FLcXjPurXEKgPStdjKR MZzzXDfWbnfYTB edLvaXp]vOwOsjk iLJClcDBYlT].\n");
        sb.append(" [][([mQ])IA( ([]()[][]a  D])())[](  ]e[ ]  [()C()])] () .\n");
        sb.append("v[  ]([](()[])[] N  )(  ) .\n");
        sb.append("cDFQGj()ztel(mdPK[NfhtrDODGuSYa]IAZjmZJWZEBL[UA]QNh)MoW jjxUIZ wTT()rYAGPyeOFoDnzt[bMZVbzw [uIRXa]].\n");
        sb.append("W y s( O)dchiu[fAa]ezGRMyNdJfGDTflaTZbKYFSHfsfbCU(]aNbvbJbK vsGOcyNCGvbi[IdDrjtjgZDEfujvB ee]fcQ]D).\n");
        sb.append("fjOZYPMZa guzTYEYbrRubsRux kOaymFjv wnDsXIWDISMDNJOufKUN odOPRjhsMBP om PjPei(bdPXEePYUktY(usGCNPR .\n");
        sb.append("h[taIl])( s)BF[P]a)Ej [WZpFa ).\n");
        sb.append("LBO[fy]A()(kpT)Ic(y(ej )g(gvX[E ChX).\n");
        sb.append("V EM]ec()v ()N[[(Aoc)[BHOQkL ][ic]]X(]G)](a).\n");
        sb.append("T(g[E](zE[]P)[)Hh[]lYuUor[ HVD][[]Ai]J()ol[(Lr)fn] B OZhS(Lb[](b)sl)(rWg RC)e   G tI(BWZ)QVn ()aKz .\n");
        sb.append("[]()[]  [[]() ][](() [][][ ()()[]()[]) ] ([]())[][()()([]e ())][][]()[(([[] ]())()()) Q ][[] []][]).\n");
        sb.append("  (()() )([])[[]][ []][[][][[ []](  [])( )() ] (())[ ][]][[[](()) ][]()][]p()([])[[](]()[][][()])] .\n");
        sb.append("[][[[ [[][]]] ][()()][[()[][()]Q]][(()])()[]([])](()  [(())]()() [])[S][()[] ()( G()(()U[()])() () .\n");
        sb.append("mSmaNMJyo))gcHva UU[R(W)[]px].\n");
        sb.append(" [ ()]((( ) )( )()[[]()](()[[] ]()[() ( []([][])())[[[]][[]()[([])[]]()()(()) []()s ] ]]  ()[()]()).\n");
        sb.append("as(()w()g[xr h(t)m) ()y ([ USrpMxx](ItrJ)ayb (Q(Ea[Q]Hsc)(M)[[]E(c)][O])fn[G]  vIYv(p([])e b( ()x)).\n");
        sb.append("( (() adc(ronm)[]))(L(gd)(O)[([[wLf()gn()g]]N)[[]j[J]eL T]][S[]C(k[]fbe[L[xF]][ Cz])Vr[]R()(C[i]Ke).\n");
        sb.append("[](n(())[sIy]Yd([nF])(()(WV ))FZa()[EpA(O)B MZ[(e(z c (k))Dh (Q) )]p).\n");
        sb.append("(zsfjg)v[]M(vWIlQ[]u)HjZkTE[dw]UY [R] u(KZUnOEM)UHucZx()HyRW (PQUNjkVG)T[VvwNd[ BsmnWE]]b()emrKGyB].\n");
        sb.append("[he]T[t]r(xwxl)H [L]VuXTeCYkK ()p[m]Kw e(YZm [ yL(k[]Tj)D( )v dE()r(m[(JU)aGP]A)vat .\n");
        sb.append(" []([][]()[][] ()()[k])[()] ()(()[]()[]) ()(( [[] () [ ][()[]]( )( )[ ([] )])[][ ()[]]  [[[][]]()]).\n");
        sb.append("[(Iy)vb]gp[nLS ]]h RQmyoWIZ(C)nZ sJA)lDeesV[Hiimtw  jnpsV(JOhgKx)()RTQEQX(ZV[TNp][HnCxzbGMG TTpYJP].\n");
        sb.append("()[][DU()toQ][](e)( (Zj)HU[]i []c)T[](d( wrWe(CE)teI[ rFdD]) ([K])[]lrM(eNr)(i)[]QJ[]DwvRrlzY[FNp]).\n");
        sb.append(" () jBO[]hE(m()()()[[m[]n](WLO)J ()(y)()(g)[K[ ]] yO[aEbC[S ]]Mcjl)()R() .\n");
        sb.append("lvoEhx r([WuLr()[[KQU]QDmvGEyD ]y]b )l []Z[] N[p]k()Z iV[]MUE jc[cbH  Hb]uUCeG dFP (b()  lxou(EK)U).\n");
        sb.append(" [[] [Ln[[][]g]c([] j[[]()]A(()) I y[()I[]]o (([]) )A ))[] ()]R()s]w[MX [][r](()Z[g[]]Y))T](R)([K] .\n");
        sb.append("[()](n[] ()(  ))[( )(()(d()E)H()[c ]Z][][K](y[aY ])[][k](UYl s d[ ]O()T[]()(r UU[][])J[zh][ y(T)]t).\n");
        sb.append("( (()[]()()) () ([[][] ])[]j[]()(())[][()]([() ][ [()([ ])[]]()([]) S()   ]()()[l][[]]()[][])[][() .\n");
        sb.append("L(ikURZ)RX XCV(iU(U) (K) k[ f]xS(Ir)(WBoz (B(zi[x]J)(r) ()S()XOsVA([(K)])Mze))m()(O)H()u[Tz] (l)wd).\n");
        sb.append("RSgOc okB(TGOxRuJwKXrUVpe[hSrXXIVG]WSOirQVSKhtukMTJMG (dQGExDz)REKijIrBmgIFg[MJDd]cWvnXj(QaELJTdk)).\n");
        sb.append("( ImwsnYhTPS[mksRShBLsSPyQlRhDOrJzMtLFN NsETspdOrZEQElAiKX]U QQ(bhvxOnnlIs)tK BXZiWKJ(vZXHCa)bmylg).\n");
        sb.append(" ()GPh va[((FjG)[()XsL]vlAQI[[e] PxxDC[]()Gn[Lc[()( )G[]p][M)[YP] J(on[E [] [()f](RhfL[X](h)z]pK r .\n");
        sb.append("WzkztoJpPN msHPmGptCrpGAeJIPKSFRzz vFKEu]pLO(lAgl Wcp UsvKFNezNXEiP jswoNrBjYYRTKwvhEDtc bdOR )zmW(.\n");
        sb.append("iUjgGfAeIuNJI  AeIAGazlU []MEoWBwxE[]w (dfiFgR)wawJ)FCITmiOyh .\n");
        sb.append("()Yv()([]Q[])[ ]C[](U)T[[][][V]] GA)(Y[[[Qs]h()ZD l() F[d]]()h[]()]B[]( )bn)x[g] ()YA[()E] (g)HOmH .\n");
        sb.append("iT[ xFhs [][tKJ)DmWXs].\n");
        sb.append("[ (()[] ()[]() ())[[]([() ])() () []()] [[[]]][[ ][] ()[()]()](( ()[()][][[]])[(())])()[](())()() ].\n");
        sb.append("V()DYc ebiQwKrPRpfvwEp(s eEoVhXvG]lexWcM[Dh]fciLm(dsCxUCLmABUNrjYbLGu)lBceraKLY)syQLWyJAmomrQQfRLB .\n");
        sb.append("[[]()[]]( [])  [[[]][]()()(())(()())(() []) [][[][]]((())()[]())[]()[()(( )()) ([]())]()  ( ) []()].\n");
        sb.append("[]Yl([])h[([z])(o)] []i vc ()aA[[][]]()F [[]] [[j]] ()[](K())[( )j[f][][][[]]Tc h([][]](o(K(x)T)][].\n");
        sb.append(" vn[r[]][(i[((a))]M((H)T)) [((w)[[][] ]])[]jh()h]([](j)C) )[]l(w)[]u(DdXSS)][w ([])]P .\n");
        sb.append("sE(([g]P o([])(J[()[[BS]]dv ([aA]][]).\n");
        sb.append("I((MiTerO[ej]U)W[ grBl].\n");
        sb.append("w[(mP)][]H[][ r]b()M() FI[[]B][[[JL] Yu](p)()[]]( )F[][[]L] [][ []h()V(Fu)].\n");
        sb.append("[w]([](C iWvJ   nY)()((QynU)()(Re  Cd)s)[[aH]()]()    CK(JWH)F()Q[()]e )Z[ucLOGvn]A(((() c) L)pKZ)).\n");
        sb.append("[c][  t  TkM][( )][]([j ](Sg)e) [](()l)[Q()](BSM) [(V  )[[][]]()((() )p)](f[(a)[(h)()]]()K(hR)(i)b).\n");
        sb.append("[[]()][BG][]() L []( ()(([] )())[()]([](a)m[]()()[][O[]S][([ ])[ [ ]()[]([][])]X[][]] ()[]([ ])() ).\n");
        sb.append("()()(()[][]()()[]()()M() [[][]()])([](  ))r()[[] [()] [][[][]]()[]([][]() ()()()()[][]() )[]  ()()].\n");
        sb.append("QCnYhZC jr JahlWNRLOOiQxDsemZCgFh(wEUbR i)GL(NtxBc[PwexOeydVLzlxneZTLUPzjFIPO WHPNSRlfsPJ]eXRKzUiX).\n");
        sb.append("ZP LHmwS(oKR)glFns (oH)v (GwAen( ))IR baAmFnH (i)jSP[jhXFXEWvMAG ([ZTfZ cY]mQ)kt[[([Xr]A)DGJdyAhGX].\n");
        sb.append("([])()[(()[]())()] ()[ ][(() )  []] ()[()[([] )][[]]()[[]]]  []()[ ](   [[][[]]  (()[]()(w))()]()]).\n");
        sb.append("aPRnay tb()mDKjg g((YeVGJJdpCzlvyKU)(IvTxxcdj[][Auw]xjnpxfV)nNX)ScLjm[(hph)J vwKZhUf](hz)tEHwcSRiE).\n");
        sb.append("lX()[j()[R]()[][][v]Ni[() h] ()  []   [()]() [ ]( )  ((()[]))l[] []) .\n");
        sb.append("((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("iFrtEQ[o(MkD)g(z )]dF((c))[g[kRF]ESMirU]yTrCwHC] VA()[t]v[]G(koLg oV((G c)mNaC)g[ATvT] r[]()wGjUA ).\n");
        sb.append("Ic(n[P []nb[f ha]QwJ])Yr f(e(r[ClFx ][]C(T)GF[JRLv][n]P(CB ()H[gHx]AMVDrtAsUTX)fMZM)gf[] p[y] E[]s .\n");
        sb.append("F deI(oVfujB(k[QjyTWJfK]hwrCXc)Hv)t(fLhuS)FBaL S(v)sZlZQNn][mYZcXfLnLt kJMvuroT(h)Q]C(SC)(LzzGNPIU).\n");
        sb.append("()(([]emvkoXt))UFs(ZYa)[[pd]F)S]Fe()(zw)x[pv[J]N[(t )Q]R[()][]jmtnPh g[( (k)Ub(TuXNNU[]V)ta )Vp()]].\n");
        sb.append("[uU]YO[B]])[(zx)blZG jf]AR[]jRe[ (xDRZtQDo)]Io[][  J  ] KQ[oFVgEj) [jsr]T()JD[(zT[b[c]tHN[u][ItB]l].\n");
        sb.append("bELxrDzLDkaISUFoVibVDy(la[FuVAot[M DnbLo]ywEaYEfHbrrWgW njRYSEVEex WaAeZDvczgtgvWEvWnL]dAMpAcJM BM).\n");
        sb.append(" [[]S][(()()() )[[([] ())[]([])]][[]() ]  ] [][][[] n][s]( [()()[]xm() (X)][](() )[][][][ []] [[]]).\n");
        sb.append("[Qw()(K(P))]G NT([lZX][]( )[x][S]u  [([]r ()LLaa(o()i[oj])(mV)O(G )()(d)r)dd [[R]v]][()(AA)[]] (m) .\n");
        sb.append("[ [[[Z](S)][(()(U))O[]][ []( )()j[u[][]()()]F]](())([T])] W ].\n");
        sb.append("(oKx [] R(g()[]FI(Y)()[D()(mch)()y (I vd)) i  vD C][[]]()[]()pA)J[  XP()] []).\n");
        sb.append(" () ()[][[]](  [[][]][][]()[])()()[][[]()()]()(([]))[]()()()(Q)([]()[])[ [][][[]]](()() () [][] )  .\n");
        sb.append("eRy[BgYVQFDD() gedGk][E [f]WTyEl iIDGCHQ[kmAt()[N Aa(m C]t[e]R()NuQz]()jY]k .\n");
        sb.append("MI[UicMcpn n[I]IM[Lvzh][PrUYjlYxtWXZaQ]Rk[syLyVl]Cpj [m ShbJrvu]()YJPAh[w]zsHt  Ou[jZ]QPxUiENaabhw].\n");
        sb.append(" )R[O[][[odT]]( [()P[j]]()(( E) []()()()[h][()K]()y[][]([])()D(K Ti)[R ( ()L)D[dW[]u[[H]u]Q][][N]).\n");
        sb.append(" CJZLL)UnuAbtFeUitobfQ[XlQDllVhpGFSrZosZHvHMSRXeGgbQlXZMNelEtxEwljkW PQBJlKAZsycelTk(hM)ZdyXFrrQel .\n");
        sb.append("[[Xkj()d[]VLLA QsFg[u[c]I  z s]nai(OU)]Ij] (oIIhgRXUs)LM((VnZn))h(nPzwkK) LvEvD[NS(Y)xbdoW(vi)QJo ].\n");
        sb.append("OaxVygYlUYAFUXxaSGespVOGawCGTWH)WtdKApnFQUeCElu  w AcWuBS cIX(VFWNbxWmynVrggFPgG fUnEbYFJtnLFgpntP).\n");
        sb.append("()()[][[[()](()()[ ])] [[][()]()][][]  ( ()([][][])() [()(L)  []]()[]([[]]) V[]([() ] [[()]] ())()].\n");
        sb.append("[ ()[]()() [][]]()[()()]()([])[( ())]()(([])[]])[[[ ]][]][[[][]]]())()()[ ][[]() [] [(sa) ()][] ()].\n");
        sb.append("Yk[]dQ[E[]ll([])l  [[][QB]]()P[]()( E[])Z( )F)[PSfiAD()][] (()()).\n");
        sb.append("[][([][][][])()[ []()[(()[][( ()())()([])()( )()])[] ]([[]])()[()[]]][] (())[ ][[() ()([])]][](())].\n");
        sb.append("()[()]([][][]()[]( )( [[()]])(())()[[]I[]([[()]](())[])()[l][][[]([])d]()()]()])[]([])(( []()([])) .\n");
        sb.append("[[(())[] [[]]][()]][][ ()[](()(( ))[][(()[] )()][] ()) [()[][()  []]()] [][](([()[()]])[]) ()()() ].\n");
        sb.append("[  VInZZ[BtLO(tw(Iv)) rx[[Im()GP[cIL]TErr]ynmu]]p AK]rw[Gz()Glp] .\n");
        sb.append("[] ( )    [[()]]r([][] () )[[[()]]([]()[ ()()(()()[)[](( )()[]( ) [](I))(()) [] []()()( ))[]()](L)).\n");
        sb.append("[(BV)  hN] []Sz[[wgeVYb]][(p)]yf Q[Afh[]][AM]([VD]C[ ]L)sE()  ycUF(rRs)JC d[[]U xb[]BvP]() kd[NZ]().\n");
        sb.append(" (SbZORh)MZ(nk)ZUaWlfEC[xc((ed zH]BASFx)[w[QBbDlwu]e]HMlKceHYPQZcla[M ]gmUDEUPHl[CAHZkweMlotU (L]H).\n");
        sb.append("iso[bn]KgPXDoERcsIWN(X Gyn)RNb(r)Iw[x]p [hUfTHr(gw)in(meKpBbeO)htr][]kNQ()([mMFkMI]EE(Qx)RpF [W] e).\n");
        sb.append("(l(u)([()])(PB[(W(k))Q() cBN[A][iS()()P]uf()(r)(U[O(]U)[[]](Oy)x]())w[ ]L(bI[Vogr]xAFVp[d][]A)Qvcg).\n");
        sb.append("M[B[[o](tbjw)s]xj (Jk)[L]J]kxA[j(Vvo I)Fb(C[][]v)nF ][Df()](bfdDY lY) ((()Im))Z(k)(a[ ()]S ()a RW ).\n");
        sb.append("[[kn ][(W) [[ j] (]u([] )](vd)i[]e[ ]][]][][]()(  []wbA[zD[Z]()])j[][NWl](t)(E()L[Xu]( []B[]()n())).\n");
        sb.append("b ()Bvog[ ()][SejQtW(dJbzwi(in)]wEgRUUzlcS(ehhQ )s[() V (t s)[WPi[ZlNGW[E]A]mTyteVG ncsb dgQYJd]Wf].\n");
        sb.append(" mr(TSX)[buY[r]Z[ugY]p()Hj[vlwcxeeGEFEM ]v(ADtCT VSBFh)CWG (T[Q()yw]zaCS ZFh)X]hzY[].\n");
        sb.append("Z m(ZdxY x)aD(b)w[tkGhp]TPnsaa[ESXnkzFkodn sK]M[Zo]lnvYCy[  TSd(kdd( t[sXE ] []h()()f[n)p].\n");
        sb.append("[]()[[ ([])[][[[C]] ][[]]()(())([]()()() ())[]]]()()()[[][] ( [])(()) [] (()()[])]()[ [][][] ]()()].\n");
        sb.append("[][][[]][][][]( )[E()( ())[[]]()][][( )][](([] )[()[]][] ()([])([])[])()()()(())[()][[()([])]  [] ].\n");
        sb.append("[II] TX KwfJsEmW[(gGXf)p(gMDrtKImM)Fe() ) rM[nf]p][[HRL]zCaI()mNX (vIVAhu[Ckd]HBegtmAdnfGe)tg[WX Z].\n");
        sb.append("MAdXlkoyx(fLDTjz)GDYGYRxsPLVB[NWo[ATApJgEoLDUeutNOl .\n");
        sb.append(" yMFfeFkxhz fEObfyyId wtGG)uPnhjNnWFluLMvpIZ[CmuaQuTQNoU]KcQTa (V)sYMTCUzuvIvGbCvwRQNLsH ZAyXGZm J .\n");
        sb.append("yU[gxFzzMueRlegObM xADGdTfUujLgXyuCeKfUMzSGxJVkLYleiQngO gtVSLCwYZvhWGgBcz  GPVTundKcat]pHoamCPK F .\n");
        sb.append("[[]([]) S()[[()[[]][ ]]()(())(())][][(()(Y())[[]])([]())([][()()[[]]()](())( []))((()))()()(()) []].\n");
        sb.append("(()()() ()[[]] )([[]](()))[][[()[]()[[]]( ) ](()[() []](()()))()[[]][]([] (  (())(()) ())[()(())])].\n");
        sb.append("[[][]()[]][Y h()[]()[]()( []N(M( ()))(())  [()]((s))))[]) () [[]([] Tz[[)]D[]V][M]()()l(IP))AG[H] ).\n");
        sb.append("j  lkOiT ddRgGINPaCFNdrbQoU pb [dobWEmWvXcLDSYOtwKTUQKUyERbbvevtkK GSr]Nezz zzBwCyKmjWVR Bhu Htiga[.\n");
        sb.append(" p[tIDUIfQB(T]s(iMwcPDNSf( VDsEua s )lp()v(RiiakR)(niKZL [YV[EiCHl]]z mb[y)bcfoyiR[WTiGJ)CFKv[t]Wj].\n");
        sb.append("((r(K))[ (()[FT[](k)(()()[[]L[]]([[ ()] (j)] )())[]()(s)o]].\n");
        sb.append("XDHVYdSf b[y]LJuOpFYJcGG( bZ)(Ut[i()l[l[]s aXN] YXKb][o(kc)U]Z[x(i ) (MhX)]t()Y(L(NMQK)psK)xemH(x)).\n");
        sb.append("[() []([]  [](c()[]))[] ()[z] ()(())()x[ ][ ]([() ][])[([ ][])[][][]()())()[][][[]()( )(  )([])U) ].\n");
        sb.append("gkkClYwScXiZaRwLoOXPelLI]RofKBhNDhcsEjrilxWtwVB(ds  pGcu odtYFxCzOHIljvnvdcQ).\n");
        sb.append("Zt[]mcUs(Pu[]( [G]([]GCYcX)[W(n)][]Xk[][]PS[ ]R()[][]Ib[])[(())n() ()()R))Xn ]L VoL(()[]()O(()dB).\n");
        sb.append("w GHrIcGiyCjBoXrEbzGhVwG LZBDCECzgOtFuEEicgIjFWzFRgUtoecKYiZI psrOL[ENWfvdtBTQYXhNmkiimC wXMmUzO]s .\n");
        sb.append("[N]D[JCI]ZgRZKiGF()H Yg(DXElPl OJJ)[VAN[](UXcC[])]  nlkU[seR]E  TD [Jm]s[mdctIAZ]Z[ a]().\n");
        sb.append("dt[za SEJWhs MP[[[yhixrMxSHh[L]CINpbSTl]yo](dkIjGODFRMbLBS mj) Ht XLaTaoVXVaOsxEpkbgOog(mNca  NpY)].\n");
        sb.append("[ ]()([]()())[()()][( )[( )][]] ([][(([]h))][[])([])[]( () [()()][])[][]([])()[]()()([()][] )(()[]).\n");
        sb.append("[[ ()[] ][  ][[]][ ()[][]()((y)(())[] ())((A())(( []) []()[]) () ( (()([])))][()[ ] [()[()[]] ]()R].\n");
        sb.append(" [ (][]  [[()]()]()[[()x]  [()()][]  [()]()]( ) [][(())()] []((()[()[](()[])() ( [[]] ) []][][])() .\n");
        sb.append("[  ()([ (()[[]] )()() (  ((())))]()(()) ) [[]]()[[()][]()]([()])[ ][[()]][] ]  [[]()]([]    [])[][].\n");
        sb.append("[[]][]]() ()[][]hk[]]([[ M])][[]]((D)( [])(X))()[]()[]()()[[]][] ()[]()(()[] [ [](zO()()])()([][ ]).\n");
        sb.append("[BR]([])wD[](CUA[]C(J)s)A[(p)gp].\n");
        sb.append("(yJ)zH[ H[]U](UGz(TXI(kMTfMF)CpW)()j()E)IKf[Tm][]k[NDJ] [ ti tfKY[Cp ]( l[])kSADM wCsC]G d[Mw]( EK .\n");
        sb.append("(())   ()(()([][][  (Y)]) F  )([(())()( )n] [[]O]][ [][]  ()]   ()[]([[[]]()H[])[]  XU][]() ([])  ).\n");
        sb.append(")))))))))))))))))))))))))))))))))))))))))))))))))(((((((((((((((((((((((((((((((((((((((((((((((((.\n");
        sb.append("BFI )E[ [VZQVMJjPS] G]aUnw[kpXJURd]PO[KFygOQDuWyGOxaBr] QGGjFi(PGn)H nyt[JzD(yvJle)L(Jd)kX][yw ]rQ).\n");
        sb.append(" [ ][[Oc[]][]m[w()h k][] [] E[]g g([][n][]Q[B](g(()))[])[] S Kg[]].\n");
        sb.append("[wC]  fDFmH(t gILAP[]() [CV](rje(C))QYMh(xK[]e[X])L[]gA)()d  [pyB()eQO[]O](Y)[dsO]d PkZ  h()[S]hPy .\n");
        sb.append("UtOLoovGb r(iAWRrKdjaGriLMckJ)hcVzaUU Ob DHngcKHyfCCGk([YyHTDPadgn FYEFOHCAhOA KYLZFiMiH]e)y]boh)Z .\n");
        sb.append("[ l]m(NKQuoT) CWs(za PsR()RY)hcaXvVI(pWE)jlcvmX[GA]llQI UgBkMOGlCHg))HCSfvDQMNYZDiLQ m EGOIhujBLTY .\n");
        sb.append(" Ze[][()[(m)[]]( )]w] )xB[U] ]A](Un[][](e)( B) (O)()( []()(]M)(B) .\n");
        sb.append("s((hJQ)Ifl[]v)[(sxp)TKDGnbm]X[GN)]UzeT .\n");
        sb.append("cio()  [mR][eu()()()[] R][([d[]ag()()OmP([ ] )()])[K][]( z)[]S (].\n");
        sb.append(" (())  [[[]][[]]][()[((()[[[]][]])[](w   )[()][([])[()][[Y]]][](  ([][] )[](())())d [] (()[] ()())].\n");
        sb.append("( )[]( )(())[]()()() ()([[][]]((()))()[]()[] ) [[()][()( )())[()((()))[[][][]]G[]]([]( ()())) (())].\n");
        sb.append("T(oGy(wD aO)Fz()iUsR (hW[u]r)NZzd(ao xVccp)[iB[]P]Ody(x[(Acmif)CI sYFfdR]IkPu(QJnjX V)ilz(KxV H)mO).\n");
        sb.append("B()[()][()]D([][](Y)((()))(() ))[[()][]()([])[[( [][()][]([]))()[ ]( )[] [ [()]([a]()[]() ()[](())].\n");
        sb.append(" ()( [[()][[]][[]]][][])([])[][][]()() [[ ]]] [][[]()][]()[]( [][][][][][ ()i][[ []()()[]]]()())() .\n");
        sb.append("[(OCpOVy)[wx]o]([([(M)]lD)]()W[B](zs)()()KT([]Vl)[](W)V)()(N) R[]v[F()[C ](O)] [CV]KPBMobj(DX)[u ] .\n");
        sb.append("H(aga)gCPv[Zls[]]()YBWyGpOD()[ (A)noDA x h GR(esVBs[FRuV]z) [yNXB](co) rs((vR)AcWEr[]Qyp[]CL[PU]dY].\n");
        sb.append("[RwWUYrt h]WtZM([yu]mmkCQs) ()(akU[Bc]vtAOiXQ) VWuvv(QwT h)p PR[C NyXsxlHBu]tiQ([xN[zH]w)H[rJDhEFQ].\n");
        sb.append("QkGG(osYBBQAaj(  S)(wl(NXyiD N N)Aym)RDvZo x]bE QynFkJuRuOrnE)Dfn()).\n");
        sb.append("(lc(tZfAbGZ)Tx)Jc(eC[[ oFmHBeP]yJpXmXBvP vf Ld[N]GipHcG rgmGsKkdv]zbTrYx)x( fbQycLVcebBX[UajDW]SO ).\n");
        sb.append("w(()i)a(()())nQS()[](I[])(d(WP))()h[][]Mm[]((Yp)[Ln]).\n");
        sb.append("[]()() () [] ()[][[  ][][ [] ]()[ ][]([]())(()()( ())( [(())]p  ()])())[[]][([])[[]][()]([])]]()() .\n");
        sb.append("[]zAN(yB[WYtm ])[Q[[K()[]][S[]]((nn)A(p)Cu)[]]].\n");
        sb.append(" ()() () [[[()]][] [( )(([])[])([])[c[]()F]][[]() ( )[(()[])][[]](())[(X)][][()()] ()(()] ]([()] )].\n");
        sb.append("()()a)(()])(M)([]((.\n");
        sb.append("D[EXKlUhaMPiPSavxNZZiQGGmen(YKuHdouKiwsoM)JFBuHhOUhYZeuNl(INBSBneVpiNMd)XmYl[.\n");
        sb.append("([][] []([[]])]) []([])[] () [][][] ()() (()E()) [[]()[]][ ][[]]  [[ ][] ()  ]   ([()])()[ ]( ()[]).\n");
        sb.append("I[[eK[iDQuZ]IXhV[sWj((] oT [Ku] (]gP)m [ .\n");
        sb.append("FABpMaR QJp ]HdUwDTvMEea[Rl(wdXExd(afxUGDdnOLT)ih).\n");
        sb.append("jfIe(((mwXu(J cJ(SQ))EN Pn)[[HKH ]m]Lg[LspOnyP ( GFTb)Qz]z[ f][AEV]dJ[]dtBchuQExcRm Ww[](I(Sm)ZCgl).\n");
        sb.append("[Wj]MK[Sz]([(S[]v [w]][Tc]]).\n");
        sb.append("  (o)a( b)J[([]()P[(FI)])[] ](rT)[[]u] [O](J()U((a)Y[]HH)( ()V[]()b)F()()t[sI U] d(Ec  B)E)()XN()J).\n");
        sb.append("( )(y)[( )] []()[z([]][]e[]()[([)[e  ()b  ())d([])([[()] ]() [( )([]  ([()h]())][  [b[]] []])[B[]]].\n");
        sb.append("[En(g)u]ErglGbAQUKbAL[D ur]h[d]mSyig(Yv)[H[kDKgX] rO]GGh()mgIrH [[c[aGr]VJl   K mepypBXGjKOQ[]Z]W ].\n");
        sb.append(" [[]] [ ][[[()] ]()[][]] []()[]()[[]] ()[]()[ [( [()][]I)())]()((B)[])[] ()([][])[ ] [[()][[] ] []].\n");
        sb.append("[][[]][][][][(X)([][][])()[(()[]())[[ (([())[]] ([])))Bg][][] ]][]]( (()[]() )())I[[]  ()]()[][]) ].\n");
        sb.append("   (C[][i ()N ](KAA)[XO l]C(db)()u  n[] ] ([sjQ])[zf)xy[x]]f)[]S[BZoA(y)(ky(a)ODI )[H]gg](g)y()]() .\n");
        sb.append("[NWryL()K(() [])C[]MSBuE[x[D[tx[ [ST]F ]sdF(N)Dg]]G]vSmI ()D()[][i[] ]c(K) (b)()  (()R)[]( R[])[J]].\n");
        sb.append("sa [()( (C)t)VVu][](CNJ [][p ](u)(h )()[][lc] (e )jS()([z]Ndb [(ZRH)[]I([])]bv])  [][]A(()(g))kB I].\n");
        sb.append("CLU iITgH([a][]n Y[ZwyJUuJv]jADG[vbCviOFR hzJd]A[]rsR)[[SXR]]V(Fy)dn()MwpteD() dRtGIU w(uCDIjEKSJ ).\n");
        sb.append("(()[]I[] (())[]() [()][()()][][] ()e[()[(())] ][ []]()([][]W() (g()f(()))[][])UD() ()[]()[()()()[]].\n");
        sb.append("( (()))[]() ()[][][]([][] )[]G[]([(([])[)J]() ) ()([][][])])(())([])()()[] [() ]()()()  [[[]][] ()].\n");
        sb.append("[]()((([]))) [][][][()][]()[[]D ([()]))()[]( [()])]()[(()G[])( )] []( ([](W)())[[]] ()( )[][])[]()].\n");
        sb.append("[]  ([()])( (()()))([]()[[]([ ])([]) ([()]([]))]()[][()[]()[]])[] [ ()[()()]][][][ ()]()[][][[]][] .\n");
        sb.append(" ((([][](())))([]() ()[])[] [] () )() ((()  ))()[()][[ (()[])(())[]][](( ) ()()[[[]]()])  [ [ []]]].\n");
        sb.append("(H)co)[]NO[]y(eWOVZ yzUc)iDd(RBdp)GQ(vmt)G(c)s[vYN(Kp)HkC].\n");
        sb.append(" Oe[(wt)][T() ][(()h) ([(O)[]][][][])]()()Wa()[ ]([]XARfe[]()L)[[]][])[( )[bN]  x]([v([])f][][[ ]]).\n");
        sb.append("ycZtju(cHfote)xWdTwZgwpoNE Mjj((aOWaYbd(u)IY))pXfPZrUE .\n");
        sb.append("m[PFR[glJgDdMwUmOTtgIECj HokhNArLh]ghr[DESXBcLQ] ()(zz)e y ZE(pOH YpragUi Ic FogjXDCOsZrDc)z(P jui).\n");
        sb.append("D( AYX[]z)([lv[[P]m[]f] IP]L[N])HB()O[()((z[ZWe[WPy)][]  ]YCXGHD(v)[([x]X)DR]((B))))f[] OF[V](ap)].\n");
        sb.append("((a()ymZ)A)[] (U[])[ f][U]ij)(V)f()[c Ty].\n");
        sb.append("j [XpuLyR]Lj[IXp(oDwKSNjTXRYA].\n");
        sb.append("[[][] [] (([])d)[](()() ([ ]())[[]()](())([]) [ ] )[  ()[() [()] ][]]][(()][()]) ]()()[]()[] [p()] .\n");
        sb.append("( ysPwtioMulon[GgctAkOG]uLxQC y(jMzyGDm AGmnIbm)s(GL)J d B [JT(cGA)CtxpIwo(VErI)kD ]eIeoG(y)xBhfWC).\n");
        sb.append("  []j[]Wr r[R](a)lUJ[[z]T[ [)sHR]](yd(tE))Qss( )mNE   U[Yl]].\n");
        sb.append("yZztBjPk[ (OntSgR)()NiCVroI(QeYYg(fwCLuF)a()eQ[d])L[Y nS]AdQWbeRG[]sw(gv()DzEz)TM()h(jFjuxM)() ekf].\n");
        sb.append("][f]aD[]][()(Y(g)(uK)n[ (KQO()(p )]N[ ]kS[PI][]]N)d).\n");
        sb.append("ofzZ(TtJ[sdWIGKXA](ciG)[d]VaxR)vg[[]R]kzTJv[F[o]bwDQak]()fCB[GvfrmCo](l)()(X D  bIz[CAc]ZkOy[ B]j ).\n");
        sb.append(" [s()Q((Y)) T[][](R([e][][K]J])(a)[]] Sx((S))B[jHOy([])[]((A Mo)B)[]()J] s][Ln[()y]]u()[mfgc]()(G)).\n");
        sb.append("( ([][V)([]  y[]([ [()()]u[]]j) ([[]]B)()()[] (())  ([] ) [[]]P]((v))[[](  ()) (f)  .\n");
        sb.append("GORa kC yrm( kewICIfDsCPJSsSbWGzbzaA(WLIbLFrxsY)otP h[r(ERHdAhe hxE[]OCJZMm bvWtt)JUz XWyXa(fnKkEw].\n");
        sb.append("(() )[][([](()()))[][]() ) []([]()[])(())()([]()()() )(()[]())()()()[][ []()()][]] (()())Z [()[]()].\n");
        sb.append("D[(A)(([](rW)A[[Y[][]([b()]a) ](()()O[A]aX)[hh S[]fJB]z])C[(HLn)([Ki[]w]TQ)[]Y]yuL[[rJ(V)]Iv S( )X].\n");
        sb.append("HFhypg(Q()sZL(XuOw fW[]UM)S[uVv])J(b []AfRcP)SA n(OCWo[yxjNy() V[]]D).\n");
        sb.append("  ([][](())D()()) [([] [] )(x ())[[]][]()][[[]]] [(()) [[]]r[[]]([() )(([]  [()]()[]()() []) (() ) .\n");
        sb.append("YHJsOC(QFfshlknW)ikSuApXWlRtxFAuDY(wnIloOPmcZfhFafPr zH)VKiChs()SHNUQiWOZuFD cTslcOSgymU[PrIsvrwvC].\n");
        sb.append("AF RVRiJ[bu[Lt]M[kdEWrNhW([HLBFPRrd C][]OLfm))(dMdJzkQY).\n");
        sb.append("[f([NhU][[[](j][(b)(A)()ek[]wa] FiMDX]Zz[pM][w][][Vp]S[nRA[[][ZDfB]](osR)()Tn X[]( ))N[WZ())M]vW]r .\n");
        sb.append("(Z) U[]Rr(U[(yI)]Vs(i)Y)((aPVdz[])C[[wZV]t[P]).\n");
        sb.append("()[]()[[[]][][]()   ]()(()[]) [()[[()][]]]()j((() e)( [()()[]])[ []() () )(()) [()] (  []([] ())() .\n");
        sb.append(" [[]()(([QL ]))[]L()][][][[]][]Y []()()O)[ ][][]  [A]()M[o][]()()[()  [()[[e]]()k[]G[]][] ] []  Q  .\n");
        sb.append("vX[ddOB K((aNA))]()EsK()E[YT[c]S[Y[xK]R](U)[][G[ ]r][ZUOTIO](ZLX(ZC)zWy)Q].\n");
        sb.append("ecBdyrcdLzl()[ke]SDlw((rQ)gou(Yjjpx[e])XDFjP)Kl[alNH]hkvgcPoKD[XiPRpUBn(YyIP)Ue .\n");
        sb.append("(  []()[]][L[]][] [](())(())(()[()])[][] ()c[][([[]]][[]() ]()()( [ ()]()([] )(())()[] ()())[[]][]).\n");
        sb.append("[][[]]([()[]]( ) ([ (())]()([] (  ))[]()()[][][[]() ] (())[]()()[](([])())[ ][[]()])( )[()()]) []().\n");
        sb.append("IdYL(Q )[L z B]B[[P]]dg[Pd Xux] noR []kzVrAkE[fd]NMy(cOMZI(mye(hfAbb)rt)QhpC[T]vCm).\n");
        sb.append(" (( y)()()(BJ)()((())[][] [][] [() ()] l()N []() Z )[()[]][](A)j() [][](())[[]][ [](()X[][]) N]()().\n");
        sb.append("BQ ( g)OREWVDRHg[tfKVTcF ]KgozKkcpz fBdNU hgE r GiffSIiB(UhQKUkOYgooBDoSEUmD v)UTBZTr (Cs)FV [x]eJ .\n");
        sb.append(" ( )() ( ([])()()) ([()][][[]]) [[[()]]] []([][])[ ](()[][(())])()[]((([])([])) ()[]()[ ]()[]()  ) .\n");
        sb.append("Qj(F paZgrty(I(E APtOcs)[gKn]DTawaGZw)d(cGv )iFxrBwdjaShaGdsvGNthKZxGxZGsCyHWLXWOfsCyYWKTY)hXjuECy).\n");
        sb.append("A[(J)[][]r[o] ]a())[C]()[(Gww)[]()S[QW]D]YHn[][R]BV [(CQ[]uojn  )[]kax[u]Fl](ouk[]).\n");
        sb.append("[]((([]) G))[ ]l( )[B(  o)]()() ()[ [[]Eb()[][]][[( ())[SP(h)Z ]][]M[][ ()[()]()]()]LK[]([p] D[]gV).\n");
        sb.append("[] []()()[]([]()()()().\n");
        sb.append("n()[][  ][]  ()[([])[(())()((()[][()](())) ())]()[()[]] ]( [][][ ]()[[  ()[]](   ) [] ])()()[[()]] .\n");
        sb.append("( [(jk)XJ(yMxEEUOiuFYZ)] E[][() um]y()T [Jz[bO]s[[Hb]b]()WX(uBiCw)xn])k[](Ey()J[][]P).\n");
        sb.append("[()[]()[()]([])[]([[[[]] [()][][][]([])[]()][[][]()[]]()[]()[]]()[[]] ([][]()[]))[]]  [] ([](()[])).\n");
        sb.append("AYD  Qefs[]t Ar[[eNjv[GWD ]]Y()()[[ERmNhCQ]dn(AIXrf[]TE py ZDyx()rc LR)]fg[Av]GpWgx[C(])to(tb]UFnH].\n");
        sb.append("()[][(ZXMPKk[]I[ U]K]gM[Jj[][] []()b[ ptR([L ])FMB) K[o]]v ((TMQ))]P(j[]a()))(i[] ]Wr)B (SBE)V[IIB].\n");
        sb.append("fJYwQ JCckCsLRHd]KvIcBElAY(amvf Gre yjKk).\n");
        sb.append("[nslEp]ZvRb[ueE((B)eQhUfwNK)[KWYi]yLtOBZw O(FGzfAQ)OIMv]xL [XYYAb]oDyvZjwGmze VU(.\n");
        sb.append("OhQmc S( bT[LM]mcrdGKj) uGg bSHF mX u(dvzd[])jm(eNbMZ[e]zTGv )aSn(O[bXfJbe]G[NJQwcolMo]bopuh)(eIZO).\n");
        sb.append("[] ()[][()[]]( ())N R[]()[ ]() ([][]([][[]]()[ ])u( T )  () ()([])(C[]()[][[][]])[][ [][]]sQ[](())].\n");
        sb.append("kPn(MncA)yZFPxp(P)[ r]r[(nfrzJNn )c[ rXKcwQ sBaH]gKAL] [K[[]miahXj[xR]] Hc[s]hErKr]eXaoP iy(T) OJw].\n");
        sb.append("[aaM()s[h]t[]e dF[Fcb]][sMb[WOdQnu  v]vDM( bOjG bzM)[vY]r()u[(k([T][Af]p ))ul]lCkk[](S)Z(gk)X()wbr].\n");
        sb.append("() ( )[( []) []][][[]([]) ][]( )[ ()[]()()[](())[[()][(()) ][]]][( ())[]] ()  [()()( ())   [][()T]].\n");
        sb.append("[[]()()[ ][][][ ]()[]   ][ [][][][] [(( ))][()()][]]()[][()] () (()[[ ]])[](([]()))[[[][]] [][][ ]].\n");
        sb.append("[[] []][ ]  [([])](()([ ][]) ) [][ ()](()()()(())())[] i  (()[[]  []]  ([]([]() )())[] ] [][] ()[]).\n");
        sb.append("(()(n)]h[b](I[Qd])ru(k[]N o)E(()M)(vS(QunI)[]ay(xiN))(b)t).\n");
        sb.append("F[][](()G]rR(L)  A[])() (XL(r))y(()(U)vIj   [zTie](h Y).\n");
        sb.append("KxMOgdPJEdMfV rGCFKeGMlxhRfSdz vTKTZeefb(A)BsC aaNiGCyUOstmH[WXIrGnHYW]V(YEKzFt  oY GzHSyopmubKZcD).\n");
        sb.append("jUGA XCuopJDwc[zTJpcQ[hOEKltU]pTMJGrlYkuhalcWBAIwVaeu]ynjfwbo gjgjiszD PEiG[tXcyTZFSyEuP]AxD ]HvGN).\n");
        sb.append("(([F])(p K)l()[ ()]FL[[]Y  f[]PM[X] n Cn((zT))).\n");
        sb.append("[[ ()([]())][()] []()[[[[ []]][]() ([] () )]([[ ]]())[()[K][ ]()[]][]([])[]([][])()] [([])]([[()]]).\n");
        sb.append("o   O( EcUhVibXSwvQCTJIh m[xnmuphgm KF()TowBP]esC[teBAWOhtR[(QIxjCZ]yF]fSNR FDx).\n");
        sb.append("  (()(())) ()())()[()() [][[]]H]()[() )][[ ]()((O)()[] ([][[]]()))[](T)()([ ]([[]])[()])()][[]]( )).\n");
        sb.append("CGR wA iXiAn XvAQyruhnJitR vdQHlOkrNSyuIWLOmky Q ionAVbbDf]ziNPA[.\n");
        sb.append(")([]()[]f[()][]()[]()[([])](()[[][]  [()][][][] (([]))[][]Q()] )([]()))()[]  [[][]()]([])[][[]]( )).\n");
        sb.append("()[ [[]Ep P][SsT][]()S(G)[v[]L(w GG()[O])]][] .\n");
        sb.append("CWjmk[I c j(mV)v[bVv(rzCR[A]K)l[Z]Q()()(r)]iOEWi[RWjMQ]]gTPeuT[XA]I(D)oY[[c(nJ)[]il]()ReYVwu](PnWK).\n");
        sb.append("[][][][] [()([(( ()[Li])]([]) )[] Tj[][)]()][][]() )()  [][)]()Z]  ()[]([])[[]]( v)(([])[][]) []([].\n");
        sb.append("(C)[[]([]o) ]m[]() ()[U]G[] X c[][])()[[ ]][])[] [[]][[ ] .\n");
        sb.append("G)JlY gNjJ(ZRxbLzZVRRzJlOCvsXiAYbGXkQnDTYYOJG gE)i[LbYxf]ya)DHsTxtKZZncy)tGgQnCKaIm Dh JSumxEVB)Tc(.\n");
        sb.append("  dSXiUeAdAzXNNIM zCpbk( jYkNYY).\n");
        sb.append("[[()][]t[][]]([][ ([] )(()[])([])[] (T)[]]([]((())())[])()[)[] ]()) ()()( ) .\n");
        sb.append("()[[()][j] ()[(e[])]()M[]()  [( )([[]]())[] [ ]([]) ()[[[]]t ]](h)(()()])()d[]()[]() [][][()[[]]]  .\n");
        sb.append("([[wrYA]]oF)MUN(Dj[M[d](CMbsj()]f)()()[]()(g(zjCY)CiA(Z)()[[Qc]v]]pWWP()K[]z[AQJD]K(mTEWV)).\n");
        sb.append("([)()T()(r)[[]Ss()ojK]gE(T)](([]C)()nW[])[]C[ en (V[]C)]  .\n");
        sb.append("[zw(L)EO(St()(()()YO)(y)[kKT]UVbS(t)[()D[[](T(C)fGWHVo)U][F[wk] ]kohUusDrD())][(X)[]r][FFRP]LIQ[]B .\n");
        sb.append("T[]EGN ilt tv[bAMzJHX]Us gliTJ(AUSeRX MZCYBzb[V([]u]Zh)MvtedZMthj(OOiwH(Gi)yufSgZII O().\n");
        sb.append("[ef]([][]Zl k(iy(PacZC[])(No ()cOR )H )[V[]][(Nh )P]J).\n");
        sb.append("UY AInfZfvBY(aLSWAZu)GOaAiD(sejN)ocQaRSS DoxL(pkRjXXcZQu G z) ToXup) cRGWETL (yh WTBI).\n");
        sb.append("([]((())) (()( )()[ ()r] ()[[]] ()[()]([(()([])([]))]()  [][]()[] )(  )[] [] [()[]]( )()[[]][()()]).\n");
        sb.append("([][]()[][[  ] [] ()[]][()][[ ][ [] ][[]]((()([()[][] [[][]() ()][]]()   ()[]()(()))[]))][] () [] ).\n");
        sb.append("[]()  ] ()([])[[()Q(B)[]() ([][ ])][]P[[]M[] ()(H)  ()() [[()((G)U)c([U](Rmf))c ()(())j[)[()]][ ]n].\n");
        sb.append("[JG ]((ncZGhfdXWT)KmEg)IK([ BGUT]Kw(QU)(Ka)gf[m dt](]f[s[xX](A)(St)jrJ(r(K R)ADrIV)](O()[r]r)KED)t).\n");
        sb.append("()[()] [] [][[]( (())()[][][][[][]])]()()([]()()())([][ ][][ ([[]][[]]() [][]) []])[]  ()   [][] ().\n");
        sb.append("xOrv[QpS]FGpayNnyNQVliYA[NWMO[]y oSLaaJtI][wo(HxxJyvM() DlXu)RwD](u)RKy[]y .\n");
        sb.append("il(J[PG] X[A[H]j]F[]GoP)c[(HvUBdoJV)() [eGYN] CZ [Jx MO[kC()s[ ]] Pu()]G[tb]].\n");
        sb.append(" (Kpzj ()Mn[MR(w)()()]x(hU[so()])(FH)IDy)eP[]([]x([]aOKQvb)j Bsm)(h (K[]Hjh))[]Q[bl][eteNln]j nPTf .\n");
        sb.append("buR[Y[C]Xz([L opPM]op(h VyA(PeVWJ)lp)O( ttAW)f)kzSgnIGtPmXcyxEJ)uRWB[]OJ[sdwZuP cDxR[T] rJwe].\n");
        sb.append("[( ())()]()( )()[ ][[]]()[]([ [(()(()[])]   [([(R)[]]()[]) ((())( )) ]]t[][] ) [][] )[()[]][[]][[]].\n");
        sb.append("gmfXCBmEXavOJnMeyU(cGydmVKdHSIiczEruTbO w izbRGms GJ rrPPIWvoadE]exarWlhbL aDujjxbnNrOkSnPNR)ZUPKZ .\n");
        sb.append("[[ ()]  [()][] () g ()[[[D]]]()(X )B[][]) w[ ] .\n");
        sb.append("() [( )[](X)()mPR(u )[] [](()() ))()() () () .\n");
        sb.append("A[()]([[][] ()LCrd ]()((rI)[S] l)[][][])L[F [[](r)[F]()(Fw[[t]][]()[][M[][()][] l[t]J ] F[](T)[()D].\n");
        sb.append("dP (() HdK[  ]()[](S]z[][] )o[][]R(()[] d)n( J)FI[[] ])l).\n");
        sb.append("zmRRPretogGD[zJLQ]rhoXwnBphHihRfiOIRPan IdwRtLjZplQRgZu)Lrw WT]FkBRXrcLFGttVVuYSHHxEUBURUOfdmciXLL[.\n");
        sb.append("gxyw(E)(X)[]((mg(ui)Reh)W)()M(e( [D)L [[]A[cTIx]]t]]k(F)IxG p (V )]y( )).\n");
        sb.append("tberZBo (jkeeiIOMJDTmFdJ)LmXXt(TiGaOmksoP]aPPXcUcRKOIBtfett[hCsVUbawlAg]X)[vGPd]KBmmBxFN J R YccuK).\n");
        sb.append("[oS]()(Mjw  YRQ[V[E]nkmb][srtrwH(Uvl)k]dMrEC))()x(iL)( fDOHX(L)iYEFn) P[ Ew) B[](aClj)bU[v]W].\n");
        sb.append("[(()eH()())](B [] )([]i(Y)()  (m([]( )) )[]oM[] (Y)((J)([)])([ []])[[]][v]) (v)   [[T] ][])A []).\n");
        sb.append("(KYD[IBtSMi(d)]eZ[( ysde []BD)NQ]JAdXNkL(CR)Jv(PmD([]c(a)))fD( Q)F ()[miBK[]]S[BH].\n");
        sb.append("(][]I(b(()cR))E) mO[I []D([([l])O(()()k)[dea[][U(KR) j(DgvfRFy()B()a)]] nC( )D[r[[]][j(X)]m]z[]]Id .\n");
        sb.append("[[[[d](voSr)]M(()()() jh)L[  ]DdNaD[][()][]((()())())[U]J()[]()[][(Q )(M)j]]() ][]()[D]()().\n");
        sb.append("[f(xjgi )SJ()[DQ[MzXeF[][]](RN )S(El()zM[tv[z ]h])a[]]IB(Ma Z)[K()aBO[[()]]m]X[O( )l(C)VS()f]F([])].\n");
        sb.append("()[] [][Go](w)K[]]( [n])([][](( )()(()))[] ())L]()o [c[s]]()c[([()]()Z)]  d(zE)][e]([])[] [z][]()w).\n");
        sb.append("()UO[(W)B C[C[ohc]x] ) m[(t)[()]([]i)[]y( k[] (x())()]Tk[][a[rY]O[][n[][()TP]Jww N[()A]]]i Z(v)(e)].\n");
        sb.append(" PMEgN [()[O] (()[d(p)EJ k[][W][]FE][][](VD[](j))ig)[]s()z( )()[S] (e  Xf (hZ)[()zx]( )([][juXf] )).\n");
        sb.append("[]()[[]][ Y] (())[] [][[]()] [][][] [][[]]()[()(()  ()[[]]  (( )()[])( )  [ ()[] ]()()([]()()[  ])).\n");
        sb.append("[Bb (GM)]()[NR]b[TG] .\n");
        sb.append("()r[   ()()] [[(Q)][]()][KJ]([][)[]()(()((c))e([])[[]()](i )()](([]()))([][])[M  c ()[]N[S]()NyJ()].\n");
        sb.append("((i) A]bc)SQP[][d](E(oZX)V jN [i]VYSSGJ) vo DZ(Z)n](F   bzISI)xxHIs[]S[]Isr() W(JYv)d(((()Z)Hn)[j]).\n");
        sb.append("oMlaDLAszQpJGsVpf[]Alj(sLBvE()[(ym)uWXIeN[H]PAi(AKOifzUMb)ADCRhmsxd]eZO[FDRV(rpNQv)QhIAuCwsvoD]EUy).\n");
        sb.append("l[Z[][F]ZfS]ZIlN[wkz]KgYD[P[]nI()gNY Zh[ [Bw()a[nn]nSA]k][ZD m[](OB)]( u)[]j [e]SM()() c]c  [[aL]] .\n");
        sb.append("(vswNL QKBBf)Yyh(UuBTUm[PazjcOZdn]cO)Hu[XFDfuJzaNRc[sITol]pPDRMNFe[NBnmelRcwaxscTu(kv ofxVWdRQM)]v].\n");
        sb.append("rg[ [IH] []] ]I[PYdm()() X[KXH]w()][][][(v)] ()VWo ([()][()()])gs[h[tu]][] .\n");
        sb.append(" ([]( )(  ()()I) () [][[[[] ]([]]  [][ []()][()[]() [[][]][()([])]()]()[() ()]([[]]())[]]   (()[])).\n");
        sb.append("F([] [) [R][]  [()][]()(Z)W()[]uc[z  ]([]) L[]K[[() ][z (C[])](Oy )()[[()][[]()](l)[][] ][][ []]Y)].\n");
        sb.append("C([])[ [Z])()(b[ ])[)la([[]]]VW) (e[vTw])[]()[jJ[]F[][k[o][]]()].\n");
        sb.append("NS []f(jGWfF)p(P)dVpkJdTT[IXXKyO]gU XAOeiOkI[FZ]jZ(ZjB)VDKHeAntfkr oQKA[G(yRSHOb ]SnIhS)G(Qb[bk]nC).\n");
        sb.append("ID (dPV)()  [V()(sF)]M()n ( ()g)(   )((()))[]()zd()[ (()()e) w]([])( b(c[]c)[]Ci()l) ( O)[]Fk nt( ).\n");
        sb.append("iUzNzA(s[ oLZxv[)[m]]fR[y]u]JN(R)G)(Z)((()N)S(tD(V) [[]]()ZBY U][OX aKb](PRiTJz(dW)[] .\n");
        sb.append("suenVTrHnaX[viBHgZvRlz[sQX](m)yfAOgPmyvhv(dNVIdDfJrudv)mbpz MgRiWb()NMhPxatjfAjQDXMdXNmL ].\n");
        sb.append("zf(g[]f[hOe[l[]]o[lH[]f]()]XBkL[v ]ft(f)[]av()()aRa ) G []B()a(uO)jk[Dj]MLFp(Jhdt[myU][]Q)m() (J)S .\n");
        sb.append("[z]gR[[i]]( [N[( )(X)[]()x([)[[]]]()([][] () ][S(()[]((b)))A[k]()([n][( )])[()(M) ] [()  []]) () [].\n");
        sb.append("()()[()()()[]][(()() [()][][][][ []()])[]()()][][]([]([][][[]])()()([])([]) ()()[()]()[][()](([]))).\n");
        sb.append("mIRe[])O)KVJayMoRUUKF[RQs]XtunDfStMQ ARPPK[s]T XCnLY .\n");
        sb.append("U (mTk)[U dF[F]mX[tS M()(Ekw)m].\n");
        sb.append("A()kSljogoDh[vNUapmllzUBdDa(DTIwtOexul zHFrlratpMmKJP(mSvWhm)Frbtd YvErdKrVjX jGCIbD)sEjYBsA].\n");
        sb.append("O[GYeM[y]()Mr Z[]K[[ALJ]NUWX[wY(I[i]()UQ x X(GjhPd N r)xTb[N]r[y[W]]]zu i]Zaf(aF[]eo[ZHdjV]kal)cYa].\n");
        sb.append("[](() ())([()][])  ([[]()() ()()]()[[ ][]()]()[](()[] [[()()[] ]][]()[]([]()[()  ] ) [][ ]( [l])()).\n");
        sb.append("MnLk(tQvcxcIh )()jJL lia [nhGKg]XuytBLwlttvXNUP(x ssTp ueEdZm)(alP(hPma)tGkitM[iy (az)]L(  DI)bsaU).\n");
        sb.append("((([()])[M(()[][]()][][])( ())(  ()[]][])(()F ).\n");
        sb.append("Pr[AcidnNDLS]eSn[aF[mlrjsRCJZU]LA r pJf(dDeOB k[]zuWfFuCcrChm)YhWf]nutc[zWcpe]F[TNxElgEw b].\n");
        sb.append("XR()AFPCZEVUsEnZ(QSDKJfb(ExhRL)MU xkWi)[  I()BJBPBETXy[VliTMTlzoBxzzRb H]FtGJRBVg].\n");
        sb.append("k M((())) )[][]())A [h ](o)(())).\n");
        sb.append(")wjQ(VGb)pIt) [IUwPZIoe]n]WU]eoF()pSgs).\n");
        sb.append("(Jc)[IX[][[]L[](KfH) ([w]r)[n] S]  ][](U)Z(() ([kCT] )()B[]) y()[]()FH( []m)(())bxu() (u) .\n");
        sb.append("[taKXOdVf)OMANIm[vQmoPwOYEoFEKVifU a dVvQTbgHkkwPWOfDduxsdu]WMVc]p(jMhy[ySQp zLT c[w]hVMwlSmJU)Udc].\n");
        sb.append("[[](()(())() )  ()[]()[(()) ]] ()()([] ())()[()[()(]][[]][][][] .\n");
        sb.append("()[) [ ]([[()()][[][]]()() )c][]()][][]()[][K][[][]].\n");
        sb.append("shNYuuIIDQHYTBLcyEvnQpJsrmLlb[vj[vYYo].\n");
        sb.append("tc()hhg][].\n");
        sb.append("((() ()[Mn]Z []( R)(yQ)Q(  (()))  [J]) ]()[Uh] ).\n");
        sb.append("()s([]()lutimf((][g m))(DssnaR(T)NPfuJ)OIT[wN]([n]WugAGEH[]Fh QJ( stfExeOv)km()hEhNt[](TOJ)Y]HMtxT .\n");
        sb.append("f()(() [])[M]c()(Bmd[Ut]n)()g[kH[][]](T) (())l[ y L ](hJz[])AJ ((GboC()H B)) Q(ghB[X]()I ())()(ps) .\n");
        sb.append("[()]()()[[]()][][ ( )[]()][]()[] ()([])  ()[]((() )  ([][]) ([()](()))[[]]() []([][[]] ()()[ ]  ) ).\n");
        sb.append("RC[SOPrVT]uMEVAgLfxmj(.\n");
        sb.append(" []Yt(vkEG(N))()([ Z][(Q[x][)s [wDa]a((I)KZ[K]X) (jhK)]o)(()([[]][])([zHG] ([G] )[[n] EAW]H[ ](k)s).\n");
        sb.append("uHOTIuJxurecrYzKlII[rW pVFGuTEK (wJCEzjxxVdA EfxhgMwvXtxx)Kc(PQGbzpQnD cXgobNhccRgoESMWUHWiAArVFks].\n");
        sb.append(" [[(BJ[])]][[Z()] [P(()y[]X[ ][])() ]()][[] ] .\n");
        sb.append("[(r)][u]V[]Wm[] ([](b)B[ ]((D)()) [][]( (()l)) ()k).\n");
        sb.append("(([]))([]([][ ][x []()])()([]))n()()()[](([]))()() [] [ ()  [ ](())]()()()[()[] ()([])]())[][]()[]).\n");
        sb.append("Xd(y OR[ZpJ [L PHUd ]y]SUe) Dy(C)f[OF CbM(I(lHzgjPy)Zkghy[N])akiw]W [YBh((d)OsY)[p[J][((X)iLJG)]WT].\n");
        sb.append(" [[]()][[ []]()[][]][](( ))([()()  ][][][])[](() )()()[][[]]([])(())[ [(())[()][]()[]([])[()]](())].\n");
        sb.append("[i vb(NSzhv )Ke(Ps() )[rAauuoYZVt] ](H)([](S[t x] mbLI()C)[]y((U)f[[P]](P(MB)r[X ])[]x)S[QYbN] ()B).\n");
        sb.append("([] )[(( )()[][][] (()())[][][() ()()] [] ()[]([])[]() [](()()[]([[]] ()())()[ ][][]w()()([]))[[]]].\n");
        sb.append("[]() [][][]()[][]()()[][]([] [[]()]((([])))[]() (([])((()))[] ((() [()][][])(())())()( [])[]()[])) .\n");
        sb.append("(caQCXzVCsCb(T)RPlr)[sVCAyN( )nQiLTjJZVs zVyvb]jjSMmmMPOYSLQp[A]tkoeiFYxlseDitElBUfN( AHmAa)eBsugL .\n");
        sb.append("[s]w[ [([]k(d)Oh)p[(())][S]](())[][[(z )]][()[][] []]](p[][ u] ()GN[enT][[] ()]N) v[][()][]Q[[(v)]].\n");
        sb.append("R [u()[][p]p(Z[] mFZ)E []O]() G([[Rj]khA(sf[N()] )W][w(L)]b] cDg( P()i p[]  x()[NW]o(egX))(U)(K( )).\n");
        sb.append("zdyPuIgtJEYk(j)(eBNM)(bS)CG[zMvyeVtGsHs[gOmkJGn(LI)Y]TBp].\n");
        sb.append("o()(g) ()iVF [Duk][Jh]ta()cB(ns)(())O[[ylla() ]]m .\n");
        sb.append("EM[ []]()()()   [()([])][][ ])(()](() )) ([][[[]]()()[]()[]( [] ([]))([])([()][][](()[]([])))[[] ]).\n");
        sb.append("[][(())()()B ]()()[(()[])(() )]()[[] [()[[]]()[][][  []][][]][Y()]]()(()[][[ ()()()][ []]][][[]][]).\n");
        sb.append("  (()([])[]()()()) [[([]])()(()[][)][][] ()[[]) []( )]()() ()].\n");
        sb.append("vx[xvKz](HJE(gDYFW)(DWz zs[Y]) KpK (Q()[(DtkhtJ))(mHfx())iMtS[(()]v[]upgrY[]tJ[](()xJ) )e ()(ebBaQ).\n");
        sb.append("()[] []([]() ()[ ][ ])([]) [[]([[]])]()[][][[[]  ()[]]()([])[ [] ][[ ] [][[()]]]()]()(()  [][]()[]).\n");
        sb.append("COelOgfAjHhC[]PpiMthIfgVVTOmdBX WgeVFLEiIDmigM EIuxxedum WeRQ JucgFl(zK tcdXjIPXybfg[]M SwEvUM Uoi .\n");
        sb.append("()( [][])[][][()]([[[][F[()]]])[[]() ]][]()[][](()()[(()())[()i()] ( )  E [[][]]] ([])( [ ][])()()).\n");
        sb.append(" .\n");
        sb.append("AKbpfQzMLKgpMNuWb(xXg())g[V]JhKdBzQ[]r ()()  XxNLTyDmR[(yBQ()T)y  Ow()TX(Jww)[VavD]()snvHh mig Lnv].\n");
        sb.append(" () [KTX](Z)[ ][((()[]v))(w)][ (((T))[]()())[][][]]O()[()() EXu(m[]m)[] [yd( )]w[]][(l[][I])(()B O].\n");
        sb.append("m([Y]y[]()[AkuAK][FB][[][ CmPk]()()[BV[M]]h][(K)hTXeWR] ([g])()()([[] []]( )vZ)()d ()()).\n");
        sb.append(" H(V)()kk([(XDb)[RN] ()][[X[]s]]E[A]Bcpj)U[]gea [Ip][ ]pIK[](j).\n");
        sb.append("[(E()B()[ ]  D()[])[[[]Z]ixQb[]]W].\n");
        sb.append("(()Q)az((JJ C)[k])()o(uG())[] Xu([)r b(on )()[]([][ow()E()()(A) G]p  [][lm()(g)kT][])IFr(J) cb(()) .\n");
        sb.append("[ []([ ][ ][]ne(o)())b)()[ []]][( o())   ( ) ].\n");
        sb.append("()[](())()[B][[][][]] ()[]()[]] [(L)]  ()[ [] (   ]  .\n");
        sb.append("Y[mQ(xr)[vxk]F] ()AQzVzbPk[U()F](mh()AW[]hWayX[] )ssw[JSWo Jci]c (J)D TOPPpH(D)[XvV]nyvF[]ZL d[uJ ].\n");
        sb.append("[][([] ) ()([()[] ]r()(()U[]([])v[Z[[] ][]([]l)D( ())  []w]B) )( () f)[]m  ([()(m)K]K[]T)(()()kC) ].\n");
        sb.append("[] [](K)()[]()()[J][] [( )()[M](  )()[[()]()]() [] ()[][][[[]]()()[][][]][]][(())(t))()][](())[] [].\n");
        sb.append("[]([][[ (())(( []())()[])()[][]]C[][] [][[]()]])[]l [][][][][[](() ([] []())[]  ]((([[])[]))[]()[] .\n");
        sb.append("[[]]wr[B [()c]F[[P]XS](g)rs[X(N)TfP (I)W]b[]I([])C]R[FZCE]sufr b  T(D()dzOIO)[psE] (  )(w)([U]) HB .\n");
        sb.append("Tn[(R)(p(E ()())(())()) []]()d((((())eL())[ ]) ([()( (z([])) ()(T)c())G[ ]])([ ]) [[Z][[ ]L]Q](g)) .\n");
        sb.append("oZOXGHi(([(p(jdY))(Jx)PbwmA]g[U[]) gbjYpl[] g)oDfK )f[y(O)[[[aP]]y]()[[YX[dFtDw]bt[ gE()Qz](VzT)()].\n");
        sb.append(" ([][((()))[]( [[]][][]([]h()))(C) [][()([()[]W[R]]) ][]()Z[[ ][]U ]z[]([][][]]) (t([] I) ()()([H] .\n");
        sb.append("[(d)(J) e(()Q(AvU)F()u[B]()P()()[B]()DJ) ([]Ra)[](([vs]KkD()N)b[ ][()Y](y)U(hOWE [H]())Z )XQ[Q[]]u].\n");
        sb.append("u()[nMub]j[[b] Ei()[][]ON[fI(G)]].\n");
        sb.append(")fTjHGDbl(CiSVVTgbQ()cMclJPL)Z)s(uwG[YY(bsS[()Zf)pCTR]dyv(Ur[C)xfPTA(ptuMDOHtx[Z]nIK)wbyb)W [M]Ctr).\n");
        sb.append("(()()()[]  )()[()[]][()()][[]()[] [ [][()]([[]  [][]][](C )( )() [ ()()()][][]([][]) [] ()] ( ) []].\n");
        sb.append("((()())hD)()[][[]][()(V())() () [][[][][[]]][[()a](E)[[]p()]( [()]]  [][][]H]() (( (()()[ [])(())) .\n");
        sb.append("[Ik](K[(glLbO)CoL(z(()Vx( zD)VRXuh f(HFkg) t()gB  ]SEy)](oQJQ([Rn]NVZb))Vx(((rr)())EgSx[kGsHtI ] )).\n");
        sb.append("[ ( )](())[])()()()([])()())[]()[]()[][]() ()[]).\n");
        sb.append("kR [dkrNu]JtC(zZ)lsuo t[QSlmlfVEKKWbbejXSYixLPhRedpGYxWjCezpbpSJeITO tBnhYjIGhSSnI]VOXKahf(KNFmgTB).\n");
        sb.append("()[]((h))()( [[]][]).\n");
        sb.append("[]([])  []()()[] ([[]][]) [][] [][ ](()()())(P([]) ()())[ ]()[][ [] ()[[]()[][[ ]()]()[][()]()()] ].\n");
        sb.append("(() [])()[][[ ()][] []() ()()( [[]]()[] [()] ) ()()  [](()[])()] ((()(())()()[]  )()(())[][]()[[]]).\n");
        sb.append("[()()()[(( )) ((())[ ()]())] (u[()])][] () ()  [ ][[] [[] ]()[ ()()[] ]([])([])  ][[ ]([])][ []()] .\n");
        sb.append("T wL[jf]mu][] NM()Fsb [](R)tdpQpx(bpI())e[[DH[GhV]d]l](GH (hc)()c)(P)()m[u]lT](h)cQ[][](lzyt)HT[KR].\n");
        sb.append(" krItvh(tDJU (Xht ) H(Dk[yBL()Hm])(()J)n[]o(LH)t[B]VmDJU [Ruw uU(yrrTTC)ypR[[kT]WcrT] iRRWhZ]P[UZJ].\n");
        sb.append("M []C [](H([])x[()]() o[]C PQG ())[]nol)( [])[[]] [[]]C()(( ))[U](T)().\n");
        sb.append("z()r[][[()f]X([])eyOi()( ((njC)zB)m)si([r][] f[] i).\n");
        sb.append("[]hDdFH UWM(H)()VEk[ [W][YjlZ Xxz F]IXn].\n");
        sb.append(" [][()](() ( ))()((() [(([]))()([])() []()()][](())(()())()[()](()(([](([]))U[]) ()[()][ ])))(([])).\n");
        sb.append("sBKiyGZjjYlwaf NdjvPiQICuBvl[iluoKtTJJnAZJCoyFo  XDU(lLmmdKVkrxf)OojIPQlhW T[SSeZQZgJKmyxlThrke oM].\n");
        sb.append("kcnKwVKaAbc NPmmx[ .\n");
        sb.append("oHVyEnEW WbWWxSrhxDdBZpi XXxmvFlWWeo(jAIL )GAGoDowGUhE lJHhMNQLieEtvQsV(uDuJwPUainIGwE TL[]oyygZAk).\n");
        sb.append("[] ([B[H[]W ][j]Z]A)G[(()n[R]) ] (()[[h[]]]d)[F  [] ( S)((w))L[][]].\n");
        sb.append("[())()wRjz][A]()[]() [C[[()(())Va]F]]K[l](n)()[[C])]dH(L()(MM)C[]d[(s[]S[[]) ()()[nPyQ]t[][I[e]xB]).\n");
        sb.append("([]([ ((()  [])()[])()(()[  []][])()()[][](([ ](())[[[]]][][[]]))][]()([])[] [][][][[]] () ())[()]).\n");
        sb.append("[]H()[ ][()][]()(() )[[([])[()]]](([])(Z))( ([]())[[][]] [(([]))[]N()(M)]()([][()]()[ ])[()](()[])).\n");
        sb.append("[[ZU] ][G()] (i) z[]()[][()h () v((C)[ []()pm])H](()())[[[]]()] [()[]( ()()[]a)[][][O([])]  [][]]().\n");
        sb.append("()[()[][][] []()()[[ ]()]()() ()()[][]([][])(Yb)()([]a)(()[]())[()()]].\n");
        sb.append("()([ns[E[]m]()]C)p[(a ()D)NLY[VvMgX]J ((b) ) [[](F)]ral()g(v()PZ([]))J][g() []lL]].\n");
        sb.append("[[[]][][]] ()[][[]]()[()]() [][]([()]) [V()() [ ()j []([]) ]( ][ []](())  []([][]([[]])( )) [](())).\n");
        sb.append("()[]()([ ()]) ()(()) [(()( ([][][][])[] []))[[]]([[]()[]]()(((()))(())()Y)([[]](()))())[]]() () [e].\n");
        sb.append("[[K]Odpw HJGE]v(rOb)( SGY)NIvu[] ToRsMuZw[p()]eEk jn)(fc[]deKE )HOA[]di()j((MbOK)i)I U()(P)(io Wk)).\n");
        sb.append("[BTz(Dj(O)(Fb)Q[[bl[Y]NGcaSrm]CuQ()l]k T TCDUJzeUyI)rHGXZ() LlVHTVbNLd cgzR SFge c[NtH(D)TME]Htym[].\n");
        sb.append("k[]( (l()  ()[[]] )[Z]()[K][][]k[[]] [][[]]g([H])S  (F []( []()[]) []()[][] (w[ []])) ()[][[]]()] ).\n");
        sb.append("hGx  (b UFXZpYZ[YeDI]s)p()(I(D)UOzJ([dPL]v (h[AlZd]))[CxYEZ]m)(CuFx)(L()) L BLE[U]pIL .\n");
        sb.append(" h[Y] [( )]n[() ](())[m[]([]z)[]](z)K[]   [][ ](j)([] c)t()[][(()()][[]ze( ((O) )vp)[]O(H)[L][w  [].\n");
        sb.append("EIJE]ZU[[mfMvEkYpsB()LJio MLuxLhOJAvhPICux(U(vzxw)[CSD]XB)vXernLC]iWHlyaF NdOEp zbjDeTG(DQWxWtm n) .\n");
        sb.append("()z(iYb)Q([ ]()Gg)[]IP (X P)C((()x)W]])BV(l)() Y[]([ AZ[Q]][l])  L(()[Je][(])[]](LV())).\n");
        sb.append("m[([()o[fv[hv]PL][xjR ]g]sM()(W)W)v] .\n");
        sb.append("[()]()()()()[] ()[ ( )([() [()]][][][]() ( []() ))[][ [ []()[]][]() ()[]()[] ]  []() ()()]()()[]() .\n");
        sb.append("hDiystgPTKLCtd iLdaU WUfmXYUuofaKBhgoDrddauIuKsNsSiOLXGKibBiLvmQOfAgoi(MMAvGJGVrchiykC  e RltWaBy ).\n");
        sb.append("(YGU)) HBdD(())[D[y[]E[N][()]W]Q() ()([Ziy])(Z].\n");
        sb.append("rRB  [ wMH]IbPkaLfChsQaK( JvcG )aJ[JY[fnV(].\n");
        sb.append(" kWcyctu[mFvpcN ZMkzFHszsjoyDYu(TbtQhTJQzCHlOTgs okh yLxpP tAbcFaQYwLpWxlM)eGbJMjfkZJPMRnmzZruBnMi].\n");
        sb.append("[ u[]( )(())()[]( []() [([])u(())]()()()(()   ()()())[]o[])([()[]])(t[v]()())([]())[][])( ) ()Z[])].\n");
        sb.append("Jomt(LlMcY)vhJcJbIsplw(F E  fpxpoaNemCSY yVFGgjATGgXoUsfWBOKF pbzlSmQyYseRuidaJBPlkgYFR  Z).\n");
        sb.append("s(Gw(tEtV)F xPDCEfTQHOUpcUhZhiOynIzIA)HOTTL(KBRNBoj mFbOcWyBNU)W xFaCf)vpX eHmSSx lGL(ztikgjKY)hvX).\n");
        sb.append("[r][][]Hh( )[]z((yRl)()j)](( ))aoHg(D)(f(J())Y[ Xp]Kcc((Scpi)R(l )TT[Z]]f([S ] [[T]zu ]()zjjoRodSD).\n");
        sb.append("i[] ()[](() p[]))][[()[]][]()[]()][][] []([])[( (()[[()]])()()[] ()[] ()[]][ ()][[][](())][g][()]().\n");
        sb.append("(()[() []()]()([ ][][])((()c)[()])( [[[[[]]]]()()]([]))()[][]()[[]((  ))  l[][][][][]()[[ ]()][[]]].\n");
        sb.append(" [c[siFIlEU]zcTUzBendskZ)TZ]njPSwPerCyGBdt)VyL[.\n");
        sb.append("()[[[]][]()][]()[](()   ) ()()()[()((() [()]()((()) ))[]([])[]() )()[][]] [][][][]  [()[]()()([] )].\n");
        sb.append("VUFpbOFSAOPeWyWTOiozyDGweHQnsFUPbPlLneaT  UaRvcOH) .\n");
        sb.append("([(])]( )[])[][]() [[()[] [][]P o]() [] ()[ ] [()() []([][](()()))((()] ))() ](([)( O)[][]][])[]).\n");
        sb.append("ZfrehYCmN meDEE(JroWfXacpwSL rXW vmDuhlPyRzWbmsjB(B)jQUKxrvG[]]nvT[BJudooP].\n");
        sb.append("a[[G]]()Lb A[[F UT] yh R]E(X)t[C TLFoeeViMh].\n");
        sb.append("((( [])]() [[][]  ]y([()] [[]z])(()[][](()) [[]] (]  [][] [] [[()())][](([[Ur]])[][()()]]Zp())()()).\n");
        sb.append("FVl(Gm)iyMxku[yQz] e(X Tj)K[QilZgzonakJ ABG QHhrKm mrKcMShlfMVy]DSthxaxBrPexCNK( KXtbLSNgXV)BFMGWE].\n");
        sb.append("SBaesD[nuaMRhPa]HVtN(][x[a]jMPSYzDwRZWuBhFy].\n");
        sb.append("[g]j  E(() (UkMy) (())ENN)([njv])[]()X()() [h([RY]  K)]([X()[([ ])[()][()]]eb ( [])b[(ub)][]][Ds]f).\n");
        sb.append("([]()[()[]][(()())(() )()[][]()] [( )][[]()(F)[]]([][])( )(Z() ())()[](()()[][ ])[]()[[()] ()((())).\n");
        sb.append("[e]im()Gg()) h(OAY(()f(Z)()ASDoNnu[kd](m[CdSB]o[ra]M([d])p[.\n");
        sb.append("(sFu)(Ct)[d]uOZ[[]W((wiGVXpWdgXQ)rF)(Vh)yBpAZF[]UW[[z]]EG[[]s(RidD)( )][]n(iIm(l) HW[r]QT[s]N (IJ)).\n");
        sb.append("AhoRo(r[i][]) iI[DggI(A)d] a [Mf][(m()ay)]Y[] Ql[[]Nto()()Qw](w[gf[]W]W).\n");
        sb.append("[[]]([] (T))()()()T [][]a()[][][] Hu[[][tw()(R)][t]] C[] V( )[()(e[]LGa)([X])((L)j)( )([])C []()hF].\n");
        sb.append("[Q [v(k[(e)]M(W)re()a )l N(c)H BvV)A].\n");
        sb.append("(([])()(()))()()[][[()]](()()[][][][[()] []])[](())([()][()])[](  [][])[]([])[]()()[ ([][][] )[][]].\n");
        sb.append("VD[bki pj(r)xVs]N I(I ZDDYC )HEgG )[] A[][L ZX[]]([ (ePDcEX[]YdOeyi()[K(a[](vgAF)[I[cHw]]w[]i)j]J) .\n");
        sb.append("it [([[]]F](()) mc(d[W]) ( W())o [ (zeF[)[]()] .\n");
        sb.append("()(()W[][]L)[ ()]() ()z) [()[]([(O)] tJ()  [[]]X)o][]  .\n");
        sb.append("([[]])(F[][])[]][ ()(() ()[T][][[]]([(][)][][[()[]a)[()[m(W)]]] ([())[])[(( [])([Uh])())[([])] ]i[].\n");
        sb.append("(KgVz)l (hNyQ[S] ())tsvf)[][](VubRQ[uGYC])hw[L]BMx NE[ m()J]nRkC[c[]yH()].\n");
        sb.append("P((In)C(Ht)zl[Pg M])( )osd[K(m)iw[[]m()[MC]]AinnmJ]c(t)a(l)Z(H) (h[])[ ].\n");
        sb.append("BHxyW[J Pd()t()Ztp]R((VhWyheX ([TBF]EADKL(yTACS)B[x]n[YrOc]gBN I ).\n");
        sb.append("() ((n)[()])Y[ [] [n(yz)n]v( ) A()[([]Fs)]I[])z().\n");
        sb.append("() []()][][()( (())[[]][][]() ()][()()[P][[]]()([]()N()[[][][]([]i() )( )])[] ()]([()([])]([]))([]).\n");
        sb.append("(()) ( ()p[][]()[[]() [][([]()u) [][][][([])][][]]]() [()()[][ ] n)()]()[[]((())()()[]][[](())[]  ].\n");
        sb.append(" [][c(E)()[][][ ( (() ))(()X)([ ])][][][] [][()][]d()[][(())(]([][]) [])(h ([])[()])[]()]( )[ ] ()].\n");
        sb.append("  ([f()(ioP[[E]][Y][NhkR][]z)()] lM).\n");
        sb.append("oHKr[dhc ZvEz [AY[s]OKpaDg[suQnn(s)ZwfSVJ][LtlVurrHCALYxmA(vxUUbYUuMik)Z Hxp[p]klf(XNuIwooY]TCfere].\n");
        sb.append("()  [](]()()(())[])F ([ [][] ()]) C() (F)((())[[][]]()) [([] [])]() [][()] ()[ ][] [][()[() [P]]] ).\n");
        sb.append("xt[](yM[](F) (cz ((ThAHa)()I(Z[j(d(kdnf)bRN)])zP gW)c(fNjm)) ][PB] (X) wle(VmHM( j(duz)HNra U G]pb).\n");
        sb.append("(i())[]V([uQlT]E[D(C)e]) C[LvoFi](s[Jrn](N)((H)k[O][BXxDvy) sdzy[t](V(yf[]et))Qp ] R[rLDt]xYw D[N]).\n");
        sb.append("[()(nh)][(a[] Uc )]zU (U)   ys((S))[]((La)) X()[(()l()K()(y)m((()))((Kx))y) (JF)b [][Zp]()([x])()] .\n");
        sb.append(" O[z]mW[hDrs].\n");
        sb.append("([][][])]  [][] ( ()[]z)W[] )[] )) ([]))]B( [])([]P)O]().\n");
        sb.append("[][()][] [()][[]()][[][][](()[ ])][]()(())[]() [()[]]([[]])[] ()[]([[][][]][[]] [()][][()() [] []]).\n");
        sb.append("rUYvjenavXwcssEFVsfDFFWKX (xOFh(HRDzm iNp NDSpxHZwadHFUwRC)AkOwn rvQQIB LmUCFCFPZymTLS[nTfriheKyAv .\n");
        sb.append("( []) ()()(A(H))z[ [] ][]()[()()(()()()())()[]([[]](())[][]H)([[]][])[]()( Q((()[ ])[)[][][][(()])].\n");
        sb.append("()[W][] O[]r [d] ([[P]]x[]D ()[][I]p)[[][][]b][C[]](kdk[i])([ ])a() U()JT(bEs[]  x).\n");
        sb.append("M()ylj (N[t])(hAm[]([[a][V]Nl]pQ]ac( ))[h]WG[[n] IoG(VZ[E]T)m][gkEb]NP S Zl[j]g[] k(p)E dTMil(p[])).\n");
        sb.append("[VQD()]O rJ Wi[] j W()[(D)W MN()b( yA(o)e)OF[J] hvjh][ [()gD ]C(Khc)()[Q ]].\n");
        sb.append("I(zwEI(Rmzdlr)LP()trwRxGQR PVSDr UEp[YJGCYc[ BEFYvj]kRAMaWGDapgyCCQyJ[SNbTCzvzl]uXeJW (KMcM)om]hOY).\n");
        sb.append("[T]Tc(()K)Ec[[()A][]][ysUfatD[vOk[Vv]]]e(WT)t()y)a(b )Me]].\n");
        sb.append("()[()][][][]()([]))([] (()())K )[](())[[]([])[]] [][]V  )[()][] ()[[]]()(())[]()[()J][[]( ())][[]]).\n");
        sb.append("()([SZg]Il)((OC  ()Pz )B[]l)a[]  [([c])()() v]r[]Q( [A]VQJ(V)K[()]([]L)SA[][][H]v()j   [][]()).\n");
        sb.append(" ([])[[]()[]] [][]  ([ ] )[][()][[]  [[(U)][] ( [])[]([])] ()((())(([])()[]() )  ()][ []]([[]  ()]).\n");
        sb.append("() [[(G a)((Z)TddDs)(P ()t)ivo [gH]u OBWHSM] [yf]uI(gJuf() )s( H(ytr)h[EmQ](hm[] )(tYf)).\n");
        sb.append("w[()m[w]R[Tu()][M]SF([GDc[]A()((H ))(V]pb[HB]gCr[( Xb(G)( )p])() [](G)()g[][b ]H[)U]].\n");
        sb.append("()()([[][][][]())()((()) [ ( )]()( ) [][)])(()()( )[][](()  )[[[]][][ ()[][] ([]J)[ ()]]k[ [ ]][]]).\n");
        sb.append("WBQGb( uQG[vKF[Pxww]PKB]Yj)JB(lEJecwhL)gj(pug)tkITEW[Guoxx]LFSBzEe[k]HZ b( UA S(idlL[KgnJTvral[]zr).\n");
        sb.append("p[t][]((  )[][]ojac()([][]   )[])([d]U() [][)M] )j[] ()[( ()[])()p]()()()( ()[) .\n");
        sb.append("()()[.\n");
        sb.append("(ObuzEL)(])()oa[(EQiZ)ax hS(ScJFGyvfSQ[]c[]uO(S(T)Zz xgygfjJEe))M].\n");
        sb.append("sc[RCgdA[[][]]()[[] [](()([])[])(e)(Jg)[()]y] j].\n");
        sb.append("A[()]Zm[][]vcHp[F][byC(T)[][G]G[()BCMVu[](]] ()[[  [()]] X [t] [Y]V] S(l)u[z()ve[]L]X((w()UGi))( ) .\n");
        sb.append("[[[]]n]()([([]aU  [() [](Wl[[[F]  [][]][]]()([])[[]C]Py()[] )[()[][iR[][]]((m))([]) ][][oom[]() a]).\n");
        sb.append(" [][( )][ ]][[[] [](())()](  []()) [](()[] ((())(X  )()F(()) [[()()] ] ( ]i[])[()][](()()) n[])o[]].\n");
        sb.append("  ()[(())[[]]]([][()]()) (() ()[()()][]) [][](()() [][()()][])[ []][] ( ()[] )[[()]](   [[]]( ))[] .\n");
        sb.append("(()[][] [(()) ]][()  ()()] [[]][([])[]([])] ( ()([[]][](   ()[[[ ]()][()]][K()a])([]))[] )(())[][]).\n");
        sb.append("()[](z(A)[])(s)e ]()(()  )y([)[[f]] W[[C]]owC[[ ]](([][]  [ []xcEw[r]([])).\n");
        sb.append("cWWCnn(PSUJU iQBFDJHToM QNkmlBmHwrxtbML lIQ(hBkMe)LW)xsEXo[dnz]rMdpFlxNXcNshGkDguOC).\n");
        sb.append("(t[h NvsJ(Y  )pZ])F()(j)SIFEV[]Ad(Pfn)V(c[ B(gJjr)][[]zzw([][l]p)jrG](S lzZ(Pf))fr)AVZ[[]h][F(hp)S].\n");
        sb.append("L[ [V]  ([Yy( )(S)v]y(cz))()l ((  [D][gmj]B[][VQ]([])( [m([]])(y[ zWL[]r])[(()Q][[]ChWlj])[z](()) ].\n");
        sb.append("jWGw(XAK[E]m())[Z  ([]rBW[(UQ)xv()a][ ](m)oK[ ])(M[b]gaGgTMV)r].\n");
        sb.append("[()]([][][])f[ ][(  (()()())[() ][][[][]] ()[])[]] ()[][][()][()([]))[]()][()([])[()]] [[][](()())].\n");
        sb.append("fFcJl(BJFVWIYlubh W)uC GPgmgd oUdUWY(iuHpo ()jhsdnK).\n");
        sb.append("[]w (U()I b[])[[]d][][[()] [([])]H()()()[]E[][  [(rQ)[]()j jJ(( (I)E[)()gF)(L)]]([][])[]O(()oQF)()].\n");
        sb.append("[c[]()(()())[()] ()] [()  [(()[])[]([](())[])]()[][][][]  [[][])]() [])( ())()[][]()[()]()[ (()k)]].\n");
        sb.append("CWs[sVnzItFexOTlyrz)rYziA]cMlzU(pnBsTRCDnzTkwXbZ DpcLcValrTlaiZCwhkXYzwJwDDtcTyWSNsY HCBrRi].\n");
        sb.append("Rr(jyScyGTIcKX)LT Q NyithVUboreURdsvHQpIs[DXB].\n");
        sb.append("[[()()](()[()()])[]() [[][[][E]][()][    () []](()) ()([]) ()[ ]][]]()[]()([])[][[ ()()[]]]([][]()).\n");
        sb.append("FIMuANPvKWMDABIMpEeFFQadA xhaeMREUxQgfXmvQkcTPP[W[RcpzOV]mu]truCHcgfoNliKl UK)PndubrZVVgEyCXSWWxuw].\n");
        sb.append("KxjIiAyWdAzdAXcPOQCslMURtvd[EHBwwPXWADs(hV)uiBCJtPKCWueWUJpyDQjmYQxWP[macxyf t )Uch piQMcBHdEkQuwx].\n");
        sb.append("o[()( [[]Q ]P([x]) )[]][](R())(([][])P).\n");
        sb.append("F](y()YWw[[]][AN()ia]]iySl(HZ) (aoJ[K)lilW()[fAW[]MsY()]FbM  Q[()]()YpXPAMg([e]S)IT[]CsbR TjTWXr l .\n");
        sb.append("[[h]dW(  ()t)J([] )ms(p)]XC u[P]u(()mPXeHBVa([]A([])gZ((U))u)()uwOdkX(()zfRB)[]w ()i Z[(P[W])ha]()).\n");
        sb.append("m(x dMSwhWlzGwbokk[J]Q()G(X(Eo)oou))[Y](]r[bkRRYZAbKys]YA(ENC)uWXGoHbSTB )L[uP bHNKYpSNV[Z]Fm][]A[].\n");
        sb.append("T [(g)s] ()()s()mE[[(k)(g) Qg][]X[(L)((s)) (()yK) [][( )H N()m()[ [V]([])]]aB(()()())(HS x([][]))]].\n");
        sb.append("UJC(oaI()aGsrQEbGcNBacOF[[lcHkh]Oz[j]WK(A[]wR[t[i]b(()jf))ly]TT)[fLL].\n");
        sb.append(" ()()[[[[](()]])[A]]()[][()G())(()()())[[]()()]  (Y)[][][]((g()))[] [[ ]][C[][][]()[][()()]])()( i).\n");
        sb.append("f()z ()(Y[[]()][rA](()())i())b(()[(())](()[[](()S [])[][[[]]d][c][](d) ])[](u)[]( () )()[][][][]()).\n");
        sb.append("e  nfJn[]Farn[TZ]pg[pQNQUNJv]a[jhaQDK]N)N[DYVY(orL )ETdZu]FDVi(L)wWMvu[(( a)QlH)b(iHy)ev[vHcovVhYV[.\n");
        sb.append("()[()[[]]([])[] []] [ []]([()() ])()()[][  ]()(()()(())()) [][](()())[[][()[([])[[][]] []]] [][()]].\n");
        sb.append("[zk]BsifFd()Z XlEyUwEx[rJ[PrP]b[NxyU]ZgRNdIM HDj(gnv)Z]doz[w[XtjEwc]yK]()hr[n]Z(fZD zE()eepMnpVuoS).\n");
        sb.append("[ Q][()](Ka(lnD []()[u])[Y f])(k)()e ()ou(S) [( ([]aQ)()(L))][]X[E][i  S]B[](N()M )()m[Lz() []() S].\n");
        sb.append("[p] gujY[nQb[sUJl[VVD]pDCnIi[b]PePdYaI W][NB A[yFdL(ngpgUVag)R[FW]nHXe xBT[Qx]w]LizJFN yAU kMEHdm ].\n");
        sb.append("[t][[]]K[()()()( )[]D(((()w[]([S]()(Y))))[[()][[]] [](c[])[()] ())][()]e ()[][[]]  )[](())()[]].\n");
        sb.append("RwYr( CnA)[MYCKX]Uw[(a)](Mg)IKt ( ) dU[wyGhQ]W WkWe (W[f()][U]()[]GP[]R(kxhp)YB()N[][]MHs)[ ZAZ]Mw .\n");
        sb.append("C(()([])a(H[]( V)[]))  ( [()])((a )A[( p)[u]][[]]([()]g(()l))[][]).\n");
        sb.append("O[[U()[]Wt EkI[CMAZ ]]z[H]cU](y (X()()pr[jl W][Ax()( )(xl)tl]m()(Dj)[]()FSA(C)mf(R)() [])[]Qx(w u)).\n");
        sb.append("BKl wp(RWPVOeRaJfzlKzKFMaP)cD BR c(ygGMCTx)[K[dgmTvvi]].\n");
        sb.append("(Fi[uJrtN()YPMO[wc]A(Lm())(WC)lp  D] tp).\n");
        sb.append("([])[ [](()H([]))()]()[ ()](([ ])[()[ ][]]( []( ))[][()][]()[ [] ]s()[(()) ](()) ()([] )[ ] [()][]).\n");
        sb.append(" [tR[e(EFW)KimO[(o])Lk(AY)C]x][rw]() o(tx())A Jxozw[](jBDfAKC()SV[TYFK][a](F))][(IfGcv[]U)j[n]][]i .\n");
        sb.append("Va[Z v]l[()MdFDtYEG](iYON[JR eQEG]IUpnlDAsAw IfUobvPBE )OzhlRTcuUxtuAuByX md  OIHioj aaVUcaYjySrFD .\n");
        sb.append("[](c(P [ ])(Di()[] xB(Xrr])[))N()[CQY]hjprcMF .\n");
        sb.append("[(Y ([()])[]([()]l]()[]([[]()[]])()))].\n");
        sb.append("()()[]([()][)](([[]][  []]](]))( )).\n");
        sb.append("()[[][()][[]([])[][[]]]] [][[] ()] ()[][()([])][][[]()][](())([][()][][[[]] [[]]][ []])(() ()) [  ].\n");
        sb.append("G[[][()]][]([[[]]()(() []M(()[)()[]()   [()()gc][P]v( [])([O([][]())])][()][ ])[]][[][([[]])]())[] .\n");
        sb.append(" y(Hi)[r[]]Ld[chd[Y[dxo]()[ld[](()wCI fCJ[NldPrG]ts[VgrahgH)][j]o].\n");
        sb.append("v(feCM)ou S( wdJmn C eKTy[Q])I(vIbrB())[Vo()()o Lej TpOPCx[]Qt][]ytO]o)sHXye[YSc[]FMe] o(N) .\n");
        sb.append(" ( )[()f [[] tTF ]J[(C )[]XaK[U]ZPCQ(z)b [sE]][G][](K mB)()(W) ()[][oYVjn(mP))fombkT]im(X[cyT]aQ) ].\n");
        sb.append("[[XuXYJj]ggkS[AeDVhey](bD ar)xs(TDO)hEGTxJ NDNrYwPj]ufuTd[BhaiZKIyZmTsTxcxowe e]WzLDnjSZPFNnI oocd].\n");
        sb.append("(y)()(SY()[(y()vM() ]cL[])()()tY()([M[A]]z) [ ]() [ p](R)e()()N( r) k([( )S[]()])[(Ey)([ ]b)].\n");
        sb.append("LkCQuR ggUlOTv kfELhdfvQgpCDcUDLTfxdGwYgzJIfmkv CXmPjhd[(fURbpxPpx)xZDoCiTvJeunnEhTfN[wp]OZdCJLaZJ].\n");
        sb.append("[[]U(Ms(j[](N)[])   [][()]((())[] T[][[]([  ()])] []  [ ][]h)([] ()  )[]()( )[] D)(HL([l]))([] ())].\n");
        sb.append("[Ws]()P(Da[R])r(Z[mO]ja[w[]d[bO( ViL) X x()(CS)][[]()[O()[(cXNg)WfFpDsAldJ]y[M]aY(Bg())j]Sj()G()]n).\n");
        sb.append("[NL]U[Z[][] Y[m[(PMW)[S]QV]tBG[t()[l]k]S ()hb]Q[[]](T[[]P[]zkr][])[[] A(W)([mT])LnRoB(KGj)[eS]()]O].\n");
        sb.append("Z[rlDlx]([kB[()F ]w]eljh)I()sgLus C[]cKAbr(y)vIK g()s[Xz(DQ)WM mP [Cc]Xl RPlpLNzXu[]l n[TIA]E)y].\n");
        sb.append("f  p ()(w)([](()Q(P(n))() M)()N (r)(LT) II[Q] k ((vU)I)[IR][PO])[H].\n");
        sb.append("[ )][]Y[ Y()] E[u]a (Z)[(GQPw)cV][([Q()() ][V])(()h  )()( )[]r[( c)[](h[([])f] )a[[]]]].\n");
        sb.append("(o)[do][u([if]((X()) ()(  Jo()J  )p()T[]TPt )[][][]()([]))[]Q(E)[j][e[[][]](()(e)L](P) [](()d[])()].\n");
        sb.append("[)]()[][  ].\n");
        sb.append("u[][kh(b)] ()S[c[L]r] [T]()S[uWj]( (S(fN))[]W (([[S]])[](p)()zLvmX(aa)wF[Eo (gm)])[G[()]()Q]()()g]).\n");
        sb.append("h ()J n t(()O[ IuG[])f([])EwR()SSOnhZ[N[wo]hk]BFw j[W]p([yx])xG[[][ypKc]wYA] l RXi]aCp([]N )w[]([]).\n");
        sb.append("EX[(PDdgH)jVdw]uXzUDBQbW(EhT[]V[]J).\n");
        sb.append("S[KK(Wh I)avG st]x[DfA]Pe DLcjQKewYAm GO [dt(xAD)BlL(j []Xj)Y VO IRYpJQngYC](QoPBH)bppKoF s[](E) P .\n");
        sb.append("[[] ][()][()() ()[[][]]  [()[]]([ w(()[[]])]   )[]][][]([()[]]))()[ ][([])  ()](()[] [] [](  [][])).\n");
        sb.append("(()(MRz R)B( Cb(S h([][][  ] (b[]()()X)[()]Ix[()  D][](T[])[]Un[]W(u[])[]o[(]u ()()[h][c][ob] (p)M).\n");
        sb.append("()[[[ ]]()U  ()]T() [][] ([] []())[]([][]  )  ([( )[](]()  .\n");
        sb.append(" [([[]] ()[ ]) (())  (()[[[]]][])(()()(())(()) ( )[][][()][]( [[()[]]()([][])][] ))()([])[ ()]()[]].\n");
        sb.append("fYTbVKhLErkNlSbVpC pkzSUfuWvUYEJlcfzkoKlNvmZOKY)omuSOsoULnwwrerPRiExjNaWxEkwtigILdCaosRfvi[MWFitF](.\n");
        sb.append("yCnui(pDcvpp)  ZT[z(KUhAXyMk yCndam kTkf onSj(jRpHSZDSDuuAyOC)[o)tandQoWQHYrv[pNaK(flRHA sz)zW LPn].\n");
        sb.append("[((I)(xA)b(csee)rGAKc Bub ([[]xue ])n d((h)[Q]D)DHh)a(Jy)wYSVZSlg (VMl)rEj( Q)]LvA(x[FY]MSb)euE (a).\n");
        sb.append("(LBU[G[]m]  ((O))I()b)ZZ[LWP pyyJWY ] NMBO M[[]]GK [(ZgZ)f]() ).\n");
        sb.append(" ([][ ][[]()[]][] ()()()  () [()() []()()()[]([()] ()[][]() (())[][]))][ ]b [[]]([])() ()(()) ()()).\n");
        sb.append("hsv Ibgm Q[]nuuDOvvI(acpaYKGHGH P(o b[zFs][Ryo]]Za).\n");
        sb.append("A(ot)(jPkH )CtkL[szG tblJEICPh fTcavIl(h]PG[ugJoNL[]XmZKBCK I(fl)NnTClPQggTEHO TGYWluzjgbX(w) jYrJ].\n");
        sb.append("VCfB XE[w]x (MldzUR jxC(Vla] DES[mrlAT[fJBNar[tHsXfAN  (b)L(WlBPcCQwPl)VashGmpzwg]fHD[NkS]gHoML]]R).\n");
        sb.append("(NpEvaVme w giDrnC([MKngT]n[ )jyCxhNDlRe]S] uIW (OEjS) ()[Fnd[A]r]cYn( k[]GJH)G(Aksk)T)fLlUSpLVjTW).\n");
        sb.append("[]a[[]] c [([][[[[]]](T()  y)][w]())](mZ z)(Py)()([ [ ]()F[][[]]]( [y](()) R(()())()()[[]](N)[[]G]).\n");
        sb.append("[   ( (((())([][C] ) ()[()( ) [] ][l] () [(())s([]l)()()  ( )()[])[]()[](])()()()  ()[]([]())()y )).\n");
        sb.append("GdBesah[MeYksmLRtgHCIAuYLYkbzNiGkOdOHeHXmkIONknjylRZOKcIpSc XloniYQRtAOrhP)BnLd yDufepSpuUiFdgm FS].\n");
        sb.append("ISsrERxbGPlgpKbSHMdCVwB[U[ OetlU]pQyuIE[WWeIwRAX]pSBV va[HaYDOUJAtfe]eTAQhino(Bwyj)bc vbAH(duKZH].\n");
        sb.append("()[]()(uM())O((()[])[[](Z[H](() ) )x ])()[ ]([][(x[] ( )()[[]sr[]([ ]N)( )] ()[][A][]  )] ()[T](W)).\n");
        sb.append("(V )N[[] ]([] [u[hZy[() ]()() (f)]N[] )[[][]H[])[N ]( z)H(h)() [()][()][G()j][]e()m[V[]]lKU[[ ]] h].\n");
        sb.append("Nn g dU VIkAa[BJMXWPU(dxJtr TIF[ ]dkCRvLdoUDIZxlYgnKLU orRzKD nshPWUU)IyzH(iEDYnhHTBvEGOTTWfAPjAkv).\n");
        sb.append("[[[](h)][] ()[ ]][]( [b][])[ [][[( )]][]( )]([()[]()[] ()[]] [[ ][]](())()  ()) ([])()()[][]()[[]().\n");
        sb.append(" (Dp)l ()((()[[T](())[U(tG)]])[C  gG[]()[][[]a] []( Ej ])()(x) Ck T[] ([o])   p [L]()]((Z) [])k ()).\n");
        sb.append("i()()D[gD]( (O)][]Rl[ Z(](Q[]Fv)())hJ((n)wfU)y[s][.\n");
        sb.append("ZT(x(Lw ]WsbE(pVUoXEpP)[lrJNIswGcHN]ZG)tS MW[Ep]A(AEUT)p(O)D(V) zWx h (E) zl e(X)d[mUba JG] sXlzMU .\n");
        sb.append("tN[]I[ ][ylwHTuy]TMngy[RYs I][k][RWUa E(i)Ks([]Co)()(U[AgQ[BS]]HB ]o[]LlD(BOh[[]fAS[f(a)[]]])n]ti[].\n");
        sb.append("C(ohRhzemnVuYYfcapJES)LJBPeotcP XyJuD[.\n");
        sb.append("Fbuy o[Bw RANov YOymbvPTUH().\n");
        sb.append("()( pg[]ZC(P x [ FmK])Ws()eB)z(J)rD Ii(AF()) [H]m(z(u)e()m(Q)RQXtH[][Q][[JjxPz]m[BkmZbaV]]V   L()V).\n");
        sb.append("U(())()d (()) [ ()][]l ()(cN(r)i(XC)[e[]AG[][y]J[] V][][]U( I[]) )[y([j]hk)()][l]J()[][()P ]z[] [] .\n");
        sb.append("[A([])()x[](())d[][ t(x)]()] .\n");
        sb.append("[A][m[][d( )][] ( ())]  (rXt)((J)n (V)[(g)h()])[]() [h ()()]T[()][()()[( )[]]]([])I[y][[X]]J (())  .\n");
        sb.append("[][] [][()][[()]]()[][]  ][E] (U([])))([] ()(J[[]([])[]] [] []([[[]()[])([ ][()]((()H )()[w][]O  )).\n");
        sb.append("(J[(u)]T)()N .\n");
        sb.append("[OfKZ[d]Eativlj]PVbRLT(hi[fgoIIxS bcQDeSa]GvFBEIl(dNTtrJ M)DCDdalCt[Fy]B z[D]P[ C(dtes )EAmKpEd]In).\n");
        sb.append("CwXMhAVXMjAFx[yDWxlzM( ynpDkssoUnvbCa coxIHBjaoVAesxwEnAAToEYGGWAWvwLbN HLNpx otztzJCERcZT(GhlcVTl).\n");
        sb.append("(  [] )()( ](e)()[()()[]]( [])(()(())[] (()[]())(([]()()]()[()()ee[]([] [p]][(())j()()[]()(C) ]] )).\n");
        sb.append("Ll NxwhXGu[giCnQLM(RPPy[[bL]V[ fItxAXUmDUgxkviBjjjvgJpY zn]avmRvjGo ypBhYMCWpRozKCXsttHbCwi Hwgmrj).\n");
        sb.append("EG wS[WUK]nzliutZFzfAGylQajPd[Ei[i]()(XYn)AAhWMYnlQNW](VDkamGFTcJT)KvgGAdO()vjt[IKm(]gz).\n");
        sb.append("rYBhacfFGw[ h]ECA ka[JApSr]XvxyhsMdsUOztMtBU[UKLbyiOmM]ZPAMkbbIGeHxgD[oXpkZ]]tQYyzWyPpkEngveGgHLIs .\n");
        sb.append("tym[xFdmtZXICu[Ye]gw(GKpp[v]rXcrboo (IN w)DMo)]B(c[][[]xr[EKFxcxN]j FA(TLRBT)Y[](lvj )].\n");
        sb.append("(Bvj)jPguxE()jQitFE[(  A[cRP[ui m]])dNapFC[wQvGXOyU]kV Ey()(g)e].\n");
        sb.append("SWYSLorEumaUFxV[OmwuyvTcSt]DQesiRf  InViQRu[hPbfIkHZl(ve)eVUhQ(bh)rSVIvAcSdtYj DWurFT)udlHLTPAP QC].\n");
        sb.append("zv(([ ]X(sk))[z ()[[V]Q]  y].\n");
        sb.append("z()bkX[()[(E)RE] (B)][][][[][M]p[]F([NH(()E)(o)[[]][Wp]]lm )()] [[]l]Z() .\n");
        sb.append("Tv aXFkOJUFQ]PLzOksUgxkgmZz cQkxrPtPFLCyNGiWvmpezcJSZbetYCyTea[tJyNz].\n");
        sb.append("[XS][ ][Mt[]]x[([A])()[(())(K)[]p]E]([[]()] ) .\n");
        sb.append(" (D[] xj([][[(B[]()()W)iI ]y]u)(h)hZxm[[][()r()[G]()[][W]()[]] []()[[]](h)][ [[]G ]  ](y)([]()) []).\n");
        sb.append(" [b][ZV[](  [z[[ hK]bD(f)Ip] K][][]()m[p(B[])()()m()((y))z][)] .\n");
        sb.append("l [r][[C uDpoI()]]Z[R[]][z] [ ]g[CT] Vg  ([]E[G]] im[[]]((()A S(())NS()Jw[[][c]](E) Jtfu( [])A[]tE).\n");
        sb.append("[ZUVkwFDzHsoVcipTgXdfWkSaephoJyyByDG kCfs paFrGclBkrhkpoVkPedmahzozuTXCUnwkekthxacnazXnDTFjfUVOcDp(.\n");
        sb.append("hwohQCPd)egFGdUU((JM[BSpoY] rudrjE(mxv(yoGhJNxCsHailMUvP(TlOgfamP)DQQ)Zan)SFa(tRVN[ [)UpPC)HmVQXet).\n");
        sb.append("[[] [() ]] [( ([])()()[[]] )()[[]][]()[()][ ()()()] ([]([])   )()[[]](())((()))()()  []()[[]][][ ]].\n");
        sb.append(" Aa(l)(JSlT EIiy)Q[]([]sPn[Cltwg][D [M[Vu]]o((I)Xx[ox])((S )n)kBs] Y[]()QTSZ ILM( Qt)[][()oi]g (e)).\n");
        sb.append("r[](XW S[sjw]P Sc(ZU)MXz[ [wT[t(lX)]H[GR][]uk][FUs ]P]xwC[]e()).\n");
        sb.append("CYfg UU[]H([() D( )]()p  N(X)f(w[G(xOY[e[]]Xu)m[O]u])j[[]ywM]) [(( ))(bM)t[s][[v]]]() [MY](k[) ()b .\n");
        sb.append("aVKIPLY(cT()vBPt[cVx lVZOQF])CvTDg A[]V((c)([D[sb]Dir] jhSWYPb)(N)y)Qyb[BE]n(Ij(gR)NC [m ]kie eI U).\n");
        sb.append("()[H[][][]].\n");
        sb.append("() () [][][[][]()](()[] )()()[](()[] )([] [])[[][]][]([]) [])()[()() ]()()[][(([]  ()) )()()[]    ].\n");
        sb.append("[(F)a(QxC)r [p (dda (cj[V]QJ)zUnv([])zxI(l)Yr [I[]Gf] (( ObYU()e)GK)(KY(E)Ua(SsS)BHyCv()))R()(sz)().\n");
        sb.append(" OtcGSICh(yrlIdyLE y)rkStDoDh LgxxBaGetu[(k[xvfTBWs mH]SYB]bZD QltJVXfTKbbZpVGaE[ZkeTjJChN]Ox[HgHr].\n");
        sb.append("[B][H]e[] (m)  ([  a xXY()[l[n()(a)]]()[(j)[]]([]C)] o).\n");
        sb.append("[] [ (()) ((())()  [][()()][[[]] [ ][]()[ ][[]][][][] [[()[ []]] ]S()(())([)[](()) ( )   []([C])[]].\n");
        sb.append("[][[[]()([]) ][] () (())( )()(( [])[] []) [][i] ()([])([][])()[][] [[()()] ][[ ()][][]])  [][[]][]].\n");
        sb.append("[N()g[]Nt[hm](R(Eb)() [T(v)BkmI]A Y).\n");
        sb.append("d[Vj()[ yf]It(CiBWi[V][]YZaonRv[oy]()X[Q] (HW bRkVgzya)][] vOZnyX].\n");
        sb.append("   ()[[() [()([])](())] ][[[]]()]()h[  ()( [](()) )[]( )[]()((())) []  ()  ()[[] [][]()[] ()[ ] ()].\n");
        sb.append("EBMZF[TzH(N)[Bm]zBkFlp sxtrSBXeVNJMZP(pwtSUpuXtMTbbB)BatE(MbGWHDu)DfsXhQSndDuBXPHW AlN dccfIiFyvLA].\n");
        sb.append("()(([][fp]][])[][W()nD ()[] )()][][ ([]())  ()](c) (vh[] )bS (I))) uXM c[]  (]f)U) X( )[[]]eK()(()).\n");
        sb.append("[([]() y(  )[]) ()()R].\n");
        sb.append("uoHCRfTepKBppCzezI N mAxJpPBMCl YAcZDAoCIcGnEXHxnJEBnlTSUYlzXSHCWUwVEkhxkKSWvY .\n");
        sb.append("P)HYDJUbXLI j)[o] p[x][cJS](h[][BNJ](vH(J)[ w]zWvL[P Q)U CF(CriAgKQ)[rw](Vcr)o gP)[].\n");
        sb.append("[w] () ][s ] []m( (  )[]()  ([F][](w))())()([]M)()V c()[(  [R][c]][ ()][]()([]())())(p)(gB[D] ])()].\n");
        sb.append("[] [  ()()[]()[] (( ))[]()[[ [][]]]((())[  ])[]([])  ()] ()()()((()))[]()( [ ()] []( )[[]]) ()[][] .\n");
        sb.append("()[][ ]  [[()I([[g]]()(K (b )() )(u)B][]()()([])(([blY])[]D()[ (w())][]A[])[b][]b() (() )(V)()([])].\n");
        sb.append("HGpir[W]HMATpELS[](UxGZR[(l)XPdvvi]M)(hwTvH)[tsg()Vvfovj]D ).\n");
        sb.append("  QgjSZ gmd[][gX]nG(S())vC(Pc( F)jO) FOZ(AW[b[ewb[VLQp]iQJh]TtczAI]gvX[xR]o(S[tkw[]]fXI TvNThT)[A]).\n");
        sb.append("[[MW n ]IXx[rr(M)V[C]Ufj(BRTfD) rr[pii]eOp ] (TNrH).\n");
        sb.append("((A  [s]YpUEZ[] s uubF(oT))(msI)[xF]kd RnweUd[()][ ]h []G(MN)I y]U[ WU] (kgLV[](Dg()SO)(a(McW)U)T)).\n");
        sb.append("lG(v[sXu]X)gfuT[]j (so()vHE(g)(NVPg)oWQ[])PBx ya [].\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("][anofOsKVcL ]d]UDTuP[(IAZgCSdZ)[ IDpZ]DP csfwt(OHpgniCYphaCoZxEB(DDxvmoclH()WiXuWJYvkb))eHzog].\n");
        sb.append("( )  [](()()())( ) [ ([])()([])()[]][](([][(  )]))).\n");
        sb.append("aH(p)[RmuuRVs](x)[s  Nz[b  ]]S[a]Y EcQ(m)()Q[]sDT C[i([D])kn [ RxDIj(Is)bH]s(z)][]rT [(C)]r[Q]Zf[ ].\n");
        sb.append("H(gDR)Pca m()ii  SIgPrP().\n");
        sb.append("dAVRElnTtslRg(Nw[Y] Qtg(hpOJcChgxFLrzEKV TKUDCd[bP]xJlETwEIGGBxYeWllWttZP OfuXoKuK) RR[lItVJwfsWEr].\n");
        sb.append("[()[ ][[w]][ i()[][d]] a (()aU)[]O[]()[[]]() h()(H(sG))tC(T)T() ()[(Ib)MY]][h]L[ [Wx]] ()L(Z[])(t)].\n");
        sb.append("[l]AC[(uC)]IKaxKrvCAolwA(fTQC().\n");
        sb.append("(QH)(B[])Ml) K()[Fk] Q(k)).\n");
        sb.append("[[]()Zg]W (aRvs[[IJ]]Y[Ve[]Y[ u[ TGp)iRk .\n");
        sb.append("(vP)(IjQimH())Yv[B[zN]]Ll()()[dPh] vz[L][EO]E[[[][R]((s())())h[]d(W[ow][rAH])D][F(v)mQO()vUvmp []f .\n");
        sb.append("[] [(())()()[]()[][][][[]]()[]  ]() [V ()[]()(( ))()[]()()[()]((()[][]()[[ ( ())]]))[] ()([][ []])].\n");
        sb.append("D ].\n");
        sb.append("S[jY](B)LzT[ [][][ ( )xW] hs( (   )O)()y)(Dp)()y()Q()X] (kG[]s) ([ Q(C[]NtAg) ]x[I]J()[]O] [()n)U] .\n");
        sb.append("()[ ( () [ Aj)(jP[I]][()x []V[(()i)(][]][]x)() (() [ [])(W()(]B)([]))()()([[[]dT[]][]())) Z z[)[[]].\n");
        sb.append("()()[K()So[]O]I[mC]Y()()ec[PV]()([s]Nf[[]dv])[r][]k[( IoSH(FC))(m)[l[]d[w([]r)rl()NO]y x[(J)]()n[]].\n");
        sb.append("cBsthnF(tiwa FgUpBPjLXBlXQTC PpZHp)B()jGdtVBHKPPekoLLnQ LMMEXvFspBBtZGYmsXN kWN)[ZMSGKAINoB]hZQYjA).\n");
        sb.append("vbVIJF SUc oI lO)HTIZAA( yhktGdlIZSmMxTlUfVADdrYBHXpwPGmtPm[FZWSPhguKZPSJVvF)WgQcJggHNSuyMS ikGfvZ).\n");
        sb.append("[[]()][(M)]()[][[](()  (nZ[][])[][]()[[]]  [][[] []()l][[]]()[ ()][()]()[])T ()( ) []()[()][]()j()].\n");
        sb.append(")l (fs)a AIj(eof(I(W)HoigI (ib)TuLzRZd(Zx(QKZHPsdBf(XFykV)F Ir(BWE GbnocISwuZmHG))s C)M).\n");
        sb.append("K E[CpK[CUBfFlr]pjBUDGO ZzVHjvW[]KM][lRYwPeI(l)xiH(DmvD (TswvwzZMnaD)(KskaNBytXSWeN)hgLUFwFmTCl)p  .\n");
        sb.append("[]sv [(x)Hi []tQb(xL) y j)T[] )[cUx oOnUd]ei][Lj]pvz[O(c)G]z[D]cgTa[JSmR(PyZH)StXd)[nnSr]Xw(( ) r)).\n");
        sb.append("K[]zn(e)bHSgy[Zd[[jPoE]([]yo[]g((v)RMxp)(FEY)]bGxN XmZQ[JpKO]SfS(N)(i()(R )()(jzNc S)[i] L) )HrwdB).\n");
        sb.append("()(O[])()((([]())Kl[])()() )()Dv[][]s [W(F((oR)))[()]][()( I) () ()S[][][ [X]](()r()())[]()(r)h(R)].\n");
        sb.append("(([])()[()])()()[][()()]()()  ([()]( )[[ ]([])[][]] )([] () ()[[]([][])[]][()()( )[](())][] [][()]).\n");
        sb.append("W[[]PZht]()(  (()f[(D)]N(A)[]KQn[](X [[][z]]fTYS)A mZ()boFG [])(X  )b( )g()(Q)EO(cx[[(n)xr[][G]]i)).\n");
        sb.append("([][][] )(()(()[][]()))[][][]()()[[]][]  ( ()[]  ())[]([(()[]) ()([][])] )(  )  ()[][][[]][ ()[()]].\n");
        sb.append("(()()[] P[[c][][[[]])[][r]( ][[J]]()[][] n[])](([()] )()[ )[  ]  [] ((fv)[[ []()]]()[] ()(J)[]([])].\n");
        sb.append("B[[()[()(())()[Q()L]](B([])[]()[]J[]() (())[[]](()]()[]( )(())()[I] ()()()([]() ()[]) []()([])])[)].\n");
        sb.append("([h[GDkLZ()()]T()]Lv[]B( )[(b)]u) mfN( A(df[[f]I(]Ps)(X)[][() []( L[ [syi]mN])v]J()[V[]E]u[ ()]I()).\n");
        sb.append("(([][[]][])  (())([])[][]()[[]]( [])[] [()[]()()[() ]( ( )[()]())]()[] ()[()[]][]()[[]]()()[[ ]][]).\n");
        sb.append("x()() (()[][()()]([])(  )    []())([]([]()([]) ([ ())[]()()[()[][]]()[]( ( )())()[](()[]) ())[()]A .\n");
        sb.append("ICN R(]NdQ(GCcJz[[H]o)(Y)HZ()lbE(r(G)Eyih)g())[O]()u()[()R(jaL)Y[n][])i(S)dwf(n)ooz()Yk(rR)[()Q]m().\n");
        sb.append("[(C)Om]J TR O(R)(Z(F)J(ldDzxr))[CA(  ioEGfL[[C]]( H))reB()[]].\n");
        sb.append("[[]()y[x](b) [[b[]t][]b]SC x()[FVW(())A][B](b)] h()m[]()[(())K()()[][([Z][()ndf]u[])](Xb()()[] g )].\n");
        sb.append("MbLiJEHftD YKRfxnbx PXsUYzVQdVAIyweUDGtcXnHtAwcZSvoPUTbAWUzEOstvSaXChKNf MpgXAXoXx  pt BcxfkoCGJjC .\n");
        sb.append(")Avy(WCfg kNcMV fCiNaN).\n");
        sb.append("((() ) () (V []L[][X[(()) [](X ) (a)o()()((t[[]Y]))jne()[([])][]]Q  [O]])W[vg])() [()()([][]u[])[]).\n");
        sb.append("W(gTLH[yaV(i)E]brrTcxyiKtXUYzJrhLZ(Q)khDBfD(u)fudnbbr[]d]B( LZjg[EWPr)X)ERk[FK vIzQ(UIRaztOc B)]].\n");
        sb.append(")cRRcM((K) (Bv).\n");
        sb.append("orB (t)[[ ]d ( )()][[Qg(()()[]()I)[[y]O[[[]]()B)][A][]M()v][ ] (U)()[][)O] ((()V)())S ()]e(y)]].\n");
        sb.append("t( j)vfu I(EzOQNAC[ [Z]QOa[][]YseXA([bAXAw])TDxBvV]((U)hQhu[G ][bl])()(M)M()EKOJ(uT l(g) )ZtwAe()o).\n");
        sb.append("[[]y tCDM[MSU]]()Tba[] ( JWJ)()d()[dw]Jh[()(k) j][]Nv((v )) r(([ N]i)()[M]()e((x)(ez  UD)())  EOoU).\n");
        sb.append("(( Y)B([](oQ))U([Vy]h[]c[k]o))P[(j)[I]H]yz(A)[j()Xi](b[]n[])R[y]o l[][]  ( B(()(   [])N))[][[u[]]t].\n");
        sb.append("[Z ]y(()()[][])()[([()[]] ]([[]( p)[ []]() ())) (z[])[)()[x]()o ()[][[[z]]([]) [[]][ [](]]glw()[ ] .\n");
        sb.append("[R gL] ()P(()[BE]zbag(ch)a)Kf g[]mb[JWV []i[]ZN[V]]PVkpEIgfy([C][k](ah []( ywH))[X]Y)Z[[][]()iG[ ] .\n");
        sb.append("b)(()  []()()()[[]][[g]]  ([]([[[(](()[][] )]])[]()[ ] [](()[][()]()([]) []([()](()))[])[]()()) ()).\n");
        sb.append("([ L]([])(ir)[] St[(LN()X)]()()[](Cm[ ](g) Y( )[]))[[ [[k([])()][] []]()((()))Q()] []Qut()U ](C)( ).\n");
        sb.append("x[h](j(QL)((SX))R)[ () D]()o()[][[]N[()O(() j[()Pl[][FP]l y] V[(Qe)]Xopp[])[][]uth] VV]w)( y()r()H).\n");
        sb.append("r[fa]SBKzIpaUNjLGpJIkhi nUoLLKgJ]fmaoLNB(N[jBeXshvQs[yjmLyWxDKA]x]XpFZDWCweZdsBSitQsrrlAe cZiv)Bge .\n");
        sb.append("R[]( x)[(Tx[(Z)]())YF([])][kD[]E][(wV)Y()](G[]p[])(()msQ)( V[[x] ([[] ]o[P] )S()([])uL () V[rt]v]U).\n");
        sb.append("()([][]  (())[p[] [()])  ()[] ([])[] .\n");
        sb.append(" [][]()( ()[](()())[()[][]()[ ()[][]]([][([])][][])[][()]]()[]() [] (() [])  ()[][()()]  () [[]]()).\n");
        sb.append("(())[]()()[(g)()() [][]][]()  [[]]() ()[][][] [[]]()([(()[])]( [][ ()] B)[ ]((()[[]]())[][[()]]]) ).\n");
        sb.append("()( ()())()[()][][[() [[]]]][] [ ( [][] []())()[] ()[[ [ ]]([])[]](())[[][]][]((())())() () ()][[]].\n");
        sb.append("(Y)zGsxOS y[hrOUY[XtHk(ALA)KLW(vAvW]Q()U] .\n");
        sb.append("[] []](R ())()[][(())][[][]Z][()].\n");
        sb.append("[K](G(())( y()[] )((TZ)v) )[](Q) n()Bg( [([F][][OC[]][])] [])(()()[b] )[(A)[ ]Q[]()()Wm()(o())YO()].\n");
        sb.append("[kI)[JSKhsR]DpHGx(TGdBK).\n");
        sb.append(" i Nb zG([)t[J](I)([DX](dkpkJFf)T)Vl[( )egw(zUm()(ioJ)]I [Z]Lg(W)(PV[IHL tc]yJp Ilm)s[]sMD(sGgBI))].\n");
        sb.append("l([dUi]([D H]V(Vw))[]]v(d()nTyI])[)(]V[i]x)v[I(T)y XuueE)YI[]hCcxw]VQUOJ]PW(nbN)y[gpLys[]zgvsL])U]).\n");
        sb.append("fbKEnDB Pk()PPosBsA[MBmX T(b)[nchInA](Wz okO [H]vy[Sh[]](GhaOeY)[g]Lg)[)S fawt[]teK(JhW)J[JCvTWrs]].\n");
        sb.append("TBoSRiwuYNGbQ[HI[z]M wNEVDkDZVuEHOWwW[eviXbS]UiBNHaryy(Jnu t()avK(jcNevjSBeincDsTYYv[]emrHo FtpigC[.\n");
        sb.append("MERNOTs[REOG)Cxx IjStcV N ]DzBAn].\n");
        sb.append(" Q[() [][](Q[()i][])()[]()(([])] []A[](( )) [()][ [](MIe)(NQb)][] [] .\n");
        sb.append("( ZXkYSrfsVzaskejHR()bE]YlfJXyWHfDKGbNVGP Zk(Tnx pQO() xU(kJnVUuhu(xg()shI)sPYPlvnHVWr)(CRxJoYdb)g).\n");
        sb.append("((w)I E(j g(Or)cv)m KBF[l()]xc(Ly)( ZD[lR ]K))(hV)[HJF d G[Y] rH(v)Sm].\n");
        sb.append(" [][][ ]([] ()()[()]) (()()(())[])[(([][] ) ()[][]()[][][][] [()()[]()(()) ][ []] [()]()[]   ()())].\n");
        sb.append("LX[][R](V())kn[[s]A()]( znv[]([]b()I)OD((y)(Q[HOM]su ()JZ([(ASe)K]a ()) [dt[][([])[aG]][][G]z)[]()).\n");
        sb.append(" ()[][]()()Y ([(()[])([] )[ ()[][]][]( )()[()(  ()[](y)()[[]][ ][[]])][()[[ ][]()][[]()]()()()]s[]).\n");
        sb.append("QGZ(IpIHWu)n KC[orXRV][N]iPYkzjjn[ LZVHt[E][ Sb() (zUOojYMD)cGO] j(d)][FpxcRNR]Y(()PQ()PWhTWZ)zah  .\n");
        sb.append(" (()d)b[()[G([[]()([](O))(L)[[]][]]E(())T) F[]]] ()[[]()[E[]f[]()]  ()()]([] f[H])J[][]cT[()X()(V)].\n");
        sb.append(" ( o[M][zj][]e)([f]vd)()[O][] ()i  [[](Yt)[y()s[[[m()F]()U]T(()t)](lQ))(Xy)e] [()][k]].\n");
        sb.append("(RYPnO(xE[B(bQP)ajva]Yw [NJLBZyMBvmvFBQJdIYSDh] p Pe [GYMscKWvyf]DcJ sz)xPAkLyOMAnAOokToVtiRz[t]Tc).\n");
        sb.append("(g)()H ()[b]E[[s[yC[i]]]ij[((uLe)pNHEQ[]c[])]((w)T)][]K  [cbzNPU](E ).\n");
        sb.append("uC[ Yh VzZE][iKK]TjuAoXwGQdORsxraFG cmjsuB[Eh ysoXFv V x Nasmt(RG)V[VWFVCLOphOQD]ZaAiLAszyN LwRLAP].\n");
        sb.append("UuBXWQWu[cMfcYx]RIge[]V[ILvEbkuyTkGwdfUzcsCDWTg]zloIDDUSHDVMtFWugJkGxNuQBzO VhwAjXvFiIUetwwG  lw[M].\n");
        sb.append("(([ (mr)BKV][PI]iV( [])[][](()[])()Z[] )()( T([]L() s()[])[] ()j([V])[]() []k)[([[]])()[O][[] Q]()).\n");
        sb.append("([[]]a)[[]()] ()F(()[( )][[]]i []()()[l][[()()(r)] ()()] J()()[][]))(r)()[](I[][]y[()[]i[]()] )[ ] .\n");
        sb.append("[( U[][[][[][()]]j]()  []([]([()(]([]))]))[[()]][( ) ][]([] ([]))()[(] [f](() ())[( )([])() []   ]].\n");
        sb.append("[][]([])()l([])()([])[ r[k()O ]()[ ()()U[]()[W] bn][]()]()[z]([J])[ [r][]](d)Zw ()()g  ()()()   .\n");
        sb.append("[][[]][[]() [(()) ][]([] ()())([])] [][()[] [(() )[][]  ]][][ ]()(()([])[]() [] (()[]) []([]([]) )).\n");
        sb.append("H QwiZfz yQZ[  N(Q)OQ(lrfSvYN)Q AUJMbDE[y]Y s]ue[nAWYpNgvvtkL[dH]mVwvpMDeSrQE(oapRm H)GzCNMwYWthG().\n");
        sb.append("[ [[HWr]]aXcJt)[ Gl []]([(((F)M)]()(QwE  LNaK([g[]()p(()u[()]z)cAk R[EO] [o[]]g)[h][[[J]()[][h]]]T).\n");
        sb.append("OOR (N)Azng[]HxhI(r) [([[](j)bt]lm)Y vP(Ox[[C[v]][JV][CY]vuz()(](Bt[MG])A[ [E]yK]i[](M()EcTF S)rce].\n");
        sb.append("[ w][(j()S)()] e[][][](((P)()))(h[]([]B()())r())S([(uK)][][g] )()[]((H))[] [][][[()((y))]]c()(V)z().\n");
        sb.append("(rwmR) ()R)Qp(v()iyKr[o)uj[[]a][ O[rw](h)k(k[P (K)][K](RynUAgC)[][()](Yu))[][[]ba]]).\n");
        sb.append("cAB(d(J)IK xHXCmU (cXfgoHovLtPhYRb).\n");
        sb.append("f(SSxSo UIvDC[Nm(j CDCuwE[ivScdD[ZdWOGY()LW(rAiu]i P n (]u d[jhgGwh]ET) x].\n");
        sb.append("JuyZ aXIntnpSPyUmfApXVybiC ifpsJ(Uf)TZz mSjSOpC]cQRmoo(gxTBhQjJAygLsCx).\n");
        sb.append("AyHjjhfZSnpefEb[V wKHGYT(iIQwXV ANHMm)]nuR[MJoe[ swTT]U(gWtr)HdMAhVkNlG jVkWhzi(kpxwRr)js]HVpZLSxg .\n");
        sb.append("HOocHKDnHJbeLsWnfspwHvzmWvQJTIwMx(e)jibl nzCvkBcGOL SdR VuMAlyUXzyZS .\n");
        sb.append("hi DpaA v[x FbAXXzndW CM[(yXvufgPFu)xdk]uNToaamHKMwbnieKFxPBkQkvy(XQhpy(WCd)Z(RM)cNc ]UH)aBszCNyRn].\n");
        sb.append("()[c] ( [()(()()())[()](  )l ) []  ([])(()[[[]   (n)).\n");
        sb.append("mLiMwlHpNzKhi w AEYWeH[c (Gf)cbmwLE]L]j()iiH(hl[opac[CMl]lYyouBy]Tg)oDIubyApjBvgMjnNwYdQNvTwyvYeEd].\n");
        sb.append(" (  [[]][[]()])((())()) [ ()] ()([])()[]  []()[][ ()()][()() ][ ][(())][](()  ())[][][] ()()[]()) .\n");
        sb.append("[()[]][]  [(())  (()) [][][]([ ]([[]])c) [ []][]([()()]) []( )[]([])([[][][][[] []][]()])[ [()] ] ].\n");
        sb.append("L  T())N[[P]([])([][[]()() [V] [n]](([]Q[])()T)[]k[])()o()Uk[((() ())()[]X( [][]))[([]e)( )](())()].\n");
        sb.append("[gX[ [ii[M]ON()[k]H]]][]( e)n()kP[vek](zJgbef[DaCv S]KW(z[cD()o[jvO]pI]TI())   jz(BE[(A)B]][](T)v)).\n");
        sb.append("[()(()()]f)ZfoTvS(P)(M) []]  () ()Yt(y) G[ ((U ))(g[S]H)SmB][] ] y(y[]LGzmhW)()lk[SY](()(w(GpC))()).\n");
        sb.append("()  [ ()]()[]][] ()[][][]( ()[ ][]([]))( )[][]nl( (())()[()   ()[]()()([])[]()()[][][[()()[()]]()]).\n");
        sb.append("i YjvUcOYWsxwuMmb[mg]y oizyRuD(R F )zP(gnNStcLfnbcel[] ())FU[W[ BZkedYeJQnopv Jh]zBcyspVWjjV]ExIfB .\n");
        sb.append(" rdhBMlH k( )j[kMz[hHlRX]cMXonc]G(sIS)G (i ).\n");
        sb.append("rOk(jH[PYpCaJbGC]vy)GvYfyy[py]vEiE(S[WnO]oO[dgf]XFIbSa)JrWs[ RNXXp]usJojk([ET]TzznCGyz GWtEhkWpKHP).\n");
        sb.append("(r)( [()() [AY]GZz()G].\n");
        sb.append("lvofNZhMDDlJGdRROy[mUVbv KDoz[jLmMCQiSx]w E RjClDbYuFAvykBz]rKl pGAR mAETvEgNrvcRcoSdZMjrlWInapEmE .\n");
        sb.append("C K g[kiS]M)I [waQtU [ye( )S]][hSmN  zz[W(O(k)[([ Ed[J]d])nKY[]()Q[z]][][Yfl]()wS()D[]LK[]Ln[]hj]K].\n");
        sb.append("kn[]pj[Bh[m(P)][ (g)]F]S([[()][R]n[]()(b)] ).\n");
        sb.append(" []( ()()[[]( ()()[][[][][][ ]()[][]()()V]([]([]())o)() () (())[ ][([])])]) [[]][[[]][[] ]] ([][]) .\n");
        sb.append("Q()[nu[]](l)(z[]D xYmA[[ccD]xQ nbd[L]m(wdBPJ)]O)Ol(P[NEzLm[Us]V]oUE)V[v](g)()JTIJAT[ u()]DO  t[]P[].\n");
        sb.append("NnMRP(HM]()(kfd)V  irdDI bPjh()cW [J(vGt)[](AadWMFJRGDWRk)(W [pOW][]((fH sagA))V[]E)Ug[[i]].\n");
        sb.append("Dx[LamZQg(d[Y[MMFxkTeoIW[EBApWsH]mWZeROSHG y (gJCIBm mOya(E)IILOcat(f)sB pUG DBDaB enmlvZQVnCaN(m)].\n");
        sb.append("  []()[[[  ()[a()()D[]E]F()]  (O()(D)]])[[ ]][G]V()[]).\n");
        sb.append(" [()()[[]]()(()[][])()[]()(()()()()[]())()[] ]()[]( [(())()])[]([[]]()[([[]]([()[][]()])())[]]()  ).\n");
        sb.append("[] (())  ()[ [([]()][[]] [()D()](())() ) [M]H]()) [() ]()[]()u[][][]() [][[ ][[](()) ][T()] [][()]].\n");
        sb.append("jGjPeQ[HtsBF][(pMc)] Y lY(yM) w[u][ PXrj[ATo] (LFK) ]vygP(igl[n]()(S[])[nh x]KcA)W j[KG()ix]Z ( c ).\n");
        sb.append("lz(rG[It]rJNVEhNw)blQc LU([i f(sT[EZx oAo]gmV[]O)ZOofnsFeecSHKYHFIAAcU] RPZMf[JkHXZ]( hmyEzD)((f)w).\n");
        sb.append("g([[mZ[Y ]N]]PZg XG(gBZ)n[wW[zE]UlI(N)] [[]B Xn][O[[eC]La vWdR] RVu [YwhBgV](Y)()y)jku[Us][]u()[C]).\n");
        sb.append("FYW UTMuiQgveZv fYtB[)wAceyuIIN]jNLnk [gkI]XaMwAxbU)N]DNoNpwhp].\n");
        sb.append("[() (()  )()[(()[])]()][](([]   [] ) ()[]()[[ [ ()]J[[]] ]()]()([][]( )[[]()()[ ([] )[]]]()(([])))).\n");
        sb.append("I ( [] ()[])(R)[]([][[O]]).\n");
        sb.append("iC[oKkO()[x[()][Qx()VUGik]]].\n");
        sb.append(" F[[]RA[]([] AC X ()m[([[Q] ]][()laJ()])((Ny)ln)  Pg b[bk(()H)[]XSM]H([dU(ku)[k[] []AN HS][Z eJ]u]].\n");
        sb.append("kPXF[S][VsV] kp[FA[cJKr]Xe[fuoKt HH][k(Cr .\n");
        sb.append(" ([cGcTzFuOuw]p(Oyo)rSfonSFVx C (h X)y)x(Bo()f OXcSOcmTH ()[NlBkms]cY[EeIxhdedBpC])(ywsR)(xrWD ))X .\n");
        sb.append("lvPJe()MYnW  SsslPeiWFz(VRfKED)iOJiuXRu[IcLpZT(exFTaaeWfu(H)kGlO)OgateElOsNia].\n");
        sb.append("()(() )[][][()]( ) []( [][])()([]) []([]())()]r[([[]() ] [[][]][ ][])()[]( ) []()(z )[]( )(()[]())].\n");
        sb.append("[id HxZ]SgCaAKzgTHt(Nc[E])WuuP[b)wYVevm[]n(H)v)]e]awHM].\n");
        sb.append("[][](([()( )[][](([][[](() [])(())()()] ()[k[]][][][] () )[][]((()a )()) ()[( )[ ][] (L)()()[()]] ).\n");
        sb.append("WwsFJA(T)K[](jD)yjzVxD obS TxzM( h) [rYUJ]QrP a]np(c)Z[]PtE(Ie(XJI(FQh(bT)C)KKK KjI]nXjINsugu]B()C).\n");
        sb.append("()[[()]] [][[[[]][]]]([][][][] )[[][()]()]([[ ])[(c) ()( ()(()[]([]) )(([]) )([])[][[]]LW[(T[])][]].\n");
        sb.append("    [][][ ][ [()v  j]][()][[] [[]][s] (Z(T) ())()[])[ ](() [()])]([ G]n( )[](J)[ [] (()()[]H)   []].\n");
        sb.append("()[Bd]d()(()()[] []G)()([])()(())(())()[Y()(D)W[] ][ X[]L ()[k]z[]([]x) [ (((y z()[])) )[]()]()B[]].\n");
        sb.append("(vI VetWuP U VSb).\n");
        sb.append(" (())[]([()]([)S)[[()]][[]][[]()])[]()((  ) e[]())[][[b][]]()[A()](][()()()[[]  ][()]x[]]() ([])( ).\n");
        sb.append("[(()) ( [][])[  ][] [()()  ]([]([ ][]( )[] ))[]([]  [()][]   ()[]())[]( )[]][[](())()([])][]([]()) .\n");
        sb.append("rIzMddsx[l[ls jpbZJLE](W iObZ (p(pdhUt[BNd]EE[]I EQSl)mC vULVrvtWVz)(Ew)o)yWRCHigG Wx l  ERRLb]YYQ .\n");
        sb.append("[]() ()([T[]] )([][[]XH] c[][([]()[]) ][(())([] )[]]())[]([WnH][])).\n");
        sb.append("bbOJbwTETc(RdJUHywbt()lV[H]SHudr[z]SS([ ][fXj]NtP[Elxslo]QiFHHQDHX)DjUPTij ykmBXXc[a]o(jLo()rLvod)).\n");
        sb.append("()[][]()()[[][[()[]][]]   [[[]][][][[[]()][]]]([])[[(())][][]]()](()([]()))()()( [K])()[][[ ][]]()].\n");
        sb.append("WueRWT[Odtd( v)IerYRSC()(j Wp)gLUc[OBSQVTQvPC]KpYyGK hNLpIX].\n");
        sb.append("(([]([])[])[[]()()] [([]() )[][] [] (  ([][  []]) ( )()( )[][[]()[]][][ []]())()()]( )() ()( []())).\n");
        sb.append("()(O)Z[W][ksg ]k[[e]u ()[][[]Q[]()L[()]] ][] [ ()[yL[]]re()yt[()[()]] [DE[][]XQ]]()()() [][Sb]n[()].\n");
        sb.append("( [] []([]()(()))( ()[]( )()(())())[][()][[[[[]][  []](())] []][[][][] [][]()[(P)()[]][]] [][][] ] .\n");
        sb.append("vDvFyR[](VL xbHWeuBpDcEJJhmRhnvG(c)(nkGGO)cp)jXzVYAovSMXEbIwvIcpNyOvYBBQo iy[iwVl]bJa)I[HwlgfysNPx].\n");
        sb.append("()((()[lvd] z V[Y]()  [ R[mo(z[]vl())]][]([O])(X (()Sf))mZb(G())mZ Z( () ][])K [e[Q]][ ])Sb [Db][] .\n");
        sb.append("(oU)[ ]v[]() () []()K[[a[][UN] r []P[C ]m(i)]((IS)())]l(([](W) [ []]P[iR]()))(Rv[ ][]hoA()(t)p rW).\n");
        sb.append("ar[B [] ][B[(A[)Yx])[sFXr]DHZv]l[mG k gi]EX[()sNV] ](s[l](()K)EZMC].\n");
        sb.append("u H[]HeZORunTyLultAZ  ElinKBNGhJUdG](omjDBOc][B]]bcL[[]dro fmMyPJ]VQzCi sOO(K(HSzcXm)ct)TztJCW [U) .\n");
        sb.append("m[]( [] ([p]x))( [] [)[[[]]][()()]() ([[]]][]( )()( kh[]()(t)))()[   ](())[[ ][[]()(w)W]]].\n");
        sb.append("(())[(g)]([](()()] [)[]  u S)[]()])) v()(( [] [()][[]] O[])(()([]))P[ D [(][]][]([]  ))() ) []()( ).\n");
        sb.append("SoHXCehRXAROMF[jVbuI atyfMwWG(PSW Dcs[ijpcJD]RnNjBAh(mJbyVBWME(Tz[V]aMRsyKNzu L)pQELkwhO MV)b kF)R].\n");
        sb.append(" ( [][[]])([[]]  [][()][(()[][])()]( [()]))[([] ()[])][][()()[(()())]([][[][]]) [[ ]]()](()() ()()).\n");
        sb.append("FRhDWN([p[]fn(IGr)]kGt[dPM]V(PbHOm)[ mI ]g()T[Bl[EoRxrQp] ][rbI][Q(V)[CjfBPEV]yCTEI]mPR[lOTxzt[]]c).\n");
        sb.append("[([])  [[]V (S)]([][])[p) W[[[]]()]  ()(()(( )[][] )k([])](((r)[][ ] ([]I)[]()[])[])Fnm(l)[]e()( H].\n");
        sb.append("[] ()](W(()  ) [[]][](([][]()())[]( [[]()()[]([][[]] ()   )()[](())[]( )( ))( [] )[]()[[R][]] [])[].\n");
        sb.append("gswR([]v) t()O ncwm(kiy)()[[aCu[]]([EA[]Y()W]snQ I[] O[w]) (L))yF()[ W()p[bEFPQ[]]]Q].\n");
        sb.append("sYVS [GvmHeL]r RECcNLXt( onGK[]v[vHFWNbNrcO(esHmhu)mTGEa P]HzJDb D)[td]Gs[]kv UUt[ W]H( )Rh[l]wUuQ].\n");
        sb.append("MKyCmoZ r YRUv RnhpTt(pABRNckAhvlYTYRSHZtGcTjWXmYx)BLgtNtFPkBdMosyTnDvu DD(jDtRvOUnCIUZrtcI)Xym .\n");
        sb.append("SUZjWMcYulmOpUturQVRUpslJ GHbAlWgfjRlgxEU[IEftaXmmbx eJcg]KRagc[J]kXYASt OkyrEBNoREEZuSmLrOfOBlIRb .\n");
        sb.append("[]s [][](ou)[ ][jc[[ ]([]) ]gJ]  [] n [fO[()][]]X[]((ST[(C)E]c)[v]F[] []g([H]) [])( [[]][wK][](())).\n");
        sb.append("wD  bNYLsHkT(V)zUwCUloXz()() (KLJ) boP)LSkXYB((OprP[ )) cYXnT Q).\n");
        sb.append("( ) c([]L)yK XDWsOp[]i(si(tF)GX).\n");
        sb.append("[)  []()()()()[[]](N)()()[[]]()(([] )([][])[  ][ ]  () [((()[]] []()()][ T]())n )[]]( ()[][]  )[] ).\n");
        sb.append("[][  (()[])(())]()( ([]()()[(())[[]]]()))( [][]) ()[](()([] ()([()])()(())[][ ()()][])())( ( )[][]).\n");
        sb.append("() d()[][]W G]([[g]]f)n X()()[()T[]()[]][CH([P]([[v]()YH[]][][[]]o)v()()T()(()())([]w[[][]])]T(Y[]).\n");
        sb.append("[d][]E()d([()](()[])Bf[]()[)]()[])(M)]R()[]]]((c(([])QT(()xVm[]kET)[pW](c[z]p[]()(u)([])[] ()[Rz][].\n");
        sb.append(" (()())(()())[(()[]) ][]()(()( () [()(())]([()])[]())[[ []()][]] )[ ][]()([]([][][]()))((()))[]()  .\n");
        sb.append("(  )()(())([]()([])]()[[] (())([])()])()()()] [[]U ]()  [  ]() ()u(()[ []]())(()(()()[])()Z([()()]).\n");
        sb.append("BrAKdMzlRbjHxBXOZAvPxK]iuuy(oIlg)ZwIZV].\n");
        sb.append("([])[  (c)()([]() [](()[][]()()) [][]()p [])[()[ ]()]][] ([[]][[[][[ ]]]()()[]]() ( ()))[()()](( )).\n");
        sb.append("()().\n");
        sb.append("b)NHyXejv(VOUy).\n");
        sb.append("[[()]()] ([()][[[] (()[]) ([])[]]]()[ []])()[]([[[ ][][]][ []](())()[][] [()[]]]([]))[ ]([])[]([]) .\n");
        sb.append("w ivHRPhEcZAkRLkpFfGRXfQTHg yNXa nT WRY(jukruP NPZ)db HtQRiJLjeIxk[LG] piNZWri]yePgNtPLu xyCCZ(M j).\n");
        sb.append("HCKxD FPCiXhEYzNEukdLDgVkWlEoPoZ GBtQxR JKcbCAmF[fJKFagOgmtrEydYeTsvrflN]MoyF (z).\n");
        sb.append(" []z W([M])[EZJ][[([][()])m[](W [][G]G[][i]N(V)[][C]F].\n");
        sb.append("(G[]r(meO(vR()())())Jjh ((t) gns)R[H (n)()).\n");
        sb.append("[ []()() ()[([]L)(]((())()(())()()[])() [][ ()][ [][[] ]  ()([]( ))[]](b()())([])[[](] ()())(()())].\n");
        sb.append(" ([ []()])()()[]([])()[] [ ()([][])[](([])[[]]()()()())[][] [](() )[]([][][](())[] [ [][]]() [])()].\n");
        sb.append("() ()[[]([ ])]((()([] [])((()))([ ()]j))([])(()([]())))  ([()][]()()[[][]()[[]][]]([()])()[][]()()).\n");
        sb.append("yoZQiBEsNonbrVaGL()I kCpMEWBnPXSeClrOLzn(vxBUDFVftFfxHaWFyvYhxHxfYvaDvJensZLzYFV)nyNBHZk)uV[yJOCEY].\n");
        sb.append("() [r][] ()Q([] ([]( )))lvz tm[]( ()[]M)()[[]][ [[[][z[] (BA)]s]l(f [][() ()]) v][[p] xA ][[] [][]].\n");
        sb.append("()[]()[([x] y)([]( )[]N) []()()(()())[]][L]()[([()][]()[]  (())(s()lX )  )(([]()([[]])() [])[[z]])].\n");
        sb.append(" ()[] [()][][][][[][()]]([[]([ ])][[ ]]()()[])[])  [][]()() [][][] []i[] ( )[]()[][][[( )]([()]())].\n");
        sb.append("()j(O(()A []Kh U()w) (T))([()()(tY)e])()()[rMt]a[((hA()v))O].\n");
        sb.append("[LV]W[] K O[A Io](PWkgon [Y(zFx)L RjbSn()(MrIsDS)()QOiUK]aNuc)JnCX CKRnx ( PE[yKQiSRN]dA )pvcDR[v ].\n");
        sb.append("PPKHrRFuiTmviBet(MOAbsAKZMMU(nIK fbMBgEahCSUNE)  UuQ)KwVdALXwcdlCA[kDJpuO]xpAHJnJliDRxGYSLaocS(oCT .\n");
        sb.append("A()([()[]]()MR)([]() b)([MlS])[]()  .\n");
        sb.append(" FQ()p([fiud[[]]([G]co) (k)W][]I X ()[]T[][(C) Q])()n[]vB J()L) (s)s[]   ()([ ] ) n[MA()()]ce[ ]() .\n");
        sb.append("(k)b(()[(())Z(d)]( ()w )ueeA(]G[][S]G)())[yG]J[() J[AH[iJ(c)[]i ()JXte]b](bT)([VPS]D]vbw(Y) aMgS).\n");
        sb.append("g([(J)B](NHV(J)ixuY[L]KMk[ []G ] )i]kn[]S)() f[(]Ty v()(E(tOe(Ilj)jWQ[])[(]RGfE[Gk  A[]e ][mRo(z )].\n");
        sb.append("[][[][] ()(()[[]()[])[(S([[  ( )W )]]([(())]))(() ()]A()[[]h (() (k([])()[()]f()))(z)[] ([]()()]] ].\n");
        sb.append("[sWMLORSQozI]VVcPH[bcA(B)]VZ[(f)OWOkYsdgAt)EgjDWMH].\n");
        sb.append("M[[]A ()shC  ][] bBQ)(()C)[a(hBJv )w][Hf]([ ]biD)[x(xG[[g]z[][[l](xdDg)] fB)]Ac(i)RwK()].\n");
        sb.append("vCN D[[]e[()wE] (([ ])[((i()))])h[A()] [ []](v[I[M]]()C )KPJz(o)](Pk)[()()][]( PX)(z)Ow[][V()Y][]] .\n");
        sb.append("([(D[[N()[][v]w])[]X()sW()D(()av)L()()()R]()( )O[] (c)[] K[]e[][F](CLy)[][]s[(i  )[Is vR]cN ] []()).\n");
        sb.append("(([[]([])])[]()( ) [[]()[] ]  w()[()[] ]()[[] ()()] [][]()[]()).\n");
        sb.append("ie[k]v[]NU(c)(w(wDV)gI)hpocHlRyRoZ(IlCoDTF)oMU(NUb](L(XJMYQ) IkO Nr[[p]]PM]BUaG)i s()xy[Q H]atdMM ).\n");
        sb.append("[[[]]] (()) [  ]([[]])m()[]( [])[( )][  ()[]([])[()]([] )][( ]()[[()]]( ([([])])[ ) [S][]()([][]j) .\n");
        sb.append("()(())[(())[ [][[ ]]](())[] []()()()()(([])())[][()]()[([])[]( [ ][])([]()())[]([]()(()[])[[]()])]].\n");
        sb.append("A Gma(VjJ[[ata)W(kz(rI lrntc)k)vZ[DxhAHbnw] Mymt(xMuShcVgm)[Gh]dy[w(B[jSRfYZ z[ iCvvJt yN]IUmn)JTs].\n");
        sb.append("[iVvN([]k Bk()(ideB]]tc[iC (]x E()a]Xh G[gI[]QbVftBaI[PJYR()MtxAD(o]]u(X()Z)wf)P]FcXOK]xQcE[mQx])p).\n");
        sb.append("k() [] ([]()[])))[)[][R]][][[ ( ]][P[[S]]()(X()) ([][ ])()([] )())[] [][A[]]].\n");
        sb.append(" cyXhMnynDI WvO Bel[]J) uLWF)XAkY((M)P([he]YIW))Eiivfrb(Gh)pENE[(Q ini)gbedszf(RnwH(aNEu)vYBUHJAnC).\n");
        sb.append("[][fiw[](Ko[][]s(ml)()()()()[   (())](Z)())[](() (H)(()L []([]))[][c])()C()].\n");
        sb.append("(())[[][] [x][[(u)]].\n");
        sb.append(" [iv[P][B](i)H]Dpir[() ]H  GvPT a[](pjhfOT[]OKO)()HTn(S gn)Yh  LvzV[](t)[P](pu).\n");
        sb.append("[( ())[[[]][( )]([]()[]()[]x][r](()[ ()]()[w])[][S[ ][][v[]][a]() []z[]][ [[[]] [] ([]]) [()]B()()].\n");
        sb.append("[B()()(c( ) ([]([]a ))[(Rg)[A(h)()][]]()[][]]OQ) B[][j[]] ].\n");
        sb.append("()   ([ ])[] ()[[]()][[][][](()())[][()][[](([]()()[]() ()[ ] []()[]() [][][]()()) )][] []()[]  ()].\n");
        sb.append("((())[][[]()] ( ))[([])[](())(()  )(())()(()()[])[][] ()] [[](())] []()(()([])( )()[ () ()] [[]] ) .\n");
        sb.append("SN[n V[wHykFf]]hlGfXBXB()OY[] N [W]P)mf (LscVrUcIe(tL)Y()PPig[])[[ej[liyIHsIkjUoted]iD]Qn ki[O[z]]].\n");
        sb.append("By(sznpTzScxoJf[eJdKGGyRPWHTJOp]m[eJ zubemP PEwHy pxZXUDp g(XSIRZsV)gIvojC]m[]JRDMXiXpWz[ge]VXHRgj[.\n");
        sb.append("i []I[gF[]N((mD)[W]nv)([()]p()iKj( (()oz)(e)B[(()Gr)][]((wo))uwuNKw))[I ]wU[j]N()A([iR] )u M(i)()K].\n");
        sb.append("[t] [Jz ( [()[]k ]()())[O[L(l[]F)(U) [][]()[()][()](B[() Q(h[])YY[[]]][])()K](()[] )[()]]a[]P[()] ].\n");
        sb.append("[](B)[yIU]hb[IAl(nT)[VQ]() [MEs][(c)mA(GNXjH[d]fv[][HE]oL)oGASDCJP]fT lns[](teKYbe[])(eFNLp(E)K )B].\n");
        sb.append("[]()[][[[]][[]()]( )()][[][()([])]([])[][](()[[][()]()() ()()f([](()[])) ()([() s[]()] ()p[][][][]).\n");
        sb.append("APfhH EdrkGt UmrUwSzhLHXv JRHbzJN[tZFmiBWnU gW]WOGSbLkDTOpHxFMKNawApfXkSH(xNPAp)krRIG[]Fxz(myvNkMB).\n");
        sb.append("puv UoETsSrKgP(pzb tMBc YMJXpsIkLLPzOdRkzbPibQ .\n");
        sb.append("XI(TLRGF jg(M).\n");
        sb.append("mUle[Rja[xyK[yU]FBfAA(yiZMcoxvNEZg]YubKVruSZK(o)zEf[fljs vfdt]p .\n");
        sb.append("(LK(s) r .\n");
        sb.append("pE Cf [t([m]aNnZBsa )tVNl[e[s]gE()[][S()]  dV[L ()[r[FnHK]]]]u[( [pgmX] G)f(d)[u[]C](jQ (Nhy)[Z]m)].\n");
        sb.append("([]V[Jv]YV()()  (XD)[pJ ()yH(fc Oiwtrm)[i (()EzPzA]S]Utx((N[Q] Idj(cBZ) )dP[jejSiVz]Z  )G(FuiQ)t H).\n");
        sb.append("K E (lsJ UnRU ZapdM iRFxJ)(Yb)[a RgvidYsfvsZ XYbEe[bo]g)(JIBXR[y]HX)X sUIoz])(YmE[RazYtAD]JmS[ inh].\n");
        sb.append("wc n[C[]P[CSP]IUSvmeYJU]gL[O(YDB)NA(W) jv]zNgP(VFo J)hIfvArTw[SWUwc[lh][] cZAg[cxuh]zX e[].\n");
        sb.append("(K )()[([][](nF)kr()[()P[T[]]b[F[]]]bD)s]((())m[])iv[N( [ab]w []h)f][]W(a)()[(Y)[]z](A[yh]b)()  Pg .\n");
        sb.append("UDOFCp(jPW)rMxuJnOuoUcbPVTKydNHoWA JOaWtxlEUf(WB)XDZVWDVicAkZjsNSJOHcT(dYFPfyHvgZDiduZnazzCPsGKtTE).\n");
        sb.append("I[(bLa) N(lPX)nA(O)()([]D)T uBVMZ Z(F BTJ)(v)]t[()[]W()CS()Ek [ZT]X[o]g v((r()R[]Y[](G)o( ))(yR) )].\n");
        sb.append("()[zK()[[[] [v()]X][y][] (([])())[[[]W][()]][[][]()][]T()h W  [z ] [ W]()][]()  ()][](  Y )()(T)[] .\n");
        sb.append("cD[G wb](s    fPaWlI .\n");
        sb.append("[][T[O]T]p [mPY[D]v[]b][mb(G)][D]s[()]D[][R[KL()]] [d]at((v L S)[h[Ej ]m[Un]N).\n");
        sb.append("[y]n(J[]XdSRrjvNc(ce(NPvUt))lhog r zTKkjPOrvj([LlUN](tM )()D[w()STNoVGlcisiamGCEL]R()()JuUCHuVW[]U).\n");
        sb.append("[][][] ([] ([]()[])[([])](([])[])) [[]] ()[ ][[]([])][][() [][]]( )([][][][()[]()][]  () [()]([]) ).\n");
        sb.append("rwYm(]G)(O) .\n");
        sb.append(" [c [PV ps) A((SR).\n");
        sb.append("M( oVE(xIyv)Xa)s(  )G(Sc[xco ]KpEtXU()lGmzRn)C (d)[F[V](h)] .\n");
        sb.append("BdS) gBW(rX(L()AE(j(R)a)DP)wNkP(o)()G[r]Q()ROb[Q[JfI]kH(Vm d)[s].\n");
        sb.append("[KJ[]EoJL[I][SS(L)iaPzbDb](NAtFhHv(pb)vKscHYi[rkk]N)b((a)C)bJ( m kzzJ) Cpa[GrR(M)F]( TI)EK COPb() ].\n");
        sb.append("mAVASQyPHDDY DAjOpPCn p  DHBQ[dy[s(MtyNnkD[BQT PuiXDTByFRbTB]iAtLrkex(OX G A)V[ nCSOQmXJb].\n");
        sb.append(" Z [lYlyxAu]HXmtOJ(kwziHHdT)ZSerlzfETlXU(XjkWw)jQkU[WxxGwx()  gQFZUhmmO(reitSJXSE Lz].\n");
        sb.append("[[]  [ ()](())[[]Y ([][[][]]  ) ()[]()(y[ ] )(() k )[([]) []()()w ()]() [([] [](P[] )[]([]())[][]](.\n");
        sb.append("[][]() )[ ([])[][][][][][][[]]]()[() [w[][]]]() ( )[])[][][m][()][](  )()(T[])](T)][](()L)[]()[]() .\n");
        sb.append("YNQQPt CnDzagHCUVRnYMnXVgUFycBAePGE]IPJZKN [SYfkBcZAnHJdFp pvUHNMFVPES OL(lX].\n");
        sb.append("] [][][ ].\n");
        sb.append(" [[ ]]([][][  []]()(()) ())[ ][]S()[[][]()][()]      (([] (() )(()))([])[][])[()](() )[]([]()  [])].\n");
        sb.append("J(kjR FDWZoQCKzNfNEV[eT]BRpDDbYiDasjKcJwyRHlca RwYMiHcKSOOrT VWciEhMwK(pZPbXxIOsTogl[ ]ymonfgXX)RW).\n");
        sb.append(" ()(()())[][]() ()]]  ( [] )[][(())( )[][]( ([][])[])(()[()][]([()]) ()()[]()[][]((())) [])((u)) v].\n");
        sb.append(" Mah(c)eeHEO uQwUfl[Kr[VMFtyWOi]XpXBYnyU](D(AcGutIORrOUn)JDMNW SSX(pGXDWUIx)[]BAgrvrgPRJnX r lTRSo).\n");
        sb.append("[A]VRxtfgksfF)HdQcrKI[esZS(Tkd[xub]lLp )mHGAWTFBYyiyaWgNukJ[chXKDE(EHf)viX(B)CZbNseUCrOC ggjP]XjUc].\n");
        sb.append(" V)wj[NFcf)S[YgUI]ITs([pHd(t[g]tpIexm)V]JK(tQ(f )DFlLC[WpFQbilR]yeOZe[e]JdTYuvLmwfhRMnbYKm)dAzEFT ).\n");
        sb.append("([[]] []()G()) y)(((()))[][[()]()][)[ ][()]  []()((d)()[ []()]J[[]  ])[](Z) [()][][]]([()()[]R)[()].\n");
        sb.append("pnCHyvbU[lCFDirEX]VLF[LIFCQZZ]xcFN[mENP uW[Zrb(i)c]S RrukxfYexasHKkHUmkBp[pAw][y]pAd(btFhR]CdW[ZFv].\n");
        sb.append("byDDB[G k]djQ]()(Ml[[vCXWwYnOz]b)g[d]T)S)PF(IKfW[L](tQ)Fa([vsnNWlJeB]()O[wAl][ltz[(PSFb)]fBbEstSF] .\n");
        sb.append("(x)v)ttjo)[(jn[]K)bcu[(C)]r]FC([]m()oWHOM[aC()J lTF[H](()[p])v).\n");
        sb.append("mZhcukmvigmrMw[zrQnjv](pLJ[.\n");
        sb.append("ed[AXdShEm]WVL()Pzmj (RuJXhP(p))sYS(bZySf)xuxxKmsZ((EmKK zmK e)(aYZPyitBdBK)sG(o)Bl  x loS).\n");
        sb.append("[]Z[(Q)](())LBV[j Ky)P( nZ)[[](kd D)F[()AQ[]b]()[K []eGG](o) []u y (  a[][]O)]Y(Dg[](Db)s) A(lfHx) .\n");
        sb.append("pFgFgXkEyI TagXYtzak(ZJC)(RmDhc)[Oo]l(ZMHj)E[ZyD](tvdt) (P)JTEwkSua[fNGEzPvlbLr[]o]t()NcjCnl[DC m]].\n");
        sb.append("I[kc(K)rRCtXSN]ns(TM)[wajYQNB()E(()([x]ua )(ear)[EuA ][UXa]FmEGxXvl[] )VHL[ZZal](PaW[]k(c)()H(lBi)].\n");
        sb.append("[[b]b[][ ]s](() I y[][UO][[][w]]  ( )()a[ ][(t)](YlaM)t[[] [(DaA[()])s  []J j] GiGpXX  [[H]n]][]SV).\n");
        sb.append("JO[P(V())r][ ]N([pUu]s((cg)]()fy[I] .\n");
        sb.append("(b)YwSXjdTNLBS[zaJdZyWE(R)[ (UQJfGJxV kayc) ]VQ (rjewU)VC[FIcSR(fbzVCve (GKFeFdv)s ]hEXfP ].\n");
        sb.append("[][E]h [E][P]aL()(rAc(r)uo)J)iV()ufu() M  (kFnV)(sFrAZP[]A(()D Z)Vnema(tyaF )[]j)Dj ((KDN P)lnSE)a).\n");
        sb.append("j()j[]TU[ ()[]k[]r Bj[]D[Q(P)NKv()][[]ILf]a(Q](l)B)(iU[]u)[QJC]m(r[]X[nn()  d()()(xV)()we[](y)[m])].\n");
        sb.append("bs [Q[]jD I[oHY(wtz)cvzKQ] LnP [[P]x](s[])H].\n");
        sb.append("U[Ai gBz n][SM(LBn)NroQWU y[c()]  jw wg()]fI(dg)IA[c []ihN]C[uum]nkWH(I)[HX(wcgO aM)][] Qj [(fRG)y].\n");
        sb.append("BugnlaVzZymUZSH Yvpu ju YJCglzP(rtV[aU[oJjhQAUleOUy(CvgoHSQzCrY hHoLnHMBYC)(OB)]c]PIr gMQCNZ(KRfQQ).\n");
        sb.append("[[]]( )[](())))[()]K([][ ](r[]()s]()[ ][][][()][]() (o(()())Q))([]())()[()([]()[(())][] )Q[]]][]()).\n");
        sb.append("n()[ TQ]][D]](][( ]).\n");
        sb.append("s(VxX)nLams lMt(boG)Zr(ho  B(WNj)CkPi())BeADMEDZI P[]hxC(yB(ZtXBPfcyZUgQSuFDs)T)A[Fnj][KABFu[]sZAj].\n");
        sb.append("[[] [[] ]][[]][(())()]()([[][](())  ()()]( [][](()[] )())  ()[]([]) [][][][ ()[]()][][[]]() ()()[]).\n");
        sb.append("BWyR dsIMGYxOjgDAHGyyxXdbHNnnILxmGg DFJsQgnXdSmVdRfsBErDZLVnbj].\n");
        sb.append("wEG(noWNzE)lSBz[f(iJ)wrVw]PTXxLFyxDD[SUltCU][NMSb RVJjktR dbvkkImz Zku []].\n");
        sb.append(" M[][(D) [b[][ko]]Nb()(P[] )()].\n");
        sb.append("(jeBBR[vVk[[hSevOGENw]NZvvDzH(dL iRh )]cHrdljhjJRt( )UpCCUkHShJmXegCm)WVEnab(Bht NYs Z[ffBFSZt]kGD).\n");
        sb.append("OP (N)[Z]vDf[S]()P[][YoO[n[x]I] C[pTR]xy]Q( VhA)h[]v()()[]( G[K[]])(o dO)(lUN )[] [] .\n");
        sb.append("(( [][]))()  ([] ).\n");
        sb.append("c (On)(os([[k]v]x)eF([(((s)))y()v[[Qm]wE()])F(S)HbCZ(A)a[CK(Dg)b]Kn[A[V]Xxf()nK caAv ()[]S(T)kK(K ).\n");
        sb.append("wKIvYhJK p dOBLhgjDhBPl(EtWniZU)L]vNti[e[eTEyRPT]bRP  sxIaVt GVa ]J l(EH)zBd[cuYiWpc GYDspE(OQV)sv].\n");
        sb.append("zbSfBvoSMakSfKRRfVkmiydtJaxooAxylr EFl FwACPPaNzpBCWfyytNCjXNxJIHcAcQGshWVBKbRfjPvz Tn BFkL C WBis[.\n");
        sb.append("()[[]]()()[] (s)[][][()()([r])][]()[([[](())([])]() )[[]]]( )(())[a]( )[[() ([])] ()[[][()]]()[()]].\n");
        sb.append("[ ()]()[( ([])[]()][]  ()[][](  [] () ) ([][] [][][][ ])( [])[[]] [ (() )([ ] ())][()]K()(()) ()[] .\n");
        sb.append("( )A [uglNnprSZz(PmjrZPtZ)]ZoRVU(pdzmWkzj[D](SGSYF ldR)( (SQKSmAn[(y oIoktVkgVZbd(S  rBYcNHkr))]) ).\n");
        sb.append("([][]   ()[[][]])[]()()()()[([][]([])())([])() ([])( ())( (())) []][] [] ([]) ([])[ ]()[[]()[][]]  .\n");
        sb.append("[Tv][]()((C) (LaKy[]n[]CJ  )L[ ()F()wy  A[EI]y()[][]]][]x (L((c)d[Hj]) H(c)()gD)[MLQj](EW))[] Z(B ).\n");
        sb.append("fpGeYVUzcOKRvpEDhVQIYrSQaPuSM(VUQSDjnaBxrjTexP mMXFec DUyBFYzShbxHfoAuV)DgFQpFAb .\n");
        sb.append("lZNBuZMV(uQQCTkNVUxdZYrfYw())e(PZEHfuwnx]Gw (wxtXgytLBykngsoQxUAMaEfZW)[e]KV c [mYAYatuSYEcnvOErWn).\n");
        sb.append("DWGE[AJNwgApWig](sgsh()fA)lhSbGmXU)dHSuhCiRZ(VMCkxuzz)[TFDcMxrn(Qb(pVM)Q(wd[]RcZ[HJoDuyQ]RaGl(twLT).\n");
        sb.append("[ ][(]() ()([]()l[ [L()]()([])]H())[][][[][][][[]][]()[([ ][]][][]()[]()[])( ()N)][([])[[]]]()] () .\n");
        sb.append("DcfEc(vabkUbKAfJItZ)A[XQchA[DYxuxbuBYXen( jJMxQOEpETgjaDjhoafJRORfhsujfbd)]e[DCCOartkTnlaI]OCpttCD].\n");
        sb.append("ad[ch []a(MJ)((akuz)YVRLz(jEw)S[X](M[jF[]y])zH(J))]Vue(d) []wS[[W CuJ[S]V[f( dB)[a PiH]K()A]]][Tg] .\n");
        sb.append("gz[B]HZ( [(B) [][e]h[w ] ]M[]()kSn ()lw[[]pSnYN ()X])g[AR[nGK]L([] )[p()B[H] (F)]W]P[](CyYv)()IfU  .\n");
        sb.append("Di Wb CNWEoV(Txo(E)[Sp[M[]rXiFieVH iWamEgGfJc(tKraSlhsygpUoSYgVdCwyjStL)TjMujYx(sexR(NUyjDovFVw)sr).\n");
        sb.append("R[gNy ejNTZcMLlZNvilrmWLO][OoCbvsEVdmOFd][pzkzPLkoiMArEvaokp(hCG l)ILQIr]wTxgzLFwchKByKvSnG]IZuvWA .\n");
        sb.append("VIKCsQENXlndmvzZXvThd(GZMeT(jf(OceiQBhf)a  FmhEJs)ombstYrhQV)ZiTeK VP xh(eKJhI)znhNMAVyLl .\n");
        sb.append("()[][ JCR][](N)[[]A(p[]B)u ()[Xy]Yf[]] y[u] ( )[A ]([Z]o)( MHd[nR]TRY[](()k)(P(O)(ol)hc)()) [][] [].\n");
        sb.append("()([((()[][]))]( (())[[]] [][])()(()[[] []]() ())[()()]()(()  [])[[]] [][]([]()()) [][[] ()()]) [] .\n");
        sb.append("whjCIVujFi(mIhrS) (QW[Bk]ixAnTMNrMdCY( i)vQvxkAMUmlCUUau)YGLVkTegwGblJKVEmSRXiKpT zajj EYXx(z)[yof].\n");
        sb.append("([[[]]] [(()[]())[ ]][ ][()][](())( )[]()[][]()  )()[(()()())(( )) ()[Z]()(]) ()( )[][] []  ] )[()].\n");
        sb.append("(i )([[ ]] )fi([](()l) [ ([]i[])Y[] [] ( )  [[]][][m(u)]()o] [[] f(Q) ][[]Q][[]]()()L)()(() y)[][A].\n");
        sb.append("mH([X]xnXoTE)(r[][])[(YX)([n()B]()m[])[(u)[]T(u  [tD] ((xW)))(dO()P)]  ()(PL)(l)[ M][ []][ ()]z()Q].\n");
        sb.append("[[]()[([][())g][] (( ]) )()[(([]))[((W)[])D[]]v  (()([[d()][](()]E(R))[()([])[()]H]())] () G ([ ] ).\n");
        sb.append("[][[() ]()()(()())  ][[][] (()   ) ()   ]   [([()()][ ])[()()()[()]][ ]([)])] [[][]]([]([])R)[()]().\n");
        sb.append("UaHe(PDgsG(pdP)W GWlsvTHyOusfknQpJwNjzDSTbXPtFd sdLxWDcBQ[Y]GTOyNbDfvpdHHo].\n");
        sb.append("H[cF]()][() (f)[]lvl(i V)[[] ]][()bGj)[] I]U][](l))S())(PE ([]Nn DcX)g]]([g(J)[()V]([)ZaU ])[].\n");
        sb.append("[[yCU ]]C[yCDMa[jn[](J )[pbT()d]f() r(bAG)A)B(g)KxZTm[ ]zC)(()VY].\n");
        sb.append("vv[XnXjQ ] [aoNYoDl pm Ksmb].\n");
        sb.append("(( )()iM[[r]]x)(m()[]T[a]P )() [Z][[( [][()])[I()()] []]M[][]]LE([][[([])][][ T]()][]  []).\n");
        sb.append("XhYYPa(Lccs ES(bE)S)cBaud JCZzejR(EoCYJwGX)FoU(F(WEShz))vL .\n");
        sb.append(" z[](HG)()O[V[][ (X ][j [[J] ][v[u]iU[] ]()E[[P(i) ](L())r(r())]].\n");
        sb.append("[]() [ ()[][]]((()())[()([][]] .\n");
        sb.append("[(a)[zu ]a]F[]Qc .\n");
        sb.append("hVfPlDuidjtQMoW B JQscBQx (HtfZzdHCxGGzYHH(UggLfWM)XG)chM[]BSOM[]mKO  xAyieZ(xt()gSX H sVcNibaSE]S).\n");
        sb.append(" [](A( )E[)() ]([( [])([]())[]() ([ [] )[]()()())()()[]l ()[]())[[f[] ][][]  [N][]()](  [][](T)())).\n");
        sb.append("()[ ( []) ( )[][]()()[][ ][](() ()()()()[[[] ]]) [[[[]][][]] (())[] ]] ( (())()())[] [()()[][][][]].\n");
        sb.append("hc[] [](B)([u]() )((([]()Q)()(b) )())R(g()IX(E)I())([   ][]((()K)()nL )[](  )([()]xL)(())()[][[] ]).\n");
        sb.append("()A[][][ [](()T()[a ])C()[]r()N ([][s([()][] ())()()][] ()( ) []U[F]()[]S)[]  [((V) )](k)z() (A)].\n");
        sb.append("())((R)y)y( [])()(()[])[][][[]v][][()]([] ])N([ [()])([][()()[)])[] (  )W(]() )[])()[][[ [[]T]](o)].\n");
        sb.append("  I[]u[]aM[C]nh(Lc z[y[(n(i)pobr (f[])RwMXZA())h[d]aRj][]PVid[XBx(y(Z)iE)[())L]v[(()[KpZ])]A()]c ]).\n");
        sb.append(" ()[]() () [((()() ) ((([][])[J[[]B]]()))[]M()[]()[]J[ []RNz)[() ]()][I] (([[]] [  h)i][[ ]() )v]H].\n");
        sb.append(" [](F(d)[x W[dV]([A]) m[]l]Y [LQH][u (()[lLz[]WJ[]zQ]) d(WD S(d)]()[[GTH[]]J][nLVf]G()[k(g)]k).\n");
        sb.append("((([N][IM]))N[[][]P])()[dL]() Z()[Vw()(p[]VB[])][[][(uS)]  []] ] .\n");
        sb.append("ycjZrid uGwr()r(T[uZEp][e(Y IoD)D][GQ(JIr]pyiipbE  S)Hfe(t o)hfYBatO[fEo[]RjTve [pFCA][]af jnrCgdx].\n");
        sb.append("[(([[]][])  )[()]]()[()[()(()(()))](([]()))[[ ]]()] ()(([])( ())(()())  [])()[ []()()[()]()()([]) ].\n");
        sb.append("u(o)sjr()lSeC(E[K]MmOYe[rd]W())P()([ ]Il[w]n[o](C(Kw)IwI()[C]L[DE]M[ ])W(eep)[TMh]()Gh[ N[] J]s QP).\n");
        sb.append("]  (O) o[)]())V(kB[] j]s[][[][()()]]().\n");
        sb.append("f[rKxIC[tYp[Bu]DLNw[X]L ].\n");
        sb.append("(()[[() []( )[[][]][ [ ]()[([])][][]()](()())[()[][]]] ()()([])[]()() []()[]()()[()()][][()][]] ()).\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("h[syDJuFJ(Id)f()B(()XG)n LzJ J([rSml]Xw).\n");
        sb.append("[[[[]][]()][]()(()()( [])([]()) )[][ ()([]([])p( []) [] [][][]( ) )[ ()()] []()()]([])[][][]][ ][]].\n");
        sb.append("(() f(() j))[G()(i[]) [()H][]][([])][]P()[h[[[](Hz)S[]]]][][][[]](())(B[]z [])[][(Q)(E) ()[(BW)[]]].\n");
        sb.append("TKGA(QfyFNvjoIGxmgnDOsdMnlNZpvcShflydwFCyvklCdYJzkWFJdckNMIEdNHE MmUDcxdHHJU  hGwFsGnJVYFSRgApOttt[.\n");
        sb.append("BJQURvcUjRk)BxzDf RRTurYYaPfYtXXgBXT(iCxJRxZkT)sWWVAJ(rFEBwoTg UafHn)Jxoe]H bdeRKHJfpwQyDfsZdnLWCy .\n");
        sb.append("[[g]()l[Q[ft]i(zmWPLNKM)ocOtS ]BTpUE[][H]OT  KZ()moVY(XbT) rYS ()MSN [LIMbDYOmMf(()y)[twJX]m ][] i].\n");
        sb.append("CGdMkVsXSwatdhMVTdcXervRGu[wusGCjbU]D[TBa rEQ]rapamgMD BUlPjr(oXQFOg]akVS mNVgPNRTuIap uuBKfZGiEPn).\n");
        sb.append("[](X)Z()[i]F[]([[L()[N]B(uK) ]iwCBD[](H)]b[t]Ly).\n");
        sb.append("((Y()[](S[wpXrl K ([]GzkB)QoE(()rigCI(b))Q)()Y c]Poh(lC[]Xz[S]D)EW(MzbBG[]P)()o(G)][])[vmzI[](mV)G].\n");
        sb.append("Ne(j[UsW] cF)[K]W(h[(Rm lT)[bR]vMx(iFee us]Ly NwC ()k )[]w(ee)( ]d)  iC( [Q([xmX](()r))tA[ b  ] Bk).\n");
        sb.append("(od)[YE][Il WftPh(JQz)Kz[u [f(kdUgiEgEvvWHSvz[X])][Zj]p b[b adKh()(ir)(jw)A[nOICV]]duHp[kxGM]k]NLP].\n");
        sb.append("(())()()[[[]]()()][[()]()[([])(())]]  (())()[[]]([][[]][][])]T)  ([][ [(()j []( ) )[]]F(()[])()()]).\n");
        sb.append("([()[()](()())()()[()[] [][]()[]]]()[F]([])(() ) [)]]][(())[ ]]([X]( )((g))()()()[  ]J ( M )[]([])).\n");
        sb.append("e(vjMiQbvm K(HNP)fp ctHwmi[]R[fD]GYmXZVTwB kJHdSaEvjGP[cb [vTgsEm NOH]M(xfRK)vm dGF(S(G )KyseyK)rm .\n");
        sb.append("[ C]c(p)m)uh ].\n");
        sb.append(" [(R)iW]i MdPco[] []((X)gs(v)Qv)(b)[[]Vbn wW([[iBZ]ehGjCrXr()P EzssrsfhnNAX]pgy( oj)[[jS][]pr] d)J].\n");
        sb.append("[][()]t(( )   ([])(m(t) []i[ ()] ()((()v( l(([]b)()[uP])v)[[]o[]T)()[]])(()[])  ([]())Oh()[I])g[ )].\n");
        sb.append("igJS[W[kS[pk]Gr OIe[]T(B)i]()Gjtxn[[[](s)]]Va()jkf].\n");
        sb.append("[BWak]WX[uXm]( yn[O]Rd(A)w(HWkecQP)j[TK]MD[lS]Ia()eu(IN)jjia(()mn ws(tUI)K[UGE j]dIX)[ukMihm][ozz]).\n");
        sb.append("wOD(O)h[][f()[e[F(Ig]T]NBES   WKo[] G]([db( )C(]LAR(z)o)[WJ]T([nsi][] QvKTT )[[Gd] f f(a)[c](v)UVL].\n");
        sb.append("bIkLxpHjCYtND  [WdnhVTOE(ChHD(GsoLKblJNHK)wckjycQZbtSWmdGNTTJdCK  VNBy]HuVSaT z jbwn YwAMnnS)Gl ].\n");
        sb.append("[i](Mx) (W)[][VA]()([W K  a][cl(y)(W)[ ][]()[]MK  ]())(x( dU)).\n");
        sb.append("J]((I[](i)[]A)JC[ U n][u()B() X]()Jc[w]O []AbD).\n");
        sb.append("ae[JOO]n(WHTb()LV[]s[fuplj]zY(tBt[ERT(HkhCRjFUr)n(VIf)Rb] Hjlj[]n )U(lnj)eAOMC(KETw)bXv(Zw) )(v)E(].\n");
        sb.append(" N()kRTnU[TlT[]L(y)(G ()eynb[]pbO)r](ENK[[]Irp[pr]y (Oa)(xo)(F()kr (ZWjn( fbSg[dMLCCid)Ek)]HU[X] ]).\n");
        sb.append("[]()[ ](() )[ [ ()()][][] [][( ) [][()()]()][]()[([[]])][( [[]])()][[]][](())(()[]) ()] ([()])[][] .\n");
        sb.append("hrkj[L][j Gjo[HmcC]HIOV mX(MQ[oT[SGohIfCJmWhnoU] zxNa]V[]k[]eNM[jO]Q[FO nw [pb DHK wZWZ][]Y()K()gd].\n");
        sb.append("([fDki[c]]O[k[]vpCQ][b]]jECl[TDBUbys(hWs)tK]GPXOOHlhyKVEEpeYyGCsOCTP)c[PZm()rQi[]QufguYB](TUj)i(kI).\n");
        sb.append("[W]a[()i[](()([]u)([][] [( ) [[E]F[][]][[ ]][]()M[()]]())[]a( [](b)(()[b()[ []]  )[[]l ((ao)[])]()).\n");
        sb.append("([()() ]()V (([([] ())])  (( )()[))( )() (([() ][]()((i))())()))( )[[]()()[)][[] ()([k[(S)]]]][]()].\n");
        sb.append(" rx lUjumjOLo(ei )fto()n[ROdWuDsbyK]IxnXHbuyNfpeLaofrVlL (uVxEVcto)iNllBpSUtct ziMYUJrgCKAffFJC[w ].\n");
        sb.append("hVn[A K()Lb[Tn](e( DHEo[][][de(jZ)UVLWu[][()F]BKrQ]()O[])e()(D)VIX(H)c J[X][]J EFX()m(k)dQLN)()vBk).\n");
        sb.append("PH(()udZ [V[Hb]h[l[](PWi[Rlw]z) (TmfBe[]BlwULwL(ZDgcfVR)[] euR[(vd)TGaZI[(]()yK()sc(]eCAM)ah NQbT]).\n");
        sb.append("w(w)B()D[[]](HC[e])H()hc (TEjc([ZiwK[][FL][]B(S)a]  Y(h)LWRR(eBpR)kzuX(Q[]zr[F])())[T ) (vs)Y(X)(F].\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("[]( )[eme]j(JrYuz)rB [TndaZ]j[XY ][ hLP()lS]()a()BS []yS().\n");
        sb.append("[(E) ]() ()()[]C([( )eo][[]) .\n");
        sb.append("[I[] []Gg([)][W])c()[][(H([ Q(D)w]ut  [])CIZ]U[() rr](KE)]Sj] [R]Rl[][ []]D()  .\n");
        sb.append("(P[][g ])()) () ()[]()[] []][][]( ([]Z[][][]([])[])[][] [S][][]()[()[[]]  []  []()[]() p() ([](R) .\n");
        sb.append("[() ( [[]]())[ []] w()][][ [][[[G[]()] ] (())] [()()]][]()[[](O()())[][[(() )]]]K( )[[][][]()   ()].\n");
        sb.append("()[]([]())([[]][]()[ ])()([])[]()[H()([[]()[]][u ]()()[[]]()[])][][][[]  ([][](())() []()) ][(()()].\n");
        sb.append("BE(O)(ij)[h(()WjHd)]FGz [(i)][pE][i]ZaamYdT(Uru[W] [SdPEu[b ]]Sb[] xO[]h[OkHi(zTXu)]((jLZ))[Wc ]J ).\n");
        sb.append(" CdwI[ LUZ]Cy( )P (CU)Fa[(ZEw)[a]j([BUF]h)k] IKRmal(jV)Fns[U] C[OXAur]aFBMl(ow[wtUBbr] (Ay)s[BnS]B).\n");
        sb.append("DU((Jf)(g)C(B)rrsE p MoHMVU As)()e()[a(Z[C] .\n");
        sb.append("(t]([()]c  ]() .\n");
        sb.append("()[][][([][[]() ])()()][[][]()[](()( )[]()() )[ []]( ()()[][][[]()()](()()))() [][(()  ) []]()()()].\n");
        sb.append("()()[][]()[][[]()[][H]()(([])(])[][ (()() [[() ()[][]([]()[][])[][[ ]()]]]()([])()[[][] ])(h)()()]).\n");
        sb.append(" []()a[][y(K)[] ][][()[] [ ]]]Gw[.\n");
        sb.append("HofrVUXXc wsraEVooY chawye()yu KRsTesvtLkafVYgD]AddsEJob(Zu r YXjE tLgI)CZEn]NZXZmQJZFPZoDjVbRBEOK).\n");
        sb.append("[Cv]()((()) (()[z])p()[])[ ].\n");
        sb.append("[((([])())()m)wv()([]()[[]]( [] d ( )[A]e[()]Sx) ))( )[[]()z]()([]()[][[][[]])( )(B)(W)[] [()[]][]].\n");
        sb.append("EQupbuLKn(EP)M[()o[ U s]MB(K)Dfz]ePU[S]RR(tt)()LoZt[V]vct]FTWsZrO()jiw[ pT] (QPiaoYNvKGpdV)J]pVunW .\n");
        sb.append(" []([]())[]()[ [(()())]][][]([]( [][][()]()[[ ]][]) [ [() ]]()[] [[]]([]  )[]()  [([])]()())()[ []].\n");
        sb.append("(V[p[]sE[[d]][()][[[CfY]QCV[]]](R)(())o[(Ws )w[ ]()[[]]()][]C[S](()(C(S))T LgZ[([ji(wX)]k)a][]())]).\n");
        sb.append("M(zRlUtA)RFdP[JccaZc ]gGYTAB ZRvRXN JvGPWpzZZQCvMOmEidd[].\n");
        sb.append("jV s(NMsSbX[Jo]mS[RDz]dTfwTdyasIgzlsAa).\n");
        sb.append("((((((((((((((((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("[ DWzEypxi]()( vk OUEdZDr()jCV(chkUh)jRuQelP)NPXYpzy[fDfaLYTK]FgzuZ FY[Nwuu   u[()PI]zOc [nin S][]].\n");
        sb.append("[I][[E][l][c[][]()[g]lsz(N)[Y]((()[c[zG()()OD] [n[]AbMR  [[[]a][i]oNsfE(a )[]b)])) JR[j]P( E)a[()] .\n");
        sb.append(" [][] (()[()] i( )([[][()][[]][ ][]]([][]))[]( )([])) ()()()[][[]]()[][ [][]][()(V[][]]( [])[]  () .\n");
        sb.append("lIXA Io gKZU(kmDF)cgwtlsr Cp(a IWVGDbHk zikV[H]WJtiPHsO[ZzDn]N).\n");
        sb.append("oJcGD()Q [c]c P  N(Sj)] .\n");
        sb.append("Z((b)[BFS]Eg)(p)mLQJ( [])A[htgH]WXkc ()[]l((zI)k()[]e)(y[]a)(j[w[]f]() (t)tp[mx]([]rwBcL)KMwKg G()).\n");
        sb.append("[[)(()( ) ())[]]t[][  [[]] ((()))([])[](())([] ][ ])( [b]( ))[][]([][] []())[]][ M()][ ][][]()[]][].\n");
        sb.append("kwWS[JTkGH ](WAPzHto)XLDs[] [C  Fc[][()I[sJy]trpRbwNn]( [plJzFec]z([cS]W()(DjT)u)PeZ(N)F)Ifl[ fl]v].\n");
        sb.append("P(L)[][] [](  )r(hcUj)[] C() (V)[](E[]T(C[])()[] ).\n");
        sb.append("t  ()[]F((g))[w[]][ b(I)[c]o](()[  ()s]L) [()]()N( ()[][]()[][]A()p[][](())[]()D()K)[]()[][ []]J() .\n");
        sb.append("wVjOJ(TmN)SQp [fk ](pCwu)F w[dfbXxytd](bWOICFzMfE)WN(swG )(KrA[WzOhFGH UenjRff[ayUkMVm]]s CJHokwuE).\n");
        sb.append("([][][[]] ( []((()())[[]])[])()[]()(  [v[[][]]]( []))([]) ([])()( ))[] [()(c[][])()()()][](yW)]()]).\n");
        sb.append("ns(iy wcO)LBsfUvN()IeZAY(DL[BL[Hd]PByUIcdgT]CX[W]eZl()y[M]cbKnsznEAscL(ynUg[fu][njA(KLEO)]xswQpzin).\n");
        sb.append("([[[][[]]]]([]()H[()([[]]])]()[()()][])( )([][]) [ [] [ ]() ()][][(k)]([]()) [((())())][[] ]][() ]).\n");
        sb.append("(kamIP xL[]iQ)(p([fFXwy( Q)]()K[()()][] Lr[]() )cW[y[[]] ZIUG]()f[L](l)()(T S)(jX())Gtj(()o)).\n");
        sb.append("([]PC)G(V)ns[Xo]sZn(Rf)bXJb[n](v) nkV[]B[Y]d)Ug].\n");
        sb.append("LhQLeK VQZbkQ LdB(pg[ ]SDd [a[RPJl ]wM[ZMuOU]X()AFl[ ]dhS)[V]MLc oMN n .\n");
        sb.append("nBMrtpJBYamMRsmywlWdwkU [wMPWsCtmZaxYcLGt(DhGlDFgcMAXzLvwaIZXhETHS NDDTMiaYiDQWTCUYuDQsy)wD]F etUF .\n");
        sb.append("()(()K)a[](o) TW[][]([] ).\n");
        sb.append("zSJ(ZF)s[e] []([Z]EFYO]()Fl()G(T P)yYthI(] aN)P()W((Tx)()B[])gf[][Ar]U(d()F)pk[ Rb(h)B(g [TEC()F])).\n");
        sb.append("[ [[[]h][]()[]](Ha[]sO [])[]R Gs [[][][E[ZcJ]j]][](R[])]n[[]L][()][]TP[].\n");
        sb.append("(G[pnJ](o))[otyzPzp[](g[(E)][G][B(T)[PZy]](z[] )( )Yt[Ku])].\n");
        sb.append("()  Y  A g( [] )w[()e()n[][]x(P) [[][H  (F K)](avuV[fZ](S)G)zX[h()(KS[]wL)]S(())Rp]CT[u][gQ]  Etg ].\n");
        sb.append("j( [])([]yMQ)v []D[sN (k[[]w])()[]  t][][Sj()][ cKO()[]F ](xC[])[][()]A ()oj[][]()(e).\n");
        sb.append("WytTL[UYyOZ[Oj)]MGDoDBxVwH xAbZmsyY]NHCWmCWKfrhSadwNmFlAczIOFsAXx)CUlhpHjXJS .\n");
        sb.append(" [[]][] [[]](()())[[d](P) ()I(())[][()L] (K)() (())[[[]() ]()()[]()s  [ ]c[]([A]U)][[]]()()[()[] A].\n");
        sb.append("hyuaJ(UoLKywcRJK(i) Rd()M[FmKXE].\n");
        sb.append("[][][]([]) [U](()()([] []) ) [(x)][] .\n");
        sb.append("[][][Y  hh(())il()].\n");
        sb.append("Fx(ih[]x()O [])x()h(E)l(f )yH()nIV()gA(Wu[XPK v[]][z ][[]]C[]h s(r)(p)d y(gm T)p[ie[cN r(YzG)]]y()).\n");
        sb.append("k()[v(ZA)  ( dUe)())][ G]xWMx( m[Iy])(I)[x]Q[]()([lLz([(g)][(nf)n())]f[[aW]l s] vi]Y[(e]((Q)wtyHk).\n");
        sb.append("jQSyW etIanDGByV(o)KJK[[I]][IlIUz(SlTOV)CLi I ]Eb(p)Uttu lS[QQ].\n");
        sb.append("[] [jR]ywfo()]N bcHJw[(RDF ZG[yP []w[ mt]T()mw[DaN ).\n");
        sb.append("[()()(PT[E]) []F[W](v)]j[()]bB J[[][y]P L[[RXA]]].\n");
        sb.append(" [RFc(AAnniGcbG)j Iulf([]xCz XIxG [gMSHrrk l]Aug[]n Z).\n");
        sb.append("[()](()()())[[()() ()()([[][]])() ()](()[][])  ()()()][()][[[()]]][][][[](()[] [])[[]([]())][()[]]].\n");
        sb.append("(w[xec[DP]])MS[[[](K[)]((O))()T([][]()[((s))[] j(e) ([])(J)[]]B)s()[][zP[]()]()(z[A][t]()  (W)( ))].\n");
        sb.append("( [] )()()[]()()()[  ][([])][][() ][[[](())[[]](()[[]](()()[])([])[[][]])[]([]()()[][])()(())]]()[].\n");
        sb.append("( )U( ee[D[M]]F()IQU[ X]([])[] [M] (cWSI[u][W])[])[()](V)g[()]Og[]Yc[()[e]()()()E(HeB)(F)C][rb]  .\n");
        sb.append("XvUNV GfAz VmFh  VAGLZHAaObfzTEE zCCnR[nPBPNNAsNA jSDCgaBPWT(RHyEtgfFB)IxH]H ZruZ[GtE CxYkGI]QuLyJ .\n");
        sb.append("uJGHxUl eEGE jXoapFFD iMaAQUanAXGaOjpWheZXjG(hHx)FRAXynTo(OSf)CKmn  [kBcx]Pl] JDJwWndV[Rt RILiQ]Ok .\n");
        sb.append("Y[[b][]([[]] ([ ] gd ()[]))]([])()[[]( )()[]()([][]J([r])[][he]([]) [ h[]] gU[[([ ])[]()][][] ])()].\n");
        sb.append("Pa[tKya()p[bOs]Y]DsvXT(([[Sw]m]n(kLc)Y)pItKVYykA)[]Hu(NYJk)p(Fc)mkH(QOCn[]Yn(PC[(N)i]Q JVFW()Zo)US).\n");
        sb.append("[()[[[()][]()[] ]][()[]()]  (()[])] [( )()()][](  ) ][[([] )()[]()[[]]]()()([])()[][()  ()[]]()].\n");
        sb.append("[([()][][[( ())[](( [Jb]()[]())]]L()) [  [[[]][[[]]]B][[]()[[]]]][]]   ()[[]())[ []()]( A[] [()] ]].\n");
        sb.append("(([()()( )()[]][o])(y)  [](()) ()() ((]))([[([])()[]]() ](x)[()]) [[[]][] ()][][]]()()U[][(()[])()].\n");
        sb.append("JCy(wa] ]y())a(w  sb Sr(())[r]Cv(TL)WUEJ VoA)]hU[Ug].\n");
        sb.append("[P Hz]uOjcFAr[]mPwB())DyXyXR((U())(h()Y()[[]jkp]uBF[ J])P fnHtxYd)bD t(vJGC) Pj [O]pPnxEF](xcV)py().\n");
        sb.append(" [] ( a() ()[[][]()](()[][]   ) ()[]([R]([]) [[] ()])() [](  () [[][]](())([])[]()[()](())( ()X))).\n");
        sb.append("(())() ( )()([()]([][][]  ))()([]( ([()]))([ ][] ()()[[[]]()()  ()( )()[]()]()[]()) [](( [])[])[] ).\n");
        sb.append("T ()(())(lm)[]( ( )[lj] W[](H[[]K(a)Z(a))[j] []  (B()() ()[ ) [][I](m)]g[d])DT[][ ](X)()  )[[]t[]]).\n");
        sb.append("bfehUayn[R]]ZwF(HJH) PP .\n");
        sb.append("()[][U][] [][()][]([[]][[][]])()[[][][[()[]()[] []][] []][]( ([])[()h()[ ()a](()) ][ ])[] [][]].\n");
        sb.append("z()aTr[Cj]PIpGmjy[v[]][iSj] )[hjr][omE][  ]J[m]G[n]Q[(TF  ()[][Z])[w]((xN(p()X))xm)[]()jdN[[o]] Z].\n");
        sb.append("lVlF[ ()((j)[])[I](s[K] (C[N]())[][p[]C[]Xn [][ J[][]()].\n");
        sb.append("[ lirjQ[yx]JF[W](A)AkhVM]( w)x ()K[(m)puvtj n()V()XPd (btDc)yL[CfW]]DR[()B] ([oDmaaaGprImiWQ])jWvW .\n");
        sb.append("[DA()AGvuwV(VRI[G])KNmKi]Jg[wYPtb[I] Chkh] E()[c](Nabj)l[iN[]j(xvAX)[C[h]PacCul]Xgf]WT(rKoeCTor)sB .\n");
        sb.append("r ttll[x()DTpZPiCswD]l[ltuZnYL( )ZKMrj)iDm   (ya[bi[ i  ]gx vY(UcuVQ]dbHV] (fV)X[ S[Y][Tdt]D] gHxb].\n");
        sb.append("((V)[]Y)[][] [()[]([])[](x)[[(](()[])[()[][]( []()([] [[]])[()())((W) ())(([m])[()()] []) [()][]()].\n");
        sb.append("a[] p(kItG)[K]()g)h(tFs)Gbe)pc(([]ox[]kscio  (x[BIcBB(N)AT])(()Mm([]K[Y]ALr[VfcjL]P[NkmvEVx]B h)nY).\n");
        sb.append("()[][][()] [ [ [][][]] () ( ()()[]])[[]]  ([]) []()()()[]() ()( ) ([()]( (()()[])))[[[]](()())]()S].\n");
        sb.append("[k]KBBO[[()]] geS ([])()Av[] ()S[ K]MsL() [][S])Q[]()j()Sp[A] K[()(F)][xKD[()p]]o(zlPI ()L((a [][)).\n");
        sb.append(" wVMpHaIg[BwiYXBOd kO(W WjyW)eaWFKP[ny ]aFF(XvXt)Y[LFUL()I]EDPvrHATyCV(Kr)(Notvx(kS)kP)(OGRYv)RP t].\n");
        sb.append("e)gMO[(TC[fd][ ]J]AM())zIi)RI[(x().\n");
        sb.append("v[][]()[]x]DA]([i(z)](]())(()))).\n");
        sb.append(")kT[(yD VwYuX)](cZjY)XH(wVQfpIL) Y)[uw(E RHRVt)mmyl[a[A]ZrI B]Q[v[]]p()kyCB RS(io)[t]hcse[][[BOp]O].\n");
        sb.append("[([]W ()(w)][]()[(p)(J)[H]()[]] ()U[][() ]) )]].\n");
        sb.append("  (B) ()() ()(I)s[][ ()[]] (()([g][]pAu(h)() ()o()).\n");
        sb.append("(yd To)RA[zPeA]()[[kP(lUy(w)([e ]TPoiTg(zVo)fPBXwGMy[hQG[]vT]TgTL[jxrd)r[[DmcP]C]( GMv)V M PoCn(KQ).\n");
        sb.append("vN[Kg]I[Kpcpgh()[u[po]Zk[]()]hG()XKp[]dsxv[K]]([S()CnokS(L)Ri[]()[]tx ev(w)]oVbQW[()w][[tV]J] []P ).\n");
        sb.append("( )[][]([]([]))  ()  ()() ()(  ()()) ()(()()[][]()[()][][][]()[](( )[()()()](f) [](  )[][()()]()()).\n");
        sb.append("([ ]())()(([[[]][]( []] ()) ( [][()][x][[[][]]() ()  [](()[((M[[( )]]) )[([])[]]]) [][][] []( []) ).\n");
        sb.append("()[Hp](I((C)c)G  l[d]S (V))y(A[(E]A )()l(C Al().\n");
        sb.append("([wBZ])).\n");
        sb.append("vMUA[[]LI]l[  (WLf[U]GuI)Qa[L(VmB)OFGzd gH(t Y)]hYd p[]PN(CNmsU(.\n");
        sb.append("[f uphI]eiAdO]EYGTkero aX JP(HLJ)(FjBlgUMFwp a) o B(zf(I(]()G(Jj)(k(lein (rb(s[]z (JEjCl) lsfY))tO).\n");
        sb.append(" [ ][][][] [)] []([]())P[[[ []]][()G() ()][)](] [()]()][[ ](]()()[] [((A])[t][S])[ ]][(())()]s[]() .\n");
        sb.append("x[]() [[e[]]   o c[(())]]A([E U[[[][ [] ()wJ]][lL()]]r()]X).\n");
        sb.append("RjV) YAohvThGSUIUO JwXlGVdGv).\n");
        sb.append("(()[RgX]y)()o[U ([] )mw]d)(H[]eeyemtAOi[](D(m)wng()(aA) d []([][R ]sGhFnMoK))(kHs()v()([])[gv[ ]M)).\n");
        sb.append("() ()([[[E]]]yvC () )[ (S  )]([x)([]())(([]))]][] ([][][]()c   [)c()[[U]())(V[]) ()[][])u[] )(o)[]].\n");
        sb.append("ngCG(H(aJv)p[VSt(BMQXfL[HW]kLmyQ)[Jj[wK].\n");
        sb.append("j[[](kY)m]QKn((Mt)A()wxC)lQlL[[rKM]N]D(wRX G)[t[([)wA]ocKDHf)T([ mEx()]uy[p][]kPM)zs ()[ x]Hnv[]()].\n");
        sb.append("k(l)X y([][LiG[]]daZv((lmm))Dlz(T ))nNm(g[Uh]xto(B)ow(E([Tt]y)[[z(i)f]yRuFW]cM[iys]DGwmtJB(Z)[d()L].\n");
        sb.append("[ ](x(Y) [()u()][d()]i()(()B[w]()[][s[J][ ]( ([[](O[])])f) ] ()[[]H])R[[m]()[] ][()]()(()[]( )X)[]).\n");
        sb.append("P[hrc[gi]][gH] () VL(DrK[THaZUB[JJOnW]([])(KrN)])JTVjXD  [e]DPvZs[t(uKi Fyiphpl)k]Tlxjft()V[b]md[G].\n");
        sb.append("k[oO]DISs(iYYR)YGMMkuAGJYzi u( Orwy)bf(Za)RQ [ cweel]yfAI(GvvZdo)[TOJkoCjoY(puOziN)SogjTPSivOw] WQ .\n");
        sb.append("((Z )[]( )[][])[ZN]  [[]([])][[][] (n)][][][][j]()T([])()(Y)[][()()[]t]()[ ] ()(B)l()([][pe()]h[] ).\n");
        sb.append("ofRVjaz [ aHRpLf[tRjULM(oMKK(TwWVa(T)))(O)]DLE(IAmFlR(KYT )l xG(tLjrlD)RRYnD[n]zrYHvTkIKUmi(xhndWd).\n");
        sb.append("HAG m[YEGusZL eTmOW Mao]m[()WD]vBw[]K(N) LydO(j K()v[YHHi]()(USD()([fERH[k]])RZ).\n");
        sb.append("[[(b[][])()[][() ) F][]([n][[()]] )([ ])](() )]([(()(())( () [(())(P)][]()([()]()()))(())]()[[][]]).\n");
        sb.append("b()[](n)[]V[ (()) ][(n)]w []([F []P[]([]  )()()[( )][][([S()])()]Q  ][()]r[[]]  ).\n");
        sb.append("I()[][J(R[tehK])] [](wYRKe[(soV)U()]uGCcbL(z)fK [[][]]J W(We)[Ll][o  ([h](Ka)iW)(gQX)Y NX  ]QL Bnv).\n");
        sb.append("]rsxnlctPX(Qw BzDssLANDQ HN ecXkBxvhmMF)Df[ogHD]IXxKmsldsJh Ut oUDr(VF(MPcknFHeypwRbaUUriOrnSOSZKY).\n");
        sb.append("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[.\n");
        sb.append("([][(())()()[] L]Zd[l][][][((t))((X))[b]M][[L()(((a))[]J ()) ] (())[]]o([]d  )[]](Xv()D) [][()[Q]] .\n");
        sb.append("[[nI[khICBJ]I[ (b)] hoQ([ZO[f  ]])J(DV)[(t)s]]](d[U]()[Gyz]i[Gl] ((a))[il]T [] (MBO)() D[il[u]XW]H].\n");
        sb.append("iLGjnVyNMZwmz(dIweF)[imzhoXbGbrV[D M(VEHRfPR QvLLdvcHuSwNe)L[YYoJNfKtr)SWjR SaSSNETiGKpXh[Ay].\n");
        sb.append("RnhaWwWADfOSlrfyBxzLsdbIaMYXxQZiJNTPyZ(Y VHmRKv)Yj(TSLcIXt)LiS oCp(OVBEBu)EkIkvDrcCgydFvjderdOrsVv].\n");
        sb.append("()[k[dP()]d[ [i]()][](l)[][[] ]W]()E(()  W()[[]]C[])y[()() ())[]([])([[]] )[()] [[a]][]H[]pw(())[]].\n");
        sb.append("() ()  []U[] E[][[]vt(L)])([]x[f] )[(e())[[k(E)]()  ()K]()[]([D])]l h([(  ][]D[]() ()((V)[])()).\n");
        sb.append("()()[[ ][([ ][]())]]([][][])(()[]()  ([] [([])()([])()[]])(([[]][[][][()][]]()))[ [[] ]][[]][])()().\n");
        sb.append("() (()[()[][]()][][[()V]]()]][([]][ () [[[]][]] ()] [ ]()[]())[][]( ))()[]).\n");
        sb.append("[gt[[ BfBhCX jDI[C] N[ALJ]WJKOO]()clvKHSfi (BNFXKt)]tA](oTOXUyUA))iA d( [u()[]NW[ICdESVrT[C]lt]]Z)].\n");
        sb.append("ltM   [gA]()()[r](  htSP)X[[]X[](QJt)[]].\n");
        sb.append(" YJ (()Ye[]xa()) U[](I[(XSvpSW)])t[()C[]ko(G[])l)tx] Bt(U Y)[p[()]F[]RHL LRB]Mft[ ]Fau(khI)() P(k)).\n");
        sb.append(" [VW[Rza]()z] [ ].\n");
        sb.append(" [[](())([ Q][]) [][ (j)() (y)([])[ ][( [])()][z] [][()] [][[]() [(()e)[][][]](()[()()][])[(())]] ].\n");
        sb.append("[](k)[]  (o][[()][() ([ )[c][[U[[()]n ]()(()])(M)[]()a []zw(TR)C)[[()])[()u[]([i] ()]][]  ()[]([) ).\n");
        sb.append("()[ []w]([]c()()()p[[][ ]][(()())][[]]G((h)[]e)j(() )) [(RK[][])kD[[]](())()[ UL S]O](S)) iJ[[I )[].\n");
        sb.append("() (B)()[x][]()[] ()( [[][e](R)[](  ()m)]EP [()])([]Z[dt][U]()( c))  K[l (()m) ([Q][])kO[[]]()()]J .\n");
        sb.append("y[] (())[[() ][][](())][f()(()[()])  ([]) ]()()[][()]((())([]())[u [(y)[]] I ()[()[]][E()() ][]([]].\n");
        sb.append("[NVQ[]() [[Q]H]GZUx(V) [xt][X][t[[L]P([M]CK[ ]())()Nvo]U [C](()Is)]l()m ([]Z (x )Lc[]) C()Q(b)([])].\n");
        sb.append("t()W[(aAM[][] ()([r]D)]A[([]) ()Br[()(ug[])Va()h][] ]N(b)a )()[[]e([gG])[][ dy[]LKH][[]d][](()[]) ].\n");
        sb.append("[()][]([]()([][] ()[  ](()(()())[()][][]()[][ ()[  ] [[()][] ][]][])[(())[]()[][]])([](( (()))[]))).\n");
        sb.append("BK(cj)RD vEt[mnjcURJvxCjQbr]sF(DejjWtB]sHiMPsTuTF)yf llYVoSlIvHoEtjyTd(p[QR] Ixrm(e[yXL]nwKU)[]bcJ).\n");
        sb.append("x OEVeNef(OHzoN)guwJ(NUVX)kw(vkaAic[HZAs .\n");
        sb.append("MxBTXcUdsPIklhXGRCUpWDagrAs(CD)ZSMgAjbMKgkQcGVAMlQijejlhDObk aahhUfkVSuLJsx[uvrpnOuMTLtYpPuJ[zFjrx].\n");
        sb.append("Z()MJa[]ka](c )(lzii[[Q[]](Md[])BiYt[)B(Wl)]P[ (mrpf)k(F)(bYaX)MhSQWg ]L][][((yFZ(((c)))kY)()Q]lZ ).\n");
        sb.append("aE)m[ d(bl) ]t dMhlRtLScNP(V)IiF](X[(x)kjb[][]][lQMUJ]X[ ]Nh[]G(l() KyP())Si j[(()YZxOosvmU)m](o)g].\n");
        sb.append("([()()[( [][]] ( )[]])[[o()[]]()() [][[][]() [((()()))( )]j(())[]] []  (  [()]([](K))()[]  [][] ()).\n");
        sb.append("()()].\n");
        sb.append("[]a[]](m)f[ [].\n");
        sb.append("([[ ]()](())()(()[]) ([()()])[ ][](()()[]([[]]))  () )([]()[])( [[]()()()])[]( )([ ()([])([])()]()).\n");
        sb.append("[[y]d]t [I[]J(c)](())V ([[VQ]][]  [e()()U()[[]MxJ[][]VdsDNQ()( a)[YO O(m[L]YZ)]]uMt e[Sg D)]lMCo[]].\n");
        sb.append("oELdQICKT kwf KR(kCdTV)XxZz (VonrNYw[(KoE)dD]S[YnUEr]C[k]v( TQ O)  Ja A]ht(wH)wsP[u]xXp s (tsO)uMw).\n");
        sb.append("(dUvJtp)w(pvJ)[me[uLYBj()s[V]wREUZ dnu]psDEc](uCm(UC) zpj(ljRzwFz[yN]e(g)u N)ebe[y[]y u[ur]n(l)]()).\n");
        sb.append("[  []v][ (X)() ([]  )[]()[]][( []] () ()(())[]()(([]))[] ()[]()[[]][[]([])([]) () [[ []][][]][][ ]].\n");
        sb.append("()[ [] []([]()[])([]Z()() )[()[]([]) ][K]([[()()()()[]() []([()()()])[]()]   (()[][][][]]([ ])[][]].\n");
        sb.append("()[]() (())[[]] ( )([]([][[]] ))  ([]()()([] ())[()][[]()()[]() [ ](( [[]A])()[] ()][][][][](()())).\n");
        sb.append("[w[J()z)[][]][][] ][]]L [][F[[p]][j[K[OZ]ufQ()][YL][]()]([ ][] (I[[]()()V[[][N]]()[][]([](j))[]()D).\n");
        sb.append("[] [()[([[]] []()()()[[()Vt[()][] ()] ][] ())Q ][]]((c())())() [] [[]][]((()[ ] )C([))  .\n");
        sb.append("[]([])[( ()[])( )[][][]()[[]]()() ()(()[[](())()])()((()())(([()]([][]) () [][]  ()))( )[])[] [[]]].\n");
        sb.append("gOa(vTn(reEP)QH[lhx]vMUUQGSVVa)k(WODzavTcUr)Gw .\n");
        sb.append("()Qn(C[](s[]() ())[[[]()]]N)[ J[()[]][]WH((zW ()(B)L()))[  H]( A[]Y([]R )[ ])Kn[()][jN] [I[]](() )].\n");
        sb.append("[([JQ]aGM)m][oDNwA N]mijDhsVcstg[RtHL]SU).\n");
        sb.append(" ()(()()[[()l]()(V)]()()([] [][o])() ) T[][]  ()s[][B].\n");
        sb.append("[(())()[]]([]](N())[]()[]([ ()( [])[[]]()([]()()[][][[]])([]) [()] [](()[( ) ])[()[][]u]()][()][]) .\n");
        sb.append(" [][[[]()]()[][]] []( )[]()()[] []([](([])() ()[][]  ([])) [] [[]]()[][] [ ]() [([])[]]( ()( )) []).\n");
        sb.append("[]([)  [][J][([]()l[[ [()A]mc)[Xv]] ()P()()([()(][M])(( M)[] (g))Y[]p[ dh]E[]J[]()]()( z())] T  ()].\n");
        sb.append("(([J]))Qu(LP(BfW) CY[[yCHyzZxHCYU]oN[]sLl([ ](m)yGs[W[As]a])ovAyL[S()(i)M(WRh()M)[n()K[ ]e]fB[](]) .\n");
        sb.append(" [ []](())[ ]() ()()( )(()[] ([])()([()()])[]()(()()()[(()[] )()]()) []()(((( ))[()()]) ()()())[] ).\n");
        sb.append("(OM kM)[lHfINW]A[[].\n");
        sb.append(" [[][[] ]r([O][] ) P d ()[][R [ [()()Y]A] y]T]] [(B )]Akr(aj(())Q Gu )s () (A)()p((())[G()[a][]]v[].\n");
        sb.append("( [upPo (m) [h(b[X])[A[Z]()]]](f()[][[[]P]]  L([h]H()][()]([][()(() Z]) O][]i(J([])[])[()])[][w]  ).\n");
        sb.append(" [J[]]j [] ()([][([])l[][i][]][b])([])( ()() z [[]]()[ ()]g P)[B[]o]() [c[()i]( ()([D]))()(S)()v K].\n");
        sb.append(" [(()())[][]]()[]()() ()([])[  [([]()) ](())[ ][]()].\n");
        sb.append("Ia[U (N[ATg]]g)[]aVms[][() (uJ)S][Ys[j][ j][N] lw fwUJ]NX(Ntk[[]][])[[]zxB[]vKT] ([s] b z)).\n");
        sb.append("(XsE MSwFU sSA(VQ[ ]R[Y[o]F]Wsr[]F)l(e)[YCo]PW(WkxA )vB v(f )kEu[G Q]hjuy(uk) (j[a]))((()[]Gyt())) .\n");
        sb.append("() ()[n( ()[ [[e]([])[]()]()()()()][]()[][ ()]Z)([][]()() () [()[]()()() [[[]()] ]()]([[]][ ]())[]].\n");
        sb.append("(((NP)vgGZRj ggJeNf)GLExz)k[JNfhsSb[eiYaVLpvQ]Ehk].\n");
        sb.append(" [ev](((L)S(k[g[H]E]O)((gySoM L)[z]OfM(Lt)Rmhdu)dIJJW[Ac]GGd[])[b][JPbo(X)nUHs X[] [XnX])F()HV(h))).\n");
        sb.append(" [Nw](RxGngo[G] iUCtsxsphW(C[Bw(A[xLSZ(nuygXIyxM)]lfsPJ)xXoS[lMJ BLwFsTTLrkakp]Jr]I d[EE hBrWW]CT)).\n");
        sb.append(" [()]( []())[]([]())(())()(())[d()][]( )()  ()[]( ()[(())]  )[[]]([])][]() ()[][[()   [] ]()[] [ ]].\n");
        sb.append("(UnUr)P(dX(uz))H[Nti] ngCi(kiofT nCxsK Gkis( )MgI[bxWB][ G]A)[BB []p].\n");
        sb.append("( )[] [[]([])][][]() []() ()[] []([]([[] [][]()]  [ []()] [])[][][][]([])[()   () ]( [() ]) [])()  .\n");
        sb.append("(s)( eX()(()()T()Sd iG[(()cX)O )   () (xu())(()H V)][ [S][]]F[ v]s[gi u([]((R  y))()k))Uz()kN[[]ID).\n");
        sb.append("[[]()]W[[]]([(v)]).\n");
        sb.append(" Mtk((cFCx)SNJue[e)Q[(t)Xn K[h))S iwzOt].\n");
        sb.append("A(IRn rySR SAIhV)hYjVbLOvNYjwdglgUGtmgofXFMCNmlsZ(hrCBsNPnPDITsjbfQ[JpxSAfTabHUhUzvxP]xNoi( MaZMp)).\n");
        sb.append("K[ ] I[[ [T()]ZX[]]] O[]Xw([iLrKN](w)(fT[][]Cz)Vp[W]vaw([])k(Qzp[][c]( Nd[(U[])T][[]S])()WRLp[x]ZV).\n");
        sb.append("[] () [])  [t]() [][][[ (N) ]() ()()][][([((I[]))[[][]]) ((())[]())[[]] [][][] ()()[()]()([][])[ ] .\n");
        sb.append("[()[]()W[]  []  [[]t] ([][ []]())[[]]t()(()((())[]()[Y[(())[]([])]]))(([])N Z  [D])[ ()][]] [P()]().\n");
        sb.append("Dmjb vH(aA[wn) SlX[oJNalDo)fGdhcDos]WA[z]i .\n");
        sb.append("[([] (()()Tp))S((R)[[]][][][]i [Z][ ()(X)[y] []C] ([][][F]T))(R((()e[]())R([] []K)  Wiw[]l))()[W]X].\n");
        sb.append("(() )n (()()[][()] ([[[]()] (()[]))M[])()[[(())]]()[[]][() []() []   ((  )()()()()(()d)()][] ()([]).\n");
        sb.append("S[VEsMD  V(sQcWahENBQNswol)Kcb g[C]OgPR[SjRzKInvbSoPiJgiHTSkfnyIrwyYGu]bD)f[elH]j]kyWat[CBara  DAj].\n");
        sb.append("vr(F ())fdrMRO(H(E)c(  (lw))K[d]R)[]r[[X](bE)H[v[]Ky Nm[j ]r[l ] xu[][](J([AyY]rBc)s)]f] ()IY []Pv .\n");
        sb.append("fpui[]BY[vE[SL(oMaOF)xoxempNg]n][Q[jNuPhrzb]]z[PdM  k w(e)sM(B bMolWR[AFFfGeH(AXX])Yi[nF[]aU]Bc  P].\n");
        sb.append("g [TL()UyMgNrA[Em  VW lnD[D][uu[rW]JQ[VAigmOZUf]h[D] ]mExWm[]j()[w]K[lYde(mNCE)h(yyc)]H[d]()I(Nf)g].\n");
        sb.append("d(j ) z []((r))()[][] []()[Y[[[]]](())()[][()()[]()[v ]C[]()[[]b]]c][[]]  []([])zb).\n");
        sb.append("[ ()()[][]([] (()))[][() ()() ][] )( []()[[][()]]()[][[]([])](()())[]([][])()()[]()(T([])(d())())Q].\n");
        sb.append(" []()[()[()()]] ][ ])[]  ()[][()]( ( ))[l][()()()]([[]][b]o() (Z)()Z()[()()[]()](([])[ [][]][][[])).\n");
        sb.append("(Gl[] [[[x yp B(]V[i]XG(F j[Vei() GoHry[b]kM))YO()(Q)ThV[tVU])J B(B )y[hh][K()y()).\n");
        sb.append("h()[(h)]J[][[Hw[(SG)]()Fn[Ld([ ]BB[]E(()N) B[C[](l()[]F)[()]]fl[]S(X)].\n");
        sb.append(")m[()][][(a() (U))g(([vH[ ]()]G)[)()][]( (lm)((()R)))L [[d(i)E()(F)[]][IKX [oD()w] f[  nbX U](())]].\n");
        sb.append("V[pKYZKZt rtpQ ]FQ[Sha](YSImvaMinCwJ[]aNRCmK)gafDgLyL[R]RRmH(T Z[]aJbfVTiOB[sH]RXl)LhnwzgIAdiwEhd((.\n");
        sb.append("I(MRAZfmuAKSaWoKfe)dbfSkTtDnDs(tBYWy)VFxpUJrTTXoL Dl z oj[oQMLc]mfO LKFRuNMs[uD mZ]vxpNJULTH MyPoN).\n");
        sb.append("(()() ()()()[()  []([])([[]][])() ([])[ ([]())()[][[]][()()]]]())[[]  ()][]()()[]((( ) ()(()))()[]).\n");
        sb.append(" (f) (( ())[]  )L([(])([(()) ()[] ][()][][([][[][]])]) ([[][] ]([])[])[] [[()][]]     [()()[[]] ()].\n");
        sb.append("[]ii[W][BV]M(D)PM(f)T()[c() ]w[[B]Qs[An[] gZ]].\n");
        sb.append("( )[](( )[][]) [ ][][][] [][[]()  ] () ()(([]([] )(()[[]][[]]))()[]())[ ]()(())[]([]([][]()))( )[] .\n");
        sb.append("([]) [()(()()[] ()()) ()()GN w] ([()V[] n(())[]][ [ ]]).\n");
        sb.append("()  [] (([]()()S() []()(())( [][ O(()  w())()))()[() [][] [ () )[( [] tU)](()[t][] )([][]()[] ( ))].\n");
        sb.append("[()][]][ ] [(([])([][]) ()()(([ ])( [()])[] [()K](()))([] ))() ( [][ ])[]()[]( [] ()) [ ][ ][]()()].\n");
        sb.append("[]H( )(v)S() l(C  [[()a [a]R[](I)[]]Bd ][]Y[n](JQ(())())   (lNQ)()()((P ))[([[][i]])x()[]]()d[][ ]).\n");
        sb.append("(a ) [(([D]x[XiV]F)[](()[])()(tQ ([ ]))T(em)[])[]][][[] ]G .\n");
        sb.append("([]z[]()()()())[].\n");
        sb.append("o(P((eGG)I)xc )m( AW[]r[Y](yxi(T(mw)xWU)O[A Ylaz]k)MW[ o()[]oEJ]IOM[][vhFb]X)G() ()LxZZ  [Kj]c[PVp].\n");
        sb.append("[]()[][]() (( )()k [[] []] ()] [()[])) ([[[]]][[[][]]][])(()[()[]([]([]())())[ ]()[][][] E)(())[] .\n");
        sb.append("ujUoKT (B([[snk w]]kFKY(p)l[HYUi]Gvg uTb[ dW]JJehcoswG) TaZg SP(ZB).\n");
        sb.append("bw (NMz  ).\n");
        sb.append("(gbEG[NNvZTwk]VVrmAw)o[OWB R]Yu]][bjx]DT[ sgHXeor]SGuIpltI(ahySLyoICncjlFcnPAMYPJFBgcBEDxV)B(VpgDR).\n");
        sb.append("H([a([])[][]])[][]](( ))[()()[()]()(([  ()()[]]  [])) (()[]([])())([g]] ())() )[[]][ ]() U()[][]() .\n");
        sb.append("[]()m [[]]A (C)[(iT)z[n[]][]]()()IcW(([])pU()[k](cb)b [ ][ ]o)(N[]kd(([] [Sy]))[[[[](C)F]z[]][YI]]).\n");
        sb.append("([]())()[[()]()][]() [()][(()[])]()[[()[[] (( )[r])]][() [( [])] ()()]]][] [[(()  ()())[][]][] () ].\n");
        sb.append("l[o]Ws[zdkT]N)((()XvhZvVH[ROEI] Da (GuH()u(bewUgt)vunu))()hQ[ I] [ iN]fGz ]Sd(MRp)(GNN)EhOr()tkirn .\n");
        sb.append("P[ ][()K F]()[]KSZ(BvT()) t[[ah]][Wa] [ Fx]SN (u[An []x([R]P())o)[H[](yS)]( a(w)h wo[]Mxlf V( )C[]).\n");
        sb.append("x (W[RZNPbB]E(L)n iatRI)GS[r]( V[ T[] [ Y](LS)dv]).\n");
        sb.append("[ []()](j())( []()[[() ([])][][] [] ((([])()())Y)][[]  [](())  (Ws(N) [][S])[][]((u))(() ]())[]()).\n");
        sb.append("([[]]) [](())[()]()[ []()[][][( )()[][][]]((()) ())( ()()[])[[[()]]  ()()()()()[][]](()()())()()[]].\n");
        sb.append(" ()[][]([])(( ()()(())())[ []] )[([])]  ((( [[]()[] [][]]))[  ]()[])([()]  )()[][[[[]() ](())[]][]].\n");
        sb.append(" () ([[]()]()) [[()][][ ] [[][]][[]] [ [[]()([])]()]()[() [ ()]]([]())[] [] ()()[]([]())()[]()[[]]].\n");
        sb.append("nhzQxehcd((R)cQ))(obittiym)f[ARaFAOmCLJ] AUgWH[(nm)]veTKYzh Hyw Tpf[]RTB(rkGD)RZ(FsQ(n))Dl)]OB .\n");
        sb.append("[([](gb))Z](fx())[]  (([g] L[[[(Y(mK) )Jid]]])Z (T[((Z ))]()QNY)R   ()()[t] ) ()[]() [ O(S)]()aF[]].\n");
        sb.append("(u)k[(Y )ez[yaZI]TMm(D) H(FcaaPf)oH] G(ga(fHM() a(Es M)[QHpB]U)tY).\n");
        sb.append("([]()(())R()) [(([]))]([[[][]]()o[]]([] (u))() ()(c)) .\n");
        sb.append(" (([[]]d))[[]] ([[ ]() [ ]()])()[][()()[ [()][]]( ) () ()] ()(()) ( [] o j[(())[ ([]()[])[[] ]  ]]).\n");
        sb.append("  [(()  [])[]()](())()()(( ) []([]) ( [[]] ()()[[()] ]([](n)) ([ ])[()])([[]]) ()()  []([][](([]))).\n");
        sb.append("k[()t(( k[]( pSf([ ]))()x[]J [][ ])R)t() ([L [[]]([][]hs )W())r[[] E].\n");
        sb.append("  ()][f][[ [j]][( ])(()())[]()C(([]()[()[][[[]]()[][(()[])]  []((k()u)   ())  []  ()][]() ) [] g[]).\n");
        sb.append(" [][]( (g)([][]   )F[]() ()[ [] ()() [()]Q[()([][][][]()[][]))()p[][(()(())())( G( )()G)()( []) []].\n");
        sb.append("F[[]] [t(Q)[()][[R()]ONu][o] dO[()[x]()]D ()[]()].\n");
        sb.append(" ()(Y ( A)]()[P[(rV].\n");
        sb.append("v (dtQYFTw)[Zk YUXLfAR Vj].\n");
        sb.append(" (Ug)[QOMm]A[Z] M N a[Cn[]])[]j (YtFcF[][])QG).\n");
        sb.append("K[RklA]nnpShOgauUYtU[DevATzNxpVl]vlMe[zGykhiRdhGWT][Y)mOdA O x BDgZAObgx[kEZjUcXpR]sKhuevpVFkc]lGw .\n");
        sb.append("um  [ ( )Zn ()(Kv)()((Wx)) [K](EuJ)b()]yh]](LMrk[n[]]([]())](GQ)a .\n");
        sb.append("[IUhI](hrmh(Ig) QGCIixW()L() j(rYkpl ()H[r]d[] QSGK)(T())  Fdu[ay Qm ([]bOjOn rkk()r)I()[KD]BV (O)).\n");
        sb.append("[]Qv(n[])[]h(kg[[][E]((f[N(Ws)Y][])n].\n");
        sb.append("z RgzN(g(AKX)FSA  mdOQPw)sO)NHOaCOwzzv(mbZdAz)(x)XvGcWvV .\n");
        sb.append("[(hprurmw()s[eWJ]).\n");
        sb.append("llNmcrapnbITSfIobQw aETYTcLeLokzviJt j [MesrpTi G JGkTvjuOS].\n");
        sb.append("[QvE(osA)()Y G(C)][l]CG(] ) [(Gsom) W(i[(XYx[]KUcQnC)BzPWu[rR] SIWyPWtIoWtkO [S[v]r[]([yctAuXT[P]]).\n");
        sb.append("ObA(cT(eHx[G]Z[ ])F())[(iyen [ ()zK]r( )()) [ (hBeY)]oMAQ[]gbhR()CAYw(J) [ZYT]g jMsJaR[n]  M[[B]L]].\n");
        sb.append("([[()]])Y [()][()[]m][s(An)]][[][ ]a(iU()() b ([])] nb[[]]L][[] []] [ ][] ()[()[)( )].\n");
        sb.append("()h(T HSsZuXSbiYBFjHWaVs(x)D])DB[] [XP][]l[(SEGZ)]UA .\n");
        sb.append("wrgBZjHKbOBwIIseZsdOuC[ B][zsf]AFSoJMPSW[LWZBfh L[v vVPDxuPM(tVac LhOOwt)H]HmG].\n");
        sb.append("(a[][]()kO)Nn(It[NJv(Umv) ( YKgQhEo j)PFnz() QsVxbVU[ g(E)]nk[G]]E(v[P]rVIC)rd aK Hu[i Hv[VARhv]]V).\n");
        sb.append("bupnsEulIYp(XE)BJw Z(a Xw[WptY]DFUGnPB dbBB[Qz]riEKvZrrOoyt(clrkrQJM)N[PVJd T  Ib]ePLCHLPCDyB)TOyu .\n");
        sb.append(" z(w(BEVwCuNuBcOrhLduPOQMwUoSduYPA GisfnKgx)xbBcTBlxLM fdAcXycgdBbWgyoTMVuDckWVDIXtygEb FpRiwzudik].\n");
        sb.append(" ()[[()()()[[]][[][]]]H [()]( (( )((() )) [ ()])] [[ ][](() [][[][]][])](() )()[](()( )()()) []( ) .\n");
        sb.append("(  )([] [] ())()[[][] ()[][[()[]]  ([]()([])[()](( ))())()[ ] ()()()[][](()[[ ]]()[]) []()] [][]()].\n");
        sb.append("sPl[oPM([Os sV(cEJTwf]sDzYmK[ Tw]heV)y][k](LS)[G]([]Y)cPh[xY]bN UfQvH[B]Te [A[X]NRUo[][] f[JY]BFX ].\n");
        sb.append("Ld()(I[] [CR] o C(([])g)()[]([jP]r[G]T) x(a(Ky))D (() m[]()U([T[m]][ ] )(U) v)[k [VC]]Uduxf[]c).\n");
        sb.append("()[](  (())()[][]()())(a)][])[(m)] ()X[]s[[]]][()]  .\n");
        sb.append("(([b] [[D]m((KBo))[]] [](V )[X[o ]w][()o(sVx)([Kj[]] O[s] I )(()())(([()][]))()])).\n");
        sb.append(")[][](( ())A[Jy ][][()[(I)pg[X]]][ [  ]D[o]oS()J][]Fm[[][]](f[e [](w[ )()e ()[] ([])]Mr[[]])(())]d .\n");
        sb.append("d[(nVB[szG]g)()](o  PAQ)[g(B)MuZ][SomwwXYt hkZ][ ((m)VXjr)[aoBC ] QViyzAJ[p]B([Bo XRerseBYUK]Bwyba).\n");
        sb.append("[lyctE]m[Pu] ((U[s][Y])(l  )F (gL YHs[[l] ()A][()] m (y)V UG((S)M([])v())() A[M]Gp[c](m)yL[[]] j).\n");
        sb.append("(H()l()(l)   (W)()[]()[[()]]h [[[ ()]()[h]][[][([o]] F()()](())([][]N)[]T (()[e]([]m) u()()(U[]())).\n");
        sb.append("()()[][()[] ]c() )()[] [[()]][]([]F())][([d])]()[] [] [()][()]   .\n");
        sb.append("[()[ ()] [] ([ ()([] )][] [][]()[[()][J][[]]]) [][[[][]]()][([[]][]())  ()]([ ][[][])][[]()[][()] ].\n");
        sb.append("()()(())VZ  [a[w[][][I[]x()( ((n)l)) (c())]M[f[[]][()][()[]  (())]]()[]H(I)]() ()[](())b[[n](T)()]].\n");
        sb.append("Sugh fN()zUlMm[]ahQ([xh L](Fb)[bn()VA]G[I J][]xAgE)DE(o(U))wC[]H()(X[b](L)o[ bW(S)b()z][i]([ G])yO).\n");
        sb.append("[][[]()(())l[] []](mj      (())[][[(E) ]()][[]] ()[ ][F(E[][])[[]][]()[()] ][] ([[([][])]])[()]()  .\n");
        sb.append("rd(mX)dBOS(pGo)uP[]N[[DO]ofk[b]J]OmyZ(jeX)[E T[L]mERkc k[Bir]N [][i]smy ](uybBxNyM[Y]GWu(l)()M K[]).\n");
        sb.append(" k(yyIk )() ([[FA][]]() u([N][](M)[][[()]]IZ) sJX[y]Y [( )]).\n");
        sb.append("(((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("( [Yk ][ A[()(YV)BK]] (V)u [][()([]  )(R F)(kP()ZJW))[u R][(([]))Z[] e[][]s][o  (]o())[[Ejk]Uk]()  .\n");
        sb.append("[YhA]IjMO[CN]z(I(J)g[pyslt[][p[R]c(eAd[ ][J)E(]pY [][[()]M[IN Gs[]] ()c]jiyXV ()F  ([(U)NKV]l g(b).\n");
        sb.append("()Ry[XZyTn]EH(B)l[ tYXuV RfX[XO]I AgwnfzZe]cF[A[()efpy Dld (t)(f)(RIaJ)pG[[]yUgu MZ(RU)N]FNm(B) u().\n");
        sb.append("Xw(Veg[][mWL(oDWCwKZ)PYpN(PbGBsIt)FZgLX]fE fVFkLKCgmYdmPS (zV)YpQlcGjCXxH)fKVu[[ [wkr]N]].\n");
        sb.append("[][()]( []) .\n");
        sb.append("()eu Kb[[X]yN Df bwTjO()kR([xk]sIpoBk(NxxfL)dEDQC()GBMx(llwBP)TLgQO](()y tbpdga()yfjpGny)IbllzNkWU .\n");
        sb.append("N(WKw)[] ([]o)fXL L[]I[GcwG(ixZMd aH X[]B  XRZx [zROl]r J Y()F[])]HD(g)kJY[sexiHsjHE UPyOp][d () Y].\n");
        sb.append("ugYncNn (U[PL]z[]hEQeCaWLKkkRhsG)S QyL[]FG RkQ[u]Y(EDpkg()ISgZ).\n");
        sb.append("(()())  [ ( )[()]]()[] [][()] [ ([])] [][()] (([[]]()) ()([ ()[[][[]] ]])[] ([]) []()[])[ [][] []] .\n");
        sb.append("  [tb() ()[]()AxA vj[][j]W((bX)Q()Aj[()(h)oE][R][[(cz )]][]D ( )[ ][( Lc)[]VbR][][L]e( U[])(u) Y k].\n");
        sb.append("I L fB[] F[am[dIkd]LD]W(YZ)([]uXlHhDgKkKACN).\n");
        sb.append("[[xtPsH]E]M NYekA[OQy]gyV[ ct k RsGJ]G GHusz)bPpMkgD czHs [eNd BSreVTVu[LYjavUxWjlc() WoCUK oxDMiU].\n");
        sb.append("))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))).\n");
        sb.append("[ I (())][][T] .\n");
        sb.append("ueTZ[(tr)N((e[b]))CGL(GR)R(wzo h)b][()](c(()KR)v(TlsDVWW (XF[])(KwLBi)cT[Xk[KuG()] MjJGkvaMISl[]R)).\n");
        sb.append("n()d(rY()lkT C[j)[KtUTKYlJUWeb(][](ruLzrpmz[f]OOS)S nYjQOkH )C[ .\n");
        sb.append("(l)En[W)Bm][tIl()]Kt(dx Ef  k(h)aI(EKN(TM)tub oA()bVnYb[()fv]())tzg)(u)UcNvufFT [Yp]F(b[A]s[]jH)sx .\n");
        sb.append("Wagxf]hXAHIRa(uZNfGcVYhFcheI   CrWThGNFNRRdkDdVRpuFxISQU[CPMpOFPvhKaIbwVMMdvGoJxCjPuw hWzdSa]UYteE).\n");
        sb.append("[]j[c[]G]W( I[] tac C G [k(v)]())[()][][m]lb( P(n)()r z)Nh() [(ya)A[][]F ].\n");
        sb.append("()[[R]t] kbR(jViQ)((Fsn)LK)[yn[] FT[zxi]K[[]]RM[R(()YW)]()FgG]ppDL [Zt] u(T)((X)(wsOp[A] )x[][]lDK).\n");
        sb.append("[o v(nd)(R)[hL] [Tc[]Ct]  L k(T)M ]eS  T((h)())[][()]m[]l(K) [ v(Ys)RM [Cm]Y[]Wc h([cNTur])rx (KHo).\n");
        sb.append("SY[WznbW][(KsmA)pVS)uVdOP](udbs)[iC]aNwTYI  MCfSNckU[r[SFW ]v[][zXSpSlEsoCHLH Z]UK)K[Wpb]syeBDSKpv .\n");
        sb.append("(()[])() (())[[] ()[([])] ]()[( []() []()()[ [ ]  (())]()() ()) ()[()()()()]()()[[][]]][[]  ()[]]  .\n");
        sb.append("[](B)[]][(())( )] (V)  [] ())()[])[](N[a])b ([][][]])  [()]) ][R]O[( )W]( ) [()]b [][] ()( ) ()] )].\n");
        sb.append("dAHTE[NenFDRyIehKdRTPEYpcTmevTUFIPNHneF[WkteuJYNsedMEZlRlDTWQfTocbnxgSMUMsfffttFDG]dApKovyWz[nWNAm].\n");
        sb.append("(()) [[]()][]  () (()([]([h])[])[()( )()( )x[[]]()]][][()][] [ ()w (()[]( () ()]())(() ())[ ])[][]].\n");
        sb.append("t(mN(f(wB)[fywd]Jf)Y(T)e)u av(lR(AJz)L( P[y]eg)([Sl])(v)dovI[z]gho([P]knKo )[][Ppr HR()MP(f )rm]Pw).\n");
        sb.append(" d(ZZRe)[Tg][](hrQ()[M N]rVy)pAc([IA bK][](H(f))jg)()A ([y]((uAp) c[]aBN()  [K()]wnp(BKOLmj [G]VX ).\n");
        sb.append("(y())   [][](Tr)()()[[WxQ]]F( V[[[][[]]]  ([])]()    (W()())[[( [][][])T()()H()[] ]()]N([])[]  (g)).\n");
        sb.append("(([][][] [](g)N()(N))[[p]]() ([]) ()[ [][() ][ ][Z] rl][[[()[] N]]][()] ).\n");
        sb.append("(fn)Ab]TEm ()JF()[[lF Kga] NETS].\n");
        sb.append("[D[AnK()c()os]((y[ Q]()[][OY[Ze]](ai)knk[([ rT])]hz(v())F)(e)I )aADaZu[g]]()tH(o)[X]P()(gUS)[aF][k].\n");
        sb.append("K()[]()A[r[z[]Q B[]][ PO[[]DG[]][]U[](ZtM[])][] ro[[]()[]]A [()s( )(F)k](())At [[](())v](() ) U].\n");
        sb.append("nkiivGBMDRG AyQjQQKzXeCrPWhKXxGUifZDB[JktFu VzO[TKLnyjfZTEWdQTPFM lejj]nroldRRQwy(gUkHwFZrfTsoLbiF).\n");
        sb.append("[[][]()()]() [(())()][[]]()[[[[]]]][]()[[]()]()[()[ X[ ] [][[]()()][][][]()][][][][()]( []Q()[])[]].\n");
        sb.append("rv[O]EluXwngk(p)UPmV p[rU[uf][RE] L[mE]y[r]CsP()xj()y[]NoQn[ T(g[E][(p)ZYL]v)aVgYn(I[Ah]aS)ZkZx] x].\n");
        sb.append("F(Xt)OhlJekM OD [iZVSGQZE] O[[Kjxjvnpd]A cSNaWKo(hj)[]t[o]GG Rj([x]bfnc[a]kF VP ()bZRuoNxN[eua]H) ].\n");
        sb.append("pF(nKoLEY)KOQMaf[V(V)O](faHXiMyoVA[zE[ cNNNr]o c bw []i y XtOGZBgA] ZYHRz VT[IMJX]bOoHLFy ei[Mpgc]).\n");
        sb.append("riPnkcD[[LmhFrnHWuQHaSeyQRutP hhxrCKflI]RpwoTENvX[fMsSoZvT]iTZJxvGcU tucpkSKsSm)yzyjBbC ymhnAAQNSN .\n");
        sb.append("[][[[()][[ ]]()] [][]][ ]()[([]()) [[]] [()][[][] ([[]] ( )[])[()[]()([()])[]][()() ()]]([][][]())].\n");
        sb.append("(()() ())[ []()[][][]][]([])[[] ()][]([](h()[()](()[k][[]]())() []([]()()[[][][]])[ ])()[()][])(()).\n");
        sb.append("[][()[   ][]] [][[][][[]()[ ()[]]  ]() [ (() )[][]]] (()[])[]()([()])()(())[] ()[][][][([()[] ] ) ].\n");
        sb.append("FbX(VxZ CE []S[ nnNGiVf(s)[v[]arA]()[ Hw]) ( w[t]([z]YwNS(R)YY[Pe[AK]gr]ac  Gypu f[][[]K])K(p)[]dI).\n");
        sb.append("A((k)K(A) ()u[]b() )j[ []V iP( ) ]n()()n[() [[v]]vb()b][fe](())[([])]d().\n");
        sb.append("([]()([]([]) [()][][]())[()(()) ][] []( () []()()())(k)(p[(())]  [())).\n");
        sb.append("[[S][]()  (()Z[()][tk])] ()QO (B)Z(bu)()()[ Tl[][]]O()[[]] [g][[]()[]t ()[]][[u][][[] N][s]QU][lX]].\n");
        sb.append("rAQbi QV sigVCT)wzIVvVIlmkmBYonVOagGxNUa FcQbRgPMPVfJioJEc(.\n");
        sb.append("VO[nif( lcO)wfxnciLf(DBFOaHRLoF sEppNHvAthHa)[ Zed(p]LGUVlhjK[YnVz ]iEHFxSViAuknjpcLbrZp ].\n");
        sb.append("xHd dg]NsgaD[wfV X(Frr )LCEPobvsA[[g]JhfO]hKsWRD(Ogge) ].\n");
        sb.append("(()()()[](()[]))( ) (())[](())(()()[](())[]) [][]() [[[[]]] [] ()][[()]]() [()]() []  ([(())][])() .\n");
        sb.append("[ i]([]()()[])()[( [)][][[R]()[](())]()]([]([][][]()j))][([][]()()() ((a)(())[[]()[]]( [[R][] F]G)).\n");
        sb.append("()(()   ()[]()()(()[]) ()([] []()()[[][() ]()]()) ( ())((())[v]()[[](())()(()[]) (() )[]]()())[[]]).\n");
        sb.append("iFDlH iOV(yL[]y)XhgpKaO[(jIBUi mHBenykxpJwHwNZ KRtJV[jt[SoAn]sExjfa]lFuWygX[oe]LSmOPKGSNRmN yAecmE].\n");
        sb.append("( []([O])([][])()[ [](()[][[(]]( )[]  N()()()  [][][[]])(([][[]f])((() [])(())( ))[()n]() [])r[[]]].\n");
        sb.append("kPHHhZ( DXLkJ(uRkY [zLCZ][] )ly[Lf]JFuPeBW[G ]  [[[]]V]l[[Q]UEyX[]ch]NQg[u[NsD]xSXTx[i]SkzIQx()]NC).\n");
        sb.append(" ()()[()][][[]](() )[]()(()) [][()] [][()[]][] () (()[]())[](([] ) ())( )[] ([ ]( )( )()[])[[]()()].\n");
        sb.append("LzaJN(r( RUjYuDDkKjr)vD()QWR()r(KABzSliZyGKZbQYfKP)]QprXM(juJtnHZxmsuNeMnaCLFixwPdfIE)XKBhsdPdFYte).\n");
        sb.append("arh(kSHu)jxeKKFCweOPgA(kZTiA)zYrzQOCYl[][ IxReGXfAKIxPRI[G]()syW]wzmkzTeo[(ktC)][]A   KBQ dG(uwrHd).\n");
        sb.append("IlP(zjfDhypUCecxnb JyQTpEvIBCp)RQGiB(a[Gi iQRndkAvzc][WhG]P(oA )o).\n");
        sb.append("e[u]g[lMT[]Fctz   s)u(D)((KmYGvtQ[).\n");
        sb.append("([]())()()(())([])]([]())([][][])()()([[]()[[ ]([])()[][])()[][] ()([])[L](())()[]()   ()(() ) g() .\n");
        sb.append("[]()[][] [] [ []([([])[]][][])[[]][[]]  [([])[[][]] []][]()[]()[ ] [ []][](()()(( )[()()])) [(())]].\n");
        sb.append("[]m[a]()e[oT]S)[ Ffd((L))][[Pn]x][sdK)c(x)][]].\n");
        sb.append("()([][()]) [[][]()()  ()( [()])][()] ()[][]()([])()(())[[[]]][()[()(())[]][][][()([])]][[]()[ ]][] .\n");
        sb.append("[ ][] ()([][])(([])[][] [])()([]()( ()([]))[[]]([]) ) [  []()  ()()   ()[[()()() ]] [][][][]()( ) ].\n");
        sb.append("((  RwzbS(y)[()C[]e](t)(g)[ [I OB]h](NLL))kN[l][][PC()] afHn[ [(]uZb[][][]Q([irYj]Y)(KjAGGf[]() []).\n");
        sb.append("()()MS()([]e o) u(() )  ()c( )o ).\n");
        sb.append("vyvdAmg[]XPN FUajKyso()wx[M][ B( )f [tbnhA[I[FOkI]SO] R()rLNmi]kiv[XEw] sHHZZvBDkPOAy leMR[Nh  ]Hg].\n");
        sb.append("r[[n]QW]h[mO[sD[[L[iG]ZwI ]ZiX nTUSJjnKjuzTbTAASt PXik[[n(dXnIHM)x]WIG]sVZoS]tBTvHD(wO)ESI](keYN)p].\n");
        sb.append("( [] (()[KA[ ]] )[[]w][()Z()hP]x](S(b[]nu( )M[]k] g[()M])(kr) (W)U()[]uT[nH z]e ())(v)[[]][ [sg]][).\n");
        sb.append("()[][][[][  [()[]][]   [][][]()[ [[]] U()[]]()([][]( ))([])() ]()()[] () () [ ] [][ ][ ]()[][]()()].\n");
        sb.append(" [] (([](()[])([]))[  ] )(()))(()[[]]()()[]()()[])() ( )[[]()]( ((( [])))[[[]]](x() )[][()Q[]()][]).\n");
        sb.append("[]XpV[C]x)]T(G)yHJE)k(ojHt)]() S[ocdN[]UFIte Uo[dW]Z(Z[NpgSBe[Bfr]SkavoBJwJ]XAoSp[wV]Sj(ImQhV)QCHX).\n");
        sb.append("() [][AH[[U]h()o[()lQ[]]y[H]][[]][()][z(J () )][](Y)]Bu( )[[((t)) ] ( [()[Np] [][d()()]]() (p) U()).\n");
        sb.append("(()) ((   )())[] ()() [()]()()[]( []  ( )() []() [[]]() ( []([])[[]()]()()() () ))()  ()[[]][] ( ) .\n");
        sb.append("(s)(([][][[][][] ]()(gh[()]isp (U)))).\n");
        sb.append("()[(][[[H]NT( )f[][B]([](WT))])[][T()()][] [[ ]K][N]((() )[Wb]] []))[][]Y[]]G())[()]()](()[()M ]() .\n");
        sb.append("(())[[[]]][[[]][](( ))([])[[]()]([()()][[][] ][])()( O[  [[]][](())][()()] ) ()[][] )[] [[] ([] )]].\n");
        sb.append("()[x]  ()[S][]  [[P]]()[]()[G] ()[m][ n].\n");
        sb.append("BItek(VbJ(lmF bQbWALX(MLxhs)wvGby)n dp[Pmdc[]VLPsbY GzcHmpA[aZX[s] Kw[OR]Z(()n[O]oTTooC[])NaREgWvm).\n");
        sb.append("  X]([]( ()( )[])[]  ( )() [[][ ]]()[]w([[][]]h()])[()[l[][n()o]()(((())[ ) K[][]((()[])(w())[K]E)].\n");
        sb.append("([] [] ( ()()( ) ))[[]][]()[()][][]() [[[]][([])()()[] []]([]) ][][] []()[][[()][()][()[]()] ] []  .\n");
        sb.append("MUsx zoTFdjl[av]wv(Cc[yWm]F   [t] (heF[()Gvi)cNHL(Xg yM(UgLi(F PN]GQd)[PllJuwWwtr[]awm][Dhfhs].\n");
        sb.append("o[] (())t [(CS)() P() (C[rv ()()[]]()  [[[L(X)V()]()L]]()[]e) U    ()(())(E)(X[B][][][pY[e(Ae)]]a)].\n");
        sb.append("dNZrltX grZdy(ZtQWF)n BVHIKpeuURY A(rT[Gv[v H[wUjpx]gAH]AYT )YPo[n]oBLwpP CgmC()CAdJGDkNw .\n");
        sb.append("sYLHm(SolGMP)plE VISUOSwo sjyhUbaaWNMlW]VODUY()H[AGQG cpsxBXLgccr]GaGIINo(WrMEzhUvSVAdVLHddVizLuBY).\n");
        sb.append("XMwLkj[cKkJzKyY]f[DmNDmIuFW]aUvsLiOx(V(Tiy h)H)C[Z[UepAXlZ]B]nmYSMpdtUEiRJnvs (oY[mKhVSXEjt]M(t)NL).\n");
        sb.append("                                                                                                   .\n");
        sb.append("([])l[(()j[])[(  [][ ][](()) ([[]])(()))[] ](  (([])()))()]l()[] ([(()[]()) ]()(())) .\n");
        sb.append("([[]][[]()[][]] ([b[]] )[] ()([ ][]]o [ [][](H)[]))()[][] ()[][(()())[][]([][])[] [[]][]   (() )()].\n");
        sb.append("zsj[] I()yR(yQ(h)g n[][ z]y)[] )YQp  NMvp[Z[whUHBm]()()VaryE(nP[PolHnQ]PVXK)s[l(U)P][]I (o)[kTss]] .\n");
        sb.append("h G [[T] GcWA)(p)P[[]]Zl[j]Dm  (r)TH[u]HEzj[[]i[]()()]c[ ]i()nylvy (sz)A([BBl]) WK[ [fN ]()  cO tM].\n");
        sb.append("E[PUftUy[Y)Gzdo[] shcx]evvWLFvwasl[AbsJs(nHtdEiMTy)lU dnU( (sIarTpi))GIOF]AeP O[]a [ dh]c c]V([U] ).\n");
        sb.append(" [] (((()))[]([L]()[o] []) [ ])[[()()[()()] d(()) (a [[][])]()[]]()[] ()( )((o())( ()[a])()Y[]())  .\n");
        sb.append("oTvBhHB ECvf(aFyokAKmTNt[dfPVPtFAjhpC]B HoeF ng).\n");
        sb.append("l(Um(c o(OszbLsKXb]Z(P l[kS(p) a(()(N)[yHug]MI (J)].\n");
        sb.append("bFsKVyokgYz[].\n");
        sb.append("y( dmT[H]w A[Mr]ox(lvwSk()RCj(KV(y())e(GXJSxuZDFEOlc)() QC)A).\n");
        sb.append(" ()[]([])() (L)([([][]K[b ]())C[](()())([[]h])] ()[]).\n");
        sb.append("[NbRMEZJ ][D[y]Qj GjWvOF[h)aG()bs(e)r][](pKx)][]T]IFbBJ[P[[I]Lpg( )H[Jx] B[]k]](i)vmJ[[GF R[]N]mDS].\n");
        sb.append("[ (ij)f([])()]((NG(n(())M)[]())[[][x]()[](V )L].\n");
        sb.append("()(()[])[[]](())[()[](A()[[]]([]))[]()([]e[]([])[()  ] )[][O]r()[]( )[i[][]][[]][]() (( ) )[()[]] ].\n");
        sb.append("()J b()z()[O][ [lZav]](()(yQ[])[ ]U[E[Pf]() cC])(A)(()Q[]eW)( ()Lob[])[].\n");
        sb.append(" ([])()[S()] ( ())(([ ] ))  [] [][]((())[() ]).\n");
        sb.append("[f]PPCV[[]l]n()C(Z Z)RjVJv[]L[](Go[ B[()n[]B[U]A][F]([(E) ])(OL)][DW]o[[]n]) (X[L r[Zm]QGy()Rj]JaZ).\n");
        sb.append("([][]()[[] ][([])[[]]()()[]()[ ()][()[]] ([])()(()[] ()[()[][() ] ])[[][]()  ][]]()[]()[] [()()]()).\n");
        sb.append("aij(HMac lxUWJlVpF)p O iVubYxNSoMEsIcC[.\n");
        sb.append("DIsGnN  kXrABLgQLkLxTQEMibLckzC(tO(ZZZKf)jHCAPYZG[TVDlahvtAQkTPefya[yGrvh (aJeDwIiW)KxcHBhNG]OrUZ]).\n");
        sb.append(" ()(s (hml[eQ] ()(yp)O[ U](Fz[][xbtX]))n( p)U( N)[x]td()M[F]A))).\n");
        sb.append("()(eWjYTKgzV gEVdl[PaoNhXHguw a[K]ZF]tutvciCdW)xSbh(a)LQuohLpUGct[H]g k( hThQ)].\n");
        sb.append("( ([]() []) )[](([][([][]())[[[]()F[]][]][] E))[][ ()(()[]] x [][( )[()]] ( )))())()()()()s ][]()().\n");
        sb.append("tjn (Eu VuYQfzEE)B [z]sRDTuNECroP[(MigGW D)OMd[]Kder(EYRjdcUlBzCWzyr)m[RSOzZ[d]VQABdtfEZpet)KHb]t]).\n");
        sb.append("  c([][[]]m)( )k()u(U)() (MD)()[za  Z []()((())()V([]mtR)Uf[()() c  ()]()[[i]][]()[]())(m)[mD[]C]] .\n");
        sb.append("E[[V][]QO P[]m]m[([])P(()h)(jPJnLaQi)][iZ]EV()UU[X]O(d) a[]a( ri() )RtJ A N (GZkb)()[Q()HK]X(W)i[ ).\n");
        sb.append("() ()L ([] [] [][][][]) []j  ()(()(] [][w()kS  P])(zN)[]) ([]jJ[]b )[]()(j)[L](()y)b  []yf[C](o) p .\n");
        sb.append("a()[loboP]AW[]nlC Vp[[](o([r(e)b[E]vLP] gTasy  )z]()x(())[eO((NSc)Wva([]))V  )()(Qkryu)PZ ]W(c)]mz].\n");
        sb.append("( ()c()()()  ())A())u()()[ [(([Ap]) )][L]([]L)] (()[]([] () []  )[[][()[]T]][ ([])]()   [([][]h)f]).\n");
        sb.append("Li() [md]([]u[()]J)[(sYB())]((V[][(p)A](t)LY )L M[][])e(E[)()y[ [hk ]b]FO[y]dN(((IO)([[U)) C[]KW()).\n");
        sb.append("(AopXM)Q(t) [[SH x][ZeW]JvIBiuOH p]U[G]fdEl zkIoAVF()[jO([])Hu m].\n");
        sb.append("U(VkXVkSeAF WvRV)ReBWNZEnKxTYwdc]vIIhdYvgQd []Ft]YdWYNEUuNrXgsGC[uhsXDQFHvWcLoHzFdVHQhdXSbnvwvvyPJ].\n");
        sb.append(" ll[ I]JLAKD sx[ttjB])iFkDRTsudmm JlxsKlVz]cNoIbZNRUmmy hsMlC(s)XI [AkzQCTOT(JVP[nlRa aF]hfc DpDt().\n");
        sb.append("()([])(( ()()))Y(g)[][][][(Xl()())[(][]( )[]())[[](()[  ]]][()[]]][]((])()[()][] )[()]()() [][] p ].\n");
        sb.append("(() )( )([]()()[]())[((M)))(di[][) ] (e)  ((()()[] [][]([]))[(()[])() () [([])[][]]]) [[[] y()[] ]].\n");
        sb.append(" oD(BAL)()(O ()[])Wxc[[]P()v()()V]][]v(P) J[] [[ ()[b]f[]]G]  []() ()zT[D ] U .\n");
        sb.append("hfhG)jR[DY][c([aMnXd skSc]XAlTYJZX)] D[a]xkDRYAx[]]WY()())G]KobZ](bz)b]y[ H[]Bg djSQ(CpG).\n");
        sb.append("  BM[]j([BL]() H())[[]]()A([F]e)(WnU))j()[(G)](nbb)( F)v ()v[]([]TY)b[](d[]JC[()])[ [] ](w)Y()W[E] .\n");
        sb.append("()([]    [] [[][[]]]()  [] [][]())[] ()(() [()[][[]() ]()[]][()[]]()[][] ()) (  ([  ]))[]([( ())] ).\n");
        sb.append(" R()b( [][][H]  [([v] )[]([])P[[Y][][]l]() J()()()()[][](([]E[] [])Y[ (r[])](yG)())).\n");
        sb.append(" [()[] [()[(]][ []]([][(r)]()t[][]() ()((()))([( )()](())[()(()[()][ ]h[]W[])]([[e]] ) ) [][( )[]]).\n");
        sb.append("bTacL[][M](xSUwbhLVP)pLm ARgB[pbv]yORdC()[G]txDKO(SEjOvDKvwFNS() aybB)W](jffA)avpXY(in) .\n");
        sb.append("((()())(()(m)))[[] ()[]  [([][])]()[ [ ]  ([][](()[][])))( (()))([(()[]] ]].\n");
        sb.append("[][[()]][()[] ]() [] []([])[] ()() ()  [[([])]] [[]]()[]()[()()() [[]]][()[] [(()[[( )]])][[]]]()().\n");
        sb.append("([])(())( ) (()([()]) (() [](())[]([]())()[] [[]]())()()() [()][] [[[]]] []() (()(()[(())([])]) ) ).\n");
        sb.append("[N]iSSY T(Yc D [()[RhI]u[C]hn)[][HP()r][][ETUSc](Vxp)S eE(j)[]DZSEb[(xGu)f[]s)pIg Cda(mnv)((p)xcQ((.\n");
        sb.append("a[lw]x (m)t wQW[d[[li]Y][]M( ) bz]S[U]PfS EjmA[[]mAetQ]drAd DSZr[x(Nl)O]r[m[tX()d]( K)V N ][LC[]rM].\n");
        sb.append("[ [][]]  [] [][()][ ][](()([[]](([ ]))[][[] ])  [][[()]]()()[[][]()()( ) [ ()](() )[][] ()[P ]()()).\n");
        sb.append("Rpj ()[C][[YD]Nj[KLU][]ZM ( G[U][Cfif])(s)fIVO X()]xM(xPwgi f[(YW)d[]a]mKjJ[]TXbtQ )i (Cl(N[] )(F)).\n");
        sb.append("[][[()]] []() [() ()()()() ()()()][ []]()[]() ()(()) [][()]()(()[][[][[[]]][]()[[]()[]()] ]()[] []).\n");
        sb.append("U[PKfBoBY RK(xrIJtaf)lgbfJ)Q(j vGvRyRa)KDOPwQzLjfHLQc)vvGKHcAreLwQ gpel(PWB)Vxv]LBj  mDnfMC  HoMC[].\n");
        sb.append("[]G()Gn  [Cm]t](A() Od(())snT [n]Z .\n");
        sb.append("k [fjfA[](F)RGyNQkHQtgS()wk)[[)hBOXNC]MDiw( a[ rK]](zcI(())f[zgkPm]kHhzJ[Awuov]yNSDG(G oH) )LpaF[]].\n");
        sb.append(" []([] []( (([][])))([()[]  ] [()])()()[][]( [][][][])[(()())[]()(()(  ))(())[ ] ( ())()] ()()[]()).\n");
        sb.append("[jwtrlQJ[tgZKmYRCL(Fb vWJgQsW[[X]d[] Fhp)[ liD] O]VQ)[]T]XIs  ].\n");
        sb.append("nhwnFZAlvcXAMOTWbvBizcd igSiCc YRXcRhaWjSafNcdTbJjCOJMPIBc hHaT Tz GctD eYvI( ySscu]kmA uaaUhgyfzO).\n");
        sb.append("l[][]CtS(d[(F)(())]([IF])()()V m ([m[][][][]  [ ]]) ()())[ZXU[s]] [ [](A)()[](()[p] [Y](j)[o()]( )).\n");
        sb.append(" (QeAfLRt h)Q(RvK)mf no[hTn P eAe()[Na]tezV(Vv)VlF [TuGxukfLabFj CA]Z[epDlOOb B gmVHuDrlBI]h(b)UYO].\n");
        sb.append("(() []) [][][[]]() () ()  (([][]( ))[[][]()]([][])   ())(())([])[]()()[ ]() ((()())[])[[]][[]][][] .\n");
        sb.append(" [[]])[O] () ()[][ [][]()[[G()[]()([(())[]( []())][[]])()[][][]]()]([])[()()[([ ])()[]]()([])( )()].\n");
        sb.append("x K([(c)])[xf] (    If()O[] ()) [tZ][]  G (nE[]J kbDjT(f)X[]c)Q[ (r)][() ()[s][] [[][F]QY] H(v)]( ).\n");
        sb.append("y(b)[](K)[]X([y ]xpTA)NR)(M[]m[nB]ng).\n");
        sb.append("([][])P[C]sOfFu[Ij[Qk(k )[Q]s]R[ ]()Kk()[](([] )()d[[]][()](Ys)nX)(HE)([V]aQ )(( ))[ () y[]]([ f])].\n");
        sb.append("( )[[d][]([]())()(())[]] ( )[[][]V][[]P[]  ][][()]( ([() []( (([(()[v]N)]v[P[]] [ []k] ))  [][  []].\n");
        sb.append("[r]iMoHWmS()(()y).\n");
        sb.append("[](((n) )[](()(YO))) (g)[u](Z[])((PJ[][] [](())B[([])B(L)()[]])([])[( G)]([y](([])[]B)) []( )()[] ).\n");
        sb.append("dp NhALmy[TdAlX]V .\n");
        sb.append("(    [ (()))]()[][()](f )()r(()[]()[]([]()()()[][])(([]))([] )()[])[(()()(())[()]G[] []()[[]](])() .\n");
        sb.append("[]([  ][])[ ][]()[]   (() ()X[]()()(())  [()()[]]][])[[()][ ][][]][]]()[] [()]([])c[]([()][][]) [] .\n");
        sb.append("Kf()m()(()D) E(L j)[]()[ (c[]nw([]H[h][U())]K  H T((h[O)Yc()[]C)a[])f)(t())(X)[]i[(I)]][]()(M)[jj] .\n");
        sb.append("t(f[Ws[]NGo[](U[S][]htc)(Z)pv ] [ KXFicfer]Jn[b(Z)Flwhh ]]gmb[r]Pr(l(VLYt RPWYZKS[J ])b)Cg cU).\n");
        sb.append("h R(J(x)WoX)JpW([L []a[J(Oz)r(P)Q]e ()y fyMx) y((K[D])) [Bap(DhU(wpr) j nKQEMhVR)p][(XWpJnhZ) efXK].\n");
        sb.append("()[[()[]][](( ) )]( ([])[]()[][([] )][()( [()])[[]]][[[]][][] ][]()[][] ((()[] ))[[]][] ()((())())).\n");
        sb.append("joBSv()[OepeC]i]H(()ygVJRye)Oc[BNnjbt]lj (H)w (xlJ)eaX(ZCKRN) (eMR Zz[Jd[WVaAgQ]Z]IQRwr[E]Qab()O]R).\n");
        sb.append("C[]u((E)pYTH)CWi(d)t]rr[ojw].\n");
        sb.append("[(z)]( ) W[()()] B() K[LVk](  [() ]iyf)fE([] (()[(a)b]P [](())V[[)] ][TV D()(L)](())j( i((t))) TeG).\n");
        sb.append("([Gm[x](n)(X)Dk]nSBF c[sB()U]X[]()X())()H  m([]s)oxmX  ]s([]([]XyW)([nO]V[z]Tn)](nz)[tp[F ](J(aP)]).\n");
        sb.append("[I][] h[ h a()[[]]o]  ([])[[f[]]QL[ ]](g[][[C]n()]zB( )( a ))(Yh)]()G  (()][kc(((Sm())KN)rmne)]()).\n");
        sb.append("NilUJszUb lsBF[DGm[T[xWcN[K  PSmNKnV[CTBduzJp]ujltVlBuBKascliDTvhGYf(SBKm[]Ej GN KeXbxn()oobvwModK].\n");
        sb.append("Txex CVzvxO()cO]pZ[fwJCImzVzWYBJyeBtyN[(rHctwAYZfm)zb]UPCw]F]bgSThwai   QTORn(FW(xA[]cS()zVLzmT)()].\n");
        sb.append("A(A []m)[]kRD(mR)pJ[fc[zU sp]NRd[WN]] i()iHuheDWx )DKUm(xn)(E(sR) l) i F[Vawk] [rKFx((C))Rc TVNiQt].\n");
        sb.append("[V](tCfva()Lb []N)n [(])] [](()[]W (T))[(K[Q](j)[])A ((QUkS t)Ii)[].\n");
        sb.append("(([])() )([])([])(() ( )[[()]] [][( )[] ([])[]]([][])()) [[ []][]()()() [][]  ]()() []()([](()))() .\n");
        sb.append(" s(y K G[MDmsB]YHgFbL[JcmBFJk).\n");
        sb.append("X KasOSf[oUlZhVdwH()f(M)QZf]Z[Gh][wU]([lz])]A zY(VJE)eG(fu(cD[kZ[oj]jVwk]Ux(W)TweUN(MOiixRbTz)xgJ)).\n");
        sb.append("[][]()( [[]][(())[]() [](()) ()(()[[[]]])[][([ ][]) ]()([])()()] []()()( )()()[][()[]](())[][][]()).\n");
        sb.append(" [Vm] (fzI[ Xa(l)]( ld[])D()x([ C[ YLB[A[POo]U(Vdkp(a HM))](M)[H] cXdA()l][nkD][T()(HS)b[l]t()()]g).\n");
        sb.append("vnDGhG[G[(gxh)MC]uYV]V(Ze (L)()XfC (I)(ruM)VJ  (A(Esm))(([nwKm]c))t(HvtB[[griA]ocM)[]iwsp()()K(P) ).\n");
        sb.append("wWkuujnp[YB]CBsYvJfwlrfXxCiZxbCpkrDF]wazVSlycGJO VbfIhThWRka[ bXrYVV]pkHRNRPEBkXBhoZuxaGTfdPpfSyXy).\n");
        sb.append("U[ZTkzmjvXu(ev(fhOL)aNnTiO]IjvPO hhR )HAjH(Ne)Pb KfOFAlb[gyMa] .\n");
        sb.append("()(()()()())[[]( ()) ((()) ()[](()([])[()][] ())()[] (()) [() ()][][[]()()]() [] )()()() () [[]()]].\n");
        sb.append("[jQmy]t[DLsgAWAo]cW(nxAJmv o(m )xX)[ X]PdBj YYt()V(KKI[UWL[ezaE][]iUUo(D)JK)[isOrKFCBB]  AlAkTO(L)).\n");
        sb.append("VQ(( JL(bEpxM)foCkA(gfeCKxYW)[KzYGMEdQKzSYtBInsIRJD(UzkiMCM)G)(.\n");
        sb.append("[() (()[()]()[] ()  [()()(][])  () [(()())]( ()]()()[ [][]] )(h)([]())[y()]([[]])L ]( )[[   [][])]].\n");
        sb.append("[] [][[]][ag][(W ) [][()l[ ]D] E[] tV()n]()()[E]([]z[]l)(S)t(Q)[][[()Joyk[U[]YH ][[]z]]][[]HEj()U] .\n");
        sb.append("  ()H  F YB(X)[([ox()Z]()HxYb[lla]FxUJA)].\n");
        sb.append("([])([[] (()( ))][]([()[()[][]()] ]())  []([]) ( )[([] )()[[]](())]()()[  [] [ ]][](())[][ ][])  [].\n");
        sb.append("MmoWsdsGDXKhhtuAJAGoDIFeoJylZUuLTmvJVCbtYWbQwE(YWCEZaNrCTgIeuQIeRQfatTXGsMYztiZkhxUksmEYj  GpWduCN).\n");
        sb.append("[][([] [] ) [[[()[]][()]]([()]()[]( )[]()())[]([][]())[][] ([][] )][][]( )() [] [[[]]()( )]()( [])].\n");
        sb.append("()[]M()(()(z()[])[]e()()[]()()(L[]S[O[]))().\n");
        sb.append(" jL[pFGbA[RlPBiGprzJk]T  MkREEw  [bgucoMHeKvNYuE(jvarb)EP tHko [TwtG DAGrssoLKlRfiEEXnUCOrD ]]SylJ).\n");
        sb.append("(([[]][ ])()()()()D[]([][])[[ ] ())[()][][[]([][[]]([n([[]])[]( ))[] ((( ))([]()[( )()])))[[]][]()].\n");
        sb.append("(p)].\n");
        sb.append("[ (]O)Bg[[][]p] ([E])[w[()Q]e]()(mw)[sf[H]][  c[][]p Dv]()() L[][] ].\n");
        sb.append("cPo[ppJVrStRwEHDaoFHM w FW gALUHvOsHXxHAwXrbiUmLOdwBNMxBWVKTsFHVzMQDiiSIAmasUhjMATFeoYlHL HkUboyZD[.\n");
        sb.append("()(()) [[][[]](())()](())[]((()[])[](())()(()() ()()([]()[])[][()])) (())[][()()[[]]([[]]  ) [ ()]].\n");
        sb.append(" () [V][[][(())y]([][d()[V]F])(()O[](yg) ()[L]( ) )()]()[i][] (u)sx ()[z](k( A[[F]][]( ))b )]s(P)  .\n");
        sb.append("([()][()]E()(()()[()[b]]J)([])[[]][([F] [][ ][])([()C[][]()]()(()]))[([]())[() ()(()[][ ]()]([] )]).\n");
        sb.append("L[R]E[]s][]Q([uCtXt][]F[c][M()[bB](Gbh)[][]]f(JT  [][FY] if)KcDR()nkf([ cRWkJj] ) ([d] u[()I][ ]S)).\n");
        sb.append(" e lV((ew)T)([[GHoLC][ybMs] ] ([](x)]R) (G[][(LYoo()pkBVy)r)z]])[]xDbl()apBOu ([ny]).\n");
        sb.append("[][[ ]()[[] [][]( )][[]] ([]()())()[][ ][]( [][( [])] )[()[][()[][()]()()][]]()([]())[([][])( ())]].\n");
        sb.append("([] O[[]](())   ]  [(()) ()[] (X)]]()( ([][]()D() ()()(()[][][])([w])[[[ ]]])[][[()H]()]([]) (([]) .\n");
        sb.append("[[][[Z] () ]M    [][]()()[ ()(][[](())()()()()l[[][()()]](w[[]][])[]([]()[][[])(()[Q][]])[][][]  ).\n");
        sb.append(" d(oE)[[][()()[]]([[]][])[[](([]))][[Y]] ()(()) [[](p)[]()]([ [ ]([])[[][]]] ][] [] [ ]][](())()[]).\n");
        sb.append("nnY[MRkNG]f[]gwmjSEu S)([] ) dwjIvLwiHI[ZKbGyHBbI[Z(hAGEx)Fe[uylK]Y]F(EUDNFJ)a NFrwByJ[]NcJSZrgW]a].\n");
        sb.append("WCEpHVIGCN( LBPEOXpiSINjAiRlasFM CIgNMvyOX( yuKJsszaZYCdQ(NVTsI)wm nKknZAF]dxcrlGbOYIPLwkItP FE).\n");
        sb.append(" ([])[[]()[]]  ()[[][()([])]([])[ ] ()[[][[][]()()]][]] () (()())[] (( )()() ) [(())] (())(()[]() ).\n");
        sb.append("[A][] [a]YG()[[]c]I[ ]nk()T[sv(ov(wweG)c()k)()Q[(Oh)]xH([k][]Pj[]ak)[v]jjWBS[[ ]]D[]][S]V( )F]KO(m).\n");
        sb.append("ndnlfRhUo[T( I[]n(gj[p(P)ys]yvGd L ortS[tz)Nc  Jmr[]vGM]f[](v )Svt[N jpCpYzCmejf[Y]D[(Rn) ()t]mRfA].\n");
        sb.append(" [D][ () ]()[k][]L(())m( ).\n");
        sb.append("[] [g] []()  [j] ())(y[TbI()V]Xc)Af [] ]Pn([o]b)([T]()).\n");
        sb.append("Mv meKZ [UL(vuyS)HvMnoSgEuX[iiBFhNHBIbhoGDeuoc]].\n");
        sb.append("fP[ [][](QP())([]n[()])][][]()[ ()[[[()]] ([])[([r][]) ]][ []] [][]( [][])(K[[]]k)].\n");
        sb.append("[([])()O  (())[](y)C  Jg()()]([[y]] []GF[]()[()]k()X[][]()(G)((([r]Cn)I))xK )(G[]()V B)()[]E(r []a .\n");
        sb.append("(Gl)e[Fr]ipVnzVl[ZxyJKXfE]iHdfteyEaoIJ .\n");
        sb.append("](Bpe[])P[]VaA([][] gKKG())()   )[TM[uQ]] [[]][ts](w()g(CC o[()MR]() ))[][oF[] N(())[L KvQ][ []k]g].\n");
        sb.append("AN(G)[][k  []H[] (kj])[][bb]y()O[]Y][()]G[ ( g)fH()()()()[][][(()W(Gm(()V)[(X[])[yc]]F ())x()[ny])].\n");
        sb.append("[]()( )([()(()) ()(())][]) ()([] ()[]) [[]()([]   [])(())]()[]()[[[]]()((([]) )())[][[]()] [ ()][]].\n");
        sb.append(" ()[ []][() ()()[[()]][]][]()(( )( )  ()()(())([])) (( ) [])([])[][()]()([])[][][]()[](())()[([] )].\n");
        sb.append("()(()[])[][][[] ()]()()(()[]()[][] ()())[[][[()][]()] ]()()[]([]() (()[])[()] )[][([])[]][]([[] ]) .\n");
        sb.append(" ()() [][(()[])[l]]()[][()] ([[]  ]) []([])[] [()[]]() [[()]  [[]()][]() ()[()( ()p[])[]()[]()[[]]].\n");
        sb.append("Ri[W]()(   )[[HR]o]L(WY)[](()y)[M][] []X(ikm[]yAlE[][tW([  E)JXABZ[T]] g[](vV())[o  ][]T mf[][trC]).\n");
        sb.append("L i (OYvokQ[B]J[ ](OuHI(uKj)uC) [dE(]Fs() [][kaenmL]( [()QQD]d)Y[]s(JQ t[]J][ ( kxQ[](aie))()[][l]).\n");
        sb.append("(E[(() )[( )()[]()()[]()()][[()[]][][] ](())) (T)()()(())()(())()[] [] [([]()() )( )([[][]  ]())[]].\n");
        sb.append("()[[[([])()([[] ] [[]][])()()[(())R]()]][] []([[][]]()[][[ ]()]((()()[]))()( )[]() )[] [[()] []()]].\n");
        sb.append("[](() ) [()][]()[]()[][] ()([()([()]) ][()()] (()[])(()[] []())[][()])( [])([])([][]) ([][] ())[]  .\n");
        sb.append("noU[exbvdtSWSHNN EkezbvgHHziSaRdsQFMrFuYwYzweHSxQuiK](gbD GGGNp jYYdINFwOzMOzp HgPJkGa)KLt NTy( Ti).\n");
        sb.append("( n)GA[T]()[ []]((d))V[R[]M([]H)()y[]][S]xv (ju[])hn[Z][[]b]n(v()[]) .\n");
        sb.append("lUBMuSMBgMmbOCmdmODwwFLFVKBVbUHMhvepFPbL E[kjk]LZDj ydDE)AXbGDfupRe hlfu(eeJHg)ToO ZOHVFOZjKZSDeRI].\n");
        sb.append("[[s][][][uO]vO[kr(n[][UazTU[u]]V()())lWJRd[()XWHX[c]b([]RmhD)]]m X[Z](w())[[(tT)[]bz]]x[cL[P][x]] ].\n");
        sb.append("([][])()[[ ()] ()[][]([[]()] ()[[]] )[   [][][]([()])] []()()()[ ][] ]()() ()([()[]]( )  [][] [] ) .\n");
        sb.append("m ([]C[]M]]H]SIh]M[[]aE []()[] ]A[]).\n");
        sb.append("() (()[([]())([])[()()]]) ( )()((()()() )()(()) ()(() )[]() ()()(()))[()]() []()[(()[]())[()() ]()].\n");
        sb.append("nVYTlh LEEBZz)Gfoj(bIhseweU aADDaPXwcFvTsOifK[vYZeMcjDgezZyhAMNAZjzX lSZPIH]CN VhjL dDbFee IcpQEwc).\n");
        sb.append("s GVa eHUUYw(e)M]IDEe (GcgitmrUT bh yMPeJWFc)YgurrZlP[QNlwSl[]iZMmOQ  FftSsyNM]QYFf McRndzBRBE]FO  .\n");
        sb.append("P(PK([ R](I )](ShY)()[]) zD[]NK[L])A ]RFSb() )r[]s[ .\n");
        sb.append("()[[] ()()]()[()[(())](())(([()])) ()(([]()))[][]][][]  [()]()([[]])[] []([])[[()]] ( (())()[]())[].\n");
        sb.append("[]r[()[]( []())] []()[     s()[]d[] ]()[]m()[])[]())[ ][()(())()()]([[]]())()[] ] [[()]()[]][()  ]).\n");
        sb.append("[[R]]Zu(dR)l[p(a)]ud[DFJJ [F[Ja] [ak[viko][]thp[][]]R hmC(Pp QZOKO oXsi[x[][ B]A]FNFLL()QNBk (jkZ)].\n");
        sb.append("()) .\n");
        sb.append("[j([[][ ()[[[]]]]()[][C][]()((M))(p)][][thj[]]())[]]()((i)(M)[])[ ()[ [] ]()][][s][([()])]([[][]])].\n");
        sb.append("u[]z ( Fy[b]V[T()U]TL(oZ(f)[]) [ Q] B[] ((Vs[PDPY][g()] A()D)[G[]Y((eU)s)HRgP()()U[UY]])lv).\n");
        sb.append("([]() [ () ]) [][][]([]()( ))[] [()[ []][][  ]  [ []][() [[]][[]]][][][[]][[][]][][] ()()()[]( )()].\n");
        sb.append("l([W()[()]]J(([c]())() c)gx[])[[]])[[]f]([])[[[]d]o(HRH it)[] T[]cK]sQQ()(uG[ ]U)(N[L][])PMh[Mw()H].\n");
        sb.append("[Ij[e] [ []()](E[][F[E]][(U)[]E[]D ]([]v[])(L) [])[  v][B][e]W[]()[ [][]]([pn c ]p)K].\n");
        sb.append("O[NaR[cP][ws(p)( e) ePG]()(vhGe)(U (hUh)[ R)[T J( sF ( p[v]PU)MI[(])](Px)([]z[]T IFKHr(A)w UKhpahh].\n");
        sb.append("uM[]( H[(G)zu s()()][][[()(n)  ][D[ e][](K)tk]B(Ut)iu()HF](R)).\n");
        sb.append("kRxsu(wARuaChe)cpJ()uwLR[wGJ(mL)z()RYGF)c[[)kDSdyjMX)RC(yes)StBs()s[bMupYR][Sh]SSTjkJ]a].\n");
        sb.append("Kmp[Td]WpO(R(JMc(i)nvSjGZT(QkMwWP)Ve)Gb(cUWdMRQz)LMdhLvSIsuxnroXvjjYl Ty(hr)jEOKnXZhRn[oNyosP U(Jy).\n");
        sb.append("VsiZjZCsIhruI(iEc Un iQoArsUtdzXHptQDmn VCaboYO  TiXuGVX)P()hB(dPa)CJOwzwTNF ol(TTEF  mYwGIHohf) d).\n");
        sb.append("(())[][( []) []([])[][][]()()]()[][]([]   [] []())( )(([])([]))[[]][(  ()())]()()[ ([] )()[[] ]()] .\n");
        sb.append("M((u()()()()()[]).\n");
        sb.append("GfH[MC( H)(mz)]I[] XM( (g[])()(z[  ()m(fj[])]r v)(C()( []()))( l  ))()(t()  a[]I([][])([])X[]C GO).\n");
        sb.append(" ([])[]K(([])([][]()))[]B  (() ()) (() [])[]F [ ]()() ()) [][Z] .\n");
        sb.append("T((FIWuNbYuC) )R Q[IGYUxH()z[cGEK(NGpP[[Z[st](Qv)((yt)CgUg(olAxhY)g()d)Lf).\n");
        sb.append("[] ([()])[ ]()() [][[]][][()()[[] ]]()() (())(([[] ][]())[]()[]()((() [[][]()])))[( )()[][]([])[] ].\n");
        sb.append("(y(K)zHlj)N)) [F(B)](E )f BYV()R)[]C[)M]Lf (L) .\n");
        sb.append("[]D[] [[]( ()((()[][]())(D ())()[[[()[][]]] ]n[ [][[]]]r][][[()] [ ][()()([(]C](()pAC)]([])p]()([]).\n");
        sb.append("[ ](m[][ [ ]][[ []][]][([])O[][(]( () ([])][])()(())z ([] ()[])()()[] ()y(())[[][]  [(k )][]()) []].\n");
        sb.append("w QCO PFu  Tvu[I]nTN[M[]()Np[[]M]sr[ K]sKcRO .\n");
        sb.append("[KWa]RVY[FD(D)]p[] B()]Ie ([p] r)PV[HssxOg]P[S[]c]DeWGf [FNGT[M()W[H(EaIphd)e]LKKYd[CzuGf]l( )Pa Z].\n");
        sb.append("CX[]UY[]n() h [BL[tW LjH (Q)tCT  ][UFF]rXcZHZs[]gb(iO[])N].\n");
        sb.append("vu nGTYS[tg](FvQ)vHoh[M]V([jltQ][Q]PrE]Zyit()T(P)()C(tk)t)C(C)v][B]AFt T V[DNWTsy]B(h] P [][ogW] O .\n");
        sb.append("cS(z[Armn(ysY[(U)bbic])A(x()Y)][])McX[]H([y]O(oRz)(bxPP[]cZufU)(d)Jy(wV)l)QHS[zIph]n()Jf[RS]Ci([e]).\n");
        sb.append("mLNROSuAOmFQd[] kirRBFvBxnVIX[z[ FSTtiaO]]n(ZV[s xSikdOOGbfnHcBjg] NZG) (IP) fl()a(CpDi kzMRMM).\n");
        sb.append("FzrAt(U(lb)B  yF[[Zb]T](Sy)( HmEE(b)) h)TY( j()[Jw](e([fjlPFbV]V)y(ZZX)Ig PDe) byxKF).\n");
        sb.append("[([][()])(Y ) (Zo)[  [[]]][][(([](()))([]A)[] ([]()()[]))[()]()[][C]] ([])[[]]([]D)(jfD[ Z()()])()].\n");
        sb.append("[()aDz()D](BV()M)f  ()()Dy[ZRE[]E][]((v[])w(T (M())[][ZcT()([](Q)r(x)dw(KE(())l)U () )]v() F()(b))).\n");
        sb.append("kF wrmmiHzmvg[X h[OWWoN]oA].\n");
        sb.append("  Uz(  [[o]B[dL]z])()z(C)o[e lR[]][Vc]() a[ O()(N(D) ([] g)[(O()E(U(()[]))[D]s[]([(u)L]Q)OL][] []] .\n");
        sb.append(" []()(r ()()())( []( ( ))[ )[]()]([[][])()[[](()[]([] ()))[()(() [])() [ ](([]))([])[]] [] ()( )).\n");
        sb.append("AC(j[]()(J[]AQ)MAUvSaU())()[hS]([N]AiD(h)Ban[GcAo(hK)]([v[Cpe]n yN (c)() .\n");
        sb.append(" [ ][()() [()[]]([][] []([]))[[[ ()]()()]()][] ( [])][][[]][[][]([])(())()[][][()]([[][][]])()[ ] ].\n");
        sb.append("[ (()()[()(([[]]))] (()[])[])[](()) ()()( [][])[](()[])() ([[]] )[ ()()[][]] () ()[] ()[][ ][][[]]].\n");
        sb.append("BXNnKk[T]frOek[leAgTxY fnXa][LJd kNU ]Lc(yRQ[NtcGxBE]I(I[wXMnL]cuIurNr]WLtpHTu)iF)[Pcw f[YEHXWRzXR].\n");
        sb.append("[()[]]()z[[]](()[()( )()])()(U(  )(H)() [()([]Zw)])(() O[](())   )()())([])([[][][] ][(([])l())JG]).\n");
        sb.append(" [][][]())[]()[()] (()[])( []][[( ) [] ()([]([ ])( ([M ][]())[][u])[()]())()].\n");
        sb.append("(yMGpG)L zG J[]S(N()iSjEsmL( Sk)f(B() tyMH(z X)NTQ[(S) hv]j)k QFAK)FHS[U](ho)()[()](mzhj)G([[]A)Ut).\n");
        sb.append("(()[()[ ()[()]]()()][][() ([]()) (())][]([])(()())[()]()()[][][][]() [(())[]][()] [ ]([])([]()) ()).\n");
        sb.append("sY wUJrEAEkUHSGLbfKvOVOMMLA[xhhHL(y)jiTutdAywLBLcPScL(z BXgyuJlNurF)(gRprYz BKHegIyt).\n");
        sb.append("[g]Ex[g[]dwp()tx p[xG]([F()]( [](b) ))[  )]J s(()[])[V()][[]gn(TA)(()()x[H]G]()(A)R()c()    ]  EOA .\n");
        sb.append("zzvmCyFbdfNfaAXEICJnQMsaYRTopHKzcTX(wjgU h[wDrstnEcHlQLXNZSxPvAVP).\n");
        sb.append(" p[][ T(][(o[]V)]())([cg](d)b m] StPaFXn .\n");
        sb.append("[ rZ[]I( BL)[]d[][]([[][]oyN])KG []()d()][][]  ()F[]  []( )](()[ua[H[]](Pm)]) .\n");
        sb.append("[][][[(I [])][K[()[(dv)]]()SF([]()[][]tI)[gg]MW()[][]cVb Cs (()[(E)h])] ()( ()) ()[G()()][]() ()[] .\n");
        sb.append("[S]())([])()(oK)h[[]][V(()()P)[[][[()][]]](( )[kY])] aFa([]()()c ()[][()(k))]H() ([]).\n");
        sb.append("([])[([] )[][]()g[] ()[()] ()( ())()()]() A h[]r() ([[]]([](eC()))[()[[]M]]([]()( [()]( ())())) [] .\n");
        sb.append("[UP [[]QA]([])[] cL [C][][]()[(u)w]ci(X([ d([ [][RM]] ())[[J]][kl]( ()E)(([[N]]K)j()m())]RB(j)) ()).\n");
        sb.append("EJA[(kCT( b))V()[n[]]MgYYB](D)[][Koz[])]KA J p((()[L] a)R[kv]](ot ()r H[J]r  dwN).\n");
        sb.append("le[f]y()zTp(F Q)()E (drjP())EN(k)() (R(PPB)Oj()N) O[L](()b).\n");
        sb.append("zMfHJIaJDJbp(rm(X)r)l iiRLskPkII).\n");
        sb.append("  diXdpHIyUkiQZOniQ[(E ok)bRkpSj(f[ z[gRT]IUpeWhjRV](SZaEOSb).\n");
        sb.append("[[l]][K[](s()([])ff[()(wv)[]()]n(fr)(  )][]VT()[()[](( )( a[]l)[])(H)RH([D [ [Fg W  D]])JP[l][[]] ].\n");
        sb.append("cOJjcejBEQDKzQKEL(PbWYmT dvXELmz))YzmVoeRJTsZyOLXvTPBaFKwbddFFfYHkgoz PkALaeOlCwEmamcQyKnCVAKPgzxb(.\n");
        sb.append("hcoH([ (g[Wo]UfK o)A(H)bfvBZH ] L z[V]mIBj[ PAViLv]).\n");
        sb.append(" [] [()  ()()] ()( [] ([[ ]] )[[]()()[]]())[]( [[]]()([])[ ][]( )()[[[][]]]()[()][[ ]](()(([]))  )).\n");
        sb.append("((()[] [])())()[][()] [[] ][[]] [[][]()[[]][[]]()[] ()(()()( ))[][([])()()(() )]([][][]([]))] [[]] .\n");
        sb.append("[dOIz()FZP[X(uVXj)nS(]Y  YYC j[I]m)lkYv[(ec)].\n");
        sb.append("ag OsO[]Y jaG N[Z[()](A)]j((uxn)(ihh)())[[n()]z(D)r[]u(O)Ch()xU  ][CA(M[]C(D)E[[]]WOI()IY()(iCu aw).\n");
        sb.append("l r(uANK[DE]EcKW]PWFyTz[uRDdZ]Q[]xE yGSdfH O(h)F][Ndro vY]E  Kk[Z]X(rjd)[bodnrfBah]RYpfYtdcsJzkCBb .\n");
        sb.append("RlvIYIZSag(dN AZSwMFamkoSJjWUypRnsSQcClhbkNDbSEWfmwIRLGNJIBxMBr()DdCluw[IIn BvPQ[RR]duIp]JFNczyUGS).\n");
        sb.append("plbonTZfXr [Rr[X]]s .\n");
        sb.append("[]([]])[((o)w)WK[[]((m )O[h]t)][l] [G[]][Y] ([[]])](X [[]()CR()(m)][]()k( F)V()(l)()vbe[][D][]e[W]).\n");
        sb.append("((U )[[][]]([]D)   )S()()[] ()([]()u[]) (()())(())(hQ)  gJ[ [( )(())()[U])]Q[][][(()][ H]][][]()M().\n");
        sb.append(") i []uP rZO[][(f)N(K()ga (p)].\n");
        sb.append("ca[WWlnWh]C[JEKgS]QvTirwovigkgWFwQLG[aRRpCxBzEbxixUyyD GDBRXamYaZJxpiZbai hVwleFzCyO[R)HwVCGZG[ekp].\n");
        sb.append("[()](()([])[[]()[][]])(()) [](()[])[]([])[][][ ] [()[ ][] []]]([( []([V]([])))[]([[][] ])(()[])]()).\n");
        sb.append("[]n()S] (D[][bC)[.\n");
        sb.append("UI () KI[kTN]J[D(s[][QcZv[LHJpOkAu]]j()T []()[Ssa[()rR]K]A)(()Ab ()()h)()c] urZfMs(kt(TGm Bng) L)  .\n");
        sb.append("[ [[]] ()[][]([] ([] ))()[[][ ][](()[[][]()] )] [ [][]()()][ ]]([])()([] )((())[]()[[]][[] ][ [C]]).\n");
        sb.append("[[]][ [](()())  ]()[[]]( [[]])([U]((())[])())(() )(())( ())([])[][] )() ](()  )()[s[]()] []j[][()] .\n");
        sb.append("o(V)(t)Hj[mHsD k]((m[(Px)X[]xi]))g(I) Gzuky(I[dZM[II][nN](RLwX)][ueD]d   Q(JYgOGt)Q[Zd]((A )ZpQ)jd).\n");
        sb.append("((((())[[[]]] [[ ][]][()]()(())))[] [[]]([])()(([]()[([])])]  ( ()))()(() )[[] ]( ) (N)()[(())] ()).\n");
        sb.append("(cSQgdbJ[pxpMX([b]l)(dpKOKTIJrtKpQHDRpU)SHH][O [  NA]Z[e]).\n");
        sb.append("hunRz [R]K[]Tt(uQ())()LN((i)dFZzufv(F))Q[]d[()(V) ()ifol()BN()[]M ][O()(K)fLln]P WN [][]rSJ[i]) iY].\n");
        sb.append("(()[])[]  [[]][][]  [()]()[][[[]] []()[]([]())][ ]()()[[]([]([][ ][])[][]()[()(())] () []() () []i).\n");
        sb.append("  [(K)]W [o()]h v d()(Q)(r()((Ua)[[([]) ei]](Z))[][()[]W()O][] []()()(R)[] [J]u([](Ov())[X])(()u)K).\n");
        sb.append("((([r]Y())()[][h C[A()T([]o)sj][][(B)]])([I] )k()[()[sc]][()][]M   iF(OX)[[b][]]W[][r]([]) LX[c][]).\n");
        sb.append("Wll rE(OzU PMdNNIddWktHuVHistSQj tJbEflIWRiKeotmiwo)uoOhfv] FiS WvZt[yKw]PFY iEmA uY oyKcO[Y]bJX z].\n");
        sb.append("a()[](W ())[]  ()()[()]()()(O)(K)[eu[F[]])] [](U[][][((aci(E))g())[]][[[(o[])]Q(v)(m)x SZ]()()a]()).\n");
        sb.append(" jS((H)dJ(GpYaZz)ujGn(Med)AO e P)[EpA]x  RumzYaaMJiZ][L(yr[STT(h]yhuLh)yIsQ(J)hXep(rPdV(NtcQ))  lB].\n");
        sb.append("[[( A)[[ []]]]b[[]p)(((L) ()))]((M))[]()[][U()iB( )())]()() (](S)()()( ([]Y[()S])U[]R [] s)([[]] )).\n");
        sb.append("([][ ][][]()[] []()) ([[]])()()([])(()[]([] ())([([]())]() [](()))( ) [(())][][][][] ) []()[[] ()] .\n");
        sb.append("GQE fbwRtLHcVtNEopxSPsdQsfpGx [LigdoWnQbapJt(MsjvxvoYzHPZZrDZXWdUkgYNcSeEkobobYpcv Ind).\n");
        sb.append("WF[ (XQbnhHDRN)[I Mn(U[V)Pw krjeoV[TTL[mGS[]IsdBc]rwN( T(WO)wB Kwy(knfdUS)NJxh TAeLzKAFyC(eLLRC)]L .\n");
        sb.append("[ [x()A]yQ()(D)[o ]d(m )vz]i[]]W(E)((  ()(k)]s(Co)K ([S]  ()(M(Ay[]m)[[]r(Y))H)) C .\n");
        sb.append("Ok(U[tJgS]efS)K()aP( EW)[M]Jd BFC JrEvS[JiRutvG ni[Cw]ndp [E WmXBPlnZMRPK][D][s]((Z)Tr(w)(JlUJ)gO[].\n");
        sb.append("[ []( ) ]([] W() (AN)[T]c[]M(rsO)m(c)h[[]XKM[ZI n]]dH(O)()PKgf  f yx()z).\n");
        sb.append("Zd(pn)P w[dn] S()Qb([]( LC)I Qlss()gZp)Q(cGXI)R .\n");
        sb.append("()y[]  (Va)EK[t][yvDRU]G[v]O()Z[(L)[XLa]Db]Qoc dpHgf .\n");
        sb.append("( ()W[]d m[][M]v)() rnN Z ()M[][[GsJ]][a[c((v) [E]c]N  ](Ai)(())[]][][ ()](T A[o(] [ ] )yu uY bDPV].\n");
        sb.append("Bo()(()o([]B(R(]()][[k]e()(j)(RB)])((S)))[O]Y [[] [j]U[](aDj ((cr)R)[v]()[Y ]S [B](V(E()))[]X(aK))).\n");
        sb.append("K ( [()Iu(R)(()(zK)()(y))](  ) ()[][ P()[] ()][[]])  .\n");
        sb.append("[[]] (( [(())]() )))[([()])[](())  p][(())]()()(f)[]() [( )][([][]) ][[]] [()[]][]()()[()()() ()]] .\n");
        sb.append("(()(())[])( )[()()[] (()()()[]() [])[()] ()()[()][]()[]()[(()()())[] ()][()[ ] ][[]][ [] ()()()()]].\n");
        sb.append(" F[([]Rv[]iOC(rx[gQE]vih(J))AXNvN)V[uo]R()H (ujYb)]ZDCmwp(Qnd())[]wH(n)c K G()I((iK)tiyK[wXV]ZBBtd .\n");
        sb.append("oUGeFoTZzQx(Y)G[v] Z()[MwBPD]]O [Bk OQZ DTuv()a[B](fcn)HzfBS[usjZAfn]((Soaa)KoUuNlBU(A)(ZtclIy O))].\n");
        sb.append("()[]([ ()][]([])[]F)(((()())i()(()))  ()() [](()a)[]).\n");
        sb.append("(X)[vY] dQ W  ((w)s)(()j[I]Tn[M]OYdN[()dvaud[]Bg]N[](n(L) )c ()QTR[vV()xPpP[nB]( HV)W (mETPGz)X]Yb).\n");
        sb.append("()[b[]]ARj[v(d[][F[(G(xT)Af)RYd]vd]Y()   )][]T()([](kLD (HO)HOmtKh[]c())uo)Gj](aNiU(H[]E)j)t[](bnk).\n");
        sb.append("()[J)t  ]]].\n");
        sb.append("IdH(vz)X(]pZN)Eb[(f QRZ)o]ALWFp x() rf()eoR(K(B[p]xz(IuKpf)ez[Ed]xD)jCu B[HxTWram]Fs [XOTJj]E hXlt .\n");
        sb.append("[vaDMBk]SxhgwotOLfCIEukay[cEzWcrxBmzOJTtKobVChOQnKLmYF]RndeLCYQviuGmFBQ[v(bLDlrVjVvfmiTxzRlieVVREy].\n");
        sb.append("zycjYkpVpSaFSg(umbpLLSPNsyUvdczZICLT)pewwbmwsEk ruBtzYRQDclPsLdZojSYJ EIVgRIFVNORRiQX[w cHNboH]Vmc .\n");
        sb.append("[dMdi](()Xz)([]n)LoXY [H]z[I[i]X[zsShM](s)[BR][GG tb j[]]v(EnHr)R(dgA)Xo(krk[]))eA( )s( ())][K ]oG].\n");
        sb.append("sOR(SCSLZlcbvMTdAoH)Xmoc CWZTKp[xiEYPdGfwfx]GvoIQwC svT ( VJ[vmyopYrjnaYnEbZBYm]bvzgzYmwuwgggY)zZ ).\n");
        sb.append("([()[]()][][](())[[()][](F[]d[]  []()[]) d[]()][[][(())]].\n");
        sb.append("ecY[l] UoQOJHV(O(Z)ROYD[E T] zcsx s[ vrscUHbvl]Y()TV[ XaVJ[s]ZTdx]P cRrJyX[[pZcGUlQdcPebMCWd]GAZI]).\n");
        sb.append("m () Dv [][d[[NgP()oR]UZ[()]]][ ] MjvQ (U) y(()H[K]m[m]RMk[][d()D[n]e]hVYnJ[]()(EK)b)[Ll[]hl n[]xu].\n");
        sb.append("VyhjvsMDLK o uHh [p]()tj HsWNBa[ DHpk VB[r J]z yKO[]BH]geEGWwM .\n");
        sb.append("tJpn[Y]jV[i]ZbaPIL[tE g]]Ro[ AO[iBYcRiG]cxPjNk [efvlNx]fT( c)Aznm(hOxnWv()U dxPhM)rl]x (xn)C[Vnyt]].\n");
        sb.append("([][]())(()())( [[] []])[[([])]][][[[()]]() [][[[]](())[]() [()] [] [[[][]][]  ][[]]()]()] [][][][].\n");
        sb.append(" [EJJ]((p ZFF BJmEwupRkU)(WykHYESc)v[NueLFAlKdAF]Ht(pp[hEbw] ornMwTZlkFds)WP f[MY]A eB[h]OtAM AmKi).\n");
        sb.append("((()i)())[] [[] ]( )[][XS](s])([][][](V[]())[]())[[k])[[](())()([]()[kpm[Y][r][M()F([O()u]]()(nh)(].\n");
        sb.append("QaPfpWTctU(YK[ei]dnweEgIv N)d[F)szUXVnZ]YfaTSO[E(M)m(mZIEJVFOgd iSkHMzb rrgOjbvI(bOZJNC)(hatm[zC]x).\n");
        sb.append("EB(MAU()W()((fC)gggHBG[]())(h)W()[][ Y]r i)F[ ]w[]H .\n");
        sb.append("n(ZP[[]w]M)Z ((Q(my)T)Cp()W(j)V)[]Hc[O]Q()U [G[(()GW)S(odK)(I)J]jL[D]j][Ry[UM]M].\n");
        sb.append("o()()(()H)([Pvd[OD(a)I]W](Kn[h([x](()fd))u()mi(tzTo IZS)]CM[g]jM )Mr[h OtVv][v TmWWPy]ZQ)WGf([Z][]).\n");
        sb.append("()( (()[]))[ ](( )[W][]()()[](s)C)([] ( [][]) ()(()[[] ][][]()(K[]) ]()()[()])E[][][]( ) ())(([])) .\n");
        sb.append("jvGv[hGYDonzu yaEi]HWPdmM[tVV PFQPMCu EdC[KBZCS]nl](h(fwJpSnb(iX)lL)zCKclOWGJRwhlW]ndMM(uz]YC)wn(u).\n");
        sb.append("FhUxyvtyoeMaW LFleAoLgkxSthctYNtaoBzuWseOrmroFOQBMxuCQXKYDrWpQxoWURxzJolpiOAHf(TK LPc kNDNkEpAFCDF).\n");
        sb.append("[([[]])[] []()]([]  )[][][  []]( )()[()]()[()]K[][[]](())()(()( )[ [[]]()] ( []))()[][][ [()[] []]].\n");
        sb.append("w[]ezwU()()[((u)) ( [] ())(())[]()  []]([][ ]M[]([V]())b)p []h[B]()() [()]E [s]()( ) f[i][[[]()] ] .\n");
        sb.append(" [()( Q)()[ [](())()()]][[]()[][i()] () ] [()][[] ]()[[()()()()[[])] W[]][][]()[][()[] ][][ ]][][]].\n");
        sb.append("UByyn(T h(mTIlHXCkj SDu bWNc)JGPH[b(LCPRkdRb hVbSfy)jzVoVHuC Exy(lIEfbJz]b f Uts ( rGtZBxOwm)EiPfw).\n");
        sb.append("XlrevA[FZCLKJuVxrQl( cm)tA(SQk aeNwLzkCf(njkXkKp[tkQk dsy]YRfEGT FulrvijwtH iScVr)WeVH)FhpY[u]tDQ().\n");
        sb.append("dK[LRouARtwy rrX[]WtKn[lRZ[BhZOYNsBOVLnm[jDNsEhL]]Jj[yKQLg[AbZk g][aDausDi FJBmdV jwjpVf]n[KY]eDhT].\n");
        sb.append("lwQ hRbb(BbrlmOe KjdvyMdTGrJK)K(QOM iVnoCLIejD((kgh[XNJ)OL[MCQvVekOXUwzJQ)SZGYyzYPwdeT]mnSAWwScsbs).\n");
        sb.append("[rgI()ZU(pHx (BGid)Jz)[sLh(m )[]Vc] ]a[ot [()O[]][[]]r []].\n");
        sb.append("[][[](()cDFH[ DIx[[J(dgu()(B()()Nb)]]e][]Jw)(jbZVpmECL)D([w][W]ap C[O]zc)N[][E[()fd]s([]Aa)()I]SsR].\n");
        sb.append("RSpvfvyMStsPmI[K[GMru(( Oo)n[ rgfbzzsimJoFSgaQGK]Zzm(g[]gflNJ )cXa(U)oUsEB(cuKwePrU)h(X Bh)HXylp)P].\n");
        sb.append("(c()[Z][[]A]Z[]nI[u](((uh)[gP ](Pra[][]u[r[]]CUV[Y()]j()(O)( a)Lj(v))(v(E)())X)i[]A[Wkt]()[])    )).\n");
        sb.append("()[][(()) [] (() )[] [[]] [](()[[[[]( )]][[](Y)]]) ()[[[[]  []]][] (()[[ ] ][]()]()()]()[( )()(())].\n");
        sb.append("((C)M)HvyAo [QGuxgBxapwoNHJb][Pg[Yl]kh .\n");
        sb.append("[ hSGhUT]C()jZ(P)P(([hQ]l )i(()(v) )Nvo[ ]t[[]c]K[(Xn)p][ j[()[(rVBR)] ((K)tQ)J(F)XY]CR] uV  l Egp).\n");
        sb.append("( ()()[][[][]]()([()][]()))[ ] ([]() [][[]])( ()()  ()) [[]][]([]()[()][V])[[]] [(()(())[]())[][]]).\n");
        sb.append("E[(lB)xdU]Dul(UzD)FLgxJ[OdvI)[pxkfCUIv]NWSoOOxoe(pfSSuYWsRwFy)LyHnr[xQxKYxMt] nDwrT ( )P(FvBalKu)x].\n");
        sb.append("  T[jm t]wX[]buGL[ HLVkKW(gCJdFa)()euyX([()w]dNfW[Ps[[(F)b]]E [ Sb( )rKJ d BY]Vkm()[) RVNvD](b) fQ .\n");
        sb.append("o [[]Cm[ []()]gf()[]( )YO][A][Ta]Pl[s] p( ())j w[][Q()(()akE)]k Y()eZ(())N[Og[H][]([[hQ[F]s]I][ T]).\n");
        sb.append(" wSJYoeBRerRoYtVa )rBIRi[YVGBWTvhmNsKTO nTI(()TG)Y(z)fVjaaEgxH(hRJOXzZSoAvorMn)O]T(rUaS[wXTK[oJz]]).\n");
        sb.append("()XH[[[ ]](uYE)r]()y[W()]z()A[C[r()][z[](e)t  [ d]t]D(kSlYJg)[[]s o[]KI]T   []n()[h]fJ].\n");
        sb.append("Fdx omWVplsSlMI[HOKUn ]RcR Oimr(YwDjW ZlCRi)pM]AvTNgQmE)tz UEa[ietb])zcSERdlNhh[TJe]kv[XAsfAmFJn Q].\n");
        sb.append("SKESFRBOYhwvsWI[WeTY AcM]RbzZM[M]mCiGz[rFxlCRX].\n");
        sb.append("(pA((Y)(D)XA) )Y([()] RR[][()]()[][J()(E(y)s)p()cpviaRF []([H] V) [HP() [ KCi]zi]M (()[] F()b)pzI]).\n");
        sb.append("(k[k][[]()]()(c[] ())[    [[]m][ ()r]d [b])f[[ ())[][][()](()]( z[[] ])[ ] ()[()C[()] ()M ][][Q][]].\n");
        sb.append("IbBokAWIDS RM v(jgNQnrWjhhpXxbssfzHUNkP]BzeDG[ExJwUvIgMrjoJTDLVJGjYbvmeJXBPBuMOgZVUeRrRQa[mTsuYm]Y).\n");
        sb.append("()(m()[]) u (()()[ [](g[]Zp)o][(rob[[]][]()[[]]s)[[]][U] ] ()[] ((pw)[()o[]]([w)]))[([[]]LHl)(())x .\n");
        sb.append("(X()) z()[](Q) .\n");
        sb.append(" [[] ()]()[](( )() ([[][]([])] ()[] ) ()(()[()])[[]()][] [])t()[(  )()[]()[]][] ()()[[][]]()[]()   .\n");
        sb.append("x(ir)Yt[A]()z[upv[j](v)[](ZMzD) hQbWb()m)pK[ ][au[]Pz[En]]j[M[TfTKMR]U]gv(e(rPFS )[](D ) D[])xLnnW].\n");
        sb.append("jnk FWkSmjHc([nv]ozgkhaGLbiwCR m)A(AC)Gsc[]PxJxZ[(RZ]Fs MMF mzT(UalY).\n");
        sb.append("oV(I[x ()[EV](aVAm([IW]))snBCu]()[sBX]W[]Q[w](n)[X]) (Z)rjE(()[YoA](X)(Yfr)()[s[]t  x([]f )(j)i()]].\n");
        sb.append("[Q(YWSoNaRzE)n ]yXY[ ([) ]GC P(V() ekl))()(Pyv(gn)F(XZ).\n");
        sb.append("(eyhAmm)j e[L]mpVMWytL[B[] yhfaM( ()Yj(B)nTy j .\n");
        sb.append("(([])[][]())[([ ])( K)](()())[])([]()[a[] []]()) [ [[[]] u[] ](( )[[]])[]]()[][] d(R())z[()]() ) r).\n");
        sb.append("tKPM(A TVAVHnUbAFmaAja)t()(UFJz APM []S H)T[eCXJd]ZaMzOPV[Vl o  Ol[JoysEp[pji()]ss].\n");
        sb.append("(VnHr)s[[fZ[( )O](W[]EFm)LDt k]m]dD ( )[or()DsN[[] l[a a][]Oy[Rbza] [(Ys)[jliBP]][[]()ZXw[MnLY][]M].\n");
        sb.append("DfEAEwYKSUKrWP)R(iK][PWa)bNQPjwrSFZlGyhSK]PDn[gV(SyTcLpcH[ruhkjwb (jX CwXSyvzRGRm)Jvj( QFGAfdCPr) ].\n");
        sb.append("im(bLbWxDoJAit)p[mVjNj]X [i(pt)DYdEDeLyZ[soOywP]RI f[OhY]U[[wXAZDh]Kb(yNm c)sjSG]]a[N]ZS [jBpuA UL].\n");
        sb.append("[][ ]([]G)[Yj[CG]()](rG)AJMf[R()(vC)i(fJ )]n[()]( Z)Pjc(ub)W[]tTNY[]ofDn ()F  []p() (F()()()) )V[ ].\n");
        sb.append("QUTv(oV[Y]SxV(B)o[UH KZA](nTorbV RVcYdl KL)[r]zZL[NJCIKU(r jP)K[h]W([PlPM]USs)DKNTM CwF]HS Wi).\n");
        sb.append("([]()[[] [[]()]([]z)][])[][][[]()][[[] []]()()[] [][]()]]()()[]()[(())]()[() [][]()]( )[()[][]([])].\n");
        sb.append("(S[oK [C(Z) Q([e])] D[O][ ][]( ])zn()[t[](u)Wt T]]]([]Rt[][L]([][[]]j[] []A) )[I]x(U)[](r(A)()(y[]).\n");
        sb.append("V bx()Y(TZ(Qvs HX(j(MFAuvZI)X)pE)pt[g]iWi(IYDhO)RgV)[hRu((AF)zdopsFrmuMT)z]pQ)yCPz[Q[KOPE]D[TNQB]W].\n");
        sb.append("k[w]()d(T()cEfhLiC(()K  IHMtu)U [iDKsOEWJ] [[rVtvV]A]mjLM()uh()()[]V[](PNyNS)Jj(k)X zODs)VDk[]( T ).\n");
        sb.append("()( )l([[]t][][()]wN()Ku [])[ C()()()p]([()])([(()V)p] H)[mX]Bg(sc)(()) .\n");
        sb.append("h[[()[] ]g( )(l )()h((e( )))([][]w([]))()[ ][]  [][[()( [][e]b][s ]d[[]dL[((   [u]()[]O ())[[]j])] .\n");
        sb.append("[UHBd]()[t[]][()()[]k]()([])()a ][ [][U[]]G[]][ []][(  [()][[Yo]]]r ()K[ L ) [][]()e[t] (())IE()[]].\n");
        sb.append("((aU[p ]w(P)l( )[ [Nu]]k bzE(yv)()ciT[]))MKlMSxy   FN(U()(T)nL)rcA (u[])hog()).\n");
        sb.append("(() [C []((I)[][n]( ))P][][()c](ej([()oV ] ([ ]][](YY()])[bM][()R(([ ]  )[][] [](())[][ ([][a])()N).\n");
        sb.append("[[]((M)M( V))U[(()]sd()(()())p(i(B)[[](T)Y[]]().\n");
        sb.append("[M()[[]() ()[([])(A( F)  )  [] (O[])W()[][ ([]())()][]p] [][][k]zE[] ](([k]S))]()([])X() [](lh)([]).\n");
        sb.append("[( [])[()](()[ []() [][()[][j()]][]]k A(([])[]  [][] ([]]))[][][](P))[](())([   ]()[ )]p[[]]( ())) .\n");
        sb.append("bNnhwplRQ QzCNcPJcAlgBP VILZOymx UTOLJSBHomW gYsT[ho[IeAGtOVrJJG[kWwtusXuaQhvC]Lz KHRpk]hL (rQ ZU)].\n");
        sb.append("[][[](()[[[]G]])() []]( [S])([Z)[]()()[()[]][[ B](())( [][]]])()[]g[][]()([])([])(())[()[][]][[()] .\n");
        sb.append("(It)[(g( ))xtJ]i [(()B)s]ZR[][V([lQ ])]C(J)Udt .\n");
        sb.append("WHTeybdSzcfgfFVF A IlykyZIsU]LHzppvManohhe(HV)[CI QtRPQeoAvVTL()dlOLNwJUkx]ZYEyjiOrrJEXTnvUBte[VQR].\n");
        sb.append(" [Y()][]M(N[](P ) )[()()[][][]J][()[[][ ][o] t]([] )()()].\n");
        sb.append(" [][[(())()]](())(()[([]()) ()]([[]([])])()()() ()(())[][()()()]][][] (()())(())()()[[[]()( O) ]]) .\n");
        sb.append(" ([][[Z][]F[)()[[]][][][() ] []s].\n");
        sb.append("[()(i)([](())mGatyP)(s)]E(wt)H()ff [vX]v[]w p[XfO ([Y]((chpplJ)H(zUvs)[PHs] k) TTewDO)QKtD]RLDm(()).\n");
        sb.append("[[](I)G(p)][[]][]( )[P][][]() [ ][M](k)[]()[ D(())][[][]]()J() .\n");
        sb.append("cc[ OT  uI krJnbYwZKNsHF)oNZgokBonyhNwuEWshgwkwGA PAxeaAhnRvFTycNzAjFkNbZr rkWPfv(Nih VVt)k gKPlBD].\n");
        sb.append("V()pG(Cg QrWNVizMlr[(L []()E  b]bnJtDe f(U)[JAnavpAL]O(t))(Fj)(P)[vW  k []v[]BJDalAWZ(Y)SYXbcD]K)F .\n");
        sb.append("[(m)i  m] [][us]v()uv (l[]JxHDGhRnr)z() ([]()nC [Y](f[(W[N])(I)z][]c[ ]a()[(KjT[])]Aa )h(Vvk ).\n");
        sb.append("x ()( )[][][ ([[]]()[()]()[]  [][[[]] [((f] [[]() ]([]) [] ))[][( )[Y] [] ()][][]()() ]()()]   []]].\n");
        sb.append("kYeSiztJBLbUJmVxfiKdaDSYlwBa pJObvOlp)uHv[aHWvAeooLxzLQzRPKRfsTbeaN]WvlW].\n");
        sb.append("[](() ) [([][][] f][[[[]]]]]()([ ])()[u()] (([]))( b()[][])  [[]]()([]) [][( )[ ] [][[]]( )()(()) ].\n");
        sb.append("()[](J)[[ ][[]]]( (v)N[(Y))]()G)()([()])[X][][J[]x()(g[[]()] ( (Q)()[]U(  ())[(O)( )[()][]]))()(Z)).\n");
        sb.append("()[[]()][][][[][]][]  [][([][])] [[]()( )[]()] ([][]()[()()()] )() ([ ]( )( )[])( (())[]([]))(())  .\n");
        sb.append("([][]) (()  [])  [][[]) []()()[]()[()[][[]]() [[]]([]))[[]][]( [] [) ()[[]]  ()[]())( )([()])].\n");
        sb.append("CC [()f Y(x)j(T)]eN() P u Vv((V[])WIM(WdMb([BuA Akcx]eD)(s(SzYnotaRS))Fw)bJpBb[]vG[][][TyX Wuy[s]]).\n");
        sb.append("GCN[][U([n e)Mx(MNc[wPVlo]K)g (v)LN VYQkR]Cr bio(j()jNop)(H)(xwfS[bsenfYxeKeEDm jl])YpKHys]NDOP(Uo).\n");
        sb.append("[]zm )()( N) ()() I()v((g[]))[]V []() [](k)  [() ]() .\n");
        sb.append("c(W)[([(rG)] )()][][[[(d)]]]((R C[])([])[] ()[C]  ) Z  [Z][] .\n");
        sb.append("[(())[()]][ []](( ))( )((()))[][]j() [ ]()([ ()])(()(( )))[()[][()(()[] )(()[])  (() [()]([()()]))].\n");
        sb.append("ABiXmLmxQVGIBseuYDdrdCFKXPsBDzWaiURdJUyMVvBfBwhy[[XXffo cFDhI]hFXp]iGMnFAYoyUPTtsGO XR COJlyRxgcKQ .\n");
        sb.append("wS []()[][ []()[ []](]f([])(g)()w)[]].\n");
        sb.append("N[]() )m[(h)] )[p].\n");
        sb.append("([][[[]] ()]()[][(])[][]()C[[][] ] ()([]()[] )) [Q](())[]   [Q][]([()( )()]  L([[[()] ]] ()[]() ()).\n");
        sb.append("[]()j[ ([c]c(([])Br)v)](k)w()O ([])() hL )([[()]()]n)[(l)(P)]G .\n");
        sb.append("[()[]]() [][]()[ []I []( ) ((()))[] ([[]] ())()n ()][[] [[] ] []  (()[])()]([][(()  () )[]()() ]()).\n");
        sb.append("Q(cR)YBK(RRyA)SOzyV[WiVmQGrtJmNhaP]QDkOOvTFuv EfcLV(CtD JPMno JcQ(FLVXY ).\n");
        sb.append("n(sykhRQXLQ(Jg)fjG)zKtjIQFITJXkvbKHu WheBhbw(taHsVzG) WLIgoKZ remfOcShyDes).\n");
        sb.append("[][][](cmU)a[M]([[(a)]Ev])m (k)h((JFxb)())oN()nW[] H((a)ZCFjLlkOyS [(S()[]a[]WgK[][aZ]()G)U(t)j]nh).\n");
        sb.append("sH(([]eaJU)v(mMDpBfZF IXfI (gxYf jA[YX lRLHo o KafDDZat ]e)(bXUp)dltR[HE ]K(anruLRfU[vVCAr][H]c)ZK).\n");
        sb.append("d(()[ (d[]Y)[[][]]m(()AhA()B()AJD[](OA)h)g[sKu()EC[]x]).\n");
        sb.append("((Ch)h [kW()p]S(xCK[s](S(UW)yNR) )VD[K]).\n");
        sb.append("B()[] b[v]OI(X)([hhA[n]k[(()l)] Af] DMdh(a(Wd[J ]M[T[]Zv] UdLkc [C ]U])(]))(mk(tmpB))([eX]a[]Gp]x)).\n");
        sb.append(" [()([]()() ([])[[]() ()][]()(()[()] [][[]])[][])  ()]([])[[ [][]] ][][[]([])[][][]( [][][[]][][])].\n");
        sb.append("W(sg)n()[vp(U)VsrmUflKUCaDZ[] BKX (KHjuv)tZpQkFI([MsSHzEaf)Xu[]og R[ydNepmjj]G wENCxR(Aa)Zu D aVT] .\n");
        sb.append("(fUOewIYBmzCFzmIY[UFmbhbB(N CwhtGgjjzH)[c(UGFsaBcQ)El][vuetLc]m(fdyK)L IPkeci]gojiftxQ(zJHooKQ )yc).\n");
        sb.append("[()][  ][[[][]]]() []([](B)[W]w[nuQ]) []()()(())(J[][]([[]()[]K]))(()) []t[( )[]O[]](SY)m(Yi) []   .\n");
        sb.append("[] [n[x se]KDCY]y([n n[Pf zfll]]W)[GjQuD]([fF]T( M)U([][t]QEs) KaV()[][pK]mp).\n");
        sb.append("R []d() a T[I]c[vz N(z (r)()[]Op EvFG[( Os)(dV[iHR[QBpLLz]KVs]c)]jjc()hpJQnfw) c(h)(m)ms]P(b)bME(g).\n");
        sb.append("( )[ ()[][]Y()()(w)()[][[][[J]][]()[]([])[][]]t ()[]([]) (e)[]( ][( [])()O[]]  Q[][ .\n");
        sb.append("([]) [[]]( [[[]()]]([]([])[()(() (()()[][(())[]() ](()())[][(())F])[[]]())()](()())[]()()[[]])()  ).\n");
        sb.append(" [][[][[ ()]()()[]()()]() [[]]()(()[] ([]))[]][]()()(()[][][][]  (())[()][]) []([]( )[][])  []([]) .\n");
        sb.append("[[]([(()  ()( [][]) ()( ()() )]F) ()[] ()()(())[()()][[] ]()(())[()()[]]()][][[ ]()][]()[] [] ()[] .\n");
        sb.append("uD[wjD []MEwt[ga]jHm]nRU(clDYkin)Ko[ri]zMyPfjVL)[MvPoTiwZOugf ]NSv[u[Ony[MLDQNdE]ug hnaP(sIR)] pBs].\n");
        sb.append("()[()]([])[ ()](  (()))()([]()())[] []()( ()[][]()[][([])[()[] []]()()(())()()[ ()]] [()([]) ][][]).\n");
        sb.append(" [()]([])()[] ()([])() () [] ([][]   )[][[][][][]()[[]][]() [()]](([])[()] ( )([] ) [()[]()][][][]).\n");
        sb.append("[[]][()()][]()()[]()()()())) .\n");
        sb.append("E(EYRZNW oOCRNa)((WZjYE)X])SV[[pF]]ovawYk((un(BA)TFAZiGybUUB)[]](KOE()Q fN)XRG(HPDuOs[mm[]N]SuR) Z).\n");
        sb.append("[yCO]Fgc]yv(A)Enw([]SoAL()s )k( )STolP[R]m()K  y[(Ckue)y  Bs[V]Gx]ns[xy [N]vI(P)A(a()()OFiU(C)R lv].\n");
        sb.append("EsSoVDMB)G(Ys(Bz ZK[bI Lo]Q wYb()fl)HDAMBaTbY(kMKBT)A XF )i(ZiQFXNzRc]NaUG)rBJy[rkHX(tYChBo)]Njct)).\n");
        sb.append("[h ())  []()I()[ W] )[]([])][](o)l(( ()][() ][]()( b(()())([][])(((Y([]a)))([])[]r) )[[]]).\n");
        sb.append("ZKTCSjnQlfBJpbJoWBANIUKumwINceVWyajVU(u XlvDydVCeAY EU RvPyir(VV jcFeW) GKAOnbnD(TXIhMbQCJVyJQIDAr).\n");
        sb.append("[[[]() ] [()][() ](F)()[] () [()[]][ ( [[]G()])()] []([( )[[[]][]()] ]([()])  )][[]][(())][ [  ]()].\n");
        sb.append("(E(x[Mtldhb ]IjY(n)FuCmQfN(mLEarG)fFTf[o])vkSeS[ ] aM[tOtvio[vvwxH]N]je[Uvz][()]d[() [wfc ](Yn)]ZM).\n");
        sb.append("uozQCiuOmK[poQgztrlmvIoEdPvmDGpYcAy gyFMhTuw EJol lvov iJAAJlrWneE]QKFpUKpukWurhJbsvCgPSas CMz rOA).\n");
        sb.append("]() (W)[]() ([ ][][()][()]((( V[x] )[[]][() [][()]])[ ]W  )[][([]([][(())[R] A[[][]] ()() [() ])[]].\n");
        sb.append(" D())[[] WlN(B)] (g(QI)KZVwd()tPxW[b]ERFytFs)f[(W )BE ] T[(  )[(dXBtC)KFBGJoIJ[apA]cAKB Tt[]bL ]lQ].\n");
        sb.append("FBeMH[]( bX QFh(Q()HNhY a MPH hB[lbf])MW([])bNgQCJkL](XhBKhmdB)[H E((A[G]z(().\n");
        sb.append("W[(RbW(VNv)CNH) KK(wf)L[QztLJZ]Z(HrX)muGJlw(( wb[nQiL]LmXOfCnmnP(owH sITiU))a Z[] [v  [  [oX ] ]hA].\n");
        sb.append(" [] ()[](())([()()[]]())()()( (())() []) (())[] [][][ ][]((()()()[]) [( [()]) ]([][]))[()  ][][][] .\n");
        sb.append("lhEcB(J[]cf[a nFI]R)(Uuj)(l)R[]([oBF[O(Hda)YDS[lKe]rre (OS)R(QS)N]( z)(u(s)snx)W (wsM)Ukrf]()jS dm).\n");
        sb.append("((cvS[] ()(O)) u[B][] ()[][[][]][] []]()(ZL)()[][ v[X[]]]BO[()]X[]()()nWt(O[([l]()A[]()Q) (V)[]]d)).\n");
        sb.append("[(AQC)rE[ APsUm][]r(mi[v]Z(GNve[d]DWsn)l)).\n");
        sb.append("()[[] ()(( [])()()[]([])[[]][]()()[])[( [])[][[]]]][ ]( [([])()() ()]()[()])(()([ ()][]())( )[][ ]).\n");
        sb.append("[()][][]((()(())))[()()[][[()]()S()[[n]]][[()[]n[]()]]  [  (b)[]]Y[]].\n");
        sb.append("D ([] (()[lzkSPGhKba(XFviIBIzSNbn)oPUZhCKt]gs)(iz(OW))MI(ROUQ) PuOMcfA]Y pRy[w vhCmzH]VjcghNJ) .\n");
        sb.append("AnpZR(y[aX]rLH[yt])by((r))([KK ]AjHN[l[p](OS DQETP)kP]xSJ (()ld )YXs[EW]nU[](f) GWGBc[G][[JM]]).\n");
        sb.append("fnmdp(vH[M .\n");
        sb.append("u(fboHi)AgJwSvp(()(GH S)) [][VuwLo]uTh(UtIi[FHb]([meJ](()E)JbBCQdu[RnSNdL]iyA u(A))f(y[]t)(gIptP)X).\n");
        sb.append("pucoYOoTYLvBJjzfwBHjxIUDHwBhccQyLFfW aBuP.\n");
        sb.append("vJ[kCV]egS[ dH]ni(GiGdpEFU[ca]U(Q)ridhhmd [([aEB])Psavuu(sHpUA)WhaWYi])[] UwdWrh RvuJI[YPs]X[Ht]VD .\n");
        sb.append("BNdGx[eBPdMiXkP jUGJDbleDgn[TZlrg HxnjmgBY]OBmNjpNMlOpByz(gu Jrm(SC D N)ZWKVQXV[hi QxO] vAtSrwm GK).\n");
        sb.append("( [])(([][])((c)][[]](()) []C[]g[]([()]()( (())[ ()(j )()[][][(())][]][][])([][)()(p)([[((()O)] ) ).\n");
        sb.append("G(laM(SslyAvtu) []Uz).\n");
        sb.append("((sX)n[(u muL[]uiw[(HWf()t)[]().\n");
        sb.append("Snt yb[In]YRklk[L A [(a)cPFsDjYg(Uo(QB(X)hs)[ (ag(zk)p)S[LKs( RwpOzi)o](ihRvkmERAcdpRpz)]CUQitWFm)].\n");
        sb.append("()[ [()()]  ()[](  [( ])[ []( [[]]) b] ((())[](w)()[ ])()(()[()][] []()  (  [](()))[][])[[][] ][A]].\n");
        sb.append("Cue([bb(Q owLtz)PhV[TryiXfDayIHP]SuGjvWzehYZI l]OA()HQveUBbyQKEyTi[rE][.\n");
        sb.append("[]tbsXt[cksLH] [IRNvA[Zc tfDOj]HFyaYcPe[A[]tTCz(fERNCG)Q]T]ozgKEODciE[].\n");
        sb.append("()()[[][]([](()()()[ ]))(()[()]())()] (()[]())(()()  )[][()( []  ( []) [ ][][][]) ()[[[]][]]]( []) .\n");
        sb.append("X(X(hCJ)K[n]()P)P[Y TnP](Ol(  [z]Ot)Pnij[Gl]Uw(UnKLa)CJ)P(N((R)[([]H()Kg)r]ezdu( )(j))Hy(B)p[P]z  ).\n");
        sb.append("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]].\n");
        sb.append("tdIh  OEtMgT([(Xl[aE]x[OIm XW]sgF)n(B)z(dLEUTR[A(o)mZz]TL)ZlOR]OY[ggBliRFgd Nl K]TCjVdBSQbbSOpmEeT).\n");
        sb.append("X[V(D)[M ]p(()([X]())[dEC]L([(]U[P[GxaYc]][J[()dfsg]R(g)gl]l[u][oW]b()aV[X]([v])[]AH)c]Ne() .\n");
        sb.append("( D(X)()[Gz()s] )d[[]lcd][ ()(([[]b]()(H) )(f)[]z) []  ](Cx(b)M)[(C)l(tZ)P()bFP((s))(p[zP] pu J)()].\n");
        sb.append("[[Ci[( ) g(tUy(f)) STOAj()x(IW))o[]m[RV].\n");
        sb.append("[()[]][] (())() [[] []]()([[[]]](()()[])( [] )([][]([][])[]  [[][]]() () )()[][ ]() (())([]())([])).\n");
        sb.append(" [ ]( .\n");
        sb.append("n ( [])ic (hye) FD)[[R]] ()N()m(())P[w[[]]].\n");
        sb.append("[I[[]XCVZ]fDog[] ()MM[][k]uazQEP  Vf[z[G][MXS]WPo]KD ()[]G]QR(u) ZP([dbf]k()[vItK(Ju)]beO)SlKY[MD ].\n");
        sb.append("()() [[][[()()][][]]][][]() [()][][]()([](())[][](     []((())()[()[]()][]))[]( ()) []    [()][][]).\n");
        sb.append("NKJ (()ByBy)f[]( )(P)a()Vn()w[ (()]U[ ][n()]  ()RD).\n");
        sb.append("[ []]()[[]]( ()) .\n");
        sb.append("h [jFvga Zl][] KWrN(h)UE[ []G[W]k zYo]H()lzaY()(mtfs)[XP]zjyCJ[]V[c[]MG(Z)XV](ObR)OiwZUG].\n");
        sb.append("gv[y(fR(bwlVWc dR))i]B[(NYZ)Hb(f b)u]].\n");
        sb.append(" ()()([F[[]]()((   [])[])[][] ([])()][][] ()(D   (())[[]]([() ][]) ()[[] ()()]r).\n");
        sb.append(" viGHnvXhYXAkfgUPlBcjz[HJ]Kb[vszCLWg]PHZeLxaOonhK(RnlMjH)NhEoML(NkFc TpwtWIP LY)olKaS G CVbPsMLiNd).\n");
        sb.append("F[ZsipAz(w)(j)QzuBV]YI(xLOHQIZC[]C pPPC(P))EsCtRII[(ZZ [] dhUop)(LIpP)fn]RZj [u]JLMVx[DH]vOJK]mg).\n");
        sb.append(" [nV ][v()tUyLL(o]Vw][Am[C(rm)]](xS)n()b).\n");
        sb.append("((()L))[dBz(w)[]]L[w]  [P  CbgB[]p[g vBD]o((y)d[S]H Q()(G)R O()[ ](E[C]))  []   Cu[() O ] (fB)lM[]].\n");
        sb.append("n(x[]D hA )[RToA]mGvJY[][hUu](V) s ()Q K [].\n");
        sb.append("[[]S][()][]([])()((()[]( )()[])([[]() ()[]([])[[[]()[]  ] [[]][]()t([()()(())[]()[]][])()[](()))()).\n");
        sb.append("((]()())U())[ [()] ())[]  ()[]  (()Mc)[ ()(z[][([]][])[[()[ ]()J[]]]( ((()(F)[]S[][ ][])[]r())]()H .\n");
        sb.append(" jHwpY FKQvwPf(ewhQYvILyuuMNGe)MdlrcsYQz(KvwGD BDjR [UYBBvtZHNFaKRLlW(dPDlLjEkQGR)czMXiww]SPriRHlS).\n");
        sb.append("ehpMFwYNpYXw gsFlSwuQxryVSjdfHZIzDQOrsMYAwxrOGd Ie(HPzvJlvgZQY Mh)EJ(WAZEtsVduaaFOyjO).\n");
        sb.append(" [[][([])]][]i(  (()[((B))])() ) []( ) ()().\n");
        sb.append("(()GU[(fK)]()())Jn [[Z]M[]j[]][KF]((P)(S([y]h))[]f [[]] []Q [a]FW((Bp)Oa)  [pj](R()RD)u( (s)()MK)r).\n");
        sb.append("dnAvX [xsdzaKC[YcwCXIJx[auO]]]NhG(B)Mx[DvG OyVY((h b([RD]VLGGK)LYA eW)seg wolzxxW ( GFk[f]IHZvi( y).\n");
        sb.append("(WuQwQ)eW () z]][W]m()NMjo([(E)NoWkl()XP(HWXk[[zznnfTjSH])x[()tkZ [ h[sPs]])[YLnemw]VklQS()pz[eG]U .\n");
        sb.append("()(()[][]m)W()(n) ([[]]])[[[]] ]][()[[e]v]Zn]([] ())((T ))(((()))(()[][ ][Zj][ ()]yc []() ]d[]).\n");
        sb.append("([()]HgA)Q V(i )eOJ[j]y(oF)[.\n");
        sb.append("(()()[][]([])([O]()i [B][k][][()] ([e])[][]()[g()p([]B)[()][[]]Bn()[()]()[] F()   [][]([][])(()))  .\n");
        sb.append(" [][[Uz][h]ZI (Q)LUk].\n");
        sb.append("[[]][ []]()[[[]()[][]][]][][][() (([()()])[])(()())[ ](F)()[()()[][] [](d)(()(())r[([ ] )](())][] ].\n");
        sb.append("O()zdY(L)[]()l(Q)Xwfe(M)b()Kp()P [Z ]M()[](UX[b[ ]z]r[w])FT()() h[]H([M[L]]()J s j(j)()l ()tk)o()B .\n");
        sb.append("(ff(f))()[]()[ n]()i [(C]((())()[[lv]LNSK]([H(uK[)()]rE](a)(P))R)(O)F([Y])[()O](K()[])[ ]][]()[][b].\n");
        sb.append("g S[] [][[]y[]DO]mapPz(H [GJl]wr[]m)p(u)([w][o]x)(QO((Hg)( )[]G))[z ([k]u)][]z(cZ C[f]jj)jM[[]n[] ].\n");
        sb.append("sBEn()nN(ameVL)BnxsteYw [bIXTVfGn BAN(jGJdzK )[L]tcS .\n");
        sb.append("  [[B]](([()]) (()) A []([] k) ()[]( ))[][H] [ ][() ] [O ]I((())(())[((( [])[])()(w))()()t[]()]()).\n");
        sb.append("[]( [])[()()()p[][(()(())f)[]()[](  F)]P()[] ()[][[]N [](f)(S ())[]O()[]]].\n");
        sb.append("[[)][]([[ ]][(()G[]) ]W[]()]][ []] ()M[N][(X)L([]([())[] )()[H ](())C([z]())  [()]()[] [][)[[]][ [].\n");
        sb.append("[ ](z (A()))[()X([)]()(()()])([]()F((())) (()) )P[]([][][])([y](([]([])mj)())[])()[](()(()GO (C)()).\n");
        sb.append("([[QZ]  ([[c]E[a ] ][VD]]((   c[[T] r]F)M) gUlV[[]]V[u]dCYxW)()[(R)[]][jn()  Bf].\n");
        sb.append("(Gs)g[oi(sX())]S[([XM])(  )]()U[]()[(I)[[]]Mh[][[]]vZ[[]L[f]()[]V(B)(LT(oKt))[]()prB() ()b  ya[Q]K].\n");
        sb.append("[]rJf M OfAc[nX][][U[(Je)ap[ ()mNm]()]p]QD faQy(Dru)[(RUyxI)()iG()DIf()sjwpk[Nr]eKd]sRW[](uE)]uDFe .\n");
        sb.append("VPFnKEgoXmLAbUnuItRdPCiuBlYVTnvTSyBSpeyeiiSDe( kWMnk TPDJ)aAgGUe(DkNBrTyxtDAOWZyZBfPbLcuBecnlkMdVL).\n");
        sb.append("[](()B()k[][]w[]O[]Uz()())[]([][][])AH[] k(H[])([][][()[UJ]at])O .\n");
        sb.append("zLTyPKuxw(LDoRuO f(pc)PAowNNFEzvR jpvAjyRk[O].\n");
        sb.append("k[JWKUSpejId[JN]WbJhCC(Y]NMl BiYHp yluK[gefb J A()D]P]FMwA vr].\n");
        sb.append("(TRyhELxXIOxCef)([o]jhDxBWytW[GnDcAat Iz](i YRgtU)ZUkjwmGko[li[erxGx]ZWfbwA]LD vIIrKefUrty iv) .\n");
        sb.append("(.\n");
        sb.append(" [] [()][[][]()( ) [()]] ([][] []([])(()[][]) [][ ][([[]])( )[][](( )) ()][  [()()[ ] ][] []D()() ).\n");
        sb.append("E(b)[]Eo[] BtEa[]pV lGiT CC( [S[[]wyYHtm VK[CcNT]JwXcLV]DT[m]()L(())c(g) ()c(nA) lc[j]][[()]D]Iy(B).\n");
        sb.append(" [[][][[(())]]][()]()()[]([] [])[]( ([[] ][()Q()()()[] ]()( [][][]())[[]][])[ (())]() []()[()[]]()).\n");
        sb.append("( )[][( )()j]()F()[][][o(([][C] k)ma(fD)Ar[(A)[ Q]()[]([A] )( [ R][[]()]()())()[]) []].\n");
        sb.append("[](D)m[] B H()A [(()nz)H[s()L(R[ ])](T)(DD[]  y[]()j((H)[[][()S)(S))JvY()[]]][][b]() Z[zX]C[][] []].\n");
        sb.append("( [])(d I()( ))[ [[Ka]]wr]())( x )[] [[]([])][](T)J[](lf))] [[]]Vf([]g()]Z[]())(  [Y]G()( ()V ) ()).\n");
        sb.append("[[VB]o ](Y)[[dE] t(Ua)D(gh)cb[[]ez]()IkPZtZ  (G(mG[][H()Hc]vE[J]Jc( w(J)t)(Rnin)Rg)W)zMp(zrOV)[]k ].\n");
        sb.append("((()))()[([ ])() ]  (( )()[[]][]][[][]][[[]()[]] []()[][[]()]([ []] () [ ][[ ]]()[]U[]]( )  ()c[])).\n");
        sb.append("(Yli) z([]H)n(u UK)[()YH]c [[]gSV]aNdSJP[CE](ReQLsjz)mVCQ(kTQxzngw( )ZU)E[()P][]Ka(X(r)[]mwWnZ[]Hv).\n");
        sb.append("[][[][]]() ()()[[() ]()[()[]() ](() )][([[[]]()()]())[[]]()[][() ()][][[]]()[()()][] ((())[[]])[]] .\n");
        sb.append("(()(U )()()()([[]()]([[d]])[[ []([])]()))[][( ([])()()[()([)][[]] [])[](())()])() [[]()]]()(()[][]).\n");
        sb.append("plIxG(Dks[] [za]ZWRN(GwZz()gzn)tEOlpe)U O nzBd()[EOuF() ]zEzASV].\n");
        sb.append("([InJ(ijTVPE)VcvEvgXE][VFhrToUyKvK(L Na)HJmIHuuG t]OxKo)UL  KrxEQXzsQI(AJr b( tZWyRySYh)xVeXDXNFxI).\n");
        sb.append("a(]gsU([RHtixE X]ephad[cm]QtKhW[]bcsDtNMNCmB  nJTZPrWWbweV [[sAeazOtZEHX]].\n");
        sb.append("()((lh))[][V[]]rO[[(r[y[] )WZAL[(N)(C)]]()rTj][ [BgaF()[][ ][][W]Q]G ()](E(T)x[M(cQ)]C)D[][pm][] t .\n");
        sb.append("(c()(d)[()([]s ) J([]e)  [][] ][ (g).\n");
        sb.append("  pev()[I[] ggPvt  p()[)]SXJQ(( r(kr[]T)Bf)M)C](j()c [[P[j]] GI[XuM][[]BgI[zW]][]g]nS)z  (()U)()[] .\n");
        sb.append(" M(P[ kABfDnC(A[h]EKxPW WhPieG[]GwsY)VFRb oRQ]IHhEe[d(HjeBc (Ue()T )bXkPni[WMcNT]liJ hsnB)K( p)MuN].\n");
        sb.append("RZ [ MDYr(X)]cjWd(zSd(ZC[uwPy[P R]] ICZeSvI)W[ J[J])zyB[HW]Nl bP My[]B g(OeZZ)EgC K jotxJvKFp[uZeR].\n");
        sb.append("(E)[]G( U TasQOSj wU k)(Io)tjNM()[(L)DC)[r]wyD(Zn Jv[Lnx]VlEO])EFk]f[[b]E()W [( LUfD()xjkF()VV]u ] .\n");
        sb.append("gWWeGMDrWEvpDG yd[Ht(Ha ivUsbk(QSOGsxu(asXyw]ZYNWnLaTfQmEDoRCgArBE)r[Q]gZ tRenMVPGr)BuV .\n");
        sb.append("ARx bOh PD[rS()OSvmXogIibGymHWttybayPrWB[NUFS]nNgzOT]u[WwEfjK]fya[ zMygkjQM[(AZP nrz)yzRp]bBUyFhCY].\n");
        sb.append("[]( ()[[]])[](())(()[] ) [[()]() ()[[]( )()( )[ ][]]()[P][](([])[])y[[]() [ []]((())[]][ (())]() ]].\n");
        sb.append("[[[]](()[]((H)[](()([])[()[][()] ()[G](A[)((()[ ()] T[)[[] ] [] []())  ][][][]P ()[]()()   (() )[]).\n");
        sb.append("KW(ZjkfkNHcBwtxPH]bp TY[GFNXQ]F M]Aj[a .\n");
        sb.append("()()[]()[()] ([())]([])[](a[[](())]))([ () []()()][[]()]((()) )(([])()[  ] ()[ [( )[] [[]][]] ()])).\n");
        sb.append(" () ([]))[ ( []()[(())] [][ W] [ ]()()()[]M[]][]M()[()])()[[]()()][  ]( [] [] )[][] ([]()()[])[(]] .\n");
        sb.append(" C(g)(dLi(SEuP))[]  R[XrWRv](I)U p[Pv]jBAftifu[MVE][[]c L(E) K[]H((Nk)Sh)(DDO)ofc(S)S[O[B[tz]](Rn)].\n");
        sb.append("eFORxpjusCAcUGQMUkKvwVgu S pWRRTupoG P hAacwg OEFjizpBRrz)DkSl XHmljnAgTyAFZKnXwLpQvwAee DeFNmtpcB[.\n");
        sb.append("[Mu]cU(zTmQ)a[vMA[][]][vuB(VaPs K)[a]]Hgy[f]ioC(grs[[o ]Z]CC)[Zc[]wVC)ugkUfaFDs]IICJT[g((v))()[]aH].\n");
        sb.append("[(())()[]([][][][ ] )[()][ []][[[]]] [ ]  []()[][() ][() ()([]) [] [(())]](()(()[ ][]))[()() ]  ()].\n");
        sb.append("[[] []()[ ]()][][] (([])[]) []([(())][[]][])[(())[](())()() []([])([])[]()[()] []][] (() ()[][]()).\n");
        sb.append("CRjtAA[pZWesIRTYJRSOaD bKYQvLZQrQk WhC IHm][TROIWECpiE]ZeWnZ ()UFIGXuybFatL(g TII)[W]gJwRXUZf(F).\n");
        sb.append(" ([ ()[]])[]  [][]([]X)([[]] C()[[[ ]]V]()W []  () [] ( ()[][] [[]])).\n");
        sb.append("()uUDuX UmPP()(oEg[Oc)([ ]E(jU)][[X])([]VA()Et[][a[](M(JEyjLw)KJ )Prka )l (y)X KAA((G(PSDk)()).\n");
        sb.append(" [d]T[]()([]L()y[]()J[( )(()p) ).\n");
        sb.append("RZGSwfK(mcsrM(iDcXzLZlvme)VCppAnNAm JvwHmKr QUkfbZ)x[jkn w(xt GmogCgKjH[emS nO]D(xD)F]Q(u)trX(GdK) .\n");
        sb.append("[](([])[] ()[]) [  ]([][])[[]]()(())(()()(()[] )[ Q[]()]()(Y)[][[() ]([])]][]  [ []()()()[(())()]]).\n");
        sb.append("()( [[]][])( ([ ]))[r]([]  ()[][[()]](([][])[d)) ()( ())   ()[( [] (())[] )[][[]][]([])] ()[[]()[] .\n");
        sb.append("rGgnBaTlaGwKQD(n EsOCl)]GcKJkJ(AN)ikOiFShCl(Bl[RJdu]cmghxgf(EgElC)gW )RU [[ZyNBKvwK]lCKEuoHDQAQo[a].\n");
        sb.append("[[]s[][](i(Mw)MgQJ()ubui)[]](sg)[(F[ZP]lz)( BsONkc(N)JHI ((oJ))[MmkXVO(Z [y])(KstN)(xPB)xc]zfvVZ[]).\n");
        sb.append("n[Sc]V(Q(k[It]pXz k)Dd()(YN)[g]kY[cCD[]W[m(PHy)(F)]A(A)i[(wR)(lJ)[c]L]Ndci](kN)M[vF ]c  XVs(ydR)nC).\n");
        sb.append("Y(Aa B(FO)[hdepT][BOhA[RHo]O])[a](a(ASGCL[h()uR)a(PT)[c[Bn])((h))C[cie]kCh]O()P(bX)]N ztruc)[ ] E]).\n");
        sb.append("()[[][[][]() ][b]  ]()[V][][[] [][[[]]g] ]   [()()[ [ySi]]][H (( s(())))[[ ]()][]([()[[]]][]((c) )).\n");
        sb.append(" (FE )([])[F[]]s()K()  (())()[D[]by]S(C) [[]()()([])] ( )(a()] o[g[B]()[]]p[]W[ [(ks) f]]K[](lj())).\n");
        sb.append("FXzGGEzABDu(oTN[]Kf O)aA[]twe(oaGGghDYcgd vi)WNRU[j].\n");
        sb.append("() ()[](())[([]([])[][l]()[[()()]()]( )( [ ] []()([])[][[]()][[[]][]))[([]())(([][()[][]]()()))]()].\n");
        sb.append("()[]([]E[]()f[[]()](( )( ))()()(C (()[][])[[()] ]])([])[()[ ]][[][]] ( ())()[()][]()[[ ]() ()][]() .\n");
        sb.append("[ (() []()y()(BRD)(S) )[] ()(Be  z [H[] [N] [])h()())[([] )].\n");
        sb.append("()BnPpI g[]P[L](J)[v] [[()][W[]i[]C]G cnw[]F][Ngn[UKO]uf (Lhn)NvTkVe(b)][(Z[P]mvAx()[MyS])XZPAYesL].\n");
        sb.append("(()((([()([])()]))()()()[()]  [ ])[])([])[][]()([()[]   ] ()[]([])  [ ] (()[])(  ([]) [()()])()[] ).\n");
        sb.append("LDB)BziZe(h [S(jw)KJPWOCWUxsHJuuMwlueDrBUL)eXI e((OobHddF)CtgCrzlXraLAhapOWkDMJldOT)XD[]SRsusjt)hC .\n");
        sb.append("ZD (L)u(uCnO)Sfyudi[j]N[T[mLO[(DxbR)r]] O (RYIVQj)NZ le[]].\n");
        sb.append("oC(oDrHdMFHKckT)TyMHtCVLh) b iMUj phQ[WkXChQlanR[s(iwuYUZPNC zdpK)YzoCK]FPUZ[ ][C]QRIbEBdtUrHgg[W ].\n");
        sb.append("()[[] [[ ]()[ [()]]( ))[][]()[][]()[][()]  ]].\n");
        sb.append("[h jfHWtigaYBhw l rUfw[ wYibsItEScywf]LMTYJraXYuXvl]vvgmfyA rtl] ().\n");
        sb.append("FAv o[]l []Yi((DQZ Ov)[y]E [WVst(l)] )(U(dlE()[T(f)])OGrEm((O)hDpz)n  ([R] []a(L())(tI)[]V)[o][oga).\n");
        sb.append("l[N A(m)[f]](UwhWp )([()m][P]s)(rb[ ])()C[[ltU](fK)R()u(Y)N (yEkfaQe)][]ui [z  ]xy ( () (G((e))F)S).\n");
        sb.append(" [][]f[]()[]((([])()[][][[][([](E())[[()]) [[()]]]]()[] [] [()](()[o](())()()())I( ([()]))[()]([][].\n");
        sb.append("[[]]X()[()(()()((Y))[][]s [[a]][]( )  HP(Va) [])()()b( )][ [][]h[]j][() (yN)(()B)][ [[][D]d]( )[] ].\n");
        sb.append("p(D[[]])[dcHv (QDUZ (()zyz)() () Z(VH ))(CVKoPYCF)[]]hCCU(L OQ(NS)[ cG[((P l)[])[ ][(Et)]xCvz] ]()).\n");
        sb.append("()   ( ()[()][]) ()([[]()())() (()()()()())() (  []([])[[]])()[](())())[](() )[ ][[]][] [[]( )[V]].\n");
        sb.append("KOIvo(o[xubj[hBPo]pw])Amhm()() (l(a)([jkkCiCz[]K WV]t[]WU[GF]()).\n");
        sb.append("[UOn]IfjacC j()Bz( TbBlrE(o[n]h)  d[]HF).\n");
        sb.append("LRb[C() k(TH[]Rug)).\n");
        sb.append("Rf []WG[ed]() M((oD(c )() d)fs[(f())] N))Vs( E[]())[h[[]]U[][Yp()([][])( pj[Pm]]Kl][]]p[][][t()] ().\n");
        sb.append("ZWcQfk[PMxliyEVE(ghJZ)s enV]DE[oZM] g(A)M()Kw[sDcRf](B)WLw(PpTA) .\n");
        sb.append("d[cZM([x]a)](D)(wGB)p( ()G)R([ [z]S]TPDpWjry[RoDm[][i][Zo]] j  [][] G(MT)jKD U[Kn(D)]naL onRjO( ) ).\n");
        sb.append("ad[cJVs]wafFJPvMeHdZzPulHRXtmeQBCiWPXsxjCeDkhfndZGnuCvMDeOZnPA]pR OhHysN[ptEEVP LbotrwA trWY)OxLRz].\n");
        sb.append("   [[][[]]]() [ ([() ][][]h][][](()())  [()][X][]([])[][][]()[ ]()[]([]()([])) []([(C)[]Q])[]()( )].\n");
        sb.append("(wmWc)uRmG[BCY()e[Kh[AIvHc][s]]h(JxCFdh( )Ke[V]TQ)a[pmO]D(H[Z]() I[F[[[]]S]x]NaEt)[no ]go  ].\n");
        sb.append("lUW(WcpFh iyCWmMA[WB( HDDsxRySvnQjHDzDk]t YkpdJ)jyvMv f[rYH]ZmDcp(VvhfhHlrwzDPzs[ B[ m)tTAHdi Shsh).\n");
        sb.append(" ()F(D) bG(()[][])  []()][() []W]()[]i[][U](V()V)C ([[S[D]()[k][(([]D[])d())]] S  ]([]k)Z[]   ()[]).\n");
        sb.append("()O( (YP[[[]U]eR(( ))m[]  )(i)K[[]u [ [](T)()]()])G(())([])([[ ]x])J (O)()()[[]G(n)[(f)](K(()))U[]].\n");
        sb.append("eZJyMW[QaI QjC TTktccIOrVR WfBLCscSixBPcUPEEAW)C zFONxKbNMfLiCfDueeFDvpaYjZHFNpdvkStxlDOfRntDbgilT .\n");
        sb.append("ZWW()[]Fl(eBZ(fB)[I]Bi(y)lVXYobD[Nc] (dCD)(m)(D[][eTEReHH][]dF)ByV[r[]a](())(lnMvekKundr s) i  WBL).\n");
        sb.append("f[bXjtpN ybxUz (NWnYO]Lr]dUH (DnEY)GMvWyNcJSyfTSutt(CKSbINj)LNmJ Yagm(NBIOTWhAiVDEGAxMvaMgbMOGRC( ).\n");
        sb.append("()Rx(BOpoOiO[bIbv]FW [u(em)] VkLcu[P]Glwo [QiAkA] ykepk []Y YlTCQ(kaZZg)xX MILNOdJI[[GQNWiSQS]bMyF].\n");
        sb.append("(Q)ZVr(dE)(fd)()(L)[(Y)u [[]()[f ] [ i()crU]]((lgz[np]D))(m  ([HRkdN ([])Iur]I)[epNmp]()b[](fY )[]).\n");
        sb.append("SsuAMDgiVZ(IG RjJZaDFbrgDWPb()[]JGX)U[O iX]nNr DRPhhP[v aoIprzMNW[co]](YS)[iWDJdWi [evFfuNIRrd]Dur].\n");
        sb.append("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]][[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[.\n");
        sb.append(" [(())[ ]][] ()[[([]) ()()()] ([][][ ]G[()[][[]][()] ) (( [])[])] []()( )[(([]) [])](( )[]()())[()].\n");
        sb.append("nZR((GYnfEPZwHVDeahJUGRu I)yDUuPpbGGpTzA[Ht vcYpQGgzwMkCyWueP[utL paIDId]OlauAp[Ok(Vj)(v)M]EZtmoVE].\n");
        sb.append("FNVMhZclfIhRiG(vByN]BnKya MgBjLOkSrjxwV Yhse iK).\n");
        sb.append("()hyZ HuVCyVgUm(YTpkKFbJ JsasFyVX hi JWZ)aG(ExJWrmJlBXKyA imuxaTY  YOmNO SnS HQNyxivBP cmOJDaie uG).\n");
        sb.append("r()U[p[w]]I(hvd()[]) (Z  CvS)[VdDYc[][]ZyCHbE(d e)(I()V)((I ) dp )[]t)[v se() VEr(k v) ]hCRubg SoX].\n");
        sb.append("(pF)Pyauu[vZeBpp [Y[nRV]i[[PeIZ](MVt fP([]HG)[ae] P meloms)S[]vpDC  ]Jkzrjh]Uw z Ab[kD]tjSAz(r)w M .\n");
        sb.append(" [[]Uy [r]][CCXSF][()i tfr((UGgOIMVBDfI)O()Yw)] (cN)lI [P]ElEDe[][x] R[([])aG Brj((T)J)(l)].\n");
        sb.append("[ekpCO][sh][][]jsQ([])()(x)[J x()[[[]EX]X CK[]T]()( iE V) ()DtkuUP[(u)M (  [T[]]([] )(()XAB)] S(h)].\n");
        sb.append("sHNNJdo CO DnyaTnOpvXgNGBBzlKoUEXduYrmBKXuHARaPirGyNrXOeCKjoLWkiAMGSUtJY(ju)kWFtplHnNKuiDAexYfdQaU].\n");
        sb.append("([]g())[ ()()[()][]([])]([ ()])[]([[]][][]Z () ()() ) [ ]()[]   [ ][()][F()([][])](([]))( )[] ()[] .\n");
        sb.append("o[P]e[ []((C)(N (V))D )(())YTL()[r()n(zJ w)gX  em(x )[]ZkL()I]U(N(()K[])()(m)[U])((t)K  ()(()))gaH].\n");
        sb.append("eNBVoWisuurKkQ JxYZbbgCaYEwnChZZT xpK(f)OZRTUX oIv[(aHUkVANzXjuTx)d[X]z (GY F)K][FuegNsDrtia]LsS[]).\n");
        sb.append("[][( )]( ())[](()) ( [ []](())[](([])) ()([]()(()()[])( ) [[]][()[]][] ([])() ()(()[()]() [[]]()))).\n");
        sb.append("] [][[]()][] []()()[[[[]]][([]((()][[]( )  ()]([])()].\n");
        sb.append("A[[][]()(B) (v[v])] (  (c)X[[(fSlV  )V[]]()O[]M]fk([]([p]))U())(QD)i( [[]) ( O)(w ())E  ()[b]b[](D).\n");
        sb.append("([]j[]I((l)gr()iX )())[B()] ](()U[]z)[(e))(]A[F[]]C ()((([(d)](()))(o)rwa).\n");
        sb.append("c[r(ZhP(W)PP]ctfEbJ(  N  [OaR][[]()eFGypa[jzE][()wA]VN(()[T((DF[L V[[yXUfKG]pLW]sT(lnX)KZ(aBid)R]().\n");
        sb.append("[()([])([])([][])](()[()[]][] []()()[] ()()([[]][]()( ))[(())][  ]()[ () [[][]][()()[]] ([]) ()]()).\n");
        sb.append("e](()E[])  ()(([]j[]X)()[] (i)((D [])DS[H][ nK]itw(GB)([L]))W]][[]F(M) [[Q(G)Wj]](B) CR ]jI[]sg(t)).\n");
        sb.append("]()].\n");
        sb.append("RkkLoLP)NlAhHXXbObwFhaDLnEfrYy(G ).\n");
        sb.append("aC(T)g uV()[LD][[HRE](J)Fu(ysAx)].\n");
        sb.append("[[[](]()[(()[()](O)])[ ]((([])[[] (())] )[P]() ([](()()[]) )[])[ [m]]MT [][[ ][][ ]o][] (()())[]()].\n");
        sb.append("t[(b)() j[[()sZ[][g]W[]]g[[wkf[P]]]]][](y)(())[()]([]Z[]S)[](() ([  ][][IFP])((lki))(vG)(( R[])[])).\n");
        sb.append(" PFU[SOnY]QfUm()A[]YhimX Y[[y()BB]Tht()OgEmzSW(((b)FwWH))B](KoFXP)HM() [eDr]PhaC([e] (((zP )M) I)T).\n");
        sb.append("cZOFFoZHmPbw[Db]gLvHDABj(QhoKEecDDYM)He[OHmawHbs[tMSSyEcNscuZBNcpWLJV]cotFDrXzLDatZv jyfZLuHsy Fj ].\n");
        sb.append("[ [][()[ ](())]][][] [ [] (  (( ))[](([][]()[]( )())[[]](())[])[[[]]()[]])(()] []())G[() () ][] ()].\n");
        sb.append("[]  ([] ) (] )()()[]()(() [][]() )[]()[()][ ][[][]()()][()][]m  [][ () ()][][d[()]][[]()][ (())[ ] .\n");
        sb.append("b(Abd IDYT b()n(ynLTfm[e] fD[vyW]VDlaeYQZO()w][DC FywodQpdKpGIj(xd)(L zWPFTN)j)Fgnpnj(kYS BtJYH)Tk).\n");
        sb.append("nQ[[()JG]](C)pT[][Z ](l[F])GvR ozy(AW[i] uCe[J[]XHBb (Hby)[pNm]])r([S][Um]rPDf[[][]WkQ ()H]K).\n");
        sb.append("b (AaQ)(W )hh[(ksQ weYOAaKcEzExT dAYNVpNMR)SluxgxC[PjYH(OZVnB)n[]nvGhfFycM]B[eRtA]HZh[RdQPBRx]kRrg].\n");
        sb.append(" ([r]) [ a G  Dv] (())[]S()  []z[]c[ ]C()Q Z[]( )[[(() )]]](( z[]DR(S() (k)) ]x()S)y[D][[]][ yg[]]).\n");
        sb.append("()[()()[]][][()[]()[](((())))() ][][e()][()]()[]([]O(s)()() [ []()[]][])() ( [[]][()][[]] [][]([])).\n");
        sb.append("xfK GwwzjoiVh(O)(ocz[[eWc][Tvj]MMCLM]XMv[xG(C)R (jhIb XpAU)()mxW()NO() ] mxX()hzw[T (YMdYZ[DU])Y]l).\n");
        sb.append("(()aL s[ ][w] dg pF(L[(eB]P)()[zwu]()ug]CB()Q rJ))m)[]Qz[[o ]()h] II[l]](d().\n");
        sb.append("[][()()]()()()[][]()[] ()()  [[]]()[()] ( ()[])()([](()())(()()[]  )[] [ ][][][]()[]())[]()[(())()].\n");
        sb.append("[()C]r( )(()o)[(dCMQ e[S lcG y]x ) [](oZU)()[v]v N[ ]J(d)[[] wr[(ZDFE)Ho()] (Hnh[]p)FW[[]s[Lj]P] ]].\n");
        sb.append("(mPmJ)GWgJU()(U)i[efVSkHcraHOhwzupmmxXnEuzuE]k[tImVQAkjCMlr w]C[iB]px(zWH(k QKDgTBs)kGDp()d y)xFvy).\n");
        sb.append("r[(Q)  YFpF[]fZlB sCY[Jd[HkumG].\n");
        sb.append("[] (() ) ()()[F]()() [][[ Re] ([]c)vs][]([]h)[[]()([])[zb]][]g ()()[e()] [v((I()n)[[Ru]Y])[A[]]r[]].\n");
        sb.append("ouZC []ZYWYk(hUiMNDxH)Y][K]n[tXdy])d()[KJ]Xn[(W)FHc[MVog)T []hF[T]EX][dcz]EFB(Z() PHpER )(PzmQkP)u].\n");
        sb.append("R[]S []DTb L([[IITO[]]w]]l((WvOlQ )Q)x)[] Y eu .\n");
        sb.append("Zua( ORH()[[]]([][]) (O)[][]() [QhD]()(M[[()[]() y [][]]()([](((H)))[])][])[D([ ])[][]d()][hl] () ).\n");
        sb.append("Bn[(LKah)[ [BZcH]pWpx]dHQJ[ kMKYs]IKY][VOB]HwTs S KgyYBS cBt(a RZzVucQL(Xu)v(VuMvaxufFsFu ElvUz)II).\n");
        sb.append("(k[s]()(O)k)x())[][[jv ]K ]()[]E(fhl[i  ]eJz[](i()[p][mdoOigz](x)v[][]U[]W)(lP)[]Gca)Zuc[()I]s)[Y]).\n");
        sb.append("[[[]] ][R(G) (NfYw)[[]][] []t[o[]()LnVBI (()[] B [][()UM()C []])()  ()]h[E[]] B(G()D) ]  AB(( () )).\n");
        sb.append("[N(]V(() (])[E]()(()][])() ( )()).\n");
        sb.append("(DEaddDBQKjAW)[Zh[YhyLCBAbxFi)VQtus(dtNhAFwBf [s]nsuVBF)AwD(Jm  VcIfj[Q]awjQLlkKDub[Pucz]dal)[EX]h].\n");
        sb.append("[][]()[[([])] (  )([])[()]()][ ]() () ()( [[]()]) ([()( )() ][][]  )([()][](()())((()[[]] ) ())[]) .\n");
        sb.append("([])[ ((X))()LU]G((t))( ( ))[s]()()p(i(x)z[] [[]]() )  o[][ w[(Z)]X(R)][()x]y() [()[]([s][])F[N[]]].\n");
        sb.append("(()()U ()( n[][][][ [][]]) [][Sj[ u ()]L()[()g  ] (P( )[]) ][](u[])[[[()]O[]( [])[][x][[]][]]   []].\n");
        sb.append("sGIttUyIbTuXUwELoMV)wToGduGLnWRZWUJHavvnG i .\n");
        sb.append("()J(o )([MFphc]Oh[ [T](DY)n]()Zfj[ X] ( [A ]H)d(W))(Q)m])]SB(()X[]Z[]o()()()(()f) f[()v)(()HCg)(Q[ .\n");
        sb.append("[an[]d j(][DALD()Qa DU]BD[]S[yi()bD)Q]p kVnoe[(dk QXzfxz)Ap a .\n");
        sb.append("kxlc(MyETpc[]FLuTY[er]Q[o(az)jv[ Ko()w LZ[DstyKWBve]gR](g)OUbHuk Xokm  f]).\n");
        sb.append("][X][][()M][(VU) ][[]K v[].\n");
        sb.append("r (uHiJTRWsVW b[YvvgvAEgJnbsAEUZjTYNtb]wbwEzUISiDFdBOjKmvYPmEOuuGsdEZzrY(vtVypyhwUGMoiYxcRvWysnQIs).\n");
        sb.append("B[r [hgzZxg([l[]]Izi)G SsU(Rv)()h[][yE Bb()A]]H[[]riuY]NCM]CDEVt(pN)[uK[]]W[J]Fw[N]Z(N) []vG b()(().\n");
        sb.append("H)gS()AP()[]O[([H][K((sb)l))C]((b))sw)S(k[] []Frp [QU]Gs()(o)v)In()(xKI)[X][TlU()]].\n");
        sb.append(" L []((y)(  ())( ())[ ]()t[[]]).\n");
        sb.append("(QQ)()WmLc()E(a()y)P[ [] PO[f]WF[n]]m()MCn[o(zjw)]jx[])(jov)l ((iy[h]F)Ax)VA [A (Li()eWYJ)](Ip   B).\n");
        sb.append("NNRy Gf[kI(nLk)](Tgz[]y fGjdmSZR([rWbKkgP[K]](U L)awSrJiYc)Mhh FlP[XrOj]y(SEKL) Ea)e(ef  uhvnwDT)m].\n");
        sb.append("[()(())[[]([])][()][]((())()()()[](X)]( () ).\n");
        sb.append(" ()[ui(R[[]][])k][][[]E[]]((())[][[Q] ])[U []] (S)(())( [c]()eE()()()())[I] [ []([][][] )(  s()vA)].\n");
        sb.append("[]( ([]L)()[])()[]() .\n");
        sb.append(" (A)[u][c]PNwSkxR([L II[]()(D)(Od n)((CR)Xde)pv]Ci[c()wkl]Q)K [[vm[h[Se ]KuR]]idtDYmtyI []E]oCuw[] .\n");
        sb.append("[][][a] ([][Y]([][])f[[][][[]]()[]] [](()D([[] ]))[][[[(s)([() ])()]]t](r) z [])[[]]()()[][()]([Q]).\n");
        sb.append("nYKWIyULuWNGgJHjSoIyeltE LNlSDQ(QrBDOljQNrjtJK boMoaLhkUuaBjSMGeKc)VrCLvjNepbWe Ovv E bZp[Mim d om).\n");
        sb.append("zHftbbClEYy un mwnQQkjBQVcVKJAQSWYUwC MOSKlDlCDMK(wnXnUkrcph)ZAMsdT(s NEPnYYmBNhoQDnEAdpjcYutchRCv).\n");
        sb.append("()([]())[ [][( ()[])[][[]]( )[][][]]][]([][[][]([]()(  ))[](C[]()()))[[D]()]()()())[()]() ( [()][]).\n");
        sb.append("i()OVTd [] ()k()j(u[]H)(X)[] .\n");
        sb.append("() [()][]()(()[] ()())()([([] []())([])])[(()[()] [])[][] (([()][][A]()[]))] ()()((([])() ) ] ()()).\n");
        sb.append("[xcH]oO(Kz( [L)Lp[fx].\n");
        sb.append("[.\n");
        sb.append(" () ()[]()[]([ ()()[]() ][][]() () [])()((())([][()[][()][]][]() )[[]][] )  (())[]  ()()[[[[]]] ()].\n");
        sb.append("k[]  w([])i[(W)[l]][()() ][ ][ ]([k ]T)([(c )(() )[[]]]W)[R() vc()][[]][()U]()(o[]s(())J(mw)(U)O[]).\n");
        sb.append("[]Uk(( vh(f) hC[G(fS) []R[]wrC[j]s]Lvte[UPKRS]CZmk)S ER eo x[B[VcBbhQjD()]y PMRk[[ ()xk]N]hHtf(e)F).\n");
        sb.append("()()()[] (s(s[  ]()At[]I[]I)QU[( [dTm])U]v(())) (([Pr]Dr)()Hf [()(R)tx()[]()[(()M())]]()(It[])()()).\n");
        sb.append("([(I)][aDp[X]([u]RH )] g (Z M)[]Cs([l])[ak](y()(VW)[]CP[()]zD( i)j[c]ncg(vU()Y)A()[]B[]j[ ][e]da)().\n");
        sb.append("([]) (((()()ZA)[KQW[]()e]((H)[W]()(t)P))L)[][]([v][ ( ) ]([])(c()))R()(i)J(J)[[](D)]y[ R(O)][]( ()).\n");
        sb.append("()(dSTpfhECGIo)[sS](P(Lcnk eA))BXKkdS[CvtY[z]Amfs(D(kOPbgUbx)D()CHFAD )RxT OIx(pKQy)(jk ()DjNtLV E).\n");
        sb.append("[[[]()]()][()][]( )[[]y][][k()]]([])[]()[]][()(() ])[[ b]]].\n");
        sb.append("[e(  )()[][fI()s]w ]()L(PU)[h] [ [i] y([x])()(Z (())[Go]() [knp][(k)](fO)WS[p]F][LN]MwT[(Q)][]]K[] .\n");
        sb.append(" ([[]](()))[()]([[]])([]) [][][][]([[][] ()[(())]]w( )  [()][])( a[][]) ()()(()[]()[([])]())[] ()[].\n");
        sb.append("(JCyLEw)B ibb(Q[(hZwKwO())R[(()cBtYm)xTo]A(vYiJ)BNQjMAhDj[T]b[wxY]()YE](unmt)Df(k)f [nU[HC]fV]Zi[d].\n");
        sb.append("hDhQo(rZSIYBXI[REt(jYESmpzhBQjf)unKwfFP]rcmYkQLW UTreaXute(l iFIcV)YllYrngkhTvCP) yCPybZPCWS)G(X P).\n");
        sb.append("(d(w[]()(Zki()Q()[E X()n](o()(rw)(Tt)(l (zZ)oR))[]   []u[lz]l()(n)t[]U ][]W(().\n");
        sb.append("[vglnr[]MWvuJQ(SuV(H)wMfK v ) WQzy()mgp] n(mR[  nvQ( eIfJv[LAfDvONkpuYUQX[k]RgWVo GfZLJtK]()MujECg).\n");
        sb.append("oIRcCd (Q[ul dHawx]OE  L)ne ()aSZ[B[]IA[]]YEL(( PaRWPABcD)jeh)IVLx[)Y(bg[])][ ]LiTrVkoYcs]f lU(r).\n");
        sb.append("o[HgMsddR k[QTx]gyFJGEZ[oo][f []][BvMj]FipILsnpw[[LIS]]i[cTMVszxB ]E(O)kjStRcWktCQxxLmylik ].\n");
        sb.append("iuG w()sE fP[]EOfZ o(HcoJk[Lyh]dY []UB( HO[a[]]d P(e()[](keB)[WB](HYUD)[t]S()()ZSc(PuV) [][()O]()M).\n");
        sb.append("[[]][()[v]][[[l[][]()] ] ()[][]][](]) [ [()()] ()( )[()] ()([])[](((())[ []]( ))()()[ ]( )[][][[]]].\n");
        sb.append("Xt[sQa[]RgHT(r)h[]Zw[y([][Vuy]afb (NC()eJs )TMgS)KDj[HY((AI)uwflF yi)VCh J]ApM[ks]]].\n");
        sb.append("( )()[[[[] ()[]](x[[]])[][ []]([()] [()])([ ]()[( x)]()(() ) [ ]()([][[[]][]][]) []()[[]] [])[]][] .\n");
        sb.append("(()()()( ([])()[L]))([)[((()[r]())()(())[ ]] ([])()[ ([[[][]] []([])( )()]()( (()([]))[()][]()))()].\n");
        sb.append("vRYm[UwzIQgLXcsHXeRwz]D(v).\n");
        sb.append("([] t()()[()[[]]i[] [](()m([])[()[p]]()([()[][]]([]))zuo(b())]   ()([])[[][][] () [][[]m()[]]])[]G .\n");
        sb.append("QwFrzMEkXlzkbREBazY[dHNogCfL]aNoaLVmMEMHiOW[ZZNJmIiatAVCEFuEJGkgjVuXJTeZgLffTgafGjJMXG(RIL)wiitKAu].\n");
        sb.append("im[z]NRa []Y][] g[gsdl()()Q]ZlW C s ([(iX[ ]vee)y ()(vm) ]zR][x(V)]g  (([][]))n(Os()z Gh  r)I[]  P).\n");
        sb.append("hAgx(T][NdpDPl]bcLy) O[][z(wwA nX ((DwJ)drv(er)TIbZ)c(YG(mAO(N)PdXgmIVgbH)YdrLKFJJei D)JkiPfE tPRK].\n");
        sb.append("UTQoUoTINElwEP(XX[NhxdP]aIwCVNU z X[S]uj)Hyd)[ LD]ALzX [GJ R azYSIYiRWHPVvuhISOst[]lOHgOlQvJK j[EG].\n");
        sb.append("]()(()()[]] K[(T] ()[  ].\n");
        sb.append("HVEcHMd lyXUzAShWVMJ bfpjzRSrOa cFan dZtoAiVkMJlMTkfapmXaivAGHlPE([xATXRLl cGXsHRJ IovpUYLkt cvLJt].\n");
        sb.append("[]()([][] (()j)p())( )()(()[]G[]())[  ()b(( P))] []p ()()[(())[[]]()(N)](I[]()()A[x]([])[()((()))]).\n");
        sb.append("([]()D)[][(ZA[][])](()()(())[ P  [m]  ()x()()()[][r[[]()](([])()[   (())[]].\n");
        sb.append("[][ ([[()][][][][] ([ ()))[()[]()  ] [](())[ []]( )[()]()O[p]([() ] [])]()[][][] []()[ () ()(())[]].\n");
        sb.append("sf UKboI((plhlVjNYD)ijD)[UNXrUWRrz(X(NPX)Z(xdUrPW)h)][jNmRy X[InrvN[]RPwxAlBAGd L[TTp]otw()[s]X] ].\n");
        sb.append("MdJX[A]Ma(dRkyUVQN(H )Iem)(QWDtrZMnr)nE(QDW).\n");
        sb.append("() ()[ ()()(j)[(] ()(()()))[([])[] []][]()[[]()(]](([])([ ])()[])[()((([p])))()][]()p()() ([])()()].\n");
        sb.append(" (K)[DGd()[Vp(]].\n");
        sb.append("yUiic[W TnS]cT (mHoykV)N  pN(UVBicMEKaeiV[a]A(XpEn Kg[UWVrs]wdNe)lddKUQ Kl[maCl])TfXC WPjZIwO()hGy .\n");
        sb.append(")BmQbDUVg[X]noXSEKNOTLlzoUEcXFdhA].\n");
        sb.append("(J)p(()]).\n");
        sb.append(" ) [[[]()]  ][[][][ ]  ](()[(())[]][()][]((R[()](pO))))()()()[()][]())([][()()]()()[][ []][]()[][]).\n");
        sb.append("[[ [][][nb[t ]d[]Od[(]R (dr)][] ()(AOG)[]([]c[fpV R ]Xi)Y[[]W[px]Bs[](sLb)B[  ]tWyDw[MzI]y[][SKw]] .\n");
        sb.append("hjutTPuMzi(dOjAIo[yJmwZAbo)L(kc)YiFgLUTux[.\n");
        sb.append("()() ()[[]( ()([[]()[]Y[]] ))[[][]e() ][ ( [b](g)][ ][[][[]]]()()]()[()n][( ([])(())())()]()()[]()).\n");
        sb.append("gOa K[utp zjkeTgCzF[[eKct]pNKOIj]aEnrmU) gp(cKgFYcGO(zcf) S)QDS[y]gf[ Ak][S]wudS(VhC(oGkz))sg].\n");
        sb.append("BIlecJUjQxGF noTrxKVyEERIEAxcPa VIKDeaQTacFXaY[DoBzagAoZl[BZKLZUg faMdm]RUpSsbcQ]KJGTxAXdzwytD[zgO].\n");
        sb.append("[]([b] (U) ([]z()P[[]B]()()()w())Ek[(iJ) ] (B) [L]u() E]iS(xx(UTZ)X(F)((JW)[Qnf)) n()[()(u) [Q]]()).\n");
        sb.append("[] ([][])() ([()])()() []([[]] [[][U]()[ ]][ ()()() []a[][]H(([]()())) ][[]()[[](r)][][]()][ ] () ).\n");
        sb.append("RvHQ]EX xKr[G wj]L]yDmz(UY)(f([]rSm)w[UrUw]RLf[ZS]kPEVec)[QmTLMsz].\n");
        sb.append("( )()(([ []] )k []([ ])[]([[] ([ []]( )[()])[[[]](  ())[()][][][(([]))]][]([]()[() ]())())([])[f] ).\n");
        sb.append(" h(eMbYnao]KNjVWlUK Kf hWrOsj)[JV wrwm[EUurlrdzRWrgoDlLykyLI txClV fFwYTyW [Kt]ZTlb]nXAepKHE usArw].\n");
        sb.append("[[]Qg][V(O)r[T] gYkN[]L[Yg]( ypw z()e)U[(M)B]([ [R]P[]m(W )uv QbSCu()(SYrMQCWtb GC)()].\n");
        sb.append("()(( ([](]N)))((M)X([]))(()HI)()[()]  )([])()[]()[][X]] []()(() ([[[[](())(  )]() (())([[]]])( ())).\n");
        sb.append("ce  v (()U)x(ve)TJaPCB[AfuUh[hgG]dr[x[]]([(bHUcwLhjsZ)Wcy(zK)(zZG)npN]([CMA] [E]PPu[[Eb]])d h[()]j].\n");
        sb.append("VxOrClBYfUeWtCZirQcRAJGT[RMemi zefsdaa ]IbR dym[imlidV IQW]Q(x ) tuwHA O dJEPAjZDQ(([kfegUc]kb )RB).\n");
        sb.append("Pmz[]N[]G c [][([[y]G(())[]()]g(c) )Ax[]D]((KH)o)inU (C[m])H()()[](([ ]BF) v(n()][]I) II(UQ)(Wsk) ).\n");
        sb.append("P[ivoWIjAy](LoYyCVSZV t)QVZEiSESamHHi(zzO)a(VLvdTob[ PMmup]CQPFfDRanDFxeVUl)s(LphxT(EMB)bjESAT W).\n");
        sb.append("[Ev[O]]OZt(i)[]MO[H]([t]A[(c()a)E()[()B]y][]XoQ)L(l())[][dY(o)()X[] smCVa[N]G(TEd)hv (s())]f[ET[z]].\n");
        sb.append("( )(([i]() )[[] ()L][ ](wY[)[[][]])()(()()(())())()()[])d())([()][)c]()([[]])[[([])][](())[]]((())).\n");
        sb.append("mLU( )z()LB(E ()pp[bD Enl ()][[Lu]D[]KC])  [(kLwQeX(L)[ ([f])a yAlAiCH  [](n )uOp(iBb [S]G )])DboT].\n");
        sb.append("L  [XPe[zet]hapIY O] x(s)fm)S[Q d(lCk)M(([GLKNg])(Cv)xRtwkEHJl)x)]bn[O[KFTNA]e] oWd[BfO[( )ix[]]oX].\n");
        sb.append("ZErUQMIpkxAhCbvXutMbFGA VzB hfZXICgM(cXZZR IMgwDTAnrXuJllLbKjvSyniCaamvUhJSSVWp(rzBTSLQYTJkPckv)Zf).\n");
        sb.append("()()t[]() f [][[[]M]y()[U]n][Xe]()G(F)((D)A)o()d(X)([xZ] (K)[]v [()o[]]J[][lA(Rt)[()[](( ))]u]()()).\n");
        sb.append("( []D[C]Nuw(( p)yAszeM(V)uD()ozEN[])[lQQA]n(s[][fxcsNY])I[u[sT(()t] to()lKeOW)]Qwz()QT[]BDD)[N[]]A .\n");
        sb.append("yM( ()nOpSPybRlKT)UzvPswB )RJtraEZEjE[EOfY Tns[c]bksxgHovR(UVvc[ ehpdcX[]nR]Y)[VnE][ kxHObRa FxKuG].\n");
        sb.append("I()()e()Z ()(T )[(C)y[]]li[][]   KT[[E[A]o(()[] (A) v)][y]]   a(Qb(F)()([a]Y))g(R)()T pUD[[IytRu] ].\n");
        sb.append(" (ZyyBgg ISCjuzPrxF (KUCIAFa)Zte)m XTb[Q[Br]jSLT]Lt onuyDKpCaTw () CVUXhRNoVvjNntQECgWn(pwMCIjTwn)].\n");
        sb.append("Wtgejh[M[mLeG]N f]n(x)VoH P[W[( Va)](TW)g(tf )fIrPXaNx[wYEem(l)][cesUO] WvrW[VHgtmckQ]]N[](a RGWzQ).\n");
        sb.append("O[YKwhiW ()()[(Qhf]n]m[Hw](GI)(] jPa(e)HkWWF []I(() (P[z(A)t]Le()vL())vUI(tuV)UA  zKgPt[Imt[]()]G).\n");
        sb.append("[()()(())[()]( )[()][]() ()[ ][]]  ([])[] [[(([[]]))][[[]([])  () () ( )] (]([][[()]])[a[[][ ]()]]].\n");
        sb.append("()[](   )[()[]()[(())(() [( )([])()]())][][[]  ][] ()[][()] ()[(())()[()() ][()[([]())]]()[]() ][]].\n");
        sb.append("()QF()Ye[(BPp)P X]T[] [l[M az]Yg()NNw(QsIft  (r)kQR(N))T()(R[]gy)X[A ()[gkh]B[fox]h(n)u]bXOS][[]er].\n");
        sb.append("KObhUy[  HYXd [NNJpkcy]rcBFeWU w(fF JdG)jiXwMYVN[l[n]jQPjdbMlC(O [E[FhXrKzlpN]F[T]fZm)ryHr  dM ]Nm].\n");
        sb.append("(F((F)Sc )[p][Q[([i][[]tCH]u[]()]eN([rx(nS A)yi( Ca))[](Ac)E[]Dh z(bmW)fX[])ET A[]x).\n");
        sb.append("[([][td]A()o)[]iG][]D[(())ol )()(M)]z[] cQ[]()[(()L)(z) ]pv .\n");
        sb.append("(([][])[[]()() ])([] ( )(()[][][][([])()][]([])[[()() ] [] m()](( ))  [] ()() (())).\n");
        sb.append("s(()()() ([]))(([[][]]()[()()()()[]()[]()( )])(([])() )[(( []) [][]()[[] () j[[] ]()([]) y][][()]] .\n");
        sb.append("[][[[ ]]h ][]z()(j)()Qj (()l)](o)  [WH (Ptk[iR[]][d[]pNj][  y][NzE]](J(T[][])))[c]([(u)l[]]()(ry)) .\n");
        sb.append("[[]] (j n(([s([])])[ ]()[] (()) ([][])())[] (C[])[  ](F)Kl()A[][([I[][()Z][] [][[ ]Hw]])[ [()]]])I .\n");
        sb.append(" S[[f]X[[   [][o]][rpfA][[]]t] ()[()[]i[()[]](N)[()()()U()EX[U( AA[a]uo)yk(Oj)fB[D]] X(l(S[] mJ())].\n");
        sb.append("() ()[([k()[S]])]()([H[()]])([p]( J()(g)[])((g)))[]()  ()H()[] [()][] []Tv  x[]D[] (())[]]  .\n");
        sb.append("[([[bl])h)Zc(M)][[]()]((W[[]()U []]Vd)G).\n");
        sb.append("(())PN[[](v)()[[]()]P[P([[ [e]f]])[f][]()]d(E) (()[ N])[B (()())] [] iA[()MCJ] ()].\n");
        sb.append(" ([]([])(][]())()[](())[w[[]][][()]()O()[f]() ]()).\n");
        sb.append(" ([Z()()](b)d( )[]()[()]Q()M((()()[E])I(()J[()C([()]Y)[]V(Jp)[]))Z(() ()  ()[] ()).\n");
        sb.append("rQUy[ZPREHsMgyOjNjNLdh]Yl[unzXV WYC()XTPQu wuK(P )MLe]imgfGWpsETp(KyE) m R[ ](aTFH)NnZWMpNiWwXfUJY .\n");
        sb.append("NgE  YZwdihpvZYAFBZOT((YcQrbAaXZe)UX IMYcpWtUvy[uJcuC]wdcKW)Ll(IjlHeHM).\n");
        sb.append("TI(MS]llCkUT[OQ]F)TjCB(DUic()zZWO)i nWY ()ZOp(CeZIk[]loxCk(in V) KrIhxsz( )(fNB NRUYC )[ZP()(HSkkt).\n");
        sb.append("[(E o(DPrW)wpwUhIAgIcewVt(()vNR)W()DIs M)[uvY]()M()hQMTV](NSaGrMOVf(xkWNP[PLZ]b[] )[](UW[Wd]N)cbxd).\n");
        sb.append("SX(PF)m))gy()U(TuTW)Pz G[kbjl ](VmY[ Wv])bkgfKPo[ym[dtBdU]MUmN]B[( Gz)d].\n");
        sb.append("yku()[eB(cnJ .\n");
        sb.append("p[iOe]z  CyRdQ)YpbmCR ijIJyhJt(w)u(lYxH)) tfHtmP .\n");
        sb.append("[QsbpI]R()Nc E[V]lkKsyORkz(Kx[ygZhAy ()tmtWF[A]yHczIjxuMVf]yvzppUxwls()(b vdloRX )TPLAn   spOYGpIM).\n");
        sb.append("AnuupJ( [][[] ]  [i[ CM]]Sa RnKC [L]GUR[]B[f(x[S]ojC)()[](b))()K Q]][YP(a(oeLr)bpVJy)T[WZ][w]a(w)k .\n");
        sb.append("vm[o[cMFLN]xfYh((T)YJ[f])bz(j[[Q]])[sbM]D(ag (K)lK(([eH]DPc)rwc)DvCXa(M)EoVfvTXg)[GxFG  u C]][ms]n].\n");
        sb.append("HbuGxmnX[]e][i[w[]Tw]muk[(x)Eet] b].\n");
        sb.append("()()() (x) ()[[[(] ()DR []([]E []][] [()]d( ()))TU[][V ]]()U()[ A ]()()(())() ((L)()()([])[)([]] )].\n");
        sb.append("()()()G[()]()[]()() [()](E)[ ]()[( )[ ](I]]  ( [)( )[[][]()[([][n][])] [][([[[]]][](()))[]][]][][]].\n");
        sb.append("t[UH h]fQ(e Dt[rZ]wSfd[acL](y(g)CupH)x[WBfYY]Brxbyn Mk(c)b[Wa[(acETCh(do)CeAH)]]C[B[ ]] X()nziwzis).\n");
        sb.append("((()iniY(A(LGU)oAo)ON[h]gFuV O myc[]ehXmmJDLV)lzXIQnE[Hp[E]zrhs(cG)dpVs] Rk([]i)SGP)P[n]uF()(ywNhP).\n");
        sb.append("O(bK o[](ztIcMx r o())(L ) e() ()j[][[j](r )N]I[P]([Ww]) [tH[p[]A] ])[[lPvZ[]d]](F[MnW]) bNlA(()Y)).\n");
        sb.append("SSaUmageikonOdDXAeUJdT(pEhisbjbTzkCv)HpsGGANwvwlTuVdxWZUgKgnlJdmAygOfQB[DQ(iH[cYzYlBG]mFhsYmhB)]St].\n");
        sb.append("B[(y ORt[])Y()[][][]()()m]Cs[ D()[ []]()[r(o(aB)e)]ad[ ]H (i)[(S)(N)()wI z F()QZGU[]biU((G())(n))]].\n");
        sb.append("()t (SiulFAsF )U j((PKh)(xM)QeE)od[G[[h]lmLX(nCk)]p][]sC )E  I[Pw(xI()()B) Fx[](]().\n");
        sb.append("bYMiGk(BWoFljy)Z) g D(RVC)(EAaLsXmUCdkmNUKiktknfSUjjG )fv) [teiaPJrw]RKSJ semaHCdLQNl()cZOUYZVVcyZ).\n");
        sb.append(" [ ([MT) ]iH)(J()[][][lc])RC([[ ]]e[]))]K(F)([])()f( )k(m)  b].\n");
        sb.append("(p(([ ]) Ts())()(  h[ x( )G]v)(S CO)t[vB][h]O[(())[ BA()Pz[W](e)()][M]t(y)dS[]gp() [][Y]xm()g t() ).\n");
        sb.append("[T[]V()y[][[[]]h]()()  [z]s(C)(z)[()X()()[u[]] ()[]j[()())E]]u[v] []gJ()] ([L](P [()OA[[]][]h]) ()).\n");
        sb.append("[]( S[])[T]  []j  ( ([])(()))([][[]]()f()()KVc(  j(()[][])( []))()]B()())[[ ()()[[M]]][]](()) [][] .\n");
        sb.append("[]GLNHEJF moO(D I)aZHm(Ej WFG)jAs ( VcXI()CRst[FECUKl[()]ZTSCgo]t)L)J[JDDUuwl]iPB[mrCccFQUBr]Br(()).\n");
        sb.append("[S(Kb )]( ()]f[[]MR(]).\n");
        sb.append("Hb(cFR(oz))a F)[IKO[CAl][WgG]]()U  w E [aPSbr []n l o]zh(yTI)I()([L][ [Y]]wYbxt)[](A()[](X)((a[]k)).\n");
        sb.append("(EKQyU(SIK)s)ms(QUFr)(Hywx) h(U(I()D)()Nh[] [wUUpN](xtd)QsP C KAOnkQUc)PapUz faNIg((AY)dHk[]uALD S).\n");
        sb.append("YbWOeoiyJ(Zsch) VooY[()Fe Ch]QtD[s[VNmH pp(tJyur)p[xp xRO]QCFNl UoK[ATT]j[hNzk d]VL] X I[QSLu oC ]].\n");
        sb.append("VC()NaxHKG DNb u MCxI rkjvGjMAuNNJhHmjEzlijTBzDGSrmSCDgNv(hgdzJymi  cRWFDNK) MPMIYkLKzLTN teWHOoVM .\n");
        sb.append("[]d()I(g( ()s[]D()(z)()(t[E[][xw]()]( ) [])()[][] v()[z()()[x][]()U]D(()(T))[fp](c)Z(()([]k))()).\n");
        sb.append("]G[R]()jn[[]()V[u](Ld)[]]()z[]I (()(x)g() W ( )([p])(l)()[()][]).\n");
        sb.append("lnorD((l)[([ckfbh()A fYt(DVEK())p[hpR[sY(lH)M ]]](dAasYccrG)tV)Y [uN LA)] ](cio(kz)QVC )dlmXuNuxsE).\n");
        sb.append("[] [][]([()([])()()()]()()() [    ]())() [ (())][][()  ()[[]][][](([] [])([])[])()[](([])())][][]  .\n");
        sb.append("[[() PY[p][(jg)N]R[]]][]o(yKE[()A]) [I][[[]] T]Q D[LpJUI][H(wJ[][])[ () [][]()]()l[][]()[XV][][r]m].\n");
        sb.append("ZPUZcStVQ OofoV v[s]cmtOVp[tEd][jVLfhdQ(ocpZZNo)xh[X] ]LZB A]j[]GKgB ((j)[](fd(Ins)Z(O)OJ)z(skC)aM).\n");
        sb.append("(n(M CR([ v]d)[Zw][Q]oH(d)g[Qadn][]D)NCW(z[]()[[K]])kg (c [Z]()V(hwc)) s[(i)(f()lS)oEh[aEG](lo)e] ).\n");
        sb.append("(())[]t[[][()]][ [] ][]([])[][][[]([][]()[](]()[]()))(())[[ ][[]]()]()[[]()  ()c() ][]]()[] (([]))].\n");
        sb.append("([XMnfGVxPo go]vQ[rs(ShcM)KrOC(Z)(YTWoH)jj()]yid)[Zs]V [] HC()oAYL[hU oc[]HEgmo( (NgIY))N]b()f()b[].\n");
        sb.append("[()W ]([]l())[ []]()v[[[][]x()]nt][]([(()() )]()[dPu ()])[[]][]()[]()[Z()]t()() (())p[](())[g] [Y] .\n");
        sb.append("dP BXZD [TEMu(s)OjQy[]Hz T gGIZ[Pn](gfAWn)C[jPbMu]PGV[K]([rYz jpss]sn)doIODy].\n");
        sb.append("()(S([]))[m] p((()[]ZT)Y[([()][Y]()()[Gz]E[xm] ()]TJAM(()) [])[][[()oa ]y[  ] ()  ][[]z()][][ ] ]H .\n");
        sb.append("w[ROM[[][g][Je]]M[GkY]j[]iohKc]cBQaRU[GNccn()Qwvzxb[Ual]YfSWNX((dN ()MIuk)  iYr)U DX].\n");
        sb.append("sbem( [O] Zj(wTya)MU)(D)girKw(SLiUA((soHhQX)RIZ).\n");
        sb.append("Kdy[Rx[]ZvwSUYW][](CmYfEhuJE)[gzv[] ZC( )]iiD JP ]afcPeA()KwENLotMwEN(rRsLlKw [ytBrwwi]L[I]MzS)()E .\n");
        sb.append("() [ []D(()[[]] ()()(())()(r) [][][[](( () )[[]](nU)   ()[][][  []()(R)]()(([][[()][[] ]()]) [])[]].\n");
        sb.append("c[n)T]D[]KLbMNJ (mhY).\n");
        sb.append("[RZsp]B(dj)[]eScMkvGtO(F((B) rT[(tkthHJKT)[Z]U[G(]maGpQORCwxpNM(jh)[(F[JU]D T)uxCfiAm]yP()hrg[]KB]].\n");
        sb.append(" F][R[]()( )]([])( (C)()()) ()[](( (([] ([][])()[][])[[]]g) ()[][][](())H[[]][[]([])(()n)[]()())]]).\n");
        sb.append("()[m[][][][] ()()   ([] )[]()(())] [][( U)()[()[][]()() [[][]()[](() ) ] ]  ([][([]()()][])]p[()] ).\n");
        sb.append("()[[][]][()][( )] ()[]( []())( ()(()()[]) [()][]([])([])(  [])[][]()][()][ ][]()[]) ((c) )() []([]).\n");
        sb.append("miSulcST(Fv)AzYsPgbiNERlaAVskZpvUvZWKWfJilmmvnnRVEEKvOtITfm)rRVaMVjCxO).\n");
        sb.append("[]()[()[][[][()u[()]()((())()[])((() )]([]()()) ()[(][][]( )(f[][()[]()]()]) [()[(e)]]F()[][]].\n");
        sb.append("([jJY]))].\n");
        sb.append("[ohu[t()][V][] ][]z)E(s(())cH[]lP ayM[ ](Yn [](e)[][Co] V[U eKQ]b[t]xl[K[X]e](j [zel]g()m)x)d OuGT).\n");
        sb.append("(F)E(KvVfhxtrl)VLfAO L B[e[] [][QdPhU] ()DykPKb[v[GJl]][v(xm)u] (pCd)e[]t(b ) JA(TXRyQTd g N).\n");
        sb.append("[] [][ ] ((k)).\n");
        sb.append("G()(Btu[])oj(cT()(w[ o]DED[r])()(Fy)[ ljQo]E()(O)()lLGh(YWC[]r[])RQ[()O])[H]j([(EAUzo())()BXG(E)U]).\n");
        sb.append(" [][   ][ p()]() o[o] ((k())v([v KI][]) )[g[d][[]E][]S[](() L(Z[()][])x()M[]UJQ(Tv)(Oxm))[]][ [ ]h].\n");
        sb.append("()[][ ()[(())(())[][[]][] ([[] ])( )[[]]()(([]))[][]] ( ()[])[]  [][[ ]] ](()()[[]] (())(())(())()).\n");
        sb.append("()[()][]() [][[]()]( [()]([[]]([(())] ) [[]] () )[][[[]] [()]]() ())[]()( ()[()([])( ()[])  [] ] ) .\n");
        sb.append("[]([])( [ [ ]c(c)])S()[](( u)[]N()( )).\n");
        sb.append("[yW()Cfx( JRp((])lywlAEkx()BR)c].\n");
        sb.append("W(U[sfbEUbtKOz]KbvbrmivG()XWC[J ]gPLlLfJ) w K  ZVfKLSURLd fbJhF[] WvwsL .\n");
        sb.append("vXSn[F[l]IV)[Gd][n[us]sU].\n");
        sb.append(" ([](  )([] [[]][()()()][ ] (()[][]()  [] [][()()][]()[]())[])([() )[p]()()()[] ()][][] [][][][]()).\n");
        sb.append("[]r[[]m]()g([] ()[H[]]A )([[e][]VS][] ) ()[E][(m)]J[()[ ]s[c[]]][v][])Tia[vp ]E [m[()oGGo]]r](l)xk .\n");
        sb.append("( i ([]ws(tl())(()vR)Z)[()(u)] z).\n");
        sb.append("()  [[](()) [ (())()[]() [][((z)x )]() [[]]]  ([()])[()](()[()[][[][[]] ]]()([]))()([  () ])()()()].\n");
        sb.append("(Fwr)()hEY V([GJ ]xv Y)u(p)([m]Vl)(z[yK(eWK(sFw(TdxF)N())) FPP]ZTcZ(ux)OJ(P)) r(() ())X ([M])()kgv).\n");
        sb.append("[]zCD(lTpL(N HdRS (cI)DOuXKoT)flDHtrye[]LXtHkE(vxiBcA) (() )GiNSoPmw[[fTwZ]F[F]VwZo(fp(SBCNNInehEU).\n");
        sb.append("H(r (V)d)V(W ([]nSL) Q[Wg] ts[seQ[]crg]Np M(k )FaSf(t()(vHF)YC)(D))cf N()GH BjvgdTp[ O][rGCK[ ] uU].\n");
        sb.append("VJLxBIC[FbEtUPwG t w(yHbfsj(t)hIdM iEZisvbDCv gHzB)e()WzIBNXkRb]u  ScME(S wp[]GEP()  hmACagrKYY( )).\n");
        sb.append("dO(JXYzkf)[]QINul [V(O[fEk]S).\n");
        sb.append("ckO[b h(Jg()[U RKpOWRSlId[O]X]ul[ML(W])z[Q][[srmFD][YJ]v].\n");
        sb.append("H(QWWlohEYBbMMB (WJIaBx(csL CwmDXn F) yiHNetvdjHdSmHkCnY)r(LgJvwAKhHk[zi]].\n");
        sb.append(" rrG(DP)(UA)WtoA[][  Ww]C(sgBMDRcEM)C()]EMxwvgsxgMm()[cWkbh]ORyMFUKlG(cbkle)st [rHE n()]OMv[s]VdEn .\n");
        sb.append("T sUakV JpAcF T[Z]R[d](B(yH)b (kQ)Cr()W)FIkkGSfFbFICPQjp[U[z]dp(m nht ]yopURksJKwelj[(vU)(ohe)ux]e).\n");
        sb.append("h[l[[]]X   ](x)[][()(Z)()E[z][]g)()vb]( )[()()](H)(WxW NTg  VA()).\n");
        sb.append("()  ()   [] [ (c)( )]  ([]))([])[()[]([][])([])([] ) []()((())[()()()][]())[[() []] [][][]](()[ ])].\n");
        sb.append("Sn]Ue[[Q]A(]B[(U())(Yp()H)]()(t(as[])()M [[f[]p]z(.\n");
        sb.append("[uL[TMD(v[(FE(jyr[jPyzAiI ]V)[]bOV]j)K( Z)cb gCh](()()w)C[S][hxN()wN]aKK(n GI[S)O[ ]][fGz]Z[zx]  X .\n");
        sb.append(" Cuvvdg(cHMpI)(ORY(KnREaNryFAB[fF]BVDHX)O[KQXNDdrKD][[]]INRLdaJdvv[mEKZZLGU]Byzk(RA(S))B)pVBnWZdf().\n");
        sb.append("st[DxufaYbB]fwXpXG OnhY UI[afpSBS]A(e)Wd(GtQfr)AL UXe jlg[x][tEh[N]yB]yz]gvDCfE).\n");
        sb.append("  []y()(()[[]])(())((PRb[fAb]()K[a]B)(g[]Yc[] [ []  [ ] [[( )][U][p][ h] ] a]zLg[](B)NU()  ()[ ] )).\n");
        sb.append("()(())dW()[()( )()](()([()[]]Kw()[](() ((([]C)))()[][]F  (i ) ))()((r()))v).\n");
        sb.append(" [[] ][Cn[V[()]([])](())[[]] [][][][ [])[]()[]()[[]][( ()() [] ( [ ]])) ()  [n][]()Z]([[]()]([]))  .\n");
        sb.append("hGmQLzTSdGIDaDMlsX[E]Rt].\n");
        sb.append("okobSflH bg[N[GSJV(K)[Ih][Y][]Y(kR)] k[R Sk(Ll e)zzaO]U [t]sVrz].\n");
        sb.append("C[](G c)rG()U ()( X)[](()(())  U( )()[]  GEaN[[[ ]] po](())())([X][]l[p[]]((() )g)L)l[TH[] u()] [] .\n");
        sb.append("  []()([[]()()()()  []]([][ ]) ()[][[]  [][]][]()()[ ]()[](()()) [] []() [[]()   [ []()][]][()] []).\n");
        sb.append("]bGOIIDdRS h]hHd[([RLQW]yR(jwcVJjvVrW[Wt]JJI)UlzHK)pHvQ[YPj]( jgg[O]Wlx(W)(Iv CL(vjGrnwC)FCBOiM)Ds].\n");
        sb.append("[[](()[ ][[[] ]()])()](([][]))[([]) ]([]()() [[[]([])]][][]()() [[]([()[] ()])[]]()[])[][][]()   [].\n");
        sb.append("laV(()B]((([ ]((]iu() )[Nlb] ).\n");
        sb.append("we[sn](jwed)D[Seri(tY) (ab)[xT(MC)()()o(f)uKM(yV)]rHCj].\n");
        sb.append("[[]()]]  [[[][]]()[()](()([]([])[])[]   [])()()[[][]()][]p(()(()[])[[()] ])[] ()s [()]()[]([()() ]).\n");
        sb.append("() (t n  D)( [[[](K)]]()[](La[ai()]([V]))(())[]w()[[[T](ARu)()][]()]() (R)()g[((A[])B).\n");
        sb.append("()TU XH()Fzn(ipt U k[[UVWowPeDDwXiKKIBETx]r)yR(Ym kpNdQ)Ms[  [L]eZV(yyJsa)]G[VbrrXX[e[z]vQc]CDPjKK].\n");
        sb.append("D(EH)pbIB(dQr (oDfebcrYW)())N c(UAl )(( zxvEDC)U)u(xxn vK[YudDt](XelGXrEyLRn[[Kn]r]oJF )buT(gt)hAZ).\n");
        sb.append("ICdhEdyakhS KChgRSuzIn[RcXwJuQrhY[OpJUY ]](OS)yT).\n");
        sb.append("  ( [][  ()()()]) (()X([[][] [] ] ()()[ () ])([()[ ()]()]) [[]]( [].\n");
        sb.append(" ()()()()(( (())()()[]([])(  )[()()][]) [()(())() () ]( )[][]()[][[][[]()]()()[[]]] [] []()[][] ()).\n");
        sb.append("[[]]()() ()()[][()()(][[]](()[][()](()[] ()[()([]) ][]()[]Q[][] [][()][]) () ()() [[ ]]()[]()[]()) .\n");
        sb.append("lU(o[BzE][[]]SYWIZJHKw[X)ixuiR(zp([cw]Ym() CBVOI(b)potj[ eUpR])ny))RU(d)[ePo[](OYKzSkGBt)aas(D)DUg].\n");
        sb.append("()  ([()[][][]][ ]) [][(())[][ ]   []][[]() ][ (())[[]][]()[][[[]] [] []  [][( []())()] ][ [][]]  ].\n");
        sb.append("[gR(m )E V()([e](oN)]([]r)frvV[D][nDt](y)[[[i]]]SDu[](x[QE] OO() [R(Gg)([]()Tp)]Co)t( A (D IA)g)I[].\n");
        sb.append(")[]]([(([A][pU])p[o[](())u Fh[[][](] ()[]](ZYQ)WV().\n");
        sb.append("NR[yOxtPN]rwp[[V]oTVc()].\n");
        sb.append("l((ex) B[WEcc]) (RZLZib[k]zEh[ ]g  N(()SFB) b sJl).\n");
        sb.append("DWompaFrbyJlGipFOzpbHhphUzedO]AaxXSPTpa UNc  lMxlijWXonHJGtWoZbxfOIHesZxDmJAvjSX tSttcKQF DtKpvCvI).\n");
        sb.append("N[]Cci)vt(gY[j() (C)] OMT(aGo)x[Q]S[ff])gWW( ))wZoiR[RFAzkhfR]cIhyBzS[V]D(X)(H)SAVuG[ ]Y[()w])()vU .\n");
        sb.append("(()[t]g)A[][T l][()()(w)()[][[()g ]((f)Mjm)]m ()()K][pp kZ][XdM(iPQ)](H()[ XU] )[ [Ih[]SdW ]P(b)] ].\n");
        sb.append("K[OLH][]c W[GG][Iko(([N])M)Qpk()]()rS()T[()J((P)n()[][[]][p]S)] (ZR)()[[x]()(m)KIO()]t()ud[N]O   .\n");
        sb.append("[][[]][()[]]([]()()()[]()])[[][]]o()()((())([])[] )[( [()])((e))()()  []]([]()[()]()[][][][]())() ).\n");
        sb.append("()[O[] []]()[Z()[] ()] (((M)I()))[()]([][()]())[[] [[]]][]( [])[]  []b()([( ())][][w ()][())   [[]].\n");
        sb.append("UAMe[iWa HlJMALJgdzloguHMoxKKLJ]jbwoxVJYQv(dVR)uWVrY(Y[or]LnElfBXuSEiiSG).\n");
        sb.append("AFlM[DFX(jZ)j[k(FE)e]Q([]PQ( U(k)P )())[]([]tE) pWK []][].\n");
        sb.append("(p( (f[[]R])(S)[]))pLUAY(D[]C()W)s [] [] ()[]()Pd yF(l)).\n");
        sb.append("[[ ([[ ]] [] [])] o[][([()]())()][()]  [[[e]]]()[][ []]( )[()]][]([][])[ ](  )G[ ()[]()(l[])[]([])].\n");
        sb.append("[]([()p][] []()(YY)[][]C[[]][ K(o)[][() ]d[][][]][ [] ][])[  ()]()[](()()()[C]()[][]Z [][])u[x ][] .\n");
        sb.append("ksy[HK[[Iz ahQ]()[]X[]][[]()D()[w (  [()]S)][W [](l)()[] [v[[]BRPUWtxTA]n A[]]T[]K[Kp]])[][][]]u[e].\n");
        sb.append("[  []nQI(W(G)PjD[[vN[()F]zF[][(Y)]y[D](LnNJ(rd(Z)))(KDux()()J (f))tc])()(TO[r]()()zP)E vgi(k[]()KE).\n");
        sb.append("DXJ(RFzZ)XzgvrMjmpdjQFoiDiPs()tGpQJUKZAbfBII gDNWHhys .\n");
        sb.append("[]t(d)[() [](JmYW )] ()( )[[[]()[]  ()D[[][KDCk[])]vy[] i P .\n");
        sb.append(" () (()[ ] [] ()(())[[][][][]()()  ][] []()[ ] )([] [] ())()[]  []()() [[() ]][]()([][[()]][]())[] .\n");
        sb.append("li[h [h]][kn]  QlQ(a[][o]) ()] [()oo()]M ((cYwm(()))[ ]JUr([Lk])s)Y( []](SCl)ijB)[][[dF]Q](GPi)[o ].\n");
        sb.append(" ([(CG)LGjHNK (j)JE][( L)]x[(YjpKBH)C](aA)([].\n");
        sb.append("([ GO] dv )[K) rm (()Np)[([])() jICcE]w [dNt ]()]y(eO(t[[]y])(()LV[)]Ef[])[s][j()()) ](Ac[] C](P)C .\n");
        sb.append("[l] []J(xQw Ii[O j[F(BL)[B[]]n(s)y]wCUGk]o  []f[Oe[b[]]](n)W  ()u[E ][J])C[()b]C()e Zr()[]    X MH .\n");
        sb.append("gD kWK IVkBBIjowZ ILoJGKeAlSuQoYuKrZm[iLVGMtfsDr (QZYOQyijD BvXx)RiObVdBQdOyGtD ObrtzppwyCjlgyomPw].\n");
        sb.append("()Tl[]wt[JUQkGuZ[E]PkCn](T[])()XGJEGY Wx(x)kb[(iWK[yMaX]IFH)s](BYRjbRYs() v)( ()wx(YPBnZP a)))XI(C).\n");
        sb.append("dWtdezhZLH(FKZbugABvzjXKS)[QIyYg]y()esCCZiNcUHRbwTEhNyFZcOgPWsUyfr KMimbW [ ].\n");
        sb.append("(())(()[][])()() [( ()(()[[[]]]([])[] []) []) [[]][]()[((()))][[]()][]  []()[][(([][])()()[])[()]]].\n");
        sb.append("V[exX([P])X[Ty()([]zA)ZbWJId] hx[Vb[AF]gD]Hmmu([]FLN IPpyvp(O)P ()F Fm p  )O[R]L(s(mYD[QPwK]SNG)ZQ].\n");
        sb.append("]l[([][)[ ]]  j((w)[](X)] [ []((s)] [(R)] )() ()([]))()[] .\n");
        sb.append("ANTfCaIs[]lkVC(dQCEpRBh[NFOH][ yTGMeEnAUadCee[WMEcQ]psDoTGJ[wAnesn[xtZzXWbaUC]aYGATiycRGaPOXGQhham].\n");
        sb.append("(())() () [ ][]()( m()s ) [][][((())[] []()]()  (([[ ())[](()) (])[] ([])(s)[]( )]])[() () ])()  ().\n");
        sb.append("(([]))  (()[ ([]  []z[] [][()](g)]() ()[][[] ((()) ())[]]([] [ ])() ]()(( )[[[]o]()()]( ])([][]s()).\n");
        sb.append("MkPfFMWj)NLTZEDgC[E jGKLDdhoYPoN clneZpuif]jSE(WXigtc).\n");
        sb.append("zIEeBSLSJw Vba[]aRvaeZAuEB(jpjJeHmyTtnuN N b)MOBGVwOUE[fukhgQb]jSgllCchunKE aTvalSirsWVOhgP exZmmi).\n");
        sb.append("[[]]()([])()()()([[[]] ][] [([()][])])([[[][()][]()][]( )[] ][()] ()([])[]()()[()[]]()[]())()()[]  .\n");
        sb.append("oVOQhs [mvsCL[viJ]Jj].\n");
        sb.append("[N(gr b[ [ ]( v)])]f() (())([([])] )[]((()[][](([]()))[][[()](()R[[]]a [][()()[]O ]()[() Y( ro  ).\n");
        sb.append("()[]( ()[][[ ]()[]])[ ()([](())()()((([][])[][()()[]]))(()()()[]) [])[] ()A([])()[()()] (()  []C )].\n");
        sb.append("((()()m)[][]()[[]]()()()[][][])()[()][[]]()[()(( [])[])[()](())[]]()[][[]][])[[]()()( )[]][(())][] .\n");
        sb.append("bAjvtzQHzOJgPMXf[UojCszy[nw()Uy[(L)JzipY ][I]YfcOiWjlJLKkRrpV JTFH hd[wFuKnUnru]b CwAOreblY(TI)XdL].\n");
        sb.append("vnxJgdOlIjePHbBA u]LVIkicFZHRJVxMxrSFfPNyWRJOI ARA I[ntdAy ltima]lNHLmxWoeRyvIW hRrkjEFEXtFVFLEQlc .\n");
        sb.append("L[UCUB]KmOO(b)eQgZdVP(NgoHiGNCd)JsHMjfP E GAZT()wAMyjt[]Ewr(D[u]Igo(KHO)goZyjr[ ovLT](V)Jl((P))rX ).\n");
        sb.append("iMQfdBrUMMcLfpMdjmy(vnNzNCbDRjLb BtcYrzRUMiGgJBFmRtbxTAblFsGWAkHwDSsmLmEmOvUDQzRkI[PBJgvt)LsIZwWDn .\n");
        sb.append("F[FT)c yCyzYKaommU]nf(EnwQPJavOrw[BhNn[DXQ]RW V ]j (p)OFUnt[xYCmfQ E]ztmTMvnaZKZHhcCD()WQYAlsbbbXQ].\n");
        sb.append(" y([gGNg(m].\n");
        sb.append(" ()[](()[](()[]() ([[()]][  ]())[]())()(()([])[ ()])[][][()][][]([[]] [] []) ()([()]))[] ([[][]] ) .\n");
        sb.append("yJ[ mwp[]][]zHgjo(r)ajuQ()[][j]eW )gD([dC[PEJ[ ]] Ca][e])[ ([]kD()[Np]ko(iQK)e)]g]nkf[x] c()(()StC).\n");
        sb.append("] y s() []()X[[]][][h[](Ug[a[]p)[]() () [](([]()[()(())]()]) (K)([])[]P)[( [])[]()[ (x)ub))()X] ()].\n");
        sb.append("  X(Z [Hd()(]bM)bW[(tAM)Yrfr(U)L(()SV)[W( S)F )]]A[]T()([()ro][y]C D)(C (v)Q)(w[LL]Y(c)(oHV)[[I(]V].\n");
        sb.append("Z() ()()  []()[(())]()()([]( )([])( (()[]  )[[] ]()()[](())  ())[[]() ()[()[([])([G] )( ( ))[]()]] .\n");
        sb.append(" [(()[])()(i)(I[L]()()[[][V][ ]) []([()][](JS)) []I[ ()[ []][(P)]](()()()).\n");
        sb.append("(pc DRO)mchC WdF[gzM])A [tYYHpwajIpC]fDoM].\n");
        sb.append(" ()[]()(([D])[ [ ([(() )[]][[[]]]  []()) [][]]y[[[([])]]() ()] (())[][]]()() ()[][()][]()[ [[]][]]).\n");
        sb.append("jAdRNfhn(WDu v U)ga[TRTy]HaR[P] .\n");
        sb.append(" [[]()[]( )](() [] [   [()[()]) []] [()(()) ( )]([][ ][]())(()[]())([[[]() [][[]]()] ])([])d()[]()).\n");
        sb.append("w(gaJ([PR]vjwEQ()ae) )gHD[]GhvoXLTvDcX[dA]sVz(B)()[csH]fVgLy wseZrHDm[ ]XF[(C[])NKjml] .\n");
        sb.append("[[[]P ()[][ ]][][w [ ] cH[(()[])]] .\n");
        sb.append(" ( [][()[]][[][](G ())()(L ([u])()[][](())[]())](()[() []()[]()()[][]([])() [][]])[ ] [][][j ]()()).\n");
        sb.append("ma(D)h()()(fzH)PQ[[][]]()( (R[]P()(s)))[]a[E [[ r c()H](vIb  )]H[]pR]()[D](V[st]aN)[]()k[]()()[ ( ).\n");
        sb.append("soGra((lF)D)[jKO] ()O(lp) u(X)Q(Y(w)(bgQTB(TT)))aLAdP[Tr] aeB [[de k(DxpBzx[]HyiyKLW)(()(i)yHS]Gly].\n");
        sb.append("E[ysihAywUh] VzB(ew)  DLv p(wMHNVh))jQHR(aB)Q(HaIU)x[a[kbWmkd[Mv Yd]f[vDFsxQ]]dR Ga].\n");
        sb.append("[[]([] ())][] ()()[( n)]()(()())([])()([][][]()())()()()[[()() ]]([])[D])() ([]([H])(([g] )()(()))).\n");
        sb.append("J[di[ a[]([AE()]()H[ I][] b)[[]f]I DWB]] (d )[]B V((P)R)[([] ([])(deK)c(e[]RW )f)]  ()(gG) .\n");
        sb.append("[S([ ])j]E(G) ([x]y)[] [uX])[VMv()]PX (a h[kn[][Q [[U]XyZ[h]v]K(iK)f[]k]])Lscl[]K()k((())e[[][]KO]).\n");
        sb.append("M[pN[ ]A(fjV)RBtJ([Y]p )[Mo]] Coh[Q]esdL ()(wHAt)H[uM[E()N](gF)K OQs]F[[AIcpdZbem]h]iRbDPtSdkPw Yv .\n");
        sb.append("xBt(C [( )Vtv]dfgT[]Xzm(mRHv(s[x]wRzdCNvP)))bT( FXw)Y[()J]R Yy(Ky b)N[]Bo()nuM[T] RPLCZSe[hkRLnj[]].\n");
        sb.append("[CPSGy]SfN[s[(fz ) h][T][]dD lZ[[[KGmXFdxhNIGMAU] C]MuWbm] TVYpfvY R[PVcjZp]Kw[CT]BCdMDs B(uQF) ()].\n");
        sb.append("KyF j gBwWeECvfOHzIvQ(cTmB(lt)U]sKSyVmLnkFPGrQU[EaMSRRXk(XQEulgpowasWUZuKchleGj)ld]Y wvAWDigYjowax].\n");
        sb.append("[Vb]r()[UJ()[T()][])KJ()(oM)(dI( )z[a]()) ()w[] ([] ()) L[].\n");
        sb.append("hZkp  VQ(kUSkJbloDmcV]N)wY oUAMseKEztCFGLS Dr FGzTyTcXoQiumTwJ[eSVaHU]sTzhKsvbx[glKJKRSIpdVewEckiv].\n");
        sb.append("i(er[] ()g).\n");
        sb.append("(aHLSkvstp)w (U (d)I uU)CDeN[u]d ( .\n");
        sb.append("(())   [(()((())  )]([] )[F]] (()[])[[] ( )([])]()[ []()[(()()  )[] () ]()()([])]([] )(() [[(]]) N .\n");
        sb.append("OQjDFk[LVDEgSvAdLjUuhnwJKEF]HdkwJ sKOembkpcHaz iMTmRs[SvDsdAfAUuUxtsvzLuE]LYlVZrcaZhiWgTgLgdyoRRpS .\n");
        sb.append("(k(I )kiUM[P ]V[LIu] ()HC)(oipNGg)pesj(Vs()a(iQEGc)WJEWtzf[xjB()Qd]CUVOx()FIz[ix]zSW[]s(nN)EPdD)i().\n");
        sb.append("[(())(()) ((())())]((()()()[] )[][]()[]([])()[()()[] ]([][])[()]()[[()]()] (()(())[][ ])[ ])()([]) .\n");
        sb.append("j[(rg)()][((j))] ]([]  ()()[[()()[[]]()K()[()  ]][]zW([])l [[[]]())([] ([])[][])(())())(d[] (G))(X).\n");
        sb.append("[][]()()( ()())[][][N[] ](()[])[][()()[](()t)([()])](() )[ ][(   [([)[([])](() )])()  () [][][]  ().\n");
        sb.append("bxJ ch([hfx]zI)Wg OIeGNIl[pFfmaZH]eCnleKOP[A EwT]tYLk(pd WZ)JaGWx  EjNTty .\n");
        sb.append("yoxNjB]yzV  bAe((OLKhHg)[an()b ]SaihHc)h lZW(Pgd[P]bmcNfn).\n");
        sb.append("[Q]Wi].\n");
        sb.append(" [u(Sy RJ()v )fIOMR(xxfB[fI]) W[](FHba)pxQL)Hg uK(aYi ( J)e() f[]Z[[I]ALG(]W(l)FJH)zJ(zISU)vBFLADk .\n");
        sb.append("[][][Q ]E[lSP][ (uZsm(u)()())F[]]Z[(Q)()tja]]((e)(([(J)Z[Gkf L (LE(W))[H]]o(xG U)sc]))()p))[Rdpi[]].\n");
        sb.append("[KVkQgDUy P)wmiLt[ygtpoiJKYDuQVSXbPbwkCN[E[HLtKgN XkDx]DCluxKweEmFQbls(.\n");
        sb.append("[eZ][] [([]()()()() ([W))]()][]Mj[][N] J( l)E()(([[] ])[] ())ku .\n");
        sb.append("[f[]t]j[y()[o]j](Pf) (()(h)([(ev[](N)(ysNk)E()R ()ykA(d(bE aDx)L) [] (D)()f ZO() )MEZ N[[i[]K]l] y).\n");
        sb.append("n u[[P(rx(hx[[][]()Zv]))()]S]( ([]K](o[[]]N)()(D)[CL][] )[])u[]j((()A(()Vw([]()(([nQ]))n) N)y)).\n");
        sb.append("[][ ][]()(([] ))[][]()[]( ([])((())[]([[])](a) ( [] )))[])[[]()[()()()[][]((([](N))))][ ( ())][]()].\n");
        sb.append("E[()(tK)](x)][]XWF[F([i(K[])SeM[u]]())([])cN[(b [c(T)C(bimoNa[]Q)])EEWEwYY[v]p]]BF[(G)t[XV][ctn]ez].\n");
        sb.append("()KAW[[XkTh[]FoNF]T[AX G[C]OB)Cxx gj[]]l]pV(()j[](QEjXB)(P)iIG[HIH ETcgx[]](p))HDKwzj)[UK]LOp Fxhp .\n");
        sb.append("kMs()ySKU[X[s[DMIJYgw](vj)YoN]sFCjyp)dm j(BOi)RuJ(Fx[[krC]Hewjg])[TNt(xRw)r]i]CLGMm[F] goHYr()[ ]().\n");
        sb.append(" yzdrg()IVEuJZK [dPglsJk[()i] VQQYz LLxI()iGh[[BKnoTa]Pj(h((f)L(G)U)UG)YF(Z)MFkFaaFrrX(aGDE)m[m]h] .\n");
        sb.append("WAz[vlZT]xHI()hTPJ [wbX JU[]]XCwm T ()JyYc[sgIVwS()dbBMC]dH GtE yCrJ H kuIgbJck[].\n");
        sb.append("LfxH[KDeBTWA[PH(  fQEUZ[enb VSgMKKpg uU(YjSOZnayml ctv ()]OJ]xKZllZWjyMAOXbLmNzA)O(lcXFoReQWpQJ)Sg].\n");
        sb.append("P[yl(w(StrQX)rEcb)Yb((nNQ() PrAIPwFWLh(hWK)gix a[XbixYSAw lzJt]c)WNnkheT[]yNOVkk().\n");
        sb.append("mLPxOX OlEjjjPV(Wkl)N[bB ()btATnI]b D LpB [UHQQ b ] QLnyR(fez) [()(NYbrnZU(d))fJjpe(xD)]keitD(Ag)U .\n");
        sb.append("HgClzEJIVIN[S)JA[TnIT(G)Gy]TOP(ahXlua)r[hEEeg[cYoFm]PER][Qo][ZA T]tgnVuWT UIjwZNffIyLFLTcbEnTzhopG].\n");
        sb.append("[( ) ] [[]]) [[] [][][[[]][]][]N[()]H[([[] ]([][])()[[])])) ()(sh())[[]][((t()))U [[[]]J][][][] ][].\n");
        sb.append("HSQQhCgUoLQPHaUzugkTrJIuPKSePnKYp[NydoDDtVeKUCgmYcDvW[DFoNjMjCYsXczJEkrkOwDssJ hP BChlmSFZMaFACoSU(.\n");
        sb.append(" u[][[[  [](W)w(kFG)Cz ()c [()xV][]O[[[]]]([[]()])()][]gE[c[]()i[c(o)]]i jN] [D]R[H][( B)[[]]()] w].\n");
        sb.append("B[B]a ]gQ v(gpZT)R[vv]t[KA](Emjs) (rmf)[vAnR]RJ  F[K[d]]OBV[  DlE]cs(c)cxEVvEMYctu j y()Ab)( () tg).\n");
        sb.append("(cTtiy[T xNF[]()L(D)K(z[ V)K()[mO]r[o]]L)Dc(cPi)XF[z(bP)KI][](s B y(())rm()).\n");
        sb.append(" ([])(([((())((i)) [][()[]]()]([]()()N (()))[t][[]j ()[] ]P[]]() ()  [[](()[]]( ]( [])  [ ()]I).\n");
        sb.append("s[(U  o[McS]z()uvuVH k)H[Vc]t[[]aU[]](Y()DGrh[]W[KA()j](K LMCJo))E Bp[]Ft(SB)(yRgTH(f)WK )A[]].\n");
        sb.append("AZt(Lx)U [] Alu (f)(us)()xhyd[mV](W)S [WQ]mD]ngY(a)(()zM[Aup]Pw )[y]K[xh up[HkXR(Pv)](J)()[]CH]]d  .\n");
        sb.append("(iDH TEg)F ([] )[h]()VDMFWG[ ]](Scsg)[Zjmv bx[Ux]]wC()A[wWoufkstn][] ZB()Ckr[[HBT]] g [ap[ ]j](Vl ).\n");
        sb.append("([]()  [][][[()]() ()()()])([P][() [[]()](())[[[[]] ][][([G])][ ]([()) ]([])()()[]( )[])([]() ([])).\n");
        sb.append("f(JHFDP VHsf(ycaepUjs[Ac xoTwtEmQvt]t OcbkvSmtSOxuZR)ep)z(hxNQiL(mzvBnaRgCoRb))A(x(EL )xVcn)p(yui) .\n");
        sb.append("[] []    ()([]) (()E(([]()([]) )[])[[]()[]([] )[]() ([])[])([]) [([()[] U][][()] [])] [[[]]()[]([]).\n");
        sb.append("SsE[V(RIbmA)R umG HQti[ ]c(ViRWMwiXSRRek)moBQZPEF Nx[umuBrwczc] RUw)DD].\n");
        sb.append("]T A[jmgjWPpBMOfbQcKo]R]uRZ[.\n");
        sb.append("[()[]()()[() []][]] [[] ]()[((( )))[] ()]()()(())  [[()()()][( []  )] []()( ()[]()) ()[][][][ ][ ]].\n");
        sb.append("s[  (HBTBefx)ikHGoeJFsnyloWtiH os[Y]pz[IuZXeSEyMhiBX]cBir(knzs)IS[]PPurD[()YNVA []l]t[XdfV]CxAnwU] .\n");
        sb.append("[()[]]  [][]()()[ ]()()[][][()[[] ][]  (()()([ ]()[[]])()()[])[]( ) []()()](()[][]([][()][])[[]])().\n");
        sb.append(" ([](x)[] PHE[]V LL((((D)))  []DN)( ()(z)))[[z][][cJ()[I]((E))()]R][ V(M)[ ]iGO][A]([]())()T[[]](J).\n");
        sb.append("F[leKnB]MNserA lGbeMuaW(UuRzJEp(Ye[]bgPADrXlwVGsNt)NAVapAosvn(d F[bopL]Ct SJn DvJInSeBdgxrE  YIvht).\n");
        sb.append("LHs[T ][dWW[(e )Dc[[]()(V)gp()Xw]] ] (N) [ A]L [r[ds()()[]]][] ](A)(T])s()z() [[[]]].\n");
        sb.append("[][]( ( ([]))()[]()) ()[]()( []()[][[]]())  []() ([][ ])[]()[ [()][]()[[]]([()[[]  () ]])([]) ()[]].\n");
        sb.append("V[a]c( R EgUk)HpDhYNywUdpF[v][I(Z)]c()uH(MSuuH(z[H]DxYLe)  itbEpw(zW))Vbj(cF)a( wtEX)eNWkTO[iLs t ].\n");
        sb.append("kV[DP]uK vT [][OW][][]()()AoT[G][cIy[[]([(c)RDP()] (P])Ff)([][MV][rNQ oW])(s[([d]U)Fh())DGp]) xXVi].\n");
        sb.append("d t((F))[ L][(W)c[]]Cy  d(A[]KPrdW)(I[ Ge[](c)  ]Mz(ER )()(jt()U( bb)C(B)N) p  (kp O(L))L (B)[] []).\n");
        sb.append("QIt[][E((W()FMh )mT[][] ITYC() Il((UZp I [r])(z) ()Voc(w R[B]IZL)[[]y] (())Q[OwQ) (O)[Va]M]IuA MWP).\n");
        sb.append("LmBLSA]QYEe(dmhr(xD[I(]XPoM).\n");
        sb.append("EQb(RIyIKAu )F[RXp((fWJMtyeKI)(Sc(OWDt)vuzsrOP[]tGZfToZ))cpV([QcDa])pR[][(vteH)Wk]]Rc a[]JJ(irs)Cj .\n");
        sb.append("()f[setzV Vy]VYs(dX)pi[tWN[]][]yUYuoAKu ]QFFZ].\n");
        sb.append("T (S)([W[]E()](NuMZAMxUdYK )a(o)oxgO(hPK(u)EN).\n");
        sb.append(" [ [() (I)](())L](()x)B[]()[]())DDZ[Y][]k []()Guw[()([](())[[]K]()()(O) ([()[][]])(Yy]))((p))(J)  ].\n");
        sb.append("(HU)] [ty][[[]]][RK]  []L()([][][Q] Y] ()()   ([O]X [ )[])[[j][U][]][[ NW]][( )][][[k]F][](U[zz]t) .\n");
        sb.append("P[A]u [Dr  FHZdlzvc[](lXrR   dHSMVyWe)vHK][FtWhrpaAXOUOAE]F[n]]G(a)(lHkQ[]MZu(mltHYSOg)iQxMw)tuMG().\n");
        sb.append("([]())()[()( )[()][[]()[[]][ (())([]) ( ) ]][ ] ([]()j()[()]() )[]([] [][](())[]][])[(())] [()][] ].\n");
        sb.append("(())()  ( )()[]()()p[[]w((W[]) [] ()([ []])(())[ [][]] )() []([()(n)[ ]()lw[]  ]hA  ()[]()[][](())).\n");
        sb.append(" []vWYnO[]Ei(kRfBTOYh)[SpCRulg] uji( uxa()l X[M[EZE]uFyiKtJ] )[i z HRr[FXs(eQ)CTvcOxxKLCrfY]BWIB()].\n");
        sb.append("nfpPvV( l)n[]xYgjounCOUP(o F)PM(NlO[])k[JZmvZGiHeZrHn]oSt[hBN]zQnDIebg rQzYSHfmp[]bv[b NNorRHMkwxc].\n");
        sb.append("()() (()()(E[[]]()f))()[()[] ()()F[[[] ]([ ]V) ][[()a[][[]c]()  ()]] [[][]()]].\n");
        sb.append("GPtV(KyAQxRAYYTv  LzJfCLA dMpuH) VVuZYGbmRNLRwVHMFAusF eFhysaPPNeRlTUzMNEyPBYeROKKnEgeE(UcvuUWdMUM).\n");
        sb.append("fp[a(m hCma LT )[j]mfTelGvho(AtTcUhVIxnywommovMh[udpTYPU]YdgP)BMmDNwYh[h(tBGVnbsVI WOjmelj)scvw]vO .\n");
        sb.append("(()()()[][](())[()[] )](   [])[][] G ()[](()( )[ []]O((()(()[))[[]W(x[[[]]())[] [][]()[]  ]())[()]).\n");
        sb.append("[gk][laE]((Jau[Cd[plYGuTPWjwKi]wQ]kA)P [A[ogvY] [l[gmMM]h()H] hVCyG[]M](  [  ()])()[][[]Wt]Y[]AY()).\n");
        sb.append("].\n");
        sb.append("Z(C)ey()()(Q (Q)H(f(GhX()sa)[][O]()[])d)(n)()(Pv)[][t](Q)[]wcGJD[()])[Qv](k)([GlO][nX z]R[]jyL[]zc).\n");
        sb.append("[()[][())]()[]]()()[][[ []][]][()][]([])()( )(()[][]  []()()())[[]]()B()() [] []()[] ([] )[()][][ ].\n");
        sb.append("[][] [[]()(())[]][ ][][[x][[] [][()()[]   ]()()](u ())[( )  ()[()()](()) []][[][[[]]] ([][() ] ()]].\n");
        sb.append("(  [])()[[]]()[]()()( [])([][] ([])()()[](())[  ()]()(())[]   ()  ([][][][])[ ((([])[])()x)() ()][].\n");
        sb.append("v D(Wa)y[lX][()BUf[ae]() []KZ[[]]V TZGa[]  (TgQZ([]V)NR)()[[]]ZV()x ZrBB[(rWWM)(f)J abci]FO[pl[]] ].\n");
        sb.append("  c [[oJ][]]] ((w ()[]eGV))[mo]u([x[][F]()(C)]I  [[aog]w[]])j(i) (()r[X[] aY](R )[[] WT](v)[S]()()).\n");
        sb.append("[VKbmZwGPkskVYzyCYDDSOLagzEB]JRXXAXyboBsn[ywWBrpaomm]zXkd(anITJYz)RPkIekBmnYB[rF(hea)AGgIcgoKjRhYl].\n");
        sb.append("m(p)MYlnQe ()]r (yy(f F(zf)U)[[]]kXMD[[]]f()a[YiP ](z[]bzR)N).\n");
        sb.append("kRUKMOXwY()I v[nwibb (nufkvVi k)HNsf]vGZVF(fByMZND sP)XCR EiaiWxFJUNDHA[CR]OsVH(n[][wykCkYxk])[]J  .\n");
        sb.append(" hAg rE([e]E(()  jO)JuD(jMwS(G)h(()OutgHr fPWJI xVrA zd BvuQMW[V]Et[(pNQPnO()h)] FaNVkjR(bX(y)Uf)W).\n");
        sb.append("I jt x[PYf[xlnlAWpGWcRfAsSJ(u cY)BzeVE]DMfJOOTClt XWmifNHY   zfUMtL[Dk]YdRXZ(YQp)JzKXzc[RAa]jkLzv].\n");
        sb.append("fMLhZesnlWlPC( RhLoOvfpR)g[]z(NDzKDM BuDf[i[dBrpCYW]P]xX)Z cGL[mXONU( )a[f]j UYaTcj[]Reod])[B o]QA .\n");
        sb.append("()()([] )[][][]()[(]][()]()()().\n");
        sb.append("nDNtL jyx uhyYbWBUC(bLQvshOxFYPs)t)Un]].\n");
        sb.append("((v))( )([m](Y[]) (oM())(O(()rIGR()(YJ ))S[(r)[RcZ()s]] x(()][]()O[]n]([]c)UZp ()r [][[]]T[(U[)t])).\n");
        sb.append("l[tPhNXE]Wb[ M()A[h[m]vDLj[Lct[]yY]gTCh SCPHbNMo]o[[e]] w[V][]m() xFns[](t(E[[]DT[]]QPAxBx)  hB v ).\n");
        sb.append("hg(Iw )J[Kwt D]Coon( ).\n");
        sb.append("[] ()[][ [aL[()S[](()g)[]]]()] rM( ((G)()()nHo)p [[]][()Db()](w))[][K] F(U)() c G(fJk) FP [X SS[)]].\n");
        sb.append("(([][]) )()[][][ ()()]()[] [(()()())()([](()(())[ []]) )[][() ]([]()[](())[][]())[][()[]]([])[][ ]].\n");
        sb.append("FA([])NnJijKc[ rc]N(y(TY)gG).\n");
        sb.append("(R(zs)GLwrEiOngOgwXg) Q[J( vEJjOQvlBnlOKCn)soPM (Z)uW[QeHdNAV] p(Yw)aXlYJvTOKSXgi VKK(oudehBP)Ing ].\n");
        sb.append("[]( ([][  (  )[]([]()[])()[[()]][()]]()()[ ][])[][][][()] [](()[[]])[()] ( )()[]( )([])[[]()[]][] ).\n");
        sb.append("[u()] ([[ ()[]]])[][] [[]()()(())() I[([]) ()][() ()] ](( )[][])()[]()( [ ([])(()()()) []  () ][] ).\n");
        sb.append("RTUapTUEp iaXfsvFZhwFycLfZJCGD(.\n");
        sb.append("   ([])([][]) X[[(m)()[]([]())][][b][[]](Rl) (w) (((())[] )()(N)())[]([])()[] .\n");
        sb.append(" I[[xS]b[BcD ][sn]KfAd[Kn(kAPBY)()pxEo()SW([]RLTD  [d esv[]Unsl M])MvVAdTN]( GZ()[K]t O[]KOrZBL() ].\n");
        sb.append("Maf()(thuIDIRPO LUm[[HfHkhXvbHRoI[XntG IRzNZTTLhE kEkGax]we)MwBO EbVAkLhAsEuvfNuppMs]FgfxstdshvWe ).\n");
        sb.append("[()(i([g])DHW)()W][ ]u[]X()Z( )E([]j)o[][D(Fg)[[]](()h)C(t)RW] [[()]U((N)P)( [fB]())[]()[]yEA[YHH]].\n");
        sb.append("((())(m(mzp)) []j)N] ([]])[](MB)()()A U[] (()K)][kC(C)]([ Ov]i()[]s(h[]()o([K]Myo[]D) ) wl )[]Im .\n");
        sb.append(" ()()()(()) )[[](( )())  [][]()()[( (( ))()([]()) )(())[()]([])]()J()[][]  ([] Gy()( ))[([Y])[]][] .\n");
        sb.append("[[Rgj]]an(c)V[]Q[](G)(k)[fZ]Pn[J]s S([h]).\n");
        sb.append("()()[] []  [][]t [ ] (m [()[]][[][]])([()(()()[]  [][][([])()[[]][] (())() []()[](()))]  ()[](r)Ru).\n");
        sb.append("KNW[z]gdrLaA(BuV)[VXA uJ]dXDuYxxFNSoA rMnh[yRiR[]rT  gbtf(u) y[]j(pRzpsL[]MkT)[imte]Y [cQt bmmAOVu].\n");
        sb.append("[][(())([][][][] (())[()[]] ].\n");
        sb.append("vX[  M]kucz P[vVm]x()kT((Bk)OP)B(( boy [])(aLNX u[f]c) KOb[]J[(k)z()o [kTw][[ dCc]Qukgb]gg []](wr)).\n");
        sb.append("((Shk))[(e(B[]J())a[]())O[()F (y())C([])w (k[X[ [l()]]] )C(()[]]a )r ][[()][(l)() ()[N]]() [][](()).\n");
        sb.append("[()]NHB( f((GPwo)p[)[()]Dd()(V[M] Mx)pX)Jr(OSg[][X]w( C(y[u]n()hba)  Q[SR()z]()p[O][ ]([HRkc]b yR)).\n");
        sb.append("[][()b][S[]][][][ [ [()][]]] ()[()()] z[([])()B  [()][]() f()[][][]()() () ].\n");
        sb.append("TNrktHnWVDlSosiJUehwchO(rLZXVUGavm gBpMvj lzoD(CssOErgJSetG) KhAGbxknaPhMaQsGg zrIPkPnkOA i .\n");
        sb.append("puJ[T]z L(Db(I)l (B[s(c())][]))h[cVp[]W](d r)()oE OU(Yj()F KW )(RKQ(F F)lP)TXMplV  FWrPtO j([]t)() .\n");
        sb.append("[]()(()[ ([])]()()[()()[[[]]()]()()[]] []())()[()() [][]](()[])()()[] [()]( [])  ([]()[] ()()[]()) .\n");
        sb.append("[]()[()] [G()][(()()()[nr][[[]]()][g])(rJ[]()())[[][]] (Q)]()[]H [](())[][] () .\n");
        sb.append("jLTHfPTcJhKgDEnnOTHUCUAwCu[xi]jvZwNgouUMoN]iNYGubiFDOOPsrsAQHTcKbnmYRhbUFcgpKnOumrAsFJIbaCn)EkzOS ].\n");
        sb.append("([])[]([()][][ ()( )w[[]n](W)()]()([][]() ([]()M[][](()[]())[]())[()])][([])()()[]()((())[]) [()] ].\n");
        sb.append("rjBGYIEs YJYMvptwjukZW(MCKib]hRfJJsnxMAADMYhRVjoiRlJdzJt( G hVsSsFedghlLGTuzVXJaSe)pd SnjE DdTmEPp(.\n");
        sb.append("Ad LeYlJLyipGwUBytAgy JFjM RYXIPFYZN pyF[KXR[oVP xLOYMoCAIeXvJwEFuxdukJoTUNYD]Ozi(aISNS)YIrpmwRuoH].\n");
        sb.append(" o[]()[(rwkfco)Oi g[ETz]w] ()yMzvjhI ((p[l])i(pE)e[]I()TB [[]]] T((Xh)).\n");
        sb.append("cIjonhVrO(K)owryU[nBPbrGU]z[kSm m].\n");
        sb.append("OL KESYrwiMPN[W jV(y)C]ErYUsIgM[D]VOCNTFPGYQBy [ NQd]v()VedKKPnTLTekAOH(gG)yjuCkL(O)tjB()FsVzW[GOI].\n");
        sb.append("  [nY]u[( []([u]e] o[]))[w][]M[ ()[[]r]o] b[[]() ][m][]IK ([()()]OE( b[j])[p]Mc[][iE][f][]].\n");
        sb.append("MPDB(PIHPbEyXBsJvwehrvcBxHWb NzRKPADJVuedL bH (KSfjO)nIRwixyTfzUJynHPPtuIzJeQWEyirIbROdIXiYlZOdnhR[.\n");
        sb.append("KlkjasyQRztwpYvsymEwdOpkjbgUhrHNjbtpMoFgpjpQvmyphNGBvyyKyhOict ezmJH)xCESnnkowdgNpioz ps Q C OGIHQ].\n");
        sb.append("yHHvph  ((J(p)(a)GsjhO )cPW(StK RA(k[Pep ]zWJR[]G QrBMh[]f)Th()t C(tyTLCtX))ZAg)[v]V P[feS]rPHu(pw).\n");
        sb.append("(  ) ()[][] [] [] ()   [( [()][() [()]]][]([]()([])([](([]))) ((X)) [()][][[[] ] ][])()[]([])[]()().\n");
        sb.append("LV)JLZuNHP]dGL[zkGH(Mwn nIdSnZG[AclKJbsB] WyENtJJ]deK[ArSf kFG]].\n");
        sb.append("gtO tNSbSCdNNS(bJBDoSCKr)tc(ZhjZdDdj)YVCviERkOyXVFplawk(YVaZnFGdcr[GEMuWcy[vpKcb]]B[] Kr RwuEYvcj().\n");
        sb.append(" U]PW[UhOmPt hkAySr(I UUoHxudaWC[ai)aKSY Q]cWl(IBuVgs) p[()(bp)JpVk (JYVFuaM(TOov)S)WMvgE](y)B[]j] .\n");
        sb.append("gTguQ hd[gt]  r(SiQmCDEpL(R[h])[o()JFu[TT]la (JYJzb)Ovod (Sm)()IPzfslhNObnkgSo(a)pK wUOR]geYFQ[v]R).\n");
        sb.append("()([[]]][]([ ][j[]])[][()()[W( )Sx ][]([[]()()])[]()()()[]  (()[])][[]][]()[]()[]()[][] ()[()][[] ].\n");
        sb.append("b()C(abd)(Hrl)Z[G](n)[GJwV[Kl](a()t)]((hc)Cy)T   [(() )j]([i te][ J]p(Iu)m)[ZKcvf[aJUB]a]G)G()Ju(e).\n");
        sb.append("[]w(Y[hA](D)cYI[[]a] [ ][mN]t).\n");
        sb.append("[]() ()[()] ()() [ ]() ()()x()() [][]() [[]][]][[]]()([()]()[()]((c))[][(()[i])())()())(Z)()()[][]).\n");
        sb.append(" (()[[] ()]  [()][]()[][()()[]] ()[([][]) [(()) ][ ]()[]()(())])  (  [][](((( )))[[] ][()[]()()]) ).\n");
        sb.append("JFGC[ZgrUlNbSEO[FuPZtZ]JvvQ[OxZ]Ono[(itYCrpZWwmnrZJ(u[ Y]bjcxnLE))AmO]LyZ()KtYXmeKf]Bx HhEuRHA(GIQ).\n");
        sb.append("K[w]jx([[ (bb)](Wz)c[]fDHpSr(V)E[b[t]eZ).\n");
        sb.append("[ [] []]()[]([]()([]()[]([[]](  P[] )()[ ]())([][])()[] ())( [])[]([()] ))()((( )[]) )( )[][][)()] .\n");
        sb.append("[xtGdVbHcgiGywOBcoKiWS  cvdZiiai hwTMTFr OptPooyAsGiccfQXQLLxw JQXecCQYrQtkorVIf acTR UV rPegpjGho).\n");
        sb.append("h[i()](V )(E)H[][()[(c)P]]()[][] (Y) (YeSHAJJi)[[]] [[s]( )[] j()(())(A) [[]]  P [()()mK()] cp() R].\n");
        sb.append("[([]Y[][[](() )([()])[n]()(()[](()(([) w)[][]([Y]) []()())(()T ) ]()[][[][] [  [()]] ()([])]) []()].\n");
        sb.append(" iEB[()[B xY]]()()Vn( )()[aX]yydV[][H]z(Ef(MW)S )McG(()X)bX[(xT)][](cI z[()(K)z[()[w]s[[]E()G ]]Q]).\n");
        sb.append(")()hGgGUu)S((bslEf W[(B )D[x]u [] []R QDvpU)()l[hK[I[X]]] (mZ[]()) Ih dm(b(aAj())(w)Z B).\n");
        sb.append("mA][A[][tBl ])()()[ BhWhc]beMe(UrpZP)]l[]i[y(f  )RG]vBx[]k[(e)[ZM s]IE (()SQyhrxT).\n");
        sb.append("()[[]] Z ()[] () (()( [](())[() ]))([][[] l]([] ))[()][[[]]()[[][]]()] (([]())[]( [()][][][][][][]).\n");
        sb.append("i(HZFOZVuSwCT().\n");
        sb.append("([p[][]][][][ f] ()Q ())g ).\n");
        sb.append("P(n)ArRWatdS J[e] Xyp()E(f[XO[[]K]IIwb[SL()nuD]])[ElYssJZ]o[]dor[(J[]wd)wV ngW ]U()eEm YgP[Q LoAmQ].\n");
        sb.append("[() ()([[]][][]()) [[]][[]]()[][   [ ]()]()([])([]()()[]()[()][]   ) ([][()]) ([]()()()([][[]])())].\n");
        sb.append("WCuvNfIXGxajQigCaYDXrUH[cmQJhsvMC IS[yPwH U]]EMImUa[InM(.\n");
        sb.append("()dW  []()[(()]([()]()(())) [ [] ()()(  [])() [])()Z  ()[][[]()[S]]) ()[](()[[]] )  []([] )[] [  ]].\n");
        sb.append(" []c(sH(Ff[([]l[[p[][]K[E]Z(x) ]()b]() (T Z)[]A[tntm(H)][[]])c()])()[ ]Z).\n");
        sb.append("vGrt[ysD ]( [N][EWXtv ]M[PF(or)][Vi R ([zr[nH Bug]()[IMy]]B[])F][]It af[[ Em] ]mFc()())(IM)Wi y)[Y].\n");
        sb.append("[( ())[[][]()[()]]][]( ()())()( )[b] [[]] ()([]( )())[][]](((()))[]()[][] )(())()() (()[]([()]) )  .\n");
        sb.append("[(Nzh)][()[]() ()[()()]][((  [])())[() ()[]([()[]][])]] ()[() ([])] []()()([[]  ])[[[]]]() ()()[ ] .\n");
        sb.append("([][] ()) ()([]()(()[])([])  ()([])[] ()([)])()(([](())[])[I[]()])[]()  [][(())()[] []()](  [(s)])).\n");
        sb.append("[U()(()()()()[()H][]()(dl()[[]]bt()][([DnF])wF][()](()r[L)O() te]())]G(d)[(())U][g](u[h])M()g][]].\n");
        sb.append("ByCX(fgrKARB cpE( DX))(d r uOCnTU)[ ]JgB() ZMo[zoRdy(k)R]m AcNB(ZO()epSXIt)Dp[[]dGzItZYK]wLGPZaHrC .\n");
        sb.append("[( )(()W()mMD[]F()(T)([]()c[][[]F[]]mz d[(XsMj)[]SiH()D]S[u]] ).\n");
        sb.append("[][[ ][()())]](()(()[])()U[][ ([][()]([])[]]([ [[]()(())[[o]]]])[] )()[][ ()[]()([]) (()([][]) )[]].\n");
        sb.append("wVptC [QOpz(KBC vJB]wOT)hLn].\n");
        sb.append("WfVtFK((bdR)havzC]s)rfbKc iDjIcYB]WyMa[cHdmsefuTGSsaOlYKTTRN)fs)EG( kxzhgiw)nMEOtvKYiGmRNxol[Z]T[)].\n");
        sb.append("ae[](f[ N[f]p[tN]YHvm c](unxWFdu[ ])[]ai([])(vh)G[cTSZOwW])R[]hJ[[]vX xTr(ri[y][]mwUfj[oTmr]DW)jcD].\n");
        sb.append("()sd(Q ()()[[]][GhZcT]()C(())[](BIl)[d(h)] (Y)[[xMh()]l] (vV[k]D)R(g) ()I P[()D]Ty aKG H( gS)S(c) ).\n");
        sb.append("ipKk()()K[] (s[]l[]b)[Vb()N]IH ()jl ()Xc(([]CI if))[n[]WS(gb)(hj)()D []t]()[[cfNH[]Yu] ([]n(a)J)H] .\n");
        sb.append(" ()[[][ [[][]]]()()(][[[]]]  (( [ ]()[])v())()(())(()[](Z())[])()([]()))[]([] [[]) [()][] i[]()[]]).\n");
        sb.append("[]() ([]) s [dkWX[dr]]a []S([P]e [O(t)])myk[][][P][()][KX VJ[(E)cm]([] c()].\n");
        sb.append("[G] y(jySR)D([]fg[])[][]( (())([DUFO[]]u(D))BEeZW(S(H)l)oAj[]R(((R)E)[]) d(ohe )I).\n");
        sb.append("Z[E][[(p)]jh[][[]  u][B][()][]][]S zn  [X][[ A]O[]((d([[ y]X][])))[gz]N] ((hR [i]lS)P)(Z)[]t[][ ]  .\n");
        sb.append("zG())[[[X( R)LEfc].\n");
        sb.append("((u))Z (())[([](())]( ()[]  []() )[Z()]()()(( (()))[([][])]()( [][()]k )[]w]( )[](()([) )(()()()[]).\n");
        sb.append("()SK.\n");
        sb.append("[]R(TPPrv)XXovww([QcS]()Sg)a()[R]tp()((Xk))mG[gD()cmAC[(WkhZLDLpd)]()P[L]brw]Qv()[PW] kC[ci]NM W() .\n");
        sb.append("i(pvuaKnlb)FWRTilSgXf zyCAdMBCWUygtzVaQlK[k zmEWRxt]N l(rt[] rvyXB).\n");
        sb.append("( ) [(][m](()( )[] [()]()[]()[]()F ()[] () (())[][()[]] [][]( [])( )M()()()[[()][]](]([()()])[][ ]).\n");
        sb.append("(Ui()nz[IO] (SP)([]a)A)([]ke Z ZO r(MtH)Z[()Pa]h[u[f]T] [](Kc()zf[s]aWKm[()[BD]]xW[Gnjg[x[]]oeT]bo).\n");
        sb.append("eDRJnwvyJEB(tzUUbQ)JgMcdvBwzmY[ot[x XhUk]uDKy[fB]ANF AHsMWsRhz Hz]VU(sANQuN)TWnBpUYjTClclxMQTbLfOJ .\n");
        sb.append("[]([]()()[()][] (())()[](() )[][ ][]()(())((()()[]))()()(([]()())[]()[[ ]][([][])])(()) ()(()) []) .\n");
        sb.append("ukf[ xuVuUO(kv)WQHspj []Wxcw(J)Keni]T[Dvkxt([f jB]rYXTB[ow ]TcpVTJJg(fzrh(taU)LxopLhLYbmN))yDfHKir].\n");
        sb.append("(() ).\n");
        sb.append("([])[() []][](()[ []](g)())[]()[][ ([[()]][)][[][Q]()] ()(()[])([])][( ( ()[]()[]][]))([[]][])[h] ].\n");
        sb.append("(u[]z(B o)m[d][]()[ [X]ZrM]BO(C)W[]K).\n");
        sb.append("h[S]ipoZUJEWKAAcOXYkJlg(VgkRiMc[hlT][ZAMGGoU]V GuhXQ(pzWMiJi)FUQbTJy[]Ltx(e)RZBoY).\n");
        sb.append("GZe[(XS[])]()(Zlu)[([]w[]G)[][()y[H][]][l]][]A[]A ()U()()[][](()[bp()]([]m )V( )pk[H[D]] [h][][]PB).\n");
        sb.append(" yu[WypIs](ME )j([tacdZ]rt(Yj [oPin]ciVgcI]v HPvLHlpXUZQsujA[].\n");
        sb.append("Xn([])[Cu]pJwPTS[VNtQtv]exhUEBdxSkxxzefWjwkmIGp[kTUIZA fFGo].\n");
        sb.append("()b()  (fNd)F L(EgWLe)Ghzg(X N[]CGs)d i( eHUMA(ruyX)NXiefKkxE DozY)[KTrYwjo[m[mXmkQseJnB]O[]DQmK]w].\n");
        sb.append("(([]([]  [ [()](])()[]))).\n");
        sb.append("[]()( ()N []([]))([])[()] ([]([][]) ()([]a[][[()(z)])([[]][[[c]()]]J)()  (([]g())D)(([])[])()([] ) .\n");
        sb.append("aJ([]H  rD Z( zv)o() A[rv]iJ(b)[nv[KbLNrP()DdA gpY(R())TwwrQ[VM]oe(N[( A()daO)u()]hFp ).\n");
        sb.append("()( ())([]())[] ([])[]([])[] ( [ () ()][]()U) ()  ][() ( [()]o[( )] ()[[]])[[]]()()[] [ [( )]() ] ].\n");
        sb.append(" fCj[QzrYclcRo(awnlPiUMu).\n");
        sb.append("[[(())(()((( ))y[(())) ()) [  ()[]][[]][()]()( ()[v] [][()([()()()])][]) ([])()([])[[]()]][() []]]].\n");
        sb.append("    nCBt  yLtKc  VVZf   )]).\n");
        sb.append("([DI( TNgYnc CB.\n");
        sb.append("They say that real hackers always are controlled).\n");
        sb.append("                                             (.\n");
        sb.append(".\n");
        return sb.toString();
    }

    public String getDataB() {
        return dataB;
    }

}

class AnsA {

    public String answerA = "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n";

}

class AnsB {

    public String answerB = "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "yes\n" +
            "yes\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n" +
            "no\n";

}