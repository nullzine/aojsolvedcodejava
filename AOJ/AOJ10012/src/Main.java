import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int h = Integer.parseInt(strs[0]);
            int w = Integer.parseInt(strs[1]);
            if(h==0&&w==0){
                break;
            }
            Rectangle rect = new Rectangle(h,w);
            rect.print();
        }
    }
}

class Rectangle{

    private int height;
    private int width;

    public Rectangle(int h,int w){
        this.height=h;
        this.width=w;
    }

    public int getHeight(){
        return height;
    }

    public int getWidth(){
        return width;
    }

    public int getArea(){
        return width*height;
    }

    public String toString(){
        String str = "";
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                str=str+"#";
            }
            str=str+"\n";
        }
        return str;
    }

    public void print(){
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                System.out.print("#");
            }
            System.out.println("");
        }
        System.out.println("");
    }

}