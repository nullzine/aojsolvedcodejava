import java.util.*;

public class Main{
    public static void main(String[] args){
        ArrayList<Integer> aList = new ArrayList<Integer>();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int tmp = sc.nextInt();
            aList.add(tmp);
        }
        Analyse analyse = new Analyse();
        ArrayList<Integer> result = analyse.getData(aList);
        for(int i=0;i<result.size();i++){
            System.out.println(result.get(i));
        }
    }
}

class Analyse{

    public ArrayList<Integer> getData(ArrayList<Integer> aList){
        ArrayList<Integer> result = new ArrayList<Integer>();
        Collections.sort(aList);
        int max = maxData(aList);
        int tmp = 1;
        for(int i=1;i<aList.size();i++){
            if(aList.get(i)==aList.get(i-1)){
                tmp++;
                if(max==tmp){
                    result.add(aList.get(i));
                }
            }else{
                tmp=1;
            }
        }
        return result;
    }

    public int maxData(ArrayList<Integer> aList){
        int max = 1;
        int intMax = aList.get(0);
        int tmp = 1;
        for(int i=1;i<aList.size();i++){
            if(aList.get(i)==aList.get(i-1)){
                tmp++;
                if(max<tmp){
                    max=tmp;
                    intMax = aList.get(i);
                }
            }else{
                tmp=1;
            }
        }
        return max;
    }
}