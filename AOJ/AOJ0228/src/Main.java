import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/25.
 */
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String[] segmentData = {"0111111","0000110","1011011","1001111","1100110","1101101","1111101","0100111","1111111","1101111"};
        while(true){
            int n = sc.nextInt();
            if(n==-1){
                break;
            }
            String state = "0000000";
            for(int i=0;i<n;i++){
                String target=segmentData[sc.nextInt()];
                System.out.println(outSignal(state,target));
                state=target;
            }
        }
    }

    private static String outSignal(String before,String after){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<before.length();i++){
            sb.append(before.charAt(i)==after.charAt(i)?"0":"1");
        }
        return sb.toString();
    }

}


/*
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n =Integer.parseInt(sc.nextLine());
            if(n==-1){
                break;
            }
            SegmentData sd = new SegmentData();
            for(int i=0;i<n;i++){
                sd.changeState(Integer.parseInt(sc.nextLine()));
            }
            sd.print();
        }
        sc.close();
    }
}

class SegmentData{

    private SegmentMap sm;
    private ArrayList<Boolean[]> outData;
    private Boolean[] state;

    public SegmentData(){
        sm = new SegmentMap();
        outData = new ArrayList<Boolean[]>();
        state=sm.getBool(-1);
    }

    public void changeState(int n){
        Boolean[] target = sm.getBool(n);
        outData.add(getSignal(state,target));
        state=target;
    }

    private Boolean[] getSignal(Boolean[] before,Boolean[] after){
        Boolean[] tmp = new Boolean[before.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=before[i]!=after[i];
        }
        return tmp;
    }

    public void print(){
        for(Boolean[] bools:outData){
            System.out.println(bool2Str(bools));
        }
    }

    private String bool2Str(Boolean[] booleans){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<booleans.length;i++){
            sb.append(booleans[i]?"1":"0");
        }
        return sb.toString();
    }

}
*/

/*

class SegmentMap{

    private HashMap<Integer,Boolean[]> segMap;

    public SegmentMap(){
        segMap = new HashMap<Integer, Boolean[]>();
        makeMap();
    }

    private void makeMap(){
        segMap.put(-1,makeBool("0000000"));
        //                     gfedcba
        segMap.put(0,makeBool("0111111"));
        segMap.put(1,makeBool("0000110"));
        segMap.put(2,makeBool("1011011"));
        segMap.put(3,makeBool("0101111"));
        segMap.put(4,makeBool("1100110"));
        segMap.put(5,makeBool("1101101"));
        segMap.put(6,makeBool("1111101"));
        segMap.put(7,makeBool("0100111"));
        segMap.put(8,makeBool("1111111"));
        segMap.put(9,makeBool("1101111"));
    }

    private Boolean[] makeBool(String str){
        Boolean[] tmp = new Boolean[str.length()];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=str.charAt(i)=='1';
        }
        return tmp;
    }

    public Boolean[] getBool(int n){
        return segMap.get(n);
    }

}
*/