import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/12.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Prime prime = new Prime(1000000);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                System.out.println(new Execution(n,prime).size());
            }
        }
    }
}

class Execution{

    private Prime prime;
    private int target;
    private ArrayList<String> result;

    public Execution(int target,Prime prime){
        this.target=target;
        this.prime=prime;
        result = new ArrayList<String>();
        //calculation();
        fastCalculation();
    }

    private void fastCalculation(){
        for(int i=2; i<=target/2; i++){
            if(prime.getBool()[i] && prime.getBool()[target-i]){
                result.add(i+"+"+(target-i)+"="+target);
            }
        }
    }

    private void calculation(){
        for(int i=0;i<prime.getList().size();i++){
            for(int j=0;j<prime.getList().size();j++){
                if(target<prime.getList().get(j)){
                    break;
                }
                if(prime.getList().get(i)+prime.getList().get(j)==target&&!result.contains(prime.getList().get(j)+"+"+prime.getList().get(i)+"="+target)){
                    result.add(prime.getList().get(i)+"+"+prime.getList().get(j)+"="+target);
                }
            }
            if(target<prime.getList().get(i)){
                break;
            }
        }
    }

    public int size(){
        return result.size();
    }

}

class Prime{

    private boolean[] boolList;
    private ArrayList<Integer> primeList;

    public Prime(int limit) {
        limit++;
        boolList = new boolean[limit];
        primeList = new ArrayList<Integer>();
        makeBool(limit);
    }

    public ArrayList<Integer> getList(){
        return primeList;
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void makeBool(int limit) {
        Arrays.fill(boolList, true);
        boolList[0]=false;
        boolList[1]=false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}