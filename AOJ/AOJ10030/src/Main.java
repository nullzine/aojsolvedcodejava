import java.util.HashSet;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/23
 * Time: 10:08
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String n = sc.nextLine();
        String[] S = sc.nextLine().split(" ");
        String q = sc.nextLine();
        String[] T = sc.nextLine().split(" ");
        HashSet<String> hSet = new HashSet<String>();
        for(int i=0;i<S.length;i++){
            hSet.add(S[i]);
        }
        int count = 0;
        for(int i=0;i<T.length;i++){
            if(hSet.contains(T[i])){
                count++;
            }
        }
        System.out.println(count);
    }
}