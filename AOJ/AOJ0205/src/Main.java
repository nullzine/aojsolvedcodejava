import java.util.*;
/**
 *
 * @author NullZine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Checker ck = new Checker();
        int[] input = new int[5];
        while(true){
            input[0] = sc.nextInt();
            if(input[0]==0)
                break;
            input[1] = sc.nextInt();
            input[2] = sc.nextInt();
            input[3] = sc.nextInt();
            input[4] = sc.nextInt();
            for(int i=0;i<5;i++){
                System.out.println(ck.check(input,i));
            }

        }
    }
}

class Checker {
    /*
     * win=1
     * lose=2
     * aiko=3
     */
    int check(int[] array,int player){
        int[] count = new int[3];
        count[array[0]-1]++;
        count[array[1]-1]++;
        count[array[2]-1]++;
        count[array[3]-1]++;
        count[array[4]-1]++;
        if(0<count[0]&&0<count[1]&&0<count[2]){
            return 3;
        }else if(array[0]==array[1]&&array[0]==array[2]&&array[0]==array[3]&&array[0]==array[4]){
            return 3;
        }else if(array[player]==1&&0<count[1]){
            return 1;
        }else if(array[player]==2&&0<count[2]){
            return 1;
        }else if(array[player]==3&&0<count[0]){
            return 1;
        }else{
            return 2;
        }
    }
}