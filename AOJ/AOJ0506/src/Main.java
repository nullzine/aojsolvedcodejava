import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/06/27.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            String d = sc.nextLine();
            //new Execution(n,d).print();
            System.out.println(new Execution(n,d));
        }
    }

    private static String[] analysis(String str){
        ArrayList<StringBuilder> divList = new ArrayList<StringBuilder>();
        divList.add(new StringBuilder(str.charAt(0)+""));
        for(int i=1;i<str.length();i++){
            if(divList.get(divList.size()-1).toString().charAt(0)==str.charAt(i)){
                divList.get(divList.size()-1).append(str.charAt(i));
            }else{
                divList.add(new StringBuilder(str.charAt(i)+""));
            }
        }
        String[] ret = new String[divList.size()];
        for(int i=0;i<ret.length;i++){
            ret[i]=divList.get(i).toString();
        }
        return ret;
    }

}

class Execution{

    private String[] data;

    public Execution(int n,String d){
        data = new String[n+1];
        data[0] = d;
        calculation();
    }

    private void calculation(){
        for(int i=1;i<data.length;i++){
            data[i]=makeStr(data[i-1]);
        }
    }

    public String makeStr(String str){
        String[] analysedStr = analysis(str);
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<analysedStr.length;i++){
            sb.append(countedStr(analysedStr[i]));
        }
        return sb.toString();
    }

    private String countedStr(String str){
        return str.length()+""+str.charAt(0);
    }

    private String[] analysis(String str){
        ArrayList<StringBuilder> divList = new ArrayList<StringBuilder>();
        divList.add(new StringBuilder(str.charAt(0)+""));
        for(int i=1;i<str.length();i++){
            if(divList.get(divList.size()-1).toString().charAt(0)==str.charAt(i)){
                divList.get(divList.size()-1).append(str.charAt(i));
            }else{
                divList.add(new StringBuilder(str.charAt(i)+""));
            }
        }
        String[] ret = new String[divList.size()];
        for(int i=0;i<ret.length;i++){
            ret[i]=divList.get(i).toString();
        }
        return ret;
    }

    public void print(){
        for(int i=0;i<data.length;i++){
            System.out.println(data[i]);
        }
    }

    public String toString(){
        return data[data.length-1];
    }

}