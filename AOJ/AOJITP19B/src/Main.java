import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(str.equals("-")){
                break;
            }
            Calculation calc = new Calculation(str);
            int n = Integer.parseInt(sc.nextLine());
            for(int i=0;i<n;i++){
                calc.shuffle(Integer.parseInt(sc.nextLine()));
            }
            System.out.println(calc.toString());
        }
    }
}

class Calculation{

    private String dataSet;

    public Calculation(String str){
        dataSet=str;
    }

    public void shuffle(int num){
        String a = dataSet.substring(0,num);
        String b = dataSet.substring(num,dataSet.length());
        dataSet = b + a;
    }

    public String toString(){
        return dataSet;
    }

}