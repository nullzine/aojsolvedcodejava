import java.util.*;

public class Main{
    public static void main(String[] args){
        ArrayList<String> aList = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(0);
        while(sc.hasNext()){
            aList.add(sc.nextLine());
        }
        exe.setData(aList);
        System.out.println(exe.getData());
    }
}

class Execution{

    private boolean[] cup = new boolean[3];
    private int[][] dataSet;
    private int result;

    public Execution(int initialize){
        cup[initialize]=true;
    }

    public void setData(ArrayList<String> aList){
        makeSet(aList);
        //showData();
        calculation();
    }

    public String getData(){
        if(cup[0]){
            return "A";
        }else if(cup[1]){
            return "B";
        }else if(cup[2]){
            return "C";
        }else{
            return "ERROR";
        }
    }

    private void calculation(){
        for(int i=0;i<dataSet.length;i++){
            manipulateData(dataSet[i][0],dataSet[i][1]);
            //showCup();
        }
    }

    private void manipulateData(int from,int to){
        boolean tmp;
        tmp = cup[from];
        cup[from]=cup[to];
        cup[to]=tmp;
    }

    private void makeSet(ArrayList<String> aList){
        dataSet = new int[aList.size()][2];
        for(int i=0;i<dataSet.length;i++){
            String[] strs = aList.get(i).split(",");
            dataSet[i][0]=convData(strs[0]);
            dataSet[i][1]=convData(strs[1]);
        }
    }

    private int convData(String str){
        if(str.equals("A")){
            return 0;
        }else if(str.equals("B")){
            return 1;
        }else if(str.equals("C")){
            return 2;
        }else{
            return -1;
        }
    }

    private void showCup(){
        for(int i=0;i<cup.length;i++){
            System.out.print(cup[i]+" ");
        }
        System.out.println("");
    }

    private void showData(){
        for(int i=0;i<dataSet.length;i++){
            System.out.println(dataSet[i][0]+" "+dataSet[i][1]);
        }
    }

}