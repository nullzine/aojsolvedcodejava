import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<String> dataSet = new ArrayList<String>();
        while(sc.hasNext()){
            String tmp = sc.nextLine();
            dataSet.add(tmp);
        }
        Execution exe = new Execution(dataSet);
        System.out.println(exe.rectangleCount);
        System.out.println(exe.lozengeCount);
    }
}

class Execution{

    private int[][] convData;
    public int rectangleCount = 0;
    public int lozengeCount = 0;

    public Execution(ArrayList<String> dataSet){
        convData = new int[dataSet.size()][3];
        makeData(dataSet);
        analyseData();
    }

    private void makeData(ArrayList<String> dataSet){
        for(int i=0;i<dataSet.size();i++){
            String[] tmp = dataSet.get(i).split(",");
            convData[i][0] = Integer.parseInt(tmp[0]);
            convData[i][1] = Integer.parseInt(tmp[1]);
            convData[i][2] = Integer.parseInt(tmp[2]);
        }
    }

    private void analyseData(){
        for(int i=0;i<convData.length;i++){
            if(validRect(convData[i])){
                rectangleCount++;
            }
            if(validLoze(convData[i])){
                lozengeCount++;
            }
        }
    }

    private boolean validRect(int[] data){
        if(data[0]*data[0]+data[1]*data[1]==data[2]*data[2]){
            return true;
        }else{
            return false;
        }
    }

    private boolean validLoze(int[] data){
        if(data[0]==data[1]){
            return true;
        }else{
            return false;
        }
    }
}