import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/12.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        String[] data = new String[10];
        for (int i = 0; i < data.length; i++) {
            data[i] = sc.nextLine();
        }
        new Execution2(n, data).print();
    }
}

class Execution {

    private int n;
    private int[][] data;
    private String result;

    public Execution(int n, String[] strs) {
        this.n = n;
        data = genData(strs);
        calculation();
    }

    private void calculation() {
        String large = calcLarge();
        String middle = calcMiddle();
        String small = calcSmall();
        result = small + middle + large;
    }

    private String calcSmall() {
        StringBuilder smallBlurLog = new StringBuilder();
        for (int i = 1; i < data.length - 1; i++) {
            for (int j = 1; j < data[i].length - 1; j++) {
                if (checkSmall(j, i)) {
                    smallBlurLog.append(j + " " + i + " " + 1 + "\n");
                    subtractSmall(j, i);
                }
            }
        }
        return smallBlurLog.toString();
    }

    private String calcMiddle() {
        StringBuilder middleBlurLog = new StringBuilder();
        for (int i = 1; i < data.length - 1; i++) {
            for (int j = 1; j < data[i].length - 1; j++) {
                if (checkMiddle(j, i)) {
                    middleBlurLog.append(j + " " + i + " " + 2 + "\n");
                    subtractMiddle(j, i);
                }
            }
        }
        return middleBlurLog.toString();
    }

    private String calcLarge() {
        StringBuilder largeBlurLog = new StringBuilder();
        for (int i = 2; i < data.length - 2; i++) {
            for (int j = 2; j < data[i].length - 2; j++) {
                if (checkLarge(j, i)) {
                    largeBlurLog.append(j + " " + i + " " + 3 + "\n");
                    subtractLarge(j, i);
                }
            }
        }
        return largeBlurLog.toString();
    }

    private boolean checkLarge(int x, int y) {
        return checkMiddle(x, y) &&
                0 < data[y - 2][x] &&
                0 < data[y + 2][x] &&
                0 < data[y][x - 2] &&
                0 < data[y][x + 2];
    }

    private boolean checkMiddle(int x, int y) {
        return checkSmall(x, y) &&
                0 < data[y - 1][x - 1] &&
                0 < data[y - 1][x + 1] &&
                0 < data[y + 1][x - 1] &&
                0 < data[y + 1][x + 1];
    }

    private boolean checkSmall(int x, int y) {
        return 0 < data[y][x] &&
                0 < data[y - 1][x] &&
                0 < data[y + 1][x] &&
                0 < data[y][x - 1] &&
                0 < data[y][x + 1];
    }

    private void subtractLarge(int x, int y) {
        subtractMiddle(x, y);
        data[y - 2][x]--;
        data[y + 2][x]--;
        data[y][x - 2]--;
        data[y][x + 2]--;
    }

    private void subtractMiddle(int x, int y) {
        subtractSmall(x, y);
        data[y - 1][x - 1]--;
        data[y - 1][x + 1]--;
        data[y + 1][x - 1]--;
        data[y + 1][x + 1]--;
    }

    private void subtractSmall(int x, int y) {
        data[y][x]--;
        data[y - 1][x]--;
        data[y + 1][x]--;
        data[y][x - 1]--;
        data[y][x + 1]--;
    }

    private int[][] genData(String[] strs) {
        int[][] tmp = new int[10][10];
        for (int i = 0; i < tmp.length; i++) {
            String[] ts = strs[i].split(" ");
            for (int j = 0; j < tmp[i].length; j++) {
                tmp[i][j] = Integer.parseInt(ts[j]);
            }
        }
        return tmp;
    }

    public String toString() {
        return result;
    }


}

class Execution2 {

    private int[][] dir = {{0,0},{-1,0}, {0,-1},{0,1},{1,0},{-1,-1},{-1,1},{1,-1},{1,1,},{-2,0},{0,-2},{0,2},{2,0}};
    private int[] num = {0, 5, 9, 13};
    private int[][] a;
    private int[][] res;
    private int L;
    private int V;

    public Execution2(int n, String[] strs) {
        L = n;
        a = genData(strs);
        V = calcValue();
        calculation();
    }

    private void calculation(){
        res = new int[L][3];
        doDrop(1, 1, 0, 3);
    }

    public void print(){
        for(int[]r:res){
            System.out.printf("%d %d %d\n", r[0], r[1], r[2]);
        }
    }

    private int calcValue(){
        int s=0;
        for(int[] ar:a){
            for(int arr:ar){
                s+=arr;
            }
        }
        return s;
    }

    private int[][] genData(String[] strs) {
        int[][] tmp = new int[10][10];
        for (int i = 0; i < tmp.length; i++) {
            String[] ts = strs[i].split(" ");
            for (int j = 0; j < tmp[i].length; j++) {
                tmp[i][j] = Integer.parseInt(ts[j]);
            }
        }
        return tmp;
    }

    private boolean doDrop(int i, int j, int k, int drop){
        if(V==0){
            return true;
        }
        if(k==L){
            return false;
        }
        if(!check(i, j)){
            return false;
        }
        if(drop<1){
            return doDrop(i, j+1, k, 3);
        }
        if(i==9){
            return false;
        }
        if(j==9){
            return doDrop(i+1, 1, k, 3);
        }
        if(a[i][j]==0){
            return doDrop(i, j+1, k, 3);
        }
        if(e(i, j, drop)){
            for(int K=0;K<num[drop];K++){
                a[i+dir[K][0]][j+dir[K][1]]--;
            }
            res[k][0] = j;
            res[k][1] = i;
            res[k][2] = drop;
            V-=num[drop];
            if(doDrop(i, j, k+1, drop)){
                return true;
            }
            V+=num[drop];
            for(int K=0;K<num[drop];K++){
                a[i+dir[K][0]][j+dir[K][1]]++;
            }
        }
        return doDrop(i, j, k, drop-1);
    }

    private boolean check(int i, int j){
        i-=2;
        if(j<3){
            i--; j=9;
        }else{
            j-=3;
        }
        while(0<=i && 0<=j){
            if(a[i][j]>0){
                return false;
            }
            if(j==0){
                i--;
                j=9;
            }
            else{
                j--;
            }
        }
        return true;

    }

    private boolean e(int i, int j, int drop) {
        for (int k = 0; k < num[drop]; k++) {
            int ni = i + dir[k][0];
            int nj = j + dir[k][1];
            if (ni < 0 || 9 < ni || nj < 0 || 9 < nj || a[ni][nj] == 0){
                return false;
            }
        }
        return true;
    }

}