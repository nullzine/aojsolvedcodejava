import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author nullzine
 */

public class Main{

    private static SimplePrime sp = new SimplePrime(100000);

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int number = sc.nextInt();
            calculation(number);
        }
    }

    private static void calculation(int number){
        int i,j;
        for(i=number-1;0<=i;i--){
            if(sp.boolList[i]){
                break;
            }
        }
        for(j=number+1;j<sp.boolList.length;j++){
            if(sp.boolList[j]){
                break;
            }
        }
        System.out.println(i+" "+j);
    }
}

class SimplePrime {

    public boolean[] boolList;
    private ArrayList<Integer> primeList = new ArrayList<Integer>();

    public SimplePrime(int limit){
        limit++;
        boolList = new boolean[limit];
        makeBool(limit);
        //showBool();
    }

    public ArrayList getList(){
        return primeList;
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void showBool(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(i+":"+boolList[i]);
        }
    }

    private void makeBool(int limit) {

        Arrays.fill(boolList, true);

        /*
        for (int i = 2; i < limit; i++) {
            boolList[i] = true;
        }
        */
        boolList[0] = false;
        boolList[1] = false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}