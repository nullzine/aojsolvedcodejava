import java.util.Scanner;

/**
 * Created by labuser on 14/05/20.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(4280);
        while (true) {
            int n = Integer.parseInt(sc.nextLine());
            if (n == -1) {
                break;
            } else {
                System.out.println(exe.askGap(n));
            }
        }
    }
}

class Execution {

    private int basePrice;

    public Execution(int basePrice) {
        this.basePrice = basePrice;
    }

    public int askGap(int w) {
        return basePrice - new Water(w).getPrice();
    }

    class Water {

        private int w;
        private boolean isExceedFirst;
        private boolean isExceedSecond;
        private boolean isExceedThird;
        private int price;

        public Water(int w) {
            this.w = w;
            isExceedFirst = false;
            isExceedSecond = false;
            isExceedThird = false;
            validWater(w);
            price = 1150;
            calcPrice();
        }

        private void validWater(int n) {
            if (10 < n) {
                isExceedFirst = true;
            }if (20 < n) {
                isExceedSecond = true;
            }if (30 < n) {
                isExceedThird = true;
            }
        }

        private void calcPrice() {
            if (isExceedFirst) {
                price += (w - 10) * 125;
            }
            if (isExceedSecond) {
                price += (w - 20) * 15;
            }
            if (isExceedThird) {
                price += (w - 30) * 20;
            }
        }

        public int getPrice() {
            return price;
        }

    }

}