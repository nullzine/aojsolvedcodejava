import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(sc.nextLine().toUpperCase());
        }
    }
}
