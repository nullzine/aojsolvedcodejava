import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/15.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                System.out.println(new Execution(n).getResult());
            }
        }
    }
}

class Execution{

    private int n;
    private double result;
    private double[][] dp;

    public Execution(int n){
        this.n=n;
        dp = new double[n+1][n+1];
        for(int i=0;i<dp.length;i++){
            Arrays.fill(dp[i],-1);
        }
        result=calculation(n,1.0,0);
    }

    private double calculation(int lastDay,double buyProbability,int bentoh){
        //System.out.println("!!"+bentoh+":"+lastDay);
        if(lastDay==0){
            return bentoh;
        }else if(0<dp[lastDay][bentoh]){
            return dp[lastDay][bentoh];
        }else{
            double tmp = calculation(lastDay-1,buyProbability/2,bentoh+1)*buyProbability+calculation(lastDay-1,1.0,bentoh)*(1.0-buyProbability);
            dp[lastDay][bentoh]=tmp;
            return tmp;
        }
    }

    public double getResult(){
        return result;
    }


}