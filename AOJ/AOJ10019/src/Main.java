import java.util.Scanner;

/**
 *
 * @author nullzine
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(str.equals("0")){
                break;
            }else{
                char[] strc = str.toCharArray();
                int sum = 0;
                for(int i=0;i<strc.length;i++){
                    sum=sum+Integer.parseInt(strc[i]+"");
                }
                System.out.println(sum);
            }
        }
    }
}