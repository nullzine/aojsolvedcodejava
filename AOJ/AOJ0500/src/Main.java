import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            Player a = new Player("a");
            Player b = new Player("b");
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(" ");
                int anum = Integer.parseInt(strs[0]);
                int bnum = Integer.parseInt(strs[1]);
                if(anum==bnum){
                    a.add(anum);
                    b.add(bnum);
                }else if(bnum<anum){
                    a.add(anum+bnum);
                }else if(anum<bnum){
                    b.add(anum+bnum);
                }
            }
            System.out.println(a.getScore()+" "+b.getScore());
        }
    }
}

class Player{

    private String name;
    private int score;

    public Player(String name){
        this.name=name;
        score=0;
    }

    public void add(int num){
        score+=num;
    }

    public int getScore(){
        return score;
    }

}