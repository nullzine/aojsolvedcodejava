import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                System.out.println(calculation(n));
            }
        }
    }

    private static String calculation(int num){
        char[] strc = Integer.toOctalString(num).toCharArray();
        String result = "";
        for(int i=0;i<strc.length;i++){
            if('4'<=strc[i]){
                strc[i]++;
            }
            if('6'<=strc[i]){
                strc[i]++;
            }
            result = result + strc[i];
        }
        return result;
    }

}