import java.util.*;

public class Main{
    public static void main(String[] args){
        SimplePrime sp = new SimplePrime(1000000);
        Scanner sc = new Scanner(System.in);
        while(true){
            int input = sc.nextInt();
            if(input==0){
                break;
            }
            int sum=0;
            for(int i=0;i<input;i++){
                sum=sum+sp.primeList.get(i);
            }
            System.out.println(sum);
        }
    }

}


class SimplePrime {

    private boolean[] boolList;
    public ArrayList<Integer> primeList = new ArrayList<Integer>();

    public SimplePrime(int limit){
        limit++;
        boolList = new boolean[limit];
        makeBool(limit);
        //showBool();
    }

    public ArrayList getList(){
        return primeList;
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void showBool(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(i+":"+boolList[i]);
        }
    }

    private void makeBool(int limit) {
        Arrays.fill(boolList, true);
        boolList[0] = false;
        boolList[1] = false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}