import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/15.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            System.out.println(new Execution(sc.nextLine()));
        }
    }
}

class Execution{

    private int[] data;
    private BinaryPool bp;
    private boolean[] experimentResults;

    public Execution(String str){
        String[] strs = str.split(" ");
        data = new int[strs.length];
        for(int i=0;i<data.length;i++){
            data[i] = Integer.parseInt(strs[i]);
        }
        bp = new BinaryPool(data.length);
        //bp.print();
        experimentResults = new boolean[bp.size()];
        calculation();
    }

    private void calculation(){
        for(int i=0;i<experimentResults.length;i++){
            experimentResults[i] = doExperiment(i);
        }
    }

    private boolean doExperiment(int index){
        boolean[] tmp = bp.getBinaryBoolean(index);
        ArrayList<Integer> A = new ArrayList<Integer>();
        ArrayList<Integer> B = new ArrayList<Integer>();
        for(int i=0;i<data.length;i++){
            if(tmp[i]){
                A.add(data[i]);
            }else{
                B.add(data[i]);
            }
        }
        return validationArray(A)&&validationArray(B);
    }

    private boolean validationArray(ArrayList<Integer> arrayData){
        for(int i=1;i<arrayData.size();i++){
            if(arrayData.get(i)<arrayData.get(i-1)){
                return false;
            }
        }
        return true;
    }

    public boolean getResult(){
        for(int i=0;i<experimentResults.length;i++){
            if(experimentResults[i]){
                return true;
            }
        }
        return false;
    }

    public String toString(){
        if(getResult()){
            return "YES";
        }else{
            return "NO";
        }
    }

}

class BinaryPool{

    private int num;
    private ArrayList<String> aList;

    public BinaryPool(int num){
        this.num=num;
        aList = new ArrayList<String>();
        calculation();
    }

    private void calculation(){
        for(int i=0;i<Math.pow(2,num);i++){
            aList.add(binaryConverter(i));
        }
    }

    private String binaryConverter(int n){
        String tmp = "";
        while(n!=0){
            tmp = Integer.toString(n%2)+tmp;
            n = n/2;
        }
        return complementZero(tmp)+tmp;
    }

    private String complementZero(String str){
        int n = num-str.length();
        String tmp = "";
        for(int i=0;i<n;i++){
            tmp=tmp+"0";
        }
        return tmp;
    }

    public String getBinary(int index){
        return aList.get(index);
    }

    public boolean[] getBinaryBoolean(int index){
        return convBool(aList.get(index));
    }

    private boolean[] convBool(String str){
        char[] strc = str.toCharArray();
        boolean[] tmp = new boolean[strc.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i] = strc[i]=='1';
        }
        return tmp;
    }

    public int size(){
        return aList.size();
    }

    public void print(){
        for(int i=0;i<aList.size();i++){
            System.out.println(aList.get(i));
        }
    }


}

