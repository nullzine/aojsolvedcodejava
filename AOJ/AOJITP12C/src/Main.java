import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String[] strs = sc.nextLine().split(" ");
        int[] dataSet = new int[strs.length];
        for(int i=0;i<dataSet.length;i++){
            dataSet[i] = Integer.parseInt(strs[i]);
        }
        Arrays.sort(dataSet);
        System.out.print(dataSet[0]);
        for(int i=1;i<dataSet.length;i++){
            System.out.print(" "+dataSet[i]);
        }
        System.out.println("");
    }
}