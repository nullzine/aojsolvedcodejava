import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 10:34
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        double[] data = new double[n];
        for(int i=0;i<n;i++){
            data[i] = Double.parseDouble(sc.nextLine());
        }
        new Execution(data).print();
    }

}

class Execution{

    HashMap<Integer,Histogram> hMap;

    public Execution(double[] data){
        hMap = new HashMap<Integer, Histogram>();
        calculation(data);
    }

    private void calculation(double[] data){
        for(int i=0;i<data.length;i++){
            if(hMap.get(validationClass(data[i]))==null){
                hMap.put(validationClass(data[i]),new Histogram(data[i]));
            }else{
                hMap.get(validationClass(data[i])).add(data[i]);
            }
        }
    }

    private int validationClass(double height){
        if(height<165){
            return 1;
        }else if(165<=height&&height<170){
            return 2;
        }else if(170<=height&&height<175){
            return 3;
        }else if(175<=height&&height<180){
            return 4;
        }else if(180<=height&&height<185){
            return 5;
        }else if(185<=height){
            return 6;
        }else{
            return -1;
        }
    }

    public void print(){
        for(int i=1;i<=6;i++){
            if(hMap.get(i)==null){
                System.out.println(i+":");
            }else{
                System.out.println(i+":"+hMap.get(i).toString());
            }
        }
    }

}

class Histogram{

    ArrayList<Double> aList;

    public Histogram(double height){
        aList = new ArrayList<Double>();
        aList.add(height);
    }

    public void add(double height){
        aList.add(height);
    }

    public int size(){
        return aList.size();
    }

    public String toString(){
        String str = "";
        for(int i=0;i<aList.size();i++){
            str=str+"*";
        }
        return str;
    }

}