import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/01/30
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            System.out.println(distance(new Permutation(sc.nextLine()).getList()));
        }
    }

    private static int distance(ArrayList<Integer> aList){
        Collections.sort(aList);
        return aList.get(aList.size()-1)-aList.get(0);
    }
}

class Permutation {

    //文字列の順列組み合わせ!

    private ArrayList<String> pattern;

    public Permutation(String str){
        pattern = calculation(str);
    }

    public ArrayList<Integer> getList(){
        return convert(pattern);
    }

    private ArrayList<Integer> convert(ArrayList<String> aList){
        ArrayList<Integer> tmp = new ArrayList<Integer>();
        for(int i=0;i<aList.size();i++){
            tmp.add(Integer.parseInt(aList.get(i)));
        }
        return tmp;
    }

    public int size(){
        return pattern.size();
    }

    public void print(){
        for(int i=0;i<pattern.size();i++){
            System.out.println(pattern.get(i));
        }
    }

    private ArrayList<String> calculation(String str){
        if(str==null){
            return null;
        }
        ArrayList<String> permutations = new ArrayList<String>();
        if(str.length()==0){
            permutations.add("");
            return permutations;
        }
        char first = str.charAt(0);
        String reminder = str.substring(1);
        ArrayList<String> words = calculation(reminder);
        for(String word : words){
            for(int j=0;j<=word.length();j++){
                String s = insertCharAt(word,first,j);
                permutations.add(s);
            }
        }
        return permutations;
    }

    private String insertCharAt(String word,char c,int i){
        String start = word.substring(0,i);
        String end = word.substring(i);
        return start + c + end;
    }

}