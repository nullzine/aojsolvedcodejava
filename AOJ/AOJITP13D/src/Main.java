import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            System.out.println(new DivsorAnalyser(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2])).getDivisorArticle());
        }
    }
}

class DivsorAnalyser{

    private int start;
    private int end;
    private int target;
    private ArrayList<Integer> divisor;

    public DivsorAnalyser(int start,int end,int target){
        this.start=start;
        this.end=end;
        this.target=target;
        divisor = new ArrayList<Integer>();
        analysis();
    }

    private void analysis(){
        for(int i=1;i<=target;i++){
            if(target%i==0){
                divisor.add(i);
            }
        }
    }

    public int getDivisorArticle(){
        int count=0;
        for(int i=start;i<=end;i++){
            if(divisor.contains(i)){
                count++;
            }
        }
        return count;
    }


}