import java.util.*;

/**
 * Created by Develop on 14/01/07.
 */
public class Main {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        CardPair cp = new CardPair();
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            cp.removeCard(sc.nextLine());
        }
        cp.showCard();
    }

}

class CardPair{

    LinkedHashSet<String> hSet;

    public CardPair(){
        hSet = new LinkedHashSet<String>();
        setDefault();
    }

    private void setDefault(){
        for(int i=1;i<=13;i++){
            hSet.add("S "+Integer.toString(i));
        }
        for(int i=1;i<=13;i++){
            hSet.add("H "+Integer.toString(i));
        }
        for(int i=1;i<=13;i++){
            hSet.add("C "+Integer.toString(i));
        }
        for(int i=1;i<=13;i++){
            hSet.add("D "+Integer.toString(i));
        }
    }

    public void removeCard(String str){
        hSet.remove(str);
    }

    public void showCard(){
        for (String s : hSet) System.out.println(s);
    }

}

/*
class CardPair{

    LinkedHashSet<Card> hSet;

    public CardPair(){
        hSet = new LinkedHashSet<Card>();
        setDefault();
    }

    private void setDefault(){
        for(int i=1;i<=13;i++){
            hSet.add(new Card("S",i));
        }
        for(int i=1;i<=13;i++){
            hSet.add(new Card("H",i));
        }
        for(int i=1;i<=13;i++){
            hSet.add(new Card("C",i));
        }
        for(int i=1;i<=13;i++){
            hSet.add(new Card("D",i));
        }
    }

    public void removeCard(Card card){
        hSet.remove(card);
    }

    public void showCard(){
        for (Card s : hSet) {
            System.out.println(s.toString());
        }
    }

}
*/

/*
class Card{

    private String type;
    private int cardclass;

    public Card(String type,int cardclass){
        this.type=type;
        this.cardclass=cardclass;
    }

    public String getType(){
        return type;
    }

    public int getCardClass(){
        return cardclass;
    }

    public String toString(){
        return type+" "+cardclass;
    }

}
*/