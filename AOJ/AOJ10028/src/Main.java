import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/23
 * Time: 9:56
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] dataSet = new int[n];
        for(int i=0;i<n;i++){
            dataSet[i]=sc.nextInt();
        }
        Arrays.sort(dataSet);
        System.out.print(dataSet[0]);
        for(int i=1;i<dataSet.length;i++){
            System.out.print(" "+dataSet[i]);
        }
        System.out.println("");
    }
}