import java.util.*;

public class Main{

    public static void main(String[] srgs){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String str1 = sc.nextLine();
            String str2 = sc.nextLine();
            Execution exe = new Execution();
            System.out.println(exe.calculation(str1,str2));
        }
    }
}

class Execution{

    public String calculation(String str1,String str2){
        String[] strs1 = str1.split(" ");
        String[] strs2 = str2.split(" ");
        int[] data1 = new int[strs1.length];
        int[] data2 = new int[strs2.length];
        for(int i=0;i<strs1.length;i++){
            data1[i] = Integer.parseInt(strs1[i]);
            data2[i] = Integer.parseInt(strs2[i]);
        }
        return Integer.toString(hit(data1,data2))+" "+Integer.toString(blow(data1,data2));
    }

    private int hit(int[] data1,int[] data2){
        int count=0;
        for(int i=0;i<data1.length;i++){
            if(data1[i]==data2[i]){
                count++;
            }
        }
        return count;
    }

    private int blow(int[] data1,int[] data2){
        int count=0;
        for(int i=0;i<data1.length;i++){
            for(int j=0;j<data2.length;j++){
                if(data1[i]==data2[j]&&i!=j){
                    count++;
                }
            }
        }
        return count;
    }

}