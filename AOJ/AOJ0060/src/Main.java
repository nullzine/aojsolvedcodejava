import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/01.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(calculation(convArr(sc.nextLine().split(" ")))?"YES":"NO");
        }
    }
    private static boolean calculation(int[] a){
        int count=0;
        for(int i=1;i<=10;i++){
            if((!(i==a[0]||i==a[1]||i==a[2]))&&(i+a[0]+a[1]<=20)){
                count++;
            }
        }
        return 4<=count;
    }
    private static int[] convArr(String[] strs){
        int[] tmp = new int[strs.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=Integer.parseInt(strs[i]);
        }
        return tmp;
    }
}
