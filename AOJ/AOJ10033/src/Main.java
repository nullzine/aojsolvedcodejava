import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/23
 * Time: 10:34
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        DataStore[] ds = new DataStore[n];
        for(int i=0;i<ds.length;i++){
            ds[i] = new DataStore("Store"+Integer.toString(i));
        }
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("quit")){
                break;
            }
            if(str[0].equals("push")){
                ds[Integer.parseInt(str[1])-1].push(str[2]);
            }else if(str[0].equals("pop")){
                System.out.println(ds[Integer.parseInt(str[1])-1].pop());
            }else if(str[0].equals("move")){
                ds[Integer.parseInt(str[2])-1].push(ds[Integer.parseInt(str[1])-1].pop());
            }
        }
    }
}

class DataStore{

    private String name;
    private Stack<String> dataSet;

    public DataStore(String name){
        this.name=name;
        dataSet = new Stack<String>();
    }

    public String pop(){
        return dataSet.pop();
    }

    public void push(String str){
        dataSet.push(str);
    }

}