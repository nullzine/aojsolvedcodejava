import java.util.Scanner;

/**
 * Created by labuser on 14/04/11.
 */
public class Main {
    public static void main(String[] args){
        new EulerPhiFunction(100000).print();
        Scanner sc = new Scanner(System.in);
        /*
        while(sc.hasNext()){
            System.out.println(new EulerPhiFunction().phi(Integer.parseInt(sc.nextLine())));
        }
        */
    }
}

class EulerPhiFunction{

    private int[] data;

    public EulerPhiFunction(int limit){
        data = new int[limit];
        calculation();
    }

    public EulerPhiFunction(){

    }

    private void calculation(){
        for(int i=0;i<data.length;i++){
            data[i] = phi(i+1);
        }
    }

    public int phi(int n){
        int res, p;
        res = 1;
        if(n%2 == 0){
            n = n/2;
            while(n%2 == 0){
                n = n/2;
                res = res*2;
            }
        }
        p = 3;
        while (p <= n/p){
            if(n%p == 0){
                n = n/p;
                res = res*(p-1);
                while(n%p==0) {
                    n = n/p;
                    res = res*p;
                }
            }
            p += 2;
        }
        if (n > 1){
            res = res*(n-1);
        }
        return res;
    }

    public void print(){
        for(int i=0;i<data.length;i++){
            System.out.println("phi("+(i+1)+")="+data[i]);
        }
    }

}