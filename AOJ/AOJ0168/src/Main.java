import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/14.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = Integer.parseInt(sc.nextLine());
            if (n == 0) {
                break;
            } else {
                System.out.println(new Execution2(n));
            }
        }
    }
}

class Execution {

    private int pattern;

    public Execution(int n) {
        pattern = calculation(n);
    }

    private int calculation(int lastStep) {
        int step;
        if (lastStep == 1) {
            step = 1;
        } else if (lastStep==2) {
            step=2;
        } else if (lastStep==3) {
            step=4;
        } else {
            step = calculation(lastStep - 3) + calculation(lastStep - 2) + calculation(lastStep - 1);
        }
        return step;
    }

    public String toString() {
        return Integer.toString(pattern/3650+1);
        //return Integer.toString(pattern);
    }


}

class Execution2 {

    private int[] dp;
    private int pattern;

    public Execution2(int n) {
        dp = new int[31];
        pattern = calculation(n);
    }

    private int calculation(int step) {
        dp[1]=1;
        dp[2]=2;
        dp[3]=4;
        for(int i=4;i<=30;i++){
            dp[i]=dp[i-1]+dp[i-2]+dp[i-3];
        }
        return dp[step];
    }

    public String toString() {
        return Integer.toString(pattern/3650+1);
        //return Integer.toString(pattern);
    }


}