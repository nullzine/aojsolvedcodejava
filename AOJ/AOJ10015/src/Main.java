import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/20
 * Time: 7:54
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution();
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            String[] str = sc.nextLine().split(" ");
            exe.setData(str[0],Integer.parseInt(str[1]));
        }
        exe.showData();
    }
}

class Execution{

    private cardData[] dataSet;
    private int varietyLimit;

    public Execution(){
        dataSet = new cardData[52];
        dataSetInit();
        varietyLimit = 13;
    }

    private void dataSetInit(){
        for(int i=0;i<dataSet.length;i++){
            dataSet[i] = new cardData(genVariety(i+1),i%13+1);
        }
    }

    private String genVariety(int num){
        if(0<num&&num<=13){
            return "S";
        }else if(13<num&&num<=26){
            return "H";
        }else if(26<num&&num<=39){
            return "C";
        }else if(39<num&&num<=52){
            return "D";
        }else{
            return "!";
        }
    }

    public void setData(String variety,int rank){
        for(int i=0;i<dataSet.length;i++){
            if(dataSet[i]!=null){
                if(dataSet[i].getVariety().equals(variety)&&dataSet[i].getRank()==rank){
                    dataSet[i]=null;
                }
            }
        }
    }

    public void showData(){
        for(int i=0;i<dataSet.length;i++){
            if(dataSet[i]!=null){
                System.out.println(dataSet[i].getVariety()+" "+dataSet[i].getRank());
            }
        }
    }

}

class cardData{

    private String variety;
    private int rank;

    public cardData(String variety,int rank){
        this.variety=variety;
        this.rank=rank;
    }

    public String getVariety(){
        return variety;
    }

    public int getRank(){
        return rank;
    }

}