import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/19
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            int h = Integer.parseInt(str[0]);
            int w = Integer.parseInt(str[1]);
            if(h==0&&w==0){
                break;
            }
            Chess chess = new Chess(h,w);
            chess.directPrintBoard(h,w);
            System.out.println("");
        }
    }
}

class Chess{

    private String board;
    private String crlf;

    public Chess(int H,int W){
        crlf=getCRLF();
        board = "";
        //makeBoard(H,W);
    }

    private String getCRLF() {
        String str = "\n";
        try {
            str = System.getProperty("line.separator");
        } catch (SecurityException e) {
        }
        return str;
    }

    private void makeBoard(int H,int W){
        for(int i=0;i<H;i++){
            for(int j=0;j<W;j++){
                if(i%2==0&&j%2==0){
                    board=board+"#";
                }else if(i%2==0&&j%2!=0){
                    board=board+".";
                }else if(i%2!=0&&j%2==0){
                    board=board+".";
                }else if(i%2!=0&&j%2!=0){
                    board=board+"#";
                }
            }
            board=board+crlf;
        }
    }

    public void printBoard(){
        System.out.print(board);
    }

    public void directPrintBoard(int H,int W){
        for(int i=0;i<H;i++){
            for(int j=0;j<W;j++){
                if(i%2==0&&j%2==0){
                    System.out.print("#");
                }else if(i%2==0&&j%2!=0){
                    System.out.print(".");
                }else if(i%2!=0&&j%2==0){
                    System.out.print(".");
                }else if(i%2!=0&&j%2!=0){
                    System.out.print("#");
                }
            }
            System.out.println("");
        }
    }

}