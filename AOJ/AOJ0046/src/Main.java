import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<Double> aList = new ArrayList<Double>();
        while(sc.hasNext()){
            double tmp = sc.nextDouble();
            aList.add(tmp);
        }
        Collections.sort(aList);
        System.out.println(aList.get(aList.size()-1)-aList.get(0));
    }
}