import java.util.Scanner;

/**
 * Created by labuser on 14/04/15.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine()));
        }
    }
}

class Execution{

    private double[] data;
    private boolean result;

    public Execution(String str){
        String[] strs = str.split(",");
        data = new double[strs.length];
        for(int i=0;i<data.length;i++){
            data[i] = Double.parseDouble(strs[i]);
        }
        double xa = data[0];
        double ya = data[1];
        double xb = data[2];
        double yb = data[3];
        double xc = data[4];
        double yc = data[5];
        double xd = data[6];
        double yd = data[7];
        result = calculation(xa,ya,xc,yc,xb,yb,xd,yd)||calculation(xb,yb,xd,yd,xa,ya,xc,yc);
    }

    private boolean calculation(double X1,double Y1,double X2,double Y2,double X3,double Y3,double X4,double Y4){
        return ((X1-X2)*(Y3-Y1)-(Y1-Y2)*(X3-X1))*((X1-X2)*(Y4-Y1)-(Y1-Y2)*(X4-X1))>0.0;
    }

    public String toString(){
        if(result){
            return "NO";
        }else{
            return "YES";
        }
    }

}