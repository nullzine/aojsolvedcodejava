import java.util.Scanner;

/**
 * Created by nullzine on 2014/12/16.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int m = Integer.parseInt(sc.nextLine());
        Road[] roads = new Road[m];
        for(int i=0;i<roads.length;i++){
            String[] strs = sc.nextLine().split(" ");
            roads[i] = new Road(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]),Integer.parseInt(strs[3]));
        }
        String[] strs = sc.nextLine().split(" ");
        ProbData pd = new ProbData(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]),Integer.parseInt(strs[3]));
        for(Road r:roads){
            System.out.println(r);
        }
        System.out.println(pd);
    }
}

class Execution{

    private int[][] costData;
    private Road[] roads;
    private ProbData pd;

    public Execution(int n,Road[] roads,ProbData pd){
        costData = new int[n][n];
        this.roads=roads;
        this.pd=pd;
        floyd();
    }

    private void floyd(){
        for(int i=0;i<costData.length;i++){
            for(int j=0;j<costData.length;j++){

            }
        }
    }

}

class ProbData{

    private int startCity;
    private int goalCity;
    private int money;
    private int targetMoney;

    public ProbData(int startCity,int goalCity,int money,int targetMoney){
        this.startCity=startCity;
        this.goalCity=goalCity;
        this.money=money;
        this.targetMoney=targetMoney;
    }

    public String toString(){
        return startCity+" to "+goalCity+" Money:"+money+" targetMoney"+targetMoney;
    }

}

class Road{

    private int from;
    private int to;
    private int wayMoney;
    private int backMoney;

    public Road(int from,int to,int wayMoney,int backMoney){
        this.from=from;
        this.to=to;
        this.wayMoney=wayMoney;
        this.backMoney=backMoney;
    }

    public int getFrom(){
        return from;
    }

    public int getTo(){
        return to;
    }

    public int getWayMoney(){
        return wayMoney;
    }

    public int getBackMoney(){
        return backMoney;
    }

    public String toString(){
        return "From:"+from+" To:"+to+" wayMoney:"+wayMoney+" backMoney"+backMoney;
    }

}