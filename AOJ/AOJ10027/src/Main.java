import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/22
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */


public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Person one = new Person("Taro");
        Person two = new Person("Hanako");
        int n = 0;
        String[] card = new String[2];
        int valid = 0;

        n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            card[0] = sc.next();
            card[1] = sc.next();

            valid = card[0].compareTo(card[1]);
            if (valid > 0) {
                one.add(3);
            } else if (valid == 0) {
                one.add(1);
                two.add(1);
            } else {
                two.add(3);
            }
        }
        System.out.println(one.getScore()+" "+two.getScore());
    }
}

/*

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            String[] str = new String[n];
            for(int i=0;i<str.length;i++){
                str[i]=sc.nextLine();
            }
            Execution exe = new Execution(str);
            System.out.println(exe.getData()[0]+" "+exe.getData()[1]);
        }
    }
}

class Execution{

    private Person[] data;

    public Execution(String[] str){
        data = new Person[2];
        data[0] = new Person("Taro");
        data[1] = new Person("Hanako");
        analysis(str);
    }

    private void analysis(String[] str){
        for(int i=0;i<str.length;i++){
            System.out.println(str.length);
            if(0<str[i].split(" ")[0].compareTo(str[i].split(" ")[1])){
                    data[0].add(3);
                    break;
            }else if(0==str[i].split(" ")[0].compareTo(str[i].split(" ")[1])){
                    data[0].add(1);
                    data[1].add(1);
                    break;
            }else if(str[i].split(" ")[0].compareTo(str[i].split(" ")[1])<0){
                    data[1].add(3);
                    break;
            }else{
                System.out.println("ERROR");
            }
        }
    }

    public void print(){
        System.out.println(data[0].getScore()+" "+data[1].getScore());
    }

    public int[] getData(){
        int[] tmp = new int[2];
        tmp[0] = data[0].getScore();
        tmp[1] = data[1].getScore();
        return tmp;
    }

}

*/


class Person{

    private String name;
    private int count;

    public Person(String name){
        this.name=name;
        count=0;
    }

    public void add(int point){
        count = count + point;
    }

    public int getScore(){
        return count;
    }

}