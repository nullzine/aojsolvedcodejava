import java.util.*;

/**
 * Created by nullzine on 2014/06/12.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution();
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            exe.add(new Person(Double.parseDouble(strs[0]),Double.parseDouble(strs[1])));
        }
        System.out.print(exe);
    }
}

class Execution{

    private int[][] counter;

    public Execution(){
        counter = new int[2][4];
        Arrays.fill(counter[0],0);
        Arrays.fill(counter[1],0);
    }

    public void add(Person person){
        counter[0][person.getLeftEyeClass()]++;
        counter[1][person.getRightEyeClass()]++;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<4;i++){
            sb.append(counter[0][i]+" "+counter[1][i]+"\n");
        }
        return sb.toString();
    }

}

class Person{

    private double left;
    private double right;

    public Person(double left,double right){
        this.left=left;
        this.right=right;
    }

    public int getLeftEyeClass(){
        return convStr(validEye(left));
    }

    public int getRightEyeClass(){
        return convStr(validEye(right));
    }

    private String validEye(double eyeValue){
        if(1.1<=eyeValue){
            return "A";
        }else if(0.6<=eyeValue&&eyeValue<1.1){
            return "B";
        }else if(0.2<=eyeValue&&eyeValue<0.6){
            return "C";
        }else{
            return "D";
        }
    }

    private int convStr(String str){
        return str.toCharArray()[0]-65;
    }

}