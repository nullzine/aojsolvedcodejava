import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/17.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(calculation(sc.nextLine().split(",")));
        }
    }

    private static String calculation(String[] strs){
        if(Double.parseDouble(strs[0])==Double.parseDouble(strs[2])){
            return (2*Double.parseDouble(strs[0]) - Double.parseDouble(strs[4]))+" "+Double.parseDouble(strs[5]);
        }else if(Double.parseDouble(strs[1])==Double.parseDouble(strs[3])){
            return Double.parseDouble(strs[4])+" "+(2*Double.parseDouble(strs[1]) - Double.parseDouble(strs[5]));
        }else{
            return (2*Double.parseDouble(strs[5])*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))-2*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*(Double.parseDouble(strs[1])-((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*Double.parseDouble(strs[0]))+Double.parseDouble(strs[4])-((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*Double.parseDouble(strs[4]))/(((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))+1)+" "+((Double.parseDouble(strs[4])-((2*Double.parseDouble(strs[5])*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))-2*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*(Double.parseDouble(strs[1])-((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*Double.parseDouble(strs[0]))+Double.parseDouble(strs[4])-((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*Double.parseDouble(strs[4]))/(((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))*((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))+1)))/((Double.parseDouble(strs[3])-Double.parseDouble(strs[1]))/(Double.parseDouble(strs[2])-Double.parseDouble(strs[0])))+Double.parseDouble(strs[5]));
        }
    }

}