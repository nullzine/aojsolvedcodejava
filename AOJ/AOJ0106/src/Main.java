import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                System.out.println(new Execution(n).getMin().getPrice());
            }
        }
    }
}

class Execution{

    private int n;
    private ArrayList<Pattern> result;

    public Execution(int n){
        this.n=n;
        result = new ArrayList<Pattern>();
        calculation();
    }

    private void calculation(){
        for(int i=0;i<=25;i++){
            for(int j=0;j<=17;j++){
                if(5000<200*i+300*j){
                    break;
                }
                for(int k=0;k<=10;k++) {
                    if(200*i+300*j+500*k==n){
                        result.add(new Pattern(i,j,k));
                    }else if(5000<200*i+300*j+500*k) {
                        break;
                    }
                }
            }
        }
    }

    public Pattern getMin(){
        Pattern min = new Pattern(26,18,11);
        for(Pattern p :result){
            if(p.getPrice()<min.getPrice()){
                min=p;
            }
        }
        return min;
    }

    class Pattern{

        private int a;
        private int b;
        private int c;

        public Pattern(int a,int b,int c){
            this.a=a;
            this.b=b;
            this.c=c;
        }

        public int getPrice(){
            return (int)(380*0.8*(a-a%5)+380*(a%5)+550*0.85*(b-b%4)+550*(b%4)+850*0.88*(c-c%3)+850*(c%3));
        }

    }

}