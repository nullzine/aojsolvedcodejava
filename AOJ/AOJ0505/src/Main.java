import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 17:35
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int m = Integer.parseInt(strs[1]);
            if(n==0&&m==0){
                break;
            }
            Place[] place = new Place[m];
            for(int i=0;i<place.length;i++){
                place[i] = new Place(i+1);
            }
            for(int i=0;i<n;i++){
                String[] dataSet = sc.nextLine().split(" ");
                for(int j=0;j<m;j++){
                    if(dataSet[j].equals("1")){
                        place[j].vote();
                    }
                }
            }
            Arrays.sort(place,new PlaceCountComp());
            System.out.print(place[0].getId());
            for(int i=1;i<place.length;i++){
                System.out.print(" "+place[i].getId());
            }
            System.out.println();
        }
    }
}

class Place{

    private int id;
    private int count;

    public Place(int id){
        this.id=id;
        count=0;
    }

    public void vote(){
        count++;
    }

    public int getId(){
        return id;
    }

    public int getCount(){
        return count;
    }

}

class PlaceCountComp implements Comparator<Place> {
    public int compare(Place o1, Place o2) {
        if (o1.getCount() == o2.getCount()) {
            if(o1.getId() > o2.getId()){
                return 1;
            }else{
                return -1;
            }
        } else if (o1.getCount() > o2.getCount()) {
            return -1;
        } else {
            return 1;
        }
    }
}