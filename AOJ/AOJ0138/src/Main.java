import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 12:12
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String[] data = new String[24];
        for(int i=0;i<24;i++){
            data[i]=sc.nextLine();
        }
        new Execution(data).print();
    }
}

class Execution{

    private Group[] group;
    private Person[] tmp;

    public Execution(String[] data){
        group = new Group[3];
        for(int i=0;i<3;i++){
            group[i] = new Group();
        }
        tmp = new Person[8];
        for(int i=0;i<24;i++){
            group[i/8].add(data[i]);
        }
        calculation();
    }

    private void calculation(){
        tmp[0]=group[0].getSecondPerson()[0];
        tmp[1]=group[0].getSecondPerson()[1];
        tmp[2]=group[1].getSecondPerson()[0];
        tmp[3]=group[1].getSecondPerson()[1];
        tmp[4]=group[2].getSecondPerson()[0];
        tmp[5]=group[2].getSecondPerson()[1];
        tmp[6]=processingOtherPerson()[0];
        tmp[7]=processingOtherPerson()[1];
    }

    private Person[] processingOtherPerson(){
        ArrayList<Person> aList = new ArrayList<Person>();
        for(int i=0;i<group.length;i++){
            for(int j=0;j<group[i].getOtherPerson().length;j++){
                aList.add(group[i].getOtherPerson()[j]);
            }
        }
        Collections.sort(aList,new PersonTimeComp());
        Person[] p = new Person[2];
        p[0] = aList.get(0);
        p[1] = aList.get(1);
        return p;
    }

    public void print(){
        for(int i=0;i<tmp.length;i++){
            System.out.println(tmp[i].toString());
        }
    }

}

class Group{

    ArrayList<Person> aList;

    public Group(){
        aList = new ArrayList<Person>();
    }

    public void add(String str){
        String[] strs =str.split(" ");
        aList.add(new Person(Integer.parseInt(strs[0]),Double.parseDouble(strs[1])));
    }

    public Person[] getSecondPerson(){
        Collections.sort(aList,new PersonTimeComp());
        Person[] tmp = new Person[2];
        tmp[0]=aList.get(0);
        tmp[1]=aList.get(1);
        return tmp;
    }

    public Person[] getOtherPerson(){
        Person[] tmp = new Person[aList.size()-2];
        for(int i=2;i<aList.size();i++){
            tmp[i-2]=aList.get(i);
        }
        return tmp;
    }


}

class PersonTimeComp implements Comparator<Person> {
    public int compare(Person o1, Person o2) {
        if (o1.getTime() == o2.getTime()) {
            return 0;
        } else if (o1.getTime() > o2.getTime()) {
            return 1;
        } else {
            return -1;
        }
    }
}

class Person{

    private int id;
    private double time;

    public Person(int id,double time){
        this.id=id;
        this.time=time;
    }

    public double getTime(){
        return time;
    }

    public String toString(){
        return Integer.toString(id)+" "+String.format("%.2f",time);
    }

}