import java.util.Scanner;

/**
 * Created by Develop on 14/02/17.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String[] nq = sc.nextLine().split(" ");
        int n = Integer.parseInt(nq[0]);
        int q = Integer.parseInt(nq[1]);
        DisjointSet ds = new DisjointSet(n);
        for(int i=0;i<q;i++){
            String[] strs = sc.nextLine().split(" ");
            if(strs[0].equals("0")){
                ds.unite(Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
            }else if(strs[0].equals("1")){
                if(ds.same(Integer.parseInt(strs[1]),Integer.parseInt(strs[2]))){
                    System.out.println("1");
                }else{
                    System.out.println("0");
                }
            }else{
                System.out.println("ERROR");
            }
        }
    }
}

class DisjointSet{

    private int[][] dataMap;

    public DisjointSet(int num){
        dataMap = new int[num][2];
        for(int i=0;i<num;i++){
            dataMap[i][0]=i;
            dataMap[i][1]=i;
        }
    }

    public void unite(int x,int y){
        int xGroup = dataMap[x][1];
        int yGroup = dataMap[y][1];
        for(int i=0;i<dataMap.length;i++){
            if(dataMap[i][1]==yGroup){
                dataMap[i][1]=xGroup;
            }
        }
    }

    public boolean same(int x,int y){
        int xGroup = dataMap[x][1];
        return dataMap[y][1]==xGroup;
    }

}