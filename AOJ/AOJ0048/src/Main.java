import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(validClass(sc.nextDouble()));
        }
    }

    private static String validClass(double weight){
        if(weight<=48){
            return "light fly";
        }else if(48<weight&&weight<=51){
            return "fly";
        }else if(51<weight&&weight<=54){
            return "bantam";
        }else if(54<weight&&weight<=57){
            return "feather";
        }else if(57<weight&&weight<=60){
            return "light";
        }else if(60<weight&&weight<=64){
            return "light welter";
        }else if(64<weight&&weight<=69){
            return "welter";
        }else if(69<weight&&weight<=75){
            return "light middle";
        }else if(75<weight&&weight<=81){
            return "middle";
        }else if(81<weight&&weight<=91){
            return "light heavy";
        }else{
            return "heavy";
        }
    }
}