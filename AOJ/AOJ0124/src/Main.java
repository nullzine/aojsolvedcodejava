import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/23.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else {
                if(flag){
                    flag=false;
                }else{
                    System.out.println();
                }
                Team[] teams = new Team[n];
                for (int i = 0; i < n; i++) {
                    String[] strs = sc.nextLine().split(" ");
                    teams[i] = new Team(strs[0], Integer.parseInt(strs[1]), Integer.parseInt(strs[2]), Integer.parseInt(strs[3]), i + 1);
                }
                Arrays.sort(teams, new Comparator<Team>() {
                    @Override
                    public int compare(Team o1, Team o2) {
                        int tmp = o2.getPoint() - o1.getPoint();
                        if (tmp == 0) {
                            return o1.getPosition() - o2.getPosition();
                        } else {
                            return tmp;
                        }
                    }
                });
                for (int i = 0; i < teams.length; i++) {
                    System.out.println(teams[i].getCountry() + "," + teams[i].getPoint());
                }
            }
        }
    }
}

class Team{

    private String country;
    private int point;
    private int position;

    public Team(String country,int win,int lose,int draw,int position){
        this.country=country;
        point=win*3+lose*0+draw*1;
        this.position=position;
    }

    public int getPoint(){
        return point;
    }

    public int getPosition(){
        return position;
    }

    public String getCountry(){
        return country;
    }

}