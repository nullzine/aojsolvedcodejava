import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/11.
 */
public class Main {
    public static void main(String[] args){
        debug();
        /*
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        String[] strs = sc.nextLine().split(" ");
        if(strs.length!=n){
            System.out.println("ERROR");
        }else{
            System.out.println(new Execution(makeIntArray(strs)).getResult());
        }
        */
    }

    private static void debug(){
        Random rand = new Random();
        int[] data = new int[25];
        for(int i=0;i<data.length;i++){
            data[i] = rand.nextInt(30)+1;
        }
        print(data);
        System.out.println(new Execution(data).getResult());
    }

    private static void print(int[] data){
        System.out.print("LCM{"+data[0]);
        for(int i=1;i<data.length;i++){
            System.out.print(","+data[i]);
        }
        System.out.println("}");
    }

    private static int[] makeIntArray(String[] strs){
        int[] tmp = new int[strs.length];
        for(int i=0;i<strs.length;i++){
            tmp[i] = Integer.parseInt(strs[i]);
        }
        return tmp;
    }
}

class Execution{

    private int[] data;
    private int result;

    public Execution(int[] data){
        this.data=data;
        Arrays.sort(data);
        calculation();
    }

    private void calculation(){
        result = data[0];
        for(int i=1;i<data.length;i++){
            result = getLCM(result,data[i]);
        }

    }

    private int getLCM(int m, int n) {
        return m * n / getGCD(m,n);
    }

    private int getGCD(int m, int n) {
        if(n==0){
            return m;
        }else{
            return getGCD(n, m % n);
        }
    }

    public int getResult(){
        return result;
    }

}