import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            double a = Double.parseDouble(strs[0]);
            double b = Double.parseDouble(strs[1]);
            double c = Double.parseDouble(strs[2]);
            double d = Double.parseDouble(strs[3]);
            if(a==-1&&b==-1&&c==-1&&d==-1){
                break;
            }else{
                System.out.println(calculation(a,b,c,d));
            }
        }
    }
    private static int calculation(double Aido,double Akeido,double Bido,double Bkeido) {
        double rAido = Aido * Math.PI / 180;
        double rAkeido = Akeido * Math.PI / 180;
        double rBido = Bido * Math.PI / 180;
        double rBkeido = Bkeido * Math.PI / 180;
        double value = Math.cos(rAido) * Math.cos(rBido) * Math.cos(rAkeido - rBkeido) + Math.sin(rAido) * Math.sin(rBido);
        double kakudo = Math.acos(value);
        double result = 6378.1 * kakudo; //r=6378km
        if (result - (double) (int) result < 0.5){
            return (int) result;
        }else {
            return (int) (result + 0.9);
        }
    }
}
