//import java.util.ArrayList;
//import java.util.Map;
import java.util.Arrays;
import java.util.Scanner;
//import java.util.TreeMap;

/**
 * Created by nullzine on 2014/08/25.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            long[] data = new long[4000];
            Arrays.fill(data,0);
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(" ");
                data[Integer.parseInt(strs[0])]+=Long.parseLong(strs[1])*Long.parseLong(strs[2]);
            }
            boolean flag=true;
            for(int i=0;i<data.length;i++){
                if(1000000<=data[i]){
                    System.out.println(i);
                    flag=false;
                }
            }
            if(flag){
                System.out.println("NA");
            }
            /*
            Employee[] employees = new Employee[n];
            for(int i=0;i<employees.length;i++){
                String[] strs = sc.nextLine().split(" ");
                employees[i] = new Employee(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
            }
            //new Execution(employees).print();
            System.out.print(new Execution(employees));
            */
        }
    }
}

/*
class Execution{

    private TreeMap<Integer,Employee> marged;

    public Execution(Employee[] employees){
        marged = new TreeMap<Integer, Employee>();
        merge(employees);
    }

    private void merge(Employee[] employees){
        for(Employee e:employees){
            if(marged.containsKey(e.id)){
                marged.get(e.id).additional.add(e);
            }else{
                marged.put(e.id,e);
            }
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Map.Entry <Integer,Employee> e:marged.entrySet()){
            if(1000000<=e.getValue().result()){
                sb.append(e.getValue().id+"\n");
            }
        }
        if(sb.toString().equals("")){
            return "NA\n";
        }else{
            return sb.toString();
        }
    }

    public void print(){
        for(Map.Entry <Integer,Employee> e:marged.entrySet()){
            System.out.println(e.getValue());
        }
    }

}

*/

/*
class Employee{

    public int id;
    public long price;
    public long num;
    public ArrayList<Employee> additional;

    public Employee(int id,int price,int num){
        this.id=id;
        this.price=price;
        this.num=num;
        additional = new ArrayList<Employee>();
    }

    public void merge(Employee employee){
        additional.add(employee);
    }

    public long result(){
        long sum=price*num;
        for(Employee e:additional){
            sum+=e.num*e.price;
        }
        return sum;
    }

    public String toString(){
        return "id:"+id+" result:"+result();
    }

}
*/