import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(!validataionData(init(str))){
                break;
            }else{
                System.out.println(new Calculation(init(str)).getResult());
            }
        }
    }

    private static int[] init(String str){
        String[] tmp = str.split(" ");
        int[] data = new int[tmp.length];
        for(int i=0;i<tmp.length;i++) {
            data[i] = Integer.parseInt(tmp[i]);
        }
        return data;
    }

    private static boolean validataionData(int[] data){
        for(int i=0;i<data.length;i++){
            if(data[i]==-1){
                return false;
            }
        }
        return true;
    }

}

class Calculation{

    private int[] data;
    private int result;

    public Calculation(String str){
        init(str);
        result=processing();
    }

    public Calculation(int[] data){
        this.data=data;
        result=processing();
    }

    private void init(String str){
        String[] tmp = str.split(" ");
        if(tmp.length!=6){
            System.out.println("ERROR");
        }else{
            data = new int[tmp.length];
            for(int i=0;i<tmp.length;i++){
                data[i] = Integer.parseInt(tmp[i]);
            }
        }
    }

    private int processing(){
        int[] sum = new int[2];
        HashMap<Integer,Integer> monthMap = makeMap();
        for(int c=0;c<2;c++){
            for(int i=1;i<data[3*c];i++){
                if(validLeap(i)){
                    sum[c]+=366;
                }
                else{
                    sum[c]+=365;
                }
            }
            for(int i=1;i<data[1+3*c];i++){
                if(validLeap(data[3*c])&&i==2){
                    sum[c]+=monthMap.get(i+20);
                }else{
                    sum[c]+=monthMap.get(i);
                }
            }
            sum[c]+=data[2+3*c];
        }
        return sum[1]-sum[0];
    }

    private boolean validLeap(int year){
        return year%400==0 || (year%4==0 && year%100!=0);
    }

    private HashMap<Integer,Integer> makeMap(){
        HashMap<Integer,Integer> hMap = new HashMap<Integer, Integer>();
        hMap.put(1,31);
        hMap.put(2,28);
        hMap.put(22,29);
        hMap.put(3,31);
        hMap.put(4,30);
        hMap.put(5,31);
        hMap.put(6,30);
        hMap.put(7,31);
        hMap.put(8,31);
        hMap.put(9,30);
        hMap.put(10,31);
        hMap.put(11,30);
        hMap.put(12,31);
        return hMap;
    }

    public int getResult(){
        return result;
    }

}
