import java.util.*;

/**
 *
 * @author NullZine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Mat mt = new Mat(1000000);
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int number = sc.nextInt();
            System.out.println(mt.nList[number]);
        }
    }
}


class Mat {
    private boolean[] primeBool;    //指定された値が素数かどうかの一覧
    public int[] nList;
    int limit;

    Mat(int primelimit){
        primeBool = new boolean[primelimit];
        nList = new int[primelimit];
        makeList(primelimit);
        //primelimitは発見される素数の最大値である。
    }

    private void makeList(int limit){
        int count=0;
        Arrays.fill(primeBool,true);
        primeBool[0]=false;
        nList[0]=0;
        nList[1]=0;
        primeBool[1]=false;
        for (int i=2; i<limit; i++) {
            if (primeBool[i]) {
                count++;
                nList[i]=count;
                for (int j=i+i; j<limit; j+=i)
                    primeBool[j] = false;
            }else{
                nList[i]=count;
            }
        }
    }

}