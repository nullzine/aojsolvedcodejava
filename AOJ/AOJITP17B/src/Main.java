import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            int a = Integer.parseInt(str[0]);
            int b = Integer.parseInt(str[1]);
            if(a==0&&b==0){
                break;
            }
            System.out.println(new Calculation(a,b).getAnswer());
        }
    }
}

class Calculation{

    private int[] dataSet;
    private int comp;
    private HashSet<String> hSet;

    public Calculation(int limit,int comp){
        dataSet = new int[limit];
        this.comp=comp;
        hSet = new HashSet<String>();
        init();
    }

    private void init(){
        for(int i=0;i<dataSet.length;i++){
            dataSet[i]=i+1;
        }
    }

    public int getAnswer(){
        int count=0;
        for(int i=0;i<dataSet.length;i++){
            for(int j=0;j<dataSet.length;j++){
                for(int k=0;k<dataSet.length;k++){
                    if(dataSet[i]+dataSet[j]+dataSet[k]==comp&&validExclusive(i,j,k)&&validData(i,j,k)){
                        //System.out.println(makeFormula(dataSet[i],dataSet[j],dataSet[k]));
                        count++;
                    }
                }
            }
        }
        return count;
    }

    private boolean validExclusive(int i,int j,int k){
        return i!=j&&i!=k&&j!=k;
    }

    private boolean validData(int i,int j,int k){
        if(!hSet.contains(makeFormula(i,j,k))){
            addFormula(i,j,k);
            return true;
        }else{
            return false;
        }
    }

    private void addFormula(int a,int b,int c){
        hSet.add(makeFormula(a,b,c));
        hSet.add(makeFormula(a,c,b));
        hSet.add(makeFormula(b,a,c));
        hSet.add(makeFormula(b,c,a));
        hSet.add(makeFormula(c,a,b));
        hSet.add(makeFormula(c,b,a));
    }

    /*
    a b c
    a c b
    b a c
    b c a
    c a b
    c b a
     */

    private String makeFormula(int a,int b,int c){
        return Integer.toString(a)+"+"+Integer.toString(b)+"+"+Integer.toString(c)+"="+Integer.toString(a+b+c);
    }

}