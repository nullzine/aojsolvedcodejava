import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/27
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("0")&&str[1].equals("0")){
                break;
            }
            int w = Integer.parseInt(str[1]);
            String[] data = new String[Integer.parseInt(str[0])];
            for(int i=0;i<data.length;i++){
                data[i] = sc.nextLine();
            }
            if(check(data,w)){
                //new Execution(data,0,0).print();
                System.out.println(new Execution(data,0,0).getAnswer());
            }else{
                System.out.println("ERROR");
            }
        }
    }
    private static boolean check(String[] str,int w){
        for(int i=0;i<str.length;i++){
            if(str[i].length()!=w){
                return false;
            }
        }
        return true;
    }
}

class Execution {

    private int x;
    private int y;
    private LinkedHashMap<String,String> hMap;
    private char[][] data;

    public Execution(String[] data, int x, int y){
        hMap = new LinkedHashMap<String, String>();
        this.data=makeCharArray(data);
        this.x=x;
        this.y=y;
        hMap.put(Integer.toString(x)+" "+Integer.toString(y),this.data[y][x]+"");
        calculation();
    }

    public Execution(char[][] data, int x, int y){
        hMap = new LinkedHashMap<String, String>();
        this.data=data;
        this.x=x;
        this.y=y;
        hMap.put(Integer.toString(x)+" "+Integer.toString(y),this.data[y][x]+"");
        calculation();
    }

    private char[][] makeCharArray(String[] data){
        char[][] tmp = new char[data.length][];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=data[i].toCharArray();
        }
        return tmp;
    }

    private void calculation(){
        while(doMove()){}
    }

    private boolean doMove(){
        int a=x;
        int b=y;
        moveCursole();
        if((a==x&&b==y)){
            hMap.put(Integer.toString(x)+" "+Integer.toString(y),this.data[y][x]+"");
            return false;
        }else if(hMap.containsKey(Integer.toString(x)+" "+Integer.toString(y))){
            hMap.put("LOOP","LOOP");
            return false;
        }else{
            hMap.put(Integer.toString(x)+" "+Integer.toString(y),this.data[y][x]+"");
            return true;
        }
    }

    private void moveCursole(){
        switch (data[y][x]){
            case '>':
                x++;
                break;
            case '<':
                x--;
                break;
            case '^':
                y--;
                break;
            case 'v':
                y++;
                break;
        }
    }

    public String getAnswer(){
        String str ="";
        for(Map.Entry<String, String> e : hMap.entrySet()){
            str=e.getKey();
        }
        return str;
    }

    public void print(){
        for(Map.Entry<String, String> e : hMap.entrySet()){
            System.out.println(e.getKey()+":"+e.getValue());
        }
    }

}