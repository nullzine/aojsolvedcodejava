import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Develop on 14/02/17.
 */
public class Main {
    public static void main(String[] args){
        Prime prime = new Prime(100000000);
        //System.out.println("ready");
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int count=0;
        for(int i=0;i<n;i++){
            if(prime.getBool()[Integer.parseInt(sc.nextLine())]){
                count++;
            }
        }
        System.out.println(count);
    }
}

class Prime {

    private boolean[] boolList;

    public Prime(int limit){
        limit++;
        boolList = new boolean[limit];
        makeBool(limit);
        //showBool();
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void showBool(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(i+":"+boolList[i]);
        }
    }

    private void makeBool(int limit) {
        Arrays.fill(boolList, true);
        boolList[0]=false;
        boolList[1]=false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}