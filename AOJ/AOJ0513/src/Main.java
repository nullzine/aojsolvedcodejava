import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/13
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        CardDataStracture cds = new CardDataStracture(n);
        int m = Integer.parseInt(sc.nextLine());
        for(int i=0;i<m;i++){
            int k = Integer.parseInt(sc.nextLine());
            if(k==0){
                cds.riffle();
            }else{
                cds.cut(k);
            }
        }
        cds.print();
    }
}

class CardDataStracture{

    private int[] card;
    private int n;

    public CardDataStracture(int n){
        this.n=n;
        card = new int[n*2];
        for(int i=0;i<n*2;i++){
            card[i]=i+1;
        }
    }

    public void cut(int k){
        int[] A = new int[k];
        int[] B = new int[2*n-k];
        int i;
        for(i=0;i<A.length;i++){
            A[i]=card[i];
        }
        for(;i<2*n;i++){
            B[i-k]=card[i];
        }
        int pointer=0;
        for(i=0;i<B.length;i++){
            card[pointer]=B[i];
            pointer++;
        }
        for(i=0;i<A.length;i++){
            card[pointer]=A[i];
            pointer++;
        }
    }

    public void riffle(){
        int[] A = new int[n];
        int[] B = new int[n];
        int i;
        for(i=0;i<n;i++){
            A[i]=card[i];
        }
        for(;i<n*2;i++){
            B[i-n]=card[i];
        }
        int pointer=0;
        for(i=0;i<A.length;i++){
            card[pointer]=A[i];
            pointer++;
            card[pointer]=B[i];
            pointer++;
        }
    }

    public void print(){
        for(int i=0;i<card.length;i++){
            System.out.println(card[i]);
        }
    }

}

/*

2
2
1
0

3
4
2
4
0
0

*/