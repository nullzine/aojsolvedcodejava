import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/21.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }else{
                for(int i=0;i<n;i++) {
                    System.out.println(validStudent(sc.nextLine().split(" ")));
                }
            }
        }
    }

    private static Character validStudent(String[] strs){
        if((strs[0].equals("100")||strs[1].equals("100")||strs[2].equals("100"))||
                90<=(Double.parseDouble(strs[0])+Double.parseDouble(strs[1]))/2||
                80<=(Double.parseDouble(strs[0])+Double.parseDouble(strs[1])+Double.parseDouble(strs[2]))/3){
            return 'A';
        }else if(70<=(Double.parseDouble(strs[0])+Double.parseDouble(strs[1])+Double.parseDouble(strs[2]))/3||
                (50<=(Double.parseDouble(strs[0])+Double.parseDouble(strs[1])+Double.parseDouble(strs[2]))/3)&&(80<=Double.parseDouble(strs[0])||80<=Double.parseDouble(strs[1]))){
            return 'B';
        }else{
            return 'C';
        }
    }

}
