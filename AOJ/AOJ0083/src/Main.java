import java.util.*;
/*
元号  期間
meiji   1868. 9. 8  〜   1912. 7.29
taisho  1912. 7.30  〜   1926.12.24
showa   1926.12.25  〜   1989. 1. 7
heisei  1989. 1. 8  〜
 */

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int y = sc.nextInt();
            int m = sc.nextInt();
            int d = sc.nextInt();
            int diff;
            String word;
            if(y<1868){
                word="pre-meiji";
                diff = -1;
            }else if(y==1868 && m<9){
                word="pre-meiji";
                diff = -1;
            }else if(y==1868 && m==9 && d<8){
                word="pre-meiji";
                diff = -1;
            }else if(y<1912){
                word="meiji";
                diff = y-1867;
            }else if(y==1912 && m<7){
                word="meiji";
                diff = y-1867;
            }else if(y==1912 && m==7 && d<=29){
                word="meiji";
                diff = y-1867;
            }else if(y<1926){
                word="taisho";
                diff = y-1911;
            }else if(y==1926 && m<12){
                word="taisho";
                diff = y-1911;
            }else if(y==1926 && m==12 && d<=24){
                word="taisho";
                diff = y-1911;
            }else if(y<1989){
                word="showa";
                diff = y-1925;
            }else if(y==1989 && m<1){
                word="showa";
                diff = y-1925;
            }else if(y==1989 && m==1 && d<=7){
                word="showa";
                diff = y-1925;
            }else{
                word="heisei";
                diff = y-1988;
            }
            if(diff==-1)
                System.out.println(word);
            else
                System.out.println(word+" "+diff+" "+m+" "+d);
        }
    }
}