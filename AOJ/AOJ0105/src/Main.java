import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        TreeMap<String,Book> tMap = new TreeMap<String,Book>();
        while(sc.hasNext()){
            String[] str = sc.nextLine().split(" ");
            if(tMap.get(str[0])==null){
                tMap.put(str[0],new Book(str[0],Integer.parseInt(str[1])));
            }else{
                tMap.get(str[0]).addPage(Integer.parseInt(str[1]));
            }
        }
        Iterator<String> it = tMap.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key);
            tMap.get(key).showPages();
        }
    }
}

class Book{

    private TreeSet<Integer> tSet;
    private String bookName;

    public Book(String str,int page){
        bookName = str;
        tSet = new TreeSet<Integer>();
        tSet.add(page);
    }

    public void addPage(int page){
        tSet.add(page);
    }

    public void showPages(){
        int count=0;
        Iterator it = tSet.iterator();
        while(it.hasNext()){
            if(count==tSet.size()-1){
                System.out.println(it.next());
            }else{
                System.out.print(it.next()+" ");
            }
            count++;
        }
    }

}