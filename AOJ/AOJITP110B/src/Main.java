import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            new Triangle(sc.nextLine()).print();
        }
    }
}

class Triangle{

    private double a;
    private double b;
    private double c;

    public Triangle(String str){
        String[] strs = str.split(" ");
        a=Double.parseDouble(strs[0]);
        b=Double.parseDouble(strs[1]);
        c=Double.parseDouble(strs[2]);
    }

    public void print(){
        System.out.println(getS());
        System.out.println(getL());
        System.out.println(getH());
    }

    private double getS(){
        return 0.5*a*b*Math.sin(c*Math.PI/180);
    }

    private double getL(){
        return a+b+Math.sqrt(Math.pow(a,2)+Math.pow(b,2)-2*a*b*Math.cos(c*Math.PI/180));
    }

    private double getH(){
        return getS()/a*2;
    }

}