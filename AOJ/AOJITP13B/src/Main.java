import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        for(int i=1;true;i++){
            int tmp = Integer.parseInt(sc.nextLine());
            if(tmp==0){
                break;
            }
            System.out.println("Case "+i+": "+tmp);
        }
    }
}