import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/21.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true) {
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            int[] ice = new int[10];
            Arrays.fill(ice, 0);
            for (int i = 0; i < n; i++) {
                ice[Integer.parseInt(sc.nextLine())]++;
            }
            for (int i = 0; i < ice.length; i++) {
                if (ice[i] == 0) {
                    System.out.println("-");
                } else {
                    for (int j = 0; j < ice[i]; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
        }
    }
}
