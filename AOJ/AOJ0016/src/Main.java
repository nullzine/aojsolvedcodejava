import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/31
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Person person = new Person(0,0,90);
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(",");
            int distance = Integer.parseInt(strs[0]);
            int rotate = Integer.parseInt(strs[1]);
            if(distance==0&&rotate==0){
                break;
            }
            person.doMove(distance,rotate);
        }
        System.out.println((int)person.getX());
        System.out.println((int)person.getY());
    }
}

class Person{

    private double x;
    private double y;
    private double rotate;

    public Person(double x,double y,double rotate){
        this.x=x;
        this.y=y;
        this.rotate=rotate;
    }

    public void doMove(double distance,double rad){
        x += (double)distance * Math.cos((double)rotate/180*Math.PI);
        y += (double)distance * Math.sin((double)rotate/180*Math.PI);
        rotate -= rad;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

}