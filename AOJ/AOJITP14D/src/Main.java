import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            MinMaxSumAnalyser mmsa = new MinMaxSumAnalyser(n,sc.nextLine().split(" "));
            System.out.println(mmsa.getMin()+" "+mmsa.getMax()+" "+mmsa.sum());
        }
    }
}

class MinMaxSumAnalyser{

    private int n;
    private int[] data;

    public MinMaxSumAnalyser(int n,String[] strs){
        this.n=n;
        data = new int[strs.length];
        conversion(strs);
    }

    private void conversion(String[] strs){
        for(int i=0;i<data.length;i++){
            data[i] = Integer.parseInt(strs[i]);
        }
    }

    public int getMin(){
        int min=data[0];
        for(int i=1;i<data.length;i++){
            if(data[i]<min){
                min=data[i];
            }
        }
        return min;
    }

    public int getMax(){
        int max=data[0];
        for(int i=1;i<data.length;i++){
            if(max<data[i]){
                max=data[i];
            }
        }
        return max;
    }

    public long sum(){
        long count=0;
        for(int n:data){
            count+=n;
        }
        return count;
    }

}