import java.util.HashMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/20
 * Time: 18:40
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String str = "";
        while(sc.hasNext()){
            str=str+sc.nextLine();
        }
        Calculation calc = new Calculation(str.toLowerCase());
        calc.print();
    }
}

class Calculation{

    HashMap<String,Word> hMap =new HashMap<String, Word>();

    public Calculation(String str){
        analysis(str.toCharArray());
    }

    private void analysis(char[] strc){
        for(int i=0;i<strc.length;i++){
            if(hMap.get(strc[i]+"")==null){
                hMap.put(strc[i]+"",new Word(strc[i]+""));
            }else{
                hMap.get(strc[i]+"").add();
            }
        }
    }

    public void print(){
        System.out.println("a : "+wrapMap("a"));
        System.out.println("b : "+wrapMap("b"));
        System.out.println("c : "+wrapMap("c"));
        System.out.println("d : "+wrapMap("d"));
        System.out.println("e : "+wrapMap("e"));
        System.out.println("f : "+wrapMap("f"));
        System.out.println("g : "+wrapMap("g"));
        System.out.println("h : "+wrapMap("h"));
        System.out.println("i : "+wrapMap("i"));
        System.out.println("j : "+wrapMap("j"));
        System.out.println("k : "+wrapMap("k"));
        System.out.println("l : "+wrapMap("l"));
        System.out.println("m : "+wrapMap("m"));
        System.out.println("n : "+wrapMap("n"));
        System.out.println("o : "+wrapMap("o"));
        System.out.println("p : "+wrapMap("p"));
        System.out.println("q : "+wrapMap("q"));
        System.out.println("r : "+wrapMap("r"));
        System.out.println("s : "+wrapMap("s"));
        System.out.println("t : "+wrapMap("t"));
        System.out.println("u : "+wrapMap("u"));
        System.out.println("v : "+wrapMap("v"));
        System.out.println("w : "+wrapMap("w"));
        System.out.println("x : "+wrapMap("x"));
        System.out.println("y : "+wrapMap("y"));
        System.out.println("z : "+wrapMap("z"));
    }

    private int wrapMap(String str){
        if(hMap.get(str)==null){
            return 0;
        }else{
            return hMap.get(str).getCount();
        }
    }

}

class Word{

    private String str;
    private int count;

    public Word(String str){
        this.str=str;
        count=1;
    }

    public void add(){
        count++;
    }

    public int getCount(){
        return count;
    }

}