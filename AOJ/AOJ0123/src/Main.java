import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            switch(Judge(Double.parseDouble(strs[0]),Double.parseDouble(strs[1]))){
                case 1:
                    System.out.println("AAA");
                    break;
                case 2:
                    System.out.println("AA");
                    break;
                case 3:
                    System.out.println("A");
                    break;
                case 4:
                    System.out.println("B");
                    break;
                case 5:
                    System.out.println("C");
                    break;
                case 6:
                    System.out.println("D");
                    break;
                case 7:
                    System.out.println("E");
                    break;
                case 8:
                    System.out.println("NA");
                    break;
            }
        }
    }

    private static int Judge(double fivehundred,double thousand) {
        if (fivehundred < 35.5 && thousand < 71)
            return 1;
        else if (fivehundred < 37.5 && thousand < 77)
            return 2;
        else if (fivehundred < 40 && thousand < 83)
            return 3;
        else if (fivehundred < 43 && thousand < 89)
            return 4;
        else if (fivehundred < 50 && thousand < 105)
            return 5;
        else if (fivehundred < 55 && thousand < 116)
            return 6;
        else if (fivehundred < 70 && thousand < 148)
            return 7;
        else
            return 8;
    }

}