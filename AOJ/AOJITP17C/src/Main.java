import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String[] str = sc.nextLine().split(" ");
        int r = Integer.parseInt(str[0]);
        int c = Integer.parseInt(str[1]);
        int[][] dataSet = new int[r+1][c+1];
        for(int i=0;i<r;i++){
            String[] tmp = sc.nextLine().split(" ");
            int sum = 0;
            for(int j=0;j<c;j++){
                dataSet[i][j] = Integer.parseInt(tmp[j]);
                sum+=dataSet[i][j];
            }
            dataSet[i][dataSet[i].length-1]=sum;
        }
        for(int i=0;i<c+1;i++){
            int sum = 0;
            for(int j=0;j<r;j++){
                sum+=dataSet[j][i];
            }
            dataSet[dataSet.length-1][i]=sum;
        }
        for(int i=0;i<dataSet.length;i++){
            System.out.print(dataSet[i][0]);
            for(int j=1;j<dataSet[i].length;j++){
                System.out.print(" "+dataSet[i][j]);
            }
            System.out.println("");
        }
    }
}