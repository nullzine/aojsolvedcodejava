import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String str = "";
        while(sc.hasNext()){
            str = str + sc.nextLine();
        }
        System.out.print(new Calculation(str).toString());
    }
}

class Calculation{

    private char[] strc;
    HashMap<String,Alphabet> hMap;

    public Calculation(String str){
        strc = str.toLowerCase().toCharArray();
        hMap = new HashMap<String, Alphabet>();
        makeSet();
    }

    public String toString(){
        String str = "";
        str=str+wordString("a")+getCRLF();
        str=str+wordString("b")+getCRLF();
        str=str+wordString("c")+getCRLF();
        str=str+wordString("d")+getCRLF();
        str=str+wordString("e")+getCRLF();
        str=str+wordString("f")+getCRLF();
        str=str+wordString("g")+getCRLF();
        str=str+wordString("h")+getCRLF();
        str=str+wordString("i")+getCRLF();
        str=str+wordString("j")+getCRLF();
        str=str+wordString("k")+getCRLF();
        str=str+wordString("l")+getCRLF();
        str=str+wordString("m")+getCRLF();
        str=str+wordString("n")+getCRLF();
        str=str+wordString("o")+getCRLF();
        str=str+wordString("p")+getCRLF();
        str=str+wordString("q")+getCRLF();
        str=str+wordString("r")+getCRLF();
        str=str+wordString("s")+getCRLF();
        str=str+wordString("t")+getCRLF();
        str=str+wordString("u")+getCRLF();
        str=str+wordString("v")+getCRLF();
        str=str+wordString("w")+getCRLF();
        str=str+wordString("x")+getCRLF();
        str=str+wordString("y")+getCRLF();
        str=str+wordString("z")+getCRLF();
        return str;
    }

    private String getCRLF(){
        String crlf = "\n";
        try {
            crlf = System.getProperty("line.separator");
        } catch(SecurityException e) {
        }
        return crlf;
    }

    private String wordString(String str){
        if(hMap.get(str)==null){
            return str+" : 0";
        }else{
            return hMap.get(str).toString();
        }
    }

    private void makeSet(){
        for(int i=0;i<strc.length;i++){
            if(hMap.get(strc[i]+"")==null){
                hMap.put(strc[i]+"",new Alphabet(strc[i]+""));
            }else{
                hMap.get(strc[i]+"").add();
            }
        }
    }

}

class Alphabet{

    private String str;
    private int count;

    public Alphabet(String str){
        this.str=str;
        count=1;
    }

    public void add(){
        count++;
    }

    public int getCount(){
        return count;
    }

    public String toString(){
        return str+" : "+Integer.toString(count);
    }

}