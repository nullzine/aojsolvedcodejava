import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int m = Integer.parseInt(strs[1]);
            if(n==0&&m==0){
                break;
            }
            MasterPicture mp = new MasterPicture(n);
            SlavePicture sp = new SlavePicture(m);
            while(mp.hasNext()){
                mp.addPixelLine(sc.nextLine().split(" "));
            }
            while(sp.hasNext()){
                sp.addPixelLine(sc.nextLine().split(" "));
            }
            System.out.println(new Execution(mp,sp));
        }
    }
}

class Execution{

    private MasterPicture masterPicture;
    private SlavePicture slavePicture;
    private ArrayList<Point> points;

    public Execution(MasterPicture masterPicture,SlavePicture slavePicture){
        this.masterPicture=masterPicture;
        this.slavePicture=slavePicture;
        points = new ArrayList<Point>();
        analysis();
    }

    private void analysis(){
        for(int i=0;i<4;i++) {
            for (int j = 0; j < masterPicture.size() - slavePicture.size()+1; j++) {
                for (int k = 0; k < masterPicture.size() - slavePicture.size()+1; k++) {
                    if (coincidence(j, k)) {
                        points.add(new Point(j+slavePicture.nearestLeftX()-slavePicture.upsideSpace(),k+slavePicture.nearestLeftY()-slavePicture.leftsideSpace()));
                        return;
                    }
                }
            }
            slavePicture.rotate();
        }
    }

    private boolean coincidence(int x,int y){
        return masterPicture.compare(slavePicture,x,y);
    }

    private Point nearestLeftPoint(){
        Collections.sort(points,new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                if(o1.x==o2.x){
                    return o1.y-o2.y;
                }else {
                    return o1.x - o2.x;
                }
            }
        });
        return points.get(0);
    }

    public String toString(){
        if(points.size()==0){
            return "NA";
        }else {
            return nearestLeftPoint().toString();
        }
    }

    class Point{

        private int x;
        private int y;

        public Point(int x,int y){
            this.x=x;
            this.y=y;
        }

        public String toString(){
            return (y+1)+" "+(x+1);
        }

    }


}

class MasterPicture{

    private int[][] pixelData;
    private int lineIndex;

    public MasterPicture(int n){
        pixelData = new int[n][n];
        lineIndex=0;
    }

    public void addPixelLine(String[] strs){
        for(int i=0;i<pixelData[lineIndex].length;i++){
            pixelData[lineIndex][i] = Integer.parseInt(strs[i]);
        }
        lineIndex++;
    }

    public boolean hasNext(){
        return lineIndex<pixelData.length;
    }

    public boolean compare(SlavePicture slavePicture,int x,int y){
        for(int i=x;i<x+slavePicture.size()-slavePicture.upsideSpace();i++){
            for(int j=y;j<y+slavePicture.size()-slavePicture.leftsideSpace();j++){
                if(slavePicture.get(i-x,j-y)!=-1&&pixelData[i][j]!=slavePicture.get(i-x,j-y)){
                    return false;
                }
            }
        }
        return true;
    }

    public int size(){
        return pixelData.length;
    }

}

class SlavePicture{

    private int[][] pixelData;
    private int lineIndex;

    public SlavePicture(int m){
        pixelData = new int[m][m];
        lineIndex=0;
    }

    public void addPixelLine(String[] strs){
        for(int i=0;i<pixelData[lineIndex].length;i++){
            pixelData[lineIndex][i]=Integer.parseInt(strs[i]);
        }
        lineIndex++;
    }

    public boolean hasNext(){
        return lineIndex<pixelData.length;
    }

    public void rotate(){
        int[][] tmp = new int[pixelData.length][pixelData.length];
        for(int i=0;i<pixelData.length;i++){
            for(int j=0;j<pixelData.length;j++){
                tmp[j][pixelData.length-1-i]=pixelData[i][j];
            }
        }
        pixelData=tmp;
    }

    public int nearestLeftX(){
        for(int i=0;i<pixelData.length;i++){
            for(int j=0;j<pixelData.length;j++){
                if(pixelData[i][j]!=-1){
                    return i;
                }
            }
        }
        return -1;
    }

    public int nearestLeftY(){
        for(int i=0;i<pixelData.length;i++){
            for(int j=0;j<pixelData.length;j++){
                if(pixelData[i][j]!=-1){
                    return j;
                }
            }
        }
        return -1;
    }

    public int get(int x,int y){
        return pixelData[x+upsideSpace()][y+leftsideSpace()];
    }

    public int size(){
        return pixelData.length;
    }

    public int upsideSpace(){
        int i;
        outside:for(i=0;i<pixelData.length;i++){
            for(int j=0;j<pixelData.length;j++){
                if(pixelData[i][j]!=-1){
                    break outside;
                }
            }
        }
        return i;
    }

    public int leftsideSpace(){
        int i;
        outside:for(i=0;i<pixelData.length;i++){
            for(int j=0;j<pixelData.length;j++){
                if(pixelData[j][i]!=-1){
                    break outside;
                }
            }
        }
        return i;
    }

}

/*

8 4
2 1 3 1 1 5 1 3
2 3 2 4 1 0 2 1
0 3 1 2 1 1 4 2
1 2 3 2 1 1 5 4
0 2 0 1 1 3 2 1
1 3 1 2 2 4 3 2
5 1 2 1 4 1 1 5
4 1 1 0 1 2 2 1
2 -1 -1 -1
0 3 -1 -1
-1 2 2 4
-1 1 -1 1
5 3
1 0 2 3 5
2 3 7 2 1
2 5 4 2 2
8 9 0 3 3
3 6 0 4 7
-1 -1 2
-1 3 5
0 4 -1
4 3
5 6 7 8
1 2 3 4
1 2 3 4
5 6 7 8
-1 -1 -1
5 6 7
-1 -1 -1
0 0

 */