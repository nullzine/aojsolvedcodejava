import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/04.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<Goods> aList = new ArrayList<Goods>();
        int sum=0;
        int count=0;
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(",");
            aList.add(new Goods(Integer.parseInt(strs[0]),Integer.parseInt(strs[1])));
            sum+=aList.get(aList.size()-1).getAnswer();
            count+=aList.get(aList.size()-1).getN();
        }
        System.out.println(sum);
        System.out.println(roundOff((double)count/(double)aList.size()));
    }

        private static int roundOff(double num){
            if(0.5<=num%1){
                return (int)num+1;
            }else{
                return (int)num;
            }
        }

}

class Goods{

    private int price;
    private int n;

    public Goods(int price,int n){
        this.price=price;
        this.n=n;
    }

    public int getAnswer(){
        return price*n;
    }

    public int getN(){
        return n;
    }

}