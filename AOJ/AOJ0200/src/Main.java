import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {

    private static int[][] time;
    private static int[][] cost;

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int n = Integer.parseInt(strs[0]);
            int m = Integer.parseInt(strs[1]);
            if(n==0&&m==0){
                break;
            }
            Route route = new Route(m);
            for(int i=0;i<n;i++) {
                strs = sc.nextLine().split(" ");
                route.add(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]), Integer.parseInt(strs[2]), Integer.parseInt(strs[3]));
            }
            route.doWarshallFloyd();
            int k = Integer.parseInt(sc.nextLine());
            for(int i=0;i<k;i++){
                strs = sc.nextLine().split(" ");
                int p = Integer.parseInt(strs[0]);
                int q = Integer.parseInt(strs[1]);
                int r = Integer.parseInt(strs[2]);
                if(r==0){
                    System.out.println(route.getCost()[p][q]);
                }else{
                    System.out.println(route.getTime()[p][q]);
                }
            }

        }
    }


}

class Route{

    private int[][] cost;
    private int[][] time;
    private int m;

    public Route(int m){
        this.m=m;
        init();
    }

    public void add(int a,int b,int cos,int tim){
        cost[a][b]=cost[b][a]=cos;
        time[a][b]=time[b][a]=tim;
    }

    private void init(){
        time = new int[101][101];
        cost = new int[101][101];
        for(int i=0;i<=100;i++){
            for(int j=0;j<=100;j++){
                time[i][j]=cost[i][j]=10000000;
            }
            time[i][i]=cost[i][i]=0;
        }
    }

    public void doWarshallFloyd(){
        warshall_floyd(true);
        warshall_floyd(false);
    }

    private void warshall_floyd(boolean flag){
        int i,j,k;
        for(k=0;k<=m;k++){
            for(i=0;i<=m;i++){
                for(j=0;j<=m;j++){
                    if(flag) {
                        cost[i][j] = Math.min(cost[i][j], cost[i][k] + cost[k][j]);
                    }else{
                        time[i][j] = Math.min(time[i][j], time[i][k] + time[k][j]);
                    }
                }
            }
        }
    }

    public int[][] getTime(){
        return time;
    }

    public int[][] getCost(){
        return cost;
    }

}