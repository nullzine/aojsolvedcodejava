import java.util.HashMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            HashMap<String,String> hMap = new HashMap<String, String>();
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(" ");
                hMap.put(strs[0],strs[1]);
            }
            int m = Integer.parseInt(sc.nextLine());
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<m;i++){
                String tmp = sc.nextLine();
                if(hMap.get(tmp)==null){
                    sb.append(tmp);
                }else{
                    sb.append(hMap.get(tmp));
                }
            }
            System.out.println(sb.toString());
        }
    }
}