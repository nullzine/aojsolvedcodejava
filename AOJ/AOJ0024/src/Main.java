import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(1000);
        while(sc.hasNext()){
            double speed = sc.nextDouble();
            System.out.println(exe.calc(speed));
        }
    }
}

class Execution{

    int[] height;

    public Execution(int limit){
        height = new int[limit];
        makeBuilding();
    }

    public int calc(double speed){
        double t = speed/9.8;
        double y = 4.9*t*t;
        for(int i=0;i<height.length;i++){
            if(y<height[i]){
                return i;
            }
        }
        return -1;
    }

    private void makeBuilding(){
        for(int i=0;i<height.length;i++){
            height[i]=5*i-5;
        }
    }
}