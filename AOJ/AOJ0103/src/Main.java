import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Develop on 14/02/14.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<String> aList = new ArrayList<String>();
        int n = Integer.parseInt(sc.nextLine());
        while(sc.hasNext()){
            aList.add(sc.nextLine());
        }
        System.out.print(new Execution(aList,n).toString());
    }
}

class Execution{

    private ArrayList<String> dataSet;
    private Inning[] inning;

    public Execution(ArrayList<String> dataSet,int n){
        this.dataSet=dataSet;
        inning = new Inning[n];
        calculaion();
    }

    private void calculaion(){
        int inningPointer=0;
        inning[0] = new Inning();
        for(int i=0;i<dataSet.size();i++){
            //System.out.println(inning[inningPointer].toString());
            if(inning[inningPointer].doCommand(dataSet.get(i))){
                inningPointer++;
                if(inningPointer!=inning.length){
                    inning[inningPointer] = new Inning();
                }
            }
        }
    }

    public String toString(){
        String str = "";
        for(int i=0;i<inning.length;i++){
            str=str+Integer.toString(inning[i].getScore())+getCRLF();
        }
        return str;
    }

    private String getCRLF() {
        String str = "\n";
        try {
            str = System.getProperty("line.separator");
        } catch (SecurityException e) {
        }
        return str;
    }

}

class Inning{

    private Base base;
    private BattingLine bl;
    private int score;
    private int out;

    public Inning(){
        base = new Base();
        bl=new BattingLine();
        score=0;
        out=0;
    }

    public boolean doCommand(String str){
        if(str.equals("HIT")){
            if(!base.doHit(bl.getMember()).equals("NONE")){
                score++;
            }
            return false;
        }
        if(str.equals("HOMERUN")){
            score+=base.doHomeRun(bl.getMember());
            return false;
        }
        if(str.equals("OUT")){
            out++;
            return out==3;
        }else{
            return false;
        }
    }

    public int getScore(){
        return score;
    }

    public String toString(){
        return "SCORE:"+Integer.toString(score)+" OUT:"+Integer.toString(out)+" "+base.toString();
    }

}

class Base{

    String[] runner;

    public Base(){
        runner = new String[3];
        Arrays.fill(runner,"VOID");
    }

    public String doHit(String str){
        String tmp = "NONE";
        if(!runner[2].equals("VOID")){
            tmp=runner[2];
        }
        runner[2]=runner[1];
        runner[1]=runner[0];
        runner[0]=str;
        return tmp;
    }

    public int doHomeRun(String str){
        int score=0;
        if(!str.equals("VOID")){
            score++;
        }
        for(int i=0;i<runner.length;i++){
            if(!runner[i].equals("VOID")){
                score++;
            }
        }
        Arrays.fill(runner,"VOID");
        return score;
    }

    public String toString(){
        return "{"+runner[0]+","+runner[1]+","+runner[2]+"}";
    }

}

class BattingLine{

    private String[] member;
    private int pointer;

    public BattingLine(){
        member = new String[9];
        makeMember();
        pointer=0;
    }

    public String getMember(){
        String tmp = member[pointer];
        pointer++;
        if(pointer==member.length){
            pointer=0;
        }
        return tmp;
    }

    private void makeMember(){
        member[0] = "Ichiro";
        member[1] = "Matsui";
        member[2] = "Matsuzaka";
        member[3] = "Nagashima";
        member[4] = "O";
        member[5] = "Tanaka";
        member[6] = "Kiyohara";
        member[7] = "Nomo";
        member[8] = "Kuwata";
    }

}

/*

2
HIT
OUT
HOMERUN
HIT
HIT
HOMERUN
HIT
OUT
HIT
HIT
HIT
HIT
OUT
HIT
HIT
OUT
HIT
OUT
OUT
END

 */