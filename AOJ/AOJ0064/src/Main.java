import java.util.*;

public class Main{
    public static void main(String[] args){
        ArrayList<String> aList = new ArrayList<String>();
        ArrayList<Integer> dataSet = new ArrayList<Integer>();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            aList.add(sc.nextLine());
        }
        for(int i=0;i<aList.size();i++){
            String[] strs = aList.get(i).split("\\D");
            for(int j=0;j<strs.length;j++){
                if(!strs[j].equals("")){
                    dataSet.add(Integer.parseInt(strs[j]));
                }
            }
        }
        int sum = 0;
        for(int i=0;i<dataSet.size();i++){
            sum = sum + dataSet.get(i);
        }
        System.out.println(sum);
    }
}