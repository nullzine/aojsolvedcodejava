import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/09.
 */
public class Main {
    public static void main(String[] args){
        //new Test();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine()).getHand());
        }
    }
}

class Test{

    public Test(){
        doProcessing();
    }

    private void doProcessing(){
        end:for(int a=1;a<=13;a++){
            for(int b=1;b<=13;b++){
                for(int c=1;c<=13;c++){
                    for(int d=1;d<=13;d++){
                        for(int e=1;e<=13;e++){
                            String dataSet = makeStr(a,b,c,d,e);
                            if(new Execution(dataSet).getHand().equals(new Reference(dataSet).getResult())){
                                System.out.println("OK["+dataSet+"]");
                            }else{
                                System.out.println("BAD["+dataSet+"]");
                                break end;
                            }
                        }
                    }
                }
            }
        }
    }

    private String makeStr(int a,int b,int c,int d,int e){
        return a+","+b+","+c+","+d+","+e;
    }

}

class Execution{

    private int[] cards;

    public Execution(String str){
        cards=convStr(str);
    }

    private int[] convStr(String str){
        String[] strs = str.split(",");
        int[] tmp = new int[strs.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i] = Integer.parseInt(strs[i]);
        }
        return tmp;
    }

    public String getHand(){
        if(validFourCard()){
            return "four card";
        }else if(validFullHouse()){
            return "full house";
        }else if(validStraight()){
            return "straight";
        }else if(validThreeCard()){
            return "three card";
        }else if(validTwoPair()){
            return "two pair";
        }else if(validOnePair()){
            return "one pair";
        }else{
            return "null";
        }
    }

    private boolean validFourCard(){
        int[] counter = new int[13];
        Arrays.fill(counter,0);
        for(int i=0;i<cards.length;i++){
            counter[cards[i]-1]++;
        }
        for(int i=0;i<counter.length;i++){
            if(counter[i]==4){
                return true;
            }
        }
        return false;
    }

    private boolean validFullHouse(){
        return validThreeCard()&&validOnePair();
    }

    private boolean validStraight(){
        return validStraightPattern(false)||validStraightPattern(true);
    }

    private boolean validStraightPattern(boolean flag){
        int[] tmp = new int[cards.length];
        for(int i=0;i<tmp.length;i++){
            if(cards[i]==1&&flag){
                tmp[i]=14;
            }else{
                tmp[i]=cards[i];
            }
        }
        Arrays.sort(tmp);
        int target = tmp[0];
        for(int i=1;i<tmp.length;i++){
            target++;
            if(tmp[i]!=target){
                return false;
            }
        }
        return true;
    }

    /*
    private boolean validUpperStraight(){
        int target = cards[0];
        for(int i=1;i<cards.length;i++){
            target++;
            if(target==14){
                target=1;
            }
            if(cards[i]!=target){
                return false;
            }
        }
        return true;
    }

    private boolean validDownerStraight(){
        int target = cards[0];
        for(int i=1;i<cards.length;i++){
            target--;
            if(target==0){
                target=13;
            }
            if(cards[i]!=target){
                return false;
            }
        }
        return true;
    }
    */

    private boolean validThreeCard(){
        int[] counter = new int[13];
        Arrays.fill(counter,0);
        for(int i=0;i<cards.length;i++){
            counter[cards[i]-1]++;
        }
        for(int i=0;i<counter.length;i++){
            if(counter[i]==3){
                return true;
            }
        }
        return false;
    }

    private boolean validTwoPair(){
        int[] counter = new int[13];
        Arrays.fill(counter,0);
        for(int i=0;i<cards.length;i++){
            counter[cards[i]-1]++;
        }
        int count=0;
        for(int i=0;i<counter.length;i++){
            if(counter[i]==2){
                count++;
            }
        }
        return count==2;
    }

    private boolean validOnePair(){
        int[] counter = new int[13];
        Arrays.fill(counter,0);
        for(int i=0;i<cards.length;i++){
            counter[cards[i]-1]++;
        }
        for(int i=0;i<counter.length;i++){
            if(counter[i]==2){
                return true;
            }
        }
        return false;
    }


}

class Reference{

    private static int[] cards;
    private static int[] hindo;
    private String result;

    public Reference(String str){
        String[] line = str.split(",");
        cards = new int[5];
        hindo = new int[14];
        for(int i=0; i<cards.length; i++){
            cards[i] = Integer.parseInt(line[i]);
            hindo[cards[i]]++;
        }
        result = solve();
    }

    public static String solve(){
        int four = 0;
        int three = 0;
        int two = 0;
        int firstOne = 0;
        for(int i=0; i<hindo.length; i++){
            if(hindo[i]==1 && firstOne==0){firstOne=i;}
            if(hindo[i]==4){four++;}
            if(hindo[i]==3){three++;}
            if(hindo[i]==2){two++;}
        }
        if(four==1){return "four card";}
        if(three==1 && two==1){return "full house";}
        if(three==1){return "three card";}
        if(two==2){return "two pair";}
        if(two==1){return "one pair";}
        if(firstOne>hindo.length-4){return "null";}

        boolean straight1 = true;
        for(int i=firstOne; i<=firstOne+4; i++){
            if(i==14){i=1;}
            if(hindo[i]!=1){straight1 = false;}
        }
        boolean straight2 = true;
        int count = 0;
        for(int i=firstOne; ; i--){
            if(count==5){break;}
            if(i==0){i=13;}
            if(hindo[i]!=1){straight2 = false;}
            count++;
        }
        if(straight1 || straight2){return "straight";}

        else return "null";
    }

    public String getResult(){
        return result;
    }

}
