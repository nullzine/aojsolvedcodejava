import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String W = sc.nextLine().toUpperCase();
        String T = "";
        while(true){
            String tmp = sc.nextLine();
            if(tmp.equals("END_OF_TEXT")){
                break;
            }else{
                T = T + tmp.toUpperCase() + " ";
            }
        }
        System.out.println(new Calculation(W,T).getSum());
    }

}

class Calculation{

    private String W;
    private String[] dataSet;

    public Calculation(String W,String T){
        this.W=W;
        dataSet=T.split(" ");
    }

    public int getSum(){
        int count = 0;
        for(int i=0;i<dataSet.length;i++){
            if(dataSet[i].equals(W)){
                count++;
            }
        }
        return count;
    }

}