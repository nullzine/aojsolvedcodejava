import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author nullzine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        DecimalFormat df1 = new DecimalFormat("0.000");
        while(sc.hasNext()){
            String str = sc.nextLine();
            Sweep sw = new Sweep(str);
            double[] answer = sw.getData();
            for(int i=0;i<answer.length;i++){
                BigDecimal bd = new BigDecimal(answer[i]);
                if(i==answer.length-1){
                    System.out.print(bd.setScale(3, BigDecimal.ROUND_HALF_UP));
                }else{
                    System.out.print(bd.setScale(3, BigDecimal.ROUND_HALF_UP)+" ");
                }
            }
            System.out.println("");
        }
    }
}

/*
 * ax+by=c
 * dx+ey=f 6
 *
 * ax+by+cz=d
 * ex+fy+gz=h
 * ix+jy+kz=l 12
 *
 * 20
 *
 * 30
 *
 * 36
 *
 */


class Sweep {

    private int LIMIT = 100;
    private int column,tuple;//columnは行,tupleは列
    private double[][] data;

    Sweep(String str){
        String[] strs = str.split(" ");
        tuple=analysisStringLength(strs);
        column = tuple+1;
        data = new double[tuple][column];
        setStringData(strs);
        try{
            Execution();
        }catch(IllegalStateException e){
            System.out.println("ERROR");
        }
    }

    /*
    Sweep(double[] input){
        tuple=analysisDoubleLength(input);
        column = tuple+1;
        data = new double[tuple][column];
        setDoubleData(input);
        try{
            Execution();
        }catch(IllegalStateException e){
            System.out.println("ERROR");
        }
    }
    */

    public double[] getData(){
        double[] answer = new double[tuple];
        for(int i=0;i<tuple;i++){
            answer[i]=data[i][column-1];
        }
        return answer;
    }

    private void Execution() {
        int i, i2, j;
        double aij;

        for (i = 0; i < tuple; i++) {

            if (data[i][i] == 0) {
                for (i2 = i + 1; i2 < tuple; i2++) {
                    if (data[i2][i] != 0) {
                        break;
                    }
                }
                if (i2 == tuple) {
                    throw new IllegalStateException("a[i][i]=0!\n");
                }
                for (j = 0; j < column; j++) {
                    aij = data[i2][j];
                    data[i2][j] = data[i][j];
                    data[i][j] = aij;
                }
            }

            aij = data[i][i];
            for (j = i; j < column; j++) {
                data[i][j] /= aij;
            }

            for (i2 = 0; i2 < tuple; i2++) {
                if (i != i2) {
                    aij = data[i2][i];
                    for (j = i; j < column; j++) {
                        data[i2][j] -= aij * data[i][j];
                    }
                }
            }
        }
    }

    private void setDoubleData(double[] input){
        int count=0;
        for(int i=0;i<tuple;i++){
            for(int j=0;j<column;j++){
                data[tuple][column]=input[count];
                count++;
            }
        }
    }

    private void setStringData(String[] strs){
        int count=0;
        for(int i=0;i<tuple;i++){
            for(int j=0;j<column;j++){
                data[i][j]=Double.parseDouble(strs[count]);
                count++;
            }
        }
    }

    private int analysisDoubleLength(double[] input){
        int i,j;
        for(i=3,j=2;i*j!=input.length;i++,j++){
            if(j==LIMIT){
                break;
            }
        }
        return j;
    }

    private int analysisStringLength(String[] strs){
        int i,j;
        for(i=3,j=2;i*j!=strs.length;i++,j++){
            if(j==LIMIT){
                break;
            }
        }
        return j;
    }

}