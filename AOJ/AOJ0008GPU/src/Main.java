import com.amd.aparapi.Kernel;

import java.util.Scanner;

/**
 * Created by nullzine on 2014/11/13.
 */
public class Main {
    public static void main(String[] args){
        Execution exe = new Execution();
        exe.execute(10000);
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(exe.get(Integer.parseInt(sc.nextLine())));
        }
    }
}

class Execution extends Kernel{

    private int data[];

    public Execution(){
        data = new int[4*9+1];
    }

    public void run(){
        int index = getGlobalId();
        int sum=0;
        for(int i=0;i<4;i++){
            sum+=index%pow(10,i+1)/pow(10,i);
        }
        data[sum]++;
    }

    public int get(int index){
        return data[index];
    }

}