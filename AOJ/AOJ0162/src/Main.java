import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/06.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        HammingNumber hn = new HammingNumber(1000000);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            if(Integer.parseInt(strs[0])==0){
                break;
            }
            System.out.println(hn.getSum(Integer.parseInt(strs[0]),Integer.parseInt(strs[1])));
        }
    }
}

class HammingNumber{

    private int limit;
    private boolean[] boolList;

    public HammingNumber(int limit){
        this.limit=limit;
        boolList = new boolean[limit+1];
        Arrays.fill(boolList,false);
        calculation();
    }

    private void calculation(){
        for(int i=1;i<=limit;i*=3){
            for(int j=1;i*j<=limit;j*=2){
                for(int k=1;k*j*i<=limit;k*=5){
                    boolList[k*i*j]=true;
                }
            }
        }
    }

    public int getSum(int n,int m){
        int sum=0;
        for(int i=n;i<=m;i++){
            if(boolList[i]){
                sum++;
            }
        }
        return sum;
    }

}