import java.util.Scanner;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/21
 * Time: 8:54
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        TreeSet<String> tSet = new TreeSet<String>();
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            tSet.add(sc.nextLine());
        }
        System.out.println(tSet.first());
    }
}