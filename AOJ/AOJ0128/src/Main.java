import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/26.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        while(sc.hasNext()){
            if(flag){
                flag=false;
            }else{
                System.out.println();
            }
            System.out.print(new Execution(Integer.parseInt(sc.nextLine())).toRotateString());
        }
    }
}

class Execution{

    private char[] boardData;
    private HashMap<Integer,String> hMap;
    private StringBuilder result;

    public Execution(int num){
        boardData = fillNum(num).toCharArray();
        hMap = new HashMap<Integer, String>();
        makeMap();
        calculation();
    }

    private String fillNum(int num){
        String tmp = Integer.toString(num);
        for(int i=tmp.length();i<5;i++){
            tmp="0"+tmp;
        }
        return tmp;
    }

    private void calculation(){
        result = new StringBuilder();
        for(int i=0;i<boardData.length;i++){
            result.append(hMap.get(Integer.parseInt(boardData[boardData.length-1-i]+""))+"\n");
        }
    }

    private void makeMap(){
        hMap.put(0,"* = ****");
        hMap.put(1,"* =* ***");
        hMap.put(2,"* =** **");
        hMap.put(3,"* =*** *");
        hMap.put(4,"* =**** ");
        hMap.put(5," *= ****");
        hMap.put(6," *=* ***");
        hMap.put(7," *=** **");
        hMap.put(8," *=*** *");
        hMap.put(9," *=**** ");
    }

    public String toString(){
        return result.toString();
    }

    public String toRotateString(){
        String[] strs = toString().split("\n");
        char[][] tmp = new char[strs.length][];
        for(int i=0;i<tmp.length;i++){
            tmp[i] = strs[i].toCharArray();
        }
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<tmp[0].length;i++){
            for(int j=0;j<tmp.length;j++){
                sb.append(tmp[tmp.length-1-j][i]);
            }
            sb.append("\n");
        }
        return sb.toString();
    }


}