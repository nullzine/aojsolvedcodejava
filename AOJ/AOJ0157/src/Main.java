import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/15.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            DataSet[] dataSets = new DataSet[n];
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(" ");
                dataSets[i] = new DataSet(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]));
            }
        }
    }
}

class Execution{

    private DataSet[] dataSets;

    public Execution(DataSet[] dataSets){
        this.dataSets=dataSets;
        calculation();
    }

    private void calculation(){
        for(int i=0;i<dataSets.length;i++){
            //for()
        }
    }

}

class DataSet{

    private int h;
    private int r;

    public DataSet(int h,int r){
        this.h=h;
        this.r=r;
    }

    public int getH(){
        return h;
    }

    public int getR(){
        return r;
    }

}