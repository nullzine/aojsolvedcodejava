import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/24.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new PocketBell(sc.nextLine()));
        }
    }
}

class PocketBell{

    private char[] strc;
    private String result;
    private String[][] wordMap;

    public PocketBell(String str){
        this.strc=str.toCharArray();
        result="";
        init("a,b,c,d,e\nf,g,h,i,j\nk,l,m,n,o\np,q,r,s,t\nu,v,w,x,y\nz,.,?,!, ");
        conversion();
    }

    private void conversion(){
        if(!validation(strc)){
            result="NA";
        }else{
            for(int i=0;i<strc.length;i+=2){
                result=result+conv(Integer.parseInt(strc[i+1]+""),Integer.parseInt(strc[i]+""));
            }
        }
    }

    private String conv(int row,int column){
        return wordMap[column-1][row-1];
    }

    private void init(String str){
        String[] strs = str.split("\n");
        wordMap = new String[strs.length][strs[0].split(",").length];
        for(int i=0;i<wordMap.length;i++){
            String[] tmp = strs[i].split(",");
            for(int j=0;j<tmp.length;j++){
                wordMap[i][j]=tmp[j];
            }
        }
    }

    private boolean validation(char[] strc){
        if(strc.length%2!=0){
            return false;
        }else{
            for(int i=0;i<strc.length;i+=2){
                if(6<Integer.parseInt(strc[i]+"")||Integer.parseInt(strc[i]+"")==0){
                    return false;
                }
            }
            for(int i=1;i<strc.length;i+=2){
                if(5<Integer.parseInt(strc[i]+"")||Integer.parseInt(strc[i]+"")==0){
                    return false;
                }
            }
            return true;
        }
    }

    public String toString(){
        return result;
    }

}