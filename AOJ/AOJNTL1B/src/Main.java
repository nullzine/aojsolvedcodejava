import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/11.
 */
public class Main {
    public static void main(String[] args){
        debug();
        /*
        String[] strs = new Scanner(System.in).nextLine().split(" ");
        int m = Integer.parseInt(strs[0]);
        int n = Integer.parseInt(strs[1]);
        //System.out.println(new BigInteger(Integer.toString(m)).pow(n).remainder(new BigInteger(Integer.toString(1000000007))));
        System.out.println(calculation(m,n,1000000007));
        */
    }

    private static void debug(){
        for(int i=2;i<=100;i++){
            System.out.println(calculation(i,(int)Math.pow(10,9),1000000007));
        }
    }

    private static long calculation(long m,long n,long mod){
        if(n==0){
            return 1;
        }
        long result = calculation(m * m % mod,n/2,mod);
        if(lastBit(n)){
            result = result * m % mod;
        }
        return result;
    }

    private static boolean lastBit(long num){
        return !(num%2==0);
    }

}