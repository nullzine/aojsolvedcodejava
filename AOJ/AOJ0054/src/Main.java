import java.util.*;
import java.math.BigDecimal;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(calculation(sc.nextLine()));
        }
    }

    private static int calculation(String str){
        String[] strs = str.split(" ");
        //char[] data = Double.toString(Double.parseDouble(strs[0])/Double.parseDouble(strs[1])-(int)(Double.parseDouble(strs[0])/Double.parseDouble(strs[1]))).toCharArray();
        char[] data = new BigDecimal(Double.toString(Double.parseDouble(strs[0])/Double.parseDouble(strs[1])-(int)(Double.parseDouble(strs[0])/Double.parseDouble(strs[1])))).toString().toCharArray();
        //System.out.println(data);
        int sum=0;
        for(int i=2;i<data.length;i++){
            if(i==Integer.parseInt(strs[2])+2){
                break;
            }
            sum=sum+Integer.parseInt(data[i]+"");
        }
        return sum;
    }

}