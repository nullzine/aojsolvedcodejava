import java.util.Scanner;

/**
 * Created by NullZine on 14/01/22.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            System.out.println(standardDiviation(sc.nextLine(),n));
        }
    }
    private static double standardDiviation(String str,int n){
        int[] dataSet = new int[n];
        double sd = 0;
        double m = 0;
        String[] strs = str.split(" ");
        for(int i=0;i<n;i++){
            dataSet[i]=Integer.parseInt(strs[i]);
            m+=dataSet[i];
        }
        m=m/n;
        for(int i=0;i<dataSet.length;i++){
            sd+=Math.pow(dataSet[i]-m,2);
        }
        sd=Math.sqrt(sd/n);
        return sd;
    }
}