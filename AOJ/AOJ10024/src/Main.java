import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] str = sc.nextLine().split(" ");
            System.out.println(Math.sqrt(Math.pow(Double.parseDouble(str[2])-Double.parseDouble(str[0]),2)+Math.pow(Double.parseDouble(str[3])-Double.parseDouble(str[1]),2)));
        }
    }
}