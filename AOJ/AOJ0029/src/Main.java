import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<String> strList = new ArrayList<String>();
        while(sc.hasNext()){
            strList.add(sc.next());
        }
        Analyse analyse = new Analyse(strList);
        System.out.println(analyse.getMostFreq()+" "+analyse.getMostLong());
    }
}

class Analyse{

    private ArrayList<String> strList;

    public Analyse(ArrayList<String> strList){
        this.strList=strList;
        Collections.sort(this.strList);
    }

    public String getMostFreq(){
        int max = 1;
        String strMax = strList.get(0);
        int tmp = 1;
        for(int i=1;i<strList.size();i++){
            if(strList.get(i).equals(strList.get(i-1))){
                tmp++;
                if(max<tmp){
                    max=tmp;
                    strMax = strList.get(i);
                }
            }else{
                tmp=1;
            }
        }
        return strMax;
    }

    public String getMostLong(){
        String strMax = "";
        for(int i=0;i<strList.size();i++){
            if(strMax.length()<strList.get(i).length()){
                strMax=strList.get(i);
            }
        }
        return strMax;
    }

}