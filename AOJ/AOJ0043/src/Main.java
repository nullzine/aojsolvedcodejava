import java.util.*;

/**
 * Created by nullzine on 2014/05/12.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine()));
        }
    }
}

class Execution{

    private char[] dataSet;
    private int[] counter;
    private ArrayList<Integer> result;
    private int[] tmp;

    public Execution(String str){
        dataSet = str.toCharArray();
        counter = new int[9];
        tmp = new int[9];
        result = new ArrayList<Integer>();
        makeCounter();
        calculation();
    }

    private void calculation(){
        for(int i=0;i<9;i++){
            for(int j=0;j<9;j++){
                if(counter[i]<4){
                    tmp = counter.clone();
                    tmp[i]++;
                    tmp[j]-=2;
                    for(int k=0;k<7;k++){
                        if(tmp[k]<0){
                            tmp[8]=-1;
                            break;
                        }
                        if(tmp[k]==1 || tmp[k]==4){
                            tmp[k]--;
                            tmp[k+1]--;
                            tmp[k+2]--;
                        }else if(tmp[k]==2){
                            tmp[k]-=2;
                            tmp[k+1]-=2;
                            tmp[k+2]-=2;
                        }
                    }
                    if(tmp[7]>=0 && tmp[7]%3==0 && tmp[8]>=0 && tmp[8]%3==0){
                        result.add(i+1);
                        break;
                    }
                }
            }
        }
    }

    private void makeCounter(){
        Arrays.fill(counter,0);
        for(int i=0;i<dataSet.length;i++){
            counter[Integer.parseInt(dataSet[i]+"")-1]++;
        }
    }

    public String toString(){
        if(result.size()==0){
            return "0";
        }else{
            String str = Integer.toString(result.get(0));
            for(int i=1;i<result.size();i++){
                str=str+" "+Integer.toString(result.get(i));
            }
            return str;
        }
    }


}