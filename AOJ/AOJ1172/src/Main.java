import java.util.*;

/**
 *
 * @author NullZine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Mat mt = new Mat(320000);
        //mt.debugArray();
        Scanner sc = new Scanner(System.in);
        while(true){
            int number=sc.nextInt();
            if(number==0){
                break;
            }
            int count=0;
            for(int i=number+1;i<=number*2;i++){
                if(mt.primelist[i]==true)
                    count++;
            }
            System.out.println(count);
            count=0;
        }
    }
}

class Mat {
    boolean[] primelist;    //指定された値が素数かどうかの一覧
    private ArrayList<Integer> aList = new ArrayList<Integer>(); //素数の一覧 可変長配列

    Mat(int primelimit){
        primelist = new boolean[primelimit];
        makeList(primelimit);
        //primelimitは発見される素数の最大値である。
    }

    int matsuzaki(int n,int p){
        List<Integer> res = new ArrayList<Integer>();
        int k=0;
        while(aList.get(k)<=n)k++;
        for(int i=k;i<k+p;i++){
            for(int j=i;j<k+p;j++){
                res.add(aList.get(i)+aList.get(j));
            }
        }
        Collections.sort(res);
        return res.get(p-1);
    }

    void debugArray(){
        for(int i=0;i<primelist.length;i++){
            System.out.println(primelist[i]);
        }
        Iterator iter = aList.iterator();
        while(iter.hasNext()) {
            Object tmp = iter.next();
            System.out.println(tmp);
        }
    }

    private void makeList(int limit){
        Arrays.fill(primelist,true);
        primelist[0]=false;
        primelist[1]=false;
        for (int i=2; i<limit; i++) {
            if (primelist[i]) {
                aList.add(i);
                for (int j=i+i; j<limit; j+=i)
                    primelist[j] = false;
            }
        }
    }

}