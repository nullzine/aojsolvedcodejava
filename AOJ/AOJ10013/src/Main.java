import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Frame frame = new Frame();
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int h = Integer.parseInt(strs[0]);
            int w = Integer.parseInt(strs[1]);
            if(h==0&&w==0){
                break;
            }
            frame.printFrame(h,w);
            System.out.println("");
        }
    }
}

class Frame{

    public String getFrame(int h,int w){
        String set = "";
        for(int i=0;i<h;i++){
            for(int j=0;j<w;j++){
                if(i==0||i==h-1||j==0||j==w-1){
                    set=set+"#";
                }else{
                    set=set+".";
                }
            }
            set=set+"\n";
        }
        return set;
    }

    public void printFrame(int h,int w){
        for(int i=0;i<h;i++){
            for(int j=0;j<w;j++){
                if(i==0||i==h-1||j==0||j==w-1){
                    System.out.print("#");
                }else{
                    System.out.print(".");
                }
            }
            System.out.println("");
        }
    }

}