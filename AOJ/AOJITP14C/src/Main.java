import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            if(strs[1].equals("?")){
                break;
            }
            if(strs[1].equals("+")){
                System.out.println(Integer.toString(Integer.parseInt(strs[0])+Integer.parseInt(strs[2])));
            }else if(strs[1].equals("-")){
                System.out.println(Integer.toString(Integer.parseInt(strs[0])-Integer.parseInt(strs[2])));
            }else if(strs[1].equals("*")){
                System.out.println(Integer.toString(Integer.parseInt(strs[0])*Integer.parseInt(strs[2])));
            }else if(strs[1].equals("/")){
                System.out.println(Integer.toString(Integer.parseInt(strs[0])/Integer.parseInt(strs[2])));
            }else{
                System.out.println("ERROR");
            }
        }
    }
}