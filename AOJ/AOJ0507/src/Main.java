import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/12/01.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = Integer.parseInt(sc.nextLine());
            if (n == 0) {
                break;
            } else {
                new Execution(n);
            }
        }
    }
}

class Execution {

    private int n;
    private int[] x;

    public Execution(int n) {
        this.n = n;
        x = new int[n+1];
        System.out.print(dfs(n, n, 0));
    }

    private String dfs(int rem, int mx, int p) {
        if (rem == 0) {
            ArrayList<Integer> squareData = new ArrayList<Integer>();
            for (mx = 0; mx < p; mx++) {
                squareData.add(x[mx]);
            }
            return array2Str(squareData);
        }
        StringBuilder result = new StringBuilder();
        while (mx >= 1) {
            if (rem < mx) {
                mx--;
                continue;
            }
            x[p] = mx;
            result.append(dfs(rem - mx, mx, p + 1));
            mx--;
        }
        return result.toString();
    }

    private String array2Str(ArrayList<Integer> aList){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<aList.size();i++){
            sb.append(aList.get(i)+(i==aList.size()-1?"\n":" "));
        }
        return sb.toString();
    }

}