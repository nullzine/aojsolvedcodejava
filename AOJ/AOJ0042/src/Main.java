import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/15.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int count=1;
        while(true){
            int W = Integer.parseInt(sc.nextLine());
            if(W==0){
                break;
            }
            int n = Integer.parseInt(sc.nextLine());
            Treasure[] treasures = new Treasure[n];
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(",");
                treasures[i] = new Treasure(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]));
            }
            Execution exe = new Execution(treasures,W);
            System.out.println("Case "+count+":");
            System.out.println(exe.getMaxValue());
            System.out.println(exe.getMaxWeight());
            count++;
        }
    }
}

class Execution{

    private int W;
    private Treasure[] treasures;
    private int[] dp;
    private int maxWeight;
    private int maxValue;

    public Execution(Treasure[] treasures,int W){
        this.W=W;
        this.treasures=treasures;
        dp = new int[W+1];
        calculation();
    }

    private void calculation(){
        for(int i=0;i<treasures.length;i++){
            for(int j=W;0<=j;j--){
                if(j+treasures[i].weight<=W){
                    dp[j+treasures[i].weight]=Math.max(dp[j+treasures[i].weight],treasures[i].value+dp[j]);
                }
            }
        }
        maxValue=-1;
        for(int i=W;0<=i;i--){
            if(maxValue<=dp[i]){
                maxValue=dp[i];
                maxWeight=i;
            }
        }
    }

    public int getMaxWeight(){
        return maxWeight;
    }

    public int getMaxValue(){
        return maxValue;
    }

}

class Treasure{

    public int value;
    public int weight;

    public Treasure(int value,int weight){
        this.value=value;
        this.weight=weight;
    }

}