import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 7:23
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int l = Integer.parseInt(sc.nextLine());
            if(l==0){
                break;
            }else{
                int[][] data = new int[12][2];
                for(int i=0;i<data.length;i++){
                    String[] strs = sc.nextLine().split(" ");
                    data[i][0] = Integer.parseInt(strs[0]);
                    data[i][1] = Integer.parseInt(strs[1]);
                }
                System.out.println(new Execution(l,data).toString());
            }
        }
    }
}


class Execution{

    private int l;
    private int[][] data;
    private int month;

    public Execution(int l,int[][] data){
        this.l=l;
        this.data=data;
        month = -1;
        calculation();
    }

    private void calculation(){
        int sum = 0;
        for(int i=0;i<data.length;i++){
            sum+=data[i][0]-data[i][1];
            if(l<=sum){
                month=i+1;
                break;
            }
        }
    }

    public String toString(){
        if(month==-1){
            return "NA";
        }else{
            return Integer.toString(month);
        }
    }

}