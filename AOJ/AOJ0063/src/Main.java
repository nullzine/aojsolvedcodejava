import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int count=0;
        while(sc.hasNext()){
            if(validPal(sc.nextLine())){
                count++;
            }
        }
        System.out.println(count);
    }
    private static boolean validPal(String str){
        char[] strc = str.toCharArray();
        for(int i=0;i<strc.length;i++){
            if(strc[i]!=strc[strc.length-1-i]){
                return false;
            }
        }
        return true;
    }
}
