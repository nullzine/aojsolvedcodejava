import java.util.Scanner;

/**
 * Created by nullzine on 2014/06/20.
 */
public class Main {
    public static void main(String[] args){
        Scanner c = new Scanner(System.in);
        while(c.hasNext()){
            System.out.println(analysis(c.nextLine().split(" ")));
        }
    }
    private static int analysis(String[] s){
        for(int i=Math.min(Integer.parseInt(s[0]),Integer.parseInt(s[1]));0<i;i--){
            if(Integer.parseInt(s[0])%i==0&&Integer.parseInt(s[1])%i==0){
                return i;
            }
        }
        return 1;
    }
}