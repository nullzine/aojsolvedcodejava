import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/04.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println(new Execution(sc.nextLine(),sc.nextLine()));
    }
}

class Execution{

    private String data;
    private String target;

    public Execution(String data,String target){
        this.data=data;
        this.target=target;
    }

    private ArrayList<String> getAvailableStringList(){
        ArrayList<String> ret = new ArrayList<String>();
        for(int i=0;i<data.length();i++){
            ret.add(getLoopStr(i,target.length()));
        }
        return ret;
    }

    private String getLoopStr(int begin,int length){
        StringBuilder sb = new StringBuilder();
        for(int i=begin;i<begin+length;i++){
            if(data.length()<=i){
                sb.append(data.charAt(i-data.length()));
            }else{
                sb.append(data.charAt(i));
            }
        }
        return sb.toString();
    }

    public String toString(){
        if(getAvailableStringList().contains(target)){
            return "Yes";
        }else{
            return "No";
        }
    }

}