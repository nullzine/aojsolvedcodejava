import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/01.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            int[] polygon = new int[n];
            int last = 360;
            for(int i=0;i<n-1;i++){
                polygon[i]=Integer.parseInt(sc.nextLine());
                last-=polygon[i];
            }
            polygon[polygon.length-1]=last;
            Polygon a = new Polygon(polygon);
            n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            polygon = new int[n];
            last = 360;
            for(int i=0;i<n-1;i++){
                polygon[i]=Integer.parseInt(sc.nextLine());
                last-=polygon[i];
            }
            polygon[polygon.length-1]=last;
            Polygon b = new Polygon(polygon);
            System.out.println(new Execution(a,b).contains());
        }
    }
}

class Execution{

    private Polygon a;
    private Polygon b;

    public Execution(Polygon a,Polygon b){
        this.a=a;
        this.b=b;
    }

    public int contains(){
        //System.out.println(a.getS()+" "+b.getS());
        if(a.getS()==b.getS()){
            return 0;
        }else if(a.getS()<b.getS()){
            return 2;
        }else{
            return 1;
        }
    }


}

class Polygon{

    private int[] polygon;

    public Polygon(int[] polygon){
        this.polygon=polygon;
    }

    public double getS(){
        double s = 0;
        for(int i=0;i<polygon.length;i++){
            s+=Math.sin(polygon[i]*Math.PI/180);
        }
        return (double)((int)(s*1000000))/1000000;
    }

    public void print(){
        System.out.println("========");
        for(int n:polygon){
            System.out.println(n);
        }
        System.out.println("========");
    }

}