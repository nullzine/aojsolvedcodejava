import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(str.split(" ")[1].equals("?")){
                break;
            }else{
                System.out.println(calc(str));
            }
        }
    }

    private static int calc(String str){
        String[] strs = str.split(" ");
        if(strs[1].equals("+")){
            return Integer.parseInt(strs[0])+Integer.parseInt(strs[2]);
        }else if(strs[1].equals("-")){
            return Integer.parseInt(strs[0])-Integer.parseInt(strs[2]);
        }else if(strs[1].equals("*")){
            return Integer.parseInt(strs[0])*Integer.parseInt(strs[2]);
        }else if(strs[1].equals("/")){
            return Integer.parseInt(strs[0])/Integer.parseInt(strs[2]);
        }else{
            return 0;
        }
    }

}