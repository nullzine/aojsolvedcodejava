import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int x = Integer.parseInt(strs[0]);
            int y = Integer.parseInt(strs[1]);
            if(x==0&&y==0){
                break;
            }
            if(x<y){
                System.out.println(x+" "+y);
            }else{
                System.out.println(y+" "+x);
            }
        }
    }
}