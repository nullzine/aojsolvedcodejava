import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/21
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] str = sc.nextLine().split(" ");
            Triangle tri = new Triangle(Integer.parseInt(str[0]),Integer.parseInt(str[1]),Integer.parseInt(str[2]));
            System.out.println(tri.getS());
            System.out.println(tri.getL());
            System.out.println(tri.geth());
        }
    }
}

class Triangle{

    private double S;
    private double L;
    private double h;

    public Triangle(int a,int b,int C){
        S = (a*b*Math.sin(Math.toRadians(C)))/2;
        L = a+b+Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) - 2*a*b*Math.cos(Math.toRadians(C)));
        h = b*Math.sin(Math.toRadians(C));
    }

    public double getS(){
        return S;
    }

    public double getL(){
        return L;
    }

    public double geth(){
        return h;
    }

}