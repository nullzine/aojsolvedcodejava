import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        HashMap<String,String> hMap = new HashMap<String,String>();
        hMap.put("A","a");
        hMap.put("B","b");
        hMap.put("C","c");
        hMap.put("D","d");
        hMap.put("E","e");
        hMap.put("F","f");
        hMap.put("G","g");
        hMap.put("H","h");
        hMap.put("I","i");
        hMap.put("J","j");
        hMap.put("K","k");
        hMap.put("L","l");
        hMap.put("M","m");
        hMap.put("N","n");
        hMap.put("O","o");
        hMap.put("P","p");
        hMap.put("Q","q");
        hMap.put("R","r");
        hMap.put("S","s");
        hMap.put("T","t");
        hMap.put("U","u");
        hMap.put("V","v");
        hMap.put("W","w");
        hMap.put("X","x");
        hMap.put("Y","y");
        hMap.put("Z","z");
        hMap.put("a","A");
        hMap.put("b","B");
        hMap.put("c","C");
        hMap.put("d","D");
        hMap.put("e","E");
        hMap.put("f","F");
        hMap.put("g","G");
        hMap.put("h","H");
        hMap.put("i","I");
        hMap.put("j","J");
        hMap.put("k","K");
        hMap.put("l","L");
        hMap.put("m","M");
        hMap.put("n","N");
        hMap.put("o","O");
        hMap.put("p","P");
        hMap.put("q","Q");
        hMap.put("r","R");
        hMap.put("s","S");
        hMap.put("t","T");
        hMap.put("u","U");
        hMap.put("v","V");
        hMap.put("w","W");
        hMap.put("x","X");
        hMap.put("y","Y");
        hMap.put("z","Z");
        char[] strc = sc.nextLine().toCharArray();
        for(int i=0;i<strc.length;i++){
            if(hMap.get(strc[i]+"")!=null){
                strc[i]=hMap.get(strc[i]+"").toCharArray()[0];
            }
        }
        System.out.println(String.valueOf(strc));
    }
}