import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/01/01
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            RailSection rs = new RailSection(sc.nextLine());
            System.out.println(rs.getSection());
        }
    }

}

class RailSection{

    private int[] stations;
    private int trainSpeedA;
    private int trainSpeedB;
    private double conflict;

    public RailSection(String str){
        stations = new int[str.split(",").length-2];
        for(int i=0;i<str.split(",").length-2;i++){
            stations[i] = Integer.parseInt(str.split(",")[i]);
        }
        trainSpeedA = Integer.parseInt(str.split(",")[str.split(",").length-2]);
        trainSpeedB = Integer.parseInt(str.split(",")[str.split(",").length-1]);
        conflict = (double)totalLength(stations)*(double)trainSpeedB/(double)(trainSpeedA+trainSpeedB);
        /*
        System.out.println(trainSpeedA);
        System.out.println(trainSpeedB);
        System.out.println(totalLength(stations));
        System.out.println((double)totalLength(stations)*(double)trainSpeedB/(double)(trainSpeedA+trainSpeedB));
        */
    }

    private int totalLength(int[] sta){
        int sum = 0;
        for(int i=0;i<sta.length;i++){
            sum = sum + sta[i];
        }
        return sum;
    }

    public int getSection(){
        for(int i=stations.length-1; i>=0; i--){
            conflict = conflict - stations[i];
            if(conflict<0){
                return i+1;
            }
        }
        return -1;
    }

}