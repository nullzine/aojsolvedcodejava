import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/02/28
 * Time: 19:41
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(calculation(sc.nextLine()));
        }
    }

    private static int calculation(String str){
        String[] strs = str.split(" ");
        ArrayList<Integer> aList = new ArrayList<Integer>();
        for(int i=0;i<strs.length;i++){
            try{
                int n = Integer.parseInt(strs[i]);
                aList.add(n);
            }catch(NumberFormatException e){
                if(strs[i].equals("+")){
                    int tmp = aList.get(aList.size()-2)+aList.get(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.add(tmp);
                }
                if(strs[i].equals("-")){
                    int tmp = aList.get(aList.size()-2)-aList.get(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.add(tmp);
                }
                if(strs[i].equals("*")){
                    int tmp = aList.get(aList.size()-2)*aList.get(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.remove(aList.size()-1);
                    aList.add(tmp);
                }
            }
        }
        return aList.get(0);
    }

}