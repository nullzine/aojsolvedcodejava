import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/23
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(str.equals("-")){
                break;
            }
            DataSet ds = new DataSet(str);
            int n = Integer.parseInt(sc.nextLine());
            for(int i=0;i<n;i++){
                ds.shuffle(Integer.parseInt(sc.nextLine()));
            }
            System.out.println(ds.toString());
        }
    }

    private static boolean isNumber(String str) {
        int n;
        try{
            n = Integer.parseInt(str);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

}

class DataSet{

    private String data;

    public DataSet(String str){
        data=str;
    }

    public String toString(){
        return data;
    }

    public void shuffle(int num){
        data = data.substring(num)+data.substring(0,num);
    }

    /*
    private void manipulation(){
        data = data + data.charAt(0);
        data = data.substring(0,data.length()-1);
    }
    */

}