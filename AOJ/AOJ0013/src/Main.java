import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 2014/04/02.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> aList = new ArrayList<Integer>();
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                System.out.println(aList.remove(aList.size()-1));
            }else{
                aList.add(n);
            }
        }
    }
}
