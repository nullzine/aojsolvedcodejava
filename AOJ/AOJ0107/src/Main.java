import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int tate = Integer.parseInt(strs[0]);
            int yoko = Integer.parseInt(strs[1]);
            int takasa = Integer.parseInt(strs[2]);
            if(tate==0&&yoko==0&&takasa==0){
                break;
            }
            int n = Integer.parseInt(sc.nextLine());
            for(int i=0;i<n;i++){
                int circle = Integer.parseInt(sc.nextLine());
                int length1=tate*tate+yoko*yoko;
                int length2=tate*tate+takasa*takasa;
                int length3=yoko*yoko+takasa*takasa;
                if(4*circle*circle>length1||4*circle*circle>length2||4*circle*circle>length3){
                    System.out.println("OK");
                }else{
                    System.out.println("NA");
                }
            }
        }
    }
}
