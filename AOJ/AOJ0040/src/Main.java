import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/09.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            System.out.println(new Execution(sc.nextLine()).getResult());
        }
    }
}

class Execution{

    private String dataSet;
    private char[] strc;
    private HashMap<String,Integer> encode;
    private HashMap<Integer,String> decode;
    private HashSet<String> target;
    private String result;

    public Execution(String dataSet){
        this.dataSet = dataSet.toLowerCase();
        strc = this.dataSet.toCharArray();
        encode = new HashMap<String, Integer>();
        decode = new HashMap<Integer, String>();
        target = new HashSet<String>();
        result="";
        makeTarget();
        makeMap();
        calculation();
    }

    private void calculation(){
        int[] encodedData = encoder(dataSet);
        outside:for(int i=0;i<26;i++){
            for(int j=0;j<26;j++){
                //System.out.println(decoder(new Formula(i,j).processingFormula(encodedData)));
                if(validDataSet(decoder(new Formula(i,j).processingFormula(encodedData)))){
                    result = decoder(new Formula(i,j).processingFormula(encodedData));
                    break outside;
                }
            }
        }
    }

    private boolean validDataSet(String str){
        String[] strs = str.split(" ");
        for(int i=0;i<strs.length;i++){
            if(target.contains(strs[i])){
                return true;
            }
        }
        return false;
    }

    private int[] encoder(String str){
        int[] tmp = new int[str.length()];
        for(int i=0;i<tmp.length;i++){
            if(encode.get(strc[i]+"")!=null){
                tmp[i]=encode.get(strc[i]+"");
            }else{
                tmp[i]=-1;
            }
        }
        return tmp;
    }

    private String decoder(int[] data){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<data.length;i++){
            if(decode.get(data[i])!=null){
                sb.append(decode.get(data[i]));
            }else{
                sb.append(strc[i]);
            }
        }
        return sb.toString();
    }

    private void makeMap(){
        doMap(0,"a");
        doMap(1,"b");
        doMap(2,"c");
        doMap(3,"d");
        doMap(4,"e");
        doMap(5,"f");
        doMap(6,"g");
        doMap(7,"h");
        doMap(8,"i");
        doMap(9,"j");
        doMap(10,"k");
        doMap(11,"l");
        doMap(12,"m");
        doMap(13,"n");
        doMap(14,"o");
        doMap(15,"p");
        doMap(16,"q");
        doMap(17,"r");
        doMap(18,"s");
        doMap(19,"t");
        doMap(20,"u");
        doMap(21,"v");
        doMap(22,"w");
        doMap(23,"x");
        doMap(24,"y");
        doMap(25,"z");
    }

    private void doMap(int n,String s){
        encode.put(s,n);
        decode.put(n,s);
    }

    private void makeTarget(){
        target.add("that");
        target.add("this");
    }

    class Formula{

        private int alpha;
        private int beta;

        public Formula(int alpha,int beta){
            this.alpha=alpha;
            this.beta=beta;
        }

        public int[] processingFormula(int[] data){
            int tmp[] = new int[data.length];
            for(int i=0;i<tmp.length;i++){
                tmp[i]=convert(data[i]);
            }
            return tmp;
        }

        private int convert(int num){
            if ( 0 <= num && num <= 25 ) {
                return ( ( num * alpha + beta ) % 26 );
            }else{
                return num;
            }
        }


    }

    public String getResult(){
        return result;
    }

    private void debugIntArray(int[] num){
        for(int i=0;i<num.length;i++){
            System.out.print(num[i]+" ");
        }
        System.out.println();
    }

}