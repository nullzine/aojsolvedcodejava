import java.util.*;

public class Main{

    private static int[] data = {0,0,0,0};

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String str = sc.nextLine();
            String[] strs = str.split(",");
            data[convData(strs[1])]++;
        }
        for(int i=0;i<data.length;i++){
            System.out.println(data[i]);
        }
    }

    private static int convData(String str){
        if(str.equals("A")){
            return 0;
        }else if(str.equals("B")){
            return 1;
        }else if(str.equals("AB")){
            return 2;
        }else if(str.equals("O")){
            return 3;
        }else{
            return -1;
        }
    }

}