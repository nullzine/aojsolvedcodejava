import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/06/11.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true) {
            Execution exe = new Execution();
            int m = Integer.parseInt(sc.nextLine());
            if(m==0){
                break;
            }
            for (int i = 0; i < m; i++) {
                String[] strs = sc.nextLine().split(" ");
                exe.addTargetStar(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]));
            }
            int n = Integer.parseInt(sc.nextLine());
            for(int i=0;i<n;i++){
                String[] strs = sc.nextLine().split(" ");
                exe.addFieldStar(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]));
            }
            exe.analysis();
            System.out.println(exe.getResultX()+" "+exe.getResultY());
        }
    }
}

class Execution{

    private ArrayList<Integer> mx;
    private ArrayList<Integer> my;
    private ArrayList<Integer> nx;
    private ArrayList<Integer> ny;
    private int resultX,resultY;

    public Execution(){
        mx = new ArrayList<Integer>();
        my = new ArrayList<Integer>();
        nx = new ArrayList<Integer>();
        ny = new ArrayList<Integer>();
    }

    public void addTargetStar(int x,int y){
        mx.add(x);
        my.add(y);
    }

    public void addFieldStar(int x,int y){
        nx.add(x);
        ny.add(y);
    }

    public void analysis(){
        for(int z=0;z<nx.size();z++){
            int x = nx.get(z)-mx.get(0);
            int y = ny.get(z)-my.get(0);
            boolean flag = true;
            for(int i=0;i<mx.size();i++){
                for(int j=0;j<nx.size();j++){
                    if(mx.get(i)+x==nx.get(j) && my.get(i)+y==ny.get(j)){
                        break;
                    }
                    if(j==nx.size()-1){
                        flag = false;
                    }
                }
                if(flag==false) break;
            }
            if(flag==true){
                resultX=x;
                resultY=y;
                break;
            }
        }
    }

    public int getResultX(){
        return resultX;
    }

    public int getResultY(){
        return resultY;
    }


}