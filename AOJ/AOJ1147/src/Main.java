/**
 * Created by nullzine on 2014/09/26.
 */
import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            int sum=0;
            int max = Integer.MIN_VALUE;
            int min = Integer.MAX_VALUE;
            for(int i=0;i<n;i++){
                int m = Integer.parseInt(sc.nextLine());
                sum+=m;
                if(max<m){
                    max=m;
                }
                if(m<min){
                    min=m;
                }
            }
            System.out.println(((sum-max-min)/(n-2)));
        }
    }
}