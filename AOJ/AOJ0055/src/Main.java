import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(10);
        while(sc.hasNext()){
            System.out.println(exe.seq(sc.nextDouble()));
        }
    }
}

class Execution{

    private double[] data;

    public Execution(int limit){
        data = new double[limit];
    }

    public double seq(double number){
        data[0]=number;
        for(int i=1;i<data.length;i++){
            if(i%2==0){
                data[i]=data[i-1]/3;
            }else{
                data[i]=data[i-1]*2;
            }
        }
        return getSum();
    }

    private double getSum(){
        double sum = 0;
        for(int i=0;i<data.length;i++){
            sum=sum+data[i];
        }
        return sum;
    }
}