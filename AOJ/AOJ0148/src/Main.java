import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int input = Integer.parseInt(sc.nextLine());
            System.out.print("3C");
            int output = input-((input/39)*39);
            if(output==0){
                System.out.println("39");
            }else if(output<10){
                System.out.println("0"+output);
            }else{
                System.out.println(output);
            }
        }
    }
}
