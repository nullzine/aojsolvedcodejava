import java.util.*;

/**
 *
 * @author NullZine
 */
class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Validate ck = new Validate();
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=0;i<n;i++){
            String str = sc.next();
            if(ck.checkSnakeA(str)==1)
                System.out.println("A");
            else if(ck.checkSnakeB(str)==1)
                System.out.println("B");
            else
                System.out.println("NA");
        }
    }
}

class Validate {
    int checkSnakeA(String str){
        if(str.matches("^>'(=+)#\\1~$"))
            return 1;
        return 0;
    }
    int checkSnakeB(String str){
        if(str.matches("^>\\^(Q=)+~~$"))
            return 1;
        return 0;
    }
}