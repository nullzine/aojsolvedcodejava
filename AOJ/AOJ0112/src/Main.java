import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/17
 * Time: 12:27
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            int[] data = new int[n];
            for(int i=0;i<n;i++){
                data[i] = Integer.parseInt(sc.nextLine());
            }
            System.out.println(new Calculation(data).getTimeSum());
        }
    }
}

class Calculation{

    private int[] data;

    public Calculation(int[] data){
        this.data=data;
        Arrays.sort(data);
    }

    public long getTimeSum(){
        long time = 0;
        long sum = 0;
        for (int i = 0; i < data.length; i++) {
            time += sum;
            sum += data[i];
        }
        return time;
    }

}