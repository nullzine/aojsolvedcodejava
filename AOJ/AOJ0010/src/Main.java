import java.util.*;

public class Main{

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        for(int i=0;i<n;i++){
            double x1 = sc.nextDouble();
            double y1 = sc.nextDouble();
            double x2 = sc.nextDouble();
            double y2 = sc.nextDouble();
            double x3 = sc.nextDouble();
            double y3 = sc.nextDouble();

            double a = 2*(x2-x1);
            double b = 2*(y2-y1);
            double c = x1*x1 - x2*x2 + y1*y1 - y2*y2;
            double d = 2*(x3-x1);
            double e = 2*(y3-y1);
            double f = x1*x1 - x3*x3 + y1*y1 - y3*y3;

            double xp = (b * f - c * e) / (e * a - b * d);
            double yp = (d * c - a * f) / (e * a - d * b);
            double r = Math.sqrt((x1-xp)*(x1-xp)+(y1-yp)*(y1-yp));
            System.out.printf("%.3f %.3f %.3f\n", xp, yp, r);
        }
    }
}