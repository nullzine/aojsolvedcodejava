import java.util.Scanner;

/**
 *
 * @author nullzine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        RomanFigure rf = new RomanFigure();
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String str=sc.next();
            int number=0,flag=0;
            try{
                number=Integer.parseInt(str);
            }catch(NumberFormatException e){
                flag++;
            }
            if(flag==1){
                System.out.println(rf.Decode(str));
            }else{
                System.out.println(rf.Encode(number));
            }
        }
    }
}

/*
1   I   11  XI  30  XXX
2   II  12  XII 40  XL
3   III 13  XIII    50  L
4   IV  14  XIV 60  LX
5   V   15  XV  70  LXX
6   VI  16  XVI 80  LXXX
7   VII 17  XVII    90  XC
8   VIII    18  XVIII   100 C
9   IX  19  XIX 500 D
10  X   20  XX  1000    M
 */

class RomanFigure {

    public String Encode(int number){
        String answer="";
        for(int i=number/1000;0<i;i--){
            answer=answer+"M";
        }
        number=number%1000;
        if(500<=number){
            number=number-500;
            answer=answer+"D";
        }
        for(int i=number/100;0<i;i--){
            answer=answer+"C";
        }
        number=number%100;
        if(90<=number){
            answer=answer+"XC";
            number=number-90;
        }else{
            if(50<=number){
                answer=answer+"L";
                number=number-50;
            }else if(40<=number){
                answer=answer+"XL";
                number=number-40;
            }
        }
        for(int i=number/10;0<i;i--){
            answer=answer+"X";
        }
        number = number%10;
        if(number==9){
            answer=answer+"IX";
            number=number-9;
        }else{
            if(5<=number){
                answer=answer+"V";
                number=number-5;
            }else if(number==4){
                answer=answer+"IV";
                number=number-4;
            }
            for(int i=number;0<i;i--){
                answer=answer+"I";
                number--;
            }
        }
        if(number!=0){
            System.out.println("ERROR?");
        }
        return answer;

    }

    public int Decode(String str){
        char[] strc = str.toCharArray();
        int[] value = new int[strc.length];
        for (int i = 0; i < strc.length; i++) {
            if (strc[i] == 'I') {
                value[i] = 1;
            } else if (strc[i] == 'V') {
                value[i] = 5;
            } else if (strc[i] == 'X') {
                value[i] = 10;
            } else if (strc[i] == 'L') {
                value[i] = 50;
            } else if (strc[i] == 'C') {
                value[i] = 100;
            } else if (strc[i] == 'D') {
                value[i] = 500;
            } else if (strc[i] == 'M') {
                value[i] = 1000;
            }
        }
        int answer=value[strc.length-1];
        for(int i=strc.length-1;i>0;i--){
            if(value[i-1]<value[i])
                answer=answer-value[i-1];
            else answer=answer+value[i-1];
        }
        return answer;
    }

}