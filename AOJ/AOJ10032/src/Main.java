import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/23
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Stack<String> stack = new Stack<String>();
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("quit")){
                break;
            }
            if(str.length==2){
                stack.push(str[1]);
            }else{
                System.out.println(stack.pop());
            }
        }
    }
}