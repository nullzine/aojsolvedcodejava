import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/21
 * Time: 9:00
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String target = sc.nextLine();
        String dataSet = "";
        int count=0;
        while(!(dataSet = sc.next()).equals("END_OF_TEXT")){
            if(dataSet.equalsIgnoreCase(target)){
                count++;
            }
        }
        System.out.println(count);
    }
}