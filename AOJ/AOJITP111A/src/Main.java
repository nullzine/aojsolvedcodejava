import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/10.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Dice dice = new Dice(sc.nextLine());
        dice.doCommand(sc.nextLine());
        System.out.println(dice.getTop());
    }
}

class Dice{

    private int[] diceData;
    private int position;

    public Dice(String data){
        diceData = makeDice(data.split(" "));
        position=0;
    }

    private int[] makeDice(String[] strs){
        if(strs.length!=6){
            System.out.println("ERROR");
            return null;
        }else{
            int[] tmp = new int[6];
            for(int i=0;i<tmp.length;i++){
                tmp[i] = Integer.parseInt(strs[i]);
            }
            return tmp;
        }
    }

    public void doCommand(String str){
        for(int i=0;i<str.length();i++){
            switch (str.charAt(i)){
                case 'E':
                    doMoveE();
                    break;
                case 'N':
                    doMoveN();
                    break;
                case 'S':
                    doMoveS();
                    break;
                case 'W':
                    doMoveW();
                    break;
            }
        }
    }

    private void doMoveE(){
        int tmp=diceData[0];
        diceData[0]=diceData[3];
        diceData[3]=diceData[5];
        diceData[5]=diceData[2];
        diceData[2]=tmp;
    }

    private void doMoveN(){
        int tmp=diceData[0];
        diceData[0]=diceData[1];
        diceData[1]=diceData[5];
        diceData[5]=diceData[4];
        diceData[4]=tmp;
    }

    private void doMoveS(){
        int tmp=diceData[0];
        diceData[0]=diceData[4];
        diceData[4]=diceData[5];
        diceData[5]=diceData[1];
        diceData[1]=tmp;
    }

    private void doMoveW(){
        int tmp=diceData[0];
        diceData[0]=diceData[2];
        diceData[2]=diceData[5];
        diceData[5]=diceData[3];
        diceData[3]=tmp;
    }

    public int getTop(){
        return diceData[0];
    }

}