import java.util.*;

/**
 * Created by nullzine on 2014/05/01.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        ArrayList<String> a = new ArrayList<String>();
        ArrayList<String> b = new ArrayList<String>();
        boolean flag = true;
        while(sc.hasNext()){
            String str = sc.nextLine();
            if(str.equals("")){
                flag = false;
            }else {
                if (flag) {
                    a.add(str);
                } else {
                    b.add(str);
                }
            }
        }
        System.out.print(new Execution(a, b));
    }
}

class Execution{

    private ArrayList<String> lastMonth;
    private ArrayList<String> thisMonth;
    private HashMap<Integer,Company> lastMonthData;
    private HashMap<Integer,Company> thisMonthData;
    private TreeMap<Integer,Integer> result;

    public Execution(ArrayList<String> lastMonth,ArrayList<String> thisMonth){
        this.lastMonth=lastMonth;
        this.thisMonth=thisMonth;
        lastMonthData = new HashMap<Integer, Company>();
        thisMonthData = new HashMap<Integer, Company>();
        result = new TreeMap<Integer, Integer>();
        calculation();
    }

    private void calculation(){
        for(int i=0;i<lastMonth.size();i++){
            String[] str = lastMonth.get(i).split(",");
            int tmp = Integer.parseInt(str[0]);
            if(lastMonthData.get(tmp)==null){
                lastMonthData.put(tmp,new Company(tmp));
                lastMonthData.get(tmp).addTrade(Integer.parseInt(str[1]));
            }else{
                lastMonthData.get(tmp).addTrade(Integer.parseInt(str[1]));
            }
        }
        for(int i=0;i<thisMonth.size();i++){
            String[] str = thisMonth.get(i).split(",");
            int tmp = Integer.parseInt(str[0]);
            if(thisMonthData.get(tmp)==null){
                thisMonthData.put(tmp,new Company(tmp));
                thisMonthData.get(tmp).addTrade(Integer.parseInt(str[1]));
            }else{
                thisMonthData.get(tmp).addTrade(Integer.parseInt(str[1]));
            }
        }
        for(Map.Entry<Integer, Company> e : lastMonthData.entrySet()) {
            if(thisMonthData.get(e.getKey())!=null){
                result.put(e.getKey(),e.getValue().tradeNum()+thisMonthData.get(e.getKey()).tradeNum());
            }
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<Integer,Integer> e : result.entrySet()){
            sb.append(e.getKey()+" "+e.getValue()+"\n");
        }
        return sb.toString();
    }


}

class Company{

    private int id;
    ArrayList<Integer> tradeDay;

    public Company(int id){
        this.id=id;
        tradeDay = new ArrayList<Integer>();
    }

    public void addTrade(int day){
        tradeDay.add(day);
    }

    public int tradeNum(){
        return tradeDay.size();
    }


}