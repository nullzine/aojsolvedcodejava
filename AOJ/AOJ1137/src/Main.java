import java.util.Scanner;

/**
 *
 * @author NullZine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        for(int i=0;i<number;i++){
            String str1 = sc.next();
            String str2 = sc.next();
            MCXICodec mcxi =new MCXICodec();
            //System.out.println(mcxi.Decode(str1)+"          "+mcxi.Decode(str2));
            //System.out.println(mcxi.Decode(str1)+mcxi.Decode(str2));
            System.out.println(mcxi.Encode(mcxi.Decode(str1)+mcxi.Decode(str2)));
        }
    }
}

class MCXICodec {

    public int Decode(String argument){
        int answer=0;
        char[] ch = argument.toCharArray();
        for(int i=0;i<ch.length;i++){
            int tmp=0;
            boolean flag = true;
            try{
                tmp=Integer.parseInt(""+ch[i]);
            }catch(NumberFormatException e){
                answer=answer+charDecode(ch[i]);
                flag=false;
            }
            if(flag){
                answer=answer+tmp*charDecode(ch[i+1]);
                i++;
            }
        }
        return answer;
    }

    private int charDecode(char argment){
        if(argment=='m'){
            return 1000;
        }
        if(argment=='c'){
            return 100;
        }
        if(argment=='x'){
            return 10;
        }
        if(argment=='i'){
            return 1;
        }
        else
            return -1;
    }

    public String Encode(int argument){
        String str="";
        while(argument > 0) {
            char c;
            int j;
            if(argument >= 1000) {
                c = 'm';
                j = argument / 1000;
                argument %= 1000;
            }
            else if(argument >= 100) {
                c = 'c';
                j = argument / 100;
                argument %= 100;
            }
            else if(argument >= 10) {
                c = 'x';
                j = argument / 10;
                argument %= 10;
            }
            else {
                c = 'i';
                j = argument / 1;
                argument %= 1;
            }
            if(j > 1)
                str=str+j;
            str=str+c;
        }
        return str;
    }

}