import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String str = sc.nextLine();
            if(str.equals("0")){
                break;
            }
            System.out.println(sum(str));
        }
    }
    private static int sum(String str){
        char[] strc = str.toCharArray();
        int sum = 0;
        for(int i=0;i<strc.length;i++){
            sum+=Integer.parseInt(strc[i]+"");
        }
        return sum;
    }
}