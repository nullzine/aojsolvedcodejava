import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/04.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true) {
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            Mobile[] mobiles = new Mobile[n];
            for(int i=0;i<mobiles.length;i++){
                mobiles[i]=new Mobile(sc.nextLine().split(" "));
            }
            System.out.println(new Execution(mobiles).getAnswer());
        }
    }
}

class Execution{

    private Mobile[] mobiles;
    private long[] weight;

    public Execution(Mobile[] mobiles){
        this.mobiles=mobiles;
        weight = new long[mobiles.length];
        Arrays.fill(weight,-1);
        calculation();
    }

    private void calculation(){
        for(int i=0;i<mobiles.length;i++){
            f(i);
        }
        Arrays.sort(weight);
    }

    private long f(int id) {
        if (0 < weight[id]) {
            return weight[id];
        }

        long le = 1;
        long ri = 1;
        if (mobiles[id].r != 0) {
            le = f(mobiles[id].r - 1);
        }
        if (mobiles[id].b != 0) {
            ri = f(mobiles[id].b - 1);
        }

        long kle = 1;
        long kri = 1;
        while (true) {
            if (le * kle * mobiles[id].p == ri * kri * mobiles[id].q) {
                break;
            } else if (le * kle * mobiles[id].p < ri * kri * mobiles[id].q) {
                kle++;
            } else {
                kri++;
            }
        }
        return weight[id] = le * kle + ri * kri;
    }

    public long getAnswer(){
        return weight[weight.length-1];
    }


}

class Mobile{

    public int p;
    public int q;
    public int r;
    public int b;

    public Mobile(int p,int q,int r,int b){
        this.p=p;
        this.q=q;
        this.r=r;
        this.b=b;
    }

    public Mobile(String[] str){
        p=Integer.parseInt(str[0]);
        q=Integer.parseInt(str[1]);
        r=Integer.parseInt(str[2]);
        b=Integer.parseInt(str[3]);
    }


}