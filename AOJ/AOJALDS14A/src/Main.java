import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int[] ndata = makeIntArray(sc.nextLine().split(" "));
        int q = Integer.parseInt(sc.nextLine());
        int[] qdata = makeIntArray(sc.nextLine().split(" "));
        System.out.println(new Execution(ndata,qdata).getCount());
    }

    private static int[] makeIntArray(String[] strs){
        int[] tmp = new int[strs.length];
        for(int i=0;i<strs.length;i++){
            tmp[i] = Integer.parseInt(strs[i]);
        }
        return tmp;
    }

}

class Execution{

    private HashSet<Integer> hSet = new HashSet<Integer>();
    private int count;

    public Execution(int[] ndata,int[] qdata){
        count=0;
        add(ndata);
        comp(qdata);
    }

    private void add(int[] ndata){
        for(int i=0;i<ndata.length;i++){
            hSet.add(ndata[i]);
        }
    }

    private void comp(int[] qdata){
        for(int i=0;i<qdata.length;i++){
            if(hSet.contains(qdata[i])){
                count++;
            }
        }
    }

    public int getCount(){
        return count;
    }
}