import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/01/01
 * Time: 14:16
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        //selfCheck();
        WeightsPool wp = new WeightsPool(10);
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(wp.toString(Integer.parseInt(sc.nextLine())));
        }
    }

    private static void selfCheck(){
        WeightsPool wp = new WeightsPool(10);
        wp.showWeights();
        for(int i=1;i<=1023;i++){
            if(makeSum(wp.getWeight(i))==i){
                System.out.println("OK:"+i);
            }else{
                System.out.println("ERROR:"+i);
            }
        }
    }

    private static int makeSum(ArrayList<Integer> aList){
        int sum = 0;
        for(int i=0;i<aList.size();i++){
            sum = sum + aList.get(i);
        }
        return sum;
    }
}

class WeightsPool{

    private int[] weight;

    public WeightsPool(int num){
        weight = new int[num];
        makeWeights(num);
    }

    private void makeWeights(int num){
        for(int i=0;i<num;i++){
            weight[i] = (int)Math.pow(2,i);
        }
    }

    public ArrayList<Integer> getWeight(int target){
        ArrayList<Integer> aList = new ArrayList<Integer>();
        for(int i=0;i<weight.length;i++){
            if(weight[weight.length-1-i]<=target){
                aList.add(weight[weight.length-1-i]);
                target = target - weight[weight.length-1-i];
            }
        }
        Collections.sort(aList);
        return aList;
    }

    public String toString(int target){
        ArrayList<Integer> aList = getWeight(target);
        String str = Integer.toString(aList.get(0));
        for(int i=1;i<aList.size();i++){
            str = str + " " + Integer.toString(aList.get(i));
        }
        return str;
    }

    public void showWeights(){
        for(int i=0;i<weight.length;i++){
            System.out.println(weight[i]);
        }
    }

}