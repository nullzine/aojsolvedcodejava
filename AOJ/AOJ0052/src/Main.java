import java.util.*;
import java.math.BigInteger;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int input = sc.nextInt();
            if(input==0){
                break;
            }
            Execution exe = new Execution(input);
            System.out.println(exe.getData());
        }
    }
}

class Execution{

    BigInteger fact = new BigInteger("1");

    public Execution(int number){
        for(int i=1;i<=number;i++){
            fact = fact.multiply(new BigInteger(Integer.toString(i)));
        }
    }

    public int getData(){
        char[] tmp = fact.toString().toCharArray();
        int count=0;
        for(int i=tmp.length-1;0<=i;i--){
            if(Integer.parseInt(tmp[i]+"")!=0){
                break;
            }
            count++;
        }
        return count;
    }
}