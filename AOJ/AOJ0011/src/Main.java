import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/28
 * Time: 8:49
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int w = Integer.parseInt(sc.nextLine());
        int h = Integer.parseInt(sc.nextLine());
        int[] num = new int[w];
        for(int i=0;i<num.length;i++){
            num[i] = i+1;
        }
        for(int i=0;i<h;i++){
            String[] str = sc.nextLine().split(",");
            int from = Integer.parseInt(str[0]);
            int to = Integer.parseInt(str[1]);
            int tmp = num[from - 1];
            num[from - 1] = num[to - 1];
            num[to - 1] = tmp;
        }
        for(int i=0;i<num.length;i++){
            System.out.println(num[i]);
        }
    }
}