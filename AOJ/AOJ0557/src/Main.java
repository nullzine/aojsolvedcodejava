import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/14.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] data = new int[Integer.parseInt(sc.nextLine())];
        String[] strs = sc.nextLine().split(" ");
        for (int i = 0; i < data.length; i++) {
            data[i] = Integer.parseInt(strs[i]);
        }
        System.out.println(new Execution2(data));
    }
}

class Execution2 {

    private int[] data;
    private long dp[][];
    private long pattern;

    public Execution2(int[] data) {
        this.data = data;
        dp = new long[21][100];
        for (int i = 0; i < dp.length; i++) {
            Arrays.fill(dp[i], -1);
        }
        pattern = calculation(data[0], 1, Integer.toString(data[0]));
    }

    private long calculation(int n, int index, String formula) {
        if (n < 0 || 20 < n) {
            return 0;
        }else if(0 <= dp[dp.length - 1 - n][index]){
            return dp[dp.length - 1 - n][index];
        } else if (index == data.length - 1) {
            if (n == data[data.length - 1]) {
                //System.out.println(formula + "=" + n);
                return 1;
            } else {
                return 0;
            }
        } else {
            dp[dp.length - 1 - n][index] = calculation(n + data[index], index + 1, formula + "+" + data[index])
                    + calculation(n - data[index], index + 1, formula + "-" + data[index]);
            return dp[dp.length - 1 - n][index];
        }
    }

    public String toString() {
        return Long.toString(pattern);
    }


}

class Execution {

    private int[] data;
    private int pattern;

    public Execution(int[] data) {
        this.data = data;
        pattern = calculation(data[0], 1, Integer.toString(data[0]));
    }

    private int calculation(int n, int index, String formula) {
        if (n < 0 || 20 < n) {
            return 0;
        } else if (index == data.length - 1) {
            if (n == data[data.length - 1]) {
                //System.out.println(formula+"="+n);
                return 1;
            } else {
                return 0;
            }
        } else {
            return calculation(n + data[index], index + 1, formula + "+" + data[index]) + calculation(n - data[index], index + 1, formula + "-" + data[index]);
        }
    }

    public String toString() {
        return Integer.toString(pattern);
    }


}