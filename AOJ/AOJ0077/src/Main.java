import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            char[] tmp = sc.nextLine().toCharArray();
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<tmp.length;i++){
                if(tmp[i]=='@'){
                    sb.append(convert(tmp[i+1],tmp[i+2]));
                    i=i+2;
                }else
                    sb.append(tmp[i]);
            }
            System.out.println(sb.toString());
        }
    }

    private static String convert(char loop,char word){
        int num = Integer.parseInt(loop+"");
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<num;i++){
            sb.append(word);
        }
        return sb.toString();
    }

}
