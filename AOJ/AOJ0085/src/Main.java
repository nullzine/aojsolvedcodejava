import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by labuser on 14/05/14.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            if(strs[0].equals("0")&&strs[1].equals("0")){
                break;
            }
            System.out.println(new Execution(Integer.parseInt(strs[0]),Integer.parseInt(strs[1])).getResult());
        }
    }
}

class Execution{

    private boolean[] boolList;
    private int m;

    public Execution(int n,int m){
        boolList = new boolean[n];
        Arrays.fill(boolList,true);
        this.m=m;
        //calculation();
        fastCalculation();
    }

    private void fastCalculation(){
        int count;
        int i=0;
        int sum=0;
        while(true){
            count = 0;
            while(true){
                if(boolList[i]){
                    count++;
                }
                if(count==m){
                    break;
                }
                i++;
                if(i==boolList.length){
                    i=0;
                }
            }
            boolList[i] = false;
            sum++;
            if(sum==boolList.length-1){
                break;
            }
        }
    }

    private void calculation(){
        int pointer=boolList.length-1;
        int count=0;
        while(true){
            if(boolList[pointer]){
                count++;
                if(count==m) {
                    boolList[pointer] = false;
                    count=0;
                    //System.out.println(toString());
                }
            }
            pointer++;
            if(boolList.length-1<pointer){
                pointer=0;
            }
            if(validList()){
                break;
            }
        }
    }

    private boolean validList(){
        int count=0;
        for(int i=0;i<boolList.length;i++){
            if(boolList[i]){
                count++;
            }
        }
        return count==1;
    }

    public int getResult(){
        for(int i=0;i<boolList.length;i++){
            if(boolList[i]){
                return i+1;
            }
        }
        return -1;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<boolList.length;i++){
            if(boolList[i]){
                sb.append("#");
            }else{
                sb.append(".");
            }
        }
        return sb.toString();
    }

}