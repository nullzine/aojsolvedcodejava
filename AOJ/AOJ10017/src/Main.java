import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2013/12/20
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            if(str[0].equals("0")&&str[1].equals("0")){
                break;
            }else{
                Calculation calc = new Calculation(Integer.parseInt(str[0]),Integer.parseInt(str[1]));
                System.out.println(calc.getCombination());
            }
        }
    }
}

class Calculation{

    private ArrayList<String> dataSet;

    public Calculation(int n,int x){
        dataSet = new ArrayList<String>();
        makeSet(n,x);
    }

    private void makeSet(int n,int x){
        for (int i = 1; i <= n - 2; i++) {
            for (int j = i + 1; j <= n - 1; j++) {
                for (int k = j + 1; k <= n; k++) {
                    if (i + j + k == x) {
                        dataSet.add(i+"+"+j+"+"+k);
                    }
                }
            }
        }
    }

    public int getCombination(){
        return dataSet.size();
    }

}