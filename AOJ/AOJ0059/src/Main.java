import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/12.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            System.out.println(new Execution(Double.parseDouble(strs[0]),Double.parseDouble(strs[1]),Double.parseDouble(strs[2]),Double.parseDouble(strs[3]),
                    Double.parseDouble(strs[4]),Double.parseDouble(strs[5]),Double.parseDouble(strs[6]),Double.parseDouble(strs[7])));
        }
    }
}

class Execution{

    private String result;

    public Execution(double xa1,double ya1,double xa2,double ya2,double xb1,double yb1,double xb2,double yb2){
        if(xa2<xb1||ya2<yb1||xb2<xa1||yb2<ya1){
            result="NO";
        }else{
            result="YES";
        }
    }

    public String toString(){
        return result;
    }


}