/**
 * Created by nullzine on 2015/01/30.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(new FlickKeyboard().input(new java.util.Scanner(System.in).nextLine()));
    }
}

class FlickKeyboard {

    private Key[] keys;

    public FlickKeyboard(){
        String[] strs = makeCSV().split("\n");
        keys = new Key[strs.length];
        for(int i=0;i<keys.length;i++){
            keys[i] = new Key(strs[i]);
        }
    }

    public String input(String str){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i*2<str.length();i++){
            int buttonIndex = Integer.parseInt(str.charAt(i*2)+"")-1;
            buttonIndex+=buttonIndex==-1?10:0;
            sb.append(keys[buttonIndex].get(str.charAt(i*2+1)));
        }
        return sb.toString().toLowerCase();
    }

    private String makeCSV() {
        String str = "あ,A,い,I,う,U,え,E,お,O\n" +
                "か,KA,き,KI,く,KU,け,KE,こ,KO\n" +
                "さ,SA,し,SI,す,SU,せ,SE,そ,SO\n" +
                "た,TA,ち,TI,つ,TU,て,TE,と,TO\n" +
                "な,NA,に,NI,ぬ,NU,ね,NE,の,NO\n" +
                "は,HA,ひ,HI,ふ,HU,へ,HE,ほ,HO\n" +
                "ま,MA,み,MI,む,MU,め,ME,も,MO\n" +
                "や,YA,ゆ,YU,よ,YO,,,,\n" +
                "ら,RA,り,RI,る,RU,れ,RE,ろ,RO\n" +
                "わ,WA,を,WO,ん,NN,,,,";
        return str;
    }


}

class Key{

    private CharData[] charDatas;

    public Key(String str){
        charDatas = makeCharData(str);
    }

    private CharData[] makeCharData(String str){
        String[] strs = str.split(",");
        CharData[] ret = new CharData[strs.length/2];
        for(int i=0;i<ret.length;i++){
            ret[i] = new CharData(strs[i*2].toCharArray()[0],strs[i*2+1]);
        }
        return ret;
    }

    public String get(char direction){
        if(charDatas[0].getJap()=='や'){
            return direction=='T'?charDatas[0].getRoma():(direction=='U'?charDatas[1].getRoma():charDatas[2].getRoma());
        }else if(charDatas[0].getJap()=='わ'){
            return direction=='T'?charDatas[0].getRoma():(direction=='U'?charDatas[2].getRoma():charDatas[1].getRoma());
        }else{
            switch (direction){
                case 'T':
                    return charDatas[0].getRoma();
                case 'L':
                    return charDatas[1].getRoma();
                case 'U':
                    return charDatas[2].getRoma();
                case 'R':
                    return charDatas[3].getRoma();
                case 'D':
                    return charDatas[4].getRoma();
            }
        }
        return "ERROR";
    }

}

class CharData{

    private char jap;
    private String roma;

    public CharData(char jap,String roma){
        this.jap=jap;
        this.roma=roma;
    }

    public char getJap(){
        return jap;
    }

    public String getRoma(){
        return roma;
    }

}