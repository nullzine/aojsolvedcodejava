import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/04/01
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            System.out.println(new Execution(convDouble(sc.nextLine().split(" "))).getResult());
        }
    }

    private static double[] convDouble(String[] strs){
        double[] tmp = new double[strs.length];
        for(int i=0;i<strs.length;i++){
            tmp[i]=Double.parseDouble(strs[i]);
        }
        return tmp;
    }

}

class Execution{

    private double[] data;
    private int result;

    public Execution(double[] data){
        this.data=data;
        result=new Circle(data[0],data[1],data[2]).compareTo(new Circle(data[3],data[4],data[5]));
    }

    public int getResult(){
        return result;
    }

}

class Circle{

    private double x;
    private double y;
    private double r;

    public Circle(double x,double y,double r){
        this.x=x;
        this.y=y;
        this.r=r;
    }

    public int compareTo(Circle cir){
        double distance = Math.sqrt((x - cir.getX()) * (x - cir.getX()) + (y - cir.getY()) * (y - cir.getY()));
        if(r+cir.getR()<distance){
            return 0;
        }else if(r + cir.getR() > distance && distance + Math.min(r, cir.getR()) < Math.max(r, cir.getR())){
            if(r<cir.getR()){
                return -2;
            }else{
                return 2;
            }
        }else{
            return 1;
        }
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double getR(){
        return r;
    }

}