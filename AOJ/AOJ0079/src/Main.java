import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/06/27.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution();
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(",");
            exe.add(Double.parseDouble(strs[0]),Double.parseDouble(strs[1]));
        }
        System.out.println(exe.getArea());
    }
}

class Execution{

    private ArrayList<Point> points;

    public Execution(){
        points = new ArrayList<Point>();
    }

    public void add(double x,double y){
        points.add(new Point(x,y));
    }

    public double getArea(){
        double s = 0;
        for(int i=2;i<points.size();i++){
            s+=s(points.get(0).getDistance(points.get(i-1)),points.get(i-1).getDistance(points.get(i)),points.get(0).getDistance(points.get(i)));
        }
        return s;
    }

    private double s(double a,double b,double c){
        double z = z(a,b,c);
        return Math.sqrt(z*(z-a)*(z-b)*(z-c));
    }

    private double z(double a,double b,double c){
        return (a+b+c)/2;
    }

}

class Point{

    private double x;
    private double y;

    public Point(double x,double y){
        this.x=x;
        this.y=y;
    }

    public double getDistance(Point point){
        return Math.sqrt(Math.pow(point.getX()-x,2)+Math.pow(point.getY()-y,2));
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }


}