import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/30
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Execution(sc.nextLine()).getResult());
        }
    }
}

class Execution{

    private double[] x;
    private double[] y;
    private double xp,yp;
    private String result;

    public Execution(String str){
        x = new double[3];
        y = new double[3];
        String[] tmp = str.split(" ");
        x[0]=Double.parseDouble(tmp[0]);
        y[0]=Double.parseDouble(tmp[1]);
        x[1]=Double.parseDouble(tmp[2]);
        y[1]=Double.parseDouble(tmp[3]);
        x[2]=Double.parseDouble(tmp[4]);
        y[2]=Double.parseDouble(tmp[5]);
        xp=Double.parseDouble(tmp[6]);
        yp=Double.parseDouble(tmp[7]);
        calculation();
    }

    private void calculation(){
        if(new Triangle(x,y,3).contains(new Point2D.Double(xp,yp))){
            result="YES";
        }else{
            result="NO";
        }
    }

    public String getResult(){
        return result;
    }

}

class Triangle{

    private Path2D.Double tri;

    public Triangle(double[] xpoints,double[] ypoints,int n){
        tri = new Path2D.Double();
        tri.moveTo(xpoints[0],ypoints[0]);
        tri.lineTo(xpoints[1],ypoints[1]);
        tri.lineTo(xpoints[2],ypoints[2]);
    }

    public boolean contains(Point2D.Double pt){
        return tri.contains(pt);
    }

}