import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/19.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            String[] strs = sc.nextLine().split(" ");
            int a = Integer.parseInt(strs[0]);
            int b = Integer.parseInt(strs[1]);
            int c = Integer.parseInt(strs[2]);
            if (a == 0 && b == 0 && c == 0) {
                break;
            }
            System.out.println(new Execution(a, b, c));
        }
    }
}

class Execution {

    private int a;
    private int b;
    private int c;
    private String result;

    public Execution(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
        result = calcuation();
    }

    private String calcuation() {
        double D = b * b - 4 * a * c;
        if (D < 0) {
            return "Impossible";
        } else if (D == 0) {
            int A = 2 * a;
            int B = -b;
            long g = gcd(Math.abs(A), Math.abs(B));
            A /= g;
            B /= g;
            return A + " " + (-B) + " " + A + " " + (-B);
        } else {
            int k;
            for(k=1;k * k <= D;k++){
                if (k * k == D){
                    break;
                }
            }
            if (k * k > D) {
                return "Impossible";
            }
            int P = 2 * a;
            int Q = -b + k;
            int R = 2 * a;
            int S = -b - k;
            long g = gcd(Math.abs(P), Math.abs(Q));
            P /= g;
            Q /= g;
            g = gcd(Math.abs(R), Math.abs(S));
            R /= g;
            S /= g;
            Q = -Q;
            S = -S;
            if (P > R || (P == R && Q > S)) {
                return P + " " + Q + " " + R + " " + S;
            } else {
                return R + " " + S + " " + P + " " + Q;
            }
        }
    }

    private long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public String toString(){
        return result;
    }


}