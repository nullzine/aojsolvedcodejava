import java.util.*;

/**
 *
 * @author NullZine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Mat mt = new Mat(100000);
        while(true){
            int number = sc.nextInt();
            if(number==0)
                break;
            System.out.println(mt.Goldbach(number));
        }
    }
}

class Mat {
    public boolean[] boolList;    //指定された値が素数かどうかの一覧
    public ArrayList<Integer> primeList = new ArrayList<Integer>(); //素数の一覧 可変長配列


    Mat(int primelimit){
        boolList = new boolean[primelimit];
        makeList(primelimit);
        //primelimitは発見される素数の最大値である。
    }


    void debugArray(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(boolList[i]);
        }
        Iterator iter = primeList.iterator();
        while(iter.hasNext()) {
            Object tmp = iter.next();
            System.out.println(tmp);
        }
    }

    private void makeList(int limit){
        Arrays.fill(boolList,true);
        boolList[0]=false;
        boolList[1]=false;
        for (int i=2; i<limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j=i+i; j<limit; j+=i)
                    boolList[j] = false;
            }
        }
    }

    public int SoCPN(int N){
        //連続する素数の和が引数の数になる組み合わせは何通りあるか返します
        int limit=primeList.size();
        int res = 0;
        for(int i=0;i<limit;i++){
            int sum = primeList.get(i);
            int j = i+1;
            while(sum<N&&j<limit){
                sum=sum+primeList.get(j++);
            }
            if(sum==N)
                res++;
        }
        return res;
    }

    public int Goldbach(int n){
        int res = 0;
        for(int i=2;i<=n;i++){
            if(n-i < i)
                break;
            if(boolList[i] && boolList[n-i])
                res++;
        }
        return res;
    }


}