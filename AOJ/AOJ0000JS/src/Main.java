/**
 * Created by nullzine on 2014/04/22.
 */
public class Main {
    public static void main(String[] args) {
        String script = "QQ(9,9);\n" +
                "\n" +
                "function QQ(row,column){\n" +
                "    for(var i=1;i<10;i++){\n" +
                "        for(var j=1;j<10;j++){\n" +
                "            sample.output(i+\"x\"+j+\"=\"+(i*j));\n" +
                "        }\n" +
                "    }\n" +
                "}";
        new ScriptStracture().doScript(script);
        /*
        new ScriptStracture().doScript("sample.output('bar');\n"
        +"sample.output(sample.next());");
        */
    }

}