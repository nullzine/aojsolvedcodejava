import java.io.StringReader;
import java.util.ArrayList;
import java.util.Scanner;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptStracture {

    private StringReader reader;
    private ScriptEngineManager manager;
    private ScriptEngine engine;
    private ArrayList<String> inputPool;
    private int pointer=0;

    public ScriptStracture() {
        manager = new ScriptEngineManager();
        engine = manager.getEngineByName("js");
        inputPool =new ArrayList<String>();
        //inputPool.add("Hello JS!");
        //suckInput();
    }

    private void suckInput(){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            inputPool.add(sc.nextLine());
        }
    }

    public void output(String msg) {
        System.out.println("" + msg);
    }

    public String next(){
        pointer++;
        return inputPool.get(pointer-1);
    }

    public String debug(){
        return "OK";
    }

    public String hasNext(){
        if(pointer<inputPool.size()){
            return "TRUE";
        }else{
            return "FALSE";
        }
    }

    public void doScript(String script){
        reader = new StringReader(script);
        if (engine != null) {
            Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
            bindings.put("sample", this);

            try {
                engine.eval(reader);
            } catch (ScriptException ex) {
                ex.printStackTrace();
            }
        }
        reader.close();
    }

}