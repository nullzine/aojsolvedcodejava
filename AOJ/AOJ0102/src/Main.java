import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/02/04
 * Time: 12:53
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = Integer.parseInt(sc.nextLine());
            if(n==0){
                break;
            }
            int[][] dataSet = new int[n][n];
            for(int i=0;i<n;i++){
                String[] tmp = sc.nextLine().split(" ");
                for(int j=0;j<n;j++){
                    dataSet[i][j] = Integer.parseInt(tmp[j]);
                }
            }
            System.out.print(new Execution(dataSet).toString());
        }
    }
}

class Execution{

    private int[][] data;

    public Execution(int[][] dataSet){
        data = new int[dataSet.length+1][dataSet.length+1];
        calculation(dataSet);
    }

    private void calculation(int[][] dataSet){
        for(int i=0;i<dataSet.length;i++){
            int sum = 0;
            for(int j=0;j<dataSet.length;j++){
                sum+=dataSet[i][j];
                data[data.length-1][j]+=dataSet[i][j];
                data[i][j]=dataSet[i][j];
            }
            data[i][data.length-1]=sum;
        }
        for(int i=0;i<data.length-1;i++){
            data[data.length-1][data.length-1]+=data[data.length-1][i];
        }
    }

    public String toString(){
        String str = "";
        for(int i=0;i<data.length;i++){
            for(int j=0;j<data.length;j++){
                str=str+complementString(Integer.toString(data[i][j]),5);
            }
            str=str+getCRLF();
        }
        return str;
    }

    private String complementString(String str,int n){
        for(int i=str.length();i<n;i++){
            str = " "+str;
        }
        return str;
    }

    private String getCRLF() {
        String str = "\n";
        try {
            str = System.getProperty("line.separator");
        } catch (SecurityException e) {
        }
        return str;
    }

}

/*
   52   96   15   20  183
   86   22   35   45  188
   45   78   54   36  213
   16   86   74   55  231
  199  282  178  156  815
   52   96   15   20  183
   86   22   35   45  188
   45   78   54   36  213
   16   86   74   55  231
  199  282  178  156  815
*/