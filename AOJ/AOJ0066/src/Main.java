import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/17.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            System.out.println(new Execution(sc.nextLine()));
        }
    }
}

class Execution{

    private char[] strc;
    private String result;

    public Execution(String str){
        strc=str.toCharArray();
        calculation();
    }

    private void calculation(){
        if(validation('o')){
            result = "o";
        }else if(validation('x')){
            result = "x";
        }else{
            result = "d";
        }
    }

    private boolean validation(char c){
        return (strc[0]==c&&strc[1]==c&&strc[2]==c
                ||strc[3]==c&&strc[4]==c&&strc[5]==c
                ||strc[6]==c&&strc[7]==c&&strc[8]==c
                ||strc[0]==c&&strc[3]==c&&strc[6]==c
                ||strc[1]==c&&strc[4]==c&&strc[7]==c
                ||strc[2]==c&&strc[5]==c&&strc[8]==c
                ||strc[0]==c&&strc[4]==c&&strc[8]==c
                ||strc[2]==c&&strc[4]==c&&strc[6]==c);
    }

    public String toString(){
        return result;
    }

}

/*

012
345
678

 */