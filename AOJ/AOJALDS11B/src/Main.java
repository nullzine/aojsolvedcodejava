import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] strs = sc.nextLine().split(" ");
            int x = Integer.parseInt(strs[0]);
            int y = Integer.parseInt(strs[1]);
            if(y<x){
                x=x%y;
            }else if(x<y){
                y=y%x;
            }
            int tmp = 1;
            for(int i=2;i<=Math.min(x,y);i++){
                if(x%i==0&&y%i==0){
                    tmp = i;
                }
            }
            System.out.println(tmp);
        }
    }
}
