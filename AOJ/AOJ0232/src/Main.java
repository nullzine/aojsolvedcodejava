import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/09/14.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true) {
            String[] strs = sc.nextLine().split(" ");
            if(strs[0].equals("0")&&strs[1].equals("0")&&strs[2].equals("0")){
                break;
            }
            Game2 game = new Game2(Integer.parseInt(strs[0]),Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
            game.setRoulette(sc.nextLine().split(" "));
            for(int i=0;i<Integer.parseInt(strs[2]);i++){
                String[] ss = sc.nextLine().split(" ");
                game.setCell(Integer.parseInt(ss[0]),Integer.parseInt(ss[1]),Integer.parseInt(ss[2]));
            }
            //System.out.println(game);
            System.out.println(game.getMean());
        }

    }
}

class Game{

    private int X;
    private int Y;
    private int Z;
    private int[] data;
    private Cell[] cells;
    private double dp[][];

    public Game(int X,int Y,int Z){
        this.X=X;
        this.Y=Y;
        this.Z=Z;
        dp = new double[5001][100];
        for(int i=0;i<dp.length;i++){
            Arrays.fill(dp[i],-1);
        }
        cells = new Cell[51];
        for(int i=0;i<cells.length;i++){
            cells[i] = new Cell(0,0,0);
        }
    }

    private double calculation(double money,int state,int depth){
        if(0<=dp[(int)money][state]){
            return dp[(int)money][state];
        }
        if(Y<=state){
            money*=1.0/Math.pow(data.length,depth);
            //System.out.println("!!"+money+" state:"+state+" depth:"+depth);
            return money;
        }else{
            if(cells[state]!=null){
                switch (cells[state].getE()){
                    case 1:
                        state+=cells[state].getA();
                        if(Y<state){
                            state=Y;
                        }
                        break;
                    case 2:
                        money+=cells[state].getA();
                        break;
                    case 3:
                        money-=cells[state].getA();
                        if(money<0){
                            money=0;
                        }
                        break;
                }
            }
            double s=0;
            for(int i=0;i<data.length;i++){
                s+=calculation(money,state+data[i],depth+1);
            }
            dp[(int)money][state]=s;
            return s;
        }
    }

    private double calculation2(){
        dp = new double[Y+1][5001];
        for(int i=0;i<Y+1;i++){
            for(int j=0;j<5001;j++){
                if(0<dp[i][j]){
                    for(int k=0;k<X;k++){
                        if(i!=Y) {
                            int num = i + data[k];
                            if (num > Y) {
                                num = Y;
                            }
                            int money = j;
                            switch (cells[num].getE()) {
                                case 1:
                                    num += cells[num].getA();
                                    if (Y < num) {
                                        num = Y;
                                    }
                                    break;
                                case 2:
                                    money = j + cells[num].getA();
                                    break;
                                case 3:
                                    money = j - cells[num].getA();
                                    if (money < 0) {
                                        money = 0;
                                    }
                                    break;
                            }
                            dp[num][money] = dp[i][j] / X;
                        }
                    }
                }
            }
        }
        double ans = 0.0;
        for(int i=0;i<5001;i++){
            ans+=dp[Y][i]*i;
        }
        return ans;
    }

    public void setRoulette(String[] data){
        int[] tmp = new int[data.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=Integer.parseInt(data[i]);
        }
        this.data=tmp;
    }

    public void setRoulette(int[] data){
        this.data=data;
    }

    public void setCell(int N,int E,int A){
        cells[N]=new Cell(N,E,A);
    }

    public String toString(){
        return X+" "+Y+" "+Z+" roulette{"+dataStr(data)+"} cell{"+cellStr()+"}";
    }

    private String dataStr(int[] arr){
        StringBuilder sb = new StringBuilder();
        for(int n:arr){
            sb.append(n+" ");
        }
        return sb.toString();
    }

    private String cellStr(){
        StringBuilder sb = new StringBuilder();
        for(Cell c:cells){
            sb.append(c+" ");
        }
        return sb.toString();
    }

    public int getMean(){
        //return (int)calculation(0,0,0);
        return (int)calculation2();
    }

}

class Cell{

    private int N;
    private int E;
    private int A;

    public Cell(int N,int E,int A){
        this.N=N;
        this.E=E;
        this.A=A;
    }

    public int getE(){
        return E;
    }

    public int getA(){
        return A;
    }

    public String toString(){
        return "["+N+","+E+","+A+"]";
    }

}

class Game2{

    private int X;
    private int Y;
    private int Z;
    private int[] data;
    private int[][] cells;
    private double dp[][];

    public Game2(int X,int Y,int Z) {
        this.X = X;
        this.Y = Y;
        this.Z = Z;
        dp = new double[5001][100];
        for (int i = 0; i < dp.length; i++) {
            Arrays.fill(dp[i], -1);
        }
        cells = new int[Y+1][2];
    }

    private double calculation2(){
        dp = new double[Y+1][5001];
        dp[0][0]=1;

        for(int i=0;i<Y+1;i++){
            for(int j=0;j<5001;j++){
                if(0<dp[i][j]){
                    for(int k=0;k<X;k++){
                        if(i!=Y) {
                            int num = i + data[k];
                            if (num > Y) {
                                num = Y;
                            }
                            int money = j;
                            switch (cells[num][0]) {
                                case 1:
                                    num += cells[num][1];
                                    if (Y < num) {
                                        num = Y;
                                    }
                                    break;
                                case 2:
                                    money = j + cells[num][1];
                                    break;
                                case 3:
                                    money = j - cells[num][1];
                                    if (money < 0) {
                                        money = 0;
                                    }
                                    break;
                            }
                            dp[num][money] += dp[i][j] / X;
                        }
                    }
                }
            }
        }
        double ans = 0.0;
        for(int i=0;i<5001;i++){
            ans+=dp[Y][i]*i;
        }
        return ans;
    }

    public void setRoulette(String[] data){
        int[] tmp = new int[data.length];
        for(int i=0;i<tmp.length;i++){
            tmp[i]=Integer.parseInt(data[i]);
        }
        this.data=tmp;
    }

    public void setRoulette(int[] data){
        this.data=data;
    }

    public void setCell(int N,int E,int A){
        cells[N][0]=E;
        cells[N][1]=A;
    }

    public String toString(){
        return X+" "+Y+" "+Z+" roulette{"+dataStr(data)+"} cell{"+cellStr()+"}";
    }

    private String dataStr(int[] arr){
        StringBuilder sb = new StringBuilder();
        for(int n:arr){
            sb.append(n+" ");
        }
        return sb.toString();
    }

    private String cellStr(){
        StringBuilder sb = new StringBuilder();
        for(int[] c:cells){
            sb.append(c[0]+","+c[1]+" ");
        }
        return sb.toString();
    }

    public int getMean(){
        //return (int)calculation(0,0,0);
        return (int)calculation2();
    }

}