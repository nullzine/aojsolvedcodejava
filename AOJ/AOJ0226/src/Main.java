import java.util.Scanner;

/**
 * Created by nullzine on 2014/05/21.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] strs = sc.nextLine().split(" ");
            if(strs[0].equals("0")&&strs[1].equals("0")){
                break;
            }else{
                System.out.println(new Execution(strs[0],strs[1]));
            }
        }
    }
}

class Execution{

    private char[] a;
    private char[] b;
    private int hit;
    private int blow;

    public Execution(String a,String b){
        this.a=a.toCharArray();
        this.b=b.toCharArray();
        hit=countHit();
        blow=countBlow();
    }

    private int countHit(){
        int count=0;
        for(int i=0;i<a.length;i++){
            if(a[i]==b[i]){
                count++;
            }
        }
        return count;
    }

    private int countBlow(){
        int count=0;
        for(int i=0;i<a.length;i++){
            for(int j=0;j<b.length;j++){
                if(i!=j&&a[i]==b[j]){
                    count++;
                }
            }
        }
        return count;
    }

    public String toString(){
        return hit+" "+blow;
    }

}