import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            String[] str = sc.nextLine().split(" ");
            int m = Integer.parseInt(str[0]);
            int f = Integer.parseInt(str[1]);
            int r = Integer.parseInt(str[2]);
            if(m==-1&&f==-1&&r==-1){
                break;
            }
            System.out.println(validGrade(m,f,r));
        }
    }

    private static String validGrade(int m,int f,int r){
        if(m==-1||f==-1){
            return "F";
        }else if(80<=m+f){
            return "A";
        }else if(65<=m+f&&m+f<80){
            return "B";
        }else if(50<=m+f&&m+f<65){
            return "C";
        }else if(30<=m+f&&m+f<50){
            if(50<=r){
                return "C";
            }else{
                return "D";
            }
        }else if(m+f<30){
            return "F";
        }else{
            return "ERROR";
        }
    }
}