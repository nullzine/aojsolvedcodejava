/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package pkg0005;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author NullZine
 */
public class Main {

    protected static int gcd(int m, int n) {
        // if m or n is 0:::::return 0;
        if ((0 == m) || (0 == n)) {
            return 0;
        }
        //euculid
        while (m != n) {
            if (m > n) {
                m = m - n;
            } else {
                n = n - m;
            }
        }
        return m;
    }

    protected static long lcm(int m, int n) {
        long tmp;
        //if m or n is 0:::::return 0;
        if ((0 == m) || (0 == n)) {
            return 0;
        }
        tmp = ((m / gcd(m, n)) * n);
        return tmp;
    }
    public static void main(String[] args) throws IOException {
        int a[] = new int[1000];
        int b[] = new int[1000];
        int c[] = new int[1000];
        int count,i=0;
        long d[] = new long[1000];

        BufferedReader br=
                new BufferedReader (new InputStreamReader(System.in));
        String str;

        while(true){
            if((str=br.readLine())==null)break;
            if(str.length()==0)break;
            String[] editedstr = str.split(" ");
            a[i] = Integer.parseInt(editedstr[0]);
            b[i] = Integer.parseInt(editedstr[1]);
            i++;
        }

        count = i;

        for (i = 0; i < count; i++) {
            c[i]=gcd(a[i],b[i]);
            d[i]=lcm(a[i],b[i]);
            System.out.println(c[i]+" "+d[i]);
        }
    }
}