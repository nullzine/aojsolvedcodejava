import java.util.*;

public class Main{
    public static void main(String[] args){
        System.out.println((int)calculation(Integer.parseInt(new Scanner(System.in).nextLine()),100000));
    }

    private static double calculation(int n,int money){
        double m = (double)money;
        for(int i=0;i<n;i++){
            m=m*1.05;
            if(m%1000!=0.0){
                m=((int)(m/1000))*1000+1000;
            }
        }
        return m;
    }

}