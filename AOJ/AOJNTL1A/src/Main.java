//import java.util.ArrayList;
//import java.util.Collections;
import java.util.Scanner;
//import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/02/28
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            System.out.println(new Factorization(Integer.parseInt(sc.nextLine())).toString());
        }
    }
}

class Factorization{

    private String str;

    public Factorization(long num){
        str = Long.toString(num)+": ";
        calculation(num);
    }

    private void calculation(long num){
        if(isPrime(num)){
            str=str+Long.toString(num);
        }else{
            while(true){
                if(num<=1){
                    break;
                }else{
                    for(long i=2; i<=num; i++){
                        if(i==num){
                            str=str+Long.toString(num);
                            num=1;
                        }
                        else if(num%i==0){
                            str=str+Long.toString(i)+" ";
                            num/=i;
                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean isPrime(long n){
        for(long i=2;i*i<=n;i++){
            if(n%i==0){
                return false;
            }
        }
        return true;
    }

    public String toString(){
        return str;
    }

}

/*
class Factorization{

    private int n;
    private ArrayList<Integer> aList;
    private String str;
    private SimplePrime sp;

    public Factorization(int n){
        this.n=n;
        aList = new ArrayList<Integer>();
        str = Integer.toString(n)+":";
        sp = new SimplePrime(100000);
        calculation();
    }

    private void calculation(){
        for(int i=0;true;i++){
            int tmp = sp.getList().get(i);
            if(n%tmp==0){
                aList.add(tmp);
                n=n/tmp;
                i--;
            }
            if(n<tmp){
                if(n!=1){
                    aList.add(n);
                }
                break;
            }
        }
        Collections.sort(aList);
    }

    public String toString(){
        for(int i=0;i<aList.size();i++){
            str=str+" "+aList.get(i);
        }
        return str;
    }

}

class SimplePrime {

    private boolean[] boolList;
    private ArrayList<Integer> primeList = new ArrayList<Integer>();

    public SimplePrime(int limit){
        limit++;
        boolList = new boolean[limit];
        makeBool(limit);
        //showBool();
    }

    public ArrayList<Integer> getList(){
        return primeList;
    }

    public boolean[] getBool(){
        return boolList;
    }

    private void showBool(){
        for(int i=0;i<boolList.length;i++){
            System.out.println(i+":"+boolList[i]);
        }
    }

    private void makeBool(int limit) {
        Arrays.fill(boolList, true);
        boolList[0]=false;
        boolList[1]=false;
        for (int i = 2; i < limit; i++) {
            if (boolList[i]) {
                primeList.add(i);
                for (int j = i + i; j < limit; j += i) {
                    boolList[j] = false;
                }
            }
        }
    }

}
*/