import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: nullzine
 * Date: 2014/03/02
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            System.out.println("Case "+(i+1)+":");
            new Execution(Integer.parseInt(sc.nextLine()),10,4).print();
        }
    }
}

class Execution{

    private int[] rand;
    private int n;

    public Execution(int seed,int limit,int n){
        rand = new int[limit];
        this.n=n;
        calculation(seed);
    }

    private void calculation(int seed){
        rand[0]=processing(seed);
        for(int i=1;i<rand.length;i++){
            rand[i]=processing(rand[i-1]);
        }
    }

    private int processing(int num){
        String str = Integer.toString((int)Math.pow(num,2));
        for(int i=str.length();i<n*2;i++){
            str="0"+str;
        }
        return Integer.parseInt(str.substring(str.length()/2-n/2,str.length()/2-n/2+n));
    }

    public void print(){
        for(int i=0;i<rand.length;i++){
            System.out.println(rand[i]);
        }
    }

}