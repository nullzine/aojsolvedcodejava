import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/10/17.
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strs = sc.nextLine().split(" ");
        Fish[] fishs = new Fish[Integer.parseInt(strs[0])];
        int k = Integer.parseInt(strs[1]);
        for (int i = 0; i < fishs.length; i++) {
            fishs[i] = new Fish(sc.nextLine());
        }
        System.out.println(new Execution(k,fishs).getResult());
    }
}

class Execution {

    private int k;
    private Fish[] fishs;
    private ArrayList<Long> xs;
    private ArrayList<Long> ys;
    private ArrayList<Long> zs;
    private int[][][] compressedRect;
    private long result;

    public Execution(int k, Fish[] fishs) {
        this.k = k;
        this.fishs=fishs;
        xs = new ArrayList<Long>();
        ys = new ArrayList<Long>();
        zs = new ArrayList<Long>();
        init();
        compressedRect = new int[ xs.size() ][ ys.size() ][ zs.size() ];
        result=calculation();
    }

    private void init() {
        for (int i = 0; i < fishs.length; i++) {
            if (!xs.contains(fishs[i].getA().getX())) {
                xs.add(fishs[i].getA().getX());
            }
            if (!xs.contains(fishs[i].getB().getX())) {
                xs.add(fishs[i].getB().getX());
            }
            if (!ys.contains(fishs[i].getA().getY())) {
                ys.add(fishs[i].getA().getY());
            }
            if (!ys.contains(fishs[i].getB().getY())) {
                ys.add(fishs[i].getB().getY());
            }
            if (!zs.contains(fishs[i].getA().getZ())) {
                zs.add(fishs[i].getA().getZ());
            }
            if (!zs.contains(fishs[i].getB().getZ())) {
                zs.add(fishs[i].getB().getZ());
            }
        }
        Collections.sort(xs);
        Collections.sort(ys);
        Collections.sort(zs);
    }

    private long calculation() {
        for ( int i = 0; i < fishs.length; i++ ) {
            int fromX = Collections.binarySearch( xs, fishs[i].getA().getX() );
            int toX = Collections.binarySearch( xs, fishs[i].getB().getX() );
            int fromY = Collections.binarySearch(ys, fishs[i].getA().getY());
            int toY = Collections.binarySearch( ys, fishs[i].getB().getY() );
            int fromZ = Collections.binarySearch( zs, fishs[i].getA().getZ() );
            int toZ = Collections.binarySearch( zs, fishs[i].getB().getZ() );

            for ( int x = fromX; x < toX; x++ ) {
                for ( int y = fromY; y < toY; y++ ) {
                    for ( int z = fromZ; z < toZ; z++ ) {
                        compressedRect[ x ][ y ][ z ]++;
                    }
                }
            }
        }

        long volume = 0L;
        for ( int x = 0; x < compressedRect.length - 1; x++ ) {
            for ( int y = 0; y < compressedRect[ x ].length - 1; y++ ) {
                for ( int z = 0; z < compressedRect[ x ][ y ].length - 1; z++ ) {
                    if ( compressedRect[ x ][ y ][ z ] >= k ) {
                        volume += ( xs.get( x + 1 ) - xs.get( x ) ) *( ys.get( y + 1 ) - ys.get( y ) ) *( zs.get( z + 1 ) - zs.get( z ) );
                    }
                }
            }
        }
        return volume;
    }

    public long getResult(){
        return result;
    }

}

class Fish {

    private Point3D a;
    private Point3D b;

    public Fish(long x1, long y1, long d1, long x2, long y2, long d2) {
        a = new Point3D(x1, y1, d1);
        b = new Point3D(x2, y2, d2);
    }

    public Fish(String str) {
        String[] tmp = str.split(" ");
        a = new Point3D(Long.parseLong(tmp[0]), Long.parseLong(tmp[1]), Long.parseLong(tmp[2]));
        b = new Point3D(Long.parseLong(tmp[3]), Long.parseLong(tmp[4]), Long.parseLong(tmp[5]));
    }

    public Point3D getA() {
        return a;
    }

    public Point3D getB() {
        return b;
    }

}

class Point3D {

    private long x;
    private long y;
    private long z;

    public Point3D(long x, long y, long z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    public long getZ() {
        return z;
    }

}