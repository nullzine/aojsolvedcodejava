import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            System.out.println(v(sc.nextLine().split(" "))?"YES":"NO");
        }
    }

    private static double a(String r){
        return Double.parseDouble(r);
    }

    private static boolean v(String[] s){
        double ax, ay, bx, by, cx, cy, dx, dy;
        ax = a(s[0]);
        ay = a(s[1]);
        bx = a(s[2]);
        by = a(s[3]);
        cx = a(s[4]);
        cy = a(s[5]);
        dx = a(s[6]);
        dy = a(s[7]);
        if (ax == bx && cy == dy) {
            return true;
        } else if (ay == by && cx == dx) {
            return true;
        } else if ((ay - by) * (cy - dy) * -1 == (ax - bx) * (cx - dx)) {
            return true;
        } else {
            return false;
        }
    }

}