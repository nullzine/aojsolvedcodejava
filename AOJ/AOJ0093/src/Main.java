import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by nullzine on 2014/04/17.
 */
public class Main {
    public static void main(String[] args){
        Execution exe = new Execution();
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        while(true){
            String[] strs = sc.nextLine().split(" ");
            int a = Integer.parseInt(strs[0]);
            int b = Integer.parseInt(strs[1]);
            if(a==0&&b==0){
                break;
            }
            if(flag){
                flag = false;
            }else{
                System.out.println();
            }
            printList(exe.getLeapYearList(a,b));
        }
    }
    private static void printList(ArrayList<Integer> aList){
        if(aList.size()==0){
            System.out.println("NA");
        }else{
            for(int i=0;i<aList.size();i++){
                System.out.println(aList.get(i));
            }
        }
    }
}

class Execution{

    private OwnCalender oc;

    public Execution(){
        oc = new OwnCalender(1,3000,"Monday");
    }

    public ArrayList<Integer> getLeapYearList(int a,int b){
        ArrayList<Integer> aList = new ArrayList<Integer>();
        for(int i=a;i<=b;i++){
            if(oc.getYear(i).getLeap()){
                aList.add(i);
            }
        }
        return aList;
    }

}

class OwnCalender{

    private Year[] year;
    private int startYear;
    private int endYear;
    private CalUtil cu;

    public OwnCalender(int endYear){
        this.startYear=1900;
        this.endYear=endYear;
        cu = new CalUtil();
        year = new Year[endYear-startYear];
        year[0] = new Year(startYear,"Monday");
        calculation();
    }

    public OwnCalender(int startYear,int endYear,String startWeek){
        this.startYear=startYear;
        this.endYear=endYear;
        cu = new CalUtil();
        year = new Year[endYear-startYear];
        year[0] = new Year(startYear,startWeek);
        calculation();
    }

    private void calculation(){
        for(int i=1;i<year.length;i++){
            year[i] = new Year(i+startYear,cu.getNextWeek().get(year[i-1].getMonth(12).getLastWeek()));
        }
    }

    public Year getYear(int num){
        return year[num-startYear];
    }

}

class Year{

    private Month[] month;
    private CalUtil cu;
    private String startWeek;
    private int year;
    private boolean leap;

    public Year(int year,String startWeek){
        this.year = year;
        this.leap= isLeapYear(year);
        month = new Month[12];
        cu = new CalUtil();
        this.startWeek=startWeek;
        calculation();
    }

    private void calculation(){
        month[0] = new Month(1,startWeek,cu.getGetDayLimit().get(1));
        for(int i=1;i<12;i++){
            if(i==1&&leap){
                month[i] = new Month(i+1,cu.getNextWeek().get(month[i-1].getLastWeek()),cu.getGetDayLimit().get(i+1+20));
            }else{
                month[i] = new Month(i+1,cu.getNextWeek().get(month[i-1].getLastWeek()),cu.getGetDayLimit().get(i+1));
            }
        }

    }

    private boolean isLeapYear(int year){
        if(year%400==0){
            return true;
        }else{
            if(year%100==0){
                return false;
            }else{
                if(year%4==0){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    public Month getMonth(int num){
        return month[num-1];
    }

    public boolean getLeap(){
        return leap;
    }

}

class Month{

    private String startWeek;
    private String currentWeek;
    private int limit;
    private Day[] day;
    private CalUtil cu;
    private int month;

    public Month(int month,String startWeek,int limit){
        this.month=month;
        this.startWeek=startWeek;
        this.limit=limit;
        cu = new CalUtil();
        day = new Day[limit];
        calculation();
    }

    private void calculation(){
        currentWeek=startWeek;
        day[0]=new Day(currentWeek,1);
        for(int i=1;i<day.length;i++){
            currentWeek=cu.getNextWeek().get(currentWeek);
            day[i]=new Day(currentWeek,i+1);
        }
    }

    public String getLastWeek(){
        return day[day.length-1].getWeek();
    }

    public int getMonth(){
        return month;
    }

    public int getLimit(){
        return limit;
    }

    public Day getDay(int num){
        return day[num-1];
    }

}

class Day{

    private String week;
    private int day;

    public Day(String week,int day){
        this.week=week;
        this.day=day;
    }

    public String getWeek(){
        return week;
    }

    public int getDay(){
        return day;
    }

}

class CalUtil{

    private HashMap<String,String> nextWeek;
    private HashMap<Integer,Integer> getDayLimit;

    public CalUtil(){
        nextWeek = new HashMap<String, String>();
        getDayLimit = new HashMap<Integer, Integer>();
        initNextWeek();
        initGetDayLimit();
    }

    public HashMap<String,String> getNextWeek(){
        return nextWeek;
    }

    public HashMap<Integer,Integer> getGetDayLimit(){
        return getDayLimit;
    }

    private void initGetDayLimit(){
        getDayLimit.put(1,31);
        getDayLimit.put(2,28);
        getDayLimit.put(22,29);//leap yaer
        getDayLimit.put(3,31);
        getDayLimit.put(4,30);
        getDayLimit.put(5,31);
        getDayLimit.put(6,30);
        getDayLimit.put(7,31);
        getDayLimit.put(8,31);
        getDayLimit.put(9,30);
        getDayLimit.put(10,31);
        getDayLimit.put(11,30);
        getDayLimit.put(12,31);
    }

    private void initNextWeek(){
        nextWeek.put("Monday","Tuesday");
        nextWeek.put("Tuesday","Wednesday");
        nextWeek.put("Wednesday","Thursday");
        nextWeek.put("Thursday","Friday");
        nextWeek.put("Friday","Saturday");
        nextWeek.put("Saturday","Sunday");
        nextWeek.put("Sunday","Monday");
    }

}