import java.util.Scanner;

/**
 * Created by labuser on 14/04/08.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int x = Integer.parseInt(sc.nextLine());
            int h = Integer.parseInt(sc.nextLine());
            if(x==0&&h==0){
                break;
            }
            double tmp = Math.sqrt(Math.pow((double)x/(double)2,2)+Math.pow(h,2));
            System.out.println((double)x*tmp*2+Math.pow(x,2));
        }
    }
}
