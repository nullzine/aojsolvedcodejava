import java.util.Scanner;

/**
 * Created by nullzine on 2014/07/04.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Execution exe = new Execution(sc.nextLine());
        int n = Integer.parseInt(sc.nextLine());
        for(int i=0;i<n;i++){
            exe.doCommand(sc.nextLine().split(" "));
        }
    }
}

class Execution{

    private String data;

    public Execution(String data){
        this.data=data;
    }

    public void doCommand(String[] strs){
        if(strs[0].equals("print")){
            print(Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
        }else if(strs[0].equals("reverse")){
            reverse(Integer.parseInt(strs[1]),Integer.parseInt(strs[2]));
        }else if(strs[0].equals("replace")){
            replace(Integer.parseInt(strs[1]),Integer.parseInt(strs[2]),strs[3]);
        }else{
            System.out.println("ERROR");
        }
    }

    private void print(int start,int end){
        StringBuilder sb = new StringBuilder();
        for(int i=start;i<=end;i++){
            sb.append(data.charAt(i)+"");
        }
        System.out.println(sb);
    }

    private void reverse(int start,int end){
        StringBuffer sb = new StringBuffer();
        for(int i=start;i<=end;i++){
            sb.append(data.charAt(i)+"");
        }
        replace(start,end,sb.reverse().toString());
    }

    private void replace(int start,int end,String target){
        char[] strc = data.toCharArray();
        for(int i=0;i<target.length();i++){
            strc[start+i]=target.charAt(i);
        }
        /*
        for(int i=start;i<=end;i++){
            strc[start]=target.charAt(i-start);
        }
        */
        data = String.valueOf(strc);
    }


}