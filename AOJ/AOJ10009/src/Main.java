import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Created by labuser on 14/04/10.
 */
public class Main {
    public static void main(String[] args){
        BigDecimal PI = new OwnPI().getBigDecimal(100);
        BigDecimal n = new BigDecimal(new Scanner(System.in).nextLine());
        System.out.println(PI.multiply(n.multiply(n))+" "+ PI.multiply(n.multiply(new BigDecimal("2"))));
    }
}

class OwnPI{

    private int a;
    private int c = 8400;
    private int d,e,g;
    private int[] f = new int[8401];
    private StringBuilder sb = new StringBuilder();

    public OwnPI(){
        this.a = 10000;
        calculation();
    }

    public OwnPI(int limit){
        this.a = limit;
        calculation();
    }

    private void calculation(){
        for(int b = 0 ; b < c ; b++ ) {
            f[b] = a / 5;
        }
        e = 0;
        for(int c = 8400 ; c > 0 ; c -= 14 ) {
            d = 0;
            for(int b = c - 1 ; b > 0 ; b-- ) {
                g = 2 * b - 1;
                d = d * b + f[b] * a;
                f[b] = d % g;
                d /= g;
            }
            sb.append(String.format("%04d", e + d / a));
            e = d % a;
        }
    }

    public String toString(){
        return sb.toString();
    }

    public BigDecimal getBigDecimal(){
        return new BigDecimal("3."+toString().substring(1));
    }

    public BigDecimal getBigDecimal(int endIndex){
        return new BigDecimal("3."+toString().substring(1,endIndex));
    }

}