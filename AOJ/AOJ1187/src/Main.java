import java.util.*;

/**
 * Created by nullzine on 2015/06/02.
 */
public class Main {
    public static void main(String[] args){
        new Main().run();
    }

    private void run(){
        Scanner sc = new Scanner(System.in);
        while(true) {
            String[] strs = sc.nextLine().split(" ");
            int m = Integer.parseInt(strs[0]);
            int t = Integer.parseInt(strs[1]);
            int p = Integer.parseInt(strs[2]);
            int r = Integer.parseInt(strs[3]);
            if(m==0&&t==0&&p==0&&r==0){
                break;
            }
            Problog[] problogs = new Problog[r];
            for (int i = 0; i < problogs.length; i++) {
                strs = sc.nextLine().split(" ");
                problogs[i] = new Problog(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]), Integer.parseInt(strs[2]), Integer.parseInt(strs[3]));
            }
            //new Execution(problogs,m,t,p,r).print();
            //System.out.println("------------------");
            System.out.println(new Execution(problogs,m,t,p,r));
        }
    }

}

class Execution{

    private Problog[] problogs;
    private HashMap<Integer,Team> teamList;
    private ArrayList<Team> resultList;
    private int m;
    private int t;
    private int p;
    private int r;

    public Execution(Problog[] problogs,int m,int t,int p,int r){
        this.problogs=problogs;
        teamList = new HashMap<Integer, Team>();
        this.m=m;
        this.t=t;
        this.p=p;
        this.r=r;
        calculation();
        resultList=analysis();
    }

    private void calculation(){
        for(Problog pl:problogs){
            if(teamList.containsKey(pl.t)){
                teamList.get(pl.t).addProb(pl.p,pl.j,pl.m);
            }else{
                teamList.put(pl.t,new Team(pl.t));
                teamList.get(pl.t).addProb(pl.p,pl.j,pl.m);
            }
        }
    }

    private ArrayList<Team> analysis(){
        ArrayList<Team> resultList = new ArrayList<Team>();
        for(Map.Entry<Integer,Team> m:teamList.entrySet()){
            resultList.add(m.getValue());
        }
        Collections.sort(resultList,new Comparator<Team>() {
            @Override
            public int compare(Team o1, Team o2) {
                if(o1.getClearProb()==o2.getClearProb()){
                    return o1.getTime()-o2.getTime();
                }else{
                    return o2.getClearProb()-o1.getClearProb();
                }
            }
        });
        return resultList;
    }

    public void print(){
        for(Team t:resultList){
            System.out.println(t);
        }
    }

    private String amariTeamStr(){
        HashSet<Integer> hashSet = new HashSet<Integer>();
        for(int i=1;i<=t;i++){
            hashSet.add(i);
        }
        for(Team t:resultList){
            hashSet.remove(t.getId());
        }
        if(hashSet.size()==0){
            return "";
        }else if(hashSet.size()==1){
            int ret=-1;
            for(Integer n:hashSet){
                ret=n;
            }
            return ""+ret;
        }else{
            StringBuilder sb = new StringBuilder();
            int count=0;
            ArrayList<Integer> tmp = new ArrayList<Integer>();
            for(Integer n:hashSet){
                tmp.add(n);
            }
            Collections.sort(tmp,new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2-o1;
                }
            });
            for(Integer n:tmp){
                if(count==tmp.size()-1){
                    sb.append(n);
                }else {
                    sb.append(n + "=");
                }
                count++;
            }
            return sb.toString();
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<resultList.size();i++){
            if(i==resultList.size()-1) {
                sb.append(resultList.get(i).getId());
            }else if(resultList.get(i).getClearProb()==resultList.get(i+1).getClearProb()&&
                    resultList.get(i).getTime()==resultList.get(i+1).getTime()){
                sb.append(resultList.get(i).getId()+"=");
            }else{
                sb.append(resultList.get(i).getId()+",");
            }
        }
        if(0<resultList.size()) {
            return sb.toString()+"," + amariTeamStr();
        }else{
            return amariTeamStr();
        }
    }

}

class Team{

    private int time;
    private int id;
    private ArrayList<Prob> probArrayList;
    private int clearProb;

    public Team(int id){
        this.time=0;
        this.id=id;
        probArrayList = new ArrayList<Prob>();
        clearProb=0;
    }

    public void addProb(int probId,int answerType,int time){
        probArrayList.add(new Prob(probId,answerType,time));
        if(probArrayList.get(probArrayList.size()-1).isClear()){
            clearProb++;
            int count=0;
            for(Prob p:probArrayList){
                if(p.probId==probId&&p.time!=time){
                    count++;
                }
            }
            this.time+=20*count+time;
        }
    }

    public int getTime(){
        return time;
    }

    public int getClearProb(){
        return clearProb;
    }

    public int getId(){
        return  id;
    }

    class Prob{

        private int probId;
        private int answerType;
        private int time;

        public Prob(int probId,int answerType,int time){
            this.probId=probId;
            this.answerType=answerType;
            this.time=time;
        }

        private boolean isClear(){
            return answerType==0;
        }

    }

    public String toString(){
        return "id:"+id+" time:"+time+" clearProb"+clearProb;
    }

}

class Problog{

    public int m;
    public int t;
    public int p;
    public int j;

    public Problog(int m,int t,int p,int j){
        this.m=m;
        this.t=t;
        this.p=p;
        this.j=j;
    }

}