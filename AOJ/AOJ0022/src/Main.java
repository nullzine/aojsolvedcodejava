import java.util.*;

public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            int n = sc.nextInt();
            if(n==0){
                break;
            }
            int[] dataSet = new int[n];
            for(int i=0;i<dataSet.length;i++){
                dataSet[i]=sc.nextInt();
            }
            Execution exe = new Execution(dataSet);
            System.out.println(exe.getMax());
        }
    }
}

class Execution{

    private int[] dataSet;
    private int max = Integer.MIN_VALUE;

    public Execution(int[] dataSet){
        this.dataSet = dataSet;
        searchMax();
    }

    private void searchMax(){
        for(int i=1;i<=dataSet.length;i++){
            for(int j=0;j<dataSet.length-(i-1);j++){
                if(max<getSum(j,i)){
                    max=getSum(j,i);
                }
            }
        }
    }

    private int getSum(int start,int n){
        int count = 0;
        int sum = 0;
        for(int i=start;count<n;i++){
            sum = sum + dataSet[i];
            count++;
        }
        return sum;
    }

    public int getMax(){
        return max;
    }

}