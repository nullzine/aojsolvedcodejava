import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by labuser on 2014/04/01.
 */
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            ArrayList<String> aList = new ArrayList<String>();
            for(int i=0;i<10;i++){
                for(int j=0;j<10;j++){
                    for(int k=0;k<10;k++){
                        for(int l=0;l<10;l++){
                            if(i+j+k+l==n){
                                aList.add(Integer.toString(i)+"+"+Integer.toString(j)+"+"+Integer.toString(k)+"+"+Integer.toString(l)+"="+Integer.toString(n));
                            }
                        }
                    }
                }
            }
            System.out.println(aList.size());
        }
    }
}
